import React , { useEffect, useState }  from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import AdminLayout from "./layouts/Admin.jsx";
import AuthLayout from "./layouts/Auth.jsx";
import PlainLayout from "./layouts/Plain.jsx";
import EditorLayout from "./layouts/Editor.jsx";
import ContextData from "./context/context";
import PopupDataProvider from "./editor/PopupNew/store/provider";
import Notification from "./components/Notification.jsx/notification.jsx";
// import Mainroutes from "routes.js";
import Cookies from 'js-cookie';


const App = () =>{

    return(
<BrowserRouter>
  <ContextData>
    <PopupDataProvider>
    <Switch>
      <Route path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/auth" render={props => <AuthLayout {...props} />} />
      <Route path="/plain" render={props => <PlainLayout {...props} />} />
      <Route path="/editor" render={props => <EditorLayout {...props} />} />
      <Redirect from="/" to="/admin/index" />
      <Notification/>
       </Switch>
    </PopupDataProvider>
    </ContextData>
  </BrowserRouter>
    )  
}
export default App
// ReactDOM.render(
// ,
//   document.getElementById("root")
// );

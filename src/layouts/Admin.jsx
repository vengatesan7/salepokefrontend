import React from "react";
import { Route, Switch } from "react-router-dom";
// reactstrap components
// import { Container } from "reactstrap";
// core components
import AdminNavbar from "../components/Navbars/AdminNavbar.jsx";
// import AdminFooter from "components/Footers/AdminFooter.jsx";
import Sidebar from "../components/Sidebar/Sidebar.jsx";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom";
import {
  clientRoute,
  premiumRoute,
  managerRoute,
  saleRoute,
  allRoute,
  agencyRoute,
  userRoute
} from "../Routesdata";

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayValue: null
    };
  }
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  componentWillMount() {
    this.getroutsArray();
  }
  getroutsArray = () => {
    const role = Cookies.get("_ACrole");
    switch (role) {
      case "5":
        return this.setState({
          arrayValue: clientRoute
        });
      case "1":
        return this.setState({
          arrayValue: premiumRoute
          // arrayValue: userRoute

        });
      case "2":
        return this.setState({
          arrayValue: agencyRoute
        });
      case "3":
        return this.setState({
          arrayValue: managerRoute
        });
      case "4":
        return this.setState({
          arrayValue: saleRoute
        });
      default:
        return this.setState({
          arrayValue: premiumRoute
        });
    }
  };
  getRoutes = routes => {
    console.log(routes);
    return routes.map((prop, key) => {
      const Token = Cookies.get("_token");
      if (prop.layout === "/admin") {
        if (!Token) {
          return <Redirect to="/auth/login" />;
        } else {
          return (
            <Route
              path={prop.layout + prop.path}
              component={prop.component}
              key={key}
              exact
            />
          );
        }
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < this.state.arrayValue.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          this.state.arrayValue[i].layout + this.state.arrayValue[i].path
        ) !== -1
      ) {
        return this.state.arrayValue[i].name;
      }
    }
    return "Brand";
  };
  render() {
    return (
      <>
        <Sidebar
          {...this.props}
          routes={this.state.arrayValue}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("../assets/img/brand/inner-logo.svg"),
            imgAlt: "..."
          }}
        />
        <div className="main-content" ref="mainContent">
          <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          />
          <Switch>{this.getRoutes(this.state.arrayValue)}</Switch>
          {/* <Container fluid>
             <AdminFooter /> 
          </Container> */}
        </div>
      </>
    );
  }
}

export default Admin;

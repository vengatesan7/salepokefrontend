import React from "react";
import { Route, Switch } from "react-router-dom";
// reactstrap components
// import { Container } from "reactstrap";
// core components
// import AdminNavbar from "components/Navbars/AdminNavbar.jsx";
// import AdminFooter from "components/Footers/AdminFooter.jsx";
import Cookies from 'js-cookie'
import {clientRoute,premiumRoute, managerRoute,saleRoute,allRoute, agencyRoute ,userRoute} from "../Routesdata";
import { Redirect } from "react-router-dom";
import { Container, Row } from "reactstrap";

class Editor extends React.Component {
  constructor(props){
    super(props);
    this.state={
      arrayValue:'null'
    }
  }
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.mainContent.scrollTop = 0;
  }
  componentDidMount(){
  }
  componentWillMount(){
    this.getroutsArray();

    }
    getroutsArray = () => {
      const role = Cookies.get("_ACrole")
      switch (role) {
      case "5":
        return (  
          this.setState({
            arrayValue : clientRoute 
          })
  
        ); 
      case "1":
        return(
          this.setState({
            arrayValue : premiumRoute 
          })
  
        );  ;
      case "2":
        return (
          this.setState({
            arrayValue : agencyRoute 
          })
  
        ); ;
      case "3":
        return(
          this.setState({
            arrayValue : managerRoute 
          })
  
        );  ;
      case "4":
        return(
          this.setState({
            arrayValue : saleRoute 
          })
  
        );  ;
      default:
        return(
          this.setState({
            arrayValue : userRoute 
          })
  
        );  ;
    }
  
    }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      const Token = Cookies.get("_token")
if (prop.layout === "/editor") {
  console.log(!Token)
          if(!Token){
          return  <Redirect to='/auth/login'/>
          }else{
          return(<Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />) 
          }
        ;
      } else {
        return null;
      }
    });
  };
 
  render() {
    return (
      <>
        {/* <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("assets/img/brand/argon-react.png"),
            imgAlt: "..."
          }}
        /> */}
        <div className="main-content bg-white app-editor" ref="mainContent">
        <Container  className="pb-5 container-fluid">
            <Row className="justify-content-last">
          {/* <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          /> */}
          <Switch>{this.getRoutes(this.state.arrayValue)}</Switch>
          {/* <Container fluid>
             <AdminFooter /> 
          </Container> */}
          </Row>
          </Container>

        </div>
      </>
    );
  }
}

export default Editor;

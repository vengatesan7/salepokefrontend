import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
// reactstrap components
import { Container, Row } from "reactstrap";
import Cookies from "js-cookie";
// core components
// import AuthNavbar from "components/Navbars/AuthNavbar.jsx";
// import AuthFooter from "components/Footers/AuthFooter.jsx";
import {
  clientRoute,
  premiumRoute,
  managerRoute,
  saleRoute,
  allRoute,
  agencyRoute,
  userRoute
} from "../Routesdata";

class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arrayValue: "null"
    };
  }
  componentDidMount() {
    // document.body.classList.add("bg-default-img");
  }
  componentWillMount() {
    this.getroutsArray();
  }
  componentDidUpdate() {
  }
  getroutsArray = () => {
    const role = Cookies.get("_token");
    switch (role) {
      case "5":
        return this.setState({
          arrayValue: clientRoute
        });
      case "1":
        return this.setState({
          arrayValue: premiumRoute
        });
      case "2":
        return this.setState({
          arrayValue: agencyRoute
        });
      case "3":
        return this.setState({
          arrayValue: managerRoute
        });
      case "4":
        return this.setState({
          arrayValue: saleRoute
        });
      default:
        return this.setState({
          arrayValue: userRoute
        });
    }
  };
  componentWillUnmount() {
    // document.body.classList.remove("bg-default-img");
  }
  getRoutes = routes => {
    console.log(routes);
    const Token = Cookies.get("_token");
    return routes.map((prop, key) => {
      if (prop.layout === "/auth") {
        if (!Token) {
          return (
            <Route
              path={prop.layout + prop.path}
              component={prop.component}
              key={key}
            />
          );
        } else {
          return <Redirect to="/admin/index" />;
        }
      } else {
        return null;
      }
    });
  };
  render() {
    return (
      <>
        <div className="main-content">
          {/* <AuthNavbar /> */}
          {/* <div className="header bg-gradient-info py-7 py-lg-8">
            <Container>
              <div className="header-body text-center mb-7">
                <Row className="justify-content-center">
                  <Col lg="5" md="6">
                    <h1 className="text-white">Welcome!</h1>
                    <p className="text-lead text-light">
                      Use these awesome forms to login or create new account in
                      your project for free.
                    </p>
                  </Col>
                </Row>
              </div>
            </Container>
            <div className="separator separator-bottom separator-skew zindex-100">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-default"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </div> */}
          {/* Page content */}
          <Container className="pb-5">
            <Row className="justify-content-last align-content-full-height">
              <Switch>{this.getRoutes(this.state.arrayValue)} </Switch>
            </Row>
          </Container>
        </div>
        {/* <AuthFooter /> */}
      </>
    );
  }
}

export default Auth;

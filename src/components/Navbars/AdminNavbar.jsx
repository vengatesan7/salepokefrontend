import React from "react";
import { Link, Redirect} from "react-router-dom";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Nav,
  Container,
  Media
} from "reactstrap";
import Cookies from 'js-cookie'
import { Context } from "../../context/index";

class AdminNavbar extends React.Component {
  static contextType = Context;
  constructor(props){
    super(props);
    this.state={
      roleName : null,
      redirect : false,
    }
  }

  logOut = () => {
    Cookies.remove('_token');
    Cookies.remove('_ACid');
    // Cookies.remove('_ACrole');
    Cookies.remove('_ACname');
    this.setState({
      redirect : true
    })
  }
  componentDidMount(){
    this.getRoleName();

  }
  getRoleName = () =>{
    if(Cookies.get('_ACrole') == "1" ){
      this.setState({
        roleName : "Premium"
      })
    }else if(Cookies.get('_ACrole') == "2" ){
      this.setState({
        roleName : "Agency"
      })
    }else if(Cookies.get('_ACrole') == "3" ){
      this.setState({
        roleName : "Manager"
      })
    }else if(Cookies.get('_ACrole') == "4" ){
      this.setState({
        roleName : "Sales Person"
      })
    } else if(Cookies.get('_ACrole') == "5" ){
      this.setState({
        roleName : "Client"
      })
    }
  }
  render() {
    console.log(this.context)
    const {roleName} = this.state;
    if(this.state.redirect){
      return <Redirect to='/auth/login'/>
    }
    return (
      <>
        <Navbar className="navbar-top navbar-light" expand="md" id="navbar-main">
          <Container fluid>
            {/* <p
              className="h3 mb-0 d-none d-lg-inline-block"
              to="/"
            >
              {this.props.brandText}
            </p> */}
            <Form className="navbar-search navbar-search-lite form-inline mr-3 d-none d-md-flex" style={{minWidth:"60%"}}>
              <FormGroup className="mb-0" style={{minWidth:"100%"}}>
                <InputGroup className="input-group-alternative" style={{minWidth:"100%"}}>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fas fa-search" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Search Notifications" type="text" />
                </InputGroup>
              </FormGroup>
            </Form>
            <Nav className="align-items-center d-none d-md-flex" navbar>
              <UncontrolledDropdown nav>
                <DropdownToggle className="pr-0" nav>
                  <Media className="align-items-center">
                    <span className="avatar avatar-sm rounded-circle">
                      {/* <img
                        alt="..."
                        src={require("../../assets/img/theme/user.svg")}
                      /> */}
                      {/* {Cookies.get('_ACname').charAt(0)} */}
                    </span>
                    <Media className="ml-2 d-none d-lg-block">
                      <span className="mb-0 text-sm font-weight-bold">
                        {Cookies.get('_ACname')}
                      </span>
                      {/* <small className='role-name text-white'>{roleName }</small> */}
                      {/* <small className='role-name text-white'>Channel one</small> */}

                    </Media>
                  </Media>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-arrow" right>
                  <DropdownItem className="noti-title" header tag="div">
                    <h6 className="text-overflow m-0">Welcome!</h6>
                  </DropdownItem>
                 <DropdownItem to="/plain/switch-account" tag={Link}>
                    <i className="ni ni-ui-04" />
                    <span>Switch Account</span>
                  </DropdownItem> 
                <DropdownItem to="/admin/team-member-list" tag={Link}>
                    <i className="ni ni-single-02" />
                    <span>Team</span>
                  </DropdownItem> 
                  {/* { Cookies.get("_ACrole") === "2"  && <DropdownItem to="/admin/team-member-list" tag={Link}>
                    <i className="ni ni-single-02" />
                    <span>Team</span>
                  </DropdownItem> } */}
                  {/* { Cookies.get("_ACrole") === "2" &&  <DropdownItem to="/admin/clients-list" tag={Link}>
                    <i className="ni ni-hat-3" />
                    <span>Clients</span>
                  </DropdownItem>} */}
                  <DropdownItem to="/admin/user-profile" tag={Link}>
                    <i className="ni ni-badge" />
                    <span>My Accounts</span>
                  </DropdownItem>
                  <DropdownItem to="/admin/user-profile" tag={Link}>
                    <i className="ni ni-settings-gear-65" />
                    <span>Settings</span>
                  </DropdownItem>
                  {/* <DropdownItem to="/admin/user-profile" tag={Link}>
                    <i className="ni ni-calendar-grid-58" />
                    <span>Activity</span>
                  </DropdownItem>
                  <DropdownItem to="/admin/user-profile" tag={Link}>
                    <i className="ni ni-support-16" />
                    <span>Support</span>
                  </DropdownItem> */}
                  <DropdownItem divider />
                  <DropdownItem onClick={this.logOut}>
                    <i className="ni ni-user-run" />
                    <span>Logout</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Container>
        </Navbar>
      </>
    );
  }
}

export default AdminNavbar;

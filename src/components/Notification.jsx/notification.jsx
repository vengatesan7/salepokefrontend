import React from 'react';
import { ToastContainer, toast } from 'react-toastify';

const Notification = props => {
    if(props.notify) {
        toast(props.message, {
            position: "top-right",
            autoClose: false,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
    }
    return (
        <ToastContainer position="top-right" autoClose={false} newestOnTop closeOnClick rtl={false} pauseOnVisibilityChange draggable />
    )
}

export default Notification;
import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";

const Detailskeleton = () => (
  <React.Fragment>
    <SkeletonTheme color="#f7fafc" highlightColor="#eff1ff">
      <Row className="mt-3 mb-4">
        <Col className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={30} />
        </Col>
        <Col className={"mb-2 , mt-2 , table-head-load"}>
          {/* <Skeleton height={30} /> */}
        </Col>
        <Col className={"mb-2 , mt-2 , table-head-load"}>
          {/* <Skeleton height={30} /> */}
        </Col>
        <Col  className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={30} />
        </Col>
      </Row>
      <Row>
        <Col xs='5' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={30} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='5' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={30} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
      <Row>
        <Col xs='8' className={"mb-2 , mt-2 , table-head-load"}>
          <Skeleton height={20} />
        </Col>
      </Row>
    </SkeletonTheme>
  </React.Fragment>
);
export default Detailskeleton;

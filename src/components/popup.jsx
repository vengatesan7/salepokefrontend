import React from 'react';
import { Modal, Button } from 'react-bootstrap';

// import Button from './button';

class Popup extends React.Component {
    render() {
        return (
            <Modal {...this.props} size={this.props.size} className={this.props.className} centered backdrop={this.props.backdrop}>
                 {/* aria-labelledby="contained-modal-title-vcenter" */}
                {this.props.heading !== null ? 
                    <Modal.Header closeButton={this.props.closeButton}>
                        <Modal.Title id="contained-modal-title-vcenter">
                            {this.props.heading}
                        </Modal.Title>
                    </Modal.Header>
                : ''}
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
                {!this.props.onHide && !this.props.onHandle ? '' :
                    <Modal.Footer>
                        {!this.props.onHide ? '' : <Button onClick={this.props.onHide} className='float-left cta-2' secondary> Close </Button> }
                        {!this.props.onHandle ? '' : <Button onClick={this.props.onHandle} label={this.props.onHandleText} className='float-right '> {this.props.onHandleText}</Button>}
                    </Modal.Footer>
                }
            </Modal>
        )
    }
}

export default Popup;
import React, { useState } from "react";

import { Context } from "./index";

const ContextData = props => {
  const [userData, setUserData] = useState(null);
  const [fullWidthLoader, setFullWidthLoader] = useState(false);

  const store = {
    userData,
    setUserData,
    fullWidthLoader,
    setFullWidthLoader
  };
  
  return <Context.Provider value={store}>{props.children}</Context.Provider>;
};

export default ContextData;

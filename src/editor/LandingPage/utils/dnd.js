import uuid from "uuidv4";
import { ALL_ITEMS } from "../components/builder/menubar/constants";


const newElementData = (layers, type, newElementID) => {
    switch (type) {
        case "IMAGE":
            const newImage = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    path: "http://www.wathannfilmfestival.com/modules/flexi/images/no-image.jpg"
                }
            }
            return newImage;
        case "BUTTON":
            const newButton = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    contents: "SUBMIT",
                    display: "inline-block",
                    textAlign: "center",
                    background: "#459fed",
                    borderRadius: 28,
                    fontColor: "#ffffff",
                    fontSize: 20,
                    lineHeight: 22,
                    // width: 18,
                    styles: {
                        paddingTop: 13,
                        paddingBottom: 10,
                        paddingRight: 30,
                        paddingLeft: 30,
                    },
                    tabletStyles: {
                        paddingTop: 8,
                        paddingBottom: 10,
                        paddingRight: 10,
                        paddingLeft: 10,
                    },
                    phoneStyles: {
                        paddingTop: 8,
                        paddingBottom: 10,
                        paddingRight: 10,
                        paddingLeft: 10,
                    },
                }
            }
            return newButton;
        case "TITLE":
            const newTitle = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    // contents: "Sample Content",
                    fontSize: 40,
                    lineHeight: 40
                }
            }
            return newTitle;
        case "TIMER":
                const newTimer = {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: type,
                        enable: true,
                        endDate: new Date().toISOString().substr(0, 10),
                        endTime: new Date().getHours()+":"+ new Date().getMinutes(),
                        timerType: "default",
                        fontSize: 40,
                        lineHeight: 40,
                        fontColor: "#ffffff",
                        background: "#000000",
                        fontFamily: "inherit"
                    }
                }
                return newTimer;
        case "PRICECHART":
            const newPriceChart = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    pricelyLabel: "Starter",
                    pricelyCurrency: "$",
                    pricelyAmount: "99",
                    pricelyForeword: "Billed annually, no set up fee.",
                    pricelyList: "<ul><li>Yes</li><li>Yes</li><li>Yes</li><li>No</li><li>No</li></ul>",
                    fontSize: 40,
                    lineHeight: 40,
                    fontColor: "#333333",
                    background: "#F5F5F5",
                    fontFamily: "inherit",
                    // purchaseButton: {
                    //     enable: true,
                    //     background: "#4452d9",
                    //     content: "Purchase",
                    //     width: "fittotext",
                    //     fontColor: "#ffffff",
                    //     padding: 15,
                    //     margin: 15,
                    // }
                }
            }
            return newPriceChart;
        case "CARD":
            const newCardElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    theme: "default",
                    label: true,
                    // style: {
                    //     marginTop: 0
                    // }
                }
            }
            return newCardElement;
        case "ADDRESS":
            const newAddressElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    theme: "default"
                }
            }
            return newAddressElement;
        case "PRODUCT":
            const newProductElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    theme: "default",
                    checkoutFormData: {
                        orderDetail: true,
                        paymentMethod: true,
                        billingDetail: true,
                        profileInfo: true
                    },
                    style: {
                        fontSize: 16,
                        fontFamily: "Roboto",
                        color: "#000000"
                    },
                    submitButton: {
                        submitText: "Submit",
                        submitWidth: "fittotext",
                        style: {
                            color: "#ffffff",
                            background: "#4452d9"
                        }
                    }
                }
            }
            return newProductElement;
            case "ICON":
                const newIconElement = {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: type,
                        theme: "default",
                        contents:"fa-smile-o",
                        fontSize: 36,
                        textAlign:'center',
                        style: {
                            fontSize: 30,
                            // fontFamily: "Roboto",
                            color: "#000000"
                        },
                       
                    }
                }
                return newIconElement;
        case "PARAGRAPH":
            const newParagraphElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    contents: "<p><span style=\"color: rgb(0,0,0);font-size: 14px;font-family: Open Sans;\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem dolore, alias, numquam enim ab voluptate id quam harum ducimus cupiditate similique quisquam et deserunt, recusandae.</span></p>",
                    lineHeight: 1.5,
                    letterSpacing: 0
                }
            }
            return newParagraphElement;
        case "LINE":
            const newLineElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    contents: "Sample Content",
                    style: "solid",
                    size: 1,
                    styles: {
                        paddingTop: 0,
                        paddingBottom: 0,
                        paddingRight: 0,
                        paddingLeft: 0,
                    },
                    color: "#000"
                }
            }
            return newLineElement;
        case "UPSELL":
                const upsellElement = {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: type,
                        theme: "default",                        
                        style: {
                            fontSize: 16,
                            fontFamily: "Roboto",
                            color: "#000000"
                        },
                        submitButton: {
                            submitText: "Submit",
                            submitWidth: "fittotext",
                            style: {
                                color: "#ffffff",
                                background: "#4452d9"
                            }
                        }
                    }
                }
                return upsellElement;
        default:
            const newDefaultElement = {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: type,
                    contents: "Sample Content"
                }
            }
            return newDefaultElement;
    }
}

export const newWrapperInDND = (layers, state, position, draggableId) => {
    const newWrapperID = uuid();
    const newElementID = uuid();
    const newWrapperOrder = Array.from(layers.entities.columns[state.dropColumnID].wrappers);
    const newPosition = newWrapperOrder.indexOf(state.dropWrapperID);
    newWrapperOrder.splice(position === "bottom" ? newPosition + 1 : newPosition, 0, newWrapperID);
    if (ALL_ITEMS.find(t => t.id === draggableId).type === "FORM") {
        const newFormID = uuid();
        const nameFieldID = uuid();
        const emailFieldID = uuid();
        const phoneFieldID = uuid();
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.dropColumnID]: {
                        ...layers.entities.columns[state.dropColumnID],
                        wrappers: newWrapperOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [newWrapperID]: {
                        id: newWrapperID,
                        elements: [newElementID]
                    }
                },
                element: {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: ALL_ITEMS.find(t => t.id === draggableId).type,
                        fontSize: 18,
                        lineHeight: 24,
                        formData: [newFormID]
                    }
                },
                formData: {
                    ...layers.entities.formData,
                    [newFormID]: {
                        id: newFormID,
                        fields: [nameFieldID, emailFieldID, phoneFieldID]
                    }
                },
                field: {
                    ...layers.entities.field,
                    [nameFieldID]: {
                        id: nameFieldID,
                        type: "text",
                        label: "Full Name",
                        placeholder: "Enter your full name",
                        required: true
                    },
                    [emailFieldID]: {
                        id: emailFieldID,
                        type: "email",
                        label: "Email",
                        placeholder: "Enter your email ID",
                        required: true
                    },
                    [phoneFieldID]: {
                        id: phoneFieldID,
                        type: "number",
                        label: "Phone",
                        placeholder: "Enter your Contact Number",
                        required: true
                    }
                }
            }
        }
        return updatedLayers;
    } else if (ALL_ITEMS.find(t => t.id === draggableId).type === "ADVANCEFORM") {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.dropColumnID]: {
                        ...layers.entities.columns[state.dropColumnID],
                        wrappers: newWrapperOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [newWrapperID]: {
                        id: newWrapperID,
                        elements: [newElementID]
                    }
                },
                element: {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: ALL_ITEMS.find(t => t.id === draggableId).type,
                        fontSize: 18,
                        lineHeight: 24,
                        rows: advanceFormRow()
                    }
                }
            }
        }
        return updatedLayers;
    } else {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.dropColumnID]: {
                        ...layers.entities.columns[state.dropColumnID],
                        wrappers: newWrapperOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [newWrapperID]: {
                        id: newWrapperID,
                        elements: [newElementID]
                    }
                },
                element: newElementData(layers, ALL_ITEMS.find(t => t.id === draggableId).type, newElementID)
            }
        }
        return updatedLayers;
    }
}

const advanceFormRow = () => {
    return [
        {
            id: uuid(),
            style: {
                
            },
            cols: [
                {
                    id: uuid(),
                    size: 6,
                    style: {
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 0,
                        paddingBottom: 0
                    },
                    elements: [
                        {
                            id: uuid(),
                            fieldType: "text",
                            label: "First Name",
                            isLabel: true,
                            isRequired: true,
                            placeholder: "Enter First Name...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    ]
                },
                {
                    id: uuid(),
                    size: 6,
                    style: {
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 0,
                        paddingBottom: 0
                    },
                    elements: [
                        {
                            id: uuid(),
                            fieldType: "text",
                            label: "Last Name",
                            isLabel: true,
                            isRequired: true,
                            placeholder: "Enter Last Name...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    ]
                }
            ]
        },
        {
            id: uuid(),
            style: {
                
            },
            cols: [
                {
                    id: uuid(),
                    size: 12,
                    style: {
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 0,
                        paddingBottom: 0
                    },
                    elements: [
                        {
                            id: uuid(),
                            fieldType: "tel",
                            label: "Phone",
                            isLabel: true,
                            prependIcon: "fa fa-phone",
                            prepend: true,
                            isRequired: true,
                            placeholder: "Ex: 12345678",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    ]
                }
            ]
        },
        {
            id: uuid(),
            style: {
                
            },
            cols: [
                {
                    id: uuid(),
                    size: 12,
                    style: {
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 0,
                        paddingBottom: 0
                    },
                    elements: [
                        {
                            id: uuid(),
                            label: "Email",
                            isLabel: true,
                            fieldType: "email",
                            prependText: "Email",
                            prepend: true,
                            isRequired: true,
                            placeholder: "example@domainname.com",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    ]
                }
            ]
        },
        {
            id: uuid(),
            style: {},
            cols: [
                {
                    id: uuid(),
                    size: 12,
                    style: {
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 10,
                        paddingBottom: 10
                    },
                    elements: [
                        {
                            id: uuid(),
                            fieldType: "submit",
                            buttonText: "Submit",
                            fieldSizes: 12,
                            color: "primary",
                            buttonStyle: {
                                display: "inline-block",
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 5,
                                paddingBottom: 5
                            },
                            buttonCol: {
                                textAlign: "center"
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

const advanceForm = (layers, state, newElementOrder, newElementID, draggableId) => {
    console.log(state, newElementOrder, newElementID, draggableId)
    return {
        ...layers,
        entities: {
            ...layers.entities,
            wrappers: {
                ...layers.entities.wrappers,
                [state.dropWrapperID]: {
                    ...layers.entities.wrappers[state.dropWrapperID],
                    elements: newElementOrder
                }
            },
            element: {
                ...layers.entities.element,
                [newElementID]: {
                    id: newElementID,
                    elementType: ALL_ITEMS.find(t => t.id === draggableId).type,
                    fontSize: 18,
                    lineHeight: 24,
                    rows: advanceFormRow()
                }
            }
        }
    }
}

export const samWrapperInDND = (layers, state, position, draggableId) => {
    const newElementID = uuid();
    const newElementOrder = Array.from(layers.entities.wrappers[state.dropWrapperID].elements);
    const newPosition = newElementOrder.indexOf(state.dropElementID);
    newElementOrder.splice(position === "right" ? newPosition + 1 : newPosition, 0, newElementID);
    if (ALL_ITEMS.find(t => t.id === draggableId).type === "FORM") {
        const newFormID = uuid();
        const nameFieldID = uuid();
        const emailFieldID = uuid();
        const phoneFieldID = uuid();
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                wrappers: {
                    ...layers.entities.wrappers,
                    [state.dropWrapperID]: {
                        ...layers.entities.wrappers[state.dropWrapperID],
                        elements: newElementOrder
                    }
                },
                element: {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: ALL_ITEMS.find(t => t.id === draggableId).type,
                        fontSize: 18,
                        lineHeight: 24,
                        formData: [newFormID]
                    }
                },
                formData: {
                    ...layers.entities.formData,
                    [newFormID]: {
                        id: newFormID,
                        fields: [nameFieldID, emailFieldID, phoneFieldID]
                    }
                },
                field: {
                    ...layers.entities.field,
                    [nameFieldID]: {
                        id: nameFieldID,
                        type: "text",
                        label: "Full Name",
                        placeholder: "Enter your full name",
                        required: true
                    },
                    [emailFieldID]: {
                        id: emailFieldID,
                        type: "email",
                        label: "Email",
                        placeholder: "Enter your email ID",
                        required: true
                    },
                    [phoneFieldID]: {
                        id: phoneFieldID,
                        type: "number",
                        label: "Phone",
                        placeholder: "Enter your Contact Number",
                        required: true
                    }
                }
            }
        }
        return updatedLayers;
    } else if (ALL_ITEMS.find(t => t.id === draggableId).type === "ADVANCEFORM") {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                wrappers: {
                    ...layers.entities.wrappers,
                    [state.dropWrapperID]: {
                        ...layers.entities.wrappers[state.dropWrapperID],
                        elements: newElementOrder
                    }
                },
                element: {
                    ...layers.entities.element,
                    [newElementID]: {
                        id: newElementID,
                        elementType: ALL_ITEMS.find(t => t.id === draggableId).type,
                        fontSize: 18,
                        lineHeight: 24,
                        rows: advanceFormRow()
                    }
                }
            }
        }
        return updatedLayers;
        // return advanceForm(layers, state, newElementOrder, newElementID, draggableId)
    } else {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                wrappers: {
                    ...layers.entities.wrappers,
                    [state.dropWrapperID]: {
                        ...layers.entities.wrappers[state.dropWrapperID],
                        elements: newElementOrder
                    }
                },
                element: newElementData(layers, ALL_ITEMS.find(t => t.id === draggableId).type, newElementID)
            }
        }
        return updatedLayers;
    }
}

export const firstElementDND = (layers, state, elementType, result) => {
    const newWrapperID = uuid();
    const newElementID = uuid();
    if (elementType === "new") {
        if (ALL_ITEMS.find(t => t.id === result.draggableId).type === "FORM") {
            const newFormID = uuid();
            const nameFieldID = uuid();
            const emailFieldID = uuid();
            const phoneFieldID = uuid();
            const updatedLayers = {
                ...layers,
                entities: {
                    ...layers.entities,
                    columns: {
                        ...layers.entities.columns,
                        [state.dropColumnID]: {
                            ...layers.entities.columns[state.dropColumnID],
                            wrappers: [newWrapperID]
                        }
                    },
                    wrappers: {
                        ...layers.entities.wrappers,
                        [newWrapperID]: {
                            id: newWrapperID,
                            elements: [newElementID]
                        }
                    },
                    element: {
                        ...layers.entities.element,
                        [newElementID]: {
                            id: newElementID,
                            elementType: ALL_ITEMS.find(t => t.id === result.draggableId).type,
                            fontSize: 18,
                            lineHeight: 24,
                            formData: [newFormID]
                        }
                    },
                    formData: {
                        ...layers.entities.formData,
                        [newFormID]: {
                            id: newFormID,
                            fields: [nameFieldID, emailFieldID, phoneFieldID]
                        }
                    },
                    field: {
                        ...layers.entities.field,
                        [nameFieldID]: {
                            id: nameFieldID,
                            type: "text",
                            label: "Full Name",
                            placeholder: "Enter your full name",
                            required: true
                        },
                        [emailFieldID]: {
                            id: emailFieldID,
                            type: "email",
                            label: "Email",
                            placeholder: "Enter your email ID",
                            required: true
                        },
                        [phoneFieldID]: {
                            id: phoneFieldID,
                            type: "number",
                            label: "Phone",
                            placeholder: "Enter your Contact Number",
                            required: true
                        }
                    }
                }
            }
            return updatedLayers;
        } else if (ALL_ITEMS.find(t => t.id === result.draggableId).type === "ADVANCEFORM") {
            const updatedLayers = {
                ...layers,
                entities: {
                    ...layers.entities,
                    columns: {
                        ...layers.entities.columns,
                        [state.dropColumnID]: {
                            ...layers.entities.columns[state.dropColumnID],
                            wrappers: [newWrapperID]
                        }
                    },
                    wrappers: {
                        ...layers.entities.wrappers,
                        [newWrapperID]: {
                            id: newWrapperID,
                            elements: [newElementID]
                        }
                    },
                    element: {
                        ...layers.entities.element,
                        [newElementID]: {
                            id: newElementID,
                            elementType: ALL_ITEMS.find(t => t.id === result.draggableId).type,
                            fontSize: 18,
                            lineHeight: 24,
                            rows: advanceFormRow()
                        }
                    }
                }
            }
            return updatedLayers;
        } else {
            const updatedLayers = {
                ...layers,
                entities: {
                    ...layers.entities,
                    columns: {
                        ...layers.entities.columns,
                        [state.dropColumnID]: {
                            ...layers.entities.columns[state.dropColumnID],
                            wrappers: [newWrapperID]
                        }
                    },
                    wrappers: {
                        ...layers.entities.wrappers,
                        [newWrapperID]: {
                            id: newWrapperID,
                            elements: [newElementID]
                        }
                    },
                    element: newElementData(layers, ALL_ITEMS.find(t => t.id === result.draggableId).type, newElementID)
                }
            }
            return updatedLayers;
        }
    } else if (elementType === "old") {
        const updateElementOrder = Array.from(layers.entities.wrappers[result.source.droppableId].elements);
        const elementOldPosition = updateElementOrder.indexOf(result.draggableId);
        updateElementOrder.splice(elementOldPosition, 1);
        if (updateElementOrder.length === 0) {
            const updateWrapperOrder = Array.from(layers.entities.columns[state.elementTakenColumn].wrappers);
            const wrapperOldPosition = updateWrapperOrder.indexOf(result.source.droppableId);
            updateWrapperOrder.splice(wrapperOldPosition, 1);
            const updatedLayers = {
                ...layers,
                entities: {
                    ...layers.entities,
                    columns: {
                        ...layers.entities.columns,
                        [state.elementTakenColumn]: {
                            ...layers.entities.columns[state.elementTakenColumn],
                            wrappers: updateWrapperOrder
                        },
                        [state.dropColumnID]: {
                            ...layers.entities.columns[state.dropColumnID],
                            wrappers: [newWrapperID]
                        }
                    },
                    wrappers: {
                        ...layers.entities.wrappers,
                        [result.source.droppableId]: {
                            id: result.source.droppableId,
                            elements: updateElementOrder
                        },
                        [newWrapperID]: {
                            id: newWrapperID,
                            elements: [result.draggableId]
                        }
                    },
                }
            }
            return updatedLayers;
        } else {
            const updatedLayers = {
                ...layers,
                entities: {
                    ...layers.entities,
                    columns: {
                        ...layers.entities.columns,
                        [state.dropColumnID]: {
                            ...layers.entities.columns[state.dropColumnID],
                            wrappers: [newWrapperID]
                        }
                    },
                    wrappers: {
                        ...layers.entities.wrappers,
                        [result.source.droppableId]: {
                            id: result.source.droppableId,
                            elements: updateElementOrder
                        },
                        [newWrapperID]: {
                            id: newWrapperID,
                            elements: [result.draggableId]
                        }
                    },
                }
            }
            return updatedLayers;
        }
    }
}


export const oldElementInsamWrapperInDND = (layers, state, position, result) => {
    
    const oldWrapperOrder = Array.from(layers.entities.wrappers[result.source.droppableId].elements);
    oldWrapperOrder.splice(result.source.index, 1);
    const newElementOrder = Array.from(layers.entities.wrappers[state.dropWrapperID].elements);
    const newPosition = newElementOrder.indexOf(state.dropElementID);
    newElementOrder.splice(position === "right" ? newPosition + 1 : newPosition, 0, result.draggableId);
    if (oldWrapperOrder.length === 0) {
        const oldColumnOrder = Array.from(layers.entities.columns[state.elementTakenColumn].wrappers);
        const wrapperPosition = oldColumnOrder.indexOf(result.source.droppableId);
        oldColumnOrder.splice(wrapperPosition, 1);
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.elementTakenColumn]: {
                        ...layers.entities.columns[state.elementTakenColumn],
                        wrappers: oldColumnOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [result.source.droppableId]: {
                        ...layers.entities.wrappers[result.source.droppableId],
                        elements: oldWrapperOrder
                    },
                    [state.dropWrapperID]: {
                        ...layers.entities.wrappers[state.dropWrapperID],
                        elements: newElementOrder
                    }
                }
            }
        }
        return updatedLayers;
    } else {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                wrappers: {
                    ...layers.entities.wrappers,
                    [result.source.droppableId]: {
                        ...layers.entities.wrappers[result.source.droppableId],
                        elements: oldWrapperOrder
                    },
                    [state.dropWrapperID]: {
                        ...layers.entities.wrappers[state.dropWrapperID],
                        elements: newElementOrder
                    }
                }
            }
        }
        return updatedLayers;
    }
}

export const oldElementInNewWrapperInDND = (layers, state, position, result) => {
    
    const newWrapperID = uuid();
    const oldElementOrder = Array.from(layers.entities.wrappers[result.source.droppableId].elements)
    oldElementOrder.splice(result.source.index, 1);
    const newWrapperOrder = Array.from(layers.entities.columns[state.dropColumnID].wrappers);
    const newPosition = newWrapperOrder.indexOf(state.dropWrapperID);
    newWrapperOrder.splice(position === "bottom" ? newPosition + 1 : newPosition, 0, newWrapperID);
    if (oldElementOrder.length === 0) {
        const oldColumnOrder = Array.from(layers.entities.columns[state.elementTakenColumn].wrappers);
        const oldWrapperPosition = oldColumnOrder.indexOf(result.source.droppableId);
        oldColumnOrder.splice(oldWrapperPosition, 1);
        const updatedWrapper = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.elementTakenColumn]: {
                        ...layers.entities.columns[state.elementTakenColumn],
                        wrappers: oldColumnOrder
                    },
                    [state.dropColumnID]: {
                        ...layers.entities.columns[state.dropColumnID],
                        wrappers: newWrapperOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [result.source.droppableId]: {
                        ...layers.entities.wrappers[result.source.droppableId],
                        elements: oldElementOrder
                    },
                    [newWrapperID]: {
                        id: newWrapperID,
                        elements: [result.draggableId]
                    }
                }
            }
        }
        return updatedWrapper;
    } else {
        const updatedLayers = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [state.dropColumnID]: {
                        ...layers.entities.columns[state.dropColumnID],
                        wrappers: newWrapperOrder
                    }
                },
                wrappers: {
                    ...layers.entities.wrappers,
                    [result.source.droppableId]: {
                        ...layers.entities.wrappers[result.source.droppableId],
                        elements: oldElementOrder
                    },
                    [newWrapperID]: {
                        id: newWrapperID,
                        elements: [result.draggableId]
                    }
                }
            }
        }
        return updatedLayers;
    }
}
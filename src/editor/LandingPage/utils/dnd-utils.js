import uuid from "uuidv4";
import { TOOL_ITEMS } from "../components/builder/menubar/constants";

// Reordering the result
export const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

// Copies an item from one list to another list
export const copy = (
  source,
  destination,
  droppableSource,
  droppableDestination
) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const item = sourceClone[droppableSource.index];

  destClone.splice(droppableDestination.index, 0, { ...item, id: uuid() });
  return destClone;
};

// Moves an item from one list to another list
export const move = (
  source,
  destination,
  droppableSource,
  droppableDestination
) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const getNewSectionId = (sections, destinationRowId) => {
  return Object.keys(sections).find(r => {
    return sections[r].rows.includes(destinationRowId);
  });
};

const getOldSectionId = (sections, sourceRowId) => {
  return Object.keys(sections).find(r => {
    return sections[r].rows.includes(sourceRowId);
  });
};

const getOldRowId = (rows, sourceColId) => {
  return Object.keys(rows).find(r => {
    return rows[r].columns.includes(sourceColId);
  });
};

const getOldColIndex = (rows, oldRowId, destinationColId) => {
  return rows[oldRowId].columns.findIndex(c => c === destinationColId);
};

export const handleAddNew = (
  layers,
  draggableId,
  source,
  destination,
  type
) => {
  const newId = uuid();
  const newFormId = uuid();
  const newElement = () => {
    switch (TOOL_ITEMS.find(t => t.id === draggableId).type) {
      case "BUTTON":
        const newButton = {
          [newId]: {
            id: newId,
            elementName: "Test element",
            elementType: TOOL_ITEMS.find(t => t.id === draggableId).type,
            contents: "Button",
            display: "inline-block"
          }
        };
        return newButton;
      case "FORM":
        const newForm = {
          [newId]: {
            id: newId,
            elementName: "Test element",
            elementType: TOOL_ITEMS.find(t => t.id === draggableId).type,
            contents: "New Form",
            formData: [newFormId]
          }
        };
        return newForm;
      case "IMAGEPLUSTEXT":
        const newImgText = {
          [newId]: {
            id: newId,
            elementName: "Image and text",
            elementType: TOOL_ITEMS.find(t => t.id === draggableId).type,
            contents: "Edit your content here...",
            title: "Tittle Text",
            path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
            direction: "HORIZONTAL",
            width: 100
          }
        };
        return newImgText;
      case "SPACE":
          const newSpace = {
            [newId]: {
              id: newId,
              elementName: "Space",
              elementType: TOOL_ITEMS.find(t => t.id === draggableId).type,
              height: 100
            }
          };
          return newSpace;
      default:
        const newElement = {
          [newId]: {
            id: newId,
            elementName: "Test element",
            elementType: TOOL_ITEMS.find(t => t.id === draggableId).type,
            contents: "Add contents here"
          }
        };
        return newElement;
    }
  };
  if (TOOL_ITEMS.find(t => t.id === draggableId).type === "FORM") {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        formData: {
          ...layers.entities.formData,
          [newFormId]: {
            id: newFormId,
            columns: 1,
            fields: []
          }
        },
        element: {
          ...layers.entities.element,
          ...newElement()
        },
        column: {
          ...layers.entities.column,
          [destination.droppableId]: {
            ...layers.entities.column[destination.droppableId],
            elements: [
              ...layers.entities.column[destination.droppableId].elements.slice(
                0,
                destination.index
              ),
              newId,
              ...layers.entities.column[destination.droppableId].elements.slice(
                destination.index
              )
            ]
          }
        }
      }
    };
    return data;
  } else {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          ...newElement()
        },
        column: {
          ...layers.entities.column,
          [destination.droppableId]: {
            ...layers.entities.column[destination.droppableId],
            elements: [
              ...layers.entities.column[destination.droppableId].elements.slice(
                0,
                destination.index
              ),
              newId,
              ...layers.entities.column[destination.droppableId].elements.slice(
                destination.index
              )
            ]
          }
        }
      }
    };
    return data;
  }
};

export const handleDropLeft = (layers, draggableId, source, destination) => {
  const newColumnId = uuid();
  const newColumn = {
    [newColumnId]: {
      id: newColumnId,
      elements: [draggableId]
    }
  };
  // find(rowItem => rowItem.columns.includes(draggableId))
  const oldRow = Object.keys(layers.entities.rows).filter(r => {
    return layers.entities.rows[r].columns.includes(source.droppableId);
  });
  const destRow = Object.keys(layers.entities.rows).filter(r => {
    return layers.entities.rows[r].columns.includes(destination.droppableId);
  });
  const oldColIndex = layers.entities.rows[oldRow].columns.findIndex(
    c => c === destination.droppableId
  );
  if (destination.droppableId === source.droppableId) {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn,
          [source.droppableId]: {
            ...layers.entities.column[source.droppableId],
            elements: layers.entities.column[
              source.droppableId
            ].elements.filter(el => el !== draggableId)
          }
        },
        rows: {
          ...layers.entities.rows,
          [oldRow]: {
            ...layers.entities.rows[oldRow],
            columns: [
              ...layers.entities.rows[oldRow].columns.slice(0, oldColIndex),
              newColumnId,
              ...layers.entities.rows[oldRow].columns.slice(oldColIndex)
            ]
          }
        }
      }
    };
    return data;
  } else {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn,
          [source.droppableId]: {
            ...layers.entities.column[source.droppableId],
            elements: layers.entities.column[
              source.droppableId
            ].elements.filter(el => el !== draggableId)
          }
        },
        rows: {
          ...layers.entities.rows,
          [destRow]: {
            ...layers.entities.rows[destRow],
            columns: [
              ...layers.entities.rows[destRow].columns.slice(0, oldColIndex),
              newColumnId,
              ...layers.entities.rows[destRow].columns.slice(oldColIndex)
            ]
          }
        }
      }
    };
    return data;
  }
};

export const handleDropRight = (layers, draggableId, source, destination) => {
  const newColumnId = uuid();
  const newColumn = {
    [newColumnId]: {
      id: newColumnId,
      width: "200px",
      elements: [draggableId]
    }
  };
  // find(rowItem => rowItem.columns.includes(draggableId))
  const oldRow = Object.keys(layers.entities.rows).filter(r => {
    return layers.entities.rows[r].columns.includes(source.droppableId);
  });
  const oldColIndex = layers.entities.rows[oldRow].columns.findIndex(
    c => c === destination.droppableId
  );
  const destRow = Object.keys(layers.entities.rows).filter(r => {
    return layers.entities.rows[r].columns.includes(destination.droppableId);
  });
  if (destination.droppableId === source.droppableId) {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn,
          [source.droppableId]: {
            ...layers.entities.column[source.droppableId],
            elements: layers.entities.column[
              source.droppableId
            ].elements.filter(el => el !== draggableId)
          }
        },
        rows: {
          ...layers.entities.rows,
          [oldRow]: {
            ...layers.entities.rows[oldRow],
            columns: [
              ...layers.entities.rows[oldRow].columns.slice(0, oldColIndex + 1),
              newColumnId,
              ...layers.entities.rows[oldRow].columns.slice(oldColIndex + 1)
            ]
          }
        }
      }
    };
    return data;
  } else {
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn,
          [source.droppableId]: {
            ...layers.entities.column[source.droppableId],
            elements: layers.entities.column[
              source.droppableId
            ].elements.filter(el => el !== draggableId)
          }
        },
        rows: {
          ...layers.entities.rows,
          [destRow]: {
            ...layers.entities.rows[destRow],
            columns: [
              ...layers.entities.rows[destRow].columns.slice(
                0,
                oldColIndex + 1
              ),
              newColumnId,
              ...layers.entities.rows[destRow].columns.slice(oldColIndex + 1)
            ]
          }
        }
      }
    };
    return data;
  }
};

export const handleDropTop = (
  layers,
  draggableId,
  sourceColId,
  destRowId // destinationColId
) => {
  const newColumnId = uuid();
  const newRowId = uuid();
  const newColumn = {
    id: newColumnId,
    // width: "200px",
    elements: [draggableId],
    locatedHere: "new col"
  };
  const newRow = {
    [newRowId]: {
      id: newRowId,
      columns: [newColumnId],
      testData: "ssss"
    }
  };
  const oldRowId = getOldRowId(layers.entities.rows, sourceColId);
  // const destRowId = getOldRowId(layers.entities.rows, destinationColId);
  const oldSectionId = getOldSectionId(layers.entities.section, oldRowId);
  const newSectionId = getNewSectionId(layers.entities.section, destRowId);
  // const oldColIndex = getOldColIndex(
  //   layers.entities.rows,
  //   oldRowId,
  //   destinationColId
  // );

  const data = {
    ...layers,
    entities: {
      ...layers.entities,
      section: {
        ...layers.entities.section,
        [newSectionId]: {
          ...layers.entities.section[newSectionId],
          rows: [
            ...layers.entities.section[newSectionId].rows.slice(0, 0),
            newRowId,
            ...layers.entities.section[newSectionId].rows.slice(0)
          ]
        }
      },
      rows: {
        ...layers.entities.rows,
        ...newRow
      },
      column: {
        ...layers.entities.column,
        [sourceColId]: {
          ...layers.entities.column[sourceColId],
          elements: layers.entities.column[sourceColId].elements.filter(
            ele => ele !== draggableId
          )
        },
        [newColumnId]: { ...newColumn }
      }
    }
  };
  return data;
};

export const handleDropBottom = (
  layers,
  draggableId,
  sourceColId,
  destRowId //destinationColId
) => {
  const newColumnId = uuid();
  const newRowId = uuid();
  const newColumn = {
    id: newColumnId,
    // width: "200px",
    elements: [draggableId],
    locatedHere: "new col"
  };
  const newRow = {
    id: newRowId,
    columns: [newColumnId],
    testData: "ssss"
  };

  const oldRowId = getOldRowId(layers.entities.rows, sourceColId);
  const oldSectionId = getOldSectionId(layers.entities.section, oldRowId);
  const newSectionId = getNewSectionId(layers.entities.section, destRowId);
  const data = {
    ...layers,
    entities: {
      ...layers.entities,
      section: {
        ...layers.entities.section,
        [newSectionId]: {
          ...layers.entities.section[newSectionId],
          rows: layers.entities.section[newSectionId].rows.concat([newRowId])
        }
      },
      rows: {
        ...layers.entities.rows,
        [newRowId]: { ...newRow }
      },
      column: {
        ...layers.entities.column,
        [sourceColId]: {
          ...layers.entities.column[sourceColId],
          elements: layers.entities.column[sourceColId].elements.filter(
            ele => ele !== draggableId
          )
        },
        [newColumnId]: { ...newColumn }
      }
    }
  };
  return data;
};

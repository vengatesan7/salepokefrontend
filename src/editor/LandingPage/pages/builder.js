import React, { useState, useContext } from "react";
import { Redirect } from "react-router-dom";
import _get from "lodash.get";

import Header from "../components/header/";
import Builder from "../components/builder";
import { toast } from "react-toastify";
import { Context } from "../../../Context";

function BuiderPage() {
  // const cardId = _get(props.location.state, 'funnel_card_id', null);
  // const [landingpageId, setlandingpageId] = useState(this.props.location.state.landingpageId);
  // const [landingpageName, setlandingpageName] = useState(this.props.location.state.landingpageName);
  // if(props.location.state === undefined) {
  //   return <Redirect to="/" />;
  // }
  const { setFullWidthLoader } = useContext(Context);
  const editortype = localStorage.getItem("editortype")
  // if(editortype === "" || !editortype) {
  //   toast.error("Landing page type is not mentioned!")
  //   return <Redirect to="/" />;
  // }
  return (
    <div className="app-editor">
      <Header />
      <main className='app-content'>
        <Builder />
      </main>
    </div>
  );
}

export default BuiderPage;

import React, { useState } from "react";
import classnames from 'classnames';
import _get from "lodash.get";
import { Link } from "react-router-dom";

import { previewURL } from "../../../Components/Base/Appurl";

const PreviewPage = props => {
  const previewType = localStorage.getItem("previewtype");
  const [deviceType, setDeviceType] = useState('desktop');

  const responsiveSize = () => {
    switch(deviceType){
      case 'desktop':
        return "100%";
      case 'tablet':
        return "990px";
      case 'mobile':
        return "412px";
      default:
        return "100%";
    }
  }

  const backButton = (key) => {
    switch (key) {
      case "overview":
        return <Link to="/landingpages/landingpageoverview" className="back-link"><i className="material-icons">keyboard_backspace</i>&nbsp;&nbsp;Back to Overview</Link>
      case "funnels":
        return <Link to="/funnels/overview" className="back-link"><i className="material-icons">keyboard_backspace</i>&nbsp;&nbsp;Back to Overview</Link>
      default:
        return <div  className="back-link" onClick={() => {window.close()}}><i className="material-icons">close</i>&nbsp;&nbsp;Close Preview</div>
    }
  }

  return (
    <div className="preview">
      <div className="preview-wrapper">
        <div className="preview-header">
          {backButton(previewType)}
          {/* <div className="preview-toggle-menu">
            <span className={classnames('preview-toggle-button', deviceType === 'desktop' && 'active')}>
              <i
                style={{ cursor: "pointer", paddingRight: "10px" }}
                className={classnames("material-icons", deviceType === 'desktop' && 'active')}
                onClick={() => setDeviceType('desktop')}
              >
                desktop_mac
              </i>
            </span>
            <span className={classnames('preview-toggle-button', deviceType === 'tablet' && 'active')}>
              <i
                style={{ cursor: "pointer" }}
                className={classnames("material-icons", deviceType === 'tablet' && 'active')}
                className="material-icons"
                onClick={() => setDeviceType('tablet')}
              >
                tablet_mac
              </i>
            </span>
            <span className={classnames('preview-toggle-button', deviceType === 'mobile' && 'active')}>
              <i
                style={{ cursor: "pointer" }}
                className={classnames("material-icons", deviceType === 'mobile' && 'active')}
                className="material-icons"
                onClick={() => setDeviceType('mobile')}
              >
                phone_iphone
              </i>
            </span>
          </div> */}
        </div>
        <div className="preview-iframe" style={{ background: "#000000" }}>
          <iframe
            src={previewURL + "preview/page"}
            // src="http://localhost:3000/preview/page"
            style={{
              height: window.innerHeight - 50,
              margin: "0 auto",
              display: "block",
              width: responsiveSize()
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default PreviewPage;

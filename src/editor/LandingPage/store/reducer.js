import actions from "./action-types";

function reducer(state, action) {
  switch (action.type) {
    case actions.SAVE_STATUS:
      return {
        ...state,
        isSaved: action.status
      }
    case actions.SET_DEVICE_TYPE:
      return {
        ...state,
        deviceType: action.deviceType
      }
    case actions.ENABLE_DROP:
      return {
        ...state,
        isDragging: action.status
      }
    case actions.DROP_POSITION:
      return {
        ...state,
        dropPosition: action.position,
        dropWrapperID: action.wrapperId,
        dropColumnID: action.columnId
      }
    case actions.DROP_SAME_WRAPPER_POSITION:
      return {
        ...state,
        dropPosition: action.position,
        dropElementID: action.elementId,
        dropWrapperID: action.wrapperId
      }
    case actions.ELEMENT_TAKEN_FROM:
      return {
        ...state,
        elementTakenColumn: action.columnId,
        elementTakenWrapper: action.wrapperId,
      }
    case actions.FIRST_ELEMENT_DROP:
      return {
        ...state,
        dropPosition: action.position,
        dropColumnID: action.columnId
      }
    case actions.UPDATE_TEMPLATE_CATEGORY_ID: 
      return {
        ...state,
        templateCategoryId: action.categoryId
      }
    case actions.UPDATE_PAGE_CONATINER:
      return {
        ...state,
        layers: {
          ...state.layers,
          result: {
            ...state.layers.result,
            page: {
              ...state.layers.result.page,
              width: action.width
            }
          }
        }
      }
    case actions.UPDATE_PAGE_CONATINER_BACKGROUND_IMAGE:
      return {
        ...state,
        layers: {
          ...state.layers,
          result: {
            ...state.layers.result,
            page: {
              ...state.layers.result.page,
              backgroundImage: action.image
            }
          }
        }
      }
    case actions.UPDATE_PAGE_CONATINER_BACKGROUND:
      return {
        ...state,
        layers: {
          ...state.layers,
          result: {
            ...state.layers.result,
            page: {
              ...state.layers.result.page,
              background: action.background
            }
          }
        }
      }
    case actions.UPDATE_FORM:
      return action.newData
    case actions.UPDATE_CONTENT:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            element: {
              ...state.layers.entities.element,
              [action.id]: {
                ...state.layers.entities.element[action.id],
                contents: action.content
              }
            }
          }
        }
      }
    case actions.GET_LAYOUTS:
      return { ...state, layers: action.payload };
    case actions.ADD_LAYER:
      return { ...state, layers: [...state.layers, action.payload] };
    case actions.UPDATE_LAYER:
      return {
        ...state,
        isSaved: false,
        layers: action.payload
      };
    case actions.REMOVE_LAYER:
      return {
        ...state,
        layers: state.layers.filter(layer => layer.id !== action.id)
      };
    case actions.TOGGLE_MENUBAR:
      return { ...state, isMenuBarActive: !state.isMenuBarActive, menuBarType: action.toolbar };
    case actions.TOGGLE_CUSTOMIZER:
      return { ...state, isCustomizerActive: !state.isCustomizerActive };
    case actions.TOGGLE_SECTION_STYLE:
      return { ...state, isStyleActive: !state.isStyleActive, selectedSectionId: action.section, selectedStyleType: "section" }
    case actions.TOGGLE_COLUMN_STYLE:
      return { ...state, isStyleActive: !state.isStyleActive, selectedColumnId: action.column, selectedStyleType: "column" }
    case actions.ACTIVE_MENUBAR:
      return { ...state, isMenuBarActive: true, menuBarType: action.toolbar }
    case actions.CLOSE_TOOLBAR:
      return { ...state, isMenuBarActive: false }
    case actions.SELECT_ELEMENT:
      return { ...state, activeElement: action.payload, selectedStyleType: "element", isStyleActive: true };
    case actions.REMOVE_SELECT_ELEMENT:
      return {
        ...state,
        activeElement: null,
        selectedStyleType: null,
        isStyleActive: false
      }
    case actions.FONT_COLOR:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            element: {
              ...state.layers.entities.element,
              [action.id]: {
                ...state.layers.entities.element[action.id],
                fontColor: action.color
              }
            }
          }
        }
      };
    case actions.BACKGROUND_COLOR:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            element: {
              ...state.layers.entities.element,
              [action.id]: {
                ...state.layers.entities.element[action.id],
                background: action.color
              }
            }
          }
        }
      };
    case actions.UPDATE_PADDING:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            element: {
              ...state.layers.entities.element,
              [action.id]: {
                ...state.layers.entities.element[action.id],
                paddingTop: action.padding.Top,
                paddingRight: action.padding.Right,
                paddingBottom: action.padding.Bottom,
                paddingLeft: action.padding.Left
              }
            }
          }
        }
      };
    case actions.UPDATE_BORDER:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            element: {
              ...state.layers.entities.element,
              [action.id]: {
                ...state.layers.entities.element[action.id],
                borderRadius: action.border.radius,
                borderColor: action.border.color,
                borderWidth: action.border.size,
                borderStyle: action.border.type
              }
            }
          }
        }
      }
    case actions.SECTION_STYLE:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            section: {
              ...state.layers.entities.section,
              [state.selectedSectionId]: {
                ...state.layers.entities.section[state.selectedSectionId],
                sectionsData: {
                  ...state.layers.entities.section[state.selectedSectionId].sectionsData,
                  styles: action.style
                }
              }
            }
          }
        }
      }
    case actions.DELETE_SECTION:
      return {
        ...state,
        layers: {
          ...state.layers,
          result: {
            ...state.layers.result,
            sections: action.sections
          }
        }
      }
    case actions.FORM_POPUP:
      return { ...state, isFormBuilderActive: !state.isFormBuilderActive }
    case actions.ADVANCE_FORM_POPUP:
      return {
        ...state,
        isFormAdvanceBuilderActive: action.enable,
        advanceFormData: action.payload
      }
    case actions.FORM_COLUMN_UPDATE:
      return {
        ...state,
        layers: {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            formData: {
              ...state.layers.entities.formData,
              [action.formId]: {
                ...state.layers.entities.formData[action.formId],
                columns: action.column
              }
            }
          }
        }
      }
    case actions.UPDATE_STATE:
      return action.state
    default:
      return state;
  }
}

export default reducer;
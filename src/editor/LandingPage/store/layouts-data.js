import uuid from 'uuid/v4';

const layoutsData = {
    page: {
        width: 1100,
        background: "#cccccc",
        margin: "0 auto",
        backgroundImage: "https://wallpaperaccess.com/full/125024.jpg"
    },
    sections: [
        {
            id: uuid(),
            sectionsData: {
                name: "section 1",
                styles: {
                    background: '#ffffff'
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "row 1",
                        styles:{
                            textAlign: "center"
                        }
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "55%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    contents: "Predefind text",
                                    path: 'https://d9hhrg4mnvzow.cloudfront.net/funnels.roundclicks.com/6b0a5641-t-logo_02p00r02p00r000000001.png',
                                    width: "20%",
                                    textAlign: "left"
                                },
                            ]
                        },
                        {
                            id: uuid(),
                            width: "15%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "BUTTON",
                                    contents: "Features"
                                },
                            ]
                        },
                        {
                            id: uuid(),
                            width: "15%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "BUTTON",
                                    contents: "Pricing Comparison"
                                },
                            ]
                        },
                        {
                            id: uuid(),
                            width: "15%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "BUTTON",
                                    contents: "678-779-4597"
                                },
                            ]
                        }
                    ]
                },
            ]
        },
        {
            id: uuid(),
            sectionsData: {
                name: "Banner",
                styles: {
                    backgroundImage: 'https://d9hhrg4mnvzow.cloudfront.net/funnels.torqs.co/ac9f10cd-t-banner-1_11z09711z09700000002s.jpg',
                    paddingTop: 50,
                    paddingBottom: 50,
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "Banner row",
                        styles:{
                            
                        }
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "25%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Emprt Space",
                                    elementType: "SPACE"
                                },
                            ]
                        },
                        {
                            id: uuid(),
                            width: "75%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Banner main content",
                                    elementType: "PARAGRAPH",
                                    contents: "#1 ClickFunnelsTM Alternative to market, sell and deliver your products and services online."
                                },
                                {
                                    id: uuid(),
                                    elementName: "Banner sub content",
                                    elementType: "PARAGRAPH",
                                    contents: "The Best Affordable Sales Funnel Building Platform Priced at $47/mo.<br />So Definitely You Should STOP Paying $297"
                                },
                                {
                                    id: uuid(),
                                    elementName: "Banner sub content",
                                    elementType: "BUTTON",
                                    contents: "Get A Product Tour",
                                    backgroundColor: "rgba(0,165,56,1)"
                                }
                            ]
                        }
                    ]
                },
            ]
        },
        {
            id: uuid(),
            sectionsData: {
                name: "section 2",
                styles: {
                    backgroundImage: 'https://lh3.googleusercontent.com/ReEgruQJW1gT5T9IyxGxpJdBrzTJR6EF4LJCDaXPCH2Yh4geh1MexXjc2BX0hAMidSdor2SlkieZXzO6SwJvZw=w1920',
                    paddingTop: 50,
                    paddingBottom: 50,
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "row 2",
                        styles:{
                            backgroundColor: "#ffffff",
                            paddingTop: 25,
                            paddingRight: 25,
                            paddingBottom: 25,
                            paddingLeft: 25
                        },
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "70%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Let’s talk bottomline",
                                    textAlign: "center",
                                    marginTop: 50,
                                    marginRight: 0,
                                    marginBottom: 0,
                                    marginLeft: 25,
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "PARAGRAPH",
                                    contents: "Improve conversions and sales without sweating the tech stuff",
                                    marginTop: 25,
                                    marginRight: 0,
                                    marginBottom: 0,
                                    marginLeft: 25,
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "30%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Test form",
                                    elementType: "FORM",
                                    paddingTop: 25,
                                    paddingRight: 0,
                                    paddingBottom: 25,
                                    paddingLeft: 0,
                                    formData:[
                                        {
                                            id: uuid(),
                                            columns: 1,
                                            fields: [
                                                {
                                                    id: uuid(),
                                                    type: "text",
                                                    label: "First Name",
                                                    placeholder: "First Name",
                                                    required: false
                                                },
                                                {
                                                    id: uuid(),
                                                    type: "email",
                                                    label: "Email",
                                                    placeholder: "Enter your email",
                                                    required: true
                                                },
                                                {
                                                    id: uuid(),
                                                    type: "radio",
                                                    label: "Gender",
                                                    options: ["Male", "Female"],
                                                    required: true
                                                },
                                                {
                                                    id: uuid(),
                                                    type: "checkbox",
                                                    label: "City",
                                                    options: ["New York", "Los Angeles", "Las Vegas"],
                                                    required: false
                                                },
                                                {
                                                    id: uuid(),
                                                    type: "select",
                                                    label: "Country",
                                                    options: ["United States", "United Kingdom", "India", "China", "Japan"],
                                                    placeholder: "Select country",
                                                    required: false
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "Predefind text",
                                    fontColor: "#ff0000",
                                    background: "#000000",
                                    marginTop: 0,
                                    marginBottom: 25,
                                    paddingTop: 10,
                                    paddingRight: 16,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "This is new",
                                    fontColor: "#ff0000",
                                    background: "#000000",
                                    marginBottom: 25,
                                    paddingTop: 10,
                                    paddingRight: 16,
                                    paddingBottom: 10,
                                    paddingLeft: 10
                                },
                                {
                                    id: uuid(),
                                    elementName: "Description",
                                    elementType: "PARAGRAPH",
                                    contents: "Search...",
                                    fontColor: "#000000",
                                    background: "#ffffff",
                                    paddingTop: 8,
                                    paddingRight: 16,
                                    paddingBottom: 8,
                                    paddingLeft: 16,
                                    marginTop: 8,
                                    marginRight: 16,
                                    marginBottom: 8,
                                    marginLeft: 16,
                                    borderRadius: 15,
                                    borderColor: "#ff0000",
                                    borderWidth: 3,
                                    borderStyle: "solid"
                                }
                            ]
                        }
                    ]
                },
                {
                    id: uuid(),
                    rowData: {
                        name: "row 3",
                        styles:{
                            textAlign: "center",
                            marginTop: 50
                        },
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "100%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "&copy; Bottom Content"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: uuid(),
            sectionsData: {
                name: "section 3",
                styles: {
                    background: '#ffffff'
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "row 4",
                        styles: {
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15
                        }
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "25%",
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15,
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://image.flaticon.com/icons/svg/235/235861.svg",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Venue"
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "203 Fake St. Mountain View, San Francisco, California, USA"
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "25%",
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15,
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: 'https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/256x256/hotel.png',
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Hotel"
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "A small river named Duden flows by their place and supplies."
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "25%",
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15,
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmAJ6L1WWxWer1NV5Mw4SMfDhQZ422zxqUdKWQOwn_qYmb4fLlww",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Restaurant"
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "A small river named Duden flows by their place and supplies."
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "25%",
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15,
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABL1BMVEU1cYD////Oy8voSEnhOzud2uzV1tUgISFeX14ma3vFy8zM0dHjaGjoRUYgaXrpPD7Tt7ehn58hc4LKWV3wRUVObXoVZHWc3O7sPDsAHx/pQkHpNzUbHBzbSkyyu8k2dIPQxMTh6etKfosfGRfN2d1ZWlmvt7q3yM1sk55WaXZEbXt5nKaYZm6QrLRPUE+ovsTiLCyJp7Dx9faswcdAQUA2Nzeox9fR3N9HfIowYW0rTVYUICA9JCSAf39ah5PLODiULy8nP0UsLS0wMDAjLzKzNDS1Vl3Aoa0qVV8pRUxsbWyTkZHvMzGbVl5WJyeAXmh/LCzQU1bFNzdPR04wHRtLNDhrXGgeFhLZgYG2s8HWc3rYjIzNho9oaXbVn5+kWWHcZ2zHlaBsKSmILi6bMDAwhpIaAAAN1ElEQVR4nOWde1vTyhbGU0HDiE0RMRYoxpTWIjEWgkgRrYp4xQu4PereHHVvPd//M5zJrc1lrslMO2G/f/hokdX1y1qz5pLJRKtJl2fv9XZc19kYtgcDS7MGg/Zww3H7O70925P/9ZpM497ezvbQAqZhACgtKf8DwzCBNdze2ZPKKYvQ67hDiJbhQgmSmmDodmRhyiD0es4Aho3KluI0jIHTk0EpnLDjtk1OujGl2XY7oh0SS9hxQEG6MSVwxEIKJCyPJwVSFKHtisEbQbq2IM/EEPaGpji8CNIc9oT4JoDQcw1DMF4oA/QFFNfShLYjMDuzAoZjT5nQ3hCenhlGc6MkYylC6XwiGEsQepPgixhLtMfihO6E+EJGd+KEOwxjaqGMYGeihPZATv9AkjGwJ0e4bU6cz5e5PSHCjjbZBB0LaAXGq/yEznQCGKpAGHkJ96xpBTAUsPbkEvanGcBQZl8ioTecfAnNyxhy9f88hHsT7gNxAoAnUzkIFcjQWDyZyk64oUKGxjI2hBN6bTUyNBZoszZGRkJbLT5fwBZJ2FEpQ2MZbAMcJsIddWpMUibTdIOFUFFARkQGQoV6iaxYeg06ocKATIhUQqUBWRBphIoDMiBSCHuqA0JEyuI/mbACgFREIuFeFQAhInGqQSK0VRzJoGTYxQi9aTvOIcIwnECo2GyCJNAuQrhRHUCIiJ8vYgn7VWmEoQxst4gj7FSjjI5l4uZSGEKvSikaCmCqDYawPW1/CwhTbdCEbrUaYSgDfY8RSViRsUxW6LENknDarhaVxUroVK/MhAIOG2HlOoqxUF0GgrCqEQzEQrhdZUKQv4OaI6xoHY2Vr6c5wsG0fSypAY2wV8W+Pikju6aRJZy2gwJEJnSrXGZCAZdE6FW7zIQyPQJhZUczSWVGNilC+zyEEAbRxhJWamkGr/SiTZLwnIQwE8Qk4TkJYSaICcJzE8J0EBOE56KQhkqW0zGhRx+vAWGSjWh4CELacAYYbce9fVmEbrvOQC5lYmAzJiSHEGjbly+J1GW5jcLIE5InFWBjXihfwDiUyDieYowIh0TAbeF8vmQiDrOE5K5iQwrgpUsSp9ujDiMmJNYZID5FQ0mcb49qTUxIzBdZIZQaRJAmJG4+BLelEUpc2Iu3LmoM4xkD2U9c4BXKyG2JT2c6KULiFyUJubnIrBIJ4zTV6EmqGfPC2LKYIaG1uKhtXsdqU4M/51eUpho9SX1C0XgxpU9oLX5/uVZfImjm9K7GzxilqUZPUpilkgAvXICEi3fWlur1GaLqS/WPBRDHhJRt3IY0QEi4+H6Jghdq6SU3YpimGsO0Qibh4sslFj4f8RovYtjpB4SUfQkSCXfeswJCxLu8iO2YkLYOLJHwViNuac3VxgpSjWYjSuP6JidhsDasUSdOUgkXIsLmX38+uIjRg1drK2EQ33MGMZhCadS+YhKEzVc3oNCA/k+uBFGsr3ESBv2FT2jRLoVswvrfGLoY8sFqGETeNLVCQuoSlHTCxisy4cUoiEvfacHIeu4FhNSHmqQTNm/SCP8btMSlj7yEnYCQes9QAcJXYTbzlhq/R9QoKzSKEP7ZDAi5xzXDgJC6kqAA4YNmoWLqLypqDLcrFCC8GPaIM5yA/nqUxvD05PQJL96IBm7XOQlhqdFqfeo0WwXCqLv4wllMQR8S0lfXVSD8FqTpCu/gG45qNIYNzyoQ/rNSrJi2ISE97ioQRh3iKS+hVdMYbhuqQHizGU6guLsLT2O4t60CYTz25i2mpq0xPGqvAuHFcJrFXUyNjsZwc0QBwos31uqFiqnR0+jdoRqEf9cLFVPQ1xh2IypBGHaI3MUUuBrD3R8lCP+JRqa8g5ptzaH/LyUIXzWKLWQ4GnV2OBlCqqIOkbuYDpUgbLy6GesBTjcb8bJwKHZChufw5K8mNpporY7VjJaOZ9agTq8x34xqq0HIo3qgpRnGnnGgMewVUIsw1tJPJkRLY2i4ahIWuFNTNULWVZvqxpA1iJVth7DmsNwztSpNyLJ8Oqheb5EQC2FbiTGNTEI1Rm1yCVWYW8gkdJSYH0okhPNDFeb4MgldJdZpZBL2lVhrk0ho9JRYL5VJ2FFizVsioWkrcd9CZgw9Ufee5kMpR2gJuX8IwRZ+nJ19Ozv7sXCBj5JAWI9UjrBd/h7w/PzCt7XZra1guWhra/b0bIEDEkNYr88mhaOkEwb3gEvdx5+/9W12azXlzurWzNktVkYk4cpsXkhIBsJ+ub0Y87f+yuBFkKvfGBkRhAh7OEY6YbAXg2E/zTxGZ0i+gHH2B+6XUsoR1jEGUYwMM+BgPw1tT5S1+fMKWmtbBHe21jC/lVbGa4JBX5AquX14bZH2xK3BsK/N+r5URwrVWjLXnEHMAQy18uNWUpcvXLi8TfJ+yLI3cQZdyeje4GsgRkwm8235Mt73aG8isdRYX9Cb6Vm84URkMrn1I1/C8CGK9pcSx22Ld5GEbIBciIwmVxfyQcQTevR93ot3V0oA+rVBLCAyT7GEFsNefTQh4osbvkogMptsnubyFOf/aK8+aRKMJMy7Mvvi6e+T3/97MZtziTFPcyZnn+BMbp1lEXGEo+ctSM/MoAgzCdV88vTR8vLywcEB/PPD09mMQ0yIGZONTyef8Sa3bjESjp6ZIU0vUIQZZ34tH7Rah7v37+8etlrQpZMnaYf4Q/ji0fKB7tvc3T3UfZO/PqVMXplnI2yzPLuGIExf7xPfmd3uXKDu/ZauHyz/TvnDEMRUSnwK+PT7kc17hy1o8lcyjlsLTISJZ9cIPaL1MddbJAP4RD/QdT3yJdBzyLj8iDOISZMny9BiazdhMrxsLxKI6WKD6y0Szx+SniG1smOaRAgbL3xvDudS2vX90ZOZSg1iMis+BID3UybvQZP68tOxyXQQcYvaiWdISf2F9b2+khpIZgH1ubk8or78JJmnFCX+6+eDPGCMeDJCXP2WCCLuefDUc8Ckgdvi9Z/XEjode/MkuNz3soRzPvaBnnD7Glljk41HPmA2K0ZXLRHF2yNhT7dKPctNftR5Man37+INLs3AneeRD12oRLvRDz6MdsO8+7hI1MuRyaANji/a2GRw1fTlT7HNd18M6nFFqefxOc6I2twMn5y/8ygAHHnz+OrV47fR31tBFD/vb8Yim7wT648ggnEIu2+Pr1593E0EUdf/c/1O8IQ+zaSWO1OB+6VcFgi/Mm6F0BmoCPF58JPWa9YtaFakZ4HNuBU+DkxGiPfCrzuwmLe1Zc7F4D88eH89cb27oTdXj7vjNNVbDzk32VlHejJJQ5PxVQsJW+zWMmebcB/rab0OCcNm2D2O3JlLEr4pRfg2MhkFMcyYdXbA7Pk0vMfRYQi70ghbnIS5M4boTyFmlMrSOelZ2uXN0tw5UdwHCEeErblkEKOycxheb+ZKExOmK81bRKXRWU0izvrirjV66oJ3Hx8fP467sSij9jktWg/Xk/UZ9hbHx2/j3iL8yRErIajlCTlrTZRSo/FHtseH4gQc50W+x48vGmviI8/cY7iRmCJ8mHFnpLjJPONMUi3OC9yojSMtkOcmcp99GaVUau40boXQG25C6010cXYzJuNWyJqk6LMveTuMqC7kEJ/HH/OHUAPRVUPNnnhqF+b8Ut4zaGN3dD2RqN1DXm+SGl213AyYL4SYM2i5gxiXPr11GDHe242daTFXvbTii9ZqRasY3Xt6bJM577HnCPMG0TpqjRzSD4NVo/jfHIOPtMn9r2NGaPP5YWtkc511jIQ/C5r/JGEdpwJlJtQ4MXImmWsz4Txv/qOEE1ETAohHZAckncle4Fz9I4Q/Lb044HhMnwFkH8YTz9XnfzcCvOTZMK4/Y3kAgGByP3fZ1jmuGfndCAXeb2GBZ18TDrW+HpUJYGTztZ64bq311kOea5Ylyvy7wJmplvbwSF8PdfSmPJ9v0tp/FpvUn73m4aO+o6TQe2YsS9t/DbWvsS+jUE0WtEl9z0zhs+f9laRivynUZrqnQBKe//c9VfxlOgic/Efn/71r1X1JAuu78yqcp0gY1IcVffcaxzssz/97SCv5Llncm6v/te8Drl6XwftO53/Be7kr9vanIu9Wh9WmOoi4KkMh9KbtN4cwVYZCWLOr0hQNm0BBIqxKQcWWUTphrVcFRDO7bsFDWNtRH9HcISNQCGt91RFNbEfISKg6IhWQTqg2Ih2QgVBlRAZAFkJ1KyqlirITqorIBMhGWNtTcXRjIBctChLW7GnjIGSzuc5IWPMUm2mANmGwXYgQzhdVylQDPx8sTqhSr8HSSxQgrHVUSVRAnEyUIISNUYVMNZibID9hreZOP1NN9MKvKMLanjXdVAUWWy9YnLBW255mGE3U3SXRhLWONq0wAo2nxBQnnFpr5G2BJQhr9hSKqtG2C/lajBBONyacqkBjmkgIJPRTdXKMoFiCliSsec6EGIHpcPXxwghhc9yYACMwN+wyTpYinABjWb7ShJDRMSS+edpwSvIJIITt0TXk9B2G4ZZofwIJoXpD4ckKzGHR/iEtMYQwWV0gMFuhLdcW5JkoQqiOIwYSWnEKjD9xEkhYEwEpGK8mmhCq47bNgpTAMNuuWLyaBEIor+dYBiclMAzL6QkonTnJIPTlddwhgMGkcwIYOjB0OzLofMkiDOR1+k7bMkzDyJ/uAD8x4E+sttOXBhdIKmEoz97r7biuMxy2BwNLswaD9nDouO5Ob8+Wyhbq/wzhNH9fHbwbAAAAAElFTkSuQmCC",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Transport"
                                },
                                {
                                    id: uuid(),
                                    elementName: "Secondary Title",
                                    elementType: "PARAGRAPH",
                                    contents: "A small river named Duden flows by their place and supplies."
                                }
                            ]
                        }
                    ]
                },
            ]
        },
        {
            id: uuid(),
            sectionsData: {
                name: "section 4",
                styles: {
                    // backgroundImage: 'https://www.creativelive.com/blog/wp-content/uploads/2015/10/jsygaVpSPKes2SCJeihQ_EM2C5950.jpg'
                    background: "#233142"
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "row 5",
                        styles:{
                            height: "180px",
                            fontColor: "#ffffff",
                            textAlign: "center"
                        },
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "100%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "TITLE",
                                    contents: "Our Clients",
                                    marginTop: 50
                                },
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "PARAGRAPH",
                                    contents: "Far far away, behind the word mountains, far from the countries Vokalia and<br />Consonantia, there live the blind texts. Separated they live in"
                                }
                            ]
                        }
                    ]
                },
                {
                    id: uuid(),
                    rowData: {
                        name: "row 6",
                        styles:{
                            fontColor: "#ffffff",
                            textAlign: "center"
                        },
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "100%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Line Bar",
                                    elementType: "LINE",
                                }
                            ]
                        }
                    ]
                },
                {
                    id: uuid(),
                    rowData: {
                        name: "row 7"
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "20%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://colorlib.com/preview/theme/webhost/images/partner-5.png",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "20%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://colorlib.com/preview/theme/webhost/images/partner-4.png",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "20%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://colorlib.com/preview/theme/webhost/images/partner-3.png",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "20%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://colorlib.com/preview/theme/webhost/images/partner-2.png",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                }
                            ]
                        },
                        {
                            id: uuid(),
                            width: "20%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Secondary Three",
                                    elementType: "IMAGE",
                                    path: "https://colorlib.com/preview/theme/webhost/images/partner-1.png",
                                    contents: "Predefind text",
                                    paddingTop: 15,
                                    paddingRight: 15,
                                    paddingBottom: 15,
                                    paddingLeft: 15
                                }
                            ]
                        }
                    ]
                },
            ]
        },
        {
            id: uuid(),
            sectionsData: {
                name: "section 5",
                styles: {
                    background: "#000000"
                },
            },
            rows: [
                {
                    id: uuid(),
                    rowData: {
                        name: "row 8",
                        styles:{
                            fontColor: "#cccccc",
                            textAlign: "center"
                        },
                    },
                    columns: [
                        {
                            id: uuid(),
                            width: "100%",
                            elements: [
                                {
                                    id: uuid(),
                                    elementName: "Main Title",
                                    elementType: "PARAGRAPH",
                                    paddingTop: 25,
                                    paddingRight: 0,
                                    paddingBottom: 25,
                                    paddingLeft: 0,
                                    contents: "Copyright ©2019 All rights reserved | This template is made with by RoundClicks"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
};

export default layoutsData;

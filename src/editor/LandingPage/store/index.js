import React from "react";
import reducer from "./reducer";
import layoutsData from "./layouts-data";
import useHistory from "./useHistory";

export const Store = React.createContext();

const initialState = {
  layers: null,
  isMenuBarActive: false,
  isSaved: true,
  isCustomizerActive: false,
  isStyleActive: false,
  isFormBuilderActive: false,
  selectedStyleType: null,
  isDragging: false,
  dropPosition: null,
  dropWrapperID: null,
  dropColumnID: null,
  dropElementID: null,
  elementTakenColumn: null,
  elementTakenWrapper: null,
  activeElement: null,
  templateCategoryId: null,
  deviceType: "desktop"
};

export function StoreProvider(props) {
  const [state, dispatch] = React.useReducer(reducer, initialState);
  const history = useHistory({ layers: state.layers });
  const value = { state, dispatch, history };
  return <Store.Provider value={value}>{props.children}</Store.Provider>;
}

import uuid from 'uuid/v4';
import { elementType } from 'prop-types';

// const layoutsData = {
//     page: {
//         width: 1100,

//         background: "#FFFFFF",
//         margin: "0 auto",
//     },
//     sections: [
//         {
//             id: uuid(),
//             sectionsData: {
//                 name: "Banner",
//                 styles: {
//                     backgroundImage: 'https://colorlib.com/preview/theme/zeedapp/img/bg/slider-bg.jpg',
//                     paddingTop: 100,
//                     paddingBottom: 100,
//                 },
//             },
//             rows: [
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "Banner row",
//                         styles:{

//                         }
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Banner title",
//                                     elementType: "TITLE",
//                                     contents: "Zeedapp"
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Banner sub content",
//                                     elementType: "PARAGRAPH",
//                                     contents: "Zeedapp is a SaaS & App Landing Startups Template",
//                                     lineHeight: 18,
//                                     bold: "bold",
//                                     italic: "italic",
//                                     underline: "underline"
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Banner button",
//                                     elementType: "BUTTON",
//                                     display: "inline-block",
//                                     contents: "Purchase Now",
//                                 }
//                             ]
//                         }
//                     ]
//                 }
//             ]
//         },
//         {
//             id: uuid(),
//             sectionsData: {
//                 name: "section 2",
//                 styles: {
//                     paddingTop: 50,
//                     paddingBottom: 50,
//                 },
//             },
//             rows: [
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "row 2",
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "TITLE",
//                                     fontSize: 44,
//                                     contents: "Awesome Feature",
//                                     marginTop: 50,
//                                     marginRight: 0,
//                                     marginBottom: 0,
//                                     marginLeft: 0,
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "row 2",
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 26,
//                                     contents: "Replenish man have thing gathering lights yielding shall you",
//                                     marginTop: 50,
//                                     marginRight: 0,
//                                     marginBottom: 0,
//                                     marginLeft: 25,
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "row 4",
//                         styles:{
//                             paddingTop: 25,
//                             paddingBottom: 25
//                         },
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     fontFamily: "Ubuntu",
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "row 4",
//                         styles:{
//                             paddingTop: 25,
//                             paddingBottom: 25
//                         },
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                         {
//                             id: uuid(),
//                             width: "33.33%",
//                             paddingTop: 15,
//                             paddingRight: 15,
//                             paddingBottom: 15,
//                             paddingLeft: 15,
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Title",
//                                     elementType: "IMAGE",
//                                     path: "https://colorlib.com/preview/theme/zeedapp/img/demo-thumb/index.jpg",
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elementName: "Feature Sub",
//                                     elementType: "PARAGRAPH",
//                                     fontSize: 16,
//                                     contents: "HOME ONE / BACKGROUND SLIDER",
//                                     background: "#f9f9ff",
//                                     paddingTop: 25,
//                                     paddingRight: 15,
//                                     paddingBottom: 25,
//                                     paddingLeft: 15,
//                                     textAlign: "center"
//                                 }
//                             ]
//                         },
//                     ]
//                 }
//             ]
//         },
//         {
//             id: uuid(),
//             sectionsData: {
//                 name: "section 8",
//                 styles: {
//                     paddingTop: 50,
//                     paddingBottom: 50,
//                     background: '#f9f9f9'
//                 },
//             },
//             rows: [
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name:"Footer Title"
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Footer Title",
//                                     elementType: "PARAGRAPH",
//                                     contents: "CREATE YOUR OWN WEBSITE TODAY !"
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name:"Footer Button"
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Footer Button",
//                                     elementType: "BUTTON",
//                                     display: "inline-block",
//                                     contents: "Purchase Now"
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "row 9",
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             elements: [
//                                 {
//                                     id: uuid(),
//                                     elementName: "Footer Content",
//                                     elementType: "PARAGRAPH",
//                                     contents: "&copy;2019 TORQS | All Rights Reserved by RoundClicks",
//                                     textAlign: "center",
//                                     fontColor: "#ffffff",
//                                     marginTop: 10
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//             ]
//         }
//     ]
// };

// const layoutsData = {
//     page: {
//         width: 1100,
//         background: "#FFFFFF",
//         margin: "0 auto",
//     },
//     sections: [
//         {
//             id: uuid(),
//             sectionsData: {
//                 name: "Banner",
//                 styles: {
//                     backgroundImage: 'https://colorlib.com/preview/theme/zeedapp/img/bg/slider-bg.jpg',
//                     paddingTop: 100,
//                     paddingBottom: 100,
//                 },
//             },
//             rows: [
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "Banner row",
//                         styles:{

//                         }
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             wrappers: [
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner title",
//                                             elementType: "TITLE",
//                                             contents: "Zeedapp"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner sub content",
//                                             elementType: "PARAGRAPH",
//                                             contents: "Zeedapp is a SaaS & App Landing Startups Template",
//                                             lineHeight: 18,
//                                             bold: "bold",
//                                             italic: "italic",
//                                             underline: "underline"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         },
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         }
//                                     ]
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     id: uuid(),
//                     rowData: {
//                         name: "Banner row",
//                         styles:{

//                         }
//                     },
//                     columns: [
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             wrappers: [
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner title",
//                                             elementType: "TITLE",
//                                             contents: "Zeedapp"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner sub content",
//                                             elementType: "PARAGRAPH",
//                                             contents: "Zeedapp is a SaaS & App Landing Startups Template",
//                                             lineHeight: 18,
//                                             bold: "bold",
//                                             italic: "italic",
//                                             underline: "underline"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         },
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         }
//                                     ]
//                                 }
//                             ]
//                         },
//                         {
//                             id: uuid(),
//                             width: "100%",
//                             wrappers: [
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner title",
//                                             elementType: "TITLE",
//                                             contents: "Zeedapp"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner sub content",
//                                             elementType: "PARAGRAPH",
//                                             contents: "Zeedapp is a SaaS & App Landing Startups Template",
//                                             lineHeight: 18,
//                                             bold: "bold",
//                                             italic: "italic",
//                                             underline: "underline"
//                                         }
//                                     ]
//                                 },
//                                 {
//                                     id: uuid(),
//                                     elements: [
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         },
//                                         {
//                                             id: uuid(),
//                                             elementName: "Banner button",
//                                             elementType: "BUTTON",
//                                             display: "inline-block",
//                                             contents: "Purchase Now",
//                                             textAlign: "center",
//                                         }
//                                     ]
//                                 }
//                             ]
//                         }
//                     ]
//                 }
//             ]
//         }
//     ]
// };

// Developed Template
const layoutsData = {
    page: {
        width: 1100,
        background: "#FFFFFF",
        margin: "0 auto"
    },
    sections: [
        {
            id: "58edfcbe-d2fd-4e84-961f-1a1722786bbf",
            sectionsData: {
                name: "New Section",
                styles: {
                    backgroundImage: "https://i.imgur.com/MLdEfNw.jpg",
                    paddingTop: 20,
                    paddingBottom: 100
                }
            },
            rows: [
                {
                    id: "12787b33-dc01-460e-b728-c0eb17843335",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "86a4d299-3e58-4ad9-a4c9-f8a273e252ed",
                            wrappers: [
                                {
                                    id: "a88a44bd-c065-41eb-b824-f4edf6db2a85",
                                    elements: [
                                        {
                                            id: "e78e60f6-89ea-4b55-95ac-0e0cbf94acd6",
                                            elementType: "PARAGRAPH",
                                            contents: "Services",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            fontSize: 24,
                                            lineHeight: 24,
                                            fontColor: "#0f0e0e",
                                            fontFamily: "Montserrat Alternates",
                                            bold: "bold"
                                        },
                                        {
                                            id: "5fcf47f0-6818-4759-ac22-c5d9204e49e3",
                                            elementType: "PARAGRAPH",
                                            contents: "Process",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            fontSize: 24,
                                            lineHeight: 24,
                                            fontColor: "#0f0e0e",
                                            fontFamily: "Montserrat Alternates",
                                            bold: "bold"
                                        },
                                        {
                                            id: "dc0c8170-f562-4ddd-ac85-b334ebcdd915",
                                            elementType: "PARAGRAPH",
                                            contents: "Clients",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            fontSize: 24,
                                            lineHeight: 24,
                                            fontColor: "#0f0e0e",
                                            fontFamily: "Montserrat Alternates",
                                            bold: "bold"
                                        },
                                        {
                                            id: "96371fce-3c47-422b-b1b0-aa5a113ce757",
                                            elementType: "PARAGRAPH",
                                            contents: "Team",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            fontSize: 24,
                                            lineHeight: 24,
                                            fontColor: "#0f0e0e",
                                            fontFamily: "Montserrat Alternates",
                                            bold: "bold"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "3649cb3d-1abc-462d-8385-d58cd58828bb",
                            wrappers: [
                                {
                                    id: "1e2e98dd-a6de-4203-adbf-8d5df0c1a2e1",
                                    elements: [
                                        {
                                            id: "f736aa71-9326-411e-9d7d-96ea6e9fd246",
                                            elementType: "BUTTON",
                                            contents: "REQUEST A CALL BACK",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            textAlign: "right",
                                            paddingTop: 10,
                                            paddingRight: 20,
                                            paddingBottom: 10,
                                            paddingLeft: 20,
                                            borderRadius: 0,
                                            borderColor: "#000000",
                                            borderWidth: "3",
                                            borderStyle: "solid",
                                            background: "#4452d9",
                                            fontColor: "#ffffff"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "b44acf14-e67a-4f95-9a5c-7f35a9e2c7a7",
                    columns: [
                        {
                            id: "85fd5c4e-4541-4196-9998-e27d9ec770bf",
                            wrappers: [
                                {
                                    id: "aea0de4b-1ce1-4a82-bc60-733229dd17e8",
                                    elements: [
                                        {
                                            id: "c754bdec-6178-4ffb-aab3-233eaab235c1",
                                            elementType: "TITLE",
                                            contents: "Success starts with<div>hardworking consultants</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            textAlign: "center",
                                            fontSize: 40,
                                            fontColor: "#1E3264",
                                            lineHeight: 40,
                                            paddingTop: 100
                                        }
                                    ]
                                },
                                {
                                    id: "6cfa3a32-bd1c-44bd-9868-ba3434386be3",
                                    elements: [
                                        {
                                            id: "e3358cbd-627f-449f-b1ac-c461519d03b9",
                                            elementType: "PARAGRAPH",
                                            contents: "Morbi leo tortor, fermentum sed orci vitae, tempor auctor turpis. Fusce bibendum accumsan fringilla. Nulla aliquam luctus sem, at posuere nibh lobortis et. Ut ac mollis risus, eu congue libero. Nulla vestibulum ultricies tortor at ultrices.",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            textAlign: "center",
                                            paddingRight: 100,
                                            paddingLeft: 100,
                                            fontSize: 18,
                                            fontColor: "#1E3264",
                                            lineHeight: 35
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "8f75ba30-1d90-4b41-b5d3-b6a6594e8af0",
            sectionsData: {
                name: "New Section",
                styles: {
                    background: "#cff1fd",
                    paddingTop: 100,
                    paddingBottom: 100
                }
            },
            rows: [
                {
                    id: "184cbff6-6c70-4d4f-adfa-ba0bbec885d3",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "71d46db7-62b5-4fe5-945f-4b69e8ab4e14",
                            wrappers: [
                                {
                                    id: "231f8359-f952-42c7-90a1-54e6c861fe93",
                                    elements: [
                                        {
                                            id: "2370e0a2-c830-441d-a695-aaedead1d8cd",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/ef53fe7d-o-consultancy-process_0cs07n000000000000001.png",
                                            width: 85
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "8d49d079-3da6-4375-87be-b7b7c4a10200",
                            wrappers: [
                                {
                                    id: "3d7fa44e-c7b8-422e-ae87-c546163b5816",
                                    elements: [
                                        {
                                            id: "697013bc-2410-4305-9c56-1134ef353c1e",
                                            elementType: "PARAGRAPH",
                                            contents: "EXPERIENCE",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            bold: "bold",
                                            fontFamily: "Oxygen"
                                        }
                                    ]
                                },
                                {
                                    id: "598e77c3-e867-4818-acba-7417d8dc9b8d",
                                    elements: [
                                        {
                                            id: "0a100415-dd46-49a5-90df-2748563646f9",
                                            elementType: "TITLE",
                                            contents: "Our Process",
                                            display: "inline-block",
                                            fontFamily: "Josefin Sans",
                                            bold: "bold"
                                        }
                                    ]
                                },
                                {
                                    id: "f8f2f4c7-48a5-462f-b0cc-734429a2a8f5",
                                    elements: [
                                        {
                                            id: "19c5906b-e792-409e-8abe-a25b32d6d83c",
                                            elementType: "PARAGRAPH",
                                            contents: "Morbi egestas ultricies est. Proin eu odio nibh. Praesent venenatis mi vitae pharetra porttitor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
                                            display: "inline-block",
                                            fontFamily: "Catamaran",
                                            bold: null,
                                            fontSize: 18,
                                            marginLeft: 0,
                                            paddingRight: 70
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "formrowid",
                    rowData: {
                        name: "Form"
                    },
                    columns: [
                        {
                            id: "formcolumnid",
                            wrappers: [
                                {
                                    id: "formwrapperid",
                                    elements: [
                                        {
                                            id: "formelementid",
                                            elementType: "FORM",
                                            columns: 1,
                                            formData: [
                                                {
                                                    id: "formdataid",
                                                    fields: [
                                                        {
                                                            id: "formtextfield",
                                                            type: "text"
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "aa20e214-7dd8-45e1-98f9-4a6f330982c5",
            sectionsData: {
                name: "New Section",
                styles: {
                    background: "#cff1fd",
                    paddingBottom: 100
                }
            },
            rows: [
                {
                    id: "41646e11-31db-4415-9642-b0afa96bd3e5",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "1d4743e8-dfd1-49ee-9575-dd87f4125e11",
                            wrappers: [
                                {
                                    id: "87d3dbc9-cda1-4854-a801-c3f7486441a1",
                                    elements: [
                                        {
                                            id: "e36fa732-5c1c-46ec-8b47-480d4075f7c7",
                                            elementType: "PARAGRAPH",
                                            contents: "TOOLKIT",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            bold: "bold",
                                            fontFamily: "Oxygen"
                                        }
                                    ]
                                },
                                {
                                    id: "8ad5aef7-9387-46b7-8252-ea09ece89a42",
                                    elements: [
                                        {
                                            id: "70ddcb71-68fa-4af0-8d12-5c3e938b51e4",
                                            elementType: "TITLE",
                                            contents: "Our Services",
                                            display: "inline-block",
                                            fontFamily: "Josefin Sans",
                                            bold: "bold"
                                        }
                                    ]
                                },
                                {
                                    id: "8763c72e-c726-4c33-8ac6-15eba3f4cb7c",
                                    elements: [
                                        {
                                            id: "02eda3c5-8482-4183-833c-6187a49a30c5",
                                            elementType: "PARAGRAPH",
                                            contents: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer est tellus, pulvinar ac iaculis vel, varius eu arcu. Integer id lectus posuere nisi dignissim gravida at ac justo. Ut nec dui rutrum, cursus mauris sed integer eu vel.",
                                            display: "inline-block",
                                            fontFamily: "Catamaran",
                                            bold: null,
                                            fontSize: 18,
                                            marginLeft: 0,
                                            paddingRight: 70
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "b4268668-f6ec-4496-b698-c8d2974f5503",
                            wrappers: [
                                {
                                    id: "193b0a8c-c75f-4661-a1f5-6d3b8dba7453",
                                    elements: [
                                        {
                                            id: "542153c7-b2b9-43b7-986e-d73a01720e53",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/5f975b5d-o-consultancy-services_0cs07n000000000000001.png",
                                            width: 85,
                                            textAlign: "left"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "8d8e856a-1ec1-409a-8e61-8678fafba048",
            sectionsData: {
                name: "New Section",
                styles: {
                    paddingTop: 0,
                    background: "#cff1fd",
                    paddingBottom: 50
                }
            },
            rows: [
                {
                    id: "a9c9a70b-16d4-4cda-b407-6aa601f48a43",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "0a086954-de57-4f6e-a2ae-1fbef9fa796b",
                            wrappers: [
                                {
                                    id: "34f28b76-20bd-4efa-8bb6-008d1f840e3e",
                                    elements: [
                                        {
                                            id: "764420db-92f4-4cda-8863-f99d8616d422",
                                            elementType: "PARAGRAPH",
                                            contents: "CLIENTS THAT LOVE US",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            textAlign: "center",
                                            fontFamily: "Ubuntu",
                                            bold: "bold"
                                        }
                                    ]
                                },
                                {
                                    id: "591556f7-6bab-4898-87f5-5d14a6df2394",
                                    elements: [
                                        {
                                            id: "3e95b691-dfb5-476b-9ed0-5d320323499b",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/d126eab4-d8ac-41e5-b3bb-1c358acf43c5/e993f0a8-o-consultancy-powerbull.svg"
                                        },
                                        {
                                            id: "e84c74f2-6092-434d-9070-9445a4c6102b",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/57860560-ac4c-4e7e-9eb3-ab703e3a808f/91262659-o-consultancy-lifebook.svg"
                                        },
                                        {
                                            id: "030ea12d-dc46-4bee-be76-3ab801dd9f1b",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/d6ae0d0c-c79e-4c81-90f6-6a7b88e3cc56/a859f9d6-o-consultancy-javastar.svg"
                                        },
                                        {
                                            id: "d1baf1c4-31e3-47e3-b404-6601224557c8",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/e3a472f9-1464-4e5a-b947-011eed70db80/3d80d19a-o-consultancy-mailbanana.svg"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    id: "6519f5cd-6d9b-4bac-96e0-16d32f3b98c8",
                    columns: [
                        {
                            id: "053bf917-6a4a-4059-9e2c-9235f9a9e86f",
                            wrappers: [
                                {
                                    id: "621686a5-670d-4f8a-bff6-9ae1c91a6386",
                                    elements: [
                                        {
                                            id: "fd2a1e04-df5d-47a6-b91a-b7206edfe2fa",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/67cb8474-ef66-4847-83c0-9f05e3392d10/70d3f04b-o-consultancy-socialbeer.svg"
                                        },
                                        {
                                            id: "e6093e9f-7eac-411a-9742-96f19dbbfcda",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/19e951c1-b4ed-44c3-8a82-743fbc7d9e31/d796f021-o-consultancy-cloudforce.svg"
                                        },
                                        {
                                            id: "9e3be5b9-21ef-47bd-bb58-edf2d8f60e95",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/835fba1d-0a79-44a2-bed4-597c4ca6634c/26810c9d-o-consultancy-simplefolk.svg"
                                        },
                                        {
                                            id: "5b42298e-4e74-4958-8056-9bfdce5966a9",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            width: 50,
                                            textAlign: "center",
                                            paddingTop: 20,
                                            paddingBottom: 20,
                                            path: "https://app.unbounce.com/publish/assets/d6280c65-ce2a-4328-b8ed-d75dc89460f5/5dc017e0-o-consultancy-calmseat.svg"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "74275897-dd78-494c-94e8-429f8f4bbc74",
            sectionsData: {
                name: "New Section",
                styles: {
                    background: "#cff1fd",
                    paddingTop: 50,
                    paddingBottom: 50
                }
            },
            rows: [
                {
                    id: "721d7620-0f9a-40d6-912a-f3d89f6dede2",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "48633fd6-ce23-44fa-a246-2ebd0e181d50",
                            wrappers: [
                                {
                                    id: "8ff88998-129e-44d5-b98b-96082ff190f9",
                                    elements: [
                                        {
                                            id: "8e72dc25-5ffa-4ed2-af36-329b71ad3f7f",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/a5ff28ef-o-consultancy-team-profile-01_03u03u000000000000001.png",
                                            width: 80
                                        },
                                        {
                                            id: "e34ace8e-7d01-422d-9d5d-5ad94735198a",
                                            elementType: "PARAGRAPH",
                                            contents: "Maecenas fermentum sit amet urna vel porttitor. Mauris bibendum volutpat ultricies.<div><br></div><div><b>JOHN CLICK&nbsp;</b></div><div>Marketing Director</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingTop: 30
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "cc389da9-3787-45a8-9544-e21afa861257",
                            wrappers: [
                                {
                                    id: "8c1c5d4c-fff4-4ec7-a768-647dd0202945",
                                    elements: [
                                        {
                                            id: "b28cdd58-0cd7-4442-912e-b741cf591d44",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/db8b96b0-o-consultancy-team-profile-02_03u03u000000000000001.png",
                                            width: 80
                                        },
                                        {
                                            id: "8c7eb1c5-15e9-46b6-a685-e2cd4cf7075d",
                                            elementType: "PARAGRAPH",
                                            contents: "Maecenas fermentum sit amet urna vel porttitor. Mauris bibendum volutpat ultricies.<div><br></div><div><b>JOHN CLICK&nbsp;</b></div><div>Marketing Director</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingTop: 30
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "9d0b2601-61a4-405e-a218-65385eec3f0d",
            sectionsData: {
                name: "New Section",
                styles: {
                    background: "#cff1fd",
                    paddingBottom: 100
                }
            },
            rows: [
                {
                    id: "40d527de-aeab-4679-8f55-e5bf88e92b48",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "0a341d07-2ff8-4e5e-a5c6-9ddcce055d21",
                            wrappers: [
                                {
                                    id: "fe0e427e-2400-4bd4-a61c-2294ea0b4c33",
                                    elements: [
                                        {
                                            id: "6ce67727-d97f-4982-b20a-ea1de373a666",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/512ca3d8-o-consultancy-team-profile-03_03u03u000000000000001.png",
                                            width: 80
                                        },
                                        {
                                            id: "fbe39eae-15a3-4922-96ca-45e3c0ee3266",
                                            elementType: "PARAGRAPH",
                                            contents: "Maecenas fermentum sit amet urna vel porttitor. Mauris bibendum volutpat ultricies.<div><br></div><div><b>JOHN CLICK&nbsp;</b></div><div>Marketing Director</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingTop: 30
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "8abc24db-868c-4485-8cfa-bc9ee38e849f",
                            wrappers: [
                                {
                                    id: "69bd02b5-ba70-4676-bce4-6a5d61adc826",
                                    elements: [
                                        {
                                            id: "223d66c7-bbfb-46af-af3c-b1cc63e4a55d",
                                            elementType: "IMAGE",
                                            contents: "Sample Content",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            path: "http://d9hhrg4mnvzow.cloudfront.net/unbouncepages.com/a-consultancy/e638e685-o-consultancy-team-profile-04_03u03u000000000000001.png",
                                            width: 80
                                        },
                                        {
                                            id: "4671a188-e237-4c01-894e-9b9df83d733f",
                                            elementType: "PARAGRAPH",
                                            contents: "Maecenas fermentum sit amet urna vel porttitor. Mauris bibendum volutpat ultricies.<div><br></div><div><b>JOHN CLICK&nbsp;</b></div><div>Marketing Director</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingTop: 30
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            id: "a9b50999-a44d-4255-8239-805866337cdd",
            sectionsData: {
                name: "New Section",
                styles: {
                    paddingTop: 50,
                    paddingBottom: 50
                }
            },
            rows: [
                {
                    id: "5a4607c0-637f-40b7-8435-2ebaf9f4800d",
                    rowData: {
                        name: "New Row"
                    },
                    columns: [
                        {
                            id: "0b4378f0-4b9a-4701-a821-efa40ca87e09",
                            wrappers: [
                                {
                                    id: "f053191b-d739-41fa-8734-973262b746f7",
                                    elements: [
                                        {
                                            id: "ab79d363-6b6d-420f-b6c4-fe7616a19b40",
                                            elementType: "PARAGRAPH",
                                            contents: "<b>VANCOUVER</b><div><br></div><div>400-401 West Georgia Street</div><div>Vancouver, BC, Canada, V6B 5A1</div>",
                                            display: "inline-block",
                                            marginLeft: 0
                                        }
                                    ]
                                },
                                {
                                    id: "8ec8f278-eeab-44af-83f4-855c0341f0c5",
                                    elements: [
                                        {
                                            id: "02f92f3b-4a49-458b-b2bc-80f8092c179a",
                                            elementType: "PARAGRAPH",
                                            contents: "<b>BERLIN</b><div>Friedrichstraße 68</div><div>10117 Berlin, Germany</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingTop: 30
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "e286c710-61e2-413a-bba0-6cc59f7f64cf",
                            wrappers: [
                                {
                                    id: "660d44fc-5f97-4829-872a-5c84a62f09c8",
                                    elements: [
                                        {
                                            id: "1bf4e2d5-94b3-41c1-8612-2b54d7506c91",
                                            elementType: "PARAGRAPH",
                                            contents: "<b>FOLLOW US ON</b><div><br></div><div>Twitter</div><div>Facebook</div><div>LinkedIn</div><div>Instagram</div>",
                                            display: "inline-block",
                                            marginLeft: 0
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            id: "3ae49b99-bcae-4672-99db-c9fc944b223a",
                            wrappers: [
                                {
                                    id: "ef370ccc-0c3d-48a5-923e-a234a6bc6d4f",
                                    elements: [
                                        {
                                            id: "9df1a9f2-a3b9-4a42-82ee-7e81b0c7e4c9",
                                            elementType: "PARAGRAPH",
                                            contents: "<b>ABOUT US</b><div><br></div><div>Since 2009, Unbounce has helped marketers and digital agencies increase website and campaign conversions.&nbsp;</div><div><br></div><div>© 2018 Unbounce All rights reserved</div>",
                                            display: "inline-block",
                                            marginLeft: 0,
                                            paddingRight: 30
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}

export default layoutsData;

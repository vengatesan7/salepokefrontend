import React, { useContext, useState, useEffect } from "react";
import { withRouter, Link, Redirect } from "react-router-dom";
import { denormalize, schema } from "normalizr";
import { ClipLoader } from "react-spinners";
import { toast } from "react-toastify";
import classNames from "classnames";
import _get from "lodash.get";

import Button from "../elements/button";
import logo from "../../../../images/rc-logo-mini.png";
import actions from "../../store/action-types";
import { Store } from "../../store/index";
import layoutsData from "../../store/layouts-data2";
import layersSchema from "../builder/schema";
import { Context } from "../../../../Context";
import { PostData } from "../../../../services/PostData";
import Popup from "../../../../Components/Base/popup";
import "./header.scss";
import { Alert } from "react-bootstrap";

const denormalizeData = data => {
  const { result, entities } = data;
  const denormalizedData = denormalize(result, layersSchema, entities);
  return denormalizedData;
};

const HeaderActions = () => {
  // withRouter(({ history })
  const {
    state,
    dispatch,
    history: {
      state: { layers },
      undo,
      redo,
      canUndo,
      canRedo
    }
  } = useContext(Store);
  const {
    setFullWidthLoader,
    selectedFunnelStep,
    selectedFunnelStepData
  } = useContext(Context);
  const [publishLoader, setPublishLoader] = useState(false);
  const [republishStatus, setRepublishStatus] = useState(false);
  const [publishButton, setPublishButton] = useState(false);
  const [saveLoader, setSaveLoader] = useState(false);
  const editortype = localStorage.getItem("editortype");
  const templateId = localStorage.getItem("templateId");

  // const customTemplateHotCode = {"page":{"width":1100,"background":"#ffffff","margin":"0 auto","fontColor":"#000000","fontFamily":"Josefin Sans"},"sections":[{"id":"a9ce006a-891c-4966-b2f5-d25e5605bf12","sectionsData":{"name":"Banner","styles":{"background":"#e0e1de","paddingTop":50,"paddingBottom":90,"backgroundImage":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/1361777618.png"}},"rows":[{"id":"ad12cc3f-e1b8-46bd-903e-cf330aea46a7","columns":[{"id":"08732301-6772-4f3d-a006-f1cee550e7d7","width":50,"wrappers":[{"id":"676bccdd-c628-4699-aafc-e68165e3e6e3","elements":[{"id":"20b62e3a-02a9-4796-b4d8-cca1d88e9624","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/1435938913.png","width":100,"styles":{"paddingRight":400},"tabletStyles":{"paddingRight":250},"phoneStyles":{"paddingRight":100,"paddingLeft":100}}]}]},{"id":"c300a387-63a8-4427-9f21-02a834d729bf","width":50,"wrappers":[{"id":"faad9dfa-7b86-4d98-8bf2-6910090b889c","elements":[{"id":"17a29bd4-6427-4a53-b8c3-dace6a926249","elementType":"BUTTON","contents":"Sign up","display":"inline-block","textAlign":"right","background":"#219deb","fontColor":"#ffffff","fontSize":14,"lineHeight":16,"paddingTop":11,"paddingRight":34,"paddingBottom":11,"paddingLeft":34,"fontFamily":"Roboto","borderColor":"#ffffff","borderStyle":"none","borderRadius":1,"borderWidth":1,"styles":{"paddingRight":35,"paddingLeft":35,"paddingBottom":10,"paddingTop":10},"phoneStyles":{"paddingRight":35,"paddingLeft":35,"paddingBottom":10,"paddingTop":10}}]}]}]},{"id":"e2cb3cef-b988-4190-91f5-5c4a72f0dbb8","columns":[{"id":"4f58cc35-3670-4a1d-8860-3cc16512e057","width":100,"wrappers":[{"id":"dae699c8-5585-4f44-a251-e9309ca2224f","elements":[{"id":"1ba67ec8-37f4-4976-9ed1-dc11ee425fed","elementType":"PARAGRAPH","contents":"<h1 style=\"text-align:center;\"><span style=\"font-size: 48px;font-family: Roboto;\"><strong>Thanks for reaching out!</strong></span></h1>\n<p style=\"text-align:center;\"></p>\n","fontSize":16,"lineHeight":58,"marginLeft":0,"textAlign":"center","fontColor":"#ffffff","marginTop":85,"styles":{"marginTop":85}}]},{"id":"65c684d6-30a2-4750-821c-ba003caf2417","elements":[{"id":"72ec7882-ec09-4c5e-9ac6-823c6012f4a6","elementType":"PARAGRAPH","contents":"<p style=\"text-align:center;\"><span style=\"font-family: Roboto;\">Join 1,200,000+ professionals who grow their business with videos</span></p>\n","fontSize":16,"lineHeight":16,"textAlign":"center","fontColor":"#ffffff","marginTop":2}]},{"id":"afaa3cbc-ba28-4c0a-af7c-11c1c3ffd9b2","elements":[{"id":"98ad48e4-f113-48fb-88dc-6751a264650d","elementType":"BUTTON","contents":"START NOW","display":"inline-block","textAlign":"center","background":"#4fb8f9","fontColor":"#ffffff","fontSize":16,"lineHeight":16,"paddingTop":16,"paddingRight":28,"paddingBottom":16,"paddingLeft":28,"fontFamily":"Roboto","marginTop":46,"marginBottom":5,"styles":{"marginTop":50,"marginBottom":5,"paddingRight":30,"paddingLeft":30,"paddingTop":15,"paddingBottom":15}}]}]}]}]},{"id":"9d50d141-df0d-4062-a1db-d937f3e83d48","sectionsData":{"name":"Partners","styles":{"paddingTop":30,"paddingBottom":30}},"rows":[{"id":"4666cd76-cd0e-4b84-906d-350b8d23fc30","rowData":{"name":"Row 1"},"columns":[{"id":"0f06adaf-4e63-47d5-bc0e-ef7c99a85958","wrappers":[{"id":"bed46202-02d7-44f5-8c5f-727e9b7c22c2","elements":[{"id":"cd44fefc-5ecf-4663-aebb-b3b5254c861b","elementType":"TITLE","contents":"<p style=\"text-align:center;\"><span style=\"font-size: 18px;font-family: Roboto;\">Certified  partners</span></p>\n","fontSize":40,"lineHeight":32}]}],"styles":{"background":""},"width":25},{"id":"37a573f3-b5e0-4964-8032-3cc8edafaf3f","wrappers":[{"id":"f8370ebc-d6f7-47f4-a1e5-9ae13cbff093","elements":[{"id":"d8e3d55c-d4ba-43f1-8c81-cc03c8e4fba7","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/153172384.png","width":100,"textAlign":"center","styles":{"paddingRight":70,"paddingLeft":70},"phoneStyles":{"paddingRight":100,"paddingLeft":100,"paddingTop":25,"paddingBottom":25}}]}],"width":25},{"id":"90c289bb-d215-49c7-885e-19cde4150a61","wrappers":[{"id":"438da03f-85f7-4c33-8b8f-a616e99d2fbe","elements":[{"id":"73db7fc6-f1cc-41e8-99b8-630d066e345a","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/298058088.png","width":100,"textAlign":"center","styles":{"paddingRight":70,"paddingLeft":70},"phoneStyles":{"paddingRight":100,"paddingLeft":100,"paddingTop":25,"paddingBottom":25}}]}],"width":25},{"id":"95364df5-f0ee-45fb-be02-7226c2338ea7","wrappers":[{"id":"83d17b96-5700-4f84-b36e-39694b670061","elements":[{"id":"16527ced-3e2d-42e2-8454-99a562a207b8","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/2127520211.png","width":100,"textAlign":"center","styles":{"paddingRight":70,"paddingLeft":70},"phoneStyles":{"paddingRight":100,"paddingLeft":100,"paddingTop":25,"paddingBottom":25}}]}],"width":25}]}]},{"id":"ce37a683-fad5-4b95-902f-d98cdde1afd3","sectionsData":{"name":"Footer","styles":{"background":"#1e2f39","paddingTop":50,"paddingBottom":50}},"rows":[{"id":"7f451e16-d03e-4acb-9f14-9a65770f6717","columns":[{"id":"2fa36ea7-4292-43f8-b675-5ef3056bb391","width":38.83177570093458,"wrappers":[{"id":"f5ef638b-3be0-42af-8dcf-2e9d6d858d15","elements":[{"id":"fcbd3b2c-321a-43c0-af36-33596029d6c8","elementType":"PARAGRAPH","contents":"<font color=\"#fdfefe\" size=\"2\" face=\"Roboto\">PROMO</font>","fontSize":16,"lineHeight":16}]},{"id":"cd2b6971-932a-4719-86b0-8c847e05e059","elements":[{"id":"d34e9e97-46fc-4a21-be51-c870bf393c7a","elementType":"PARAGRAPH","contents":"<p><span style=\"color: #ffffff;font-size: 14px;font-family: Roboto;\">Lorem Ipsum is simply dummied text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to</span></p>\n","fontSize":16,"lineHeight":18,"marginTop":17,"paddingRight":41,"styles":{"marginTop":20,"paddingRight":50}}]}]},{"id":"a8c74df7-4a81-4502-b683-3bb9761e0492","width":27.570093457943926,"wrappers":[{"id":"91cb7c07-5aec-470c-a4b6-f1b480029319","elements":[{"id":"cbc80aa4-6c5d-4e76-af94-a717e32578b5","elementType":"PARAGRAPH","contents":"<font face=\"Roboto\" color=\"#ffffff\" size=\"1\">FAQ</font>","fontSize":16,"lineHeight":16}]},{"id":"69d7b3db-bd27-45f4-a2d3-2a4e4ef8dad2","elements":[{"id":"87b44505-474b-4127-b2d6-50fdb963fa5d","elementType":"PARAGRAPH","contents":"<font face=\"Roboto\" color=\"#ffffff\" size=\"1\">TERMS OF SERVICE</font>","fontSize":16,"lineHeight":16,"marginTop":11}]},{"id":"7fc85f3a-c144-4d6a-a708-af32dbd82ece","elements":[{"id":"89e91422-82f7-49d7-9aa4-6157748e4854","elementType":"PARAGRAPH","contents":"<font size=\"1\"><font face=\"Roboto\" color=\"#ffffff\">PRIVACY</font><font color=\"#fdfefe\" face=\"Roboto\"> POLICY</font></font>","fontSize":16,"lineHeight":16,"marginTop":11}]},{"id":"abde7248-b63f-4199-ab37-fc7f115db278","elements":[{"id":"e389db2f-fce9-44fe-854b-1262f3fd5312","elementType":"PARAGRAPH","contents":"<font color=\"#ffffff\" face=\"Roboto\" size=\"1\">BLOG</font>","fontSize":16,"lineHeight":16,"marginTop":11}]},{"id":"13432ee3-1b24-4401-8b06-4e0fe319ef10","elements":[{"id":"f62b3c95-6330-4607-90fc-2ab3925925f5","elementType":"PARAGRAPH","contents":"<font color=\"#ffffff\" face=\"Roboto\" size=\"1\">PRICING</font>","fontSize":16,"lineHeight":16,"marginTop":11}]},{"id":"b3109dbb-716b-44fd-a52b-8ad5a806354e","elements":[{"id":"9e07ab25-4cb5-4ef8-b38a-6b1d45f1b529","elementType":"PARAGRAPH","contents":"<font color=\"#ffffff\" face=\"Roboto\" size=\"1\">INTEGRTIONS</font>","fontSize":16,"lineHeight":16,"marginTop":11}]}]},{"id":"37c5d64e-98b2-4c94-8a4b-bb820a2eaa52","width":33.598130841121495,"wrappers":[{"id":"7bbd2f78-2975-4389-bf71-2c0cd93a1a39","elements":[{"id":"159c7cbf-9d51-4c43-9b56-ef2f9ab9c8fd","elementType":"PARAGRAPH","contents":"<font face=\"Roboto\" color=\"#fdfefe\">Follow us</font>","fontSize":16,"lineHeight":16,"marginBottom":17}]},{"id":"e6f9b6e6-9116-43d1-a919-85dac42342f7","elements":[{"id":"1c183e9c-c7ac-49f5-80b7-9799707351c1","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/1895297932.png","width":41,"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0,"phoneStyles":{"paddingRight":0,"paddingLeft":0}},{"id":"68f0eef9-cf37-4963-a1c1-e3f1791415da","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/1550516160.png","width":41,"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0},{"id":"0019a84a-6e79-4502-b34d-b10ceab42725","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/1565967859.png","width":41,"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0},{"id":"574a81a0-fec9-442f-9a4c-d09338dcc83e","elementType":"IMAGE","path":"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/2036287974.png","width":42,"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0},{"id":"ab32898e-e6b1-43ed-b9ab-a3059831655b","elementType":"PARAGRAPH","contents":"","fontSize":16,"lineHeight":16},{"id":"b5c58572-3075-4a70-9443-bb4f89cc800a","elementType":"PARAGRAPH","contents":"","fontSize":16,"lineHeight":16}]}]}]},{"id":"93c06a33-390e-44d7-bde9-8c11a35b881f","columns":[{"id":"b7ed8ca2-d90f-4bfa-82e4-4d1cf1aac123","width":100,"wrappers":[{"id":"12534d1a-25b8-4777-b38f-ecc0c4fd3a5e","elements":[{"id":"56452568-83fd-4afa-9e93-b3b2057ed262","elementType":"LINE","contents":"Sample Content","fontSize":16,"lineHeight":16,"color":"#b2b2b2","paddingTop":48,"paddingBottom":24,"styles":{"paddingTop":50,"paddingBottom":25},"style":"solid","size":1}]},{"id":"cefefcbd-8c00-4509-9658-58e31c3f0a3d","elements":[{"id":"79eac802-052d-4ef2-b444-08095ec5f939","elementType":"PARAGRAPH","contents":"<p style=\"text-align:center;\"><span style=\"color: #ffffff;font-size: 12px;font-family: Roboto;\">Copyright © 2013-2019 Promo Company.All rights reserved.</span></p>\n","fontSize":16,"lineHeight":16,"textAlign":"center"}]}]}]}]}],"meta":{"title":"","keywords":"","description":""},"tracking":{"head":"<script>\n  window.intercomSettings = {\n    app_id: 'wlpk1rxu',\n    };\n</script>\n<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic===\"function\"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/' + APP_ID;var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>"}}

  const saveTemplate = () => {
    setFullWidthLoader(true);
    setSaveLoader(true);
    const formattedData = denormalizeData(layers);
    if (editortype === "funnels") {
      // console.log(JSON.stringify(formattedData));
      //     setFullWidthLoader(false);
      //     setSaveLoader(false);
      const updateTemplate = {
        funnel_card_id: localStorage.getItem("templateId"),
        json: JSON.stringify(formattedData)
        // json: JSON.stringify(customTemplateHotCode)
      };
      PostData("ms3", "savejson", updateTemplate).then(result => {
        if (result !== "Invalid") {
          toast.info(result.message);
          setFullWidthLoader(false);
          setSaveLoader(false);
          if (result.status === "success") {
            return dispatch({
              type: actions.SAVE_STATUS,
              status: true
            });
          }
        }
      });
    } else if (editortype === "landingpage") {
      const updateTemplate = {
        landing_page_id: localStorage.getItem("templateId"),
        json: JSON.stringify(formattedData)
      };
      const url = "landingpagesavejson";
      PostData("ms4", url, updateTemplate).then(result => {
        if (result !== "Invalid") {
          toast.info(result.message);
          setFullWidthLoader(false);
          setSaveLoader(false);
          if (result.status === "success") {
            return dispatch({
              type: actions.SAVE_STATUS,
              status: true
            });
          }
        }
      });
    } else {
      toast.warn("Somthing went wrong!");
    }
  };
  const pageData = () => {
    if (editortype === "funnels") {
      if (selectedFunnelStepData !== null) {
        setPublishButton(true);
      }
    } else if (editortype === "landingpage") {
      const Id = {
        landing_page_master_id: localStorage.getItem("landing_page_master_id")
      };
      PostData("ms4", "landingpagedata", Id).then(result => {
        if (result !== "Invalid") {
          if (result.status === "success") {
            setRepublishStatus(result.republish_status);
            setPublishButton(true);
          }
        } else {
          alert("Template Data Not Found!");
        }
      });
    }
  };
  useEffect(() => {
    pageData();
    setPublishButton(true);

  }, [publishButton]);
  const publish = () => {
    // saveTemplate();
    setPublishLoader(true);
    if (editortype === "funnels") {
      if (selectedFunnelStepData.republish_status) {
        const funnelStepId = { funnel_step_id: selectedFunnelStep };
        PostData("ms3", "funnelpublish", funnelStepId).then(data => {
          if (data !== "Invalid") {
            toast.info(data.message);
            setPublishLoader(false);
          }
        });
      } else {
        const apiBody = {
          funnel_step_id: selectedFunnelStep,
          publish_status: true,
          publish_home_status:selectedFunnelStepData.publish_home_status
        };
        PostData("ms3", "publishtoggle", apiBody).then(data => {
          if (data !== "Invalid") {
            toast.info(data.message);
            setPublishLoader(false);
          }
        });
      }
    } else if (editortype === "landingpage") {
      if (republishStatus) {
        const apiBody = {
          // landing_page_id: templateId
          landing_page_master_id: localStorage.getItem("landing_page_master_id")
        };
        PostData("ms4", "landingpagerepublish", apiBody).then(data => {
          if (data !== "Invalid") {
            setPublishLoader(false);
            toast.info(data.message);
            pageData();
          }
        });
      } else {
        const apiBody = {
          // landing_page_id: templateId,
          landing_page_master_id: localStorage.getItem(
            "landing_page_master_id"
          ),
          publish_status: true
        };
        PostData("ms4", "landingpagepublishtoggle", apiBody).then(data => {
          if (data !== "Invalid") {
            setPublishLoader(false);
            toast.info(data.message);
            pageData();
          }
        });
      }
    }
  }
  
  const handleDeviceType = (thisType) => {
    return dispatch({
      type: actions.SET_DEVICE_TYPE,
      deviceType: thisType
    });
  }

  return (
    <div className="header-actions">
      <div className="header-actions-devices" style={{display: "flex"}}>
        <Button
          className={classNames("header-button", state.deviceType === "phone" && "active")}
          onClick={() => handleDeviceType("phone")}
          iconOnly
        >
          <i className="material-icons">phone_iphone</i>
        </Button>
        <Button
          className={classNames("header-button", state.deviceType === "tablet" && "active")}
          onClick={() => handleDeviceType("tablet")}
          iconOnly
        >
          <i className="material-icons">tablet_mac</i>
        </Button>
        <Button
          className={classNames("header-button", state.deviceType === "desktop" && "active")}
          onClick={() => handleDeviceType("desktop")}
          // disabled={!canUndo}
          iconOnly
        >
          <i className="material-icons">tv</i>
        </Button>  
      </div>
      <Button
        className="header-button"
        onClick={undo}
        disabled={!canUndo}
        iconOnly
      >
        <i className="pro-icons undo">undo</i>
      </Button>
      <Button
        className="header-button"
        onClick={redo}
        disabled={!canRedo}
        iconOnly
      >
        <i className="pro-icons redo">redo</i>
      </Button>
      {/* <Button className="header-button" iconOnly> */}
        <a href="https://support.roundclicks.com/en/articles/3806990-editor-guides" target="_blank" className="header-button help-icon">
          <i className="pro-icons lifebuoy">help</i>
        </a>
      {/* </Button> */}
      {/* <Button
        className="header-button"
        outline
        onClick={() => history.push("/preview")}
      >
        <i className="pro-icons eye">visibility</i> Preview
      </Button> */}
      {publishButton && (
        <React.Fragment>
          {publishLoader ? (
            <Button className="header-button" primary disabled>
              <span style={{ marginRight: "7px" }}>
                <ClipLoader
                  sizeUnit={"px"}
                  size={10}
                  color={"#123abc"}
                  loading
                />
              </span>
              loading...
            </Button>
          ) : (
            <Button
              className="header-button"
              primary
              onClick={() => publish()}
              disabled={!state.isSaved}
            >
              <i className="material-icons">language</i>Publish
            </Button>
          )}
        </React.Fragment>
      )}
      <Link
        to={{
          pathname: "/preview",
          state: { templateDetails: "templateDetails" }
        }}
        target="_blank"
        className="header-button preview"
      >
        {/* target="_blank" */}
        <i className="pro-icons eye">visibility</i> Preview
      </Link>
      {saveLoader ? (
        <Button className="header-button" primary disabled>
          <span style={{ marginRight: "7px" }}>
            <ClipLoader sizeUnit={"px"} size={10} color={"#123abc"} loading />
          </span>
          loading...
        </Button>
      ) : (
        <Button
          className="header-button"
          primary
          onClick={() => saveTemplate()}
        >
          {/* <i className="material-icons">save</i> */}
          Save
        </Button>
      )}
    </div>
  );
};

const HeaderLinks = () => {
  const { funnelStepData, setFullWidthLoader } = useContext(Context);
  const {
    state,
    history: {
      state: { layers }
    },
    history: { state: stateHistory }
  } = useContext(Store);
  const [savePopup, setSavePopup] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const cardName = _get(funnelStepData, "funneldata[0].funnel_card_name", "");
  const editortype = localStorage.getItem("editortype");
  const popupToggle = () => {
    if (state.isSaved) {
      setRedirect(true);
    } else {
      setSavePopup(!savePopup);
    }
  };
  const popupClose = () => {
    setSavePopup(false);
    setRedirect(true);
  };
  const saveTemplate = () => {
    setSavePopup(false);
    setFullWidthLoader(true);
    // const formattedData = denormalizeData(layers);
    // const updateTemplate = {
    //   funnel_card_id: localStorage.getItem("templateId"),
    //   json: JSON.stringify(formattedData)
    // }
    // PostData('ms4', 'savejson', updateTemplate).then((result) => {
    //   if(result !== 'Invalid') {
    //     toast.info(result.message);
    //     setFullWidthLoader(false);
    //     setRedirect(true);
    //   }
    // });
    const formattedData = denormalizeData(layers);
    const editortype = localStorage.getItem("editortype");
    if (editortype === "funnels") {
      const updateTemplate = {
        funnel_card_id: localStorage.getItem("templateId"),
        json: JSON.stringify(formattedData)
      };
      PostData("ms3", "savejson", updateTemplate).then(result => {
        if (result !== "Invalid") {
          toast.info(result.message);
          setFullWidthLoader(false);
          setRedirect(true);
        }
      });
    } else if (editortype === "landingpage") {
      const updateTemplate = {
        landing_page_id: localStorage.getItem("templateId"),
        json: JSON.stringify(formattedData)
      };
      const url = "landingpagesavejson";
      PostData("ms4", url, updateTemplate).then(result => {
        if (result !== "Invalid") {
          toast.info(result.message);
          setFullWidthLoader(false);
          setRedirect(true);
        }
      });
    } else {
      toast.warn("Somthing went wrong!");
    }
  };
  return redirect ? (
    editortype === "funnels" ? (
      <Redirect to="/funnels/overview" />
    ) : (
      <Redirect to="/landingpages/landingpageoverview" />
    )
  ) : (
    <div className="header-links">
      <div className="logo">
        <Link to="/dashboard">
          <img src={logo} alt="logo" />
        </Link>
      </div>
      <h3 className="title">
        <span onClick={popupToggle}>
          <i className="material-icons">keyboard_backspace</i>&nbsp;&nbsp;Back to
          overview
          {/* {_get(props.templateDetails, 'name', '')} */}
        </span>
      </h3>
      <Popup
        show={savePopup}
        size="md"
        onHide={popupClose}
        onHandle={saveTemplate}
        onHandleText="SAVE"
        heading="Save"
      >
        <p>Save Your Template</p>
      </Popup>
      <div className="book-an-appoinment-editor">
        <p>
          Can I show you how to use RoundClicks?
          <a
            href="https://calendly.com/roundclicks-support/account-demo"
            target="_blank"
            className="book ml-20"
          >
            Book a free Demo
          </a>
        </p>
      </div>
    </div>     
  );
};

const Header = () => {
  return (
    <header className="app-header">
      <HeaderLinks />
      <HeaderActions />
    </header>
  );
};

export default Header;

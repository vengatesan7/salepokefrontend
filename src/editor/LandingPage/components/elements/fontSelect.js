import React from 'react';

const FontSelect = ({ value, onChange }) => {
    const fontFamilies = [
        "Roboto", "Open Sans", "Ubuntu", "Nunito Sans",'Noto Serif',"Noto Sans JP","Noto Sans", "Lato","Roboto Slab","Montserrat", "Anton","Source Sans Pro","Roboto Condensed","Oswald", "Josefin Sans", "Tajawal", "Raleway", "Oxygen", "Maven Pro", "Catamaran", "Orbitron", "Darker Grotesque", "Montserrat Alternates", "Enemy"
    ]
    return (
        <select value={value} onChange={(e) => onChange(e.target.value)}>
            <option key="" value="">Select Font Family</option>
            {fontFamilies.map(fontName => {
                return <option key={fontName.replace(/\s/g, "")} value={fontName}>{fontName}</option>
            })}
        </select>
    );
}

export default FontSelect;
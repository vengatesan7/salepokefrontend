import React, { useState, useEffect } from "react";

import './spinButtonInput.scss';

const SpinInputButton = ({
  step,
  value,
  max,
  min,
  callBack,
  inputBoxDisabled
}) => {
  const [thisValue, setValue] = useState(value);
  const [isMin, setIsMin] = useState(min || 0);
  const [isMax, setIsMax] = useState(max || null);
  const handleValue = type => {
    const stepValue = step || 1;
    if (type === "increment" && thisValue < max) {
      const incresed = thisValue + stepValue;
      incresed === max ? setIsMax(true) : setIsMax(false);
      incresed === min ? setIsMin(true) : setIsMin(false);
      setValue(incresed);
      callBack(incresed);
    } else if (type === "decrement" && thisValue > min) {
      const decresed = thisValue - stepValue;
      decresed === max ? setIsMax(true) : setIsMax(false);
      decresed === min ? setIsMin(true) : setIsMin(false);
      setValue(decresed);
      callBack(decresed);
    }
  };
  useEffect(() => {
    if (min || min === 0) {
      thisValue <= min ? setIsMin(true) : setIsMin(false);
      thisValue < min && setValue(min);
    } 
    if (max) {
      thisValue >= max ? setIsMax(true) : setIsMax(false);
      thisValue > max && setValue(max);
    }
  }, [thisValue]);
  useEffect(() => {
    setValue(value);
  }, [value]);
  const handleChangesInput = event => {
    let newValue = event.target.valueAsNumber;
    if (newValue <= max && newValue >= min) {
      newValue === max ? setIsMax(true) : setIsMax(false);
      newValue === min ? setIsMin(true) : setIsMin(false);
      setValue(newValue);
      callBack(newValue);
    }
  };
  return (
    <div className="spin-button">
      <button
        className='spin-di'
        onClick={() => handleValue("decrement")}
        disabled={isMin}
      >
        -
      </button>
      <input
        type="number"
        step={step}
        value={value}
        min={min}
        max={max}
        disabled={inputBoxDisabled}
        onChange={e => handleChangesInput(e)}
      />
      <button
        className='spin-in'
        onClick={() => handleValue("increment")}
        disabled={isMax}
      >
        +
      </button>
    </div>
  );
};

export default SpinInputButton;
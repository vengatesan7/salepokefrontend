import React from "react";
import classNames from "classnames";
import "./button.scss";

const Button = ({
  children,
  onClick,
  className,
  outline,
  primary,
  iconOnly,
  disabled,
}) => {
  return (
    <button
      onClick={onClick}
      className={classNames(
        "button",
        { "button-primary": primary },
        { "button-outline": outline },
        { "button-icon-only": iconOnly },
        className
      )}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;

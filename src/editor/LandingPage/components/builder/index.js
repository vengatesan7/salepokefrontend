import React, { useContext, useEffect, useState } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import uuid from "uuidv4";
import { normalize } from "normalizr";
import classnames from "classnames";

import Canvas from "./canvas";
import MenuBar from "./menubar";
import { Store } from "../../store/index";
import actions from "../../store/action-types";
import layoutsData from "../../store/layouts-data2";
import layersSchema from "./schema";
import {
  reorder,
  handleDropLeft,
  handleDropRight,
  handleDropTop,
  handleDropBottom,
  handleAddNew
} from "../../utils/dnd-utils";
import {
  newWrapperInDND,
  samWrapperInDND,
  firstElementDND,
  oldElementInsamWrapperInDND,
  oldElementInNewWrapperInDND
} from "../../utils/dnd";
import FormBuilder from "./form-builder";
import AdvanceFormBuilder from "../../../Form";
import Style from "./stylebar";
import "./builder.scss";
import { PostData } from "../../../../services/PostData";
import { Context } from "../../../../Context";

import { TOOL_ITEMS } from "./menubar/constants";

const getNormalizedData = (data, schema) => normalize(data, schema);

const Builder = () => {
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory, set }
  } = useContext(Store);
  const { setFullWidthLoader } = useContext(Context);
  const [droppedZone, setDroppedZone] = useState("");
  const [dragging, setDragging] = useState(false);
  const [selectedRow, setSelectedRow] = useState("");

  const [templateData, setTemplateData] = useState(null);

  const customDragEnd = e => {
    const {
      className,
      dataset: { rowid: rowId }
    } = e.target;
    setDroppedZone(className);
    setSelectedRow(rowId);
  };

  const getLayouts = data => {
    return dispatch({
      type: actions.GET_LAYOUTS,
      payload: getNormalizedData(data, layersSchema)
    });
  };

  const updateLayers = data => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  };

  const onDragStart = result => {
    if (result.type === "element") {
      setDragging(true);
      return dispatch({
        type: actions.ENABLE_DROP,
        status: true
      })
    }
  };

  const onDragEnd = result => {
    const { destination, source, draggableId, type } = result;
    dispatch({
      type: actions.DROP_POSITION,
      position: null,
      wrapperId: null,
      columnId: null
    });
    dispatch({
      type: actions.DROP_SAME_WRAPPER_POSITION,
      position: null,
      elementId: null,
      wrapperId: null
    });
    dispatch({
      type: actions.FIRST_ELEMENT_DROP,
      position: null,
      columnId: null
    });
    dispatch({
      type: actions.ELEMENT_TAKEN_FROM,
      columnId: null,
      wrapperId: null
    });
    switch (type) {
      case "element":
        setDragging(false);
        dispatch({
          type: actions.ENABLE_DROP,
          status: false
        })
        if (source.droppableId === "ITEMS") {
          switch (state.dropPosition) {
            case "top":
              const newTopUpdatedLayers = newWrapperInDND(stateHistory.layers, state, "top", draggableId)
              return updateLayers(newTopUpdatedLayers);
            case "bottom":
              const newBottomUpdatedLayers = newWrapperInDND(stateHistory.layers, state, "bottom", draggableId);
              return updateLayers(newBottomUpdatedLayers);
            case "left":
              const newLeftupdatedLayers = samWrapperInDND(stateHistory.layers, state, "left", draggableId);
              return updateLayers(newLeftupdatedLayers);
            case "right":
              const newRightUpdatedLayers = samWrapperInDND(stateHistory.layers, state, "right", draggableId);
              return updateLayers(newRightUpdatedLayers);
            case "first":
              const newFirstUpdatedLayers = firstElementDND(stateHistory.layers, state, "new", result);
              return updateLayers(newFirstUpdatedLayers);
            default:
              break;
          }
        } else {
          if (state.dropWrapperID === state.elementTakenWrapper) {
            const leftRight = () => {
              const newOrder = Array.from(layers.entities.wrappers[state.dropWrapperID].elements);
              const revPosition = newOrder.indexOf(draggableId);
              const newPosition = newOrder.indexOf(state.dropElementID);
              newOrder.splice(revPosition, 1);
              newOrder.splice(state.dropPosition === "left" ? newPosition : newPosition + 1, 0, draggableId)
              const sameWrapperRerderLeft = {
                ...layers,
                entities: {
                  ...layers.entities,
                  wrappers: {
                    ...layers.entities.wrappers,
                    [state.dropWrapperID]: {
                      ...layers.entities.wrappers[state.dropWrapperID],
                      elements: newOrder
                    }
                  }
                }
              }
              return updateLayers(sameWrapperRerderLeft);
            }
            switch (state.dropPosition) {
              case "left":
                return leftRight();
              case "right":
                return leftRight();
              case "top":
                const topUpdatedLayers = oldElementInNewWrapperInDND(stateHistory.layers, state, "top", result);
                return updateLayers(topUpdatedLayers);
              case "bottom":
                const bottomUpdatedLayers = oldElementInNewWrapperInDND(stateHistory.layers, state, "bottom", result);
                return updateLayers(bottomUpdatedLayers);
              default:
                break;
            }
          } else {
            switch (state.dropPosition) {
              case "top":
                const topUpdatedLayers = oldElementInNewWrapperInDND(stateHistory.layers, state, "top", result);
                return updateLayers(topUpdatedLayers);
              case "bottom":
                const bottomUpdatedLayers = oldElementInNewWrapperInDND(stateHistory.layers, state, "bottom", result);
                return updateLayers(bottomUpdatedLayers);
              case "left":
                const leftUpdatedLayers = oldElementInsamWrapperInDND(stateHistory.layers, state, "left", result);
                return updateLayers(leftUpdatedLayers);
              case "right":
                const rightUpdatedLayers = oldElementInsamWrapperInDND(stateHistory.layers, state, "right", result);
                return updateLayers(rightUpdatedLayers);
              case "first":
                const firstUpdatedLayers = firstElementDND(stateHistory.layers, state, "old", result);
                return updateLayers(firstUpdatedLayers);
              default:
                break;
            }
          }
        }
        return;
      case "wrapper":
        if (source.droppableId === destination.droppableId) {
          const newWrapperOrder = Array.from(layers.entities.columns[destination.droppableId].wrappers);
          newWrapperOrder.splice(source.index, 1);
          newWrapperOrder.splice(destination.index, 0, draggableId);
          const newData = {
            ...layers,
            entities: {
              ...layers.entities,
              columns: {
                ...layers.entities.columns,
                [destination.droppableId]: {
                  ...layers.entities.columns[destination.droppableId],
                  wrappers: newWrapperOrder
                }
              }
            }
          }
          return updateLayers(newData);
        } else {
          const sourceOrder = Array.from(layers.entities.columns[source.droppableId].wrappers);
          const destinationOrder = Array.from(layers.entities.columns[destination.droppableId].wrappers);
          sourceOrder.splice(source.index, 1);
          destinationOrder.splice(destination.index, 0, draggableId);
          const newData = {
            ...layers,
            entities: {
              ...layers.entities,
              columns: {
                ...layers.entities.columns,
                [source.droppableId]: {
                  ...layers.entities.columns[source.droppableId],
                  wrappers: sourceOrder
                },
                [destination.droppableId]: {
                  ...layers.entities.columns[destination.droppableId],
                  wrappers: destinationOrder
                }
              }
            }
          }
          return updateLayers(newData);
        }
      default:
        break;
    }
    if (!destination) {
      return;
    }
    if (type === "rowLayouts") {
      if (source.droppableId === destination.droppableId) {
        const newRowLayoutArray = [...layers.entities.section[destination.droppableId].rows];
        newRowLayoutArray.splice(source.index, 1);
        newRowLayoutArray.splice(destination.index, 0, draggableId);
        const newRowLayoutData = {
          ...layers,
          entities: {
            ...layers.entities,
            section: {
              ...layers.entities.section,
              [destination.droppableId] : {
                ...layers.entities.section[destination.droppableId],
                rows: newRowLayoutArray
              }
            }
          }
        }
        return updateLayers(newRowLayoutData);
      } else {
        const oldRowLayoutArray = [...layers.entities.section[source.droppableId].rows];
        const newRowLayoutArray = [...layers.entities.section[destination.droppableId].rows];
        oldRowLayoutArray.splice(source.index, 1);
        newRowLayoutArray.splice(destination.index, 0, draggableId);
        const newRowLayoutData = {
          ...layers,
          entities: {
            ...layers.entities,
            section: {
              ...layers.entities.section,
              [source.droppableId] : {
                ...layers.entities.section[source.droppableId],
                rows: oldRowLayoutArray
              },
              [destination.droppableId] : {
                ...layers.entities.section[destination.droppableId],
                rows: newRowLayoutArray
              }
            }
          }
        }
        return updateLayers(newRowLayoutData);
      }
    }
    // if (type === "formbuilder") {
    //   switch (source.droppableId) {
    //     case "FORMBUILDER":
    //       console.log('worl');
    //       return;
    //     case "FORMBUILDERCANVAS":
    //       console.log('soco');
    //       return;
    //     default:
    //       break;
    //   }
    // }
    if (type === "sectionLayouts") {
      const newSectionOrder = Array.from(layers.result.sections);
      newSectionOrder.splice(source.index, 1);
      newSectionOrder.splice(destination.index, 0, draggableId);
      const newData = {
        ...layers,
        result: {
          ...layers.result,
          sections: newSectionOrder
        }
      };
      updateLayers(newData);
      return;
    }
    if (type === "element") {
      // dropped on center
      if (droppedZone === "drop-area") {
        if (source.droppableId === "ITEMS") {
          const updatedLayers = handleAddNew(
            layers,
            draggableId,
            source,
            destination,
            "top"
          );
          return updateLayers(updatedLayers);
        } else if (destination.droppableId !== source.droppableId) {
          // when placing on other columns
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              column: {
                ...layers.entities.column,
                [source.droppableId]: {
                  ...layers.entities.column[source.droppableId],
                  elements: layers.entities.column[
                    source.droppableId
                  ].elements.filter(el => el !== draggableId)
                },
                [destination.droppableId]: {
                  ...layers.entities.column[destination.droppableId],
                  elements: [
                    ...layers.entities.column[
                      destination.droppableId
                    ].elements.slice(0, destination.index),
                    draggableId,
                    ...layers.entities.column[
                      destination.droppableId
                    ].elements.slice(destination.index)
                  ]
                }
              }
            }
          };
          return updateLayers(data);
        } else if (destination.droppableId === source.droppableId) {
          // when placing on same columns
          const updatedElements = reorder(
            layers.entities.column[destination.droppableId].elements,
            source.index,
            destination.index
          );
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              column: {
                ...layers.entities.column,
                [destination.droppableId]: {
                  ...layers.entities.column[destination.droppableId],
                  elements: updatedElements
                }
              }
            }
          };
          return updateLayers(data);
        }
      } else if (droppedZone === "left") {
        const updatedLayers = handleDropLeft(
          layers,
          draggableId,
          source,
          destination
        );
        return updateLayers(updatedLayers);
      } else if (droppedZone === "right") {
        const updatedLayers = handleDropRight(
          layers,
          draggableId,
          source,
          destination
        );
        return updateLayers(updatedLayers);
      } else if (droppedZone === "top") {
        const updatedLayers = handleDropTop(
          layers,
          draggableId,
          source.droppableId,
          selectedRow
        );
        return updateLayers(updatedLayers);
      } else if (droppedZone === "bottom") {
        const updatedLayers = handleDropBottom(
          layers,
          draggableId,
          source.droppableId,
          selectedRow
        );
        updateLayers(updatedLayers);
      }
    }
  };

  useEffect(() => {
    setFullWidthLoader(true);
    const editortype = localStorage.getItem("editortype");
    if (editortype === "funnels") {
      const updateTemplate = {
        funnel_card_id: localStorage.getItem("templateId")
      }
      PostData('ms3', 'retrievejson', updateTemplate).then((result) => {
        if (result !== 'Invalid') {
          if (result.status === "success") {
            console.log(result.json);
            dispatch({
              type: actions.UPDATE_TEMPLATE_CATEGORY_ID,
              categoryId: result.page_tmpl_category_id
            });
            setTemplateData(JSON.parse(result.json));
            setFullWidthLoader(false);
          }
        }
      });
    } else if (editortype === "landingpage") {
      const updateTemplate = {
        landing_page_id: localStorage.getItem("templateId")
      }
      PostData('ms4', 'landingpageretreivejson', updateTemplate).then((result) => {
        if (result !== 'Invalid') {
          if (result.status === "success") {
            console.log(result.json);
            setTemplateData(JSON.parse(result.json));
            setFullWidthLoader(false);
          }
        }
      });
    } else {
      getLayouts(layoutsData);
    }
  }, []);

  useEffect(() => {
    templateData !== null && getLayouts(templateData);
  }, [templateData]);

  useEffect(() => {
    return set({ ...stateHistory, layers });
  }, [layers]);

  useEffect(() => {
    window.addEventListener("mouseup", customDragEnd, false);
    return () => {
      window.removeEventListener("mouseup", customDragEnd);
    };
  }, []);
  return (
    <React.Fragment>
      <DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
        <div className={classnames("builder", { "drag-active": dragging })}>
          <div className="builder-main">
            <MenuBar />
            {state.isStyleActive && <Style />}
            <Canvas />
          </div>
        </div>
      </DragDropContext>
      {state.isFormBuilderActive && <FormBuilder />}
      {state.isFormAdvanceBuilderActive && <AdvanceFormBuilder template={state.advanceFormData} />}
      
    </React.Fragment>
  );
};

export default Builder;

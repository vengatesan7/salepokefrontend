import React, { useContext, useState, useEffect } from 'react';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import uuid from 'uuid/v4';

import actions from "../../../store/action-types";
import { Store } from '../../../store';
import './form-builder.scss';

const overAllFields = ['Text', 'Checkbox', 'Radio Button', 'Select Box', 'Textarea', 'File Upload'];

const FormBuilder = () => {
    const { state, dispatch } = useContext(Store);
    const [selectedField, setSelectedField] = useState(null);
    const [formData, setFormData] = useState(state);
    const formId = formData.layers.entities.element[formData.activeElement].formData[0];
    
    const closeFormDesignPopup = () => {
        return dispatch({
            type: actions.FORM_POPUP
        })
    }
    const selectField = (id) => {
        setSelectedField(id);
    }
    const updateForm = (newData) => {
        setFormData(newData);
        return dispatch({
            type: actions.UPDATE_FORM,
            newData: newData
        })
    }
    const handleFileTypeArray = (oldArray, name, value) => {
        if (value) {
            const alreadyThere = oldArray.some(function(arrVal) { return value === arrVal; });
            !alreadyThere && oldArray.push(name);
        } else {
            const position = oldArray.indexOf(name)
            oldArray.splice(position, 1);
        }
        const newListArray = oldArray;
        return handleInputchange(newListArray, 'fileTypes')
    }
    const handleInputchange = (value, type) => {
        const newUpdate = {...state,
            layers: {
                ...state.layers,
                entities: {
                    ...state.layers.entities,
                    field: {
                        ...state.layers.entities.field,
                        [selectedField]: {
                            ...state.layers.entities.field[selectedField],
                            [type]: value
                        }
                    }
                }
            }
        }
        return updateForm(newUpdate);
    }
    const formBuilderOnDragEnd = result => {
        const { draggableId, source, destination } = result;
        if (!destination) {
            return;
        }
        if (source.droppableId === destination.droppableId) {
            const newFieldOrder = Array.from(formData.layers.entities.formData[formId].fields);
            newFieldOrder.splice(source.index, 1);
            newFieldOrder.splice(destination.index, 0, draggableId);
            const newUpdate = {...state,
                layers: {
                    ...state.layers,
                    entities: {
                        ...state.layers.entities,
                        formData: {
                            ...state.layers.entities.formData,
                            [formId]: {
                                ...state.layers.entities.formData[formId],
                                fields: newFieldOrder
                            }
                        }
                    }
                }
            }
            return updateForm(newUpdate);
        }
        // if(source.droppableId === "FORMBUILDER") {
        //     const newFieldId = uuid();
        //     const newField = {
        //         id: newFieldId,
        //         type: "TEXT",
        //         label: "text",
        //         placeholder: "text"
        //     }
        //     let newOrder = state.layers.entities.formData[formId].fields;
        //     newOrder.push(newFieldId);
        //     const newUpdate = {...state,
        //         layers: {
        //             ...state.layers,
        //             entities: {
        //                 ...state.layers.entities,
        //                 formData: {
        //                     ...state.layers.entities.formData,
        //                     [formId]: {
        //                         ...state.layers.entities.formData[formId],
        //                         fields: newOrder
        //                     }
        //                 },
        //                 field: {
        //                     ...state.layers.entities.field,
        //                     [newFieldId]: newField
        //                 }
        //             }
        //         }
        //     }
        //     return updateForm(newUpdate);
        // } 
    }

    const updateField = (newData) => {
        let newOrder = state.layers.entities.formData[formId].fields;
        newOrder.push(newData.id);
        const newUpdate = {...state,
            layers: {
                ...state.layers,
                entities: {
                    ...state.layers.entities,
                    formData: {
                        ...state.layers.entities.formData,
                        [formId]: {
                            ...state.layers.entities.formData[formId],
                            fields: newOrder
                        }
                    },
                    field: {
                        ...state.layers.entities.field,
                        [newData.id]: newData
                    }
                }
            }
        }
        return updateForm(newUpdate);
    }

    const addNewField = (type) => {
        const newFieldId = uuid();
        switch (type) {
            case 'Text':
                const newText = {
                    id: newFieldId,
                    type: "text",
                    label: "Text",
                    placeholder: "Text",
                    required: false
                }
                return updateField(newText);
            case "Checkbox":
                const newCheckbox = {
                    id: newFieldId,
                    type: "checkbox",
                    label: "Choice",
                    options: ["Choice 1", "Choice 2"]
                }
                return updateField(newCheckbox);
            case "Radio Button":
                const newRadioButton = {
                    id: newFieldId,
                    type: "radio",
                    label: "Choice",
                    options: ["Choice 1", "Choice 2"]
                }
                return updateField(newRadioButton);
            case "Select Box":
                const newSelectBox = {
                    id: newFieldId,
                    type: "select",
                    label: "Select",
                    options: ["Option 1", "Option 2"]
                }
                return updateField(newSelectBox);
            case "Textarea":
                const newTextarea = {
                    id: newFieldId,
                    type: "textarea",
                    label: "Textarea",
                    placeholder: "Textarea",
                    required: false
                }
                return updateField(newTextarea);
            case "File Upload":
                const newFileUpload = {
                    id: newFieldId,
                    type: 'file',
                    label: 'File Upload',
                    fileTypes:["pdf","doc","docx"],
	                multiFile: false
                }
                return updateField(newFileUpload);
            default:
                break;
        }
    }
    const deleteField = (id) => {
        let newFieldArr = state.layers.entities.formData[formId].fields
        const position = newFieldArr.indexOf(id);
        newFieldArr.splice(position, 1);
        setSelectedField(null);
        const newUpdate = {...state,
            layers: {
                ...state.layers,
                entities: {
                    ...state.layers.entities,
                    formData: {
                        ...state.layers.entities.formData,
                        [formId]: {
                            ...state.layers.entities.formData[formId],
                            fields: newFieldArr
                        }
                    }
                }
            }
        }
        return updateForm(newUpdate);
    }
    const updateOption = (type, index, value) => {
        let newOptionArr = state.layers.entities.field[selectedField].options;
        const updatedOptionsArray = (newArr) => {
            const newUpdate = {...state,
                layers: {
                    ...state.layers,
                    entities: {
                        ...state.layers.entities,
                        field: {
                            ...state.layers.entities.field,
                            [selectedField]: {
                                ...state.layers.entities.field[selectedField],
                                options: newArr
                            }
                        }
                    }
                }
            }
            return updateForm(newUpdate);
        }
        switch (type) {
            case 'edit':
                newOptionArr.splice(index, 1, value);
                return updatedOptionsArray(newOptionArr);
            case 'remove':
                newOptionArr.splice(index, 1);
                return updatedOptionsArray(newOptionArr);
            case 'add':
                // let arrlength = newOptionArr.length + 1;
                // let defaultValue = "Option" + arrlength ;
                newOptionArr.splice(index + 1, 0, "");
                return updatedOptionsArray(newOptionArr);
            default:
                break;
        }
    }
    const fieldContent = () => {
        if (selectedField !== null) {
            const selectedFieldData = formData.layers.entities.field[selectedField];
            if (selectedFieldData.type === 'text' || 
                selectedFieldData.type === 'email' ||
                selectedFieldData.type === 'number') {
                return (
                    <React.Fragment>
                        <div>
                            <label>Placeholder</label>
                            <input value={selectedFieldData.placeholder} onChange={(e) => handleInputchange(e.target.value, 'placeholder')} />
                        </div>
                        <div>
                            <label>Type</label>
                            <select value={selectedFieldData.type} onChange={(e) => handleInputchange(e.target.value, 'type')} >
                                <option value='text'>Text</option>
                                <option value='email'>Email</option>
                                <option value='number'>Number</option>
                            </select>
                        </div>
                    </React.Fragment>
                )
            } else if (selectedFieldData.type === 'textarea') {
                return (
                    <React.Fragment>
                        <div>
                            <label>Placeholder</label>
                            <input value={selectedFieldData.placeholder} onChange={(e) => handleInputchange(e.target.value, 'placeholder')} />
                        </div>
                    </React.Fragment>
                )
            } else if (selectedFieldData.type === 'file') {
                const pdf = selectedFieldData.fileTypes.some(function(arrVal) { return 'pdf' === arrVal; });
                const doc = selectedFieldData.fileTypes.some(function(arrVal) { return 'doc' === arrVal; });
                const docx = selectedFieldData.fileTypes.some(function(arrVal) { return 'docx' === arrVal; });
                return (
                    <React.Fragment>
                        <div>
                            <label>File Type</label>
                            <div>
                                <input
                                    style={{margin: '0', verticalAlign: 'middle'}}
                                    type="checkbox"
                                    name="pdf"
                                    value="pdf"
                                    id="pdf"
                                    checked={pdf}
                                    onChange={(e) => handleFileTypeArray(selectedFieldData.fileTypes, e.target.value, e.target.checked)} /> 
                                <label
                                    style={{fontSize: '10px', fontWeight: '400', marginRight: '15px'}}
                                    for="pdf"
                                >PDF</label>
                                <input
                                    style={{margin: '0', verticalAlign: 'middle'}}
                                    type="checkbox"
                                    name="doc"
                                    value="doc"
                                    id="doc"
                                    checked={doc}
                                    onChange={(e) => handleFileTypeArray(selectedFieldData.fileTypes, e.target.value, e.target.checked)} />
                                <label
                                    style={{fontSize: '10px', fontWeight: '400', marginRight: '15px'}}
                                    for="doc"
                                >DOC</label>
                                <input
                                    style={{margin: '0', verticalAlign: 'middle'}}
                                    type="checkbox"
                                    name="docx"
                                    value="docx"
                                    id="docx"
                                    checked={docx}
                                    onChange={(e) => handleFileTypeArray(selectedFieldData.fileTypes, e.target.value, e.target.checked)} />
                                <label
                                    style={{fontSize: '10px', fontWeight: '400'}}
                                    for="docx"
                                >DOCX</label>
                            </div>
                        </div>
                        <div style={{display: "flex"}}>
                            <input type="checkbox" name="multifile" value="multifile" id="multifile" onChange={(e) => handleInputchange(e.target.checked, "multiFile")} />
                            <label for="multifile">Accept Multiple Files</label>
                        </div>
                    </React.Fragment>
                )
            } else if (selectedFieldData.type === 'select') {
                return (
                    <React.Fragment>
                        <div>
                            <label>Placeholder</label>
                            <input value={selectedFieldData.placeholder} onChange={(e) => handleInputchange(e.target.value, 'placeholder')} />
                        </div>
                        <div>
                            <label>Options</label>
                            {selectedFieldData.options.map((option, index) => {
                                return (
                                    <div className="options">
                                        <input value={option} onChange={(e => updateOption('edit', index, e.target.value))} />
                                        <button onClick={() => updateOption('add', index)}><i className="material-icons">add</i></button>
                                        <button onClick={() => updateOption('remove', index)}><i className="material-icons">remove</i></button>
                                    </div>
                                )
                            })}
                        </div>
                    </React.Fragment>
                )
            } else if (selectedFieldData.type === 'checkbox' ||
                        selectedFieldData.type === 'radio' || 
                        selectedFieldData.type === 'select') {
                return (
                    <div>
                        <label>Options</label>
                        {selectedFieldData.options.map((option, index) => {
                            return (
                                <div className="options">
                                    <input value={option} onChange={(e => updateOption('edit', index, e.target.value))} />
                                    <button onClick={() => updateOption('add', index)}><i className="material-icons">add</i></button>
                                    <button onClick={() => updateOption('remove', index)}><i className="material-icons">remove</i></button>
                                </div>
                            )
                        })}
                    </div>
                )
            }
        }
        return;
    }
    return (
        <DragDropContext onDragEnd={formBuilderOnDragEnd}>
            <div className="form-builder">
                <div className="form-builder-wrapper">
                    <div className="close-popup" onClick={closeFormDesignPopup}>
                        <i className="material-icons">close</i>
                    </div>
                    <div className="form-fields">
                        {overAllFields.map((field, index) => {
                            return <div className="draggable-button" onMouseDown={() => addNewField(field)}>{field}</div>
                        })}
                    </div>
                    <div className="form-layout">
                        <Droppable droppableId="FORMBUILDERCANVAS" type="formbuilder">
                            {provided => (
                                <div ref={provided.innerRef}>
                                    {formData.layers.entities.element[formData.activeElement].formData.map(formId => {
                                        return formData.layers.entities.formData[formId].fields.map((fieldId, index) => {
                                            const fieldData = formData.layers.entities.field[fieldId];
                                            if (fieldData.type === 'text' || fieldData.type === 'email'
                                                 || fieldData.type === 'number' || fieldData.type === 'select'
                                                 || fieldData.type === 'textarea') {
                                                return (
                                                    <Draggable key={fieldId} draggableId={fieldId} index={index}>
                                                        {dragProvided => (
                                                            <div 
                                                                className="form-field" 
                                                                // onClick={() => selectField(fieldData.id)}
                                                                ref={dragProvided.innerRef}
                                                                {...dragProvided.dragHandleProps}
                                                                {...dragProvided.draggableProps}
                                                            >
                                                                <label {...dragProvided.dragHandleProps}>{fieldData.label}{fieldData.required && " *"}</label>
                                                                <span onClick={() => deleteField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>delete</i>
                                                                </span>
                                                                <span onClick={() => selectField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>edit</i>
                                                                </span>
                                                                <div className="alt-input" {...dragProvided.dragHandleProps}>{fieldData.placeholder}</div>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                );
                                            } else if (fieldData.type === 'file') {
                                                return (
                                                    <Draggable key={fieldId} draggableId={fieldId} index={index}>
                                                        {dragProvided => (
                                                            <div
                                                                className="form-field"
                                                                // onClick={() => selectField(fieldData.id)}
                                                                ref={dragProvided.innerRef}
                                                                {...dragProvided.dragHandleProps}
                                                                {...dragProvided.draggableProps}
                                                            >
                                                                <label {...dragProvided.dragHandleProps}>{fieldData.label}{fieldData.required && " *"}</label>
                                                                <span onClick={() => deleteField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>delete</i>
                                                                </span>
                                                                <span onClick={() => selectField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>edit</i>
                                                                </span>
                                                                <div className="alt-input file-upload" {...dragProvided.dragHandleProps}>
                                                                    <i className="material-icons">cloud_upload</i>
                                                                    <span>Drop Here</span>
                                                                </div>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                );
                                            } else if (fieldData.type === 'radio' || fieldData.type === 'checkbox') {
                                                return (
                                                    <Draggable key={fieldId} draggableId={fieldId} index={index}>
                                                        {dragProvided => (
                                                            <div 
                                                                className="form-field" 
                                                                // onClick={() => selectField(fieldData.id)}
                                                                ref={dragProvided.innerRef}
                                                                {...dragProvided.dragHandleProps}
                                                                {...dragProvided.draggableProps}
                                                            >
                                                                <label {...dragProvided.dragHandleProps}>{fieldData.label}{fieldData.required && " *"}</label>
                                                                <span onClick={() => deleteField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>delete</i>
                                                                </span>
                                                                <span onClick={() => selectField(fieldId)} style={{float: "right", cursor: "pointer"}}>
                                                                    <i className="material-icons" style={{fontSize: "20px"}}>edit</i>
                                                                </span>
                                                                {fieldData.options.map(option => {
                                                                    return (
                                                                        <div>
                                                                            <input type={fieldData.type} name={fieldData.label} value={option} disabled/>
                                                                            <span>{option}</span>
                                                                        </div>
                                                                    )
                                                                })}
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                );
                                            }
                                            
                                        })
                                    })}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </div>
                    <div className="form-field-style">
                        {selectedField !== null && 
                            <React.Fragment>
                                <div>
                                    <label>Label</label>
                                    <input value={formData.layers.entities.field[selectedField].label} onChange={(e) => handleInputchange(e.target.value, 'label')} />
                                </div>
                                {fieldContent()}
                                <div>
                                    <label>
                                        <input type="checkbox" checked={formData.layers.entities.field[selectedField].required} onChange={(e) => handleInputchange(e.target.checked, 'required')} />
                                        <span>Required field</span>
                                    </label>
                                </div>
                                {/* <div>
                                    <button onClick={deleteField} className="dtn-delete"><i className="material-icons">delete</i>Delete Field</button>
                                </div> */}
                            </React.Fragment>
                        }
                    </div>
                </div>
            </div>
        </DragDropContext>
    );
}

export default FormBuilder;
import React, { useContext } from "react";
import Button from "../../elements/button";
import { Store } from "../../../store";
import actions from "../../../store/action-types";
import "./menubar.scss";
import Toolbar from "../toolbar/index";
import { TOOL_ITEMS } from "./constants";

const Menubar = ({ items }) => {
  const { state, dispatch } = useContext(Store);
  const toggleMenubar = (menuName) => {
    dispatch({
      type: actions.REMOVE_SELECT_ELEMENT
    });
    if (state.menuBarType === menuName) {
      return dispatch({
        type: actions.TOGGLE_MENUBAR,
        toolbar: menuName
      });
    }
    return dispatch({
      type: actions.ACTIVE_MENUBAR,
      toolbar: menuName
    });
  };
  return (
    <React.Fragment>
      <div className="menubar">
        <ul>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("layout")}>
              {/* <i className="pro-icons layout">view_quilt</i> */}
              <i className="material-icons">layers</i>
              <span>Layouts</span>
            </Button>
          </li>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("widgets")}>
              {/* <i className="pro-icons socket">widgets</i> */}
              <i className="material-icons">widgets</i>
              <span>Widgets</span>
            </Button>
          </li>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("style")}>
              <i className="material-icons">color_lens</i>
              <span>Styles</span>
            </Button>
          </li>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("track")}>
              {/* <i className="pro-icons tract">settings</i> */}
              <i className="material-icons">track_changes</i>
              {/* <span>Settings</span> */}
              <span>Tracking</span>
            </Button>
          </li>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("sticky")}>
              <i className="material-icons">call_to_action</i>
              <span>Sticky</span>
            </Button>
          </li>
          <li className="menubar-item">
            <Button className="menubar-button" onClick={() => toggleMenubar("popup")}>
              <i className="material-icons">featured_video</i>
              <span>Popup</span>
            </Button>
          </li>
        </ul>
      </div>
      {!state.isStyleActive && state.isMenuBarActive && <div className="menubar-details">
          <Toolbar tools={TOOL_ITEMS} />
          {/* <Menu items={items} /> */}
      </div>}
    </React.Fragment>
  );
};

export default Menubar;

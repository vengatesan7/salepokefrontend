import uuid from 'uuid/v4';

export const TOOL_ITEMS = [
  // {
  //   id: uuid(),
  //   name: "Title",
  //   type: "TITLE",
  //   className: "text_format"
  // },
  {
    id: uuid(),
    // name: "Paragraph",
    name: "Text",
    type: "PARAGRAPH",
    // className: "subject"
    className: "text_fields"
  },
  {
    id: uuid(),
    name: "Image",
    type: "IMAGE",
    // className: "image"
    className: "wallpaper"
  },
  {
    id: uuid(),
    name: "Button",
    type: "BUTTON",
    className: "remove_from_queue"
  },
  {
    id: uuid(),
    name: "Video",
    type: "VIDEO",
    className: "ondemand_video"
    // className: "videocam"
  },
  {
    id: uuid(),
    name: "Form",
    type: "FORM",
    // className: "assignment"
    className: "post_add"
  },
  {
    id: uuid(),
    name: "Advance Form",
    type: "ADVANCEFORM",
    className: "assignment"
  },
  {
    id: uuid(),
    name: "Line",
    type: "LINE",
    className: "line_style"
  },
  {
    id: uuid(),
    name: "Space",
    type: "SPACE",
    className: "space_bar"
  },
  {
    id: uuid(),
    name: "Timer",
    type: "TIMER",
    className: "timer"
  },
  {
    id: uuid(),
    name: "Pricing Table",
    type: "PRICECHART",
    className: "local_atm"
  },
  // {
  //   id: uuid(),
  //   name: "Image + Text",
  //   type: "IMAGEPLUSTEXT",
  //   className: "art_track"
  // },
  {
    id: uuid(),
    // name: "Billing & Shipping",
    name: "Checkout Form",
    type: "PRODUCT",
    className: "shopping_basket"
  },
  {
    id: uuid(),
    // name: "Billing & Shipping",
    name: "Upsell Product",
    type: "UPSELL",
    className: "shopping_basket"
  },
  {
    id: uuid(),
    name: "Icons",
    type: "ICON",
    className: "face"
  }
];

// Ecommerce Widgets
export const ORDER_TOOLS = [
  // {
  //   id: uuid(),
  //   name: "Credit Card",
  //   type: "CARD",
  //   className: "credit_card"
  // },
  {
    id: uuid(),
    // name: "Billing & Shipping",
    name: "Checkout Form",
    type: "PRODUCT",
    className: "shopping_basket"
  },
  {
    id: uuid(),
    // name: "Billing & Shipping",
    name: "Upsell",
    type: "UPSELL",
    className: "shopping_basket"
  },
  // {
  //   id: uuid(),
  //   name: "Shipping",
  //   type: "ADDRESS",
  //   className: "local_shipping"
  // }
];

export const ALL_ITEMS = TOOL_ITEMS.concat(ORDER_TOOLS);
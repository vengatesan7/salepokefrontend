import React, { useContext } from 'react';

import './stylebar.scss';
import actions from "../../../store/action-types";
import { Store } from '../../../store';
import Section from './section';
import Element from './element';
import Column from './column';

const Style = () => {
    const { state, dispatch } = useContext(Store)
    const closeStylebar = () => {
        return dispatch({
            type: actions.REMOVE_SELECT_ELEMENT
        })
        // return dispatch({
        //     type: actions.TOGGLE_SECTION_STYLE
        // });
    }
    const styleType = (type) => {
        switch (type) {
            case "section":
                return <Section />
            case "element":
                return <Element />
            case "column":
                return <Column />
            default:
                return;
        }
    }
    return (
        <div className="style-bar">
            <div className="close">
                <i className="material-icons" onClick={closeStylebar}>close</i>
            </div>
            <div className="style-bar-wrapper">
                {styleType(state.selectedStyleType)}
            </div>
        </div>
    );
}

export default Style;
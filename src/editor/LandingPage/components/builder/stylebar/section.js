import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from 'lodash.get';
import actions from '../../../store/action-types';
import { Store } from '../../../store';
import SpinButtonInput from '../../elements/spinButtonInput';
import ImageUpload from '../../../../components/image-upload';
 
const Section = () => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    // const [sectionData, setSectionData] = useState(null);
    const [colorPicker, popup] = useState(false);
    const [thisColor, pickedColor] = useState(null);
    const [visibleOnMobile, setVisibleOnMobile] = useState(true);
    const [sectionStyles, setSectionStyles] = useState(null);
    const [importPopup, setImportPopup] = useState(false);
    const [backgroundImage, setBackgroundImage] = useState(null);    
    const getData = () => {        
        const sectionData = stateHistory.layers.entities.section[state.selectedSectionId];
        setVisibleOnMobile(_get(sectionData, 'sectionsData.visibleOnMobile', true));
        const desktopData = {
            background: _get(sectionData, 'sectionsData.styles.background', '#ffffff'),
            styles: _get(sectionData, 'sectionsData.styles', ''),
            backgroundImage: _get(sectionData, 'sectionsData.styles.backgroundImage', null),            
        }
        switch (state.deviceType) {
            case "phone":
                pickedColor(
                    { 'background': _get(sectionData, 'sectionsData.phoneStyles.background', desktopData.background) }
                );
                setSectionStyles(_get(sectionData, 'sectionsData.phoneStyles', desktopData.styles));
                setBackgroundImage(_get(sectionData, 'sectionsData.phoneStyles.backgroundImage', desktopData.backgroundImage));                
                break;
            case "tablet":
                pickedColor(
                    { 'background': _get(sectionData, 'sectionsData.tabletStyles.background', desktopData.background) }
                );
                setSectionStyles(_get(sectionData, 'sectionsData.tabletStyles', desktopData.styles));
                setBackgroundImage(_get(sectionData, 'sectionsData.tabletStyles.backgroundImage', desktopData.backgroundImage));                
                break;
            default:
                pickedColor(
                    { 'background': desktopData.background }
                );
                setSectionStyles(desktopData.styles);
                setBackgroundImage(desktopData.backgroundImage);                            
                break;
        }
    }

    useEffect(() => {
        getData();
    }, [state.selectedSectionId]);
    useEffect(() => {
        getData();
    }, [stateHistory]);
    useEffect(() => {
        getData();
    }, [state.deviceType]);
    
    const colorClick = (colorCode) => {        
        pickedColor({ 'background': colorCode });
        const newUpdate = {
            ...layers,
            entities: {
                ...layers.entities,
                section: {
                    ...layers.entities.section,
                    [state.selectedSectionId]: {
                        ...layers.entities.section[state.selectedSectionId],
                        sectionsData: {
                            ...layers.entities.section[state.selectedSectionId].sectionsData,
                            styles: {
                                ...layers.entities.section[state.selectedSectionId].sectionsData.styles,
                                background: colorCode
                            }
                        }
                    }
                }
            }
        }
        return updateLayers(newUpdate);
    }
    const handleSectionStyleChanges = (value, type) => {
        setImportPopup(false);
        const newStyle = {
            ...sectionStyles,
            [type]: value
        }        
        setSectionStyles(newStyle);
        switch (state.deviceType) {
            case "phone":
                const newPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        section: {
                            ...layers.entities.section,
                            [state.selectedSectionId]: {
                                ...layers.entities.section[state.selectedSectionId],
                                sectionsData: {
                                    ...layers.entities.section[state.selectedSectionId].sectionsData,
                                    phoneStyles: newStyle
                                }
                            }
                        }
                    }
                }
                return updateLayers(newPhoneData);
            case "tablet":
                const newTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        section: {
                            ...layers.entities.section,
                            [state.selectedSectionId]: {
                                ...layers.entities.section[state.selectedSectionId],
                                sectionsData: {
                                    ...layers.entities.section[state.selectedSectionId].sectionsData,
                                    tabletStyles: newStyle
                                }
                            }
                        }
                    }
                }
                return updateLayers(newTabletData);
            default:
                const newData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        section: {
                            ...layers.entities.section,
                            [state.selectedSectionId]: {
                                ...layers.entities.section[state.selectedSectionId],
                                sectionsData: {
                                    ...layers.entities.section[state.selectedSectionId].sectionsData,
                                    styles: newStyle
                                }
                            }
                        }
                    }
                }
                return updateLayers(newData);
        }
    }
    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });        
    };
    const handleSectionDataChanges = (value, name) => {
        name === "visibleOnMobile" && setVisibleOnMobile(value);
        const updatedData = {
            ...layers,
            entities: {
                ...layers.entities,
                section: {
                    ...layers.entities.section,
                    [state.selectedSectionId]: {
                        ...layers.entities.section[state.selectedSectionId],
                        sectionsData: {
                            ...layers.entities.section[state.selectedSectionId].sectionsData,
                            [name]: value
                        }
                    }
                }
            }
        }
        return updateLayers(updatedData);
    }
    return (
        sectionStyles !== null && <React.Fragment>
            <div className="property-form">
                <h4>Background</h4>
                <div className="two-column">
                    <label>Color</label>
                    <div>
                        <div className="color-picker-swatch" onClick={() => popup(true)}>
                            <div className="color-picker-color" style={thisColor}></div>
                        </div>
                        {colorPicker && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => popup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={thisColor.background} onChange={(e) => colorClick(e.hex)} disableAlpha />
                                {/* <button onClick={() => popup(false)}>Cancel</button>
                                <button onClick={}>Choose</button> */}
                                <button className='color-picker-button' onClick={() => popup(false)}>Ok</button>
                            </div>
                        </div>}
                        <div className="color-picker-none" onClick={() => colorClick("transparent")} ><i className="material-icons">not_interested</i></div>
                    </div>
                </div>
            </div>
            <div className="property-form">
                <button className="edit-form-button" onClick={() => setImportPopup(true)}>Upload Image</button>
            </div>
            {importPopup && <ImageUpload imageLink={(e) => handleSectionStyleChanges(e, 'backgroundImage')} popupClose={() => setImportPopup(false)} />}
            {backgroundImage && <div className="property-form">
                    <div className="image-viewer">
                        <img src={backgroundImage} alt="URL is not valid" />
                    </div>
                    <button className="edit-form-button" onClick={() => handleSectionStyleChanges(null, 'backgroundImage')}>Remove Image</button>
                </div>
            }            
            <div className="property-form">
                <h4>Padding</h4>
                <div className="two-column">
                    <div>
                        <label>Top</label>
                        <div className="rangeslider-wrapper">
                            <div className="rangeslider-slider">
                                <Slider min={0} max={10} step={1}
                                    value={sectionStyles.paddingTop / 10 || 0}
                                    orientation="horizontal"
                                    tooltip={false}
                                    onChange={(e) => handleSectionStyleChanges(e * 10, 'paddingTop')}
                                />
                            </div>
                            <div className="rangeslider-value">
                                {sectionStyles.paddingTop / 10 || 0}
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>Right</label>
                        <div className="rangeslider-wrapper">
                            <div className="rangeslider-slider">
                                <Slider min={0} max={10} step={1}
                                    value={sectionStyles.paddingRight / 10 || 0}
                                    orientation="horizontal"
                                    tooltip={false}
                                    onChange={(e) => handleSectionStyleChanges(e * 10, 'paddingRight')}
                                />
                            </div>
                            <div className="rangeslider-value">
                                {sectionStyles.paddingRight / 10 || 0}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="two-column">
                    <div>
                        <label>Bottom</label>
                        <div className="rangeslider-wrapper">
                            <div className="rangeslider-slider">
                                <Slider min={0} max={10} step={1}
                                    value={sectionStyles.paddingBottom / 10 || 0}
                                    orientation="horizontal"
                                    tooltip={false}
                                    onChange={(e) => handleSectionStyleChanges(e * 10, 'paddingBottom')}
                                />
                            </div>
                            <div className="rangeslider-value">
                                {sectionStyles.paddingBottom / 10 || 0}
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>Left</label>
                        <div className="rangeslider-wrapper">
                            <div className="rangeslider-slider">
                                <Slider min={0} max={10} step={1}
                                    value={sectionStyles.paddingLeft / 10 || 0}
                                    orientation="horizontal"
                                    tooltip={false}
                                    onChange={(e) => handleSectionStyleChanges(e * 10, 'paddingLeft')}
                                />
                            </div>
                            <div className="rangeslider-value">
                                {sectionStyles.paddingLeft / 10 || 0}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="property-form">
                <h4>Device Specific</h4>
                <div>
                    <input type="checkbox" checked={visibleOnMobile} id="mobile-active" onChange={(e) => handleSectionDataChanges(e.target.checked, 'visibleOnMobile')}
                        style={{
                            height: "18px",
                            width: "18px",
                            marginRight: "5px",
                            verticalAlign: "bottom"
                        }}
                    />
                    <label for="mobile-active"
                        style={{
                            lineHeight: "12px",
                            verticalAlign: "bottom",
                            fontWeight: "400",
                            margin: "0"
                        }}
                    >Enable section to visible in mobile</label>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Section;
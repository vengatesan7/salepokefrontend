import React, { useContext, useEffect, useState } from 'react';

import { Store } from '../../../store';
import Lineheight from "./element/properties/lineheight";

import Font from "./element/properties/font";
import Background from './element/properties/background';
import Margin from './element/properties/margin';
import Padding from './element/properties/padding';
import Border from './element/properties/border';
import Form from './element/properties/form';
import Align from './element/properties/align';
import Video from './element/properties/video';
import Button from './element/properties/button';
import Icon from './element/properties/Icon';

import Image from './element/properties/image';
import Direction from './element/properties/direction';
import Size from './element/properties/size';
import HightWidth from './element/properties/hightwidth';
import Line from './element/properties/line';
import ButtonAction from './element/properties/action';
import List from './element/properties/list';
import Timer from './element/properties/timer';
import Checkout from './element/properties/checkout';
import AdvanceForm from './element/properties/advanceform';

const Element = () => {
    const {
        state,
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [elementType, setElementType] = useState(null);
    const [elementData, setElementData] = useState(null);

    useEffect(() => {
        setElementData(layers.entities.element[state.activeElement]);
    }, [state.activeElement])
    useEffect(() => {
        setElementData(layers.entities.element[state.activeElement]);
    }, [stateHistory])
    const elementTypeStyles = (type) => {
        switch (type) {
            case "FORM":
                return (
                    <React.Fragment>
                        <Form elementId={state.activeElement} />
                        {/* <Font elementId={state.activeElement} /> */}
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        <Align elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "ADVANCEFORM":
                return <React.Fragment>
                    <AdvanceForm elementId={state.activeElement} /> 
                    <Background elementId={state.activeElement} />
                    <Margin elementId={state.activeElement} />
                    <Padding elementId={state.activeElement} />
                    <Border elementId={state.activeElement} />
                    <Align elementId={state.activeElement} />
                </React.Fragment>;
            case "TITLE":
                return (
                    <React.Fragment>
                        {/* <Font elementId={state.activeElement} /> */}
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        {/* <Align elementId={state.activeElement} /> */}
                    </React.Fragment>
                );
            case "PARAGRAPH":
                return (
                    <React.Fragment>
                        <Lineheight elementId={state.activeElement} />
                       {/* <List elementId={state.activeElement} /> */}
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        {/* <Align elementId={state.activeElement} /> */}
                    </React.Fragment>
                );
            case "IMAGE":
                return (
                    <React.Fragment>
                        <Image elementId={state.activeElement} />
                        <ButtonAction elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                        <Align elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "BUTTON":
                return (
                    <React.Fragment>
                        <Button elementId={state.activeElement} />
                        <ButtonAction elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <HightWidth elementId={state.activeElement} />
                        <Align elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        {/* <Font elementId={state.activeElement} /> */}
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "VIDEO":
                return (
                    <React.Fragment>
                        <Video elementId={state.activeElement} />
                        <HightWidth elementId={state.activeElement} />
                        <Size elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "LINE":
                return (
                    <React.Fragment>
                        <Padding elementId={state.activeElement} />
                        <Line elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "IMAGEPLUSTEXT":
                return (
                    <React.Fragment>
                        <Image elementId={state.activeElement} />
                        <Direction elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "SPACE":
                return (
                    <React.Fragment>
                        <Size elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "TIMER":
                return (
                    <React.Fragment>
                        <Timer elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "PRICECHART":
                return (
                    <React.Fragment>
                        <Font elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "PRODUCT":
                return (
                    <React.Fragment>
                        <Checkout elementId={state.activeElement} />
                        <ButtonAction elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                    </React.Fragment>
                );
            case "ICON":
            return (
                <React.Fragment>
                        <Icon elementId={state.activeElement} />
                        <ButtonAction elementId={state.activeElement} />
                        <Border elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <Align elementId={state.activeElement} />
                        {/* <Font elementId={state.activeElement} /> */}
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                </React.Fragment>
            );
            case "UPSELL":
                return (
                    <React.Fragment>
                        <Checkout elementId={state.activeElement} />
                        <ButtonAction elementId={state.activeElement} />
                        <Background elementId={state.activeElement} />
                        <Margin elementId={state.activeElement} />
                        <Padding elementId={state.activeElement} />
                    </React.Fragment>
                );
            default:
                break;
        }
    }
    useEffect(() => {
        setElementType(state.layers.entities.element[state.activeElement].elementType);
    }, [state.activeElement])

    return (
        <div>{elementTypeStyles(elementType)}</div>
    );
}

export default Element;
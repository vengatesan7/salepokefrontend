import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from 'lodash.get';

import actions from '../../../store/action-types';
import { Store } from '../../../store';
import SpinButtonInput from '../../elements/spinButtonInput';
import ImageUpload from '../../../../components/image-upload';

const Column = () => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [colorPicker, popup] = useState(false);
    const [thisColor, pickedColor] = useState(null);
    const [backgroundImage, setBackgroundImage] = useState(null);
    const [paddingTop, setPaddingTop] = useState(null);
    const [paddingRight, setPaddingRight] = useState(null);
    const [paddingBottom, setPaddingBottom] = useState(null);
    const [paddingLeft, setPaddingLeft] = useState(null);
    const [marginTop, setMarginTop] = useState(null);
    const [marginRight, setMarginRight] = useState(null);
    const [marginBottom, setMarginBottom] = useState(null);
    const [marginLeft, setMarginLeft] = useState(null);

    const [importPopup, setImportPopup] = useState(false);

    const getColumnData = () => {
        const columnData = stateHistory.layers.entities.columns[state.selectedColumnId];
        const desktopData = {
            background: _get(columnData.phoneStyles, 'background', ''),
            backgroundImage: _get(columnData.styles, 'backgroundImage', null),
            paddingTop: _get(columnData.styles, 'paddingTop', 0),
            paddingRight: _get(columnData.styles, 'paddingRight', 0),
            paddingBottom: _get(columnData.styles, 'paddingBottom', 0),
            paddingLeft: _get(columnData.styles, 'paddingLeft', 0),
            marginTop: _get(columnData.styles, 'marginTop', 0),
            marginRight: _get(columnData.styles, 'marginRight', 0),
            marginBottom: _get(columnData.styles, 'marginBottom', 0),
            marginLeft: _get(columnData.styles, 'marginLeft', 0)
        }
        switch (state.deviceType) {
            case "phone":
                pickedColor({ 'background': _get(columnData.phoneStyles, 'background', desktopData.background) });
                setBackgroundImage(_get(columnData.phoneStyles, 'backgroundImage', desktopData.backgroundImage));
                setPaddingTop(_get(columnData.phoneStyles, 'paddingTop', desktopData.paddingTop));
                setPaddingRight(_get(columnData.phoneStyles, 'paddingRight', desktopData.paddingRight));
                setPaddingBottom(_get(columnData.phoneStyles, 'paddingBottom', desktopData.paddingBottom));
                setPaddingLeft(_get(columnData.phoneStyles, 'paddingLeft', desktopData.paddingLeft));
                setMarginTop(_get(columnData.phoneStyles, 'marginTop', desktopData.marginTop));
                setMarginRight(_get(columnData.phoneStyles, 'marginRight', desktopData.marginRight));
                setMarginBottom(_get(columnData.phoneStyles, 'marginBottom', desktopData.marginBottom));
                setMarginLeft(_get(columnData.phoneStyles, 'marginLeft', desktopData.marginLeft));
                break;
            case "tablet":
                pickedColor({ 'background': _get(columnData.tabletStyles, 'background', desktopData.background) });
                setBackgroundImage(_get(columnData.tabletStyles, 'backgroundImage', desktopData.backgroundImage));
                setPaddingTop(_get(columnData.tabletStyles, 'paddingTop', desktopData.paddingTop));
                setPaddingRight(_get(columnData.tabletStyles, 'paddingRight', desktopData.paddingRight));
                setPaddingBottom(_get(columnData.tabletStyles, 'paddingBottom', desktopData.paddingBottom));
                setPaddingLeft(_get(columnData.tabletStyles, 'paddingLeft', desktopData.paddingLeft));
                setMarginTop(_get(columnData.tabletStyles, 'marginTop', desktopData.marginTop));
                setMarginRight(_get(columnData.tabletStyles, 'marginRight', desktopData.marginRight));
                setMarginBottom(_get(columnData.tabletStyles, 'marginBottom', desktopData.marginBottom));
                setMarginLeft(_get(columnData.tabletStyles, 'marginLeft', desktopData.marginLeft));
                break;
            default:
                pickedColor({ 'background': desktopData.background });
                setBackgroundImage(desktopData.backgroundImage);
                setPaddingTop(desktopData.paddingTop);
                setPaddingRight(desktopData.paddingRight);
                setPaddingBottom(desktopData.paddingBottom);
                setPaddingLeft(desktopData.paddingLeft);
                setMarginTop(desktopData.marginTop);
                setMarginRight(desktopData.marginRight);
                setMarginBottom(desktopData.marginBottom);
                setMarginLeft(desktopData.marginLeft);
                break;
        }
    }

    useEffect(() => {
        getColumnData();
    }, [stateHistory]);
    useEffect(() => {
        getColumnData();
    }, [state.deviceType]);

    const handleBackgroundColor = (value) => {
        pickedColor({ background: value })
    }
    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };
    const handleChanges = (value, name) => {
        popup(false);
        setImportPopup(false);
        name === 'background' &&  pickedColor({ 'background': value });
        name === 'backgroundImage' && setBackgroundImage(value);
        name === 'paddingTop' && setPaddingTop(value);
        name === 'paddingRight' && setPaddingRight(value);
        name === 'paddingBottom' && setPaddingBottom(value);
        name === 'paddingLeft' && setPaddingLeft(value);
        name === 'marginTop' && setMarginTop(value);
        name === 'marginRight' && setMarginRight(value);
        name === 'marginBottom' && setMarginBottom(value);
        name === 'marginLeft' && setMarginLeft(value);
        switch (state.deviceType) {
            case "phone":
                const updatedPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        columns: {
                            ...layers.entities.columns,
                            [state.selectedColumnId]: {
                                ...layers.entities.columns[state.selectedColumnId],
                                phoneStyles: {
                                    ...layers.entities.columns[state.selectedColumnId].phoneStyles,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return updateLayers(updatedPhoneData);
            case "tablet":
                const updatedTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        columns: {
                            ...layers.entities.columns,
                            [state.selectedColumnId]: {
                                ...layers.entities.columns[state.selectedColumnId],
                                tabletStyles: {
                                    ...layers.entities.columns[state.selectedColumnId].tabletStyles,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return updateLayers(updatedTabletData);
            default:
                const updatedData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        columns: {
                            ...layers.entities.columns,
                            [state.selectedColumnId]: {
                                ...layers.entities.columns[state.selectedColumnId],
                                styles: {
                                    ...layers.entities.columns[state.selectedColumnId].styles,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return updateLayers(updatedData);
        }
    }
    return (
        <React.Fragment>
            <div className="property-form">
                <h4>Background</h4>
                <div className="two-column">
                    <label>Color</label>
                    <div>
                        <div className="color-picker-swatch" onClick={() => popup(true)}>
                            <div className="color-picker-color" style={thisColor}></div>
                        </div>
                        {colorPicker && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => popup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={thisColor.background} onChange={(e) => handleBackgroundColor(e.hex)} disableAlpha />
                                <button className='color-picker-button' onClick={() => handleChanges(thisColor.background, 'background')}>Ok</button>
                            </div>
                        </div>}
                        <div className="color-picker-none" onClick={() => handleChanges("transparent", 'background')} >
                            <i className="material-icons">not_interested</i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="property-form">
                <button className="edit-form-button" onClick={() => setImportPopup(true)}>Upload Image</button>
            </div>
            {backgroundImage && <div className="property-form">
                <div className="image-viewer">
                    <img src={backgroundImage} alt="URL is not valid" />
                </div>
                <button className="edit-form-button" onClick={() => handleChanges(null, 'backgroundImage')}>Remove Image</button>
            </div>}
            {importPopup && <ImageUpload imageLink={(e) => handleChanges(e, 'backgroundImage')} popupClose={() => setImportPopup(false)} />}
            <div className="property-form">
                <h4>Margin</h4>
                <div className="two-column">
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Top
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {marginTop !== null && <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={marginTop}
                                    callBack={(v) => handleChanges(v, 'marginTop')}
                                />}
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Bottom
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {marginBottom !== null && <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={marginBottom}
                                    callBack={(v) => handleChanges(v, 'marginBottom')}
                                />}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="property-form">
                <h4>Padding</h4>
                <div className="two-column">
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Top
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {paddingTop !== null && <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={paddingTop}
                                    callBack={(v) => handleChanges(v, 'paddingTop')}
                                />}
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Right
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {paddingRight !== null && <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={paddingRight}
                                    callBack={(v) => handleChanges(v, 'paddingRight')}
                                />}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="two-column">
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Bottom
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {paddingBottom !== null && <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={paddingBottom}
                                    callBack={(v) => handleChanges(v, 'paddingBottom')}
                                />}
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Left
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                {paddingLeft !== null && <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={paddingLeft}
                                    callBack={(v) => handleChanges(v, 'paddingLeft')}
                                />}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Column;
import React, { useState, useEffect, useContext } from 'react';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import ImageUpload from '../../../../../../components/image-upload';
import { Store } from "../../../../../store";

const Image = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [imagePath, setImagePath] = useState(null);
    const [width, setWidth] = useState(null);
    const [importPopup, setImportPopup] = useState(false);
    const getData = () => {
        setImagePath(_get(stateHistory.layers.entities.element[elementId], 'path', null));
        setWidth(_get(stateHistory.layers.entities.element[elementId], 'width', null));
    }
    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [stateHistory]);
    const updateLayer = (data) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }
    const handleChanges = (value, name) => {
        name === "path" && setImagePath(value);
        name === "width" && setWidth(value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return updateLayer(data)
    }
    const handleImageUploadPackage = (path) => {
        setImagePath(path);
        setImportPopup(false);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        path: path
                    }
                }
            }
        }
        return updateLayer(data);
    }
    return (
        <React.Fragment>
            <div className="property-form">
                <button className="edit-form-button" onClick={() => setImportPopup(true)}>Upload Image</button>
                <div>
                    <label>Image URL</label>
                    <input
                        value={imagePath}
                        style={{
                            width: "100%",
                            padding: "5px"
                        }}
                        onChange={(e) => handleImageUploadPackage(e.target.value)}
                    />
                </div>
            </div>
            <div className="property-form">
                {imagePath && <div className="image-viewer">
                    <img src={imagePath} alt="URL is not valid" />
                </div>}
                <div>
                    <label>Width</label>
                    <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={0} max={100} step={1}
                                value={width || 100}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleChanges(e, 'width')}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {width || 100}%
                        </div>
                    </div>
                </div>
            </div>
            {importPopup && <ImageUpload imageLink={(e) => handleImageUploadPackage(e)} popupClose={() => setImportPopup(false)} />}
        </React.Fragment>
    );
}

export default Image;
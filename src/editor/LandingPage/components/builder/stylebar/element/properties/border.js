import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Border = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [colorPicker, popup] = useState(false);
    const [styles, setStyles] = useState(null)

    const getData = () => {
        const elementData = stateHistory.layers.entities.element[elementId]
        setStyles({
            borderRadius: elementData.borderRadius !== undefined ? elementData.borderRadius: 0,
            borderStyle: elementData.borderStyle !== undefined ? elementData.borderStyle: "none",
            borderWidth: elementData.borderWidth !== undefined ? elementData.borderWidth: 0,
            borderColor: elementData.borderColor !== undefined ? elementData.borderColor: "#000000"
        });
    }
    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [stateHistory]);

    const handleChange = (value, name) => {
        setStyles({ ...styles, [name]: value })
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    return (
        styles !== null &&
        <form className="property-form">
            <h4>Border</h4>
            <div className='property-border'>
                <select value={styles.borderStyle} onChange={(e) => handleChange(e.target.value, 'borderStyle')}>
                    <option value="none">None</option>
                    <option value="solid">Solid</option>
                    <option value="dotted">Dotted</option>
                    <option value="dashed">Dashed</option>
                    <option value="double">Double</option>
                </select>
                <select value={styles.borderWidth} onChange={(e) => handleChange(Number(e.target.value), 'borderWidth')}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
                <div>
                    <div className="color-picker-swatch" onClick={() => popup(true)}>
                        <div className="color-picker-color" style={{ 'background': styles.borderColor }}></div>
                    </div>
                    {colorPicker && <div className="color-picker-popover">
                        <div className='color-picker-cover' onClick={() => popup(false)} />
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={styles.borderColor} onChange={(e) => handleChange(e.hex, 'borderColor')} disableAlpha />
                            <button className='color-picker-button' onClick={() => popup(false)}>Ok</button>
                        </div>
                    </div>}
                </div>
            </div>
            <div className="two-column">
                <label>Radius</label>
                <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={1} value={styles.borderRadius} orientation="horizontal" tooltip={false} onChange={(e) => handleChange(e, 'borderRadius')} />
                    </div>
                    <div className="rangeslider-value">
                        {styles.borderRadius}
                    </div>
                </div>
            </div>
        </form>
    )
}

export default Border;
import React, { useState, useContext, useEffect } from "react";
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import classnames from "classnames";
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";
import ColorBoxSelection from "../../../../elements/colorBoxSelection";

const Font = ({ elementId }) => {
  const {
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);

  const [elementData, setElementData] = useState(null);
  const [colorPicker, popup] = useState(false);
  const [thisColor, pickedColor] = useState(null);
  const [fontSize, setFontSize] = useState(null);
  const [fontSpace, setFontSpace] = useState(null);
  const [fontFamily, setFontFamily] = useState(null);
  const [bold, setBold] = useState(false);
  const [italic, setItalic] = useState(false);
  const [underline, setUnderline] = useState(false);
  const [strikeout, setStrikeout] = useState(false);
  const [textLink, setTextLink] = useState(null);
  const [fontColor, setFontColor] = useState("#ffffff");

  const colorClick = (selectedColor) => {
    pickedColor({ 'background': selectedColor })
    dispatch({
      type: actions.FONT_COLOR,
      id: elementId,
      color: selectedColor
    });
  }
  const getData = () => {
    setElementData(stateHistory.layers.entities.element[elementId]);
    setFontSize(_get(stateHistory.layers.entities.element[elementId], 'fontSize', ''))
    setBold(_get(stateHistory.layers.entities.element[elementId], 'bold', null) === "bold");
    setItalic(_get(stateHistory.layers.entities.element[elementId], 'italic', null) === "italic");
    setUnderline(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "underline");
    setStrikeout(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "line-through");
    setFontSpace(_get(stateHistory.layers.entities.element[elementId], 'lineHeight', 1));
    setFontFamily(_get(stateHistory.layers.entities.element[elementId], 'fontFamily', ""))
  }
  useEffect(() => {
    getData();
  }, [stateHistory])
  useEffect(() => {
    getData();
  }, [elementId])
  useEffect(() => {
    pickedColor({ 'background': _get(elementData, 'fontColor', '#000000') })
  }, [elementData]);
  useEffect(() => {
    document.execCommand("foreColor", false, fontColor);
  }, [fontColor])
  const handleChange = (value, name) => {
    name === "fontSize" && setFontSize(value);
    name === "lineHeight" && setFontSpace(value);
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            [name]: value
          }
        }
      }
    }
    return updateElement(data);
  }
  const updateElement = (data) => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  }
  const handleChangeStyle = (value, name) => {
    name === "bold" && setBold(value);
    name === "italic" && setItalic(value);
    if (name === "underline") {
      setUnderline(value);
      // setStrikeout(!value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              textDecoration: value === true ? "underline" : null
            }
          }
        }
      }
      return updateElement(data);
    } else if (name === "strikeout") {
      setStrikeout(value);
      // setUnderline(!value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              textDecoration: value === true ? "line-through" : null
            }
          }
        }
      }
      return updateElement(data);
    } else {
      // name === "underline" && setUnderline(value);
      // name === "strikeout" && setStrikeout(value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              [name]: value === true ? name : null
            }
          }
        }
      }
      return updateElement(data);
    }
  }

  return (
    <React.Fragment>
      {elementData !== null &&
        <React.Fragment>
          <div className="property-form">
            <div className="property-form-header">
              <h4>Font Style</h4>
            </div>
            <div className="two-column">
              <label>Font Color</label>
              <div>
                <div className="color-picker-swatch" onClick={() => popup(true)}>
                  <div className="color-picker-color" style={thisColor}></div>
                </div>
                {colorPicker && <div className="color-picker-popover">
                  <div className='color-picker-cover' onClick={() => popup(false)} />
                  <div className='color-picker-wrapper'>
                    <ChromePicker color={thisColor.background} onChange={(e) => colorClick(e.hex)} disableAlpha />
                    <button className='color-picker-button' onClick={() => popup(false)}>Ok</button>
                  </div>
                </div>}
              </div>
            </div>
            <div>
              {/* <ChromePicker color={fontColor} onChange={evt => setFontColor(evt.hex)} disableAlpha /> */}
              <ColorBoxSelection selectedColor={evt => {
                document.execCommand("foreColor", false, evt);
              }}/>
            </div>
            {/* <div className="two-column">
              <label>Font Size</label>
              <div>
                <select value={fontFamily} onChange={evt => {
                    evt.preventDefault();
                    document.execCommand("fontSize", false, evt.target.value);
                  }}>
                  <option value="">Select Font Size</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                </select>
              </div>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={1} max={100} step={1}
                    value={fontSize || 1}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'fontSize')}
                  />
                </div>
                <div className="rangeslider-value">
                  {fontSize || 1}
                </div>
              </div>
            </div> */}
            {/* <div className="two-column">
              <label>Line Spacing</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={1} max={100} step={1}
                    value={fontSpace || 1}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'lineHeight')}
                  />
                </div>
                <div className="rangeslider-value">
                  {fontSpace || 1}
                </div>
              </div>
            </div> */}
            <div className="two-column">
              <label>Font Family</label>
              <div>
                <select value={fontFamily} onChange={(e) => handleChange(e.target.value, 'fontFamily')}>
                  <option value="">Select Font Family</option>
                  <option value="Roboto">Roboto</option>
                  <option value="Open Sans">Open Sans</option>
                  <option value="Ubuntu">Ubuntu</option>
                  <option value="Josefin Sans">Josefin Sans</option>
                  <option value="Tajawal">Tajawal</option>
                  <option value="Raleway">Raleway</option>
                  <option value="Oxygen">Oxygen</option>
                  <option value="Maven Pro">Maven Pro</option>
                  <option value="Catamaran">Catamaran</option>
                  <option value="Orbitron">Orbitron</option>
                  <option value="Darker Grotesque">Darker Grotesque</option>
                  <option value="Montserrat Alternates">Montserrat Alternates</option> 
                  <option value="Nunito Sans"> Nunito Sans</option>
                  <option value="Noto Serif">Noto Serif</option>
                  <option value="Lato">Lato</option>
                  <option value="Roboto Slab"> Roboto Slab</option>
                  <option value="Montserrat"> Montserrat</option>
                  <option value="Anton">Anton</option>
                  <option value="Source Sans Pro">Source Sans Pro</option>
                  <option value="Roboto Condensed ">Roboto Condensed </option>
                  <option value="Oswald">Oswald</option>  
                  <option value="Nunito Sans">Nunito Sans</option>
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                               
                </select>
                {/* <select value={fontFamily} onChange={evt => {
                    evt.preventDefault();
                    document.execCommand("fontName", false, evt.target.value);
                  }}>
                  <option value="">Select Font Family</option>
                  <option value="Roboto">Roboto</option>
                  <option value="Open Sans">Open Sans</option>
                  <option value="Ubuntu">Ubuntu</option>
                  <option value="Josefin Sans">Josefin Sans</option>
                  <option value="Tajawal">Tajawal</option>
                  <option value="Raleway">Raleway</option>
                  <option value="Oxygen">Oxygen</option>
                  <option value="Maven Pro">Maven Pro</option>
                  <option value="Catamaran">Catamaran</option>
                  <option value="Orbitron">Orbitron</option>
                  <option value="Darker Grotesque">Darker Grotesque</option>
                  <option value="Montserrat Alternates">Montserrat Alternates</option> 
                  <option value="Nunito Sans"> Nunito Sans</option>
                  <option value="Noto Serif">Noto Serif</option>
                  <option value="Lato">Lato</option>
                  <option value="Roboto Slab"> Roboto Slab</option>
                  <option value="Montserrat"> Montserrat</option>
                  <option value="Anton">Anton</option>
                  <option value="Source Sans Pro">Source Sans Pro</option>
                  <option value="Roboto Condensed ">Roboto Condensed </option>
                  <option value="Oswald">Oswald</option>  
                </select> */}
              </div>
            </div>
            <div className="two-column">
              <label>Style</label>
              <div className='property-font-style'>
                {/* <input
                  type="checkbox"
                  id="element-style-bold"
                  checked={bold} onChange={(e) => handleChangeStyle(e.target.checked, "bold")}
                />
                  <label for="element-style-bold">
                    <i className="material-icons">format_bold</i>
                  </label>
                <input
                  type="checkbox"
                  id="element-style-italic"
                  checked={italic} onChange={(e) => handleChangeStyle(e.target.checked, "italic")}
                />
                  <label for="element-style-italic">
                    <i className="material-icons">format_italic</i>
                  </label>
                <input
                  type="checkbox"
                  id="element-style-underline"
                  checked={underline} onChange={(e) => handleChangeStyle(e.target.checked, "underline")}
                />
                  <label for="element-style-underline">
                    <i className="material-icons">format_underline</i>
                  </label>
                <input
                  type="checkbox"
                  id="element-style-line-through"
                  checked={strikeout} onChange={(e) => handleChangeStyle(e.target.checked, "strikeout")}
                />
                  <label for="element-style-line-through">
                    <i className="material-icons">format_clear</i>
                  </label> */}
                <div>
                  <button className="style-action-button" onMouseDown={evt => {
                    evt.preventDefault();
                    document.execCommand("bold", false);
                  }}>
                    <i className="material-icons">format_bold</i>
                  </button>
                  <button className="style-action-button" onMouseDown={evt => {
                    evt.preventDefault();
                    document.execCommand("italic", false);
                  }}>
                    <i className="material-icons">format_italic</i>
                  </button>
                </div>
                <div>
                  <button className="style-action-button" onMouseDown={evt => {
                    evt.preventDefault();
                    document.execCommand("underline", false);
                  }}>
                    <i className="material-icons">format_underline</i>
                  </button>
                  <button className="style-action-button" onMouseDown={evt => {
                    evt.preventDefault();
                    document.execCommand("strikeThrough", false);
                  }}>
                    <i className="material-icons">format_clear</i>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="property-form">
            <div className="property-form-header">
              <h4>Link</h4>
              <span>Submint your link for action</span>
              <div className="two-column">
                <input value={textLink} placeholder="https://example.com" onChange={(e) => setTextLink(e.target.value)}/>
                <div className='property-font-style'>
                  <button
                    disabled={textLink === null}
                    className="style-action-button" 
                    onMouseDown={evt => {
                      evt.preventDefault();
                      document.execCommand("createLink", false, textLink);
                    }}
                  >
                    <i className="material-icons">link</i>
                  </button>
                  <button
                    className="style-action-button" 
                    onMouseDown={evt => {
                      evt.preventDefault();
                      document.execCommand("unlink", false, false);
                    }}
                  >
                    <i className="material-icons">link_off</i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      }
    </React.Fragment>
  )
};

export default Font;
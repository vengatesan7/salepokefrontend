import React, { useState, useContext, useEffect } from "react";
import { ChromePicker } from "react-color";
import Slider from "react-rangeslider";
import _get from "lodash.get";
import classnames from "classnames";
import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";
import ImageUpload from "../../../../../../components/image-upload";
import Switch from "react-switch";

const Background = ({ elementId }) => {
  const {
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);

  const [elementData, setElementData] = useState(null);
  const [colorPicker, popup] = useState(false);
  const [thisColor, pickedColor] = useState(null);
  //const [opacity, handleOpacity] = useState(50)
  const [importPopup, setImportPopup] = useState(false);
  const [backgroundImage, setBackgroundImage] = useState(null);
  const [backgroundImagePosition, setBackgroundImagePosition] = useState(null);
  const [backgroundImageMoreOptions, setBackgroundImageMoreOptions] = useState(
    false
  );
  const [backgroundRepeat, setBackgroundRepeat] = useState(null);
  const [backgroundImageSize, setBackgroundImageSize] = useState(null);

  const colorClick = selectedColor => {
    pickedColor({ background: selectedColor });
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            background: selectedColor
          }
        }
      }
    };
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  };
  const updateLayers = data => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  };
  const handleChanges = (value, name) => {
    popup(false);
    setImportPopup(false);
    if (name === "backgroundImage") {
      setBackgroundImage(value);
    }
    if (name === "backgroundImagePosition") {
      setBackgroundImagePosition(value);
    }
    if (name === "backgroundRepeat") {
      setBackgroundRepeat(value);
    }
    if (name === "backgroundImageSize") {
      setBackgroundImageSize(value);
    }
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            [name]: value
          }
        }
      }
    };
    return updateLayers(data);
  };

  useEffect(() => {
    setElementData(stateHistory.layers.entities.element[elementId]);
  }, [elementId]);
  useEffect(() => {
    setElementData(stateHistory.layers.entities.element[elementId]);
  }, [stateHistory]);
  useEffect(() => {
    pickedColor({ background: _get(elementData, "background", "") });
    setBackgroundImage(_get(elementData, "backgroundImage", null));
    setBackgroundImagePosition(
      _get(elementData, "backgroundImagePosition", null)
    );
    setBackgroundRepeat(_get(elementData, "backgroundRepeat", null));
    setBackgroundImageSize(_get(elementData, "backgroundImageSize", null));
  }, [elementData]);
  // useEffect(() => {
  //   console.log(elementData, backgroundRepeat);
  // }, [elementData]);

  return (
    <div className="property-form">
      <h4>Background</h4>
      <div className="two-column">
        <label>Color</label>
        <div>
          <div className="color-picker-swatch" onClick={() => popup(true)}>
            <div className="color-picker-color" style={thisColor}></div>
          </div>
          {colorPicker && (
            <div className="color-picker-popover">
              <div
                className="color-picker-cover"
                onClick={() => popup(false)}
              />
              <div className="color-picker-wrapper">
                <ChromePicker
                  color={thisColor.background}
                  onChange={e => colorClick(e.hex)}
                  disableAlpha
                />
                <button
                  className="color-picker-button"
                  onClick={() => popup(false)}
                >
                  Ok
                </button>
              </div>
            </div>
          )}
          <div
            className="color-picker-none"
            onClick={() => colorClick("transparent")}
          >
            <i className="material-icons">not_interested</i>
          </div>
        </div>
      </div>
      <div className="property-form">
        <button className="edit-form-button" onClick={() => setImportPopup(true)}>
          Upload Image
        </button>
        {backgroundImage && (
          <React.Fragment>
            <div className="image-viewer">
              <img src={backgroundImage} alt="URL is not valid" />
            </div>
            <button
              className="edit-form-button"
              onClick={() => handleChanges(null, "backgroundImage")}
            >
              Remove Image
            </button>
          </React.Fragment>
        )}
      </div>
      {backgroundImage && (
        <React.Fragment>
          <div className="two-column" style={{ marginTop: "15px" }}>
            <div className="properties-title">More Option</div>
            <div className="properties-value text-right">
              {/* <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span> */}
              <Switch
                checked={backgroundImageMoreOptions}
                onChange={e => setBackgroundImageMoreOptions(e)}
                onColor="#43da71"
                onHandleColor="#f8f8f8"
                offColor="#c5c5c5"
                offHandleColor="#f8f8f8"
                handleDiameter={12}
                height={16}
                width={30}
              />
            </div>
          </div>
        </React.Fragment>
      )}
      {importPopup && (
        <ImageUpload
          imageLink={e => handleChanges(e, "backgroundImage")}
          popupClose={() => setImportPopup(false)}
        />
      )}
      {backgroundImageMoreOptions && (
        <React.Fragment>
          <div className="two-column">
            <div className="properties-title" style={{ lineHeight: "34px" }}>
              Background Size
            </div>
            <div className="properties-value ">
              <select
                value={backgroundImageSize}
                onChange={e =>
                  handleChanges(e.target.value, "backgroundImageSize")
                }
                style={{ float: "right" }}
              >
                {backgroundImageSize === undefined && (
                  <option>Select Size </option>
                )}
                <option value="auto">Auto</option>
                <option value="contain">Contain</option>
                <option value="cover">Cover</option>
                <option value="10%">10%</option>
                <option value="20%">20%</option>
                <option value="30%">30%</option>
                <option value="40%">40%</option>
                <option value="50%">50%</option>
                <option value="60%">60%</option>
                <option value="70%">70%</option>
                <option value="80%">80%</option>
                <option value="90%">90%</option>
                <option value="100%">100%</option>
              </select>
            </div>
          </div>
          <div className="two-column">
            <div className="properties-title" style={{ lineHeight: "40px" }}>
              Background Position
            </div>
            <div className="properties-value">
              <div className="background-positioning">
                <div
                  className="background-positioning-wrapper"
                  style={{ float: "right" }}
                >
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "top left" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("top left", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "top center" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("top center", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "top right" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("top right", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "center left" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("center left", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "center" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("center", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "center right" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("center right", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "bottom left" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("bottom left", "backgroundImagePosition")
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "bottom center" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges(
                          "bottom center",
                          "backgroundImagePosition"
                        )
                      }
                    />
                  </div>
                  <div
                    className={classnames(
                      "background-positioning-field",
                      backgroundImagePosition === "bottom right" && "active"
                    )}
                  >
                    <button
                      className="background-positioning-button"
                      onClick={() =>
                        handleChanges("bottom right", "backgroundImagePosition")
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            {/* <div className="properties-title">Background Repeat</div> */}
            <div className="properties-value">
              <input
                type="checkbox"
                checked={
                  backgroundRepeat === undefined ||
                  backgroundRepeat === "repeat"
                }
                id="row-background-image-repeate"
                onChange={e =>
                  handleChanges(
                    e.target.checked ? "repeat" : "no-repeat",
                    "backgroundRepeat"
                  )
                }
              />
              <label
                for="row-background-image-repeate"
                style={{
                  fontWeight: "300",
                  fontStyle: "italic",
                  verticalAlign: "top",
                  marginLeft: "5px"
                }}
              >
                Repeat background image
              </label>
            </div>
          </div>
        </React.Fragment>
      )}
      {/* <div>
                <label className="">Opacity</label>
                <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                    <Slider min={0} max={100} step={10} value={opacity} orientation="horizontal" tooltip={false} onChange={(e) => opacityChange(e)} />
                    </div>
                    <div className="rangeslider-value">
                    <input type='number' step='10' value={opacity} onChange={(e) => opacityChange(e.target.value) } />
                    </div>
                </div>
            </div> */}
      {/* background-position: center;
background-repeat: no-repeat;
background-size: cover; */}
    </div>
  );
};

export default Background;

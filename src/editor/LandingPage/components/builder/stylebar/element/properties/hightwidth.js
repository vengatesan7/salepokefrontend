import React, { useContext, useState, useEffect } from 'react';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';
import SpinInputButton from '../../../../elements/spinButtonInput';

const HightWidth = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [height, setHeight] = useState(_get(stateHistory.layers.entities.element[elementId], 'height', 26));
    const [width, setWidth] = useState(_get(stateHistory.layers.entities.element[elementId], 'width', 90));

    useEffect(() => {
        setHeight(_get(stateHistory.layers.entities.element[elementId], 'height', 26));
        setWidth(_get(stateHistory.layers.entities.element[elementId], 'width', 90));
    }, [stateHistory]);

    const handleChange = (value, name) => {
        name === 'width' ? setWidth(value) : setHeight(value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }
    return (
        <div className="property-form">
            <h4>Size</h4>
            <div className="two-column">
                <label>Width</label>
                {/* <input type="number" step="1" min="0" onChange={(v) => handleChange(v.target.value, 'width')} value={width} /> */}
                <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={1} max={100} step={1}
                        value={width || 1}
                        orientation="horizontal"
                        tooltip={false}
                        onChange={(e) => handleChange(e, 'width')}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {width || 1}
                    </div>
                </div>
                {/* <SpinInputButton max={1000} min={1} step={1} value={width} callBack={(v) => handleChange(v, 'width')} inputBoxDisabled /> */}
            </div>
            {/* <div className="two-column">
                <label>Height</label>
                <input type="number" step="1" min="0" onChange={(v) => handleChange(v.target.value, 'height')} value={height} />
                <SpinInputButton max={1000} min={1} step={1} value={height} callBack={(v) => handleChange(v, 'height')} inputBoxDisabled />
            </div> */}
        </div>
    )
}

export default HightWidth;
import React, { useState, useContext, useEffect } from "react";
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import classnames from "classnames";
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Line = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    const [colorPopup, setColorPopup] = useState(false);
    const [color, setColor] = useState({ background: "#000000" });
    const [style, setStyle] = useState("solid");
    const [size, setSize] = useState("1");
    const getData = () => {
        // setColorPopup(stateHistory.layers.entities.element[elementId]);
        setColor({ background: _get(stateHistory.layers.entities.element[elementId], "color", "#000000") });
        setStyle(_get(stateHistory.layers.entities.element[elementId], "style", "solid"));
    }

    useEffect(() => {
        getData();
    }, [])

    const updateElement = (data) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }

    const colorClick = (value, name) => {
        name === "color" && setColor({ background: value })
    }

    const handleLineUpdate = (value, name) => {
        setColorPopup(false);
        name === "style" && setStyle(value);
        name === 'size' && setSize(value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return updateElement(data);
    }

    return (
        <div className="property-form">
            <h4>Line</h4>
            <div className="two-column">
                <label>Style</label>
                <div>
                    <select value={style} onChange={(e) => handleLineUpdate(e.target.value, "style")}>
                        <option value="solid">Solid</option>
                        <option value="dashed">Dashed</option>
                        <option value="double">Double</option>
                        {/* <option value="inset">Inset</option>
                        <option value="outset">Outset</option>
                        <option value="ridge">Ridge</option>
                        <option value="groove">Groove</option> */}
                    </select>
                </div>
            </div>
            <div className="two-column">
                <label>Color</label>
                <div>
                    <div className="color-picker-swatch" onClick={() => setColorPopup(true)}>
                        <div className="color-picker-color" style={color}></div>
                    </div>
                    {colorPopup && <div className="color-picker-popover">
                        <div className='color-picker-cover' onClick={() => setColorPopup(false)} />
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={color.background} onChange={(e) => colorClick(e.hex, "color")} disableAlpha />
                            <button className='color-picker-button' onClick={() => handleLineUpdate(color.background, "color")}>Ok</button>
                        </div>
                    </div>}
                </div>
            </div>
            <div className="two-column">
                <label>Size</label>
                <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={1} max={10} step={1}
                            value={size || 1}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handleLineUpdate(e, 'size')}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {size || 1}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Line;
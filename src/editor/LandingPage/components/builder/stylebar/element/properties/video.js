import React, { useState, useContext, useEffect } from "react";
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Video = ({ elementId }) => {
    const {
        state,
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [link, setLink] = useState(_get(stateHistory.layers.entities.element[elementId], 'path', ''));
    const elementData = () => {
        setLink(_get(stateHistory.layers.entities.element[elementId], 'path', ''));
    }
    useEffect(() => {
        elementData();
    }, [elementId]);
    useEffect(() => {
        elementData();
    }, [stateHistory])
    const handleChange = (value, name) => {
        setLink(value)
        const newData = {
            ...state,
            layers: {
                ...state.layers,
                entities: {
                    ...state.layers.entities,
                    element: {
                        ...state.layers.entities.element,
                        [elementId]: {
                            ...state.layers.entities.element[elementId],
                            [name]: value
                        }
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_STATE,
            state: newData
        })
    }
    return (
        <div className="property-form">
            <div className="property-form-header">
                <h4>Video</h4>
            </div>
            <div>
                <label>Embedded HTML Code</label>
                <div>
                    <textarea style={{ width: "100%", minHeight: "150px" }} value={link} onChange={(e) => handleChange(e.target.value, 'path')} />
                </div>
            </div>
        </div>
    );
}

export default Video;
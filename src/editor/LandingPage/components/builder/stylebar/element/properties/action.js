import React, { useContext, useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import cookie from "react-cookies";
import _get from 'lodash.get';

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';
import { PostData } from '../../../../../../../services/PostData';

const ButtonAction = ({ elementId }) => {
    const {
        state,
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    const [actionType, setActionType] = useState("default");
    const [actionURL, setActionURL] = useState(null);
    const [actionPopupId, setActionPopupId] = useState(null);
    const [actionLandingPageId, setActionLandingPageId] = useState(null);
    const [actionSectionId, setActionSectionId] = useState(null);
    const [actionStepId, setActionStepId] = useState(null);
    const [popupList, setPopupList] = useState([]);
    const [landinPageList, setLandinPageList] = useState([]);
    const [funnelStepList, setFunnelStepList] = useState([]);
    const [sectionList, setSectionList] = useState([]);
    const editortype = localStorage.getItem('editortype');
    const [getFunnelsteps, setFunnelsteps] = useState([]);
    const getData = () => {
        setActionType(_get(stateHistory.layers.entities.element[elementId], 'actionType', 'default'));
        setActionURL(_get(stateHistory.layers.entities.element[elementId], 'actionURL', null));
        setActionPopupId(_get(stateHistory.layers.entities.element[elementId], 'actionPopupId', null));
        setActionLandingPageId(_get(stateHistory.layers.entities.element[elementId], 'actionLandingPageId', null));
        setActionStepId(_get(stateHistory.layers.entities.element[elementId], 'actionStepId', null));
        setActionSectionId(_get(stateHistory.layers.entities.element[elementId], 'actionSectionId', null));
        const sectionList = [];
        state.layers.result.sections.map(sectionId => {
            const sectionName = _get(state.layers.entities.section[sectionId], 'sectionsData.name', '');
            sectionList.push({
                id: sectionId,
                name: sectionName
            });
        });
        setSectionList(sectionList);
    }

    useEffect(() => {
        PostData('ms4', 'popuplistforeditor').then(response => {
            if (response !== "Invalid") {
                if (response.status === "success") {
                    setPopupList(response.popupmaster);
                }
            }
        });
        if (editortype === "landingpage") {
            PostData('ms4', 'landingpagelistforformaction').then(response => {
                if (response !== "Invalid") {
                    if (response.status === "success") {
                        setLandinPageList(response.landingpagedata);
                    }
                }
            });
        } else if (editortype === "funnels") {
            const stepID = {
                "funnel_step_id": cookie.load("stepId")
            }          
            PostData('ms3', 'funnelstepslist', stepID).then(response => {
                if (response !== "Invalid") {
                    if (response.status === "success") {
                        setFunnelStepList(response.funnelsteplist);
                    }
                }
            });
            PostData('ms3', 'funnelstepseditor', stepID).then(response => {
                if (response !== "Invalid") {
                    if (response.status === "success") {
                        setFunnelsteps(response.getfunnelsteps);
                    }
                }
            });
        }
    }, []);

    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [stateHistory]);

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handleButtonAction = (value, name) => {

        if(name === "actionStepId" ){
            setActionType('funnels');
            setActionURL(value);
            setActionStepId(value)
            const updateOnLoadPopup = {
                ...layers,
                entities: {
                    ...layers.entities,
                    element: {
                        ...layers.entities.element,
                        [elementId]: {
                            ...layers.entities.element[elementId],
                            ['actionURL']: value
                        }
                    }
                }
            }
            return updatelayer(updateOnLoadPopup);

            
        }else{
           // console.log('gfhfg')
           name === 'actionType' && setActionType(value);
            name === 'actionURL' && setActionURL(value);
            name === 'actionPopupId' && setActionPopupId(value);
            name === 'actionLandingPageId' && setActionLandingPageId(value);
            name === 'actionSectionId' && setActionSectionId(value);
            const updateOnLoadPopup = {
                ...layers,
                entities: {
                    ...layers.entities,
                    element: {
                        ...layers.entities.element,
                        [elementId]: {
                            ...layers.entities.element[elementId],
                            [name]: value
                        }
                    }
                }
            }
            return updatelayer(updateOnLoadPopup);
        }
    }

    return (
        <div className="property-form">
            <div>
                <label>Action</label>
                <div>
                    <select value={actionType} onChange={(e) => handleButtonAction(e.target.value, 'actionType')} style={{ width: "100%", padding: "10px" }}>
                        <option value="default">No action</option>
                        {editortype === "funnels" && <option value="funnels">Funnels</option>}
                        {editortype === "landingpage" && <option value="landingpage">Landing Page</option>}
                        <option value="url">External URL</option>
                        <option value="popup">Popup</option>
                        {state.layers.entities.element[state.activeElement].elementType === "BUTTON" && <option value="section">Section</option>}
                    </select>
                </div>
            </div>
            {actionType === "section" && <div>
                {sectionList.length !== 0 && <React.Fragment>
                    <label style={{ marginTop: "10px" }}>Select Your Internal Page Section</label>
                    <select
                        onChange={(e) => handleButtonAction(e.target.value, 'actionSectionId')}
                        value={actionSectionId}
                        style={{ width: "100%", padding: "10px" }}
                    >
                        {actionSectionId === null && <option value={null}>Select your section</option>}
                        {sectionList.map(data => (
                            <option value={data.id}>{data.name}</option>
                        ))}
                    </select>
                </React.Fragment>}
            </div>}
            {actionType === "landingpage" && <div>
                {landinPageList.length !== 0 && <React.Fragment>
                    <label style={{ marginTop: "10px" }}>Select Your Landing Page</label>
                    <select
                        onChange={(e) => handleButtonAction(e.target.value, 'actionLandingPageId')}
                        value={actionLandingPageId}
                        style={{ width: "100%", padding: "10px" }}
                    >
                        {actionLandingPageId === null && <option value={null}>Select your Landing Page</option>}
                        {landinPageList.map(data => (
                            <option value={data.landing_page_id}>{data.landing_page_name}</option>
                        ))}
                    </select>
                </React.Fragment>}
            </div>}
            {actionType === "funnels" && <div>
                {getFunnelsteps.length !== 0 && <React.Fragment>
                    <label style={{ marginTop: "10px" }}>Select Your Funnel Step</label>
                    <select
                        onChange={(e) => handleButtonAction(e.target.value, 'actionStepId')}
                        value={actionStepId}
                        style={{ width: "100%", padding: "10px" }}
                    >
                        {actionStepId === null && <option value={null}>Select Your Funnel Step</option>}
                        {getFunnelsteps.map(data => (
                            <option value={data.funnel_step_url}>{data.funnel_step_name}</option>
                        ))}
                    </select>
                </React.Fragment>}
            </div>}
            {actionType === "url" && <div>
                <label style={{ marginTop: "10px" }}>Enter External Action URL</label>
                <div>
                    <input 
                        type="text"
                        value={actionURL}
                        onChange={(e) => handleButtonAction(e.target.value, 'actionURL')}
                        style={{ width: "100%", padding: "10px" }}
                        placeholder="https://example.com/"
                    />
                </div>
            </div>}
            {actionType === "popup" && <div>
                {popupList.length !== 0 ? <React.Fragment>
                    <label style={{ marginTop: "10px" }}>Select Your Popup</label>
                    <select
                        onChange={(e) => handleButtonAction(e.target.value, 'actionPopupId')}
                        value={actionPopupId}
                        style={{ width: "100%", padding: "10px" }}
                    >
                        <option value={null}>Select your popup</option>
                        {popupList.map(data => (
                            <option value={data.pop_up_id}>{data.pop_up_name}</option>
                        ))}
                    </select>
                </React.Fragment> : <span>No data. <Link to="/popuppopup/addnewpopup" >Please create popup</Link></span>}
            </div>}
        </div>
    );
}

export default ButtonAction;
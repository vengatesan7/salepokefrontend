import React, { useState, useEffect, useContext } from "react";
import Switch from "react-switch";
import _get from 'lodash.get';

import SpinButtonInput from "../../../../elements/spinButtonInput";
import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Padding = ({ elementId }) => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [styles, setStyles] = useState(null);
    const [switchButton, switchHandle] = useState(true);

    const elementData = () => {
        const elementData = stateHistory.layers.entities.element[elementId];
        const oldData = {
            Top: _get(elementData, 'paddingTop', 0),
            Right: _get(elementData, 'paddingRight', 0),
            Bottom: _get(elementData, 'paddingBottom', 0),
            Left: _get(elementData, 'paddingLeft', 0),
        }
        const desktopData = {
            Top: _get(elementData.styles, 'paddingTop', oldData.Top),
            Right: _get(elementData.styles, 'paddingRight', oldData.Right),
            Bottom: _get(elementData.styles, 'paddingBottom', oldData.Bottom),
            Left: _get(elementData.styles, 'paddingLeft', oldData.Left),
        }
        switch (state.deviceType) {
            case "phone":
                return setStyles({
                    paddingTop: _get(elementData.phoneStyles, 'paddingTop', desktopData.Top),
                    paddingRight: _get(elementData.phoneStyles, 'paddingRight', desktopData.Right),
                    paddingBottom: _get(elementData.phoneStyles, 'paddingBottom', desktopData.Bottom),
                    paddingLeft: _get(elementData.phoneStyles, 'paddingLeft', desktopData.Left),
                });
            case "tablet":
                return setStyles({
                    paddingTop: _get(elementData.tabletStyles, 'paddingTop', desktopData.Top),
                    paddingRight: _get(elementData.tabletStyles, 'paddingRight', desktopData.Right),
                    paddingBottom: _get(elementData.tabletStyles, 'paddingBottom', desktopData.Bottom),
                    paddingLeft: _get(elementData.tabletStyles, 'paddingLeft', desktopData.Left),
                });
            default:
                return setStyles({
                    paddingTop: _get(elementData.styles, 'paddingTop', desktopData.Top),
                    paddingRight: _get(elementData.styles, 'paddingRight', desktopData.Right),
                    paddingBottom: _get(elementData.styles, 'paddingBottom', desktopData.Bottom),
                    paddingLeft: _get(elementData.styles, 'paddingLeft', desktopData.Left),
                });

        }
        // setStyles({
        //     paddingTop: elementData.paddingTop === undefined ? 0 : elementData.paddingTop,
        //     paddingRight: elementData.paddingRight === undefined ? 0 : elementData.paddingRight,
        //     paddingBottom: elementData.paddingBottom === undefined ? 0 : elementData.paddingBottom,
        //     paddingLeft: elementData.paddingLeft === undefined ? 0 : elementData.paddingLeft
        // });
    }
    useEffect(() => {
        elementData();
    }, [elementId]);
    useEffect(() => {
        elementData();
    }, [stateHistory]);
    useEffect(() => {
        elementData();
    }, [state.deviceType]);

    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };

    const handleAlChange = (value) => {
        setStyles({
            ...styles,
            paddingTop: value,
            paddingRight: value,
            paddingBottom: value,
            paddingLeft: value
        });
        switch (state.deviceType) {
            case "phone":
                const newPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                phoneStyles: {
                                    ...layers.entities.element[elementId].phoneStyles,
                                    paddingTop: value,
                                    paddingRight: value,
                                    paddingBottom: value,
                                    paddingLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newPhoneData);
            case "tablet":
                const newTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                tabletStyles: {
                                    ...layers.entities.element[elementId].tabletStyles,
                                    paddingTop: value,
                                    paddingRight: value,
                                    paddingBottom: value,
                                    paddingLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newTabletData);
            default:
                const newData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                styles: {
                                    ...layers.entities.element[elementId].styles,
                                    paddingTop: value,
                                    paddingRight: value,
                                    paddingBottom: value,
                                    paddingLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newData);
        }
        // const newData = {
        //     ...layers,
        //     entities: {
        //         ...layers.entities,
        //         element: {
        //             ...layers.entities.element,
        //             [elementId]: {
        //                 ...layers.entities.element[elementId],
        //                 paddingTop: value,
        //                 paddingRight: value,
        //                 paddingBottom: value,
        //                 paddingLeft: value,
        //             }
        //         }
        //     }
        // }
        // return updateLayers(newData);
    }

    const handleChange = (value, name) => {
        setStyles({
            ...styles,
            [name]: value
        });
        switch (state.deviceType) {
            case "phone":
                const newPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                phoneStyles: {
                                    ...layers.entities.element[elementId].phoneStyles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newPhoneData);
            case "tablet":
                const newTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                tabletStyles: {
                                    ...layers.entities.element[elementId].tabletStyles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newTabletData);
            default:
                const newData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                styles: {
                                    ...layers.entities.element[elementId].styles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newData);
        }
        // const newData = {
        //     ...layers,
        //     entities: {
        //         ...layers.entities,
        //         element: {
        //             ...layers.entities.element,
        //             [elementId]: {
        //                 ...layers.entities.element[elementId],
        //                 [name]: value,
        //             }
        //         }
        //     }
        // }
        // return updateLayers(newData);
    }

    return (
        styles !== null &&
        <div className="property-form">
            <div className="property-form-header">
                <h4>Padding</h4>
                <div className="property-switch">
                    <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                    <Switch checked={!switchButton} onChange={(e) => switchHandle(!e)}
                        onColor="#43da71" onHandleColor="#f8f8f8"
                        offColor="#c5c5c5" offHandleColor="#f8f8f8"
                        handleDiameter={12} height={16} width={30} />
                </div>
            </div>
            {switchButton ? <div className="all">
                <div className="two-column rangeslider-container">
                    <label>
                        All
                    </label>
                    <div>
                        <div style={{ float: "right" }}>
                            <SpinButtonInput
                                max={999}
                                min={0}
                                step={1}
                                value={styles.paddingTop}
                                callBack={(v) => handleAlChange(v)}
                                // inputBoxDisabled
                            />
                        </div>
                    </div>
                </div>
            </div> : <div className="each two-column">
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Top
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={styles.paddingTop}
                                    callBack={(v) => handleChange(v, 'paddingTop')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Right
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={styles.paddingRight}
                                    callBack={(v) => handleChange(v, 'paddingRight')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Bottom
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={styles.paddingBottom}
                                    callBack={(v) => handleChange(v, 'paddingBottom')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Left
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={0}
                                    step={1}
                                    value={styles.paddingLeft}
                                    callBack={(v) => handleChange(v, 'paddingLeft')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                </div>}
        </div>
    );
}

export default Padding;
import React, { useState, useContext, useEffect } from "react";
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const List = ({ elementId }) => {
  const {
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);

  const [elementData, setElementData] = useState(null);

  const getData = () => {
    setElementData(stateHistory.layers.entities.element[elementId]);
  }
  useEffect(() => {
    getData();
  }, [stateHistory])
  useEffect(() => {
    getData();
  }, [elementId])
  
  const updateElement = (data) => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  }
  
  return (
    <React.Fragment>
      {elementData !== null &&
        <div className="property-form">
          <div className="property-form-header">
            <h4>Listing Style</h4>
          </div>
          <div className="two-column">
            <label>List Type</label>
            <div className='property-font-style'>
              <div>
                <button className="style-action-button" onMouseDown={evt => {
                  evt.preventDefault();
                  document.execCommand("insertUnorderedList", false);
                }}>
                  <i className="material-icons">list</i>
                </button>
                <button className="style-action-button" onMouseDown={evt => {
                  evt.preventDefault();
                  document.execCommand("insertOrderedList", false);
                }}>
                  <i className="material-icons">format_list_numbered</i>
                </button>
              </div>
            </div>
          </div>
        </div>
      }
    </React.Fragment>
  )
};

export default List;
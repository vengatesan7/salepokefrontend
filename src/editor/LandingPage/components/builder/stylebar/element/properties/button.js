import React, { useState, useEffect, useContext } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';

const Button = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [content, setContent] = useState(null);
    const [bold, setBold] = useState(false);
    const [italic, setItalic] = useState(false);
    const [underline, setUnderline] = useState(false);
    const [strikeout, setStrikeout] = useState(false);

    const [fontColor, setFontColor] = useState("#000000");
    const [colorPopup, setColorPopup] = useState(false);
    const [fontSize, setFontSize] = useState(null);
    const [fontSpace, setFontSpace] = useState(null);
    const [fontFamily, setFontFamily] = useState(null);
    const [width, setWidth] = useState(null);

    const getData = () => {
        setContent(_get(stateHistory.layers.entities.element[elementId], 'contents', ''));
        setBold(_get(stateHistory.layers.entities.element[elementId], 'bold', null) === "bold");
        setItalic(_get(stateHistory.layers.entities.element[elementId], 'italic', null) === "italic");
        setUnderline(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "underline");
        setStrikeout(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "line-through");
        setFontFamily(_get(stateHistory.layers.entities.element[elementId], 'fontFamily', ""));
        setWidth(_get(stateHistory.layers.entities.element[elementId], 'width', 50));
        setFontColor(_get(stateHistory.layers.entities.element[elementId], 'fontColor', '#000000'));
        setFontSize(_get(stateHistory.layers.entities.element[elementId], 'fontSize', 1));
        setFontSpace(_get(stateHistory.layers.entities.element[elementId], 'lineHeight', 1));
    }

    useEffect(() => {
        getData();
    }, [stateHistory]);

    useEffect(() => {
        getData();
    }, [elementId]);
    
    const updateElement = (data) => {
        return dispatch({
        type: actions.UPDATE_LAYER,
        payload: data
        });
    }

    const handleOnChange = event => {
        setContent(event.target.value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        contents: event.target.value
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }

    const handleChange = (value, name) => {
        setColorPopup(false);
        name === "fontSize" && setFontSize(value);
        name === "lineHeight" && setFontSpace(value);
        const data = {
          ...layers,
          entities: {
            ...layers.entities,
            element: {
              ...layers.entities.element,
              [elementId]: {
                ...layers.entities.element[elementId],
                [name]: value
              }
            }
          }
        }
        return updateElement(data);
    }

    const handleChangeStyle = (value, name) => {
        name === "bold" && setBold(value);
        name === "italic" && setItalic(value);
        if (name === "underline") {
          setUnderline(value);
          // setStrikeout(!value);
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              element: {
                ...layers.entities.element,
                [elementId]: {
                  ...layers.entities.element[elementId],
                  textDecoration: value === true ? "underline" : null
                }
              }
            }
          }
          return updateElement(data);
        } else if (name === "strikeout") {
          setStrikeout(value);
          // setUnderline(!value);
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              element: {
                ...layers.entities.element,
                [elementId]: {
                  ...layers.entities.element[elementId],
                  textDecoration: value === true ? "line-through" : null
                }
              }
            }
          }
          return updateElement(data);
        } else {
          // name === "underline" && setUnderline(value);
          // name === "strikeout" && setStrikeout(value);
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              element: {
                ...layers.entities.element,
                [elementId]: {
                  ...layers.entities.element[elementId],
                  [name]: value === true ? name : null
                }
              }
            }
          }
          return updateElement(data);
        }
    }

    return (
        <div className="property-form">
            <h4>Button</h4>
            <div className="two-column">
                <label>Text</label>
                <div>
                    <input
                        style={{ padding: "5px" }}
                        type="text"
                        value={content}
                        onChange={e => handleOnChange(e)}
                    />
                </div>
            </div>
            <div className="two-column">
                <label>Font Family</label>
                <div>
                    <select value={fontFamily} onChange={(e) => handleChange(e.target.value, 'fontFamily')}>
                    <option value="">Select Font Family</option>
                    <option value="Roboto">Roboto</option>
                    <option value="Open Sans">Open Sans</option>
                    <option value="Ubuntu">Ubuntu</option>
                    <option value="Josefin Sans">Josefin Sans</option>
                    <option value="Tajawal">Tajawal</option>
                    <option value="Raleway">Raleway</option>
                    <option value="Oxygen">Oxygen</option>
                    <option value="Maven Pro">Maven Pro</option>
                    <option value="Catamaran">Catamaran</option>
                    <option value="Orbitron">Orbitron</option>
                    <option value="Darker Grotesque">Darker Grotesque</option>
                    <option value="Montserrat Alternates">Montserrat Alternates</option>
                    <option value="Nunito Sans">Nunito Sans</option>
                                <option value="Noto Serif"> Noto Serif </option>
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                                <option value="Lato"> Lato </option>
                                <option value="Roboto Slab"> Roboto Slab </option>
                                <option value="Montserrat"> Montserrat </option> 
                                <option value="Anton"> Anton </option>
                                <option value="Source Sans Pro">Source Sans Pro  </option>
                                <option value="Roboto Condensed"> Roboto Condensed </option>
                                <option value="Oswald">Oswald</option>
                                <option value="Enemy"> Enemy </option>
                    </select>
                </div>
            </div>
            <div className="two-column">
              <label>Font Size</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={1} max={100} step={1}
                    value={fontSize}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'fontSize')}
                  />
                </div>
                <div className="rangeslider-value">
                  {fontSize}
                </div>
              </div>
            </div>
            <div className="two-column">
              <label>Line Height</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={1} max={100} step={1}
                    value={fontSpace}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'lineHeight')}
                  />
                </div>
                <div className="rangeslider-value">
                  {fontSpace}
                </div>
              </div>
            </div>
            <div className="two-column">
                <label>Style</label>
                <div className='property-font-style'>
                    <div>
                        <input
                            type="checkbox"
                            id="element-style-bold"
                            checked={bold} onChange={(e) => handleChangeStyle(e.target.checked, "bold")}
                        />
                        <label for="element-style-bold">
                            <i className="material-icons">format_bold</i>
                        </label>
                        <input
                            type="checkbox"
                            id="element-style-italic"
                            checked={italic} onChange={(e) => handleChangeStyle(e.target.checked, "italic")}
                        />
                        <label for="element-style-italic">
                            <i className="material-icons">format_italic</i>
                        </label>
                    </div>
                    <div>
                        <input
                            type="checkbox"
                            id="element-style-underline"
                            checked={underline} onChange={(e) => handleChangeStyle(e.target.checked, "underline")}
                        />
                        <label for="element-style-underline">
                            <i className="material-icons">format_underline</i>
                        </label>
                        <input
                            type="checkbox"
                            id="element-style-line-through"
                            checked={strikeout} onChange={(e) => handleChangeStyle(e.target.checked, "strikeout")}
                        />
                        <label for="element-style-line-through">
                            <i className="material-icons">format_clear</i>
                        </label>
                    </div>
                </div>
            </div>
            <div className="two-column">
              <label>Font Color</label>
              <div>
                <div className="color-picker-swatch" onClick={() => setColorPopup(true)} style={{ float: "none" }}>
                  <div className="color-picker-color" style={{ backgroundColor: fontColor }}></div>
                </div>
                {colorPopup && <div className="color-picker-popover">
                  <div className='color-picker-cover' onClick={() => handleChange(fontColor, 'fontColor')} />
                  <div className='color-picker-wrapper'>
                    <ChromePicker color={fontColor} onChange={(e) => setFontColor(e.hex)} disableAlpha />
                    <button className='color-picker-button' onClick={() => handleChange(fontColor, 'fontColor')}>Ok</button>
                  </div>
                </div>}
              </div>
            </div>
            {/* <div className="two-column">
              <label>Width</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={1} max={100} step={1}
                    value={width || 1}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'width')}
                  />
                </div>
                <div className="rangeslider-value">
                  {width || 1}
                </div>
              </div>
            </div> */}
        </div>
    );
}

export default Button;
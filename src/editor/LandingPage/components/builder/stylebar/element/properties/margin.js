import React, { useState, useEffect, useContext } from "react";
import Slider from 'react-rangeslider';
import Switch from "react-switch";
import _get from 'lodash.get';

import SpinButtonInput from "../../../../elements/spinButtonInput";
import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Margin = ({ elementId }) => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [styles, setStyles] = useState(null);
    const [switchButton, switchHandle] = useState(true);

    const elementData = () => {
        const elementData = stateHistory.layers.entities.element[elementId];
        const oldData = {
            Top: _get(elementData, 'marginTop', 0),
            Right: _get(elementData, 'marginRight', 0),
            Bottom: _get(elementData, 'marginBottom', 0),
            Left: _get(elementData, 'marginLeft', 0),
        }
        const desktopData = {
            Top: _get(elementData.styles, 'marginTop', oldData.Top),
            Right: _get(elementData.styles, 'marginRight', oldData.Right),
            Bottom: _get(elementData.styles, 'marginBottom', oldData.Bottom),
            Left: _get(elementData.styles, 'marginLeft', oldData.Left),
        }
        switch (state.deviceType) {
            case "phone":
                return setStyles({
                    marginTop: _get(elementData.phoneStyles, 'marginTop', desktopData.Top),
                    marginRight: _get(elementData.phoneStyles, 'marginRight', desktopData.Right),
                    marginBottom: _get(elementData.phoneStyles, 'marginBottom', desktopData.Bottom),
                    marginLeft: _get(elementData.phoneStyles, 'marginLeft', desktopData.Left),
                });
            case "tablet":
                return setStyles({
                    marginTop: _get(elementData.tabletStyles, 'marginTop', desktopData.Top),
                    marginRight: _get(elementData.tabletStyles, 'marginRight', desktopData.Right),
                    marginBottom: _get(elementData.tabletStyles, 'marginBottom', desktopData.Bottom),
                    marginLeft: _get(elementData.tabletStyles, 'marginLeft', desktopData.Left),
                });
            default:
                return setStyles({
                    marginTop: _get(elementData.styles, 'marginTop', desktopData.Top),
                    marginRight: _get(elementData.styles, 'marginRight', desktopData.Right),
                    marginBottom: _get(elementData.styles, 'marginBottom', desktopData.Bottom),
                    marginLeft: _get(elementData.styles, 'marginLeft', desktopData.Left),
                });

        }
    }
    
    useEffect(() => {
        elementData();
    }, [elementId]);
    useEffect(() => {
        elementData();
    }, [stateHistory]);
    useEffect(() => {
        elementData();
    }, [state.deviceType]);

    const handleSwitch = (value) => {
        switchHandle(value);
    }

    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };
    const handleChange = (value, name) => {
        setStyles({
            ...styles,
            [name]: value
        });
        switch (state.deviceType) {
            case "phone":
                const newPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                phoneStyles: {
                                    ...layers.entities.element[elementId].phoneStyles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newPhoneData);
            case "tablet":
                const newTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                tabletStyles: {
                                    ...layers.entities.element[elementId].tabletStyles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newTabletData);
            default:
                const newData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                styles: {
                                    ...layers.entities.element[elementId].styles,
                                    [name]: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newData);
        }
       
    }

    const handleAllChange = (value) => {
        setStyles({
            ...styles,
            marginTop: value,
            marginRight: value,
            marginBottom: value,
            marginLeft: value
        });
        switch (state.deviceType) {
            case "phone":
                const newPhoneData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                phoneStyles: {
                                    ...layers.entities.element[elementId].phoneStyles,
                                    marginTop: value,
                                    marginRight: value,
                                    marginBottom: value,
                                    marginLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newPhoneData);
            case "tablet":
                const newTabletData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                tabletStyles: {
                                    ...layers.entities.element[elementId].tabletStyles,
                                    marginTop: value,
                                    marginRight: value,
                                    marginBottom: value,
                                    marginLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newTabletData);
            default:
                const newData = {
                    ...layers,
                    entities: {
                        ...layers.entities,
                        element: {
                            ...layers.entities.element,
                            [elementId]: {
                                ...layers.entities.element[elementId],
                                styles: {
                                    ...layers.entities.element[elementId].styles,
                                    marginTop: value,
                                    marginRight: value,
                                    marginBottom: value,
                                    marginLeft: value,
                                }
                            }
                        }
                    }
                }
                return updateLayers(newData);
        }
    }

    return (
        styles !== null &&
        <div className="property-form">
            <div className="property-form-header">
                <h4>Margin</h4>
                <div className="property-switch">
                    <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                    <Switch checked={!switchButton} onChange={(e) => handleSwitch(!e)}
                        onColor="#43da71" onHandleColor="#f8f8f8"
                        offColor="#c5c5c5" offHandleColor="#f8f8f8"
                        handleDiameter={12} height={16} width={30} />
                </div>
            </div>
            {switchButton ? <div className="all">
                <div className="two-column rangeslider-container">
                    <label>
                        All
                    </label>
                    <div>
                        <div style={{ float: "right" }}>
                            <SpinButtonInput
                                max={999}
                                min={-999}
                                step={1}
                                value={styles.marginTop}
                                callBack={(v) => handleAllChange(v)}
                                // inputBoxDisabled
                            />
                        </div>
                    </div>
                </div>
            </div> : <div className="each two-column">
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Top
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={styles.marginTop}
                                    callBack={(v) => handleChange(v, 'marginTop')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Right
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={styles.marginRight}
                                    callBack={(v) => handleChange(v, 'marginRight')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Bottom
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={styles.marginBottom}
                                    callBack={(v) => handleChange(v, 'marginBottom')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rangeslider-container">
                        <label style={{ width: "100%" }}>
                            Left
                        </label>
                        <div style={{ display: "inline-block", width: "100%" }}>
                            <div style={{ float: "left" }}>
                                <SpinButtonInput
                                    max={999}
                                    min={-999}
                                    step={1}
                                    value={styles.marginLeft}
                                    callBack={(v) => handleChange(v, 'marginLeft')}
                                    // inputBoxDisabled
                                />
                            </div>
                        </div>
                    </div>
                </div>}
        </div>
    );
}

export default Margin;
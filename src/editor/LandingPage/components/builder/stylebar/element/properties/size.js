import React, { useContext, useState, useEffect } from 'react';
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';

const Size = ({ elementId }) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [height, setHeight] = useState(null);
    useEffect(() => {
        setHeight(_get(stateHistory.layers.entities.element[elementId], 'height', 0));
    }, [elementId]);
    useEffect(() => {
        setHeight(_get(stateHistory.layers.entities.element[elementId], 'height', 0));
    }, [stateHistory]);
    const handleChange = (value, name) => {
        setHeight(value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }
    return (
        <div className="property-form">
            <h4>Size</h4>
            <div className="two-column">
                <label>Height</label>
                <input type="number" value={height || 100} min="100" onChange={e => handleChange(Number(e.target.value), 'height')} />
            </div>
        </div>
    )
}

export default Size;
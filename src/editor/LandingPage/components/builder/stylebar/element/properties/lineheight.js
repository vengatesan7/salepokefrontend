import React, { useState, useContext, useEffect } from "react";
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import classnames from "classnames";
import _get from "lodash.get";
import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";
import ColorBoxSelection from "../../../../elements/colorBoxSelection";
import SpinButtonInput from "../../../../elements/spinButtonInput";

const Lineheight = ({ elementId }) => {
  const {
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);

  const [elementData, setElementData] = useState(null);
  const [colorPicker, popup] = useState(false);
  const [thisColor, pickedColor] = useState(null);
  const [fontSize, setFontSize] = useState(null);
  const [fontSpace, setFontSpace] = useState(null);
  const [fontFamily, setFontFamily] = useState(null);
  const [bold, setBold] = useState(false);
  const [italic, setItalic] = useState(false);
  const [underline, setUnderline] = useState(false);
  const [strikeout, setStrikeout] = useState(false);
  const [textLink, setTextLink] = useState(null);
  const [fontColor, setFontColor] = useState("#ffffff");
  const [letterSpacing, setLetterSpacing] = useState(null);

  const colorClick = (selectedColor) => {
    pickedColor({ 'background': selectedColor })
    dispatch({
      type: actions.FONT_COLOR,
      id: elementId,
      color: selectedColor
    });
  }
  const getData = () => {
    setElementData(stateHistory.layers.entities.element[elementId]);
    setFontSize(_get(stateHistory.layers.entities.element[elementId], 'fontSize', ''))
    setBold(_get(stateHistory.layers.entities.element[elementId], 'bold', null) === "bold");
    setItalic(_get(stateHistory.layers.entities.element[elementId], 'italic', null) === "italic");
    setUnderline(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "underline");
    setStrikeout(_get(stateHistory.layers.entities.element[elementId], 'textDecoration', null) === "line-through");
    var lineHeight  = _get(stateHistory.layers.entities.element[elementId], 'lineHeight', 1);
    if( lineHeight <= 3 ){
      setFontSpace(_get(stateHistory.layers.entities.element[elementId], 'lineHeight', 1).toFixed(1));
    }else{
      setFontSpace("1.5");
    }
    setFontFamily(_get(stateHistory.layers.entities.element[elementId], 'fontFamily', ""))
    setLetterSpacing(_get(stateHistory.layers.entities.element[elementId], 'letterSpacing', 0));

  }
  useEffect(() => {
    getData();
  }, [stateHistory])
  useEffect(() => {
    getData();
  }, [elementId])
  useEffect(() => {
    pickedColor({ 'background': _get(elementData, 'fontColor', '#000000') })
  }, [elementData]);
  useEffect(() => {
    document.execCommand("foreColor", false, fontColor);
  }, [fontColor])
  const handleChange = (value, name) => {
    name === "fontSize" && setFontSize(value);
    name === "lineHeight" && setFontSpace(value.toFixed(1));        
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            [name]: value
          }
        }
      }
    }
    return updateElement(data);
  }
  const updateElement = (data) => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  }
  const handleChangeStyle = (value, name) => {
    name === "bold" && setBold(value);
    name === "italic" && setItalic(value);
    if (name === "underline") {
      setUnderline(value);
      // setStrikeout(!value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              textDecoration: value === true ? "underline" : null
            }
          }
        }
      }
      return updateElement(data);
    } else if (name === "strikeout") {
      setStrikeout(value);
      // setUnderline(!value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              textDecoration: value === true ? "line-through" : null
            }
          }
        }
      }
      return updateElement(data);
    } else {
      // name === "underline" && setUnderline(value);
      // name === "strikeout" && setStrikeout(value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          element: {
            ...layers.entities.element,
            [elementId]: {
              ...layers.entities.element[elementId],
              [name]: value === true ? name : null
            }
          }
        }
      }
      return updateElement(data);
    }
  }

  return (
    <React.Fragment>
      {elementData !== null &&
        <React.Fragment>
          <div className="property-form">
            <div className="property-form-header">
              <h4>Font Style</h4>
            </div>
          
            <div className="two-column">
              <label>Line Spacing</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={0} max={3} step={0.1}
                    value={fontSpace || 1}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'lineHeight')}
                  />
                </div>
                <div className="rangeslider-value">
                  {fontSpace || 1}
                </div>
              </div>
            </div>


            <div className="two-column">
              <label>Letter Spacing</label>
              <div className="rangeslider-wrapper">
                <div className="rangeslider-slider">
                  <Slider min={-10} max={10} step={1}
                    value={letterSpacing || 0}
                    orientation="horizontal"
                    tooltip={false}
                    onChange={(e) => handleChange(e, 'letterSpacing')}
                  />
                </div>
                <div className="rangeslider-value">
                  {letterSpacing || 0}
                </div>
              </div>
            </div>
                      
          </div>
        </React.Fragment>
      }
    </React.Fragment>
  )
};

export default Lineheight;
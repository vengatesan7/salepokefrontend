import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";

const Direction = ({ elementId }) => {
    const {
        dispatch,
        state: {layers},
        history: { state: stateHistory }
    } = useContext(Store);
    const [direction, setDirection] = useState(null);
    const [first, setFirst] = useState(null);
    
    const getData = () => {
        setDirection(_get(layers.entities.element[elementId], "direction", "HORIZONTAL"));
        setFirst(_get(layers.entities.element[elementId], "first", "TEXT"));
    }

    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [stateHistory])
    const updateLayer = (data) => {
        const newLayers = {...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId], ...data
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newLayers
        });
    }
    const handleChangeStyle = (value, type) => {
        if(value === true) {
            switch (type) {
                case "one":
                    setDirection("HORIZONTAL");
                    setFirst("IMAGE");
                    const styleOne = {
                        direction: "HORIZONTAL",
                        first: "IMAGE"
                    }
                   return updateLayer(styleOne);
                case "two":
                    setDirection("HORIZONTAL");
                    setFirst("TEXT");
                    const styleTwo = {
                        direction: "HORIZONTAL",
                        first: "TEXT"
                    }
                    return updateLayer(styleTwo);
                case "three":
                    setDirection("VERTICAL");
                    setFirst("IMAGE");
                    const styleThree = {
                        direction: "VERTICAL",
                        first: "IMAGE"
                    }
                    return updateLayer(styleThree);
                case "four":
                    setDirection("VERTICAL");
                    setFirst("TEXT");
                    const styleFour = {
                        direction: "VERTICAL",
                        first: "TEXT"
                    }
                    return updateLayer(styleFour);
                default:
                    break;
            }
        }
    }

    return (
        <div className="property-form">
            <h4>Direction</h4>
            <div className="two-column">
                <div>
                    <input
                        type="radio"
                        name="image-text"
                        id="image-text-one"
                        checked={direction === "HORIZONTAL" && first === "IMAGE"}
                        onChange={(e) => handleChangeStyle(e.target.checked, "one")}
                    />
                    <label for="image-text-one">Image + Text</label>
                </div>
                <div>
                    <input
                        type="radio"
                        name="image-text"
                        id="image-text-two"
                        checked={direction === "HORIZONTAL" && first === "TEXT"}
                        onChange={(e) => handleChangeStyle(e.target.checked, "two")}
                    />
                    <label for="image-text-two">Text + Image</label>
                </div>
                <div>
                    <input
                        type="radio"
                        name="image-text"
                        id="image-text-three"
                        checked={direction === "VERTICAL" && first === "IMAGE"}
                        onChange={(e) => handleChangeStyle(e.target.checked, "three")}
                    />
                    <label for="image-text-three">Image <br /> Text</label>
                </div>
                <div>
                    <input
                        type="radio"
                        name="image-text"
                        id="image-text-four"
                        checked={direction === "VERTICAL" && first === "TEXT"}
                        onChange={(e) => handleChangeStyle(e.target.checked, "four")}
                    />
                    <label for="image-text-four">Text <br /> Image</label>
                </div>
            </div>
        </div>
    );
}

export default Direction;
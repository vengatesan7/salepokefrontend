import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import Switch from "react-switch";
import _get from 'lodash.get';

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';
import FontSelect from '../../../../elements/fontSelect';

const Timer = ({elementId}) => {
    const {
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    const [elementData, setElementData] = useState(null);
    const [fontColorPopup, setFontColorPopup] = useState(false)

    useEffect(() => {
        setElementData(stateHistory.layers.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(stateHistory.layers.entities.element[elementId]);
    }, [stateHistory]);
    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };
    const handleChanges = (value, name) => {
        setFontColorPopup(false);
        setElementData({...elementData, [name]: value});
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        }
        return updateLayers(newData);
    }

    return (
        elementData !== null && <React.Fragment>
            <div className='property-form'>
                <div className="pl-10 pr-10">
                    <div className="row align-items-center">
                        <div className="col">
                            <label><h5>Timer Status</h5></label>
                        </div>
                        <div className="col text-right">
                            <div className="property-switch">
                                <Switch checked={elementData.enable} onChange={(e) => handleChanges(e, 'enable')}
                                    onColor="#43da71" onHandleColor="#f8f8f8"
                                    offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                    handleDiameter={12} height={16} width={30} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='property-form'>
                <div className="pl-10 pr-10">
                    <div className="row align-items-center">
                        <div className="col-8">
                            <label><h5>Evergreen Timer</h5></label>
                        </div>
                        <div className="col text-right">
                            <div className="property-switch">
                                <Switch checked={elementData.EverGreen} onChange={(e) => handleChanges(e, 'EverGreen')}
                                    onColor="#43da71" onHandleColor="#f8f8f8"
                                    offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                    handleDiameter={12} height={16} width={30} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {elementData.EverGreen &&
                <div className="property-form">
                    <h4>Timer Run Time</h4>
                    <div className="pl-10 pr-10">
                        <div className="row mb-20 mt-25">
                            <div className="col">
                                <label>Hours</label>
                            </div>
                            <div className="col">
                                <input type="text" value={elementData.hours} onChange={ (e) => handleChanges(e.target.value, 'hours')} className="form-control" />
                            </div>
                        </div>
                        <div className="row mb-20">
                            <div className="col">
                                <label>Minutes</label>
                            </div>
                            <div className="col">
                                <input type="text" value={elementData.minutes} onChange={ (e) => handleChanges(e.target.value, 'minutes')} className="form-control" />
                            </div>
                        </div>
                        <div className="row mb-20">
                            <div className="col">
                                <label>Seconds</label>
                            </div>
                            <div className="col">
                                <input type="text" value={elementData.seconds} onChange={ (e) => handleChanges(e.target.value, 'seconds')}  className="form-control" />
                            </div>
                        </div>
                        <label><strong>Expire Action</strong></label>                        
                        <div className="row mb-20">
                            <div className="col-12">
                                <label>Redirect URL</label>
                            </div>
                            <div className="col-12">
                                <input type="text" value={elementData.redirectURL} placeholder="https://www.google.com" onChange={ (e) => handleChanges(e.target.value, 'redirectURL')} className="form-control" />
                            </div>
                        </div>
                        <div className="row mb-20 justify-content-end">
                            <div className="col-12">
                                <label>Re-Visit Action</label>
                            </div>
                            <div className="col-12">
                                <select value={elementData.revisitAction} onChange={ (e) => handleChanges(e.target.value, 'revisitAction')} className="form-control">
                                    <option value="reset">Auto Reset Timer</option>
                                    <option value="expirecookie">Auto Expire For X Days</option>
                                </select>
                            </div>
                        </div>
                        {elementData.revisitAction == "expirecookie" &&
                            <div className="row mb-20 justify-content-end">
                                <div className="col-12">
                                    <label>Expire Cookie (days)</label>
                                </div>
                                <div className="col-12">
                                    <input type="text" value={elementData.cookieExpire} onChange={ (e) => handleChanges(e.target.value, "cookieExpire")} className="form-control" />
                                </div>
                            </div>
                        }
                    </div>
                </div>
            }
            {!elementData.EverGreen &&            
                <div className="property-form">                
                    <h4>Timer Run Time</h4>
                    <div className="two-column">
                        <label>End Date</label>
                        <input type="date" value={elementData.endDate} onChange={(e) => handleChanges(e.target.value, 'endDate')} />
                    </div>
                    <div className="two-column">
                        <label>End Time</label>
                        <input type="time" value={elementData.endTime}  onChange={(e) => handleChanges(e.target.value, 'endTime')} />
                    </div>
                </div>
            }            
            <div className="property-form">
                <h4>Font Style</h4>
                <div className="two-column">
                    <label>Font Color</label>
                    <div>
                        <div className="color-picker-swatch" onClick={() => setFontColorPopup(true)}>
                            <div className="color-picker-color" style={{ backgroundColor: elementData.fontColor }}></div>
                        </div>
                        {fontColorPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => handleChanges(elementData.fontColor, "fontColor")} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={elementData.fontColor} onChange={(e) => setElementData({...elementData, "fontColor": e.hex})} disableAlpha />
                                <button className='color-picker-button' onClick={() => handleChanges(elementData.fontColor, "fontColor")}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </div>
                <div className="two-column">
                    <label>Font Family</label>
                    <div>
                        <FontSelect value={elementData.fontFamily} onChange={(value) => handleChanges(value, 'fontFamily')} />
                    </div>
                </div>
                <div className="two-column">
                    <label>Font Size</label>
                    <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={1} max={100} step={1}
                                value={elementData.fontSize}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleChanges(e, 'fontSize')}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {elementData.fontSize}
                        </div>
                    </div>
                </div>
                <div className="two-column">
                    <label>Line Height</label>
                    <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={1} max={100} step={1}
                                value={elementData.lineHeight}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleChanges(e, 'lineHeight')}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {elementData.lineHeight}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Timer;
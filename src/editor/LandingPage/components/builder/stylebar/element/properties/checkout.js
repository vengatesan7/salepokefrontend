import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from 'lodash.get';

import actions from "../../../../../store/action-types";
import FontSelect from '../../../../elements/fontSelect';
import { Store } from '../../../../../store';

const Checkout = ({ elementId }) => {
    const {
        state,
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    const [elementStyle, setElementStyle] = useState(null);
    const [submitButton, setSubmitButton] = useState(null);
    const [checkoutFormData, setCheckoutFormData] = useState(null);

    const [colorPopup, setColorPopup] = useState(false);
    const [buttonColorPopup, setButtonColorPopup] = useState(false);
    const [buttonBackgroundColorPopup, setButtonBackgroundColorPopup] = useState(false);

    const getData = () => {
        setElementStyle(_get(state.layers.entities.element[state.activeElement], 'style', null));
        setSubmitButton(_get(state.layers.entities.element[state.activeElement], 'submitButton', null));
        setCheckoutFormData(_get(state.layers.entities.element[state.activeElement], 'checkoutFormData', null));
    }

    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [stateHistory]);

    const updateElement = (data) => {
        return dispatch({
          type: actions.UPDATE_LAYER,
          payload: data
        });
    }

    const handleChange = (value, name) => {
        setColorPopup(false);
        setElementStyle({
            ...elementStyle,
            [name]: value
        });
        const elementStyleUpdate = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [state.activeElement]: {
                        ...layers.entities.element[state.activeElement],
                        style: {
                            ...layers.entities.element[state.activeElement].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return updateElement(elementStyleUpdate);
    }
    
    const handleSubmitChanges = (value, name) => {
        setButtonColorPopup(false);
        setButtonBackgroundColorPopup(false);
        if (name === "submitText" || name === "submitWidth") {
            setSubmitButton({
                ...submitButton,
                [name]: value
            });
            const submitButtonUpdate = {
                ...layers,
                entities: {
                    ...layers.entities,
                    element: {
                        ...layers.entities.element,
                        [state.activeElement]: {
                            ...layers.entities.element[state.activeElement],
                            submitButton: {
                                ...layers.entities.element[state.activeElement].submitButton,
                                [name]: value
                            }
                        }
                    }
                }
            }
            return updateElement(submitButtonUpdate);
        } else {
            setSubmitButton({
                ...submitButton,
                style: {
                    ...submitButton.style,
                    [name]: value
                }
            });
            const submitButtonStyleUpdate = {
                ...layers,
                entities: {
                    ...layers.entities,
                    element: {
                        ...layers.entities.element,
                        [state.activeElement]: {
                            ...layers.entities.element[state.activeElement],
                            submitButton: {
                                ...layers.entities.element[state.activeElement].submitButton,
                                style: {
                                    ...layers.entities.element[state.activeElement].submitButton.style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
            }
            return updateElement(submitButtonStyleUpdate);
        }
    }

    const handleBlocks = (value, name) => {
        setCheckoutFormData({
            ...checkoutFormData,
            [name]: value
        });
        const checkoutFormDataUpdate = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [state.activeElement]: {
                        ...layers.entities.element[state.activeElement],
                        checkoutFormData: {
                            ...layers.entities.element[state.activeElement].checkoutFormData,
                            [name]: value
                        }
                    }
                }
            }
        }
        return updateElement(checkoutFormDataUpdate);
    }

    return (
        <React.Fragment>
            {checkoutFormData !== null && <div className="property-form">
                <div className="property-form-header">
                    <h4 className="full-width">Blocks</h4>
                    <div style={{ marginBottom: "10px", display: "inline-block", width: "100%" }}>
                        <input
                            id="checkoutFormDataorderDetail"
                            checked={checkoutFormData.orderDetail}
                            type="checkbox"
                            style={{ marginRight: "5px" }}
                            onChange={(e) => handleBlocks(e.target.checked, 'orderDetail')}
                        />
                        <label for="checkoutFormDataorderDetail" style={{ fontWeight: '400', margin: "0", verticalAlign: "top" }}>Order Detail</label>
                    </div>
                    <div style={{ marginBottom: "10px", display: "inline-block", width: "100%" }}>
                        <input
                            id="checkoutFormDatapaymentMethod"
                            checked={checkoutFormData.paymentMethod}
                            type="checkbox"
                            style={{ marginRight: "5px" }}
                            onChange={(e) => handleBlocks(e.target.checked, 'paymentMethod')}
                        />
                        <label for="checkoutFormDatapaymentMethod" style={{ fontWeight: '400', margin: "0", verticalAlign: "top" }}>Payment MEthod</label>
                    </div>
                    <div style={{ marginBottom: "10px", display: "inline-block", width: "100%" }}>
                        <input
                            id="checkoutFormDatabillingDetail"
                            checked={checkoutFormData.billingDetail}
                            type="checkbox"
                            style={{ marginRight: "5px" }}
                            onChange={(e) => handleBlocks(e.target.checked, 'billingDetail')}
                        />
                        <label for="checkoutFormDatabillingDetail" style={{ fontWeight: '400', margin: "0", verticalAlign: "top" }}>Billing Detail</label>
                    </div>
                    <div style={{ marginBottom: "10px", display: "inline-block", width: "100%" }}>
                        <input
                            id="checkoutFormDataprofileInfo"
                            checked={checkoutFormData.profileInfo}
                            type="checkbox"
                            style={{ marginRight: "5px" }}
                            onChange={(e) => handleBlocks(e.target.checked, 'profileInfo')}
                        />
                        <label for="checkoutFormDataprofileInfo" style={{ fontWeight: '400', margin: "0", verticalAlign: "top" }}>Profile Information</label>
                    </div>
                </div>
            </div>}
            {elementStyle !== null && <div className="property-form">
                <div className="property-form-header">
                    <h4 className="full-width">Form Style</h4>
                    <div className="two-column">
                        <label>Font Family</label>
                        <div>
                            <FontSelect
                                value={elementStyle.fontFamily}
                                onChange={(value) => handleChange(value, 'fontFamily')}
                            />
                        </div>
                    </div>
                    <div className="two-column">
                        <label>Font Size</label>
                        <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={1} max={100} step={1}
                                value={elementStyle.fontSize}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleChange(e, 'fontSize')}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {elementStyle.fontSize}
                        </div>
                        </div>
                    </div>
                    <div className="two-column">
                        <label>Font Color</label>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setColorPopup(true)} style={{ float: "none" }}>
                                <div className="color-picker-color" style={{ backgroundColor: elementStyle.color }}></div>
                            </div>
                            {colorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleChange(elementStyle.color, 'color')} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={elementStyle.color} onChange={(e) => setElementStyle({...elementStyle, color: e.hex})} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleChange(elementStyle.color, 'color')}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>}
            {submitButton !== null && <div className="property-form">
                <div className="property-form-header">
                    <h4 className="full-width">Form Button Style</h4>
                    <div style={{ marginBottom: "10px" }}>
                        <label style={{ fontWeight: '400', margin: "0" }}>Button Text</label>
                        <div>
                            <input
                                value={submitButton.submitText}
                                style={{ width: "100%", height: "34px", padding: "5px" }}
                                onChange={(e) => handleSubmitChanges(e.target.value, 'submitText')}
                            />
                        </div>
                    </div>
                    <div style={{ marginBottom: "10px" }}>
                        <label style={{ fontWeight: '400', margin: "0" }}>Width</label>
                        <div>
                            <select
                                value={submitButton.submitWidth}
                                style={{
                                borderRadius: "0",
                                width: "100%",
                                height: "34px"
                                }}
                                onChange={(e) => handleSubmitChanges(e.target.value, "submitWidth")}
                            >
                                <option value="fittotext">Fit to Text</option>
                                <option value="fullwidth">Full Width</option>
                            </select>
                        </div>
                    </div>
                    <div className="two-column" style={{ marginBottom: "10px" }}>
                        <label style={{ fontWeight: '400', margin: "0" }}>Color</label>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setButtonColorPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: submitButton.style.color }}></div>
                            </div>
                            {buttonColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleSubmitChanges(submitButton.style.color, "color")} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={submitButton.style.color} 
                                        onChange={(e) => setSubmitButton({
                                            ...submitButton, 
                                            style: {
                                                ...submitButton.style,
                                                color: e.hex
                                            }
                                        })} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleSubmitChanges(submitButton.style.color, "color")}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div className="two-column" style={{ marginBottom: "0px" }}>
                        <label style={{ fontWeight: '400', margin: "0" }}>Background</label>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setButtonBackgroundColorPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: submitButton.style.background }}></div>
                            </div>
                            {buttonBackgroundColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleSubmitChanges(submitButton.style.background, "background")} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={submitButton.style.background}
                                        onChange={(e) => setSubmitButton({
                                            ...submitButton, 
                                            style: {
                                                ...submitButton.style,
                                                background: e.hex
                                            }
                                        })} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleSubmitChanges(submitButton.style.background, "background")}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                </div>
            </div>}
        </React.Fragment>
    );
}

export default Checkout;
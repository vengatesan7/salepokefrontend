import React, { useContext, useEffect, useState } from "react";
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import classnames from "classnames";
import Switch from "react-switch";
import _get from 'lodash.get';
import cookie from "react-cookies";

import SpinButtonInput from "../../../../elements/spinButtonInput";
import ColorBoxSelection from "../../../../elements/colorBoxSelection";
import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";
import FontSelect from "../../../../elements/fontSelect";
import { PostData } from '../../../../../../../services/PostData';

const Form = ({ elementId }) => {
  const {
    state,
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);
  const formId = _get(state.layers.entities.element[state.activeElement], 'formData[0]', null);
  const [column, setColumn] = useState(null);
  const [label, setLabel] = useState(false);
  const [actionType, setActionType] = useState("default");
  const [actionURL, setActionURL] = useState(null);
  const [buttonText, setButtonText] = useState(null);
  const [submitColor, setSubmitColor] = useState(null);
  const [submitAlign, setSubmitAlign] = useState(null);
  const [buttonColorPopup, setButtonColorPopup] = useState(false);
  const [submitBackground, setSubmitBackground] = useState(null);
  const [buttonBackgroundColorPopup, setButtonBackgroundColorPopup] = useState(false);
  const [submitWidth, setSubmitWidth] = useState(null);
  const [labelColor, setLabelColor] = useState(null);
  const [labelColorPopup, setLabelColorPopup] = useState(false);
  const [formName, setFormName] = useState(null);
  const [spaceBetweenColumn, setSpaceBetweenColumn] = useState(null);
  
  const [fontColor, setFontColor] = useState("#000000");
  const [colorPopup, setColorPopup] = useState(false);
  const [placeholderColor, setPlaceholderColor] = useState("#000000");
  const [placeholderColorPopup, setPlaceholderColorPopup] = useState(false);

  const [fontSize, setFontSize] = useState(null);
  const [fontFamily, setFontFamily] = useState(null);
  const [fontSpace, setFontSpace] = useState(null);
  const [submitBorderRadius, setSubmitBorderRadius] = useState(null);
  const [formInputBorderRadius, setFormInputBorderRadius] = useState(null);
  const editortype = localStorage.getItem('editortype');
  const [getFunnelsteps, setFunnelsteps] = useState([]);
  const [actionStepId, setActionStepId] = useState(null);
  
  const getData = () => {
    setLabel(_get(stateHistory.layers.entities.formData[formId], 'label', true));
    setActionType(_get(stateHistory.layers.entities.formData[formId], 'formType', "default"));
    setActionURL(_get(stateHistory.layers.entities.formData[formId], 'actionURL', null));
    setSubmitColor(_get(stateHistory.layers.entities.formData[formId], 'submitColor', "#ffffff"));
    setSubmitAlign(_get(stateHistory.layers.entities.formData[formId], 'submitAlign', "center"));
    setSubmitBackground(_get(stateHistory.layers.entities.formData[formId], 'submitBackground', "#4452d9"));
    setButtonText(_get(stateHistory.layers.entities.formData[formId], 'submitText', "Submit"));
    setSubmitWidth(_get(stateHistory.layers.entities.formData[formId], 'submitWidth', "fullwidth"));
    setLabelColor(_get(stateHistory.layers.entities.formData[formId], 'labelColor', "#000000"));
    setFormName(_get(stateHistory.layers.entities.formData[formId], 'formName', ""));
    setSpaceBetweenColumn(_get(stateHistory.layers.entities.formData[formId], 'spaceBetweenColumn', 0));
    setFontColor(_get(stateHistory.layers.entities.element[elementId], 'fontColor', '#000000'));
    setFontSize(_get(stateHistory.layers.entities.element[elementId], 'fontSize', 1));
    setFontFamily(_get(stateHistory.layers.entities.element[elementId], 'fontFamily', ""));
    setFontSpace(_get(stateHistory.layers.entities.element[elementId], 'lineHeight', 1));
    setSubmitBorderRadius(_get(stateHistory.layers.entities.formData[formId], 'submitBorderRadius', 0));
    setFormInputBorderRadius(_get(stateHistory.layers.entities.formData[formId], 'formInputBorderRadius', 0));
    setPlaceholderColor(_get(stateHistory.layers.entities.element[elementId], 'placeholderColor', '#000000'));

    const desktopColumns = _get(stateHistory.layers.entities.formData[formId], 'columns', 1);
    switch (state.deviceType) {
      case "phone":
        return setColumn(_get(stateHistory.layers.entities.formData[formId], 'phoneColumns', desktopColumns));
      case "tablet":
        return setColumn(_get(stateHistory.layers.entities.formData[formId], 'tabletColumns', desktopColumns));
      case "desktop":
        return setColumn(desktopColumns);
      default:
        return setColumn(desktopColumns);
    }
  }
  useEffect(() => {
    if (editortype === "funnels") {
      const stepID = {
          "funnel_step_id": cookie.load("stepId")
      }   
      PostData('ms3', 'funnelstepseditor', stepID).then(response => {
          if (response !== "Invalid") {
              if (response.status === "success") {
                  setFunnelsteps(response.getfunnelsteps);
              }
          }
      });
  }
  },[])
  useEffect(() => {
    getData();
  }, [elementId]);
  useEffect(() => {
    getData();
  }, [stateHistory]);
  useEffect(() => {
    getData();
  }, [state.deviceType]);
  const updateElement = (data) => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  }
  const formDesignPopup = () => {
    return dispatch({
      type: actions.FORM_POPUP
    })
  }
  const handleFormChanges = (value, name) => {
    setLabelColorPopup(false);
    setButtonColorPopup(false);
    setButtonBackgroundColorPopup(false);
    name === "formType" && setActionType(value);
    name === "actionURL" && setActionURL(value);
    name === "label" && setLabel(value);
    name === "submitText" && setButtonText(value);
    name === "submitWidth" && setSubmitWidth(value);
    name === "submitBorderRadius" && setSubmitBorderRadius(value);
    name === "formInputBorderRadius" && setFormInputBorderRadius(value);

    if(name === "actionStepId"){
    setActionType('funnels');
    setActionURL(value);
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          formData: {
            ...layers.entities.formData,
            [formId]: {
              ...layers.entities.formData[formId],
              ['actionURL']: value
            }
          }
        }
      }
      return updateElement(data);
    }else{
      const data = {
        ...layers,
        entities: {
          ...layers.entities,
          formData: {
            ...layers.entities.formData,
            [formId]: {
              ...layers.entities.formData[formId],
              [name]: value
            }
          }
        }
      }
      return updateElement(data);
    }
    
  }

  const handleFormColumnChanges = (value, name) => {
      setColumn(value);
      switch (state.deviceType) {
        case "phone":
          const phoneData = {
            ...layers,
            entities: {
              ...layers.entities,
              formData: {
                ...layers.entities.formData,
                [formId]: {
                  ...layers.entities.formData[formId],
                  phoneColumns: value
                }
              }
            }
          }
          return updateElement(phoneData);
        case "tablet":
          const tabletData = {
            ...layers,
            entities: {
              ...layers.entities,
              formData: {
                ...layers.entities.formData,
                [formId]: {
                  ...layers.entities.formData[formId],
                  tabletColumns: value
                }
              }
            }
          }
          return updateElement(tabletData);
        case "desktop":
          const desktopData = {
            ...layers,
            entities: {
              ...layers.entities,
              formData: {
                ...layers.entities.formData,
                [formId]: {
                  ...layers.entities.formData[formId],
                  columns: value
                }
              }
            }
          }
          return updateElement(desktopData);
        default:
          const data = {
            ...layers,
            entities: {
              ...layers.entities,
              formData: {
                ...layers.entities.formData,
                [formId]: {
                  ...layers.entities.formData[formId],
                  [name]: value
                }
              }
            }
          }
          return updateElement(data);
      }
  }
  
  const handleChange = (value, name) => {

    setColorPopup(false);
    setPlaceholderColorPopup(false);
    
    name === "fontColor" && setFontColor(value);
    name === "placeholderColor" && setPlaceholderColor(value);

    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            [name]: value
          }
        }
      }
    }
    return updateElement(data);
  }
  
  return (
    
    <React.Fragment>
      {/* Form Edit button */}
      <div className="property-form">
        <button className="edit-form-button" onClick={() => formDesignPopup(elementId)}>Edit Your Form</button>
      </div>
      <div className="property-form">
        <div className="property-form-header">
          <h4>Form Setting</h4>
        </div>
        <div className="two-column">
          <label style={{ lineHeight: "34px" }}>Form Name</label>
          <div>
            <input 
              value={formName}
              onChange={(e) => setFormName(e.target.value)}
              onBlur={(e) => handleFormChanges(e.target.value, "formName")}
              style={{ height: "34px", padding: "5px" }}
            />
          </div>
        </div>
        <div className="two-column">
          <label>Form Label</label>
          <div>
            <div className="property-switch" style={{ float: "right" }}>
              <Switch checked={label} onChange={(e) => handleFormChanges(e, "label")}
                onColor="#43da71" onHandleColor="#f8f8f8"
                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                handleDiameter={12} height={16} width={30} />
            </div>
          </div>
        </div>
        {label && <div className="two-column">
          <label>Label Color</label>
          <div>
            <div className="color-picker-swatch" onClick={() => setLabelColorPopup(true)}>
              <div className="color-picker-color" style={{ backgroundColor: labelColor }}></div>
            </div>
            {labelColorPopup && <div className="color-picker-popover">
              <div className='color-picker-cover' onClick={() => handleFormChanges(labelColor, "labelColor")} />
              <div className='color-picker-wrapper'>
                <ChromePicker color={labelColor} onChange={(e) => setLabelColor(e.hex)} disableAlpha />
                <button className='color-picker-button' onClick={() => handleFormChanges(labelColor, "labelColor")}>Ok</button>
              </div>
            </div>}
          </div>
        </div>}
        {column !== null && <React.Fragment>
          <div className="two-column">
            <label>Column</label>
            <div>
              <div style={{ float: "right" }}>
                <SpinButtonInput
                  max={3}
                  min={1}
                  step={1}
                  value={column}
                  callBack={(v) => handleFormColumnChanges(v, "columns")}
                  // inputBoxDisabled
                />
              </div>
            </div>
          </div>
          {column > 1 && <div className="two-column">
            <label>Space Between Column</label>
            <div>
              <div style={{ float: "right" }}>
                <SpinButtonInput
                  max={99}
                  min={0}
                  step={1}
                  value={spaceBetweenColumn}
                  callBack={(v) => handleFormChanges(v, "spaceBetweenColumn")}
                  // inputBoxDisabled
                />
              </div>
            </div>
          </div>}
        </React.Fragment>}
        <div className="two-column">
          <label>Form Action</label>
          <div>
            <select value={actionType} onChange={(e) => handleFormChanges(e.target.value, 'formType')} style={{ borderRadius: "0" }}>
              <option value="default">Default thank you page</option>
              {/* <option value="landingpage">Landing Page</option> */}
              {editortype === "funnels" && <option value="funnels">Funnels</option>}
              <option value="url">External URL</option>
              <option value="samepage">Remain on same page</option>
            </select>
          </div>
        </div>
        {actionType === "url" && <div className="two-column">
          <label>Action URL</label>
          <div>
            <input
              value={actionURL}
              onChange={(e) => setActionURL(e.target.value)}
              onBlur={(e) => handleFormChanges(e.target.value, 'actionURL')}
              placeholder="https://example.com/"
            />
          </div>
        </div>}
        {actionType === "funnels" && <div>
                {getFunnelsteps.length !== 0 && <React.Fragment>
                    <label style={{ marginTop: "10px" }}>Select Your Funnel Step</label>
                    <select
                        onChange={(e) => handleFormChanges(e.target.value, 'actionStepId')}
                        value={actionStepId}
                        style={{ width: "100%", padding: "10px" }}
                    >
                        {actionStepId === null && <option value={null}>Select Your Funnel Step</option>}
                        {getFunnelsteps.map(data => (
                            <option value={data.funnel_step_url}>{data.funnel_step_name}</option>
                        ))}
                    </select>
                </React.Fragment>}
            </div>}
      </div>
      <div className="property-form">
        <div className="property-form-header">
          <h4>Form Style</h4>
          <div className="two-column">
            <label>Font Family</label>
            <div>
              <FontSelect value={fontFamily} onChange={(value) => handleChange(value, 'fontFamily')} />
            </div>
          </div>
          <div className="two-column">
            <label>Font Size</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={fontSize}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleChange(e, 'fontSize')}
                />
              </div>
              <div className="rangeslider-value">
                {fontSize}
              </div>
            </div>
          </div>
          <div className="two-column">
            <label>Line Height</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={fontSpace}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleChange(e, 'lineHeight')}
                />
              </div>
              <div className="rangeslider-value">
                {fontSpace}
              </div>
            </div>
          </div>
          <div className="two-column">
            <label>Font Color</label>
            <div>
              <div className="color-picker-swatch" onClick={() => setColorPopup(true)} style={{ float: "none" }}>
                <div className="color-picker-color" style={{ backgroundColor: fontColor }}></div>
              </div>
              {colorPopup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => handleChange(fontColor, 'fontColor')} />
                <div className='color-picker-wrapper'>
                  <ChromePicker color={fontColor} onChange={(e) => setFontColor(e.hex)} disableAlpha />
                  <button className='color-picker-button' onClick={() => handleChange(fontColor, 'fontColor')}>Ok</button>
                </div>
              </div>}
            </div>
          </div>
          <div className="two-column">
            <label>Placeholder Color</label>
            <div>
              <div className="color-picker-swatch" onClick={() => setPlaceholderColorPopup(true)} style={{ float: "none" }}>
                <div className="color-picker-color" style={{ backgroundColor: placeholderColor }}></div>
              </div>
              {placeholderColorPopup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => handleChange(placeholderColor, 'placeholderColor')} />
                <div className='color-picker-wrapper'>
                  <ChromePicker color={placeholderColor} onChange={(e) => setPlaceholderColor(e.hex)} disableAlpha />
                  <button className='color-picker-button' onClick={() => handleChange(placeholderColor, 'placeholderColor')}>Ok</button>
                </div>
              </div>}
            </div>
          </div>
          <div className="two-column">
            <label>Input Border Radius</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={formInputBorderRadius}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleFormChanges(e, 'formInputBorderRadius')}
                />
              </div>
              <div className="rangeslider-value">
                {formInputBorderRadius}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="property-form">
        <div className="property-form-header">
          <h4>Form Button Style</h4>
          <div style={{ marginBottom: "10px" }}>
            <label style={{ fontWeight: '400', margin: "0" }}>Button Text</label>
            <div>
              <input
                value={buttonText}
                style={{ width: "100%", height: "34px", padding: "5px" }}
                onChange={(e) => handleFormChanges(e.target.value, 'submitText')}
              />
            </div>
          </div>
          <div style={{ marginBottom: "10px" }}>
            <label style={{ fontWeight: '400', margin: "0" }}>Width</label>
            <div>
              <select
                value={submitWidth}
                style={{
                  borderRadius: "0",
                  width: "100%",
                  height: "34px"
                }}
                onChange={(e) => handleFormChanges(e.target.value, "submitWidth")}
              >
                <option value="fittotext">Fit to Text</option>
                <option value="fullwidth">Full Width</option>
              </select>
            </div>
          </div>
          <div className="two-column" style={{ marginBottom: "10px" }}>
            <label style={{ fontWeight: '400', margin: "0", lineHeight: "35px" }}>Align</label>
            <div className='property-align' style={{ marginTop: "0px" }}>
                <button className={classnames(submitAlign === "left" && "active")} onMouseDown={() => handleFormChanges("left", "submitAlign")}><i className="material-icons">format_align_left</i></button>
                <button className={classnames(submitAlign === "center" && "active")} onMouseDown={() => handleFormChanges("center", "submitAlign")}><i className="material-icons">format_align_center</i></button>
                <button className={classnames(submitAlign === "right" && "active")} onMouseDown={() => handleFormChanges("right", "submitAlign")}><i className="material-icons">format_align_right</i></button>
            </div>
          </div>
          <div className="two-column" style={{ marginBottom: "10px" }}>
            <label style={{ fontWeight: '400', margin: "0" }}>Color</label>
            <div>
              <div className="color-picker-swatch" onClick={() => setButtonColorPopup(true)}>
                <div className="color-picker-color" style={{ backgroundColor: submitColor }}></div>
              </div>
              {buttonColorPopup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => handleFormChanges(submitColor, "submitColor")} /> 
                <div className='color-picker-wrapper'>
                  <ChromePicker color={submitColor} onChange={(e) => setSubmitColor(e.hex)} disableAlpha />
                  <button className='color-picker-button' onClick={() => handleFormChanges(submitColor, "submitColor")}>Ok</button>
                </div>
              </div>}
            </div>
          </div>
          <div className="two-column" style={{ marginBottom: "0px" }}>
            <label style={{ fontWeight: '400', margin: "0" }}>Background</label>
            <div>
              <div className="color-picker-swatch" onClick={() => setButtonBackgroundColorPopup(true)}>
                <div className="color-picker-color" style={{ backgroundColor: submitBackground }}></div>
              </div>
              {buttonBackgroundColorPopup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => handleFormChanges(submitBackground, "submitBackground")} />
                <div className='color-picker-wrapper'>
                  <ChromePicker color={submitBackground} onChange={(e) => setSubmitBackground(e.hex)} disableAlpha />
                  <button className='color-picker-button' onClick={() => handleFormChanges(submitBackground, "submitBackground")}>Ok</button>
                </div>
              </div>}
            </div>
          </div>
          <div style={{ marginTop: "10px" }}>
            <ColorBoxSelection selectedColor={color => handleFormChanges(color, "submitBackground")} />
          </div>
          {/* <div className="two-column">
                <label>Radius</label>
                <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={1} value={styles.borderRadius} orientation="horizontal" tooltip={false} onChange={(e) => handleChange(e, 'borderRadius')} />
                    </div>
                    <div className="rangeslider-value">
                        {styles.borderRadius}
                    </div>
                </div>
            </div> */}
          <div className="two-column">
            <label>Border Radius</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={submitBorderRadius}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleFormChanges(e, 'submitBorderRadius')}
                />
              </div>
              <div className="rangeslider-value">
                {submitBorderRadius}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Form; 

import React, { useContext, useEffect, useState } from 'react';
import classnames from "classnames";
import _get from "lodash.get";

import actions from "../../../../../store/action-types";
import { Store } from '../../../../../store';

const Align = ({ elementId }) => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [textAlign, setTextAlign] = useState(null);

    const elementData = () => {
        setTextAlign(_get(stateHistory.layers.entities.element[elementId], "textAlign", null));
    }

    useEffect(() => {
        elementData();
    }, [elementId]);
    useEffect(() => {
        elementData();
    }, [stateHistory]);
    const handleAlignment = (position) => {
        setTextAlign(position);
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [elementId]: {
                        ...layers.entities.element[elementId],
                        textAlign: position
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        })
    }
    return (
        <div className="property-form">
            <h4>Align</h4>
            <div className='property-align'>
                <button className={classnames(textAlign === "left" && "active")} onMouseDown={() => handleAlignment("left")}><i className="material-icons">format_align_left</i></button>
                <button className={classnames(textAlign === "center" && "active")} onMouseDown={() => handleAlignment("center")}><i className="material-icons">format_align_center</i></button>
                <button className={classnames(textAlign === "right" && "active")} onMouseDown={() => handleAlignment("right")}><i className="material-icons">format_align_right</i></button>
            </div>
        </div>
    )
}

export default Align;
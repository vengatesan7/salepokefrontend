import React, { useContext, useEffect, useState } from "react";
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from 'lodash.get';
import cookie from "react-cookies";

import actions from "../../../../../store/action-types";
import { Store } from "../../../../../store";
import FontSelect from "../../../../elements/fontSelect";
import { PostData } from '../../../../../../../services/PostData';

const AdvanceForm = ({ elementId }) => {
  const {
    state,
    dispatch,
    state: { layers },
    history: { state: stateHistory }
  } = useContext(Store);
  const [formData, setFormData] = useState(null);
  const [colorPopup, setColorPopup] = useState(false);
  const editortype = localStorage.getItem('editortype');
  const [getFunnelsteps, setFunnelsteps] = useState([]);
  
  const getData = () => {
    setFormData(stateHistory.layers.entities.element[elementId]);
  }
  useEffect(() => {
    if (editortype === "funnels") {
      const stepID = {
          "funnel_step_id": cookie.load("stepId")
      }   
      PostData('ms3', 'funnelstepseditor', stepID).then(response => {
          if (response !== "Invalid") {
              if (response.status === "success") {
                  setFunnelsteps(response.getfunnelsteps);
              }
          }
      });
  }
  },[])
  useEffect(() => {
    getData();
  }, [elementId]);
  useEffect(() => {
    getData();
  }, [stateHistory]);
  useEffect(() => {
    getData();
  }, [state.deviceType]);
  const updateElement = (data) => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  }
  const formDesignPopup = () => {
    return dispatch({
      type: actions.ADVANCE_FORM_POPUP,
      payload: formData,
      enable: true
    })
  }

  const handleChange = (value, name) => {
    setColorPopup(false);
    setFormData({
        ...formData,
        [name]: value
    });

    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementId]: {
            ...layers.entities.element[elementId],
            [name]: value
          }
        }
      }
    }
    return updateElement(data);
  }
  
  return (
    formData !== null && <React.Fragment>
      <div className="property-form">
        <button className="edit-form-button" onClick={() => formDesignPopup(elementId)}>Edit Your Form</button>
      </div>
      <div className="property-form">
        <div className="property-form-header">
          <h4>Form Setting</h4>
        </div>
        <div className="two-column">
          <label style={{ lineHeight: "34px" }}>Form Name</label>
          <div>
            <input 
              value={_get(formData, 'formName', '')}
              onChange={(e) => handleChange(e.target.value, "formName")}
              style={{ height: "34px", padding: "5px" }}
            />
          </div>
        </div>
        <div className="two-column">
          <label>Form Action</label>
          <div>
            <select value={_get(formData, 'actionType', 'default')} onChange={(e) => handleChange(e.target.value, 'actionType')} style={{ borderRadius: "0" }}>
              <option value="default">Default thank you page</option>
              {editortype === "funnels" && <option value="funnels">Funnels</option>}
              <option value="url">External URL</option>
              <option value="samepage">Remain on same page</option>
            </select>
          </div>
        </div>
        {formData.actionType === "url" && <div className="two-column">
            <label>Action URL</label>
            <div>
                <input
                    value={_get(formData, 'actionURL', '')}
                    onChange={(e) => handleChange(e.target.value, 'actionURL')}
                    placeholder="https://example.com/"
                />
            </div>
        </div>}
        {formData.actionType === "funnels" && <div>
            {getFunnelsteps.length !== 0 && <React.Fragment>
                <label style={{ marginTop: "10px" }}>Select Your Funnel Step</label>
                <select
                    onChange={(e) => handleChange(e.target.value, 'actionStepId')}
                    value={_get(formData, 'actionStepId', null)}
                    style={{ width: "100%", padding: "10px" }}
                >
                    {_get(formData, 'actionStepId', null) === null && <option value={null}>Select Your Funnel Step</option>}
                    {getFunnelsteps.map(data => (
                        <option value={data.funnel_step_url}>{data.funnel_step_name}</option>
                    ))}
                </select>
            </React.Fragment>}
        </div>}
      </div>
      <div className="property-form">
        <div className="property-form-header">
          <h4>Form Style</h4>
          <div className="two-column">
            <label>Font Family</label>
            <div>
              <FontSelect value={_get(formData, 'fontFamily', '')} onChange={(value) => handleChange(value, 'fontFamily')} />
            </div>
          </div>
          <div className="two-column">
            <label>Font Size</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={_get(formData, 'fontSize', 14)}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleChange(e, 'fontSize')}
                />
              </div>
              <div className="rangeslider-value">
                {_get(formData, 'fontSize', 14)}
              </div>
            </div>
          </div>
          <div className="two-column">
            <label>Line Height</label>
            <div className="rangeslider-wrapper">
              <div className="rangeslider-slider">
                <Slider min={1} max={100} step={1}
                  value={_get(formData, 'lineHeight', 16)}
                  orientation="horizontal"
                  tooltip={false}
                  onChange={(e) => handleChange(e, 'lineHeight')}
                />
              </div>
              <div className="rangeslider-value">
                {_get(formData, 'lineHeight', 16)}
              </div>
            </div>
          </div>
          <div className="two-column">
            <label>Font Color</label>
            <div>
              <div className="color-picker-swatch" onClick={() => setColorPopup(true)} style={{ float: "none" }}>
                <div className="color-picker-color" style={{ backgroundColor: _get(formData, 'fontColor', '#000000') }}></div>
              </div>
              {colorPopup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => handleChange(_get(formData, 'fontColor', '#000000'), 'fontColor')} />
                <div className='color-picker-wrapper'>
                  <ChromePicker color={_get(formData, 'fontColor', '#000000')} onChange={(e) => setFormData({ ...formData, fontColor: e.hex})} disableAlpha />
                  <button className='color-picker-button' onClick={() => handleChange(_get(formData, 'fontColor', '#000000'), 'fontColor')}>Ok</button>
                </div>
              </div>}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AdvanceForm; 

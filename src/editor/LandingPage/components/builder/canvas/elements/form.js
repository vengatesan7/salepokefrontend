import React, { useContext, useEffect, useState } from "react";
import _get from 'lodash.get';

import { Store } from "../../../../store";

const Form = ({ elementData }) => {
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory, set }
  } = useContext(Store);
  
  const formId = _get(stateHistory.layers.entities.element[elementData.id], 'formData[0]', null);

  const [column, setColumn] = useState(null);
  const [width, setWidth] = useState(null);

  // const width = 100 / _get(stateHistory.layers.entities.formData[formId], 'columns', 1);
  const labelRequired = _get(stateHistory.layers.entities.formData[formId], 'label', true);
  const submitText = _get(stateHistory.layers.entities.formData[formId], 'submitText', 'Submit');
  const submitWidth = _get(stateHistory.layers.entities.formData[formId], 'submitWidth', "fullwidth");
  const submitColor = _get(stateHistory.layers.entities.formData[formId], 'submitColor', "#ffffff");
  const submitAlign = _get(stateHistory.layers.entities.formData[formId], 'submitAlign', "center");
  const submitBackground = _get(stateHistory.layers.entities.formData[formId], 'submitBackground', "#4452d9");
  const thisButtonWidth = submitWidth === "fullwidth" ? "100%" : "auto";
  const labelColor = _get(stateHistory.layers.entities.formData[formId], 'labelColor', "#000000");
  const spaceBetweenColumn = _get(stateHistory.layers.entities.formData[formId], 'spaceBetweenColumn', 0);
  const submitBorderRadius = _get(stateHistory.layers.entities.formData[formId], 'submitBorderRadius', 0);
  const formInputBorderRadius =_get(stateHistory.layers.entities.formData[formId], 'formInputBorderRadius', 0);;

  const findColumn = () => {
    const desktopColumns = _get(stateHistory.layers.entities.formData[formId], 'columns', 1);
    switch (state.deviceType) {
      case "phone":
        const findPhoneColumn = _get(stateHistory.layers.entities.formData[formId], 'phoneColumns', desktopColumns);
        setColumn(findPhoneColumn);
        setWidth(100 / findPhoneColumn);
        break;
      case "tablet":
        const findTabletColumn = _get(stateHistory.layers.entities.formData[formId], 'tabletColumns', desktopColumns);
        setColumn(findTabletColumn);
        setWidth(100 / findTabletColumn);
        break;
      case "desktop":
        setColumn(desktopColumns);
        setWidth(100 / desktopColumns);
        break;
      default:
        setColumn(desktopColumns);
        setWidth(100 / desktopColumns);
        break;
    }
  }

  useEffect(() => {
    findColumn();
  }, [state.deviceType]);
  useEffect(() => {
    findColumn();
  }, [stateHistory]);

  return (formId !== null &&
    <form className="form">
      <div className="form-container" style={{ columnCount: _get(stateHistory.layers.entities.formData[formId], 'columns', 1) }}>
        {stateHistory.layers.entities.element[elementData.id].formData.map(formId => {
          const fieldItemList = stateHistory.layers.entities.formData[formId].fields;
          return fieldItemList.length !== 0 ? fieldItemList.map(fieldId => {
            const fieldData = stateHistory.layers.entities.field[fieldId];
            if(fieldData !== undefined) {
              if (fieldData.type === 'text' || fieldData.type === 'email' || fieldData.type === 'number') {
                return (
                  <div className="field-group" style={column > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                    <div className="sample-text-input" style={{ borderRadius: formInputBorderRadius }}>{fieldData.placeholder}</div>
                  </div>
                );
              } if (fieldData.type === 'textarea') {
                return (
                  <div className="field-group" style={column > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                    <div className="sample-text-input" style={{ height: "65px",  borderRadius: formInputBorderRadius }}>{fieldData.placeholder}</div>
                  </div>
                ) 
              } if (fieldData.type === 'file') {
                return (
                  <div className="field-group" style={column > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                    <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>
                    {/* <div className="file-upload">{fieldData.placeholder}</div> */}
                    <div className="file-upload">
                      <i className="material-icons">cloud_upload</i>
                      <span>Drop Here</span>
                    </div>
                  </div>
                );
              } else if (fieldData.type === 'radio' || fieldData.type === 'checkbox') {
                return (
                  <div className="field-group" style={column > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                    <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>
                    {fieldData.options.map(option => {
                      return (option !== "" &&
                        <div>
                          <input type={fieldData.type} name={fieldData.label} value={option} />
                          <span>{option}</span>
                        </div>
                      );
                    })}
                  </div>
                );
              } else if (fieldData.type === 'select') {
                return (
                  <div className="field-group" style={column > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                    <div className="sample-text-input">{fieldData.placeholder}</div>
                  </div>
                )
              }
            }
          }) : 'Press edit to add fields.'
        })}
      </div>
      <div className="form-footer" style={column > 1 ? {textAlign: submitAlign, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { textAlign: submitAlign }}>
        <span className="form-submit" style={{ width: thisButtonWidth, color: submitColor, backgroundColor: submitBackground, borderRadius : submitBorderRadius }}>{submitText}</span>
      </div>
    </form>
  );
}

export default Form;

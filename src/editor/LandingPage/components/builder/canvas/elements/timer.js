import React from 'react';

const Timer = ({ elementData }) => {
    return (
        <div className="timer" style={{lineHeight: `${elementData.lineHeight}px`}}>
            { !elementData.EverGreen &&
                <div className="time-part">
                    <div className="time-value">
                        00
                        <span className="timer-dot">:</span>
                    </div>
                    <label
                        style={{
                            fontSize: `${elementData.fontSize / 3}px`,
                            lineHeight: `${elementData.lineHeight / 3}px`
                        }}
                    >Days</label>
                </div>
            }
            <div className="time-part">
                <div className="time-value">
                    00
                    <span className="timer-dot">:</span>
                </div>
                <label
                    style={{
                        fontSize: `${elementData.fontSize / 3}px`,
                        lineHeight: `${elementData.lineHeight / 3}px`
                    }}
                >Hours</label>
            </div>
            <div className="time-part">
                <div className="time-value">
                    00
                    <span className="timer-dot">:</span>
                </div>
                <label
                    style={{
                        fontSize: `${elementData.fontSize / 3}px`,
                        lineHeight: `${elementData.lineHeight / 3}px`
                    }}
                >Minutes</label>
            </div>
            <div className="time-part">
                <div className="time-value">00</div>
                <label
                    style={{
                        fontSize: `${elementData.fontSize / 3}px`,
                        lineHeight: `${elementData.lineHeight / 3}px`
                    }}
                >Seconds</label>
            </div>
        </div>
    );
}

export default Timer;
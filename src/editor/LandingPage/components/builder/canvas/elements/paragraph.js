import React, { useContext, useState, useEffect } from "react";
import _get from "lodash.get";

import actions from "../../../../store/action-types";
import { Store } from "../../../../store";
import EditorConvertToHTML from "../../../../../components/wysiwygText";

const Paragraph = ({ elementData }) => {
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory }
  } = useContext(Store);
  const [content, setContent] = useState(elementData.contents);
  const [contentChanges, setContentChanges] = useState(false);
  const [itIsActive, setItIsActive] = useState(null);

  useEffect(() => {
    setContent(_get(stateHistory.layers.entities.element[elementData.id], 'contents', ''));
  }, [stateHistory]);


  useEffect(() => {
    setItIsActive(state.activeElement);
  }, [state.activeElement]);

  useEffect(() => {
    if (itIsActive === null) {
      return dispatch({
        type: actions.REMOVE_SELECT_ELEMENT
      })
    }
  }, [itIsActive]);

  const updatelayer = (newData) => {
    return dispatch({
        type: actions.UPDATE_LAYER,
        payload: newData
    });
  }
  const handleUpdate = (value) => {
    setContent(value);
    setItIsActive(null);
    const updatedData = {
      ...stateHistory.layers,
      entities: {
        ...stateHistory.layers.entities,
        element: {
          ...stateHistory.layers.entities.element,
          [elementData.id]: {
            ...stateHistory.layers.entities.element[elementData.id],
            contents: value
          }
        }
      }
    }
    return updatelayer(updatedData);
  }
  const handleContentUpdate = (value) => {
    if( contentChanges ){
      console.log("content changed");
      setContentChanges(false);
      setContent(value);    
      const updatedData = {
        ...stateHistory.layers,
        entities: {
          ...stateHistory.layers.entities,
          element: {
            ...stateHistory.layers.entities.element,
            [elementData.id]: {
              ...stateHistory.layers.entities.element[elementData.id],
              contents: value
            }
          }
        }
      }
      return updatelayer(updatedData);
    }
  }
  
  return (
    elementData.id === itIsActive ? <React.Fragment>
        <div
          className="text-editor-overlay"
          onMouseDown={() => handleUpdate(content)}
          onMouseOut={() => handleContentUpdate(content)}
          style={{ height: document.getElementById('page-editor-main-container').clientHeight + 200 }}
        />
        <EditorConvertToHTML 
          content={content}
          options={[ 'inline', 'colorPicker', 'list', 'textAlign', 'fontSize', 'fontFamily', 'link' ]}
          onSubmitCallBack={(e) => { if( e != content){ setContentChanges(true);} setContent(e); } }
        />
      </React.Fragment> :
      <div style={{ pointerEvents: "none" }} dangerouslySetInnerHTML={{__html: content}} />
  );
};

export default Paragraph;
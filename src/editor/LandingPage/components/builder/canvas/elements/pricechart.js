import React, { useContext, useState, useEffect } from "react";
import ContentEditable from "react-contenteditable";
import sanitizeHtml from "sanitize-html";
import classNames from "classnames";
import _get from "lodash.get";

import actions from "../../../../store/action-types";
import { Store } from "../../../../store";

const PriceChart = ({ elementData }) => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);
    const [data, setData] = useState(elementData);
    const [pricelyLabel, setPricelyLabel] = useState(null);


    useEffect(() => {
        setData(stateHistory.layers.entities.element[data.id]);
        let dataValue = state.layers.entities.element[state.activeElement]
        setPricelyLabel(dataValue)

    }, [stateHistory])
  
    const sanitizeConf = {
        allowedTags: ["b", "i", "em", "strong", "a", "p", "h1","u", "strike", "div", "ul", "ol", "li", "font"],
        allowedAttributes: { a: ["href"], font: ["color", "size", "face"] }
    };
    
    const pricelyLabelConf = {
        allowedTags: ["b", "i", "em", "strong", "u", "strike", "font"],
        allowedAttributes: { font: ["color", "size", "face"] }
    }

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const formatedHtml = (name, value) => {
        switch (name) {
            case "pricelyLabel":
                return sanitizeHtml(value, pricelyLabelConf);
            case "pricing-currency":
                return sanitizeHtml(value, pricelyLabelConf);
            case "pricing-amount":
                return sanitizeHtml(value, pricelyLabelConf);
            case "pricing-foreword":
                return sanitizeHtml(value, pricelyLabelConf);
            default:
                return sanitizeHtml(value, sanitizeConf);
        }
    }

    const handleUpdate = (name, value) => {
        const newContent = formatedHtml(name, value);
        setData({
            ...data, 
            [name]: newContent
        });
    }
    const handleMainUpdate = () => {

        const updatedData = {
          ...stateHistory.layers,
            entities: {
                ...stateHistory.layers.entities,
                element: {
                ...stateHistory.layers.entities.element,
                    [data.id]: data
                }
            }
        }
        updatelayer(updatedData);
        return dispatch({
            type: actions.REMOVE_SELECT_ELEMENT
        });
    }
    
    // const purchaseButtonStyle = {
    //     background: data.purchaseButton.background,
    //     width: data.purchaseButton.width,
    //     color: data.purchaseButton.fontColor,
    //     padding: data.purchaseButton.padding,
    //     margin: data.purchaseButton.margin,
    // }

    return (
        <React.Fragment>
            {data.id === state.activeElement && <div
                className="text-editor-overlay"
                onMouseDown={() => handleMainUpdate(data)}
                style={{ height: document.getElementById('page-editor-main-container').clientHeight + 200 }}
            />}
            <div className={classNames( "pricing-table", data.id === state.activeElement && "active")}>
                <div className="pricing-header">
                    <ContentEditable
                        html={data && data.pricelyLabel}
                        disabled={!data.id === state.activeElement}
                        tagName={"div"}
                        className={classNames("pricing-label", data.id === state.activeElement && "active-editable" )}
                        onChange={e => handleUpdate("pricelyLabel", e.target.value)}
                        // OnBlur is not right way to do this. It's affecting history of JSON
                        // onBlur={() => handleUpdate()}
                        // onKeyDown={e => e.keyCode === 13 && handleBreak(e.target.innerHTML)}
                    />
                    <div className="pricing-figure">
                        <ContentEditable
                            html={data && data.pricelyCurrency}
                            disabled={!data.id === state.activeElement}
                            tagName={"div"}
                            className={classNames("pricing-currency", data.id === state.activeElement && "active-editable" )}
                            onChange={e => handleUpdate("pricelyCurrency", e.target.value)}
                        />
                        <ContentEditable
                            html={data && data.pricelyAmount}
                            disabled={!data.id === state.activeElement}
                            tagName={"div"}
                            className={classNames("pricing-amount", data.id === state.activeElement && "active-editable" )}
                            onChange={e => handleUpdate("pricelyAmount", e.target.value)}
                        />
                    </div>
                    <ContentEditable
                        html={data && data.pricelyForeword}
                        disabled={!data.id === state.activeElement}
                        tagName={"div"}
                        className={classNames("pricing-foreword", data.id === state.activeElement && "active-editable" )}
                        onChange={e => handleUpdate("pricelyForeword", e.target.value)}
                    />
                </div>
                <ContentEditable
                    html={data && data.pricelyList}
                    disabled={!data.id === state.activeElement}
                    tagName={"div"}
                    className={classNames("pricing-list", data.id === state.activeElement && "active-editable" )}
                    style={{"border-bottom-left-radius": data.borderRadius, "border-bottom-right-radius": data.borderRadius}}
                    onChange={e => handleUpdate("pricelyList", e.target.value)}
                />
                {/* <div className="pricing-footer">
                    <ContentEditable
                        html={data && data.purchaseButton.content}
                        disabled={!data.id === state.activeElement}
                        tagName={"span"}
                        className={classNames("pricing-button", data.id === state.activeElement && "active-editable" )}
                        style={purchaseButtonStyle}
                        onChange={e => handleUpdate("pricelyList", e.target.value)}
                    />
                </div> */}
            </div>
        </React.Fragment>
    );
}

export default PriceChart;
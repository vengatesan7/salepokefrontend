import React, { useContext, useState, useEffect } from "react";
import _get from 'lodash.get';

import actions from "../../../../store/action-types";
import { Store } from "../../../../store";
import EditorConvertToHTML from "../../../../../components/wysiwygText";

const Title = ({ elementData }) => {
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory }
  } = useContext(Store)
  const [content, setContent] = useState(elementData.contents);
  const [itIsActive, setItIsActive] = useState(null);
  
  useEffect(() => {
    setContent(_get(stateHistory.layers.entities.element[elementData.id], 'contents', ''));
  }, [stateHistory]);

  useEffect(() => {
    setItIsActive(state.activeElement);
  }, [state.activeElement]);

  useEffect(() => {
    if (itIsActive === null) {
      return dispatch({
        type: actions.REMOVE_SELECT_ELEMENT
      })
    }
  }, [itIsActive]);

  const handleUpdate = (value) => {
    setContent(value);
    setItIsActive(null);
    const newData = {
      ...layers,
      entities: {
        ...layers.entities,
        element: {
          ...layers.entities.element,
          [elementData.id]: {
            ...layers.entities.element[elementData.id],
            contents: value
          }
        }
      }
    }
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: newData
    });
  }
  
  return (
      elementData.id === state.activeElement ? <React.Fragment>
          <div 
            className="text-editor-overlay"
            onMouseDown={() => handleUpdate(content)}
            style={{ height: document.getElementById('page-editor-main-container').clientHeight + 200 }}
          />
          <EditorConvertToHTML
            content={content}
            onSubmitCallBack={(e) => setContent(e)}
          />
        </React.Fragment> :
        <div style={{ pointerEvents: "none" }} dangerouslySetInnerHTML={{__html: content}} />
  );
};

export default Title;

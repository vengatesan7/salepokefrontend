import React from 'react';
import _get from 'lodash.get';

const Video = ({ elementData }) => {
    const width = _get(elementData, 'width', null) === null ? "auto" : `${elementData.width}%`;
    const height = _get(elementData, 'height', null);
    const path = _get(elementData, 'path', null);
    return (
        path ? <div style={{ minHeight: "100px", width: width, height: height }}
            className="video"
            dangerouslySetInnerHTML={{ __html: elementData.path }} /> : <div style={{ textAlign: "center", padding: "25px", backgroundColor: "#ffffff" }} >
                <img
                    src="https://icon-library.net/images/video-png-icon/video-png-icon-16.jpg"
                    style={{ width: "100%", height: "100%", maxWidth: "100px", }}
                />
            </div>
    );
}

export default Video;
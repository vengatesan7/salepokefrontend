import React, { useContext, useState } from "react";
import _get from 'lodash.get';
import ContentEditable from "react-contenteditable";

import actions from "../../../../store/action-types";
import { Store } from "../../../../store";

const ImageText = ({ elementData }) => {
    const { state, dispatch } = useContext(Store)
    const [content, setContent] = useState(_get(elementData, 'contents', null));
    const [title, setTitle] = useState(_get(elementData, 'title', null))
    const handleChange = (value) => {
        setContent(value)
        return dispatch({
            type: actions.UPDATE_CONTENT,
            id: elementData.id,
            content: value
        })
    }
    const handleTitleChange = (value, name) => {
        setTitle(value);
        const newData = {...state,
            layers: {
                ...state.layers,
                entities: {
                    ...state.layers.entities,
                    element: {
                        ...state.layers.entities.element,
                        [elementData.id]: {
                            ...state.layers.entities.element[elementData.id],
                            [name]: value
                        }
                    }
                }
            }
        }
        return dispatch({
            type: actions.UPDATE_STATE,
            state: newData
        })
    }
  return (
    <div style={{display:"flex",flexWrap: "wrap"}}>
        {elementData.direction === "HORIZONTAL" ? 
            elementData.first === "IMAGE" ? <React.Fragment>
                <div style={{width:"50%"}}>
                    <img src={_get(elementData, 'path', '')} style={{width: _get(elementData, 'width', '100')+"%"}}/>
                </div>
                <div style={{width:"50%"}}>
                    <ContentEditable
                        html={elementData && title}
                        disabled={false}
                        tagName="h3"
                        onChange={e => handleTitleChange(e.target.value, 'title')}
                    />
                    <ContentEditable
                        html={elementData && content}
                        disabled={false}
                        tagName="p"
                        onChange={e => handleChange(e.target.value)}
                    />
                </div>
            </React.Fragment>:<React.Fragment>
                <div style={{width:"50%"}}>
                    <ContentEditable
                        html={elementData && title}
                        disabled={false}
                        tagName="h3"
                        onChange={e => handleTitleChange(e.target.value, 'title')}
                    />
                    <ContentEditable
                        html={elementData && content}
                        disabled={false}
                        tagName="p"
                        onChange={e => handleChange(e.target.value)}
                    />
                </div>
                <div style={{width:"50%"}}>
                    <img src={_get(elementData, 'path', '')} style={{width: _get(elementData, 'width', '100')+"%"}}/>
                </div>
            </React.Fragment> :
            elementData.first === "IMAGE" ? <React.Fragment>
                <div style={{width: "100%"}}>
                    <img src={_get(elementData, 'path', '')} style={{width: _get(elementData, 'width', '100')+"%"}}/>
                </div>
                <div style={{width: "100%"}}>
                    <ContentEditable
                        html={elementData && title}
                        disabled={false}
                        tagName="h3"
                        onChange={e => handleTitleChange(e.target.value, 'title')}
                    />
                    <ContentEditable
                        html={elementData && content}
                        disabled={false}
                        tagName="p"
                        onChange={e => handleChange(e.target.value)}
                    />
                </div>
            </React.Fragment>:<React.Fragment>
                <div style={{width: "100%"}}>
                    <ContentEditable
                        html={elementData && title}
                        disabled={false}
                        tagName="h3"
                        onChange={e => handleTitleChange(e.target.value, 'title')}
                    />
                    <ContentEditable
                        html={elementData && content}
                        disabled={false}
                        tagName="p"
                        onChange={e => handleChange(e.target.value)}
                    />
                </div>
                <div style={{width: "100%"}}>
                    <img src={_get(elementData, 'path', '')} style={{width: _get(elementData, 'width', '100')+"%"}}/>
                </div>
            </React.Fragment>
        }
    </div>
  );
};

export default ImageText;

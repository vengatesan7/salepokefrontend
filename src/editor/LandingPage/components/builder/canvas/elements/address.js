import React from 'react';

const Address = ({ elementData }) => {
    return (
        <div className="address-form">
            <div className="address-form-field-group">
                <div className="address-form-field">
                    <label>Full Address</label>
                    <input placeholder="Full Address..." disabled />
                </div>
            </div>
            <div className="address-form-field-group">
                <div className="address-form-field">
                    <label>City Name</label>
                    <input placeholder="City Name..." disabled />
                </div>
            </div>
            <div className="address-form-field-group">
                <div className="address-form-field">
                    <label>State / Province</label>
                    <input placeholder="State / Province..." disabled />
                </div>
                <div className="address-form-field">
                    <label>Zip Code</label>
                    <input placeholder="Zip Code..." disabled />
                </div>
            </div>
            <div className="address-form-field-group">
                <div className="address-form-field">
                    <label>Select Country</label>
                    <input placeholder="Select Country..." disabled />
                </div>
            </div>
        </div>
    )
}

export default Address;
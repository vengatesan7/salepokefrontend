import React from 'react';
import _get from 'lodash.get';

const Space = ({ elementData }) => {
    const style = {
        minHeight: "100px",
        height: _get(elementData, 'height', ""),
    }
    return (
        <div style={style}></div>
    );
}

export default Space;
import React from "react";
import _get from 'lodash.get';

const DEFAULT_URL =
  "https://res.cloudinary.com/sivadass/image/upload/v1559361306/icons/default-imge.jpg";
const DEFAULT_TAG = "default image";
const DEFAULT_STYLES = {
  display: "block",
  width: "100%"
};

const Image = ({
  elementData,
  url = DEFAULT_URL,
  altTag = DEFAULT_TAG
}) => {
  const style = {
    width: _get(elementData, 'width', '100') + "%",
  }
  return <img src={elementData.path || url} alt={altTag} style={style} />;
};

export default Image;

import React, { useContext } from 'react';
import _get from 'lodash.get';

import { Store } from '../../../../store'; 

const Icons = ({ elementData }) => {
    const { state } = useContext(Store);
    const style = {
        "width": _get(elementData, 'width', null) === null ? "auto" : `${elementData.width}%`,
        // "height": _get(elementData, 'height', "auto"),
        "background": _get(elementData, 'background', null),
        "padding-top": _get(elementData, 'paddingTop', null),
        "padding-right": _get(elementData, 'paddingRight', null),
        "padding-bottom": _get(elementData, 'paddingBottom', null),
        "padding-left": _get(elementData, 'paddingLeft', null),
        "margin-top": _get(elementData, 'marginTop', null),
        "margin-right": _get(elementData, 'marginRight', null),
        "margin-bottom": _get(elementData, 'marginBottom', null),
        "margin-left": _get(elementData, 'marginLeft', null),
        "border-width": _get(elementData, 'borderWidth', 0),
        "border-color": _get(elementData, 'borderColor', null),
        "border-style": _get(elementData, 'borderStyle', null),
        "border-radius": _get(elementData, 'borderRadius', null),
        "color": _get(elementData, 'fontColor', null),
        "display": _get(elementData, 'display', null),
        "text-decoration": _get(elementData, 'textDecoration', null)
    }
    return (
        <div className="rc-icon-widget"><i className={`fa ${elementData.contents}`}></i></div>

    );
}

export default Icons;
// style={style}
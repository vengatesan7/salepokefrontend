import React from 'react';
import _get from "lodash.get";

import { Store } from '../../../../store';

const Upsell = ({ elementData }) => {
    
    return (
        <div className="ecommerce-module" style={elementData.style}>
            <React.Fragment>
                <label>Order Detail</label>
                <div className="product-list">
                    <table className="product-list-table">
                        <thead>
                            <th>
                                Item
                            </th>
                            <th>
                                Price
                            </th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="radio" /> Product List
                                </td>
                                <td>
                                    $0.00
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </React.Fragment>            
            <div style={{textAlign: "center"}}>
                <span
                    style={{
                        ...elementData.submitButton.style,
                        margin: "0 auto", 
                        width: elementData.submitButton.submitWidth === "fittotext" ? "auto" : "100%",
                        display: "inline-block",
                        padding: "5px 15px"
                    }}
                >
                    {elementData.submitButton.submitText}
                </span>
            </div>
        </div>
    )
}

export default Upsell;
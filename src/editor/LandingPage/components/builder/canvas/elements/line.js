import React from 'react';
import _get from 'lodash.get';

const Line = ({ elementData }) => {
    // return <hr />
    const style = {
        marginTop: "10px",
        marginBottom: "10px",
        borderBottomWidth: `${_get(elementData, "size", "1")}px`,
        borderBottomColor: _get(elementData, "color", "#000000"),
        borderBottomStyle: _get(elementData, "style", "solid"),
    }
    return <div style={style} />
}

export default Line;
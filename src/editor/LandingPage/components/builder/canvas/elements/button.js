import React, { useContext, useState, useEffect } from 'react';
import _get from 'lodash.get';

import { Store } from '../../../../store';

const Button = ({ elementData }) => {
    const { 
        state,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    const [style, setStyle] = useState({});

    const otherStyle = {
        "width": _get(elementData, 'width', null) === null ? "auto" : `${elementData.width}%`,
        // "height": _get(elementData, 'height', "auto"),
        "background": _get(elementData, 'background', null),
        "border-width": _get(elementData, 'borderWidth', 0),
        "border-color": _get(elementData, 'borderColor', null),
        "border-style": _get(elementData, 'borderStyle', null),
        "border-radius": _get(elementData, 'borderRadius', null),
        "color": _get(elementData, 'fontColor', null),
        "display": _get(elementData, 'display', null),
        "text-decoration": _get(elementData, 'textDecoration', null)
    }

    const updatedStyle = () => {
        //console.log(layers);
        const updatedButtonData = layers === null ? elementData : layers.entities.element[elementData.id]
        const oldBoxModule = {
            paddingTop: _get(updatedButtonData, 'paddingTop', null),
            paddingRight: _get(updatedButtonData, 'paddingRight', null),
            paddingBottom: _get(updatedButtonData, 'paddingBottom', null),
            paddingLeft: _get(updatedButtonData, 'paddingLeft', null),
            marginTop: _get(updatedButtonData, 'marginTop', null),
            marginRight: _get(updatedButtonData, 'marginRight', null),
            marginBottom: _get(updatedButtonData, 'marginBottom', null),
            marginLeft: _get(updatedButtonData, 'marginLeft', null),
        }
        switch (state.deviceType) {
            case "phone":
                return setStyle({
                    ...otherStyle,
                    paddingTop: _get(updatedButtonData.phoneStyles, 'paddingTop', oldBoxModule.paddingTop),
                    paddingRight: _get(updatedButtonData.phoneStyles, 'paddingRight', oldBoxModule.paddingRight),
                    paddingBottom: _get(updatedButtonData.phoneStyles, 'paddingBottom', oldBoxModule.paddingBottom),
                    paddingLeft: _get(updatedButtonData.phoneStyles, 'paddingLeft', oldBoxModule.paddingLeft),
                    marginTop: _get(updatedButtonData.phoneStyles, 'marginTop', oldBoxModule.marginTop),
                    marginRight: _get(updatedButtonData.phoneStyles, 'marginRight', oldBoxModule.marginRight),
                    marginBottom: _get(updatedButtonData.phoneStyles, 'marginBottom', oldBoxModule.marginBottom),
                    marginLeft: _get(updatedButtonData.phoneStyles, 'marginLeft', oldBoxModule.marginLeft),
                });
            case "tablet":
                return setStyle({
                    ...otherStyle,
                    paddingTop: _get(updatedButtonData.tabletStyles, 'paddingTop', oldBoxModule.paddingTop),
                    paddingRight: _get(updatedButtonData.tabletStyles, 'paddingRight', oldBoxModule.paddingRight),
                    paddingBottom: _get(updatedButtonData.tabletStyles, 'paddingBottom', oldBoxModule.paddingBottom),
                    paddingLeft: _get(updatedButtonData.tabletStyles, 'paddingLeft', oldBoxModule.paddingLeft),
                    marginTop: _get(updatedButtonData.tabletStyles, 'marginTop', oldBoxModule.marginTop),
                    marginRight: _get(updatedButtonData.tabletStyles, 'marginRight', oldBoxModule.marginRight),
                    marginBottom: _get(updatedButtonData.tabletStyles, 'marginBottom', oldBoxModule.marginBottom),
                    marginLeft: _get(updatedButtonData.tabletStyles, 'marginLeft', oldBoxModule.marginLeft),
                });
            default:
                return setStyle({
                    ...otherStyle,
                    paddingTop: _get(updatedButtonData.styles, 'paddingTop', oldBoxModule.paddingTop),
                    paddingRight: _get(updatedButtonData.styles, 'paddingRight', oldBoxModule.paddingRight),
                    paddingBottom: _get(updatedButtonData.styles, 'paddingBottom', oldBoxModule.paddingBottom),
                    paddingLeft: _get(updatedButtonData.styles, 'paddingLeft', oldBoxModule.paddingLeft),
                    marginTop: _get(updatedButtonData.styles, 'marginTop', oldBoxModule.marginTop),
                    marginRight: _get(updatedButtonData.styles, 'marginRight', oldBoxModule.marginRight),
                    marginBottom: _get(updatedButtonData.styles, 'marginBottom', oldBoxModule.marginBottom),
                    marginLeft: _get(updatedButtonData.styles, 'marginLeft', oldBoxModule.marginLeft),
                });

        }
    }
    useEffect(() => {
        updatedStyle();
    }, [stateHistory]);
    useEffect(() => {
        updatedStyle();
    }, [state.deviceType]);
    return (
        <button style={style}>{elementData.contents}</button>
    );
}

export default Button;
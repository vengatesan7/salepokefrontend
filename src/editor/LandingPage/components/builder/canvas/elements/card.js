import React from 'react';

const Card = ({ elementData }) => {
    return (
        <div className="card-form">
            <div className="card-form-field-group">
                <div className="card-form-field">
                    <label className="">Credit Card Number *:</label>
                    <input className="" placeholder="Card number" type="text" disabled />
                </div>
            </div>
            <div className="card-form-fields">
                <div className="card-form-field-group-left card-form-field-group">
                    <div className="card-form-field">
                        <label className="">Expiry Month *:</label>
                        <select className="" disabled>
                            <option>01</option>
                            <option>02</option>
                            <option>03</option>
                            <option>04</option>
                            <option>05</option>
                            <option>06</option>
                            <option>07</option>
                            <option>08</option>
                            <option>09</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                        </select>
                    </div>
                    <div className="card-form-field">
                        <label cclassName="">Expiry Year *:</label>
                        <select className="" disabled>
                            <option>2020</option>
                            <option>2021</option>
                            <option>2022</option>
                            <option>2023</option>
                            <option>2024</option>
                            <option>2025</option>
                            <option>2026</option>
                            <option>2027</option>
                            <option>2028</option>
                            <option>2029</option>
                            <option>2030</option>
                            <option>2031</option>
                        </select>
                    </div>
                </div>
                <div className="card-form-field-group-right card-form-field-group">
                    <div className="card-form-field">
                        <div className="card-form">
                            <label className="" >CVC Code *:</label>
                            <input className="" placeholder="CVC" type="text" disabled />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card;
import React from 'react';
import _get from "lodash.get";

import { Store } from '../../../../store';

const Product = ({ elementData }) => {
    
    return (
        <div className="ecommerce-module" style={elementData.style}>
            {elementData.checkoutFormData.orderDetail && <React.Fragment>
                <label>Order Detail</label>
                <div className="product-list">
                    <table className="product-list-table">
                        <thead>
                            <th>
                                Item
                            </th>
                            <th>
                                Price
                            </th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input type="radio" /> Product List
                                </td>
                                <td>
                                    $0.00
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </React.Fragment>}
            {elementData.checkoutFormData.profileInfo && <React.Fragment>
                <label>Profile Information</label>
                <div className="address-form">
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>Name</label>
                            <input placeholder="Full Name..." disabled />
                        </div>
                    </div>
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>Email</label>
                            <input placeholder="Email..." disabled />
                        </div>
                        <div className="address-form-field">
                            <label>Phone</label>
                            <input placeholder="Phone..." disabled />
                        </div>
                    </div>
                </div>
            </React.Fragment>}
            {elementData.checkoutFormData.paymentMethod && <React.Fragment>
                <label>Payment method</label>
                <div className="card-form">
                    <div className="card-form-field-group">
                        <div className="card-form-field">
                            <label className="">Credit Card Number *:</label>
                            <input className="" placeholder="Card number" type="text" disabled />
                        </div>
                    </div>
                    <div className="card-form-fields">
                        <div className="card-form-field-group-left card-form-field-group">
                            <div className="card-form-field">
                                <label className="">Expiry Month *:</label>
                                <input className="" placeholder="mm" type="text" disabled />
                            </div>
                            <div className="card-form-field">
                                <label cclassName="">Expiry Year *:</label>
                                <input className="" placeholder="yyyy" type="text" disabled />
                            </div>
                        </div>
                        <div className="card-form-field-group-right card-form-field-group">
                            <div className="card-form-field">
                                <div className="card-form">
                                    <label className="" >CVC Code *:</label>
                                    <input className="" placeholder="CVC" type="text" disabled />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>}
            {elementData.checkoutFormData.billingDetail && <React.Fragment>
                <label>Billing Detail</label>
                <div className="address-form">
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>Full Address</label>
                            <input placeholder="Full Address..." disabled />
                        </div>
                    </div>
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>City Name</label>
                            <input placeholder="City Name..." disabled />
                        </div>
                    </div>
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>State / Province</label>
                            <input placeholder="State / Province..." disabled />
                        </div>
                        <div className="address-form-field">
                            <label>Zip Code</label>
                            <input placeholder="Zip Code..." disabled />
                        </div>
                    </div>
                    <div className="address-form-field-group">
                        <div className="address-form-field">
                            <label>Select Country</label>
                            <input placeholder="Select Country..." disabled />
                        </div>
                    </div>
                </div>
            </React.Fragment>}
            <div style={{textAlign: "center"}}>
                <span
                    style={{
                        ...elementData.submitButton.style,
                        margin: "0 auto", 
                        width: elementData.submitButton.submitWidth === "fittotext" ? "auto" : "100%",
                        display: "inline-block",
                        padding: "5px 15px"
                    }}
                >
                    {elementData.submitButton.submitText}
                </span>
            </div>
        </div>
    )
}

export default Product;
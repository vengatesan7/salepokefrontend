import React, { useContext, useState, useRef, useCallback, useEffect } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import uuid from "uuid/v4";
import _get from "lodash.get";
import classNames from "classnames";

import ElementWrapper from "./element-wrapper";
import { Store } from "../../../store";
import actions from "../../../store/action-types";
import { relative } from "path";

const getListStyle = (isDraggingOver, width, background) => ({
  background: isDraggingOver ? "#2196f3" : background,
  margin: "0 0 0 0",
  width: width
  // transform: "none !important"
});

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  // change background colour if dragging
  background: isDragging ? "lightyellow" : "inherit",
  // display: "inline-flex",

  // styles we need to apply on draggables
  ...draggableStyle,
  // padding: "10px",
  display: "block",
  maxWidth: isDragging ? "48px !important" : "auto",
  height: isDragging ? "48px !important" : "auto"
  // overflow: isDragging ? "hidden !important" : "initial",
});

const Column = ({
  rowId,
  columnId,
  columnIndex,
  // columnData,
  elementsData,
  wrapperData,
  columnCount,
  nextColumn,
  onDragStart,
  sectionId,
  selectedColumnDragId,
  type
}) => {
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory, set }
  } = useContext(Store);
  const [columnStyle, setColumnStyle] = useState({});
  const [columnData, setColumnData] = useState(layers.entities.columns[columnId])

  const updateLayers = data => {
    return dispatch({
      type: actions.UPDATE_LAYER,
      payload: data
    });
  };

  // const columnStyle = {
  //   display: "inline-block",
  //   width: _get(columnData, "width", null) !== null ? `${columnData.width}%` : `${100 / columnCount}%`,
  //   "background-color": _get(columnData.styles, "background", ""),
  //   "background-image": _get(columnData.styles, "backgroundImage", null) && `url(${_get(columnData.styles, "backgroundImage", null)})`,
  //   "padding-top": _get(columnData.styles, "paddingTop", ""),
  //   "padding-right": _get(columnData.styles, "paddingRight", ""),
  //   "padding-bottom": _get(columnData.styles, "paddingBottom", ""),
  //   "padding-left": _get(columnData.styles, "paddingLeft", ""),
  //   "margin-top": _get(columnData.styles, "marginTop", ""),
  //   "margin-bottom": _get(columnData.styles, "marginBottom", ""),
  //   // "margin-right": _get(columnData.styles, "marginRight", ""),
  //   // "margin-left": _get(columnData.styles, "marginLeft", "")
  // };

  const columnStyleUpdate = () => {
    const newColumnData = stateHistory.layers.entities.columns[columnId];
    return setColumnStyle({
      display: "inline-block",
      width: _get(newColumnData, "width", null) !== null ? `${newColumnData.width}%` : `${100 / columnCount}%`,
      backgroundColor: _get(newColumnData.styles, "background", ""),
      backgroundImage: _get(newColumnData.styles, "backgroundImage", null) && `url(${_get(newColumnData.styles, "backgroundImage", null)})`,
      paddingTop: _get(newColumnData.styles, "paddingTop", ""),
      paddingRight: _get(newColumnData.styles, "paddingRight", ""),
      paddingBottom: _get(newColumnData.styles, "paddingBottom", ""),
      paddingLeft: _get(newColumnData.styles, "paddingLeft", ""),
      marginTop: _get(newColumnData.styles, "marginTop", ""),
      marginBottom: _get(newColumnData.styles, "marginBottom", "")
    });
  }

  const columnStyleWhileDeviceChange = () => {
    const newColumnData = stateHistory.layers.entities.columns[columnId];
    const desktopStyle = {
      backgroundColor: _get(newColumnData.styles, "background", ""),
      backgroundImage: _get(newColumnData.styles, "backgroundImage", null) && `url(${_get(newColumnData.styles, "backgroundImage", null)})`,
      paddingTop: _get(newColumnData.styles, "paddingTop", ""),
      paddingRight: _get(newColumnData.styles, "paddingRight", ""),
      paddingBottom: _get(newColumnData.styles, "paddingBottom", ""),
      paddingLeft: _get(newColumnData.styles, "paddingLeft", ""),
      marginTop: _get(newColumnData.styles, "marginTop", ""),
      marginBottom: _get(newColumnData.styles, "marginBottom", ""),
    }
    switch (state.deviceType) {
      case "phone":
        return setColumnStyle({
          display: "inline-block",
          width: _get(newColumnData, "width", null) !== null ? `${newColumnData.width}%` : `${100 / columnCount}%`,
          backgroundColor: _get(newColumnData.phoneStyles, "background", desktopStyle.backgroundColor),
          backgroundImage: `url(${_get(newColumnData.phoneStyles, "backgroundImage", desktopStyle.backgroundImage)})`,
          paddingTop: _get(newColumnData.phoneStyles, "paddingTop", desktopStyle.paddingTop),
          paddingRight: _get(newColumnData.phoneStyles, "paddingRight", desktopStyle.paddingRight),
          paddingBottom: _get(newColumnData.phoneStyles, "paddingBottom", desktopStyle.paddingBottom),
          paddingLeft: _get(newColumnData.phoneStyles, "paddingLeft", desktopStyle.paddingLeft),
          marginTop: _get(newColumnData.phoneStyles, "marginTop", desktopStyle.marginTop),
          marginBottom: _get(newColumnData.phoneStyles, "marginBottom", desktopStyle.marginBottom),
        });
      case "tablet":
        return setColumnStyle({
          display: "inline-block",
          width: _get(newColumnData, "width", null) !== null ? `${newColumnData.width}%` : `${100 / columnCount}%`,
          backgroundColor: _get(newColumnData.tabletStyles, "background", desktopStyle.backgroundColor),
          backgroundImage: `url(${_get(newColumnData.tabletStyles, "backgroundImage", desktopStyle.backgroundImage)})`,
          paddingTop: _get(newColumnData.tabletStyles, "paddingTop", desktopStyle.paddingTop),
          paddingRight: _get(newColumnData.tabletStyles, "paddingRight", desktopStyle.paddingRight),
          paddingBottom: _get(newColumnData.tabletStyles, "paddingBottom", desktopStyle.paddingBottom),
          paddingLeft: _get(newColumnData.tabletStyles, "paddingLeft", desktopStyle.paddingLeft),
          marginTop: _get(newColumnData.tabletStyles, "marginTop", desktopStyle.marginTop),
          marginBottom: _get(newColumnData.tabletStyles, "marginBottom", desktopStyle.marginBottom),
        });
      case "desktop":
        return setColumnStyle({
          display: "inline-block",
          width: _get(newColumnData, "width", null) !== null ? `${newColumnData.width}%` : `${100 / columnCount}%`,
          backgroundColor: desktopStyle.backgroundColor,
          backgroundImage: desktopStyle.backgroundImage,
          paddingTop: desktopStyle.paddingTop,
          paddingRight: desktopStyle.paddingRight,
          paddingBottom: desktopStyle.paddingBottom,
          paddingLeft: desktopStyle.paddingLeft,
          marginTop: desktopStyle.marginTop,
          marginBottom: desktopStyle.marginBottom
        });
      default:
        return setColumnStyle({
          display: "inline-block",
          width: _get(newColumnData, "width", null) !== null ? `${newColumnData.width}%` : `${100 / columnCount}%`,
          backgroundColor: desktopStyle.backgroundColor,
          backgroundImage: desktopStyle.backgroundImage,
          paddingTop: desktopStyle.paddingTop,
          paddingRight: desktopStyle.paddingRight,
          paddingBottom: desktopStyle.paddingBottom,
          paddingLeft: desktopStyle.paddingLeft,
          marginTop: desktopStyle.marginTop,
          marginBottom: desktopStyle.marginBottom
        });
    }
  }

  useEffect(() => {
    setColumnData(layers.entities.columns[columnId]);
    state.deviceType === "desktop" ? columnStyleUpdate() : columnStyleWhileDeviceChange();
  }, [stateHistory]);

  useEffect(() => {
    columnStyleWhileDeviceChange();
  }, [state.deviceType]);

  // if (columnData.width === undefined) {
  //   const columnArr = layers.entities.rows[rowId].columns
  //   const columnCount = columnArr.length
  //   const newWidth = 100 / columnCount;
  //   const newArr = []
  //   const promises = columnArr.map((thisColumnId, index) => {
  //     const newObj = Object.assign({}, {
  //       ...stateHistory.layers.entities.columns[thisColumnId],
  //       width: newWidth
  //     })
  //     // newArr.push({
  //     //   [thisColumnId]: {
  //     //     ...stateHistory.layers.entities.columns[thisColumnId],
  //     //     width: newWidth
  //     //   }
  //     // });
  //     console.log(newObj)
  //     return newObj;
  //   });

  //   Promise.all(promises).then(data => function() {
  //     console.log(data)
  //   });
  // }
  const handleLeftColumn = (colId, colIndex) => {
    const newColumnId = uuid();
    const newColumn = {
      [newColumnId]: {
        id: newColumnId,
        width: "200px",
        elements: []
      }
    };
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn
        },
        rows: {
          ...layers.entities.rows,
          [rowId]: {
            columns: [
              ...layers.entities.rows[rowId].columns.slice(0, colIndex),
              newColumnId,
              ...layers.entities.rows[rowId].columns.slice(colIndex)
            ]
          }
        }
      }
    };
    updateLayers(data);
  };
  const handleRightColumn = (colId, colIndex) => {
    const newColIndex = colIndex + 1;
    const newColumnId = uuid();
    const newColumn = {
      [newColumnId]: {
        id: newColumnId,
        width: "200px",
        elements: []
      }
    };
    const data = {
      ...layers,
      entities: {
        ...layers.entities,
        column: {
          ...layers.entities.column,
          ...newColumn
        },
        rows: {
          ...layers.entities.rows,
          [rowId]: {
            columns: [
              ...layers.entities.rows[rowId].columns.slice(0, newColIndex),
              newColumnId,
              ...layers.entities.rows[rowId].columns.slice(newColIndex)
            ]
          }
        }
      }
    };
    updateLayers(data);
  };
  const editStyle = data => {
    // const overlayHeight = document.getElementById('page-editor-main-container').clientHeight;
    // console.log(overlayHeight);
    return dispatch({
      type: actions.SELECT_ELEMENT,
      payload: data.id
    });
  }
  const deleteElement = (elementData, wrapperId, columnId) => {
    const newElementArr = layers.entities.wrappers[wrapperId].elements;
    const elementPosition = newElementArr.indexOf(elementData.id);
    newElementArr.splice(elementPosition, 1);
    if (newElementArr.length === 0) {
      const newWrapperArr = layers.entities.columns[columnId].wrappers;
      const wrapperPosition = newWrapperArr.indexOf(wrapperId);
      newWrapperArr.splice(wrapperPosition, 1);
      const updatedData = {
        ...layers,
        entities: {
          ...layers.entities,
          columns: {
            ...layers.entities.columns,
            [columnId]: {
              ...layers.entities.columns[columnId],
              wrappers: newWrapperArr
            }
          },
          wrappers: {
            ...layers.entities.wrappers,
            [wrapperId]: {
              ...layers.entities.wrappers[wrapperId],
              elements: newElementArr
            }
          }
        }
      }
      return updateLayers(updatedData);
    } else {
      const updatedData = {
        ...layers,
        entities: {
          ...layers.entities,
          wrappers: {
            ...layers.entities.wrappers,
            [wrapperId]: {
              ...layers.entities.wrappers[wrapperId],
              elements: newElementArr
            }
          }
        }
      }
      return updateLayers(updatedData);
    }
  }
  const cloneElement = (elementsData, wrapperId) => {
    const newId = uuid();
    const newElementsList = { ...layers.entities.element, [newId]: { ...elementsData, id: newId } }
    const newElementArr = layers.entities.wrappers[wrapperId].elements;

    const elementPosition = newElementArr.indexOf(elementsData.id);
    newElementArr.splice(elementPosition + 1, 0, newId);

    const updatedData = {
      ...layers,
      entities: {
        ...layers.entities,
        element: newElementsList,
        wrappers: {
          ...layers.entities.wrappers,
          [wrapperId]: {
            ...layers.entities.wrappers[wrapperId],
            elements: newElementArr
          }
        }
      }
    }
    return updateLayers(updatedData);
  }
  const newDropedPosition = (position, wrapperId, columnId) => {
    return dispatch({
      type: actions.DROP_POSITION,
      position: position,
      wrapperId: wrapperId,
      columnId: columnId
    });
  }
  const sameDropedPosition = (position, elementId, wrapperId) => {
    return dispatch({
      type: actions.DROP_SAME_WRAPPER_POSITION,
      position: position,
      elementId: elementId,
      wrapperId: wrapperId
    });
  }
  const elementTakenFrom = (columnId, wrapperId) => {
    return dispatch({
      type: actions.ELEMENT_TAKEN_FROM,
      columnId: columnId,
      wrapperId: wrapperId
    });
  }
  const firstElement = (position, columnId) => {
    return dispatch({
      type: actions.FIRST_ELEMENT_DROP,
      position: position,
      columnId: columnId
    });
  }

  const columnBar = (columnId) => {
    return dispatch({
        type: actions.TOGGLE_COLUMN_STYLE,
        column: columnId
    });
  }

  const deleteColumn = (colId, roId, secId) => {
    const columnArr = layers.entities.rows[roId].columns;
    const lenthOfRow = columnArr.length;
    const position = columnArr.indexOf(colId);
    const deletingSpace = layers.entities.columns[colId].width;
    if (lenthOfRow === 1) {
      const rowArr = [...layers.entities.section[secId].rows];
      const rowPosition = rowArr.indexOf(roId);
      rowArr.splice(rowPosition, 1);
      const lastColumnDelete = {
        ...layers,
        entities: {
          ...layers.entities,
          section: {
            ...layers.entities.section,
            [secId] : {
              ...layers.entities.section[secId],
              rows: rowArr
            }
          }
        }
      }
      return updateLayers(lastColumnDelete);
    } else {
      if (position !== 0) {
        const prevColumnId = columnArr[position - 1];
        const prevColumnWidth = layers.entities.columns[prevColumnId].width;
        columnArr.splice(position, 1);
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                rows: {
                    ...layers.entities.rows,
                    [roId]: {
                        ...layers.entities.rows[roId],
                        columns: columnArr
                    }
                },
                columns: {
                    ...layers.entities.columns,
                    [prevColumnId]: {
                        ...layers.entities.columns[prevColumnId],
                        width: prevColumnWidth + deletingSpace
                    }
                }
            }                
        }
        return updateLayers(newData);
      } else {
        const nextColumnId = columnArr[position + 1];
        const nextColumnWidth = layers.entities.columns[nextColumnId].width;
        columnArr.splice(position, 1);
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                rows: {
                    ...layers.entities.rows,
                    [roId]: {
                        ...layers.entities.rows[roId],
                        columns: columnArr
                    }
                },
                columns: {
                    ...layers.entities.columns,
                    [nextColumnId]: {
                        ...layers.entities.columns[nextColumnId],
                        width: nextColumnWidth + deletingSpace
                    }
                }
            }                
        }
        return updateLayers(newData);
      }

    }
  }

  return (
    <Droppable droppableId={columnId} key={columnId} type="wrapper" direction="vertical">
      {(provided, snapshot) => (
        <React.Fragment>
          <div
            ref={provided.innerRef}
            // style={getListStyle(snapshot.isDraggingOver, columnStyle)}
            style={columnData.wrappers.length === 0 ? { ...columnStyle, height: "100px" } : columnStyle}
            id={columnId}
            className={classNames("column", {
              "is-dropping": snapshot.isDraggingOver
            }, state.activeElement === null && "hover")}
          >
            <span
              className={classNames("column-btn", state.activeElement === null && "active")}
              onClick={() => columnBar(columnId)}
            >
              Column
            </span>
            <span onClick={() => deleteColumn(columnId, rowId, sectionId)} className={classNames("column-btn column-delete", state.activeElement === null && "active")}>
              <i className="material-icons">delete</i>
            </span>
            {columnData.wrappers.length === 0 ? <div className="column-inner-layer" style={{ height: "100px" }}>
              <div
                className="drop-area top empty-column"
                onMouseUp={() => firstElement("first", columnId)}
                style={{ 
                  height: "100%",
                  display: "block",
                }}
              >
                Drop your widget here...
              </div>
            </div> : columnData.wrappers.map((wrapperId, index) => {
              return (
                <Draggable id={wrapperId} draggableId={wrapperId} index={index}>
                  {(dragProvided, snapshot) => {
                    return (
                      <div className={classNames("column-inner-layer", state.isDragging && "dragging")} >
                        {state.isDragging && <div 
                          className="drop-area top" 
                          onMouseUp={() => newDropedPosition("top", wrapperId, columnId)}
                          style={layers.entities.wrappers[wrapperId].elements.length !== 0 ? { textAlign: "center", padding: "5px" } : { textAlign: "center", padding: "5px", height: "250px" }}>
                            Drop here!
                        </div>}
                        <div
                          ref={dragProvided.innerRef}
                          {...dragProvided.draggableProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            dragProvided.draggableProps.style
                          )}
                          className="element-wrapper"
                        >
                          <span {...dragProvided.dragHandleProps} className="row-move hidden">
                            <i className="material-icons">unfold_more</i>
                          </span>
                          <Droppable droppableId={wrapperId} key={wrapperId} type="element" >
                            {(elementProvided, elementSnapshot) => (
                              <div
                                ref={elementProvided.innerRef}
                                // className={classNames("element-container", {"onDragging" : elementSnapshot.isDraggingOver})}
                                className="element-container"
                              >
                                {layers.entities.wrappers[wrapperId] && layers.entities.wrappers[wrapperId].elements.map((elementId, index) => {
                                  const totalColumns = layers.entities.wrappers[wrapperId].elements.length;
                                  const columnWidth = 100 / totalColumns;
                                  return (
                                    <Draggable id={elementId} draggableId={elementId} index={index}>
                                      {(elementDragProvided, elementDragSnapshot) => {
                                        return (
                                          <React.Fragment>
                                            {/* <div className="element-inner-layer"> */}
                                            {state.isDragging && <div className="drop-area left" onMouseUp={() => sameDropedPosition("left", elementId, wrapperId)}
                                              style={{ position: "relative" }}>
                                              <span style={{
                                                transform: "rotate(-90deg)",
                                                display: "inline-block",
                                                width: "70px",
                                                position: "absolute",
                                                top: "calc(50% - 15px)",
                                                left: "-20px"
                                              }}>Drop here!</span>
                                            </div>}
                                            <div
                                              ref={elementDragProvided.innerRef}
                                              {...elementDragProvided.draggableProps}
                                              // style={!elementSnapshot.isDragging && {"flex-basis": "100%"}}
                                              // style={{"width": columnWidth + "%"}}
                                              className={classNames("element", elementSnapshot.isDragging && "dragging", elementDragSnapshot.isDragging && "element-dragging")}
                                              onMouseDown={() => elementTakenFrom(columnId, wrapperId)}
                                            >
                                              <span 
                                                {...elementDragProvided.dragHandleProps}
                                                className="element-drag-handler"
                                                onClick={() => editStyle(elementsData[elementId])} 
                                                // onDoubleClick={() => editStyle(elementsData[elementId])}
                                              >
                                                <i className="material-icons">control_camera</i>
                                              </span>
                                              <span 
                                                {...elementDragProvided.dragHandleProps}
                                                className="element-drag-handler drag-handler-show"
                                              >
                                                <i className="material-icons">control_camera</i>
                                              </span>
                                              <span onClick={() => editStyle(elementsData[elementId])} className="element-edit-handler">
                                                <i className="material-icons">edit</i>
                                              </span>
                                              <span onClick={() => deleteElement(elementsData[elementId], wrapperId, columnId)} className="element-delete-handler">
                                                <i className="material-icons">delete</i>
                                              </span>
                                              <span onClick={() => cloneElement(elementsData[elementId], wrapperId)} className="element-copy-handler">
                                                <i className="material-icons">content_copy</i>
                                              </span>
                                              {elementsData && elementDragSnapshot.isDragging !== true ?
                                                <React.Fragment>
                                                  <ElementWrapper key={elementId} elementData={elementsData[elementId]} />
                                                </React.Fragment>
                                                :
                                                <span {...provided.dragHandleProps} className="element-drag-handler" style={{ "visibility": "visible" }}>
                                                  <i className="material-icons">control_camera</i>
                                                </span>
                                              }
                                            </div>
                                            {state.isDragging && <div className="drop-area right" onMouseUp={() => sameDropedPosition("right", elementId, wrapperId)}
                                              style={{ position: "relative" }}>
                                              <span style={{
                                                transform: "rotate(-90deg)",
                                                display: "inline-block",
                                                width: "70px",
                                                position: "absolute",
                                                top: "calc(50% - 15px)",
                                                left: "-20px"
                                              }}>Drop here!</span>
                                            </div>}
                                          </React.Fragment>
                                        );
                                      }}
                                    </Draggable>
                                  )
                                })}
                                {elementProvided.placeholder}
                              </div>
                            )}
                          </Droppable>
                        </div>
                        {state.isDragging && <div className="drop-area bottom" onMouseUp={() => newDropedPosition("bottom", wrapperId, columnId)} style={{ textAlign: "center", padding: "5px" }}>Drop here!</div>}
                      </div>
                    )
                  }}
                </Draggable>
              );
            })}
            {/* {provided.placeholder} */}
            {nextColumn !== undefined && <div 
              className={classNames("column-width-controler", state.activeElement !== null && "hidden")}
              onMouseDownCapture={() => selectedColumnDragId(columnId)}
              onMouseDown={onDragStart}
              onTouchStart={onDragStart}
            >
              <i class="material-icons" 
              style={{
                position: "absolute",
                left: "-9px",
                fontSize: "16px",
                top: "calc(50% - 8px)",
              }}
              >
                code
              </i>
              <span>{Math.round(columnData.width)}</span>
            </div>}
          </div>
        </React.Fragment>
      )}
    </Droppable>
  );
};

export default Column;

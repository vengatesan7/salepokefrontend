import React, { useContext, useEffect, useState } from 'react';
import _get from "lodash.get";

import { Store } from '../../../store';

function Container(props) {
    const {
        history: { state: stateHistory }
    } = useContext(Store);
    const [pageStyle, setPageStyle] = useState({});
    const newStyle = {
        width: _get(pageStyle, "width", ""),
        maxWidth: "100%",
        margin: "0 auto"
    }
    useEffect(() => {
        setPageStyle(_get(stateHistory, 'layers.result.page', {}));
    }, [stateHistory]);
    return (
        <div className="container" style={newStyle}>
            {props.children}
        </div>
    );
}

export default Container;
import React, { useContext, useEffect, useState, useRef, useCallback } from "react";
import classNames from "classnames";
import _get from "lodash.get";
import uuid from 'uuid/v4';

import actions from "../../../store/action-types";
import { Store } from "../../../store";
import ColumnItem from "./column-item";
import PageContainer from "./page-container";
import Container from "./container";

import "./canvas.scss";
import SectionSticky from "./sticky";

const Section = ({
  sectionData,
  rowsData,
  columnsData,
  elementsData,
  wrapperData,
  sectionIndex,
  ...otherProps
}) => {
  const {
    state,
    dispatch,
    state: { layers },
    history: { state: stateHistory }
} = useContext(Store);
  const [isHover, setIsHover] = useState(false);
  const [style, setStyle] = useState({});  
  const [newSectionPopup, setNewSectionPopup] = useState({
    enable: false,
    id: null
  })
  // const style = ;

  const getData = () => {
    const updatedSectionData = sectionData !== undefined && layers.entities.section[sectionData.id];    
    const desktopStyle = {
      backgroundColor: _get(updatedSectionData, "sectionsData.styles.background", ""),
      backgroundImage: _get(updatedSectionData, "sectionsData.styles.backgroundImage", ""),      
      paddingTop: _get(updatedSectionData, "sectionsData.styles.paddingTop", ""),
      paddingBottom: _get(updatedSectionData, "sectionsData.styles.paddingBottom", ""),
      paddingRight: _get(updatedSectionData, "sectionsData.styles.paddingRight", ""),
      paddingLeft: _get(updatedSectionData, "sectionsData.styles.paddingLeft", ""),     
    }    
    switch (state.deviceType) {
      case "phone":
        return setStyle({
          backgroundColor: _get(updatedSectionData, "sectionsData.phoneStyles.background", desktopStyle.backgroundColor),
          backgroundImage:
          "url(" +
          _get(updatedSectionData, "sectionsData.phoneStyles.backgroundImage", desktopStyle.backgroundImage) +
          ")",
          backgroundSize: "cover",
          backgroundPosition: "center",
          paddingTop: _get(updatedSectionData, "sectionsData.phoneStyles.paddingTop", desktopStyle.paddingTop),
          paddingBottom: _get(updatedSectionData, "sectionsData.phoneStyles.paddingBottom", desktopStyle.paddingBottom),
          paddingRight: _get(updatedSectionData, "sectionsData.phoneStyles.paddingRight", desktopStyle.paddingRight),
          paddingLeft: _get(updatedSectionData, "sectionsData.phoneStyles.paddingLeft", desktopStyle.paddingLeft)
          });        
      case "tablet":
        return setStyle({
          backgroundColor: _get(updatedSectionData, "sectionsData.tabletStyles.background", desktopStyle.backgroundColor),
          backgroundImage:
          "url(" +
          _get(updatedSectionData, "sectionsData.tabletStyles.backgroundImage", desktopStyle.backgroundImage) +
          ")",
          backgroundSize: "cover",
          backgroundPosition: "center",
          paddingTop: _get(updatedSectionData, "sectionsData.tabletStyles.paddingTop", desktopStyle.paddingTop),
          paddingBottom: _get(updatedSectionData, "sectionsData.tabletStyles.paddingBottom", desktopStyle.paddingBottom),
          paddingRight: _get(updatedSectionData, "sectionsData.tabletStyles.paddingRight", desktopStyle.paddingRight),
          paddingLeft: _get(updatedSectionData, "sectionsData.tabletStyles.paddingLeft", desktopStyle.paddingLeft)
          });    
      default:
        return setStyle({
          backgroundColor: desktopStyle.backgroundColor,
          backgroundImage: "url(" + desktopStyle.backgroundImage + ")",
          backgroundSize: "cover",
          backgroundPosition: "center",
          paddingTop: desktopStyle.paddingTop,
          paddingBottom: desktopStyle.paddingBottom,
          paddingRight: desktopStyle.paddingRight,
          paddingLeft: desktopStyle.paddingLeft
        });
    }
  }

  useEffect(() => {
    getData();
  }, [stateHistory]);
  useEffect(() => {
    getData();
  }, [state.deviceType]);

  const handleHover = () => {
    setIsHover(true);
  };
  const handleHoverOut = () => {
    setIsHover(false);
  };
  const sectionBar = () => {
    return dispatch({
      type: actions.TOGGLE_SECTION_STYLE,
      section: sectionData.id
    });
  };

  const updateLayers = data => {
    return dispatch({
        type: actions.UPDATE_LAYER,
        payload: data
    });
};

  const addNewSection = (type, index) => {
    setNewSectionPopup({
      enable: false,
      id: null
    });
    const newSectionId = uuid();
    const newRowId = uuid();
    const newColumnid = uuid();
    const newColumnidTwo = uuid();
    const newColumnidThree = uuid();
    const newSectionOrder = state.layers.result.sections
    newSectionOrder.splice(index + 1, 0, newSectionId);
    switch (type) {
      case "100":
        const data100 = {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            columns: {
              ...state.layers.entities.columns,
              [newColumnid]: {
                id: newColumnid,
                width: 100,
                wrappers: []
              }
            },
            rows: {
              ...state.layers.entities.rows,
              [newRowId]: {
                id: newRowId,
                rowData: {
                  name: "Name Your Row"
                },
                columns: [newColumnid]
              }
            },
            section: {
              ...state.layers.entities.section,
              [newSectionId]: {
                id: newSectionId,
                sectionsData: {
                    name: "New Section"
                },
                rows: [newRowId]
              }
            }
          },
          result: {
            ...state.layers.result,
            sections: newSectionOrder
          }
        }
        return updateLayers(data100);
      case "50_50":
        const data5050 = {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            columns: {
              ...state.layers.entities.columns,
              [newColumnid]: {
                id: newColumnid,
                width: 50,
                wrappers: []
              },
              [newColumnidTwo]: {
                id: newColumnidTwo,
                width: 50,
                wrappers: []
              }
            },
            rows: {
              ...state.layers.entities.rows,
              [newRowId]: {
                id: newRowId,
                rowData: {
                  name: "Name Your Row"
                },
                columns: [newColumnid, newColumnidTwo]
              }
            },
            section: {
              ...state.layers.entities.section,
              [newSectionId]: {
                id: newSectionId,
                sectionsData: {
                    name: "New Section"
                },
                rows: [newRowId]
              }
            }
          },
          result: {
            ...state.layers.result,
            sections: newSectionOrder
          }
        }
        return updateLayers(data5050);
      case "33_33_33":
        const data333333 = {
          ...state.layers,
          entities: {
            ...state.layers.entities,
            columns: {
              ...state.layers.entities.columns,
              [newColumnid]: {
                id: newColumnid,
                width: 33.33,
                wrappers: []
              },
              [newColumnidTwo]: {
                id: newColumnidTwo,
                width: 33.33,
                wrappers: []
              },
              [newColumnidThree]: {
                id: newColumnidThree,
                width: 33.33,
                wrappers: []
              }
            },
            rows: {
              ...state.layers.entities.rows,
              [newRowId]: {
                id: newRowId,
                rowData: {
                  name: "Name Your Row"
                },
                columns: [newColumnid, newColumnidTwo, newColumnidThree]
              }
            },
            section: {
              ...state.layers.entities.section,
              [newSectionId]: {
                id: newSectionId,
                sectionsData: {
                    name: "New Section"
                },
                rows: [newRowId]
              }
            }
          },
          result: {
            ...state.layers.result,
            sections: newSectionOrder
          }
        }
        return updateLayers(data333333);
      default:
        break;
    }
  }
  const deleteSection = (id) => {
    const sectionsArr = stateHistory.layers.result.sections
    const position = sectionsArr.indexOf(id);
    sectionsArr.splice(position, 1);

    const sectionList = stateHistory.layers.entities.section;
    const numRows = _get(sectionList[id], 'rows', null);

    const rowList = stateHistory.layers.entities.rows;
    const columnList = stateHistory.layers.entities.columns;
    const wrapperList = stateHistory.layers.entities.wrappers;
    const elementList = stateHistory.layers.entities.element;

    numRows && numRows.map(rowId => {
        rowList[rowId].columns.map(columnId => {
            columnList[columnId].wrappers.map(wrapperId => {
                wrapperList[wrapperId].elements.map(elementId => {
                    delete elementList[elementId];
                });

                delete wrapperList[wrapperId];
            });
            delete columnList[columnId];
        });
        delete rowList[rowId];
    });
    delete sectionList[id];

    const data = {
        ...layers,
        entities: {
            ...layers.entities,
            section: sectionList,
            rows: rowList,
            columns: columnList,
            wrappers: wrapperList,
            element: elementList
        },
        result: {
            ...layers.result,
            sections: sectionsArr
        }
    }
    return updateLayers(data);
  }  
  return (
    <React.Fragment>
      <section
        id={_get(sectionData, 'id', '')}
        className={classNames("section", state.activeElement === null && isHover && "section-hover")}
        style={style}
        onMouseOver={() => handleHover()}
        onMouseOut={() => handleHoverOut()}
      >
        <div
          className={classNames("section-btn", state.activeElement === null && isHover && "active")}
          onClick={() => sectionBar()}
        >
          Section
        </div>
        <div
          className={classNames("section-btn delete", state.activeElement === null && isHover && "active")}
          onClick={() => deleteSection(sectionData.id)}
        >
          <i className="material-icons">delete</i>
        </div>        
          <Container>
            {sectionData.rows.map(rowId => {
              return (
                <Row
                  key={rowId}
                  rowId={rowId}
                  rowData={rowsData[rowId]}
                  columnsData={columnsData}
                  elementsData={elementsData}
                  wrapperData={wrapperData}
                  sectionId={sectionData.id}
                  {...otherProps}
                />
              );
            })}
          </Container>
      </section>
      <div className={classNames("section-option", newSectionPopup.enable && newSectionPopup.id === sectionData.id && "isClicked")}>
        <span
            className="add-section-option"
            onClick={() => setNewSectionPopup({
                enable: !newSectionPopup.enable,
                id: sectionData.id
            })}
        >
            <i className="material-icons">add_circle_outline</i>
        </span>
        {newSectionPopup.enable && newSectionPopup.id === sectionData.id && <div className="add-new-section-popup">
            <div
                className="add-new-section-popup-wrapper"
                onMouseDown={() => addNewSection("100", sectionIndex)}
            >
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
            </div>
            <div
                className="add-new-section-popup-wrapper"
                onMouseDown={() => addNewSection("50_50", sectionIndex)}
            >
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
            </div>
            <div
                className="add-new-section-popup-wrapper"
                onMouseDown={() => addNewSection("33_33_33", sectionIndex)}
            >
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
                <div className="add-new-section-popup-column">
                    <span></span>
                </div>
            </div>
        </div>}
      </div>
    </React.Fragment>
  );
};

const Row = ({ rowId, rowData, columnsData, elementsData, wrapperData, sectionId, ...otherProps }) => {
  const {
    state,
    state: { layers },
    dispatch
  } = useContext(Store);
  const rowStyle = {
    "background-color": _get(rowData, "rowData.styles.backgroundColor", ""),
    "background-image": "url(" + _get(rowData, "backgroundImage", "") + ")",
    "background-size": "cover",
    "background-position": "bottom",
    height: _get(rowData, "rowData.styles.height", ""),
    "margin-top": _get(rowData, "rowData.styles.marginTop", ""),
    "margin-bottom": _get(rowData, "rowData.styles.marginBottom", ""),
    "padding-top": _get(rowData, "rowData.styles.paddingTop", ""),
    "padding-right": _get(rowData, "rowData.styles.paddingRight", ""),
    "padding-bottom": _get(rowData, "rowData.styles.paddingBottom", ""),
    "padding-left": _get(rowData, "rowData.styles.paddingLeft", ""),
    "text-align": _get(rowData, "rowData.styles.textAlign", ""),
    color: _get(rowData, "rowData.styles.fontColor", "")
  };
  
  //Column Width Control
  const rowRef = useRef();
  const dragging = useRef();
  const [aggColumns, setAggColumns] = useState({ left: null, right: null});
  const onDragStart = useCallback(event => {
    dragging.current = true;
  }, []);
  const updatelayer = (newData) => {
    return dispatch({
        type: actions.UPDATE_LAYER,
        payload: newData
    });
  }
  const onDrag = event => {
    if (!dragging.current) return;
    const pos = event.touches ? event.touches[0].clientX : event.clientX;
    const move = () => {
      const boundingRect = rowRef.current.getBoundingClientRect();
      const leftWidth = _get(state.layers.entities.columns[aggColumns.left], 'width', 50);
      const rightWidth = _get(state.layers.entities.columns[aggColumns.right], 'width', 50);
      const columnRect = document.getElementById(aggColumns.left).getBoundingClientRect();
      const elemLeft = columnRect.left;
      const elemWidth = boundingRect.width;
      const min = elemLeft + 10;
      const max = elemWidth + elemLeft - 10;
      const newDividerPos = Math.max(min, Math.min(max, pos));
      const percent = ((newDividerPos - elemLeft) / elemWidth) * 100;
      const addingSize = leftWidth - percent + rightWidth;
      const updatedData = {
        ...layers,
        entities: {
          ...layers.entities,
          columns: {
            ...layers.entities.columns,
            [aggColumns.left]: {
              ...layers.entities.columns[aggColumns.left],
              width: percent
            },
            [aggColumns.right]: {
              ...layers.entities.columns[aggColumns.right],
              width: addingSize
            }
          }
        }
      }
      updatelayer(updatedData)
    };
    requestAnimationFrame(move);
  };
  const onDragEnd = useCallback(event => {
    if (dragging.current) {
      dragging.current = false;
    }
  }, []);
  const selectedColumnDragId = (id) => {
    const position = rowData.columns.indexOf(id);
    const nextColumn = rowData.columns[position + 1];
    return setAggColumns({left: id, right: nextColumn});
  }

  return (
    <div className="row-wrapper">
      {/* <div
        className="top"
        // data-columnId={columnId}
        data-rowId={rowId}
        data-sectionId={otherProps.sectionId}
      /> */}
      <div
        className="row"
        style={rowStyle}
        id={rowId}
        ref={rowRef}
        onMouseMove={onDrag}
        onTouchMove={onDrag}
        onMouseUp={onDragEnd}
        onTouchEnd={onDragEnd}
      >
        {rowData.columns.map((columnId, columnIndex) => {
          const columnCount = rowData.columns.length;
          const position = rowData.columns.indexOf(columnId);
          const nextColumn = rowData.columns[position + 1]
          return (
            <React.Fragment>
              <ColumnItem
                key={columnId}
                rowId={rowId}
                columnId={columnId}
                columnIndex={columnIndex}
                columnData={columnsData[columnId]}
                elementsData={elementsData}
                wrapperData={wrapperData}
                sectionId={sectionId}
                //Column Width Control
                columnCount={columnCount}
                nextColumn={nextColumn}
                onDragStart={onDragStart}
                selectedColumnDragId={selectedColumnDragId}
                {...otherProps}
              />
            </React.Fragment>
          );
        })}
      </div>
      {/* <div
        className="bottom"
        // data-columnId={columnId}
        data-rowId={rowId}
        data-sectionId={otherProps.sectionId}
      /> */}
    </div>
  );
};

function Canvas() {
  const {
    state,
    history: { state: stateHistory },
    dispatch
  } = useContext(Store);
  const [loading, setLoading] = useState(false);
  const [elementsData, setElementData] = useState(null);
  const [wrapperData, setWrapperData] = useState(null);
  const [sectionsData, setSectionsData] = useState(null);
  const [columnsData, setColumnsData] = useState(null);
  const [rowsData, setRowsData] = useState(null);

  const [stickyBar, setStickyBar] = useState(false);
  const [stickyBarPosition, setStickyBarPosition] = useState('bottom')

  const sections = _get(stateHistory, "layers.result.sections", []);
  // const rows = _get(stateHistory, "layers.entities.rows", {});
  // const columnsData = _get(stateHistory, "layers.entities.columns", {});
  // const sectionsData = _get(stateHistory, "layers.entities.section", {});
  // const elementsData = _get(stateHistory, "layers.entities.element", {});

  useEffect(() => {
    setElementData(_get(stateHistory, "layers.entities.element", {}));
    setWrapperData(_get(stateHistory, "layers.entities.wrappers", {}));
    setSectionsData(_get(stateHistory, "layers.entities.section", {}));
    setColumnsData(_get(stateHistory, "layers.entities.columns", {}));
    setRowsData(_get(stateHistory, "layers.entities.rows", {}));
    setStickyBar(_get(stateHistory, 'layers.result.sticky.enable', false));
    setStickyBarPosition(_get(stateHistory, 'layers.result.sticky.position', 'bottom'));
  }, [stateHistory]);
  const handleActiveElement = () => {
    return dispatch({
      type: actions.REMOVE_SELECT_ELEMENT
    })
  }
  return (
    <>
      <div className={classNames("canvas", state.activeElement && "active", state.isMenuBarActive && "menu-active", state.isStyleActive && "menu-active")} >
        {loading && <h1>Loading...</h1>}
        {state.activeElement && <div 
          className="overlay"
          onClick={() => handleActiveElement()}
          style={{background: "transparent", height: document.getElementById('page-editor-main-container').clientHeight + 200}}
        />}
        <PageContainer>
        {stickyBar && <SectionSticky />}
          {/* {state.menuBarType === "sticky" && stickyBar && stickyBarPosition === 'top' && <SectionSticky />} */}
          {sections.map((sectionId, index) =>
              _get(sectionsData, `${[sectionId]}`, null) && (
                <Section
                  key={sectionId}
                  sectionData={sectionsData[sectionId]}
                  rowsData={rowsData}
                  columnsData={columnsData}
                  elementsData={elementsData}
                  wrapperData={wrapperData}
                  sectionIndex={index}
                />
              )
          )}
          {/* {state.menuBarType === "sticky" && stickyBar && stickyBarPosition === 'bottom' && <SectionSticky />} */}
        </PageContainer>
      </div>
    </>
  );
}

export default Canvas;

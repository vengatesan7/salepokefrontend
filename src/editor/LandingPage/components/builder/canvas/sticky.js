import React, { useContext, useEffect, useState } from 'react';
import ContentEditable from "react-contenteditable";
import classNames from "classnames";
import _get from 'lodash.get';

import actions from "../../../store/action-types";
import { Store } from '../../../store';
import Container from './container';

const StickyTypeComponent = ({ type }) => {
    const {
        state: { layers },
        dispatch,
        history: { state: stateHistory }
    } = useContext(Store);

    const [stickyData, setStickyData] = useState(null);
    const [content, setContent] = useState(null);
    const [linkText, setLinkText] = useState(null);
    const [buttonText, setButtonText] = useState(null);
    const [inputPlaceholder, setInputPlaceholder] = useState(null);

    useEffect(() => {
        const thisStickyData = _get(stateHistory.layers.result, 'sticky', null);
        setStickyData(thisStickyData);
        setContent(_get(thisStickyData, 'content', 'Sample text content'));
        setLinkText(_get(thisStickyData, 'linkText', 'link'));
        setButtonText(_get(thisStickyData, 'buttonText', 'OK'));
        setInputPlaceholder(_get(thisStickyData, 'inputPlaceholder', 'Subscription Email'));
    }, [stateHistory]);

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handleChange = (name, value) => {
        name === 'content' && setContent(value);
        name === 'inputPlaceholder' && setInputPlaceholder(value);
        name === 'buttonText' && setButtonText(value);
        name === 'linkText' && setLinkText(value);
        const newData = {
            ...layers,
            result: {
                ...layers.result,
                sticky: {
                    ...layers.result.sticky,
                    [name]: value
                }
            }
        }
        return updatelayer(newData);
    }

    const textField = () => {
        return (
            <ContentEditable
                html={content}
                disabled={false}
                tagName={'span'}
                className='text'
                style={{ color: stickyData.textColor || "#000000" }}
                onChange={(e) => handleChange('content', e.target.value)}
            />
        );
    }
    const inputField = () => {
        return (
            <ContentEditable
                html={inputPlaceholder}
                disabled={false}
                tagName={'span'}
                className='input'
                onChange={(e) => handleChange('inputPlaceholder', e.target.value)}
            />
        );
    }
    const buttonField = () => {
        return (
            <ContentEditable
                html={buttonText}
                disabled={false}
                tagName={'span'}
                className='button'
                style={{ backgroundColor: stickyData.submitBackground || "#eeeeee", color: stickyData.submitColor || "#000000" }}
                onChange={(e) => handleChange('buttonText', e.target.value)}
            />
        )
    }
    const linkField = () => {
        return (
            <ContentEditable
                html={linkText}
                disabled={false}
                tagName={'span'}
                className='link'
                style={{ color: stickyData.textColor || "#000000" }}
                onChange={(e) => handleChange('linkText', e.target.value)}
            />
        );
    }
    switch (type) {
        case "textfieldbutton":
            return (
                <div className={type}>
                    {textField()}{inputField()}{buttonField()}
                </div>
            );
        case "textbutton":
            return (
                <div className={type}>
                    {textField()}{buttonField()}
                </div>
            );
        case "textlink":
            return (
                <div className={type}>
                    {textField()}{linkField()}
                </div>
            );
        case "text":
            return (
                <div className={type}>
                    {textField()}
                </div>
            );
        default:
            return (
                <div>Select your layout</div>
            );
    }
}

const SectionSticky = () => {
    const {
        history: { state: stateHistory }
    } = useContext(Store);

    const [position, setPosition] = useState('bottom');
    const [stickyData, setStickyData] = useState(_get(stateHistory.layers.result, 'sticky', null));
    const [backgroundColor, setBackgroundColor] = useState(null);
    const [fontFamily, setFontFamily] = useState(null);
    const [fontSize, setFontSize] = useState(null);
    const [type, setType] = useState(null);

    useEffect(() => {
        setPosition(_get(stateHistory.layers.result, 'sticky.position', 'bottom'));
        setBackgroundColor(_get(stateHistory.layers.result, 'sticky.background', null));
        setType(_get(stateHistory.layers.result, 'sticky.type', null));
        setStickyData(_get(stateHistory.layers.result, 'sticky', null));
        setFontFamily(_get(stateHistory.layers.result, 'sticky.fontFamily', null));
        setFontSize(_get(stateHistory.layers.result, 'sticky.fontSize', null));
    }, [stateHistory]);
    
    return (
        <div className="sticky-bar-wrapper">
            <div style={{
                width: "50px",
                textAlign: "center",
                padding: "12px",
                boxSizing: "border-box",
                lineHeight: "5px",
            }}>
                {/* <span>Position</span> */}
                <i className="material-icons" style={{transform: position === 'top' &&"rotate(180deg)"}}>
                    call_to_action
                </i>
            </div>
            <div style={{
                width: "calc(100% - 50px)"
            }}>
                <section className={classNames('sticky-bar', position)} style={{ backgroundColor: backgroundColor, fontFamily: fontFamily, fontSize: fontSize }}>
                    <Container>
                        {stickyData !== null ? <StickyTypeComponent type={type} /> : <div style={{ textAlign: "center" }}>Select your layout</div>}
                    </Container>
                </section>
            </div>
        </div>
    );
}

export default SectionSticky;
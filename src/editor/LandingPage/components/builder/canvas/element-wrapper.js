import React, { useState, useEffect, useRef, useContext } from "react";
import classnames from 'classnames';
import _get from "lodash.get";

import ElementItem from "./element-item";
import actions from "../../../store/action-types";
import { Store } from "../../../store";

const ElementWrapper = ({ elementData }) => {
  const [active, setActive] = useState(false);

  const node = useRef();
  const {
    state,
    state: { layers },
    dispatch,
    history: { state: stateHistory }
  } = useContext(Store);

  const [style, setStyle] = useState({});

  const getStyle = () => {    
    const elementUpdatedData = elementData !== undefined && layers.entities.element[elementData.id];
    if (elementData !== undefined) {
      const oldBoxModule = {
        marginTop: _get(elementUpdatedData, 'marginTop', ''),
        marginRight: _get(elementUpdatedData, 'marginRight', ''),
        marginBottom: _get(elementUpdatedData, 'marginBottom', ''),
        marginLeft: _get(elementUpdatedData, 'marginLeft', ''),
        paddingTop: _get(elementUpdatedData, 'paddingTop', ''),
        paddingRight: _get(elementUpdatedData, 'paddingRight', ''),
        paddingBottom: _get(elementUpdatedData, 'paddingBottom', ''),
        paddingLeft: _get(elementUpdatedData, 'paddingLeft', ''),
      }
      const otherStyle = {
        background: elementUpdatedData.background,
        backgroundImage: elementUpdatedData.backgroundImage && `url(${elementUpdatedData.backgroundImage})`,
        backgroundPosition:elementUpdatedData.backgroundImagePosition,
        backgroundRepeat:elementUpdatedData.backgroundRepeat,
        backgroundSize:elementUpdatedData.backgroundImageSize,
        letterSpacing:elementUpdatedData.letterSpacing,
        color: elementUpdatedData.fontColor,
        borderRadius: elementUpdatedData.borderRadius,
        borderColor: elementUpdatedData.borderColor,
        borderWidth: elementUpdatedData.borderWidth,
        borderStyle: elementUpdatedData.borderStyle,
        textAlign: elementUpdatedData.textAlign,
        fontSize: elementUpdatedData.fontSize,
        fontWeight: elementUpdatedData.bold,
        fontStyle: elementUpdatedData.italic,
        textDecoration: elementUpdatedData.textDecoration,
        lineHeight: elementUpdatedData.lineHeight && elementUpdatedData.elementType != "PARAGRAPH" ? elementUpdatedData.lineHeight + "px" : ( elementUpdatedData.lineHeight <= 3 ? elementUpdatedData.lineHeight : 1.5 ),
        fontFamily: elementUpdatedData.fontFamily && elementUpdatedData.fontFamily + ", sans-serif",
      }
      const styleDefault = {
        background: _get(elementUpdatedData.style, 'background', otherStyle.background),
        backgroundImage:`url(${_get(elementUpdatedData.style, 'backgroundImage', otherStyle.backgroundImage)}`,
        backgroundPosition:_get(elementUpdatedData.style, 'backgroundImagePosition', otherStyle.backgroundPosition),
        backgroundRepeat:_get(elementUpdatedData.style, 'backgroundRepeat', otherStyle.backgroundRepeat),
        backgroundSize:_get(elementUpdatedData.style, 'backgroundImageSize', otherStyle.backgroundSize),
        letterSpacing:_get(elementUpdatedData.style, 'letterSpacing', otherStyle.letterSpacing),
        color: _get(elementUpdatedData.style, 'color', otherStyle.color),
        borderRadius: _get(elementUpdatedData.style, 'borderRadius', otherStyle.borderRadius),
        borderColor: _get(elementUpdatedData.style, 'borderColor', otherStyle.borderColor),
        borderWidth: _get(elementUpdatedData.style, 'borderWidth', otherStyle.borderWidth),
        borderStyle: _get(elementUpdatedData.style, 'borderStyle', otherStyle.borderStyle),
        textAlign: _get(elementUpdatedData.style, 'textAlign', otherStyle.textAlign),
        fontSize: _get(elementUpdatedData.style, 'fontSize', otherStyle.fontSize),
        fontWeight: _get(elementUpdatedData.style, 'bold', otherStyle.fontWeight),
        fontStyle: _get(elementUpdatedData.style, 'italic', otherStyle.fontStyle),
        textDecoration: _get(elementUpdatedData.style, 'textDecoration', otherStyle.textDecoration),
        lineHeight: _get(elementUpdatedData.style, 'lineHeight',  otherStyle.lineHeight),
        fontFamily: _get(elementUpdatedData.style, 'fontFamily',  otherStyle.fontFamily) + ", sans-serif",
      }
      switch (state.deviceType) {
        case "phone":
          return setStyle({
            ...otherStyle,
            paddingTop: _get(elementUpdatedData.phoneStyles, 'paddingTop', oldBoxModule.paddingTop),
            paddingRight: _get(elementUpdatedData.phoneStyles, 'paddingRight', oldBoxModule.paddingRight),
            paddingBottom: _get(elementUpdatedData.phoneStyles, 'paddingBottom', oldBoxModule.paddingBottom),
            paddingLeft: _get(elementUpdatedData.phoneStyles, 'paddingLeft', oldBoxModule.paddingLeft),
            marginTop: _get(elementUpdatedData.phoneStyles, 'marginTop', oldBoxModule.marginTop),
            marginRight: _get(elementUpdatedData.phoneStyles, 'marginRight', oldBoxModule.marginRight),
            marginBottom: _get(elementUpdatedData.phoneStyles, 'marginBottom', oldBoxModule.marginBottom),
            marginLeft: _get(elementUpdatedData.phoneStyles, 'marginLeft', oldBoxModule.marginLeft),
          });
        case "tablet":
          return setStyle({
            ...otherStyle,
            paddingTop: _get(elementUpdatedData.tabletStyles, 'paddingTop', oldBoxModule.paddingTop),
            paddingRight: _get(elementUpdatedData.tabletStyles, 'paddingRight', oldBoxModule.paddingRight),
            paddingBottom: _get(elementUpdatedData.tabletStyles, 'paddingBottom', oldBoxModule.paddingBottom),
            paddingLeft: _get(elementUpdatedData.tabletStyles, 'paddingLeft', oldBoxModule.paddingLeft),
            marginTop: _get(elementUpdatedData.tabletStyles, 'marginTop', oldBoxModule.marginTop),
            marginRight: _get(elementUpdatedData.tabletStyles, 'marginRight', oldBoxModule.marginRight),
            marginBottom: _get(elementUpdatedData.tabletStyles, 'marginBottom', oldBoxModule.marginBottom),
            marginLeft: _get(elementUpdatedData.tabletStyles, 'marginLeft', oldBoxModule.marginLeft),
          });
        default:
          return setStyle({
            ...otherStyle,
            paddingTop: _get(elementUpdatedData.styles, 'paddingTop', oldBoxModule.paddingTop),
            paddingRight: _get(elementUpdatedData.styles, 'paddingRight', oldBoxModule.paddingRight),
            paddingBottom: _get(elementUpdatedData.styles, 'paddingBottom', oldBoxModule.paddingBottom),
            paddingLeft: _get(elementUpdatedData.styles, 'paddingLeft', oldBoxModule.paddingLeft),
            marginTop: _get(elementUpdatedData.styles, 'marginTop', oldBoxModule.marginTop),
            marginRight: _get(elementUpdatedData.styles, 'marginRight', oldBoxModule.marginRight),
            marginBottom: _get(elementUpdatedData.styles, 'marginBottom', oldBoxModule.marginBottom),
            marginLeft: _get(elementUpdatedData.styles, 'marginLeft', oldBoxModule.marginLeft),
          });
      }
    }
  }

  useEffect(() => {
    getStyle();
  }, [stateHistory]);
  useEffect(() => {
    getStyle();
  }, [state.deviceType]);

  const handleClick = () => {
    setActive(true);
    return dispatch({
      type: actions.SELECT_ELEMENT,
      payload: elementData.id
    });
  };

  const handleClickOutside = e => { 
    if (node.current.contains(e.target)) {
      return;
    }
    setActive(false); // outside click
  };

  useEffect(() => {
    if (active) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [active]);
  if (!elementData) {
    return null;
  }

  return (
    <div
      className={classnames("element", elementData.id === state.activeElement && "active")}
      ref={node}
      style={elementData.elementType !== "BUTTON" ? style : {
          textAlign: elementData.textAlign,
          fontSize: elementData.fontSize,
          fontWeight: elementData.bold,
          fontStyle: elementData.italic,
          lineHeight: elementData.lineHeight + "px",
          fontFamily: elementData.fontFamily && elementData.fontFamily + ", sans-serif"
        }}
      onClick={() => handleClick()}
    >
      <ElementItem elementData={elementData} />
    </div>
  );
};

export default ElementWrapper;

import React from "react";
import Title from "./elements/title";
import Paragraph from "./elements/paragraph";
import Form from "./elements/form";
import Image from "./elements/image";
import Line from "./elements/line";
import Button from "./elements/button";
import ImageText from "./elements/imagestext";
import Video from "./elements/video";
import Space from "./elements/space";
import Timer from "./elements/timer";
import PriceChart from "./elements/pricechart";
import Card from "./elements/card";
import Address from "./elements/address";
import Product from "./elements/product"; 
import Icon from "./elements/icon"; 
import AdvanceForm from "./elements/advanceForm";
import Upsell from "./elements/upsell"


const ElementItem = ({ elementData }) => {
  switch (elementData.elementType) {
    case "TITLE":
      return <Title elementData={elementData} />;
    case "PARAGRAPH":
      return <Paragraph elementData={elementData} />;
    case "LINK":
      return <Paragraph elementData={elementData} />;
    case "FORM":
      return <Form elementData={elementData} />;
    case "ADVANCEFORM":
      return <AdvanceForm elementData={elementData} />;
    case "IMAGE":
      return <Image elementData={elementData} />;
    case "LINE":
      return <Line elementData={elementData} />;
    case "BUTTON":
      return <Button elementData={elementData} />;
    case "IMAGEPLUSTEXT":
      return <ImageText elementData={elementData} />;
    case "VIDEO":
      return <Video elementData={elementData} />;
    case "SPACE":
      return <Space elementData={elementData} />;
    case "TIMER":
      return <Timer elementData={elementData} />;
    case "PRICECHART":
      return <PriceChart elementData={elementData} />;
    case "CARD":
      return <Card elementData={elementData} />;
    case "ADDRESS":
      return <Address elementData={elementData} />;
    case "PRODUCT":
      return <Product elementData={elementData} />;
    case "ICON":
      return <Icon elementData={elementData} />;
    case "UPSELL":
      return <Upsell elementData={elementData} />;
    default:
      return;
  }
};

export default ElementItem;

import React, { useContext, useEffect, useState } from 'react';
import classNames from "classnames";
import _get from "lodash.get";

import { Store } from '../../../store';

function PageContainer(props) {
    const {
        state,
        history: { state: stateHistory }
    } = useContext(Store);
    const [pageStyle, setPageStyle] = useState({});
    const [device, setDevice] = useState({
        deviceType: "desktop",
        deviceSize: "auto"
    });
    const newStyle = {
        backgroundColor: _get(pageStyle, 'background', ''),
        backgroundImage: _get(pageStyle, 'backgroundImage', null) && `url(${pageStyle.backgroundImage})`,
        color: _get(pageStyle, 'fontColor', ''),
        fontFamily: _get(pageStyle, 'fontFamily', ''),
        margin: _get(pageStyle, 'margin', ''),
        width: device.deviceSize
    }
    useEffect(() => {
        setPageStyle(_get(stateHistory, 'layers.result.page', {}));
    }, [stateHistory]);
    useEffect(() => {
        switch (state.deviceType) {
            case "desktop":
                return setDevice({
                    deviceType: state.deviceType,
                    deviceSize: "auto"
                });
            case "tablet":
                return setDevice({
                    deviceType: state.deviceType,
                    deviceSize: "768px"
                });
            case "phone":
                return setDevice({
                    deviceType: state.deviceType,
                    deviceSize: "414px"
                });
            default:
                return setDevice({
                    deviceType: state.deviceType,
                    deviceSize: "auto"
                });
        }  
    }, [state.deviceType])
    return (
        <React.Fragment>
            {/* <div style={{ height: "100px", backgroundColor: "#282828" }}></div> */}
            <div className={classNames("main-container", device.deviceType)} id="page-editor-main-container" style={newStyle}>
                {props.children}
            </div>
            {/* <div style={{ height: "100px", backgroundColor: "#282828" }}></div> */}
        </React.Fragment>
    );
}

export default PageContainer;
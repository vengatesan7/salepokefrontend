import { schema } from "normalizr";

const field = new schema.Entity("field");
const form = new schema.Entity("formData", {
  fields: [field]
});
const element = new schema.Entity("element", {
  formData: [form]
});
const wrapper = new schema.Entity("wrappers", {
  elements: [element]
});
const column = new schema.Entity("columns", {
  wrappers: [wrapper]
});
const row = new schema.Entity('rows', {
  columns: [column]
});
const section = new schema.Entity('section', {
  rows: [row],
});
const pageprop = new schema.Entity('pageprop');
const meta = new schema.Entity('meta');
const tracking = new schema.Entity('tracking');
const sticky = new schema.Entity('sticky');
const popup = new schema.Entity('popup');

const layersSchema = {
  meta: { meta },
  tracking: {tracking},
  sticky: { sticky },
  popup: { popup },
  page: { pageprop },
  sections: [section],
  rows: [row],
  columns: [column],
  wrappers: [wrapper],
  elements: [element],
  formData: [form]
};

export default layersSchema;

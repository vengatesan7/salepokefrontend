import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import { ClipLoader } from 'react-spinners';
import Slider from 'react-rangeslider';
import _get from 'lodash.get';

import { Store } from '../../../../store';
import actions from "../../../../store/action-types";
import ImageUpload from '../../../../../components/image-upload';

const PageProperties = () => {
    const {
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);
    const [pageStyle, setPageStyle] = useState(null);
    const [backgroundColorPopup, setBackgroundColorPopup] = useState(false);
    const [backgroundColor, setBackgroundColor] = useState(null);
    const [fontColorPopup, setFontColorPopup] = useState(false);
    const [fontColor, setFontColor] = useState(null);
    const [importPopup, setImportPopup] = useState(false);
    const [backgroundImage, setBackgroundImage] = useState(null);

    useEffect(() => {
        setPageStyle(_get(stateHistory.layers.result, 'page', null));
        setBackgroundColor({ background: _get(stateHistory.layers.result.page, 'background', '') });
        setFontColor({ background: _get(stateHistory.layers.result.page, 'fontColor', '') });
        setBackgroundImage(_get(stateHistory.layers.result.page, 'backgroundImage', null))
    }, [stateHistory]);

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handlePageProperties = (propertie, value) => {
        setImportPopup(false)
        if (propertie === "fontColor") {
            setFontColor({ background: value });
        } else if (propertie === "background") {
            setBackgroundColor({ background: value });
        } else if (propertie === "backgroundImage") {
            setBackgroundImage(value);
        } else {
            setPageStyle({
                ...pageStyle,
                [propertie]: value
            });
        }
        const newData = {
            ...layers,
            result: {
                ...layers.result,
                page: {
                    ...layers.result.page,
                    [propertie]: value
                }
            }
        }
        return updatelayer(newData);
    }

    return (
        <div className="page-properties">
            <h4>Page Properties</h4>
            {pageStyle ? <React.Fragment>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Container</label>
                    <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={990} max={1200} step={10}
                                value={_get(pageStyle, 'width', '990')}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handlePageProperties("width", e)}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {_get(pageStyle, 'width', '990')}
                        </div>
                    </div>
                </div>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Default Style</label>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Font Color</span>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setFontColorPopup(true)}>
                                <div className="color-picker-color" style={fontColor}></div>
                            </div>
                            {fontColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => setFontColorPopup(false)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={fontColor.background} onChange={(e) => handlePageProperties("fontColor", e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => setFontColorPopup(false)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    {/* <div>
                        <span>Link Color</span>
                        <input type="text" value={_get(pageStyle, 'linkColor', '')} />
                    </div> */}
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Font Family</span>
                        <div>
                            <select value={_get(pageStyle, 'fontFamily', '')} onChange={(e) => handlePageProperties('fontFamily', e.target.value)}>
                                <option value="">Select Font Family</option>
                                <option value="Roboto">Roboto</option>
                                <option value="Open Sans">Open Sans</option>
                                <option value="Ubuntu">Ubuntu</option>
                                <option value="Josefin Sans">Josefin Sans</option>
                                <option value="Tajawal">Tajawal</option>
                                <option value="Raleway">Raleway</option>
                                <option value="Oxygen">Oxygen</option>
                                <option value="Maven Pro">Maven Pro</option>
                                <option value="Catamaran">Catamaran</option>
                                <option value="Orbitron">Orbitron</option>
                                <option value="Darker Grotesque">Darker Grotesque</option>
                                <option value="Montserrat Alternates">Montserrat Alternates</option>
                                <option value="Nunito Sans">Nunito Sans</option>
                                <option value="Noto Serif"> Noto Serif </option>
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                                <option value="Lato"> Lato </option>
                                <option value="Roboto Slab"> Roboto Slab </option>
                                <option value="Montserrat"> Montserrat </option> 
                                <option value="Anton"> Anton </option>
                                <option value="Source Sans Pro">Source Sans Pro  </option>
                                <option value="Roboto Condensed"> Roboto Condensed </option>
                                <option value="Oswald">Oswald</option>
                                <option value="Enemy"> Enemy </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Background</label>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>color</span>
                        <div>
                            <div className="color-picker-none" onClick={() => handlePageProperties("background", "transparent")} ><i className="material-icons">not_interested</i></div>
                            <div className="color-picker-swatch" onClick={() => setBackgroundColorPopup(true)}>
                                <div className="color-picker-color" style={backgroundColor}></div>
                            </div>
                            {backgroundColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => setBackgroundColorPopup(false)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={backgroundColor.background} onChange={(e) => handlePageProperties("background", e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => setBackgroundColorPopup(false)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div className="property-form">
                        <button className="edit-form-button" onClick={() => setImportPopup(true)}>Upload Image</button>
                    </div>
                    {backgroundImage && <div className="property-form">
                        <div className="image-viewer">
                            <img src={backgroundImage} alt="URL is not valid" />
                        </div>
                        <button className="edit-form-button" onClick={() => handlePageProperties('backgroundImage', null)}>Remove Image</button>
                    </div>}
                    {importPopup && <ImageUpload imageLink={(e) => handlePageProperties('backgroundImage', e)} popupClose={() => setImportPopup(false)} />}
                </div>
            </React.Fragment> : <React.Fragment>
                    <ClipLoader sizeUnit={"px"} size={50} color={'#ffffff'} loading />
                </React.Fragment>}
        </div>
    );
}

export default PageProperties;
import React, { useContext, useEffect, useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import _get from 'lodash.get';

import { Store } from '../../../../store';
import actions from "../../../../store/action-types";

const Tracking = () => {
    const {
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);
    const [metaData, setMetaData] = useState({});
    const [trackingScript, setTrackingScript] = useState({});

    useEffect(() => {
        setMetaData(_get(stateHistory.layers.result, 'meta', {}));
        setTrackingScript(_get(stateHistory.layers.result, 'tracking', {}));
    }, [stateHistory]);

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handleMetaOnchange = (value, name) => {
        setMetaData({ ...metaData, [name]: value })
        const data = {
            ...layers,
            result: {
                ...layers.result,
                meta: {
                    ...layers.result.meta,
                    [name]: value
                }
            }
        }
        return updatelayer(data);
    }

    const handleScriptOnchange = (value, name) => {
        setTrackingScript({ ...trackingScript, [name]: value === undefined ? "" : value })
        const data = {
            ...layers,
            result: {
                ...layers.result,
                tracking: {
                    ...layers.result.tracking,
                    [name]: value === undefined ? "" : value
                }
            }
        }
        return updatelayer(data);
    }
    
    return (
        <div className="tracking">
            <Tabs className="popup-menu-tabs">
                <TabList className="popup-list">
                    <Tab className="popup-tab-option">
                        <span>Meta Data</span>
                    </Tab>
                    <Tab className="popup-tab-option">
                        <span>Scripts</span>
                    </Tab>
                </TabList>
                <TabPanel className="popup-panel">
                    <div>
                        <div style={{ padding: "15px", borderBottom: "1px solid rgb(204, 204, 204)" }}>
                            <div>Title</div>
                            <textarea value={_get(metaData, 'title', '')}
                                onChange={(e) => setMetaData({ ...metaData, 'title': e.target.value })}
                                onBlur={(e) => handleMetaOnchange(metaData.title, 'title')}
                            />
                        </div>
                        <div style={{ padding: "15px", borderBottom: "1px solid rgb(204, 204, 204)" }}>
                            <div>Keywords</div>
                            <textarea value={_get(metaData, 'keywords', '')}
                                onChange={(e) => setMetaData({ ...metaData, 'keywords': e.target.value })}
                                onBlur={(e) => handleMetaOnchange(metaData.keywords, 'keywords')}
                            />
                        </div>
                        <div style={{ padding: "15px", borderBottom: "1px solid rgb(204, 204, 204)" }}>
                            <div>Description</div>
                            <textarea value={_get(metaData, 'description', '')}
                                onChange={(e) => setMetaData({ ...metaData, 'description': e.target.value })}
                                onBlur={(e) => handleMetaOnchange(metaData.description, 'description')}
                            />
                        </div>
                    </div>
                </TabPanel>
                <TabPanel className="popup-panel">
                    <div>
                        <div style={{ padding: "15px", borderBottom: "1px solid rgb(204, 204, 204)" }}>
                            <div>Head Section Tracking Code</div>
                            <textarea value={_get(trackingScript, 'head', '')}
                                onChange={(e) => setTrackingScript({ ...trackingScript, 'head': e.target.value })}
                                onBlur={(e) => handleScriptOnchange(trackingScript.head, 'head')}
                            />
                        </div>
                        <div style={{ padding: "15px", borderBottom: "1px solid rgb(204, 204, 204)" }}>
                            <div>Body Section Tracking code</div>
                            <textarea value={_get(trackingScript, 'body', '')}
                                onChange={(e) => setTrackingScript({ ...trackingScript, 'body': e.target.value })}
                                onBlur={(e) => handleScriptOnchange(trackingScript.body, 'body')}
                            />
                        </div>
                    </div>
                </TabPanel>
            </Tabs>
            
        </div>
    );
}

export default Tracking;
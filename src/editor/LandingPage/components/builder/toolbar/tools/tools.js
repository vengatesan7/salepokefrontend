import React, { useContext } from "react";
import classnames from 'classnames';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { Store } from "../../../../store";
import { TOOL_ITEMS, ORDER_TOOLS } from "../../menubar/constants";

const WidgetsDnd = ({ tools }) => {
  return (
    <Droppable droppableId="ITEMS" isDropDisabled type="element">
        {(provided, snapshot) => (
          <ul
            // className="toolbar-list"
            ref={provided.innerRef}
          >
            {tools.map((tool, index) => (
              <Draggable key={tool.id} draggableId={tool.id} index={index} disableInteractiveElementBlocking>
                {(provided, snapshot) => (
                  <>
                    <li
                      className={classnames("toolbar-item", { "is-dragging": snapshot.isDragging })}
                      key={tool.id}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      style={provided.draggableProps.style}
                    >
                      <button
                        className="toolbar-button"
                        title={tool.name}
                        {...provided.dragHandleProps}
                      >
                        <i className="material-icons">{tool.className}</i>
                        {/* <i className={classnames("pro-icons", tool.className)}>{tool.icon}</i>  */}
                        {tool.name}
                      </button>
                    </li>
                    {snapshot.isDragging && <li className="toolbar-item">
                      <button className="toolbar-button" title={tool.name}>
                        <i className="material-icons">{tool.className}</i>
                        {/* <i className={classnames("pro-icons", tool.className)}>{tool.icon}</i> */}
                        {tool.name}
                      </button>
                    </li>}
                  </>
                )}
              </Draggable>
            ))}
          </ul>
        )}
      </Droppable>
  )
}

const Tools = ({ tools, addLayer }) => {
  const {
    state
  } = useContext(Store);
  const eCommerceWidgets = [13, 17]
  return (
    <div className="toolbar-list">
      <WidgetsDnd tools={TOOL_ITEMS} />
      {/* {eCommerceWidgets.includes(state.templateCategoryId) && <React.Fragment>
        <label className="toolbar-list-label">Ecommerce Widgets</label>
        <WidgetsDnd tools={ORDER_TOOLS} />
      </React.Fragment>} */}
    </div>
  );
};

export default Tools;

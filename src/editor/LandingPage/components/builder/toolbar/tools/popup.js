import React, { useState, useEffect, useContext } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Link } from "react-router-dom";
import Switch from "react-switch";
import _get from 'lodash.get';

import { PostData } from '../../../../../../services/PostData';
import actions from "../../../../store/action-types";
import { Store } from '../../../../store';
import { REFUSED } from 'dns';

const PopupBar = () => {
    const {
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);

    const [popupList, setPopupList] = useState([]);
    const [onLoadPopupData, setOnLoadPopupData] = useState(null);
    const [onExitPopupData, setOnExitPopupData] = useState(null);

    useEffect(() => {
        PostData('ms4', "popuplistforeditor").then(response => {
            if (response !== "Invalid") {
                if (response.status === "success") {
                    setPopupList(response.popupmaster);
                }
            }
        });
    }, []);
    useEffect(() => {
        setOnLoadPopupData(_get(stateHistory.layers.result.popup, 'onPageLoad', null));
        setOnExitPopupData(_get(stateHistory.layers.result.popup, 'onPageExit', null));
    }, [stateHistory]);
    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handleOnLoadPopup = (name, value) => {
        setOnLoadPopupData({ ...onLoadPopupData, [name]: value });
        if (onLoadPopupData === null) {
            const updateOnLoadPopup = {
                ...layers,
                result: {
                    ...layers.result,
                    popup: {
                        ...layers.result.popup,
                        onPageLoad: {
                            [name]: value,
                            enable: true,
                            startDelay: 60,
                            duration: 120,
                        }
                    }
                }
            }
            return updatelayer(updateOnLoadPopup);
        } else {
            if (name === 'enable') {
                const updateOnLoadPopup = {
                    ...layers,
                    result: {
                        ...layers.result,
                        popup: {
                            ...layers.result.popup,
                            onPageLoad: {
                                ...layers.result.popup.onPageLoad,
                                [name]: value
                            }
                        }
                    }
                }
                return updatelayer(updateOnLoadPopup);
            } else if (name === 'popupId') {
                const updateOnLoadPopup = {
                    ...layers,
                    result: {
                        ...layers.result,
                        popup: {
                            ...layers.result.popup,
                            onPageLoad: {
                                ...layers.result.popup.onPageLoad,
                                [name]: value
                            }
                        }
                    }
                }
                return updatelayer(updateOnLoadPopup);
            } else {
                const updateOnLoadPopup = {
                    ...layers,
                    result: {
                        ...layers.result,
                        popup: {
                            ...layers.result.popup,
                            onPageLoad: {
                                ...layers.result.popup.onPageLoad,
                                [name]: value,
                                enable: true
                            }
                        }
                    }
                }
                return updatelayer(updateOnLoadPopup);
            }
        }
    }

    const handleOnExitPopup = (name, value) => {
        setOnExitPopupData({ ...onExitPopupData, [name]: value });
        if (onExitPopupData === null) {
            const updateOnExitPopup = {
                ...layers,
                result: {
                    ...layers.result,
                    popup: {
                        ...layers.result.popup,
                        onPageExit: {
                            [name]: value,
                            enable: true
                        }
                    }
                }
            }
            return updatelayer(updateOnExitPopup);
        } else {
            const updateOnExitPopup = {
                ...layers,
                result: {
                    ...layers.result,
                    popup: {
                        ...layers.result.popup,
                        onPageExit: {
                            ...layers.result.popup.onPageExit,
                            [name]: value
                        }
                    }
                }
            }
            return updatelayer(updateOnExitPopup);
        }
    }

    const startDelay = _get(onLoadPopupData, 'startDelay', 600);
    const duration = _get(onLoadPopupData, 'duration', 600);
    return (
        <Tabs className="popup-menu-tabs">
            <TabList className="popup-list">
                <Tab className="popup-tab-option">
                    {/* <i className="material-icons">widgets</i> */}
                    <span>On-load</span>
                </Tab>
                <Tab className="popup-tab-option">
                    <span>On-exit</span>
                </Tab>
            </TabList>
            <TabPanel className="popup-panel">
                <div className="popup-option">
                    <label>Popup List</label>
                    <div className="property-select">
                        {popupList.length !== 0 ? <select
                            onChange={(e) => handleOnLoadPopup('popupId', e.target.value)}
                            value={onLoadPopupData !== null && onLoadPopupData.popupId}
                            disabled={onLoadPopupData !== null && !onLoadPopupData.enable}
                        >
                            {onLoadPopupData === null && <option value="null">Select your popup</option>}
                            {popupList.map(data => (
                                <option value={data.pop_up_id}>{data.pop_up_name}</option>
                            ))}
                        </select> : <div style={{textAlign: "center"}}>No data. <br/><Link to="/popup/addnewpopup" >Create new popup</Link></div>}
                    </div>
                </div>
                {onLoadPopupData !== null && onLoadPopupData.popupId !== undefined &&  onLoadPopupData.popupId !== "null" && <React.Fragment>
                    <div className="popup-option two-column">
                        <label>{onLoadPopupData.enable ? "Popup is enabled" : "Popup is disabled"}</label>
                        <div className="property-switch">
                            <Switch
                                checked={onLoadPopupData === null ? false : onLoadPopupData.enable}
                                onChange={(e) => handleOnLoadPopup('enable', e)}
                                onColor="#fcbc5a" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30} />
                        </div>
                    </div>
                    {onLoadPopupData !== null && onLoadPopupData.enable && <React.Fragment>
                        <div className="popup-option">
                            <label>Start time</label>
                            <div className="property-time">
                                <div className="property-min">
                                    <label>Min</label>
                                    <input type="number" max={10} min={0} disabled value={startDelay / 60} />
                                    <div className="property-time-up-down">
                                        <span onClick={() => handleOnLoadPopup('startDelay', startDelay < 600 ? startDelay + 60 : startDelay)}>
                                            <i className="material-icons">arrow_drop_up</i>
                                        </span>
                                        <span onClick={() => handleOnLoadPopup('startDelay', startDelay > 0 ? startDelay - 60 : startDelay)}>
                                            <i className="material-icons">arrow_drop_down</i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="popup-option">
                            <label>Duration time</label>
                            <div className="property-time">
                                <div className="property-min">
                                    <label>Min</label>
                                    <input type="number" max={10} min={0} disabled value={duration / 60} />
                                    <div className="property-time-up-down">
                                        <span onClick={() => handleOnLoadPopup('duration', duration < 600 ? duration + 60 : duration)}>
                                            <i className="material-icons">arrow_drop_up</i>
                                        </span>
                                        <span onClick={() => handleOnLoadPopup('duration', duration > 0 ? duration - 60 : duration)}>
                                            <i className="material-icons">arrow_drop_down</i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>}
                </React.Fragment>}
            </TabPanel>
            <TabPanel className="popup-panel">
                <div className="popup-option">
                    <label>Popup List</label>
                    <div className="property-select">
                        {popupList.length !== 0 ? <select
                            onChange={(e) => handleOnExitPopup('popupId', e.target.value)}
                            value={onExitPopupData !== null && onExitPopupData.popupId}
                            disabled={onExitPopupData !== null && !onExitPopupData.enable}
                        >
                            {onExitPopupData === null && <option value="null">Select your popup</option>}
                            {popupList.map(data => (
                                <option value={data.pop_up_id}>{data.pop_up_name}</option>
                            ))}
                        </select> : <div style={{textAlign: "center"}}>No data. <br/><Link to="/popup/addnewpopup" >Create new popup</Link></div>}
                    </div>
                </div>
                {onExitPopupData !== null && <div className="popup-option two-column">
                    <label>{onExitPopupData.enable ? "Popup is enabled" : "Popup is disabled"}</label>
                    <div className="property-switch">
                        <Switch
                            checked={onExitPopupData === null ? false : onExitPopupData.enable}
                            onChange={(e) => handleOnExitPopup('enable', e)}
                            onColor="#fcbc5a" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>}
                
            </TabPanel>
        </Tabs>
    );
}

export default PopupBar;
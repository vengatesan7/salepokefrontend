import React, { useContext, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import { denormalize, schema } from 'normalizr';
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from 'uuid/v4';

import actions from "../../../../store/action-types"
import { Store } from "../../../../store";
import SectionLayout from './layoutSection';

const Layouts = () => {
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);

    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };
    const addSection = () => {
        const newSectionId = uuid();
        const newRowId = uuid();
        const newColumnid = uuid();
        const newSectionOrder = layers.result.sections
        newSectionOrder.push(newSectionId)
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                columns: {
                    ...layers.entities.columns,
                    [newColumnid]: {
                        id: newColumnid,
                        width: 100,
                        wrappers: []
                    }
                },
                rows: {
                    ...layers.entities.rows,
                    [newRowId]: {
                        id: newRowId,
                        rowData: {
                            name: "Row 1"
                        },
                        columns: [newColumnid]
                    }
                },
                section: {
                    ...layers.entities.section,
                    [newSectionId]: {
                        id: newSectionId,
                        sectionsData: {
                            name: "New Section"
                        },
                        rows: [newRowId]
                    }
                }
            },
            result: {
                ...layers.result,
                sections: newSectionOrder
            }
        }
        return updateLayers(newData)
    }

    return (
        <div className="layout-settings">
            <h4>Page Layout</h4>
            <Droppable droppableId="section" type="sectionLayouts">
                {(provided, snapshot) => (
                    <div {...provided.droppableProps}
                        ref={provided.innerRef}
                        style={{ backgroundColor: snapshot.isDraggingOver ? 'lightblue' : 'white' }}
                    >
                        {layers.result.sections.map((sectionId, index) => {
                            const onlyOneSection = stateHistory.layers.result.sections.length === 1;
                            return (
                                <SectionLayout
                                    sectionId={sectionId}
                                    data={_get(stateHistory.layers.entities.section[sectionId], "sectionsData", null)}
                                    index={index}
                                    draggingOver={snapshot.isDraggingOver}
                                    onlyOneSection={onlyOneSection}
                                />
                            )
                        })}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
            <div className="add-section-btn">
                <button className="edit-form-button" onClick={addSection}>Create New Section</button>
            </div>
        </div>
    )
}

export default Layouts;
import React, { useContext, useState, useEffect } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from 'lodash.get';
import uuid from "uuidv4";

import actions from "../../../../store/action-types";
import { Store } from '../../../../store';

const RowLayout = (props) => {
    // const { state, dispatch } = useContext(Store);
    const {
        state,
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);
    const [thisLayers, setThisLayers] = useState({});

    useEffect(() => {
        setThisLayers()
    }, [stateHistory]);

    const deleteRow = (rowId) => {
        const rowArr = state.layers.entities.section[props.sectionId].rows;
        const position = rowArr.indexOf(rowId);
        rowArr.splice(position, 1);
        const newRows = state.layers.entities.rows;
        delete newRows[rowId];
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                rows: newRows,
                section: {
                    ...layers.entities.section,
                    [props.sectionId]: {
                        ...layers.entities.section[props.sectionId],
                        rows: rowArr
                    }
                }
            }
        }
        return updateLayers(newData)
    }
    const deleteColumn = (columnId, rowId) => {
        const columnArr = layers.entities.rows[rowId].columns;
        const position = columnArr.indexOf(columnId);
        const deletingSpace = layers.entities.columns[columnId].width;
        if (position !== 0) {
            const prevColumnId = columnArr[position - 1];
            const prevColumnWidth = layers.entities.columns[prevColumnId].width;
            columnArr.splice(position, 1);
            const newData = {
                ...layers,
                entities: {
                    ...layers.entities,
                    rows: {
                        ...layers.entities.rows,
                        [rowId]: {
                            ...layers.entities.rows[rowId],
                            columns: columnArr
                        }
                    },
                    columns: {
                        ...layers.entities.columns,
                        [prevColumnId]: {
                            ...layers.entities.columns[prevColumnId],
                            width: prevColumnWidth + deletingSpace
                        }
                    }
                }                
            }
            return updateLayers(newData);
        } else {
            const nextColumnId = columnArr[position + 1];
            const nextColumnWidth = layers.entities.columns[nextColumnId].width;
            columnArr.splice(position, 1);
            const newData = {
                ...layers,
                entities: {
                    ...layers.entities,
                    rows: {
                        ...layers.entities.rows,
                        [rowId]: {
                            ...layers.entities.rows[rowId],
                            columns: columnArr
                        }
                    },
                    columns: {
                        ...layers.entities.columns,
                        [nextColumnId]: {
                            ...layers.entities.columns[nextColumnId],
                            width: nextColumnWidth + deletingSpace
                        }
                    }
                }                
            }
            return updateLayers(newData);
        }
    }
    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };
    const newColumnData = (dataArr, newColumnId) => {
        let columnData = {
            ...layers.entities.columns,
            [newColumnId]: {
                id: newColumnId,
                wrappers: []
            }
        };
        const totalColumn = dataArr.length;
        const equialWidth = 100 / totalColumn;
        for (let i = 0; i < totalColumn; i++) {
            columnData[dataArr[i]] = {
                ...columnData[dataArr[i]],
                width: equialWidth
            }
        }
        return columnData;
    }
    const addNewColumn = (rowId) => {
        const newColumnId = uuid();
        const updateColumnOrder = [...layers.entities.rows[rowId].columns];
        updateColumnOrder.push(newColumnId);
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                rows: {
                    ...layers.entities.rows,
                    [rowId]: {
                        ...layers.entities.rows[rowId],
                        columns: updateColumnOrder
                    }
                },
                columns: newColumnData(updateColumnOrder, newColumnId)
            }
        }
        return updateLayers(newData);
    }
    const addNewRow = (sectionId) => {
        const newRowId = uuid();
        const newColumnId = uuid();
        const updateRowOrder = Array.from(layers.entities.section[sectionId].rows)
        updateRowOrder.push(newRowId)
        const newData = {
            ...layers,
            entities: {
                ...layers.entities,
                section: {
                    ...layers.entities.section,
                    [sectionId]: {
                        ...layers.entities.section[sectionId],
                        rows: updateRowOrder
                    }
                },
                rows: {
                    ...layers.entities.rows,
                    [newRowId]: {
                        id: newRowId,
                        columns: [newColumnId]
                    }
                },
                columns: {
                    ...layers.entities.columns,
                    [newColumnId]: {
                        id: newColumnId,
                        width: 100,
                        wrappers: []
                    }
                }
            }
        }
        return updateLayers(newData)
    }
    const columnStyleBar = (id) => {
        return dispatch({
            type: actions.TOGGLE_COLUMN_STYLE,
            column: id
        });
    }
    
    return (
        <Droppable droppableId={props.sectionId} type="rowLayouts">
            {(provided, snapshot) => (
                <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={{
                        display: props.display ? "block" : "none"
                    }}
                    className={classnames("layout-row-order")}
                >
                    {layers.entities.section[props.sectionId].rows.length !== 0 && layers.entities.section[props.sectionId].rows.map((rowId, index) => {
                        const countOfRow = layers.entities.section[props.sectionId].rows.length;
                        const rowStaticName = index + 1;
                        return (
                            <Draggable draggableId={rowId} index={index}>
                                {dragProvided => (
                                    <div
                                        {...dragProvided.draggableProps}
                                        ref={dragProvided.innerRef}
                                        className={classnames('layout-row', snapshot.isDraggingOver && "isDragging")}
                                    >
                                        <div style={{border: '1px solid #cccccc', backgroundColor: '#ffffff'}}>
                                            <div className="layout-row-name" {...dragProvided.dragHandleProps}>
                                                <i className="material-icons" {...dragProvided.dragHandleProps}>drag_indicator</i>
                                                {_get(layers.entities.rows[rowId].rowData, 'name', `Row ${rowStaticName}`)}
                                                {countOfRow > 1 && <i className="material-icons right" onClick={() => deleteRow(rowId)}>delete</i>}
                                            </div>
                                            <ul className="layout-column">
                                                {layers.entities.rows[rowId].columns.map((columnId, indexOfColumn) => {
                                                    const countOfColumn = layers.entities.rows[rowId].columns.length
                                                    const columnStaticName = indexOfColumn + 1
                                                    return (
                                                        <li>
                                                            {_get(layers.entities.columns[columnId], 'name', `Column ${columnStaticName}`)}
                                                            <i className="material-icons right" onClick={() => columnStyleBar(columnId)}>settings</i>
                                                            {countOfColumn > 1 && <i className="material-icons right" onClick={() => deleteColumn(columnId, rowId)}>delete</i>}
                                                        </li>
                                                    );
                                                })}
                                            </ul>
                                            <div className="layout-add-column-btn" onClick={() => addNewColumn(rowId)}><i className="material-icons">add_circle_outline</i>Add Column</div>
                                        </div>
                                    </div>
                                )}
                            </Draggable>
                        )
                    })}
                    {provided.placeholder}
                    <div className="layout-add-row-btn" onClick={() => addNewRow(props.sectionId)}><i className="material-icons">add_circle_outline</i>Add Row</div>
                </div>
            )}
        </Droppable>
    );
}

export default RowLayout;
import React, { useState, useContext, useEffect } from 'react';
import ContentEditable from "react-contenteditable";
import { Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from 'lodash.get';

import { Store } from "../../../../store";
import actions from "../../../../store/action-types";
import RowLayout from './layoutRow';

const SectionLayout = (props) => {
    const {
        state,
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);
    const [dropDown, setDropDown] = useState(false)
    const [sectionName, setSectionName] = useState(null);
    const [editSectionName, setEditSectionName] = useState(true);

    useEffect(() => {
        const newName = _get(layers.entities.section[props.sectionId], 'sectionsData.name', '');
        setSectionName(newName);
    }, [stateHistory])

    const dropDownToggle = () => {
        setDropDown(!dropDown);
    }
    const closeStylebar = (id) => {
        return dispatch({
            type: actions.TOGGLE_SECTION_STYLE,
            section: id
        });
    }
    const updateLayers = data => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    };

    const handleChange = (e, id) => {
        setSectionName(e.target.value);
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                section: {
                    ...layers.entities.section,
                    [id]: {
                        ...layers.entities.section[id],
                        sectionsData: {
                            ...layers.entities.section[id].sectionsData,
                            name: e.target.value
                        }
                    }
                }
            }
        }
        return updateLayers(data);
    }

    const deleteSection = (id) => {
        const sectionsArr = stateHistory.layers.result.sections
        const position = sectionsArr.indexOf(id);
        sectionsArr.splice(position, 1);

        const sectionList = stateHistory.layers.entities.section;
        const numRows = _get(sectionList[id], 'rows', null);

        const rowList = stateHistory.layers.entities.rows;
        const columnList = stateHistory.layers.entities.columns;
        const wrapperList = stateHistory.layers.entities.wrappers;
        const elementList = stateHistory.layers.entities.element;

        numRows && numRows.map(rowId => {
            rowList[rowId].columns.map(columnId => {
                columnList[columnId].wrappers.map(wrapperId => {
                    wrapperList[wrapperId].elements.map(elementId => {
                        delete elementList[elementId];
                    });

                    delete wrapperList[wrapperId];
                });
                delete columnList[columnId];
            });
            delete rowList[rowId];
        });
        delete sectionList[id];

        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                section: sectionList,
                rows: rowList,
                columns: columnList,
                wrappers: wrapperList,
                element: elementList
            },
            result: {
                ...layers.result,
                sections: sectionsArr
            }
        }
        return updateLayers(data);
    }
    return (
        <Draggable draggableId={props.sectionId} index={props.index}>
            {dragProvided => (
                <React.Fragment>

                    <div className={classnames("layout-section-order", props.draggingOver && 'dragging')}
                        {...dragProvided.draggableProps}
                        ref={dragProvided.innerRef}
                    >
                        <a href={`#${props.sectionId}`}>
                            <i className="material-icons" {...dragProvided.dragHandleProps}>drag_indicator</i>
                        </a>
                        {editSectionName ? <ContentEditable
                            className="editable"
                            html={sectionName}
                            disabled={false}
                            tagName="span"
                            onChange={(e) => handleChange(e, props.sectionId)}
                        /> : <span className="editable" onClick={() => setEditSectionName(true)}>{sectionName}</span>}
                        <a href={`#${props.sectionId}`}>
                            {!props.onlyOneSection && <i className="material-icons right" onClick={() => deleteSection(props.sectionId)}>delete</i>}
                            <i className="material-icons right" onClick={() => closeStylebar(props.sectionId)}>settings</i>
                            <i className="material-icons right" onClick={() => dropDownToggle()}>keyboard_arrow_downward</i>
                        </a>
                    </div>
                    <RowLayout display={dropDown} sectionId={props.sectionId} />
                </React.Fragment>
            )}
        </Draggable>
    );
}

export default SectionLayout;
import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import { ClipLoader } from 'react-spinners';
import Slider from 'react-rangeslider';
import Switch from "react-switch";
import _get from 'lodash.get';

import { Store } from '../../../../store';
import actions from "../../../../store/action-types";
import ImageUpload from '../../../../../components/image-upload';
import ColorBoxSelection from '../../../elements/colorBoxSelection';

const StickyBar = () => {
    const {
        state: { layers },
        dispatch,
        history: { state: stateHistory, set }
    } = useContext(Store);
    const [stickyEnable, setStickyEnable] = useState(false);
    const [stickyType, setStickyType] = useState(null);
    const [stickyProperties, setStickyProperties] = useState(null);
    const [backgroundColor, setBackgroundColor] = useState(null);
    const [backgroundColorPopup, setBackgroundColorPopup] = useState(false);
    const [textColor, setTextColor] = useState(null);
    const [textColorPopup, setTextColorPopup] = useState(false);
    const [defaultAction, setDefaultAction] = useState(true);
    const [submitColor, setSubmitColor] = useState(null);
    const [submitColorPopup, setSubmitColorPopup] = useState(false);
    const [submitBackground, setSubmitBackground] = useState(null);
    const [submitBackgroundPopup, setSubmitBackgroundPopup] = useState(false);
    const [fontSize, setFontSize] = useState(null);

    const getData = () => {
        setStickyProperties(_get(stateHistory.layers.result, 'sticky', null));
        setStickyEnable(_get(stateHistory.layers.result, 'sticky.enable', false));
        setBackgroundColor(_get(stateHistory.layers.result, 'sticky.background', null));
        setStickyType(_get(stateHistory.layers.result, 'sticky.type', null));
        setDefaultAction(_get(stateHistory.layers.result, 'sticky.defaultAction', true));
        setTextColor(_get(stateHistory.layers.result, 'sticky.textColor', "#000000"));
        setSubmitColor(_get(stateHistory.layers.result, 'sticky.submitColor', "#000000"));
        setSubmitBackground(_get(stateHistory.layers.result, 'sticky.submitBackground', "#eeeeee"));
        setFontSize(_get(stateHistory.layers.result, 'sticky.fontSize', 14));
    }

    const buttonStyle = () => {
        if (stickyType === "textfieldbutton" || stickyType === "textbutton") {
            return true;
        } else {
            return false;
        }
    }

    useEffect(() => {
        getData();
    }, [stateHistory]);

    const updatelayer = (newData) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: newData
        });
    }

    const handleStickyEnable = (value) => {
        // setStickyEnable(value);
        if (value && stickyProperties === null) {
            const newSticky = {
                ...layers,
                result: {
                    ...layers.result,
                    sticky: {
                        ...layers.result.sticky,
                        enable: value,
                        position: 'top',
                        type: "textfieldbutton",
                        background: "#fcbc5a",
                        defaultAction: true
                    }
                }
            }
            return updatelayer(newSticky);
        } else {
            const updateStickyMode = {
                ...layers,
                result: {
                    ...layers.result,
                    sticky: {
                        ...layers.result.sticky,
                        enable: value,
                    }
                }
            }
            return updatelayer(updateStickyMode);
        }
    }

    const handleStickyUpdate = (name, value) => {
        setBackgroundColorPopup(false);
        setSubmitBackgroundPopup(false);
        setSubmitColorPopup(false);
        setTextColorPopup(false);
        setStickyProperties({
            ...stickyProperties,
            [name]: value
        });
        
        const updateStickyProperties = {
            ...layers,
            result: {
                ...layers.result,
                sticky: {
                    ...layers.result.sticky,
                    [name]: value,
                }
            }
        }
        return updatelayer(updateStickyProperties);
    }
    
    return (
        <div className="page-properties">
            <h4 style={{ display: "flex", justifyContent: "space-between" }}>
                <span>Sticky Bar</span>
                <div className="property-switch">
                    <Switch
                        checked={stickyEnable}
                        onChange={(e) => handleStickyEnable(e)}
                        onColor="#fcbc5a" onHandleColor="#f8f8f8"
                        offColor="#c5c5c5" offHandleColor="#f8f8f8"
                        handleDiameter={12} height={16} width={30} />
                </div>
            </h4>
            {stickyProperties ? stickyEnable ? <React.Fragment>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Layout</label>
                    <div className="sticky-bar-layouts">
                        <ul>
                            <li className="sticky-bar-layout">
                                <input type="radio" name="stickyLayout" value="textfieldbutton" id="textfieldbutton"
                                    checked={stickyProperties.type === "textfieldbutton"}
                                />
                                <label for="textfieldbutton" onMouseDown={() => handleStickyUpdate("type", "textfieldbutton")}>Text Field Button</label>
                            </li>
                            <li className="sticky-bar-layout">
                                <input type="radio" name="stickyLayout" value="textbutton" id="textbutton"
                                    checked={stickyProperties.type === "textbutton"}
                                />
                                <label for="textbutton" onMouseDown={() => handleStickyUpdate("type", "textbutton")}>Text Button</label>
                            </li>
                            <li className="sticky-bar-layout">
                                <input type="radio" name="stickyLayout" value="textlink" id="textlink"
                                    checked={stickyProperties.type === "textlink"}
                                />
                                <label for="textlink" onMouseDown={() => handleStickyUpdate("type", "textlink")}>Text Link</label>
                            </li>
                            <li className="sticky-bar-layout">
                                <input type="radio" name="stickyLayout" value="text" id="text"
                                    checked={stickyProperties.type === "text"}
                                />
                                <label for="text" onMouseDown={() => handleStickyUpdate("type", "text")}>Text</label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Action</label>
                    <div style={{ lineHeight: "12px", marginBottom: "15px" }}>
                        <input 
                            type="checkbox"
                            id="default-action" 
                            checked={stickyProperties.defaultAction}
                            onChange={(e) => handleStickyUpdate('defaultAction', e.target.checked)}
                            style={{
                                marginTop: "0",
                                marginRight: "5px",
                                width: "18px",
                                height: "18px",
                                lineHeight: "12px",
                                verticalAlign: "bottom"
                            }}
                        />
                        <label for="default-action"
                            style={{
                                margin: "0px",
                                fontSize: "12px",
                                fontWeight: "400"
                            }}
                        >Enable for default thank you popup</label>
                    </div>
                    {!defaultAction ? <div>
                        <span>Redirection URL</span>
                        <input
                            type="text" value={stickyProperties.actionUrl}
                            onChange={(e) => setStickyProperties({...stickyProperties, 'actionUrl': e.target.value})}
                            onBlur={(e) => handleStickyUpdate('actionUrl', e.target.value)}
                            style={{
                                width: "100%",
                                borderRadius: "0px",
                                padding: "5px"
                            }}
                        />
                    </div> : <div>
                            <span>Write popup message</span>
                            <textarea 
                                value={stickyProperties.popupMessage}
                                onChange={(e) => setStickyProperties({...stickyProperties, 'popupMessage': e.target.value})}
                                onBlur={(e) => handleStickyUpdate('popupMessage', e.target.value)}
                                style={{
                                    width: "100%",
                                    height: "100px",
                                    borderRadius: "0px",
                                    padding: "5px"
                                }}
                            />
                        </div>}
                </div>
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Sticky</label>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Color</span>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setTextColorPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: stickyProperties.textColor }}></div>
                            </div>
                            {textColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleStickyUpdate("textColor", textColor)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={stickyProperties.textColor} onChange={(e) => setTextColor(e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleStickyUpdate("textColor", textColor)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Background</span>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setBackgroundColorPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: stickyProperties.background }}></div>
                            </div>
                            {backgroundColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleStickyUpdate("background", backgroundColor)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={stickyProperties.background} onChange={(e) => setBackgroundColor(e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleStickyUpdate("background", backgroundColor)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div style={{marginBottom: "15px"}}>
                        <ColorBoxSelection selectedColor={color => handleStickyUpdate("background", color)} />
                    </div>
                    <div style={{ "display": "flex", "justify-content": "space-between", marginBottom: "15px" }}>
                        <span>Font Family</span>
                        <div>
                            <select value={stickyProperties.fontFamily} onChange={(e) => handleStickyUpdate('fontFamily', e.target.value)}>
                                <option value="">Select Font Family</option>
                                <option value="Roboto">Roboto</option>
                                <option value="Open Sans">Open Sans</option>
                                <option value="Ubuntu">Ubuntu</option>
                                <option value="Josefin Sans">Josefin Sans</option>
                                <option value="Tajawal">Tajawal</option>
                                <option value="Raleway">Raleway</option>
                                <option value="Oxygen">Oxygen</option>
                                <option value="Maven Pro">Maven Pro</option>
                                <option value="Catamaran">Catamaran</option>
                                <option value="Orbitron">Orbitron</option>
                                <option value="Darker Grotesque">Darker Grotesque</option>
                                <option value="Montserrat Alternates">Montserrat Alternates</option>
                                <option value="Nunito Sans">Nunito Sans</option>
                                <option value="Noto Serif"> Noto Serif </option>
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                                <option value="Lato"> Lato </option>
                                <option value="Roboto Slab"> Roboto Slab </option>
                                <option value="Montserrat"> Montserrat </option> 
                                <option value="Anton"> Anton </option>
                                <option value="Source Sans Pro">Source Sans Pro  </option>
                                <option value="Roboto Condensed"> Roboto Condensed </option>
                                <option value="Oswald">Oswald</option>
                                <option value="Enemy"> Enemy </option>
                              
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                              
                               

                            </select>
                        </div>
                    </div>
                    <div className="two-column">
                        <span>Font Size</span>
                        <div className="rangeslider-wrapper">
                        <div
                            className="rangeslider-slider"
                            // onBlur={() => handleStickyUpdate('fontSize', fontSize)}
                        >
                            <Slider min={1} max={40} step={1}
                                value={stickyProperties.fontSize || 1}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleStickyUpdate('fontSize', e)}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {stickyProperties.fontSize || 1}
                        </div>
                        </div>
                    </div>
                    {/* <div className="property-form">
                        <button className="edit-form-button" onClick={() => setImportPopup(true)}>Upload Image</button>
                    </div>
                    {backgroundImage && <div className="property-form">
                        <div className="image-viewer">
                            <img src={backgroundImage} alt="URL is not valid" />
                        </div>
                        <button className="edit-form-button" onClick={() => handlePageProperties('backgroundImage', null)}>Remove Image</button>
                    </div>}
                    {importPopup && <ImageUpload imageLink={(e) => handlePageProperties('backgroundImage', e)} popupClose={() => setImportPopup(false)} />} */}
                </div>
                {buttonStyle() && <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Submit Button</label>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Color</span>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setSubmitColorPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: stickyProperties.submitColor }}></div>
                            </div>
                            {submitColorPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleStickyUpdate("submitColor", submitColor)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={stickyProperties.submitColor} onChange={(e) => setSubmitColor(e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleStickyUpdate("submitColor", submitColor)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div style={{ "display": "flex", "justify-content": "space-between" }}>
                        <span>Background</span>
                        <div>
                            <div className="color-picker-swatch" onClick={() => setSubmitBackgroundPopup(true)}>
                                <div className="color-picker-color" style={{ backgroundColor: stickyProperties.submitBackground }}></div>
                            </div>
                            {submitBackgroundPopup && <div className="color-picker-popover">
                                <div className='color-picker-cover' onClick={() => handleStickyUpdate("submitBackground", submitBackground)} />
                                <div className='color-picker-wrapper'>
                                    <ChromePicker color={stickyProperties.submitBackground} onChange={(e) => setSubmitBackground(e.hex)} disableAlpha />
                                    <button className='color-picker-button' onClick={() => handleStickyUpdate("submitBackground", submitBackground)}>Ok</button>
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div>
                        <ColorBoxSelection selectedColor={color => handleStickyUpdate("submitBackground", color)} />
                    </div>
                </div>}
                <div style={{ "padding": "15px 15px", "border-bottom": "1px solid #cccccc" }}>
                    <label>Position</label>
                    <div className="position-btn">
                        <input
                            name="position"
                            type="radio"
                            value="top"
                            id="stickytop"
                            checked={stickyProperties.position === "top"}
                        /><label for="stickytop" onMouseDown={() => handleStickyUpdate("position", 'top')} >Top</label>
                        <input
                            name="position"
                            type="radio"
                            value="bottom"
                            id="stickybottom"
                            checked={stickyProperties.position === "bottom"}
                        /><label for="stickybottom" onMouseDown={() => handleStickyUpdate("position", 'bottom')} >Bottom</label>
                    </div>
                </div>
            </React.Fragment> : <p style={{ padding: "15px", textAlign: "center", fontSize: "12px" }}>
                    Ready to raise the bar in your digital marketing? Easily publish alert bars on landing page. <br /> Enable to continue
            </p> : <p style={{ padding: "15px", textAlign: "center", fontSize: "12px" }}>
                    Ready to raise the bar in your digital marketing? Easily publish alert bars on landing page. <br /> Enable to continue
            </p>}
        </div>
    );
}

export default StickyBar;
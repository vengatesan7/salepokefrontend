import React, { useContext } from "react";

import { Store } from "../../../store";
import actions from "../../../store/action-types";

import "./toolbar.scss";
import Button from "../../elements/button";
import Layouts from "./tools/layouts";
import Tools from "./tools/tools";
import PageProperties from "./tools/pageProperties";
import Tracking from "./tools/tracking";
import StickyBar from "./tools/sticky";
import PopupBar from "./tools/popup";

function Toolbar({ tools }) {
  const { state, dispatch } = useContext(Store);
  const addLayer = data => {
    return dispatch({
      type: actions.ADD_LAYER,
      payload: data
    });
  };
  const closeToolBar = props => {
    return dispatch({
      type: actions.CLOSE_TOOLBAR
    });
  };
  const toolbarType = () => {
    switch (state.menuBarType) {
      case "layout":
        return <Layouts />
      case "widgets":
        return <Tools tools={tools} addLayer={addLayer} /> 
      case "style":
        return <PageProperties />
      case "track":
        return <Tracking />
      case "sticky":
        return <StickyBar />
      case "popup":
        return <PopupBar />;
      default:
        break;
    }
  }
  return (
    <div className="toolbar">
      <div className="toolbar-head">
        <div className="close-toolbar"><i className="material-icons" onClick={closeToolBar}>close</i></div>
      </div>
      {toolbarType()}
    </div>
  );
}

export default Toolbar;

import uuid from 'uuid/v4';

export const TOOL_ITEMS = [
  {
    id: uuid(),
    name: "Title",
    type: "title",
    className: "title"
  },
  {
    id: uuid(),
    name: "Paragraph",
    type: "paragraph",
    className: "paragraph"
  },
  {
    id: uuid(),
    name: "Image",
    type: "image",
    className: "picture"
  },
  {
    id: uuid(),
    name: "Form",
    type: "form",
    className: "form"
  }
];

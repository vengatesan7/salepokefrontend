import React from "react";

const TextField = ({ data }) => {
    return (
        <span
            className="text"
            style={{
                color: data.textColor || "#000000"
            }}>
            {data.content}
        </span>
    );
}
const InoutField = ({ placeholder }) => {
    return (
        <input className="input" placeholder={placeholder} />
    );
}
const ButtonField = ({ data }) => {
    return (
        <button
            className="button"
            style={{
                backgroundColor: data.submitBackground || "#eeeeee",
                color: data.submitColor || "#000000"
            }}
        >
            {data.buttonText}
        </button>
    );
}

const LinkField = ({ data }) => {
    return (
        <a
            className="link"
            href="#"
            style={{ color: data.textColor || "#000000" }}
        >
            {data.linkText}
        </a>
    );
}

const Sticky = ({ data }) => {
    switch (data.type) {
        case "textfieldbutton":
            return (
                <div className={data.type}>
                    <TextField data={data} />
                    <InoutField placeholder={data.inputPlaceholder} />
                    <ButtonField data={data} />
                </div>
            );
        case "textbutton":
            return (
                <div className={data.type}>
                    <TextField data={data} />
                    <ButtonField data={data} />
                </div>
            );
        case "textlink":
            return (
                <div className={data.type}>
                    <TextField data={data} />
                    <LinkField data={data} />
                </div>
            );
        case "text":
            return (
                <div className={data.type}>
                    <TextField data={data} />
                </div>
            );
        default:
            return;
    }
}

export default Sticky;
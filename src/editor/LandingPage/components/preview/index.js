import React, { useState, useEffect } from "react";
import classnames from 'classnames';
import _get from 'lodash.get';
import { normalize } from "normalizr";

import { PostData } from '../../../../services/PostData';
import layersSchema from "../builder/schema";

// import Title from ;
// import Paragraph from "../builder/canvas/elements/paragraph";
import Image from "../builder/canvas/elements/image";
import Form from "../builder/canvas/elements/form";
import Line from "../builder/canvas/elements/line";
import Button from "../builder/canvas/elements/button";
import ImageText from "../builder/canvas/elements/imagestext";
import Video from "../builder/canvas/elements/video";
import Space from "../builder/canvas/elements/space";
import Sticky from "./sticky";
import "./preview.scss";

import layoutsData from '../../store/layouts-data2';
import Title from "./elements/title";
import Paragraph from "./elements/paragraph";
import PriceChart from "./elements/pricechart";
import Timer from "../builder/canvas/elements/timer";
import Icons from "../builder/canvas/elements/icon";
import Card from "../builder/canvas/elements/card";
import Product from "../builder/canvas/elements/product";
import Upsell from "../builder/canvas/elements/upsell";
import { element } from "prop-types";

const RenderElement = ({ data }) => {
    switch (data.elementType) {
        case "TITLE":
            return <Title elementData={data} />;
        case "PARAGRAPH":
            return <Paragraph elementData={data} />;
        case "LINK":
            return <Paragraph elementData={data} />;
        case "FORM":
            return <Form elementData={data} />;
        case "IMAGE":
            return <Image elementData={data} />;
        case "LINE":
            return <Line elementData={data} />;
        case "BUTTON":
            return <Button elementData={data} />;
        case "IMAGEPLUSTEXT":
            return <ImageText elementData={data} />;
        case "VIDEO":
            return <Video elementData={data} />;
        case "SPACE":
            return <Space elementData={data} />;
        case "PRICECHART":
            return <PriceChart elementData={data} />;
        case "TIMER":
            return <Timer elementData={data} />;
        case "ICON":
            return <Icons elementData={data} />;
        case "PRODUCT":
            return <Product elementData={data} />;
        case "UPSELL":
            return <Upsell elementData={data} />;
        default:
            return <Title elementData={data} />;
    }
}

export const RenderPage = ({ data }) => {
    const sections = _get(data, "result.sections", []);
    const sectionsData = _get(data, "entities.section", {});
    const rowsData = _get(data, "entities.rows", {});
    const columnsData = _get(data, "entities.columns", {});
    const wrappersData = _get(data, "entities.wrappers", {});
    const elementsData = _get(data, "entities.element", {});
    const formData = _get(data, "entities.formData", {});
    const field = _get(data, "entities.field", {});
    const page = _get(data, "result.page", {});
    const sticky = _get(data, "result.sticky", null);

    if (!data) {
        return null;
    }
    const pageContainerStyle = {
        backgroundColor: page.background,
        backgroundImage: "url(" + page.backgroundImage + ")",
        margin: page.margin,
        color: _get(page, 'fontColor', ''),
        fontFamily: _get(page, 'fontFamily', ''),
    }
    const containerStyle = {
        "width": page.width,
        // "margin": page.margin
    }
    const elementStyle = elementData => {        
        const oldStyle = {
            marginTop: _get(elementData, 'marginTop', null),
            marginRight: _get(elementData, 'marginRight', null),
            marginBottom: _get(elementData, 'marginBottom', null),
            marginLeft: _get(elementData, 'marginLeft', null),
            paddingTop: _get(elementData, 'paddingTop', null),
            paddingRight: _get(elementData, 'paddingRight', null),
            paddingBottom: _get(elementData, 'paddingBottom', null),
            paddingLeft: _get(elementData, 'paddingLeft', null),
            color: _get(elementData, 'fontColor', null),
            borderWidth: _get(elementData, 'borderWidth', null),
            borderStyle: _get(elementData, 'borderStyle', null),
            borderRadius: _get(elementData, 'borderRadius', null),
            borderColor: _get(elementData, 'borderColor', null),
            backgroundColor: _get(elementData, 'background', null),
            backgroundImage: _get(elementData, 'backgroundImage', null),
            backgroundPosition: _get(elementData, 'backgroundImagePosition', null),
            backgroundRepeat: _get(elementData, 'backgroundRepeat', null),
            backgroundSize: _get(elementData, 'backgroundImageSize', null),
            textAlign: _get(elementData, 'textAlign', null),
            fontSize: _get(elementData, 'fontSize', null),
            fontFamily: _get(elementData, 'fontFamily', null),
            lineHeight: _get(elementData, 'lineHeight', null),
            fontWeight: _get(elementData, 'bold', null),
            fontStyle: _get(elementData, 'italic', null),
            textDecoration: _get(elementData, 'textDecoration', null)
        }
        const boxModule = {
            marginTop: _get(elementData.styles, 'marginTop', oldStyle.marginTop),
            marginRight: _get(elementData.styles, 'marginRight', oldStyle.marginRight),
            marginBottom: _get(elementData.styles, 'marginBottom', oldStyle.marginBottom),
            marginLeft: _get(elementData.styles, 'marginLeft', oldStyle.marginLeft),
            paddingTop: _get(elementData.styles, 'paddingTop', oldStyle.paddingTop),
            paddingRight: _get(elementData.styles, 'paddingRight', oldStyle.paddingRight),
            paddingBottom: _get(elementData.styles, 'paddingBottom', oldStyle.paddingBottom),
            paddingLeft: _get(elementData.styles, 'paddingLeft', oldStyle.paddingLeft),
        }
        switch (elementData.elementType) {
            case "BUTTON":
                return {
                    //...boxModule,
                    color: _get(elementData, 'fontColor', ''),
                    textAlign: _get(elementData, 'textAlign', ''),
                    fontSize: _get(elementData, 'fontSize', ''),
                    fontFamily: _get(elementData, 'fontFamily', ''),
                    lineHeight: elementData.lineHeight && _get(elementData, 'lineHeight', '') + 'px',
                    fontWeight: _get(elementData, 'bold', ''),
                    fontStyle: _get(elementData, 'italic', ''),
                    textDecoration: _get(elementData, 'textDecoration', '')
                }
            default:
                return {
                    ...boxModule,
                    // color: _get(elementData.style, 'color', oldStyle.color),
                    // textAlign: _get(elementData.style, 'textAlign', oldStyle.textAlign),
                    // fontSize: _get(elementData.style, 'fontSize', oldStyle.fontSize),
                    // fontFamily: _get(elementData.style, 'fontFamily', oldStyle.fontFamily),
                    // lineHeight: _get(elementData.style, 'lineHeight', oldStyle.lineHeight)+'px',
                    // fontWeight: _get(elementData.style, 'bold', oldStyle.bold),
                    // fontStyle: _get(elementData.style, 'italic', oldStyle.italic),
                    // textDecoration: _get(elementData.style, 'textDecoration', oldStyle.textDecoration)
                    color: _get(elementData, 'fontColor', ''),
                    textAlign: _get(elementData, 'textAlign', ''),
                    fontSize: _get(elementData, 'fontSize', ''),
                    fontFamily: _get(elementData, 'fontFamily', ''),
                    lineHeight: elementData.elementType != "PARAGRAPH" ? _get(elementData, 'lineHeight', '') + 'px' : ( _get(elementData, 'lineHeight', '') <= 3 ? _get(elementData, 'lineHeight', '') : 1.5 ),
                    fontWeight: _get(elementData, 'bold', ''),
                    fontStyle: _get(elementData, 'italic', ''),
                    textDecoration: _get(elementData, 'textDecoration', ''),
                    borderWidth: _get(elementData.style, 'borderWidth', oldStyle.borderWidth),
                    borderStyle: _get(elementData.style, 'borderStyle', oldStyle.borderStyle),
                    borderRadius: _get(elementData.style, 'borderRadius', oldStyle.borderRadius),
                    borderColor: _get(elementData.style, 'borderColor', oldStyle.borderColor),
                    backgroundColor: _get(elementData.style, 'background', oldStyle.backgroundColor),
                    backgroundImage: `url(${_get(elementData.style, 'backgroundImage', oldStyle.backgroundImage)})`,
                    backgroundPosition: _get(elementData.style, 'backgroundPosition', oldStyle.backgroundPosition),
                    backgroundRepeat: _get(elementData.style, 'backgroundRepeat', oldStyle.backgroundRepeat),
                    backgroundSize: _get(elementData.style, 'backgroundImageSize', oldStyle.backgroundSize),
                }
        }
        
    }
    const columnStyle = columnData => {
        return {
            width: _get(columnData, "width", null) !== null ? `${columnData.width}%` : '100%',
            "background-color": _get(columnData.styles, "background", ""),
            "background-image": _get(columnData.styles, "backgroundImage", null) && `url(${_get(columnData.styles, "backgroundImage", null)})`,
            "padding-top": _get(columnData.styles, 'paddingTop', ''),
            "padding-right": _get(columnData.styles, 'paddingRight', ''),
            "padding-bottom": _get(columnData.styles, 'paddingBottom', ''),
            "padding-left": _get(columnData.styles, 'paddingLeft', '')
        }
    }
    const rowStyle = (style) => {
        return {
            "display": 'flex',
            "padding-top": _get(style.rowData, 'styles.paddingTop', ''),
            "padding-right": _get(style.rowData, 'styles.paddingRight', ''),
            "padding-bottom": _get(style.rowData, 'styles.paddingBottom', ''),
            "padding-left": _get(style.rowData, 'styles.paddingLeft', ''),
            "text-align": _get(style.rowData, "styles.textAlign", ''),
            "background-color": _get(style.rowData, "styles.backgroundColor", ''),
            "background-image": "url(" + _get(style.rowData, 'styles.backgroundImage', '') + ")",
            "background-size": "cover",
            "background-position": "bottom",
            "height": _get(style.rowData, "styles.height", ''),
            "margin-top": _get(style.rowData, "styles.marginTop", ''),
            "margin-bottom": _get(style.rowData, "styles.marginBottom", ''),
            "padding-top": _get(style.rowData, "styles.paddingTop", ''),
            "padding-right": _get(style.rowData, "styles.paddingRight", ''),
            "padding-bottom": _get(style.rowData, "styles.paddingBottom", ''),
            "padding-left": _get(style.rowData, "styles.paddingLeft", ''),
            "color": _get(style.rowData, "styles.fontColor", ''),
        }
    }
    const sectionStyle = style => {
        return {
            "background-image": "url(" + _get(style, 'sectionsData.styles.backgroundImage', '') + ")",
            "background-color": _get(style, 'sectionsData.styles.background', ''),
            "background-size": "cover",
            "background-position": "center",
            "padding-top": _get(style, 'sectionsData.styles.paddingTop', ''),
            "padding-bottom": _get(style, 'sectionsData.styles.paddingBottom', ''),
            "padding-left": _get(style, 'sectionsData.styles.paddingLeft', ''),
            "padding-right": _get(style, 'sectionsData.styles.paddingRight', ''),
        }
    }
    
    return (
        <div className="main-container" style={pageContainerStyle}>
            {sticky && sticky.position === "top" && <section className={classnames('sticky-bar', sticky.position)} style={{ backgroundColor: sticky.background }}>
                <div className="container" style={containerStyle}>
                    <Sticky data={sticky} />
                </div>
            </section>}
            {sections.map(sectionId => (
                <section className={classnames("pb-section", _get(sectionsData[sectionId].sectionsData, 'visibleOnMobile', true) && "show-mobile")} key={sectionId} style={sectionStyle(sectionsData[sectionId])}>                    
                    <div className="container" style={containerStyle}>
                        {sectionsData[sectionId].rows.map(rowId => (
                            <div className="pb-row" key={rowId} style={rowStyle(rowsData[rowId])}>
                                {rowsData[rowId].columns.map(columnId => (
                                    <React.Fragment>
                                        <div className="pb-column" key={columnId} style={columnStyle(columnsData[columnId])}>
                                            {columnsData[columnId].wrappers.map(wrapperId => {
                                                return (
                                                    <div className="pb-wrapper">
                                                        {wrappersData[wrapperId].elements.map(elementID => {
                                                            const element = elementsData[elementID]
                                                            return (
                                                                <div className="element" style={elementStyle(element)}>
                                                                    {element.elementType === "FORM" ? <form className="form">
                                                                        {elementsData[elementID].formData.map(formId => {                                                                            const thisFormData = formData[formId];
                                                                            const fieldItemList = formData[formId].fields;
                                                                            const labelRequired = _get(formData[formId], 'label', true);
                                                                            const labelColor = _get(formData[formId], 'labelColor', "#000000");
                                                                            const columns = _get(formData[formId], 'columns', 1);
                                                                            const width = 100 / columns;
                                                                            const submitAlign = _get(formData[formId], 'submitAlign', "center");
                                                                            const spaceBetweenColumn = _get(formData[formId], 'spaceBetweenColumn', 0);
                                                                            const formInputBorderRadius = _get(formData[formId], 'formInputBorderRadius', 0);
                                                                            return (fieldItemList.length !== 0 && <React.Fragment>
                                                                                <div className="form-container" style={{ columnCount: _get(formData[formId], 'columns', 1) }}>
                                                                                    {fieldItemList.map(fieldId => {
                                                                                        const fieldData = field[fieldId];
                                                                                        if (fieldData.type === 'text' || fieldData.type === 'email' || fieldData.type === 'number') {
                                                                                            return (
                                                                                                <div className="field-group" style={columns > 1 ? { width: `${width}%`, borderRadius: formInputBorderRadius, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%`, borderRadius: formInputBorderRadius }}>
                                                                                                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                                                                                                    <input type={fieldData.type} placeholder={fieldData.placeholder} style={{ borderRadius: formInputBorderRadius }}/>
                                                                                                </div>
                                                                                            );
                                                                                        } else if (fieldData.type === 'textarea') {
                                                                                            return (
                                                                                                <div className="field-group" style={columns > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn, borderRadius: formInputBorderRadius, } : { width: `${width}%` }}>
                                                                                                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                                                                                                    <input type={fieldData.type} placeholder={fieldData.placeholder} style={{ height: "65px", borderRadius: formInputBorderRadius }}/>
                                                                                                </div>
                                                                                            );
                                                                                        } else if (fieldData.type === 'file') {
                                                                                            return (
                                                                                                <div className="field-group" style={columns > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%`}}>
                                                                                                    <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>
                                                                                                    <div className="file-upload-group">
                                                                                                        <div className="file-upload-fake">
                                                                                                            <i className="material-icons">cloud_upload</i>
                                                                                                            <span>Drop Here</span>
                                                                                                        </div>
                                                                                                        <input type={fieldData.type} className="file-upload" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            )
                                                                                        } else if (fieldData.type === 'radio' || fieldData.type === 'checkbox') {
                                                                                            return (
                                                                                                <div className="field-group" style={columns > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { width: `${width}%` }}>
                                                                                                    <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>
                                                                                                    {fieldData.options.map(option => {
                                                                                                        return (option !== "" &&
                                                                                                            <div>
                                                                                                                <input type={fieldData.type} name={fieldData.label} value={option} />
                                                                                                                <span>{option}</span>
                                                                                                            </div>
                                                                                                        );
                                                                                                    })}
                                                                                                </div>
                                                                                            );
                                                                                        } else if (fieldData.type === 'select') {
                                                                                            return (
                                                                                                <div className="field-group" style={columns > 1 ? { width: `${width}%`, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn, borderRadius: formInputBorderRadius } : { width: `${width}%`, borderRadius: formInputBorderRadius, }}>
                                                                                                    {labelRequired && <label style={{ color: labelColor }}>{fieldData.label}{fieldData.required && "*"}</label>}
                                                                                                    <select>
                                                                                                        <option>{fieldData.placeholder}</option>
                                                                                                        {fieldData.options.map(option => {
                                                                                                            return (
                                                                                                                <option value={option}>{option}</option>
                                                                                                            )
                                                                                                        })}
                                                                                                    </select>
                                                                                                </div>
                                                                                            )
                                                                                        }
                                                                                    })}
                                                                                </div>
                                                                                <div className="form-footer" style={columns > 1 ? { textAlign: submitAlign, paddingLeft: spaceBetweenColumn, paddingRight: spaceBetweenColumn } : { textAlign: submitAlign }}>
                                                                                    <button
                                                                                        type="submit"
                                                                                        className="form-submit"
                                                                                        style={{
                                                                                            width: thisFormData.submitWidth === undefined ? "100%" : thisFormData.submitWidth === "fullwidth" ? "100%" : "auto",
                                                                                            color: thisFormData.submitColor,
                                                                                            backgroundColor: thisFormData.submitBackground,
                                                                                            borderRadius: thisFormData.submitBorderRadius 
                                                                                        }}
                                                                                    >{thisFormData.submitText || "Submit"}</button>
                                                                                </div>
                                                                            </React.Fragment>
                                                                            );
                                                                        })}
                                                                    </form> : <React.Fragment>
                                                                        <RenderElement data={element} key={elementID} />
                                                                        {/* {element.elementType !== 'PRICECHART' && <RenderElement data={element} key={elementID} />} */}
                                                                    </React.Fragment>}
                                                                </div>
                                                            )
                                                        })}
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </React.Fragment>
                                ))}
                            </div>
                        ))}
                    </div>
                </section>
            ))}
            {sticky && sticky.position === "bottom" && <section className={classnames('sticky-bar', sticky.position)} style={{ backgroundColor: sticky.background }}>
                <div className="container" style={containerStyle}>
                    <Sticky data={sticky} />
                </div>
            </section>}
        </div>
    );
}

const PreviewPage = () => {
    const [templateData, setTemplateData] = useState(null)
    const [newLayers, setNewLayers] = useState(null);
    const sectionsLength = _get(newLayers, 'result.sections', []).length;
    const editortype = localStorage.getItem("editortype");
    useEffect(() => {
        if (editortype === "funnels") {
            const updateTemplate = {
                funnel_card_id: localStorage.getItem("templateId")
            }
            PostData('ms3', 'retrievejson', updateTemplate).then((result) => {
                console.log(result);
                if (result !== 'Invalid') {
                    setTemplateData(JSON.parse(result.json));
                }
            });
        } else if (editortype === "landingpage") {
            const updateTemplate = {
                landing_page_id: localStorage.getItem("templateId")
            }
            PostData('ms4', 'landingpageretreivejson', updateTemplate).then((result) => {
                if (result !== 'Invalid') {
                    setTemplateData(JSON.parse(result.json));
                }
            });
        } else {
            setTemplateData(layoutsData);
        }
    }, []);
    useEffect(() => {
        templateData !== null && setNewLayers(normalize(templateData, layersSchema));
    }, [templateData]);

    return (
        <div className="preview">
            {sectionsLength > 0 ? <RenderPage data={newLayers} /> : <div style={{ textAlign: "center", marginTop: "150px" }}>Loading . . .</div>}
        </div>
    );
}

export default PreviewPage;

import React from 'react';
import _get from "lodash.get";

const Title = ({ elementData }) => {
  return (
    <h1 
      dangerouslySetInnerHTML={{__html: _get(elementData, 'contents', null)}}
      style={{
        fontSize: _get(elementData, 'fontSize', 'inherit')
      }}
    />
  );
};

export default Title;

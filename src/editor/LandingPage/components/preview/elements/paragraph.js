import React from 'react';
import _get from "lodash.get";

const Paragraph = ({ elementData }) => {
  return (
    <div 
      dangerouslySetInnerHTML={{__html: _get(elementData, 'contents', null)}}
      style={{
        fontSize: _get(elementData, 'fontSize', 'inherit')
      }}
    />
  );
};

export default Paragraph;

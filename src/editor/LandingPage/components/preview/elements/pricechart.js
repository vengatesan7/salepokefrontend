import React from 'react';
import _get from "lodash.get";

const PriceChart = ({ elementData }) => {
  return (
    <div className="pricing-table">
        <div className="pricing-header">
            <div className="pricing-label" dangerouslySetInnerHTML={{__html: _get(elementData, 'pricelyLabel', '')}} />
            <div className="pricing-figure">
                <div className="pricing-currency" dangerouslySetInnerHTML={{__html: _get(elementData, 'pricelyCurrency', '')}} />
                <div className="pricing-amount" dangerouslySetInnerHTML={{__html: _get(elementData, 'pricelyAmount', '')}} />
            </div>
            <div className="pricing-foreword" dangerouslySetInnerHTML={{__html: _get(elementData, 'pricelyForeword', '')}} />
        </div>
        <div className="pricing-list" dangerouslySetInnerHTML={{__html: _get(elementData, 'pricelyList', '')}} />
    </div>
  );
};

export default PriceChart;
import React, { useState } from "react";

import { EmailStore } from "./index";

const EmailDataProvider = props => {
    const [emailName, setEmailName] = useState(null);
    const [emailState, setEmailState] = useState(null);
    const [selectedElement, setSelectedElement] = useState(null);
    const [propertyBarActive, setPropertyBarActive] = useState(false);
    const [isNewElementDrag, setIsNewElementDrag] = useState(false);
    const [mouseInPosition, setMouseInPosition] = useState("section");
    const [isDndRuning, setIsDndRuning] = useState(false);
    const [dndOnDrag, setDndOnDrag] = useState({
        type: null,
        dropPosition: null,
        droppedId: null,
        droppedIndex: null,
        parentDropId: null
    });
    const [deleteAction, setDeleteAction] = useState({
        enable: false,
        type: null,
        id: null,
        index: null,
        parentIDs: null
    });
    const [dndDragType, setDndDragType] = useState(null);
    const [dropPosition, setDropPosition] = useState(null);
    const [fontStyle, setFontStyle] = useState({
        bold: false,
        italic: false,
        underline: false,
        fontSize: null
    });
    const [parentIDs, setParentIDs] = useState({
        section: null,
        structure: null,
        column: null
    });
    const [editable, setEditable] = useState({
        enable: false,
        type: null,
        id: null,
        index: null,
        parentId: null
    });
    const [propertiesDropDowns, setPropertiesDropDowns] = useState({
        buttonSize: false,
        inputColor: false,
        inputBackgroundColor: false,
        buttonColor: false,
        buttonBackgroundColor: false,
        colorEmail: false,
        fontSizeOption: false,
        backgroundColorEmail: false,
        alignOption: false,
        verticalPadding: false,
        horizontalPadding: false,
        radius: false,
        elementBorder: false,
        elementLink: false,
        marginTop: false,
        marginBottom: false
    });

    const store = {
        emailName,
        setEmailName,
        emailState,
        setEmailState,
        propertyBarActive,
        setPropertyBarActive,
        selectedElement,
        setSelectedElement,
        isNewElementDrag,
        setIsNewElementDrag,
        propertiesDropDowns,
        setPropertiesDropDowns,
        // DND Store
        parentIDs,
        setParentIDs,
        isDndRuning,
        setIsDndRuning,
        dndDragType,
        setDndDragType,
        dropPosition,
        setDropPosition,
        dndOnDrag,
        setDndOnDrag,
        // UI Store
        mouseInPosition,
        setMouseInPosition,
        //Properties 
        fontStyle,
        setFontStyle,
        editable,
        setEditable,
        // Delete Action
        deleteAction,
        setDeleteAction
    };
    return <EmailStore.Provider value={store}>{props.children}</EmailStore.Provider>;
};

export default EmailDataProvider;

import { schema } from "normalizr";

const element = new schema.Entity("element");
const container = new schema.Entity("containers", {
  elements: [element]
});
const column = new schema.Entity('columns', {
  containers: [container]
});
const structure = new schema.Entity('structures', {
  columns: [column]
});
const section = new schema.Entity('section', {
  structures: [structure],
});

const emailSchema = {
  sections: [section],
  structures: [structure],
  columns: [column],
  containers: [container],
  elements: [element]
};

export default emailSchema;

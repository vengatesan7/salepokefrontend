import React, { useContext, useState, useEffect } from 'react';
import { normalize } from "normalizr";
import _get from "lodash.get";

import emailSchema from '../../store/schema';
import { YoutubeThumb } from '../../../components/YouTubeThumb';

const EmailPreview = () => {

    const [emailState, setEmailState] = useState(null);

    const sampleData = {
        sections: [
            {
                id: "sectionOne",
                style: {
                    backgroundColor: "#ffffff"
                },
                structures: [
                    {
                        id: "structureImage",
                        style: {
                            backgroundColor: "#28a5d1"
                        },
                        columns: [
                            {
                                id: "columnImage",
                                width: 560,
                                containers: [
                                    {
                                        id: "containerImage",
                                        elements: [
                                            {
                                                id: "elementImage",
                                                type: "IMAGE",
                                                url: "https://micro4.cisconet.world/public/uploads/imagelibraries/user_180/1343620606.png",
                                                alt: "Image",
                                                style: {
                                                    width: 100,
                                                    textAlign: "center",
                                                    paddingTop: 50,
                                                    paddingRight: 100,
                                                    paddingBottom: 50,
                                                    paddingLeft: 100
                                                }
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        id: "structureOne",
                        style: {
                            backgroundColor: "#28a5d1",
                            backgroundImage: "https://lh3.google.com/u/0/d/1iloWPPNGhkRhRVCsiLWn4QK2F3ecWtYO=w1600-h827-iv1",
                            backgroundRepeat: "no-repeat",
                            backgroundPosition: "bottom right",
                            backgroundSize: "contain"
                        },
                        columns: [
                            {
                                id: "columnTwo",
                                width: 560,
                                containers: [
                                    {
                                        id: "containerThree",
                                        elements: [
                                            {
                                                id: "elementFour",
                                                type: "TEXT",
                                                content: "Get them before thy're gone",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "elementSix",
                                                type: "TEXT",
                                                content: "Pick 4 free minis + enjoy free shipping with any $40 purchase.",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "buttonElement",
                                                type: "BUTTON",
                                                content: "CHOOSE NOW",
                                                buttonStyle: {
                                                    backgroundColor: "#ffa7bd",
                                                    fontSize: 12,
                                                    color: "#ffffff",
                                                    paddingTop: 10,
                                                    paddingRight: 15,
                                                    paddingBottom: 10,
                                                    paddingLeft: 15,
                                                    display: "inline-block",
                                                    textAlign: "center"
                                                },
                                                style: {
                                                    paddingTop: 15,
                                                    paddingRight: 15,
                                                    paddingBottom: 15,
                                                    paddingLeft: 15
                                                }
                                            },
                                            {
                                                id: "elementFive",
                                                type: "TEXT",
                                                content: "Don't wait. Ends in.",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }

    useEffect(() => {
        setEmailState(normalize(sampleData, emailSchema));
    }, []);
    console.log(emailState)

    const elementStyle = (id, type, style) => {
        switch (type) {
            case "TEXT":
                return style;
            case "IMAGE":
                const imageStyle = {...style, width: "auto"}
                return imageStyle;
            case "BUTTON":
                const buttonStyle = _get(emailState.entities.element[id], "buttonStyle", {textAlign: "center"});
                return {...style, textAlign: buttonStyle.textAlign};
            default:
                break;
        }
    }
    
    const elementDomValue = (elementData) => {
        switch (elementData.type) {
            case "TEXT":
                return  <p 
                    dangerouslySetInnerHTML={{__html: elementData.content}}
                />;
            case "IMAGE":
                return <img
                    src={elementData.url}
                    style={{ width: `${elementData.style.width}%` }}
                    alt={elementData.alt}
                />;
            case "BUTTON":
                return <button
                    style={elementData.buttonStyle}
                    dangerouslySetInnerHTML={{__html: elementData.content}}
                />;
            case "VIDEO":
                return <span>
                    <img src={YoutubeThumb(elementData.url, 'big')} />
                </span>;
            default:
                break;
        }
    }

    return (emailState !== null && <div>
        <table cellspacing="0" cellpadding="0" className="email-container"><tbody><tr><td>
            <table cellspacing="0" cellpadding="0" className="email-content" width="100%"><tbody>
                {emailState.result.sections.map((sectionId, sectionIndex) => {
                    const sectionStyleData = _get(emailState.entities.section[sectionId], "style", {});
                    const sectionStyle = {
                    ...sectionStyleData,
                    backgroundImage: sectionStyleData.backgroundImage !== undefined ? `url(${sectionStyleData.backgroundImage})`: ""
                    }
                    return (
                        <tr className="email-section">
                            <td className="email-section-wrapper" style={sectionStyle}>
                                <table cellspacing="0" cellpadding="0" width="600" className="email-structure-wrapper" style={{margin: "0px auto"}}><tbody>
                                {emailState.entities.section[sectionId].structures.map((structureId, structureIndex) => {
                                    const structureStyleData = _get(emailState.entities.structures[structureId], "style", {});

                                    const structureStyle = {
                                    ...structureStyleData,
                                    backgroundImage: structureStyleData.backgroundImage !== undefined ? `url(${structureStyleData.backgroundImage})`: ""
                                    }
                                    return (
                                        <tr className="email-structure">
                                            <td style={{...structureStyle, padding: "5px 20px"}}>
                                                <table cellspacing="0" cellpadding="0" width="100%"><tbody>
                                                    <tr className="email-structure-wrapper">
                                                        {emailState.entities.structures[structureId].columns.map((columnId, columnIndex) => {
                                                            const columnsCount = emailState.entities.structures[structureId].columns.length;
                                                            return (
                                                                <td
                                                                    style={{verticalAlign: "top"}}
                                                                    align={columnsCount - 1 === columnIndex ? "right" : "left"}
                                                                >
                                                                    <table cellspacing="0" cellpadding="0" width={emailState.entities.columns[columnId].width}><tbody>
                                                                        {emailState.entities.columns[columnId].containers.map((containerId, containerIndex) => {
                                                                            const elementCount = emailState.entities.containers[containerId].elements.length;
                                                                            const containerStyleData = _get(emailState.entities.containers[containerId], "style", {});
                                                                            const containerStyle = {
                                                                                ...containerStyleData,
                                                                                backgroundImage: containerStyleData.backgroundImage !== undefined ? `url(${containerStyleData.backgroundImage})`: ""
                                                                            }
                                                                            return (
                                                                                <tr className="email-container">
                                                                                    <td className="email-container-wrapper" style={{...containerStyle,padding: "5px"}}>
                                                                                        <table cellspacing="0" cellpadding="0" width="100%"><tbody className="email-element-wapper">
                                                                                            {emailState.entities.containers[containerId].elements.length !== 0 && emailState.entities.containers[containerId].elements.map((elementId, elementIndex) => {
                                                                                                const thisElementStyle = _get(emailState.entities.element[elementId], "style", null);
                                                                                                return (
                                                                                                    <tr className="email-element">
                                                                                                        <td className="email-element-wrapper">
                                                                                                            <div style={thisElementStyle !== null && elementStyle(elementId, emailState.entities.element[elementId].type, thisElementStyle)}>
                                                                                                                {elementDomValue(emailState.entities.element[elementId])}
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                );
                                                                                            })}
                                                                                        </tbody></table>
                                                                                    </td>
                                                                                </tr>
                                                                            )
                                                                        })}
                                                                    </tbody></table>
                                                                </td>
                                                            )
                                                        })}
                                                    </tr>
                                                </tbody></table>
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody></table>
                            </td>
                        </tr>
                    )}
                    
                )}
            </tbody></table>
        </td></tr></tbody></table>
    </div>);
}

export default EmailPreview;
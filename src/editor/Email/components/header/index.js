import React, { useContext, useState,useRef } from 'react';
import { Button } from 'react-bootstrap';
import { Link , Redirect } from "react-router-dom";
import { denormalize } from "normalizr";
import { toast } from 'react-toastify';
import cookie from 'react-cookies';

import logo from "../../../../images/rc-logo-mini.png";
import "./header.scss";
import Schema from '../../store/schema';
import { PostData } from '../../../../services/PostData';
import { EmailStore } from '../../store';

const EmailEditorHeader = () => {
    const { emailState, emailName } = useContext(EmailStore);
    const [saveLoader, setSaveLoader] = useState(false);
    const [saveExitLoader, setSaveExitLoader] = useState(false);

    const [reDirect, setReDirect] = useState(false);

    // const childRef = useRef();
    const savePopup = () => {
        
        setSaveLoader(true);
        // console.log('editorStatus',cookie.load("editorStatus"))
        // if(cookie.load("editorStatus") === true){
        //     childRef.current.handleUpdateState()
        //     cookie.save("autotrigger",true)
        // }
        const denormalizeData = denormalize(emailState.result, Schema, emailState.entities);
        const saveData = {
            "automation_email_id": cookie.load("_automationid"),
            "email_text": JSON.stringify(denormalizeData),
            "type":localStorage.getItem("followuptype")
        }
        PostData('ms3', 'automationtemplatesavejson', saveData).then(response => {
            console.log(response)
            setSaveLoader(false); 
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    // cookie.save("autotrigger",false)
                    // cookie.save("editorStatus",false)
                    toast.info(response.message);
                }
            } else {
                console.log(response);
                toast.warn("Update is not saved");
            }
        });
    }
    const saveExitPopup = () => {

        setSaveExitLoader(true);
        const denormalizeData = denormalize(emailState.result, Schema, emailState.entities);
        const saveData = {
            "automation_email_id": cookie.load("_automationid"),
            "email_text": JSON.stringify(denormalizeData),
            "type":localStorage.getItem("followuptype")
        }
        PostData('ms3', 'automationtemplatesavejson', saveData).then(response => {
            setSaveExitLoader(false); 
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    // cookie.save("autotrigger",false)
                    // cookie.save("editorStatus",false)
                    setReDirect(true)
                    toast.info(response.message);
                }
            } else {
                toast.warn("Update is not saved");
            }
        });
    }
    const redirect = () =>{
        if(reDirect){
        if(localStorage.getItem("followuptype") =="followup"){
            return <Redirect  to={{pathname:'/followup/steps', state:{followup_id:localStorage.getItem("followup_id")}}}/>
        }
        else if(localStorage.getItem("followuptype") =="Funnelfollowup"){
            return <Redirect  to={{pathname:'/funnels/funnelfollowupsteps', state:{followup_id:localStorage.getItem("followup_id")}}}/>
        }else{
            return <Redirect  to={{pathname:'funnels/automation'}}/>
        }}
        
    }
    return (<React.Fragment>
        {redirect()}
        <header
            className="email-header"
        >
            <div className="email-header-left">
                <div className="logo">
                    <Link to="/dashboard">
                        <img src={logo} alt="logo" />
                    </Link>
                </div>
                <h3 className="title">
                 {localStorage.getItem("followuptype") =="followup" ?
                  <Link to={{pathname:'/followup/steps', state:{followup_id:localStorage.getItem("followup_id")}}}>
                        <i className="material-icons">keyboard_backspace</i>
                        Back to Follow-up steps
                    </Link> 
                 : localStorage.getItem("followuptype") =="Funnelfollowup" ?   <Link to={{pathname:'/funnels/funnelfollowupsteps', state:{followup_id:localStorage.getItem("followup_id")}}}>
                 <i className="material-icons">keyboard_backspace</i>
                 Back to Funnel Follow-up steps
             </Link> : <Link to="/funnels/automation">
                 <i className="material-icons">keyboard_backspace</i>
                 Back to email list
             </Link>}
                </h3>
            </div>
            <div className="email-header-center">
                <span className="email-header-name">{emailName}</span>
            </div>
            <div className="email-header-right">

            <Button
                    onClick={() => saveExitPopup()}
                    variant="primary"
                    style={{ float: "right" , background:"#12344d" ,borderColor  :"#12344d"         }}
                    disabled={saveExitLoader}
                >{saveExitLoader ? "Loading..." : " Save and Exit "}</Button>

                <Button
                    onClick={() => savePopup()}
                    variant="primary"
                    style={{ float: "right",    marginRight: "20px"     }}
                    disabled={saveLoader}
                >{saveLoader ? "Loading..." : " Save "}</Button>


            </div>
        </header>
   </React.Fragment> );
}
 
export default EmailEditorHeader;
import React, { useContext } from 'react';

import './propertybar.scss';
import { EmailStore } from '../../../store';

const PropertyBar = () => {
    const {
        setPropertyBarActive,
        setSelectedElement,
        setEditable
    } = useContext(EmailStore);

    const closePropertyBar = () => {
        setPropertyBarActive(false);
        setSelectedElement(null);
        setEditable({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentId: null
        });
    }

    return (
        <React.Fragment>
            <div className="property-bar-container" onMouseDown={() => closePropertyBar()} />
        </React.Fragment>
    );
}

export default PropertyBar;
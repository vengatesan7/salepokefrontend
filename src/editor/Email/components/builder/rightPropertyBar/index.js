import React, { useContext } from 'react';
import classnames from 'classnames';

import './propertybar.scss';
import { EmailStore } from '../../../store';
import Border from './properties/border';
import Image from './properties/image';
import Padding from './properties/padding';
import Background from './properties/background';
import Button from './properties/button';
import ActionURL from './properties/action';
import Video from './properties/video';
import Space from './properties/space';
import Line from './properties/line';

const AllProperties = () => {
    const {
        emailState,
        setEmailState,
        setDeleteAction,
        editable,
        setEditable,
        setPropertyBarActive,
        setSelectedElement
    } = useContext(EmailStore);

    const elementProperties = () => {
        const elementType = emailState.entities.element[editable.id].type
        switch (elementType) {
            case "IMAGE":
                return <React.Fragment>
                    <Image />
                    <ActionURL />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "TEXT":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "BUTTON":
                return <React.Fragment>
                    <Button />
                    <ActionURL />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "VIDEO":
                return <React.Fragment>
                    <Video />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "SPACE":
                return <React.Fragment>
                    <Space />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                </React.Fragment>;
            case "LINE":
                return <React.Fragment>
                    <Line />
                    {/* <Border blockType={editable.type} /> */}
                </React.Fragment>;
            default:
                break;
        }
    }

    const propertiesByType = () => {
        switch (editable.type) {
            case "SECTION":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>
            case "STRUCTURE":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    {/* <Padding  blockType={editable.type} /> */}
                    <Border blockType={editable.type} />
                </React.Fragment>
            case "ELEMENT":
                return elementProperties();
            case "CONTAINER":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                </React.Fragment>
            default:
                break;
        }
    }

    const handleClose = () => {
        setPropertyBarActive(false);
        setSelectedElement(null);
        setEditable({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentId: null
        });
    }

    return (
        <React.Fragment>
            <div className={classnames("properties-options-header", editable.type === "SECTION" && "section-header")}>
                <div className="property-seleted">
                    {editable.type}
                </div>
                <div className="block-settings">
                    {editable.type === "SECTION" && <div className="button">
                        <i className="material-icons">save</i>
                    </div>}
                    <div
                        className="button"
                        onClick={() => setDeleteAction({
                            enable: true,
                            type: editable.type,
                            id: editable.id,
                            index: editable.index,
                            parentIDs: editable.parentId
                        })}
                    >
                        <i className="material-icons">delete</i>
                    </div>
                    <div className="button">
                        <i className="material-icons">content_copy</i>
                    </div>
                    <div className="button" onClick={() => handleClose()}
                    >
                        <i className="material-icons">close</i>
                    </div>
                </div>
            </div>
            <div className="properties-options-content">
                {propertiesByType()}
            </div>
        </React.Fragment>
    );
}

export default AllProperties;
import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from 'classnames';
import Switch from "react-switch";
import _get from "lodash.get";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { EmailStore } from '../../../../store';
import { ColorLuminance } from '../../../../../components/ColorLuminance';
import { BorderStyle } from './utils';

const ColorPopup = ({color, handleCallBack}) => {
    const [popup, setPopup] = useState(false);
    return (
        <React.Fragment>
            <div className="color-picker-swatch" onClick={() => setPopup(true)}>
                <div 
                    className="color-picker-color"
                    style={{
                        backgroundColor: color
                    }}></div>
            </div>
            {popup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => setPopup(false)} />
                <div className='color-picker-wrapper'>
                    <ChromePicker color={color} onChange={(e) => handleCallBack(e.hex)} disableAlpha />
                    <button className='color-picker-button' onClick={() => setPopup(false)}>Ok</button>
                </div>
            </div>}
        </React.Fragment>
    );
}

const Button = () => {
    const {
        emailState,
        setEmailState,
        editable
    } = useContext(EmailStore);

    const [buttonStyle, setButtonStyle] = useState(null);
    const [content, setContent] = useState("");
    const [allSidePadding, setAllSidePadding] = useState(false);
    const [backgroundPopup, setBackgroundPopup] = useState(false);
    const [allSideBorder, setAllSideBorder] = useState(false);
    const [buttonGradient, setButtonGradient] = useState({
        enable: false,
        direction: "bottom"
    });
    useEffect(() => {
        setButtonStyle(_get(emailState.entities.element[editable.id], "buttonStyle", {
            backgroundColor: "#ffffff"
        }));
        setContent(_get(emailState.entities.element[editable.id], "content",""));
        const gradient = _get(emailState.entities.element[editable.id], "buttonStyle.backgroundImage", "");
        setButtonGradient(gradient === "" ? {
            enable: false,
            direction: "bottom"
        } : {
            enable: true,
            direction: emailState.entities.element[editable.id].buttonStyle.backgroundImage.split(" ", 2)[1].split(",", 1)[0]
        });
    }, [editable.id]);

    const handleStyle = (name, value) => {
        setButtonStyle({
            ...buttonStyle,
            [name]: value
        });
        if (name === "backgroundColor") {
            if (buttonGradient.enable) {
                const newStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        element: {
                            ...emailState.entities.element,
                            [editable.id]: {
                                ...emailState.entities.element[editable.id],
                                buttonStyle: {
                                    ...emailState.entities.element[editable.id].buttonStyle,
                                    [name]: value,
                                    backgroundImage: `linear-gradient(to ${buttonGradient.direction}, ${value} 0%, ${ColorLuminance(value, 0.5)} 51%, ${value} 100%)`
                                }
                            }
                        }
                    }
                }
                return setEmailState(newStyleUpdate);
            }
        } else if (name === "paddingAllSide") {
            setButtonStyle({
                ...buttonStyle,
                paddingTop: value,
                paddingRight: value,
                paddingBottom: value,
                paddingLeft: value,
            });
            const newStyleUpdate = {
                ...emailState,
                entities: {
                    ...emailState.entities,
                    element: {
                        ...emailState.entities.element,
                        [editable.id]: {
                            ...emailState.entities.element[editable.id],
                            buttonStyle: {
                                ...emailState.entities.element[editable.id].buttonStyle,
                                paddingTop: value,
                                paddingRight: value,
                                paddingBottom: value,
                                paddingLeft: value,
                            }
                        }
                    }
                }
            }
            return setEmailState(newStyleUpdate);
        } else if (name === "allSideBorderStyle") {
        // }, e)}
        // handleBorderWidth={(e) => handleStyle('allSideBorderWidth', e)}
        // handleBorderColor={(e) => handleStyle('allSideBorderColor") {
            // borderTopStyle}
            //                 borderWidth={buttonStyle.borderTopWidth}
            //                 borderColor={buttonStyle.borderTopColor
            setButtonStyle({
                ...buttonStyle,
                borderTopStyle: value,
                borderRightStyle: value,
                borderBottomStyle: value,
                borderLeftStyle: value,
            });
            const newStyleUpdate = {
                ...emailState,
                entities: {
                    ...emailState.entities,
                    element: {
                        ...emailState.entities.element,
                        [editable.id]: {
                            ...emailState.entities.element[editable.id],
                            buttonStyle: {
                                ...emailState.entities.element[editable.id].buttonStyle,
                                borderTopStyle: value,
                                borderRightStyle: value,
                                borderBottomStyle: value,
                                borderLeftStyle: value,
                            }
                        }
                    }
                }
            }
            return setEmailState(newStyleUpdate);
        } else if (name === "allSideBorderWidth") {
            // }, e)}
            // handleBorderWidth={(e) => handleStyle('allSideBorderWidth', e)}
            // handleBorderColor={(e) => handleStyle('allSideBorderColor") {
                // borderTopStyle}
                //                 borderWidth={buttonStyle.borderTopWidth}
                //                 borderColor={buttonStyle.borderTopColor
            setButtonStyle({
                ...buttonStyle,
                borderTopWidth: value,
                borderRightWidth: value,
                borderBottomWidth: value,
                borderLeftWidth: value,
            });
            const newStyleUpdate = {
                ...emailState,
                entities: {
                    ...emailState.entities,
                    element: {
                        ...emailState.entities.element,
                        [editable.id]: {
                            ...emailState.entities.element[editable.id],
                            buttonStyle: {
                                ...emailState.entities.element[editable.id].buttonStyle,
                                borderTopWidth: value,
                                borderRightWidth: value,
                                borderBottomWidth: value,
                                borderLeftWidth: value,
                            }
                        }
                    }
                }
            }
            return setEmailState(newStyleUpdate);
        } else if (name === "allSideBorderColor") {
            setButtonStyle({
                ...buttonStyle,
                borderTopColor: value,
                borderRightColor: value,
                borderBottomColor: value,
                borderLeftColor: value,
            });
            const newStyleUpdate = {
                ...emailState,
                entities: {
                    ...emailState.entities,
                    element: {
                        ...emailState.entities.element,
                        [editable.id]: {
                            ...emailState.entities.element[editable.id],
                            buttonStyle: {
                                ...emailState.entities.element[editable.id].buttonStyle,
                                borderTopColor: value,
                                borderRightColor: value,
                                borderBottomColor: value,
                                borderLeftColor: value,
                            }
                        }
                    }
                }
            }
            return setEmailState(newStyleUpdate);
        }
        const newStyleUpdate = {
            ...emailState,
            entities: {
                ...emailState.entities,
                element: {
                    ...emailState.entities.element,
                    [editable.id]: {
                        ...emailState.entities.element[editable.id],
                        buttonStyle: {
                            ...emailState.entities.element[editable.id].buttonStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setEmailState(newStyleUpdate);
    }

    const handleButtonGradient = (name, value) => {
        setButtonGradient({
            ...buttonGradient,
            [name]: value
        });
        if (name === "enable") {
            if (value) {
                handleStyle("backgroundImage", `linear-gradient(to ${buttonGradient.direction}, ${buttonStyle.backgroundColor} 0%, ${ColorLuminance(buttonStyle.backgroundColor, 0.5)} 51%, ${buttonStyle.backgroundColor} 100%)`);
            } else {
                handleStyle("backgroundImage", "");
            }
        } else {
            handleStyle("backgroundImage", `linear-gradient(to ${value}, ${buttonStyle.backgroundColor} 0%, ${ColorLuminance(buttonStyle.backgroundColor, 0.5)} 51%, ${buttonStyle.backgroundColor} 100%)`);
        }
    }

    const handleButtonText = (name, value) => {
        setContent(value);
        const newContentUpdate = {
            ...emailState,
            entities: {
                ...emailState.entities,
                element: {
                    ...emailState.entities.element,
                    [editable.id]: {
                        ...emailState.entities.element[editable.id],
                        [name]: value
                    }
                }
            }
        }
        return setEmailState(newContentUpdate);
    }

    return(
        buttonStyle !== null && <div className="properties-group">
            <div className="properties-group-name">
                BUTTON PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field ">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Button Text</div>
                    <div className="properties-value padding">
                        <input type="text" value={content} onChange={(e) => handleButtonText('content', e.target.value)}/>
                    </div>
                </div>
                <div className="properties-field ">
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Font Size</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={40}
                                min={0}
                                step={1}
                                value={buttonStyle.fontSize}
                                callBack={(v) => handleStyle("fontSize", v)}
                            />
                        </div>
                    </div>
                    <div className="two-col" style={{"marginTop": "15px"}}>
                        <div className="properties-title">Font Color</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <ColorPopup color={buttonStyle.color} handleCallBack={(e) => handleStyle("color", e)} />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title">Background</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <div className="color-picker-none" onClick={(e) => handleStyle("backgroundColor", "")}><i className="material-icons">not_interested</i></div>
                            <ColorPopup color={buttonStyle.backgroundColor} handleCallBack={(e) => handleStyle("backgroundColor", e)} />
                        </div>
                    </div>
                    {buttonStyle.backgroundColor !== undefined && <div className="two-col" style={{"marginTop": "15px"}}>
                        <div className="properties-title">Gradient</div>
                        <div className="properties-value">
                            <Switch checked={buttonGradient.enable} onChange={(e) => handleButtonGradient("enable", e)}
                                onColor="#43da71" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30}
                            />
                        </div>
                    </div>}
                    {buttonGradient.enable && <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "32px"}}>Sample Buttons</div>
                        <div className="properties-value" style={{display: "flex"}}>
                            <div 
                                onClick={() => handleButtonGradient("direction", "right")}
                                style={{cursor: "pointer", marginRight: "10px"}}
                            >
                                <div style={{
                                    backgroundImage: `linear-gradient(to right, ${buttonStyle.backgroundColor} 0%, ${ColorLuminance(buttonStyle.backgroundColor, 0.5)} 51%, ${buttonStyle.backgroundColor} 100%)`,
                                    padding: "5px 15px",
                                    color: buttonStyle.color,
                                    borderRadius: "5px",
                                    boxShadow: buttonGradient.direction === "right" && "0 0 0 2px #00ff00"
                                }}>Button</div>
                            </div>
                            <div
                                onClick={() => handleButtonGradient("direction", "bottom")}
                                style={{cursor: "pointer"}}
                            >
                                <div style={{
                                    backgroundImage: `linear-gradient(to bottom, ${buttonStyle.backgroundColor} 0%, ${ColorLuminance(buttonStyle.backgroundColor, 0.5)} 51%, ${buttonStyle.backgroundColor} 100%)`,
                                    padding: "5px 15px",
                                    color: buttonStyle.color,
                                    borderRadius: "5px",
                                    boxShadow: buttonGradient.direction === "bottom" && "0 0 0 2px #00ff00"
                                }}>Button</div>
                            </div>
                        </div>
                    </div>}
                </div>
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title">Internal Padding</div>
                        <div className="properties-value">
                            <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                            <Switch checked={allSidePadding} onChange={(e) => setAllSidePadding(e)}
                                onColor="#43da71" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30}
                            />
                        </div>
                    </div>
                    {!allSidePadding ? <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>All Sides</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={1000}
                                min={0}
                                step={1}
                                value={buttonStyle.paddingTop}
                                callBack={(v) => handleStyle("paddingAllSide", v)}
                            />
                        </div>
                    </div> :
                    <div className="two-col">
                        <div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Top</div>
                                <div className="properties-value padding">
                                    <SpinButtonInput
                                        max={1000}
                                        min={0}
                                        step={1}
                                        value={buttonStyle.paddingTop}
                                        callBack={(v) => handleStyle("paddingTop", v)}
                                    />
                                </div>
                            </div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Bottom</div>
                                <div className="properties-value padding">
                                    <SpinButtonInput
                                        max={1000}
                                        min={0}
                                        step={1}
                                        value={buttonStyle.paddingBottom}
                                        callBack={(v) => handleStyle("paddingBottom", v)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Right</div>
                                <div className="properties-value padding">
                                    <SpinButtonInput
                                        max={1000}
                                        min={0}
                                        step={1}
                                        value={buttonStyle.paddingRight}
                                        callBack={(v) => handleStyle("paddingRight", v)}
                                    />
                                </div>
                            </div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Left</div>
                                <div className="properties-value padding">
                                    <SpinButtonInput
                                        max={1000}
                                        min={0}
                                        step={1}
                                        value={buttonStyle.paddingLeft}
                                        callBack={(v) => handleStyle("paddingLeft", v)}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Align</div>
                    <div className="properties-value flex-row alignments">
                        <button className={classnames(buttonStyle.textAlign === "left" && "active")} onClick={() => handleStyle("textAlign", "left")}>
                            <i className="material-icons">format_align_left</i>
                        </button>
                        <button className={classnames(buttonStyle.textAlign === "center" && "active")} onClick={() => handleStyle("textAlign", "center")}>
                            <i className="material-icons">format_align_center</i>
                        </button>
                        <button className={classnames(buttonStyle.textAlign === "right" && "active")} onClick={() => handleStyle("textAlign", "right")}>
                            <i className="material-icons">format_align_right</i>
                        </button>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title">Border</div>
                        <div className="properties-value">
                            <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                            <Switch checked={allSideBorder} onChange={(e) => setAllSideBorder(e)}
                                onColor="#43da71" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30}
                            />
                        </div>
                    </div>
                    {!allSideBorder ? <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>All Sides</div>
                        <div className="properties-value">
                            <BorderStyle
                                borderStyle={buttonStyle.borderTopStyle}
                                borderWidth={buttonStyle.borderTopWidth}
                                borderColor={buttonStyle.borderTopColor}
                                handleBorderStyle={(e) => handleStyle('allSideBorderStyle', e)}
                                handleBorderWidth={(e) => handleStyle('allSideBorderWidth', e)}
                                handleBorderColor={(e) => handleStyle('allSideBorderColor', e)}
                            />
                        </div>
                    </div> : <div className="properties-field two-col">
                        <div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Top</div>
                                <div className="properties-value">
                                    <BorderStyle
                                        borderStyle={buttonStyle.borderTopStyle}
                                        borderWidth={buttonStyle.borderTopWidth}
                                        borderColor={buttonStyle.borderTopColor}
                                        handleBorderStyle={(e) => handleStyle('borderTopStyle', e)}
                                        handleBorderWidth={(e) => handleStyle('borderTopWidth', e)}
                                        handleBorderColor={(e) => handleStyle('borderTopColor', e)}
                                    />
                                </div>
                            </div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Bottom</div>
                                <div className="properties-value">
                                    <BorderStyle
                                        borderStyle={buttonStyle.borderBottomStyle}
                                        borderWidth={buttonStyle.borderBottomWidth}
                                        borderColor={buttonStyle.borderBottomColor}
                                        handleBorderStyle={(e) => handleStyle('borderBottomStyle', e)}
                                        handleBorderWidth={(e) => handleStyle('borderBottomWidth', e)}
                                        handleBorderColor={(e) => handleStyle('borderBottomColor', e)}
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Right</div>
                                <div className="properties-value">
                                    <BorderStyle
                                        borderStyle={buttonStyle.borderRightStyle}
                                        borderWidth={buttonStyle.borderRightWidth}
                                        borderColor={buttonStyle.borderRightColor}
                                        handleBorderStyle={(e) => handleStyle('borderRightStyle', e)}
                                        handleBorderWidth={(e) => handleStyle('borderRightWidth', e)}
                                        handleBorderColor={(e) => handleStyle('borderRightColor', e)}
                                    />
                                </div>
                            </div>
                            <div>
                                <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Left</div>
                                <div className="properties-value">
                                    <BorderStyle
                                        borderStyle={buttonStyle.borderLeftStyle}
                                        borderWidth={buttonStyle.borderLeftWidth}
                                        borderColor={buttonStyle.borderLeftColor}
                                        handleBorderStyle={(e) => handleStyle('borderLeftStyle', e)}
                                        handleBorderWidth={(e) => handleStyle('borderLeftWidth', e)}
                                        handleBorderColor={(e) => handleStyle('borderLeftColor', e)}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Border Radius</div>
                    <div className="properties-value padding">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={buttonStyle.borderRadius || 0}
                            callBack={(v) => handleStyle("borderRadius", v)}
                        />
                    </div>
                </div>
            </div>
            {/* <div className="properties-group-fields">
                
            </div> */}
        </div>
    )
}

export default Button;
import React, { useState, useContext, useEffect } from 'react';
import classnames from 'classnames';
import Switch from "react-switch";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { EmailStore } from '../../../../store';

const Padding = ({ blockType }) => {
    const {
        emailState,
        setEmailState,
        editable
    } = useContext(EmailStore)

    const [selectedData, setSelectedData] = useState(null);
    const [allSidePadding, setAllSidePadding] = useState(false);


    useEffect(() => {
        const findThisData = (params) => {
            switch (blockType) {
                case "ELEMENT":
                    return emailState.entities.element[editable.id];
                case "SECTION":
                    return emailState.entities.section[editable.id];
                case "STRUCTURE":
                    return emailState.entities.structures[editable.id];
                case "CONTAINER":
                    return emailState.entities.containers[editable.id];
                default:
                    break;
            }
        }
        // const thisData = emailState.entities.element[editable.id];
        const thisData = findThisData();
        setSelectedData(thisData);
        if (thisData.style !== undefined) {
            if (thisData.style.paddingTop !== undefined) {
                if (
                    thisData.style.paddingTop !== thisData.style.paddingRight || 
                    thisData.style.paddingTop !== thisData.style.paddingBottom ||
                    thisData.style.paddingTop !== thisData.style.paddingLeft
                ) {
                    setAllSidePadding(true);
                }
            } else {
                setSelectedData({
                    ...thisData,
                    style: {
                        ...thisData.style,
                        paddingTop: 0,
                        paddingRight: 0,
                        paddingBottom: 0,
                        paddingLeft: 0,
                    }
                });
            }
        } else {
            setSelectedData({
                ...thisData,
                style: {
                    paddingTop: 0,
                    paddingRight: 0,
                    paddingBottom: 0,
                    paddingLeft: 0,
                }
            });
        }
    }, [editable.id]);

    const handleStyle = (name, value) => {
        setSelectedData({
            ...selectedData,
            style: {
                ...selectedData.style,
                [name]: value
            }
        });
        if (name === "paddingAllSide") {
            setSelectedData({
                ...selectedData,
                style: {
                    ...selectedData.style,
                    paddingTop: value,
                    paddingRight: value,
                    paddingBottom: value,
                    paddingLeft: value
                }
            });
            switch (blockType) {
                case "ELEMENT":
                    const newStyleUpdate = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            element: {
                                ...emailState.entities.element,
                                [editable.id]: {
                                    ...emailState.entities.element[editable.id],
                                    style: {
                                        ...emailState.entities.element[editable.id].style,
                                        paddingTop: value,
                                        paddingRight: value,
                                        paddingBottom: value,
                                        paddingLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setEmailState(newStyleUpdate);
                case "SECTION":
                    const newSectionStyleUpdate = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            section: {
                                ...emailState.entities.section,
                                [editable.id]: {
                                    ...emailState.entities.section[editable.id],
                                    style: {
                                        ...emailState.entities.section[editable.id].style,
                                        paddingTop: value,
                                        paddingRight: value,
                                        paddingBottom: value,
                                        paddingLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setEmailState(newSectionStyleUpdate);
                case "STRUCTURE":
                    const newStrutureStyleUpdate = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            structures: {
                                ...emailState.entities.structures,
                                [editable.id]: {
                                    ...emailState.entities.structures[editable.id],
                                    style: {
                                        ...emailState.entities.structures[editable.id].style,
                                        paddingTop: value,
                                        paddingRight: value,
                                        paddingBottom: value,
                                        paddingLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setEmailState(newStrutureStyleUpdate);
                case "CONTAINER":
                    const newContainerStyleUpdate = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            containers: {
                                ...emailState.entities.containers,
                                [editable.id]: {
                                    ...emailState.entities.containers[editable.id],
                                    style: {
                                        ...emailState.entities.containers[editable.id].style,
                                        paddingTop: value,
                                        paddingRight: value,
                                        paddingBottom: value,
                                        paddingLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setEmailState(newContainerStyleUpdate);
                default:
                    break;
            }
        }
        switch (blockType) {
            case "ELEMENT":
                const newStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        element: {
                            ...emailState.entities.element,
                            [editable.id]: {
                                ...emailState.entities.element[editable.id],
                                style: {
                                    ...emailState.entities.element[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setEmailState(newStyleUpdate);
            case "SECTION":
                const newSectionStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        section: {
                            ...emailState.entities.section,
                            [editable.id]: {
                                ...emailState.entities.section[editable.id],
                                style: {
                                    ...emailState.entities.section[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setEmailState(newSectionStyleUpdate);
            case "STRUCTURE":
                const newStructuresStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        structures: {
                            ...emailState.entities.structures,
                            [editable.id]: {
                                ...emailState.entities.structures[editable.id],
                                style: {
                                    ...emailState.entities.structures[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setEmailState(newStructuresStyleUpdate);
            case "CONTAINER":
                const newContainersStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        containers: {
                            ...emailState.entities.containers,
                            [editable.id]: {
                                ...emailState.entities.containers[editable.id],
                                style: {
                                    ...emailState.entities.containers[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setEmailState(newContainersStyleUpdate);
            default:
                break;
        }
    }

    return(
        selectedData !== null && <div className="properties-group">
            <div className="properties-group-name">
                GENERAL
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title">Padding</div>
                    <div className="properties-value">
                        <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                        <Switch checked={allSidePadding} onChange={(e) => setAllSidePadding(e)}
                            onColor="#43da71" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>
                {!allSidePadding ? <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>All Sides</div>
                    <div className="properties-value padding">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={selectedData.style.paddingTop}
                            callBack={(v) => handleStyle("paddingAllSide", v)}
                        />
                    </div>
                </div> :
                <div className="properties-field two-col">
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Top</div>
                            <div className="properties-value padding">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.paddingTop}
                                    callBack={(v) => handleStyle("paddingTop", v)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Bottom</div>
                            <div className="properties-value padding">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.paddingBottom}
                                    callBack={(v) => handleStyle("paddingBottom", v)}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Right</div>
                            <div className="properties-value padding">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.paddingRight}
                                    callBack={(v) => handleStyle("paddingRight", v)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Padding Left</div>
                            <div className="properties-value padding">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.paddingLeft}
                                    callBack={(v) => handleStyle("paddingLeft", v)}
                                />
                            </div>
                        </div>
                    </div>
                </div>}
            </div>
        </div>
    )
}

export default Padding;
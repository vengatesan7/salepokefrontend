import React, { useState } from 'react';
import { ChromePicker } from 'react-color';

import SpinButtonInput from "../../../../../components/SpinButtonInput";

export const BorderStyle = ({borderStyle, borderWidth, borderColor, handleBorderStyle, handleBorderWidth, handleBorderColor}) => {
    const [popup, setPopup] = useState(false)
    return (
        <React.Fragment>
            <div>
                <select value={borderStyle} onChange={(e) => handleBorderStyle(e.target.value)} className="select-border-style">
                    {borderStyle === undefined && <option>Select Style</option>}
                    <option value="solid">Solid</option>
                    <option value="dotted">Dotted</option>
                    <option value="dashed">Dashed</option>
                    <option value="double">Double</option>
                </select>
            </div>
            <div className="two-col">
                <SpinButtonInput
                    max={100}
                    min={0}
                    step={1}
                    value={borderWidth}
                    callBack={(v) => handleBorderWidth(v)}
                />
                <div className="color-picker-swatch" onClick={() => setPopup(true)}>
                    <div 
                        className="color-picker-color"
                        style={{
                            backgroundColor: borderColor
                        }}></div>
                </div>
                {popup && <div className="color-picker-popover">
                    <div className='color-picker-cover' onClick={() => setPopup(false)} />
                    <div className='color-picker-wrapper'>
                        <ChromePicker color={borderColor} onChange={(e) => handleBorderColor(e.hex)} disableAlpha />
                        <button className='color-picker-button' onClick={() => setPopup(false)}>Ok</button>
                    </div>
                </div>}
            </div>
        </React.Fragment>
    )
}
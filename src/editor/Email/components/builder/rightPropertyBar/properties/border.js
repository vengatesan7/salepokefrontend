import React, { useState, useContext, useEffect } from 'react';
import Switch from "react-switch";
import _get from "lodash.get";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { EmailStore } from '../../../../store';
import { BorderStyle } from './utils';

const Border = ({ blockType }) => {
    const {
        emailState,
        setEmailState,
        editable
    } = useContext(EmailStore);
    const [blockStyle, setblockStyle] = useState(null);

    // All Side Border
    const [allSideBorder, setAllSideBorder] = useState(false);

    useEffect(() => {
        switch (blockType) {
            case "CONTAINER":
                return setblockStyle(_get(emailState.entities.containers[editable.id], "style", {
                    borderTopWidth: 0,
                    borderRightWidth: 0,
                    borderBottomWidth: 0,
                    borderLeftWidth: 0
                }));
            case "STRUCTURE":
                return setblockStyle(_get(emailState.entities.structures[editable.id], "style", {
                    borderTopWidth: 0,
                    borderRightWidth: 0,
                    borderBottomWidth: 0,
                    borderLeftWidth: 0
                }));
            case "ELEMENT":
                return setblockStyle(_get(emailState.entities.element[editable.id], "style", {
                    borderTopWidth: 0,
                    borderRightWidth: 0,
                    borderBottomWidth: 0,
                    borderLeftWidth: 0
                }));
            default:
                break;
        }
    }, [editable]);

    useEffect(() => {
        // console.log(blockStyle);
        const newBlockStyle = {...blockStyle};
        if (newBlockStyle.style !== undefined) {
            if (newBlockStyle.style.borderTopWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderRightWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderBottomWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderLeftWidth !== undefined) {
                console.log(newBlockStyle);
            }
        }
    }, [blockStyle])

    const blockStyleUpdate = (block, name, value) => {
        switch (block) {
            case "CONTAINER":
                switch (name) {
                    case "borderAllSideStyle":
                        const newAllSideStyleContainer = {
                            ...emailState.entities.containers[editable.id].style,
                            borderTopStyle: value,
                            borderRightStyle: value,
                            borderBottomStyle: value,
                            borderLeftStyle: value
                        }
                        return newAllSideStyleContainer;
                    case "borderAllSideWidth":
                        const newAllSideWidthContainer = {
                            ...emailState.entities.containers[editable.id].style,
                            borderTopWidth: value,
                            borderRightWidth: value,
                            borderBottomWidth: value,
                            borderLeftWidth: value
                        }
                        return newAllSideWidthContainer;
                    case "borderAllSideColor":
                        const newAllSideColorContainer = {
                            ...emailState.entities.containers[editable.id].style,
                            borderTopColor: value,
                            borderRightColor: value,
                            borderBottomColor: value,
                            borderLeftColor: value
                        }
                        return newAllSideColorContainer;
                    default:
                        const newContainer = {
                            ...emailState.entities.containers[editable.id].style,
                            [name]: value
                        }
                        return newContainer;
                }
            case "STRUCTURE":
                switch (name) {
                    case "borderAllSideStyle":
                        const newAllSideStyleStructure = {
                            ...emailState.entities.structures[editable.id].style,
                            borderTopStyle: value,
                            borderRightStyle: value,
                            borderBottomStyle: value,
                            borderLeftStyle: value
                        }
                        return newAllSideStyleStructure;
                    case "borderAllSideWidth":
                        const newAllSideWidthStructure = {
                            ...emailState.entities.structures[editable.id].style,
                            borderTopWidth: value,
                            borderRightWidth: value,
                            borderBottomWidth: value,
                            borderLeftWidth: value
                        }
                        return newAllSideWidthStructure;
                    case "borderAllSideColor":
                        const newAllSideColorStructure = {
                            ...emailState.entities.structures[editable.id].style,
                            borderTopColor: value,
                            borderRightColor: value,
                            borderBottomColor: value,
                            borderLeftColor: value
                        }
                        return newAllSideColorStructure;
                    default:
                        const newStructure = {
                            ...emailState.entities.structures[editable.id].style,
                            [name]: value
                        }
                        return newStructure;
                }
            case "ELEMENT":
                switch (name) {
                    case "borderAllSideStyle":
                        const newAllSideStyleElement = {
                            ...emailState.entities.element[editable.id].style,
                            borderTopStyle: value,
                            borderRightStyle: value,
                            borderBottomStyle: value,
                            borderLeftStyle: value
                        }
                        return newAllSideStyleElement;
                    case "borderAllSideWidth":
                        const newAllSideWidthElement = {
                            ...emailState.entities.element[editable.id].style,
                            borderTopWidth: value,
                            borderRightWidth: value,
                            borderBottomWidth: value,
                            borderLeftWidth: value
                        }
                        return newAllSideWidthElement;
                    case "borderAllSideColor":
                        const newAllSideColorElement = {
                            ...emailState.entities.element[editable.id].style,
                            borderTopColor: value,
                            borderRightColor: value,
                            borderBottomColor: value,
                            borderLeftColor: value
                        }
                        return newAllSideColorElement;
                    default:
                        const newElement = {
                            ...emailState.entities.element[editable.id].style,
                            [name]: value
                        }
                        return newElement;
                }
            default:
                break;
        }
    }

    const handleStyle = (name, value) => {
        if (name === "borderAllSideStyle") {
            setblockStyle({
                ...blockStyle,
                borderTopStyle: value,
                borderRightStyle: value,
                borderBottomStyle: value,
                borderLeftStyle: value
            });
        } else if (name === "borderAllSideWidth") {
            setblockStyle({
                ...blockStyle,
                borderTopWidth: value,
                borderRightWidth: value,
                borderBottomWidth: value,
                borderLeftWidth: value
            });
        } else if (name === "borderAllSideColor") {
            setblockStyle({
                ...blockStyle,
                borderTopColor: value,
                borderRightColor: value,
                borderBottomColor: value,
                borderLeftColor: value
            });
        } else {
            setblockStyle({
                ...blockStyle,
                [name]: value
            });
        }
        switch (blockType) {
            case "CONTAINER":
                const containersStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        containers: {
                            ...emailState.entities.containers,
                            [editable.id]: {
                                ...emailState.entities.containers[editable.id],
                                style: blockStyleUpdate(blockType, name, value)
                            }
                        }
                    }
                }
                return setEmailState(containersStyleUpdate);
            case "STRUCTURE":
                const structuresStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        structures: {
                            ...emailState.entities.structures,
                            [editable.id]: {
                                ...emailState.entities.structures[editable.id],
                                style: blockStyleUpdate(blockType, name, value)
                            }
                        }
                    }
                }
                return setEmailState(structuresStyleUpdate);
            case "ELEMENT":
                const elementStyleUpdate = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        element: {
                            ...emailState.entities.element,
                            [editable.id]: {
                                ...emailState.entities.element[editable.id],
                                style: blockStyleUpdate(blockType, name, value)
                            }
                        }
                    }
                }
                return setEmailState(elementStyleUpdate);
            default:
                break;
        }
    }

    return (
        blockStyle !== null && <div className="properties-group">
            <div className="properties-group-name">
                BORDER PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title">Border</div>
                    <div className="properties-value">
                        <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                        <Switch checked={allSideBorder} onChange={(e) => setAllSideBorder(e)}
                            onColor="#43da71" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>
                {!allSideBorder ? <div className="properties-field two-col properties-field-email-editor-border">
                    <div className="properties-title" style={{lineHeight: "34px"}}>All Sides</div>
                    <div className="properties-value">
                        <BorderStyle
                            borderStyle={blockStyle.borderTopStyle}
                            borderWidth={blockStyle.borderTopWidth}
                            borderColor={blockStyle.borderTopColor}
                            handleBorderStyle={(e) => handleStyle('borderAllSideStyle', e)}
                            handleBorderWidth={(e) => handleStyle('borderAllSideWidth', e)}
                            handleBorderColor={(e) => handleStyle('borderAllSideColor', e)}
                        />
                    </div>
                </div> :
                <div className="properties-field two-col properties-field-email-editor-border">
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Top</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderTopStyle}
                                    borderWidth={blockStyle.borderTopWidth}
                                    borderColor={blockStyle.borderTopColor}
                                    handleBorderStyle={(e) => handleStyle('borderTopStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderTopWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderTopColor', e)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Bottom</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderBottomStyle}
                                    borderWidth={blockStyle.borderBottomWidth}
                                    borderColor={blockStyle.borderBottomColor}
                                    handleBorderStyle={(e) => handleStyle('borderBottomStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderBottomWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderBottomColor', e)}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Right</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderRightStyle}
                                    borderWidth={blockStyle.borderRightWidth}
                                    borderColor={blockStyle.borderRightColor}
                                    handleBorderStyle={(e) => handleStyle('borderRightStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderRightWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderRightColor', e)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Left</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderLeftStyle}
                                    borderWidth={blockStyle.borderLeftWidth}
                                    borderColor={blockStyle.borderLeftColor}
                                    handleBorderStyle={(e) => handleStyle('borderLeftStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderLeftWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderLeftColor', e)}
                                />
                            </div>
                        </div>
                    </div>
                </div>}
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Radius</div>
                    <div className="properties-value">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={blockStyle.borderRadius}
                            callBack={(v) => handleStyle("borderRadius", v)}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Border;
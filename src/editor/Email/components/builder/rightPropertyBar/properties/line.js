import React, { useState, useEffect, useContext } from 'react';

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { EmailStore } from '../../../../store';
import { ChromePicker } from 'react-color';

const Line = () => {
    const {
        emailState,
        setEmailState,
        editable
    } = useContext(EmailStore);

    const [blockData, setBlockData] = useState(null);
    const [colorPopup, setColorPopup] = useState(false)

    useEffect(() => {
        setBlockData(emailState.entities.element[editable.id]);
    }, [editable]);

    const handleStyleUpdate = (name, value) => {
        setColorPopup(false);
        setBlockData({
            ...blockData,
            style: {
                ...blockData.style,
                [name]: value
            }
        })
        const elementStyleUpdate = {
            ...emailState,
            entities: {
                ...emailState.entities,
                element: {
                    ...emailState.entities.element,
                    [editable.id]: {
                        ...emailState.entities.element[editable.id],
                        style: {
                            ...emailState.entities.element[editable.id].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setEmailState(elementStyleUpdate);
    }

    return (
        blockData !== null && <div className="properties-group">
            <div className="properties-group-name">
                SPACE PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Line Size</div>
                    <div className="properties-value padding">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={blockData.style.borderWidth}
                            callBack={(v) => handleStyleUpdate("borderWidth", v)}
                        />
                    </div>
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title">Color</div>
                    <div className="properties-value" style={{height: "22px"}}>
                        <div className="color-picker-none" onClick={(e) => handleStyleUpdate("borderColor", "")}><i className="material-icons">not_interested</i></div>
                        <div className="color-picker-swatch" onClick={() => setColorPopup(true)}>
                            <div 
                                className="color-picker-color"
                                style={{
                                    backgroundColor: blockData.style.borderColor
                                }}></div>
                        </div>
                        {colorPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => handleStyleUpdate("borderColor", blockData.style.borderColor)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={blockData.style.borderColor} onChange={(e) => setBlockData({...blockData, style: { ...blockData.style, borderColor: e.hex}})} disableAlpha />
                                <button className='color-picker-button' onClick={() => handleStyleUpdate("borderColor", blockData.style.borderColor)}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Line;
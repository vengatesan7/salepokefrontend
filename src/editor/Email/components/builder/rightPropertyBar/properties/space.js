import React, { useState, useEffect, useContext } from 'react';

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { EmailStore } from '../../../../store';

const Space = () => {
    const {
        emailState,
        setEmailState,
        editable
    } = useContext(EmailStore);

    const [blockData, setBlockData] = useState(null);

    useEffect(() => {
        setBlockData(emailState.entities.element[editable.id]);
    }, [editable]);

    const handleStyleUpdate = (name, value) => {
        setBlockData({
            ...blockData,
            style: {
                ...blockData.style,
                [name]: value
            }
        })
        const elementStyleUpdate = {
            ...emailState,
            entities: {
                ...emailState.entities,
                element: {
                    ...emailState.entities.element,
                    [editable.id]: {
                        ...emailState.entities.element[editable.id],
                        style: {
                            ...emailState.entities.element[editable.id].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setEmailState(elementStyleUpdate);
    }

    return (
        blockData !== null && <div className="properties-group">
            <div className="properties-group-name">
                SPACE PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Height</div>
                    <div className="properties-value padding">
                        <SpinButtonInput
                            max={1000}
                            min={0}
                            step={5}
                            value={blockData.style.height}
                            callBack={(v) => handleStyleUpdate("height", v)}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Space;
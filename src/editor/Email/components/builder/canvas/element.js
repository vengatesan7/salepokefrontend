import React, { useContext, useEffect, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from "uuidv4";
import cookie from 'react-cookies';

import TextElement from './elements/text';
import ImageElement from './elements/image';
import { EmailStore } from '../../../store';
import ButtonElement from './elements/button';
import VideoElement from './elements/video';
import SpaceElement from './elements/space';
import LineElement from './elements/line';

const Element = ({ elementId, elementType, style, isSelected, isDragging }) => {
    const { emailState } = useContext(EmailStore);

    const elementDomValue = (type) => {
        switch (type) {
            case "TEXT":
                return <TextElement elementId={elementId} />;
            case "IMAGE":
                return <ImageElement elementId={elementId} />;
            case "BUTTON":
                return <ButtonElement elementId={elementId} />;
            case "VIDEO":
                return <VideoElement elementId={elementId} />;
            case "SPACE":
                return <SpaceElement elementId={elementId} />;
            case "LINE":
                return <LineElement elementId={elementId} />;
            default:
                break;
        }
    }

    const thisStyle = () => {
        switch (elementType) {
            case "TEXT":
                return style;
            case "IMAGE":
                const imageStyle = {...style, width: "auto"}
                return imageStyle;
            case "BUTTON":
                const buttonStyle = _get(emailState.entities.element[elementId], "buttonStyle", {textAlign: "center"});
                return {...style, textAlign: buttonStyle.textAlign};
            default:
                break;
        }
    }

    return (
        <div style={isDragging ? {...thisStyle(), display: "none"} : thisStyle()} className={classnames(isSelected && "is-selected")}>
            {elementDomValue(elementType)}
        </div>
    );
}

const CanvasElement = ({ containerId, parentColumnId }) => {
    const {
        emailState,
        setEmailState,
        setSelectedElement,
        selectedElement,
        mouseInPosition,
        setMouseInPosition,
        parentIDs,
        setParentIDs,
        isDndRuning,
        dndDragType,
        setDropPosition,
        setDeleteAction,
        dndOnDrag,
        setDndOnDrag,
        setEditable
    } = useContext(EmailStore);

    const elementsArray = emailState.entities.containers[containerId].elements;

    const handleDuplicate = (id, index) => {
        const newContainerArr = [...emailState.entities.containers[containerId].elements];
        const newElementId = uuid();
        newContainerArr.splice(index + 1, 0, newElementId);
        const duplicatedData = {
            ...emailState,
            entities: {
                ...emailState.entities,
                containers: {
                    ...emailState.entities.containers,
                    [containerId]: {
                        ...emailState.entities.containers[containerId],
                        elements: newContainerArr
                    }
                },
                element: {
                    ...emailState.entities.element,
                    [newElementId]: {
                        ...emailState.entities.element[id],
                        id: newElementId
                    }
                }
            }
        }
        return setEmailState(duplicatedData);
    }

    const handleSelectElement = (id, index, parentId) => {
        setSelectedElement(id);
        setEditable({
            enable: true,
            type: "ELEMENT",
            id: id,
            index: index,
            parentId: parentId
        })
        cookie.save("editorStatus", true);
    }
    
    return (
        <table width="100%">
            <Droppable droppableId={containerId} type="ELEMENT">
                {(containerProvider, containerSnapshot) => (
                    <tbody
                        className={classnames("email-element-wapper", containerSnapshot.isDraggingOver && "on-dragging")}
                        {...containerProvider.droppableProps}
                        ref={containerProvider.innerRef}
                        isDraggingOver={containerSnapshot.isDraggingOver}
                    >
                        {elementsArray.length !== 0 ? elementsArray.map((elementId, elementIndex) => {
                            const elementStyle = _get(emailState.entities.element[elementId], "style", null);
                            return (
                                <Draggable draggableId={elementId} index={elementIndex}>
                                        {(elementDragProvided, elementDragSnapshot) => (
                                        <React.Fragment>
                                            <tr
                                                className={
                                                    classnames(
                                                        "email-element",
                                                        elementDragSnapshot.isDragging && "isDragging",
                                                        !isDndRuning && mouseInPosition === "element" && "isHovered"
                                                    )}
                                                {...elementDragProvided.draggableProps}
                                                ref={elementDragProvided.innerRef}
                                            >
                                                <td
                                                    className={classnames("email-element-wrapper", elementDragSnapshot.isDragging && "isDragging")}
                                                    onClick={() => handleSelectElement(elementId, elementIndex, containerId)}
                                                    onMouseEnter={() => setMouseInPosition("element")}
                                                    onMouseLeave={() => setMouseInPosition("column")}
                                                >
                                                    {dndDragType === "ELEMENT" && <div
                                                        className={classnames("drop-it-here", "top")}
                                                        onMouseUp={() => setDndOnDrag({
                                                            ...dndOnDrag,
                                                            dropPosition: "top",
                                                            droppedId: elementId,
                                                            droppedIndex: elementIndex,
                                                            parentDropId: containerId
                                                        })}
                                                    >
                                                        <div className="drop-it-here-border">
                                                            <span>Drop it Here</span>
                                                        </div>
                                                    </div>}
                                                    <div className={classnames("element-option", selectedElement === elementId && "hidden")}>
                                                        <span
                                                            className="move"
                                                            {...elementDragProvided.dragHandleProps}
                                                            onMouseDownCapture ={() => setParentIDs({...parentIDs, column: parentColumnId})}
                                                        >
                                                            <i className="material-icons">control_camera</i>
                                                        </span>
                                                        {/* <span
                                                            className="edit"
                                                        >
                                                            <i className="material-icons">edit</i>
                                                        </span> */}
                                                    </div>
                                                    <Element
                                                        elementId={elementId}
                                                        elementType={emailState.entities.element[elementId].type}
                                                        style={elementStyle}
                                                        isSelected={selectedElement === elementId}
                                                        isDragging={elementDragSnapshot.isDragging}
                                                    />
                                                    {selectedElement === elementId && <div className="element-edit-option">
                                                        <div 
                                                            className="button"
                                                            onClick={() => setDeleteAction({
                                                                enable: true,
                                                                type: "ELEMENT",
                                                                id: elementId,
                                                                index: elementIndex,
                                                                parentIDs: containerId
                                                            })}
                                                        >
                                                            <i className="material-icons">delete</i>
                                                        </div>
                                                        <div
                                                            className="button"
                                                            onClick={() => handleDuplicate(elementId, elementIndex)}
                                                        >
                                                            <i className="material-icons">content_copy</i>
                                                        </div>
                                                    </div>}
                                                    {dndDragType === "ELEMENT" && <div
                                                        className={classnames("drop-it-here", "bottom")}
                                                        onMouseUp={() => setDndOnDrag({
                                                            ...dndOnDrag,
                                                            dropPosition: "bottom",
                                                            droppedId: elementId,
                                                            droppedIndex: elementIndex,
                                                            parentDropId: containerId
                                                        })}
                                                    >
                                                        <div className="drop-it-here-border">
                                                            <span>Drop it Here</span>
                                                        </div>
                                                    </div>}
                                                </td>
                                            </tr>
                                            {elementDragSnapshot.isDragging && <tr className={classnames("email-element")}>
                                                <td
                                                    style={{
                                                        backgroundColor: "white",
                                                    }}
                                                    className="email-element-wrapper"
                                                >
                                                    {/* <div className="element-option">
                                                        <span
                                                            className="move"
                                                            {...elementDragProvided.dragHandleProps}
                                                        >
                                                            <i className="material-icons">control_camera</i>
                                                        </span>
                                                    </div> */}
                                                    {console.log(elementStyle)}
                                                    <Element
                                                        elementId={elementId}
                                                        elementType={emailState.entities.element[elementId].type}
                                                        style={elementStyle}
                                                        isSelected={selectedElement === elementId}
                                                    />
                                                </td>
                                            </tr>}
                                        </React.Fragment>
                                        )}
                                </Draggable>
                            )
                        }) : <div style={{position: "relative"}}>
                                {dndDragType === "ELEMENT" && <div
                                    className={classnames("drop-it-here", "emptyElementArr")}
                                    onMouseUp={() => setDndOnDrag({
                                        ...dndOnDrag,
                                        dropPosition: "top",
                                        droppedId: "thisIsEmptyElementArray",
                                        droppedIndex: 0,
                                        parentDropId: containerId
                                    })}
                                >
                                    <div className="drop-it-here-border">
                                        <span>Drop it Here</span>
                                    </div>
                                </div>}
                                <div style={{ 
                                    textAlign: "center", 
                                    padding: "15px",
                                    border: "1px dashed gray",
                                    borderRadius: "4px",
                                    background: "white",
                                    fontSize: "12px"
                                }}>Drop new widget here...</div>
                            </div>}
                    </tbody>
                )}
            </Droppable>
        </table>
    );
}

export default CanvasElement;
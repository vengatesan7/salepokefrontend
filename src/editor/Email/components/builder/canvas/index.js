import React, { useContext, useEffect, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from "uuidv4";

import './canvas.scss';
import CanvasColumn from './column';
import PropertyBar from '../propertybar';

import { EmailStore } from '../../../store';
import EmailPreview from '../../preview';

const EmailEditorCanvas = () => {

    const {
        emailState,
        setEmailState,
        mouseInPosition,
        setMouseInPosition,
        isDndRuning,
        setDeleteAction,
        editable,
        setEditable
    } = useContext(EmailStore);
    
    const [newSectionPopup, setNewSectionPopup] = useState({
        enable: false,
        id: null
    });

    const [sectionOptionEnable, setSectionOptionEnable] = useState(false);

    const addNewSection = (columnType) => {
        const newSectionArr = [...emailState.result.sections];
        const prevSecPosition = newSectionArr.indexOf(newSectionPopup.id);
        const newSectionId = uuid();
        newSectionArr.splice(prevSecPosition + 1, 0, newSectionId)
        const newStructureId = uuid();
        const firstColumnId = uuid();
        const firstContainerId = uuid();
        const secoundColumnId = uuid();
        const secoundContainerId = uuid();
        const thirdColumnId = uuid();
        const thirdContainerId = uuid();
        const fourthColumnId = uuid();
        const fourthContainerId = uuid();
        setNewSectionPopup({
            enable: false,
            id: null
        })
        switch (columnType) {
            case "esc_100":
                const esc100 = {
                    ...emailState,
                    result: {
                        ...emailState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...emailState.entities,
                        section: {
                            ...emailState.entities.section,
                            [newSectionId]: {
                                ...emailState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...emailState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId]
                            }
                        },
                        columns: {
                            ...emailState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 560,
                                containers: [firstContainerId]
                            }
                        },
                        containers: {
                            ...emailState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setEmailState(esc100);
            case "esc_50_50":
                const esc5050 = {
                    ...emailState,
                    result: {
                        ...emailState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...emailState.entities,
                        section: {
                            ...emailState.entities.section,
                            [newSectionId]: {
                                ...emailState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...emailState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId, secoundColumnId]
                            }
                        },
                        columns: {
                            ...emailState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 270,
                                containers: [firstContainerId]
                            },
                            [secoundColumnId]: {
                                id: secoundColumnId,
                                width: 270,
                                containers: [secoundContainerId]
                            }
                        },
                        containers: {
                            ...emailState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            },
                            [secoundContainerId]: {
                                id: secoundContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setEmailState(esc5050);
            case "esc_33_33_33":
                const esc333333 = {
                    ...emailState,
                    result: {
                        ...emailState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...emailState.entities,
                        section: {
                            ...emailState.entities.section,
                            [newSectionId]: {
                                ...emailState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...emailState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId, secoundColumnId, thirdColumnId]
                            }
                        },
                        columns: {
                            ...emailState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 167,
                                containers: [firstContainerId]
                            },
                            [secoundColumnId]: {
                                id: secoundColumnId,
                                width: 167,
                                containers: [secoundContainerId]
                            },
                            [thirdColumnId]: {
                                id: thirdColumnId,
                                width: 167,
                                containers: [thirdContainerId]
                            }
                        },
                        containers: {
                            ...emailState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            },
                            [secoundContainerId]: {
                                id: secoundContainerId,
                                elements: []
                            },
                            [thirdContainerId]: {
                                id: thirdContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setEmailState(esc333333);
            default:
                break;
        }
    }

    return (
        <div className="main-content">
            <table className="email-container">
                <tbody>
                    <tr>
                        <td>
                            <Droppable droppableId="sectionDrop" type="SECTION">
                                {(sectionProvider, sectionSnapshot) => (
                                    <table
                                        className="email-content"
                                        width="100%"
                                        {...sectionProvider.droppableProps}
                                        ref={sectionProvider.innerRef}
                                        isDraggingOver={sectionSnapshot.isDraggingOver}
                                    >
                                        <tbody>
                                            {emailState.result.sections.map((sectionId, sectionIndex) => {
                                                const sectionStyleData = _get(emailState.entities.section[sectionId], "style", {});

                                                const sectionStyle = {
                                                  ...sectionStyleData,
                                                  backgroundImage: sectionStyleData.backgroundImage !== undefined ? `url(${sectionStyleData.backgroundImage})`: ""
                                                }
                                                return (
                                                    <Draggable draggableId={sectionId} index={sectionIndex}>
                                                        {(sectionDragProvided, sectionSnapshot) => (
                                                            <tr
                                                                className={
                                                                    classnames(
                                                                        "email-section",
                                                                        sectionSnapshot.isDragging && "isDragging",
                                                                        !isDndRuning && mouseInPosition ==="section" && "isHovered",
                                                                        editable.id === sectionId && "section-selected"
                                                                    )}
                                                                {...sectionDragProvided.draggableProps}
                                                                ref={sectionDragProvided.innerRef}
                                                                onClick={() => mouseInPosition === "section" && setEditable({
                                                                    enable: true,
                                                                    type: "SECTION",
                                                                    id: sectionId,
                                                                    index: sectionIndex,
                                                                    parentId: null
                                                                })}
                                                            >
                                                                <td
                                                                    className="email-section-wrapper"
                                                                    style={sectionStyle}
                                                                    onMouseEnter={() => setMouseInPosition("section")}
                                                                >
                                                                    <div
                                                                        className={classnames("email-section-option", sectionOptionEnable && "open")}
                                                                        onMouseEnter={() => setSectionOptionEnable(true)}
                                                                        onMouseLeave={() => setSectionOptionEnable(false)}
                                                                    >
                                                                        <span className="arrow">
                                                                            <i className="material-icons">keyboard_arrow_right</i>
                                                                        </span>
                                                                        <span
                                                                            className="move"
                                                                            {...sectionDragProvided.dragHandleProps}
                                                                        >
                                                                            <i className="material-icons">control_camera</i>
                                                                        </span>
                                                                        <span
                                                                            className="delete"
                                                                            onClick={() => setDeleteAction({
                                                                                enable: true,
                                                                                type: "SECTION",
                                                                                id: sectionId,
                                                                                index: sectionIndex,
                                                                                parentIDs: null
                                                                            })}
                                                                        >
                                                                            <i className="material-icons">delete</i>
                                                                        </span>
                                                                    </div>
                                                                    {/* {!sectionSnapshot.isDragging && <React.Fragment> */}
                                                                        <Droppable droppableId={sectionId} type="STRUCTURE">
                                                                            {(structureProvider, structureSnapshot) => (
                                                                                <table
                                                                                    width="600"
                                                                                    className={classnames("email-structure-wrapper", sectionSnapshot.isDragging && "isDragging")}
                                                                                    {...structureProvider.droppableProps}
                                                                                    ref={structureProvider.innerRef}
                                                                                    isDraggingOver={structureSnapshot.isDraggingOver}
                                                                                    style={{ margin: "0 auto" }}
                                                                                >
                                                                                    <CanvasColumn sectionId={sectionId} />
                                                                                </table>
                                                                            )}
                                                                        </Droppable>
                                                                        <div className={classnames("section-option", newSectionPopup.enable && newSectionPopup.id === sectionId && "isClicked")}>
                                                                            <span
                                                                                className="add-section-option"
                                                                                onClick={() => setNewSectionPopup({
                                                                                    enable: !newSectionPopup.enable,
                                                                                    id: sectionId
                                                                                })}
                                                                            >
                                                                                <i className="material-icons">add_circle_outline</i>
                                                                            </span>
                                                                            {newSectionPopup.enable && newSectionPopup.id === sectionId && <div className="add-new-section-popup">
                                                                                <div
                                                                                    className="add-new-section-popup-wrapper"
                                                                                    onMouseDown={() => addNewSection("esc_100")}
                                                                                >
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    className="add-new-section-popup-wrapper"
                                                                                    onMouseDown={() => addNewSection("esc_50_50")}
                                                                                >
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    className="add-new-section-popup-wrapper"
                                                                                    onMouseDown={() => addNewSection("esc_33_33_33")}
                                                                                >
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                    <div className="add-new-section-popup-column">
                                                                                        <span></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>}
                                                                        </div>
                                                                    {/* </React.Fragment>} */}
                                                                </td>
                                                            </tr>
                                                        )}
                                                    </Draggable>
                                                );
                                            })}
                                        </tbody>
                                    </table>
                                )}
                            </Droppable>
                        </td>
                    </tr>
                </tbody>
            </table>
            {/* <EmailPreview /> */}
            {newSectionPopup.enable && <div 
                    style={{
                        position: "fixed",
                        top: "0",
                        width: "100vw",
                        height: "100vh",
                        background: "transparent"
                    }}
                    onMouseDown={() => setNewSectionPopup({
                        enable: false,
                        id: null
                    })}
                >
            </div>}
        </div>
    );
}

export default EmailEditorCanvas;
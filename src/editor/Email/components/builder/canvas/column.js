import React, { useContext, useEffect, useState } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";

import CanvasElement from "./element";
import { EmailStore } from "../../../store";

const CanvasColumn = ({ sectionId }) => {
  const { 
    emailState,
    setEmailState,
    mouseInPosition,
    setMouseInPosition,
    isDndRuning,
    dndDragType,
    setDropPosition,
    
    dndOnDrag,
    setDndOnDrag,
    editable,
    setEditable
  } = useContext(EmailStore);

  return (<tbody>
    {emailState.entities.section[sectionId].structures.map((structureId, structureIndex) => {
      const structureStyleData = _get(emailState.entities.structures[structureId], "style", {});

      const structureStyle = {
        ...structureStyleData,
        backgroundImage: structureStyleData.backgroundImage !== undefined ? `url(${structureStyleData.backgroundImage})`: ""
      }
      return (<Draggable draggableId={structureId} index={structureIndex}>
        {(structureDragProvided, structureDragSnapshot) => (
          <tr
            className={
              classnames(
                "email-structure",
                structureDragSnapshot.isDragging && "isDragging",
                !isDndRuning && mouseInPosition === "structure" && "isHovered",
                editable.id === structureId && "stracture-selected"
              )}
            onClick={() => mouseInPosition === "structure" && setEditable({
              enable: true,
              type: "STRUCTURE",
              id: structureId,
              index: structureIndex,
              parentId: sectionId
            })}
            {...structureDragProvided.draggableProps}
            ref={structureDragProvided.innerRef}
          >
            <td
              style={{...structureStyle, padding: "5px 20px"}}
              onMouseEnter={() => setMouseInPosition("structure")}
              onMouseLeave={() => setMouseInPosition("section")}
            >
              {dndDragType === "STRUCTURE" && <div 
                className={classnames("drop-it-here", "top")}
                onMouseUp={() => setDndOnDrag({
                  ...dndOnDrag,
                  dropPosition: "top",
                  droppedId: structureId,
                  droppedIndex: structureIndex,
                  parentDropId: sectionId
                })}
              >
                <div className="drop-it-here-border">
                    <span>Drop it Here</span>
                </div>
              </div>}
              <span
                className="structure-option move"
                {...structureDragProvided.dragHandleProps}
              >
                <i className="material-icons">control_camera</i>
              </span>
              <table width="100%">
                <tbody>
                  <tr className="email-structure-wrapper">
                    {emailState.entities.structures[structureId].columns.map((columnId, columnIndex) => {
                      const columnsCount = emailState.entities.structures[structureId].columns.length;
                        return (<td
                          style={{
                            verticalAlign: "top"
                          }}
                          align={columnsCount - 1 === columnIndex ? "right" : "left"}
                        >
                          <table width={emailState.entities.columns[columnId].width}>
                            <Droppable droppableId={columnId} type="CONTAINER">
                              {(columnProvider, columnSnapshot) => (
                                <tbody
                                  {...columnProvider.droppableProps}
                                  ref={columnProvider.innerRef}
                                  isDraggingOver={columnSnapshot.isDraggingOver}
                                  className={classnames(emailState.entities.columns[columnId].containers.length === 0 && "empty-container")}
                                >
                                  {emailState.entities.columns[columnId].containers.map((containerId, containerIndex) => {
                                    const elementCount = emailState.entities.containers[containerId].elements.length;
                                    const containerStyleData = _get(emailState.entities.containers[containerId], "style", {});
                                    const containerStyle = {
                                      ...containerStyleData,
                                      backgroundImage: containerStyleData.backgroundImage !== undefined ? `url(${containerStyleData.backgroundImage})`: ""
                                    }
                                      return (
                                        <Draggable draggableId={containerId} index={containerIndex}>
                                          {(containerDragProvided, containerDragSnapshot) => (
                                            <React.Fragment>
                                              <tr
                                                className={
                                                  classnames("email-container",
                                                  containerDragSnapshot.isDragging && "isDragging",
                                                  !isDndRuning && mouseInPosition === "column" && "isHovered",
                                                  editable.id === containerId && "container-selected"
                                                )}
                                                {...containerDragProvided.draggableProps}
                                                ref={containerDragProvided.innerRef}
                                                onClick={() => mouseInPosition === "column" && setEditable({
                                                  enable: true,
                                                  type: "CONTAINER",
                                                  id: containerId,
                                                  index: containerIndex,
                                                  parentId: columnId
                                                })}
                                              >
                                                <td
                                                  className={classnames("email-container-wrapper", containerDragSnapshot.isDragging && "isDragging")}
                                                  style={{
                                                    ...containerStyle,
                                                    padding: "5px"
                                                  }}
                                                  onMouseEnter={() => setMouseInPosition("column")}
                                                  onMouseLeave={() => setMouseInPosition("structure")}
                                                >
                                                  {dndDragType === "CONTAINER" && <div 
                                                    className={classnames("drop-it-here", "top")}
                                                    onMouseUp={() => setDndOnDrag({
                                                      ...dndOnDrag,
                                                      dropPosition: "top",
                                                      droppedId: containerId,
                                                      droppedIndex: containerIndex,
                                                      parentDropId: columnId
                                                    })}
                                                  >
                                                    <div className="drop-it-here-border">
                                                        <span>Drop it Here</span>
                                                    </div>
                                                  </div>}
                                                  <span
                                                    className={classnames("column-option move", elementCount === 0 && "hidden")}
                                                    {...containerDragProvided.dragHandleProps}
                                                  >
                                                      <i className="material-icons">control_camera</i>
                                                  </span>
                                                  <CanvasElement containerId={containerId} parentColumnId={columnId}/>
                                                  {dndDragType === "CONTAINER" && <div
                                                    className={classnames("drop-it-here", "bottom")}
                                                    onMouseUp={() => setDndOnDrag({
                                                      ...dndOnDrag,
                                                      dropPosition: "bottom",
                                                      droppedId: containerId,
                                                      droppedIndex: containerIndex,
                                                      parentDropId: columnId
                                                    })}
                                                  >
                                                    <div className="drop-it-here-border">
                                                        <span>Drop it Here</span>
                                                    </div>
                                                  </div>}
                                                </td>
                                              </tr>
                                            </React.Fragment>
                                          )}
                                        </Draggable>
                                      );
                                  })}
                                </tbody>
                              )}
                            </Droppable>
                          </table>
                        </td>)
                      })
                    }
                  </tr>
                </tbody>
              </table>
              {dndDragType === "STRUCTURE" && <div 
                className={classnames("drop-it-here", "bottom")}
                onMouseUp={() => setDndOnDrag({
                  ...dndOnDrag,
                  dropPosition: "bottom",
                  droppedId: structureId,
                  droppedIndex: structureIndex,
                  parentDropId: sectionId
                })}
              >
                <div className="drop-it-here-border">
                    <span>Drop it Here</span>
                </div>
              </div>}
            </td>
          </tr>
        )}
      </Draggable>)
    })}
  </tbody>)
}

export default CanvasColumn;

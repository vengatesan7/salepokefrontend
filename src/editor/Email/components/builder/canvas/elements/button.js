import React, { useState, useEffect, useContext } from 'react';
import ContentEditable from "react-contenteditable";
import sanitizeHtml from "sanitize-html";
import _get from "lodash.get";

import { EmailStore } from '../../../../store';
import EditorConvertToHTML from '../../../../../components/wysiwygText';

const ButtonElement = ({ elementId, tagName = "span" }) => {
    const {
        emailState,
        setEmailState,
        fontStyle,
        setFontStyle,
        selectedElement
    } = useContext(EmailStore);
    const [elementData, setElementData] = useState(emailState.entities.element[elementId]);

    // const sanitizeConf = {
    //     allowedTags: ["b", "i", "em", "strong", "a", "p", "h1","u", "strike", "sup", "sub", "div", "ul", "ol", "li", "font", "br", "span"],
    //     allowedAttributes: { a: ["href"], font: ["color", "size", "face"], span: ["style"] }
    // };

    useEffect(() => {
        setElementData(emailState.entities.element[elementId]);
    }, [emailState]);

    const handleUpdateState = (value) => {
        setElementData({
            ...elementData,
            content: value
        });
        setEmailState({
            ...emailState,
            entities: {
                ...emailState.entities,
                element: {
                    ...emailState.entities.element,
                    [elementId]: {
                        ...emailState.entities.element[elementId],
                        content: value
                    }
                }
            }
        });
    }

    // const handleChange = (value) => {
    //     setElementData({
    //         ...elementData,
    //         content: value
    //     });
    // }

    // const findStyle = () => {
    //     setFontStyle({
    //         ...fontStyle,
    //         bold: document.queryCommandState ("bold"),
    //         italic: document.queryCommandState ("italic"),
    //         underline: document.queryCommandState ("underline")
    //     })
    // }

    return (
        <span dangerouslySetInnerHTML={{__html: elementData.content}} style={elementData && elementData.buttonStyle} />
        // {/* <span onBlur={() => handleUpdateState()}>
        //     <ContentEditable
        //         html={elementData && elementData.content}
        //         style={elementData && elementData.buttonStyle}
        //         disabled={selectedElement === null || selectedElement !== elementId}
        //         tagName={tagName}
        //         onChange={e => setElementData({
        //             ...elementData,
        //             content: e.target.value
        //         })}
        //         onClick={e => findStyle()}
        //         onKeyDown={e => findStyle()}
        //     />
        // </span> */}
    )
}

export default ButtonElement;
import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { EmailStore } from '../../../../store';

const ImageElement = ({ state, elementId }) => {
    const { emailState } = useContext(EmailStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(emailState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(emailState.entities.element[elementId]);
    }, [emailState]);
    return (
        elementData !== null && <React.Fragment>
            {elementData.url === null ?
                <div className="initial-sample-image">
                    <img src="https://i-love-png.com/images/img_234957_3642.png" width="50" />
                </div>
                :
                <img
                    src={emailState.entities.element[elementId].url}
                    style={{ width: `${elementData.style.width}%` }}
                    alt={emailState.entities.element[elementId].alt}
                />}
        </React.Fragment>
    )
}

export default ImageElement;
import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { EmailStore } from '../../../../store';

const LineElement = ({ state, elementId }) => {
    const { emailState } = useContext(EmailStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(emailState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(emailState.entities.element[elementId]);
    }, [emailState]);
    return (
        elementData !== null && <hr style={ elementData.style } />
    )
}

export default LineElement;
import React, { useContext } from 'react';
import { DragDropContext } from "react-beautiful-dnd";
import uuid from "uuidv4";

import './builder.scss';
import EmailEditorCanvas from './canvas';
import EmailEditorMenuBar from './menubar';
import { EmailStore } from '../../store';
import Popup from "../../../../Components/Base/popup";
import {
    newSingleColumnStructure
} from "../../utils/emailDnd";
import PropertyBar from './propertybar';
import AllProperties from './rightPropertyBar';

const EmailEditorBuilder = () => {
    const {
        selectedElement,
        setSelectedElement,
        setIsNewElementDrag,
        emailState,
        setEmailState,
        parentIDs,
        setParentIDs,
        isDndRuning,
        setIsDndRuning,
        dndDragType,
        setDndDragType,
        dropPosition,
        editable,
        dndOnDrag,
        setDndOnDrag,
        deleteAction,
        setDeleteAction,
        setEditable
    } = useContext(EmailStore);
    const newElementJSON = (type, elementId) => {
        switch (type) {
            case "Text":
                const newText = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "TEXT",
                        content: "Text",
                        style: {
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15
                        }
                    }
                }
                return newText;
            case "Image":
                const newImage = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "IMAGE",
                        url: "http://www.wathannfilmfestival.com/modules/flexi/images/no-image.jpg",
                        alt: "Image",
                        style: {
                            width: 100,
                            textAlign: "center",
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0,
                            paddingLeft: 0
                        }
                    }
                }
                return newImage;
            case "Button":
                const newButton = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "BUTTON",
                        content: "Button Text",
                        buttonStyle: {
                            backgroundColor: "#3aaee0",
                            color: "#ffffff",
                            fontSize: 12,
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15,
                            display: "inline-block",
                            textAlign: "center"
                        },
                        style: {
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0,
                            paddingLeft: 0
                        }
                    }
                }
                return newButton;
            case "Video": 
                const newVideo = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "VIDEO",
                        embed: "",
                        url: "https://youtu.be/Bg_tJvCA8zw",
                        // url: "https://www.youtube.com/watch?v=Bg_tJvCA8zw&feature=",
                        style: {
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0,
                            paddingLeft: 0
                        }
                    }
                }
                return newVideo;
            case "Space": 
                const newSpace = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "SPACE",
                        style: {
                            height: 100,
                            borderRadius: 0
                        }
                    }
                }
                return newSpace;
            case "Line": 
                const newLine = {
                    ...emailState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "LINE",
                        style: {
                            borderStyle: "solid",
                            borderRadius: 0,
                            borderWidth: 1,
                            borderColor: "black",
                        }
                    }
                }
                return newLine;
            default:
                break;
        }
    }
    const popupOnDragEnd = (result) => {
        const { draggableId, destination, source, type } = result;
        // console.log(draggableId, destination, source, type, dndOnDrag);
        setIsDndRuning(false);
        setDndDragType(null);
        setDndOnDrag({
            type: null,
            dropPosition: null,
            droppedId: null
        });
        if (destination === null || dndOnDrag.droppedId === null) {
            return;
        }
        switch (type) {
            case "SECTION":
                const newSectionArr = [...emailState.result.sections];
                newSectionArr.splice(source.index, 1);
                newSectionArr.splice(destination.index, 0, draggableId);
                const structureInSameArray = {
                    ...emailState,
                    result: {
                        ...emailState.result,
                        sections: newSectionArr
                    }
                }
                return setEmailState(structureInSameArray);
            case "CONTAINER":
                const newContainerArr = [...emailState.entities.columns[dndOnDrag.parentDropId].containers];
                if (dndOnDrag.parentDropId === source.droppableId) {
                    newContainerArr.splice(source.index, 1);
                    const newDroppedId = newContainerArr.indexOf(dndOnDrag.droppedId);
                    newContainerArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const containerSwapSameColumn = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            columns: {
                                ...emailState.entities.columns,
                                [dndOnDrag.parentDropId]: {
                                    ...emailState.entities.columns[dndOnDrag.parentDropId],
                                    containers: newContainerArr
                                }
                            }
                        }
                    }
                    return setEmailState(containerSwapSameColumn);
                } else {
                    const prevContainerArr = [...emailState.entities.columns[source.droppableId].containers];
                    prevContainerArr.splice(source.index, 1);
                    if (newContainerArr.length === 1) {
                        if (emailState.entities.containers[newContainerArr[0]].elements.length === 0) {
                            newContainerArr.splice(0, 1, draggableId);
                        } else {
                            newContainerArr.splice(dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1, 0, draggableId);
                        }
                    } else {
                        newContainerArr.splice(dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1, 0, draggableId);
                    }
                    if (prevContainerArr.length === 0) {
                        const newContainerId = uuid();
                        const containerSwapOtherColumn = {
                            ...emailState,
                            entities: {
                                ...emailState.entities,
                                columns: {
                                    ...emailState.entities.columns,
                                    [dndOnDrag.parentDropId]: {
                                        ...emailState.entities.columns[dndOnDrag.parentDropId],
                                        containers: newContainerArr
                                    },
                                    [source.droppableId]: {
                                        ...emailState.entities.columns[source.droppableId],
                                        containers: [newContainerId]
                                    }
                                },
                                containers: {
                                    ...emailState.entities.containers,
                                    [newContainerId]: {
                                        ...emailState.entities.containers[newContainerId],
                                        elements: []
                                    }
                                }
                            }
                        }
                        return setEmailState(containerSwapOtherColumn);
                    } else {
                        const containerSwapOtherColumn = {
                            ...emailState,
                            entities: {
                                ...emailState.entities,
                                columns: {
                                    ...emailState.entities.columns,
                                    [dndOnDrag.parentDropId]: {
                                        ...emailState.entities.columns[dndOnDrag.parentDropId],
                                        containers: newContainerArr
                                    },
                                    [source.droppableId]: {
                                        ...emailState.entities.columns[source.droppableId],
                                        containers: prevContainerArr
                                    }
                                }
                            }
                        }
                        return setEmailState(containerSwapOtherColumn);
                    }
                }
            case "ELEMENT":
                const newElementArr = [...emailState.entities.containers[dndOnDrag.parentDropId].elements];
                if (source.droppableId === "ITEMS") {
                    const newElementId = uuid();
                    const newDestination = dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1;
                    newElementArr.splice(newDestination, 0, newElementId);
                    const newElement = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            containers: {
                                ...emailState.entities.containers,
                                [dndOnDrag.parentDropId]: {
                                    ...emailState.entities.containers[dndOnDrag.parentDropId],
                                    elements: newElementArr
                                }
                            },
                            element: newElementJSON(draggableId, newElementId)
                        }
                    }
                    return setEmailState(newElement);
                } else {
                    if (source.droppableId === dndOnDrag.parentDropId) {
                        newElementArr.splice(source.index, 1);
                        const newDroppedId = newElementArr.indexOf(dndOnDrag.droppedId);
                        newElementArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                        const elementSwapSameContainer = {
                            ...emailState,
                            entities: {
                                ...emailState.entities,
                                containers: {
                                    ...emailState.entities.containers,
                                    [dndOnDrag.parentDropId]: {
                                        ...emailState.entities.containers[dndOnDrag.parentDropId],
                                        elements: newElementArr
                                    }
                                }
                            }
                        }
                        return setEmailState(elementSwapSameContainer);
                    } else {
                        const prevElementArr = [...emailState.entities.containers[source.droppableId].elements];
                        prevElementArr.splice(source.index, 1);
                        const newDroppedId = newElementArr.indexOf(dndOnDrag.droppedId);
                        newElementArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                        if(prevElementArr.length === 0) {
                            const parentContainerArr = [...emailState.entities.columns[parentIDs.column].containers];
                            if(parentContainerArr.length > 1) {
                                const containerPosition = parentContainerArr.indexOf(source.droppableId);
                                parentContainerArr.splice(containerPosition, 1);
                                const containerSwapOtherColumn = {
                                    ...emailState,
                                    entities: {
                                        ...emailState.entities,
                                        columns: {
                                            ...emailState.entities.columns,
                                            [parentIDs.column]: {
                                                ...emailState.entities.columns[parentIDs.column],
                                                containers: [parentContainerArr]
                                            }
                                        },
                                        containers: {
                                            ...emailState.entities.containers,
                                            [dndOnDrag.parentDropId]: {
                                                ...emailState.entities.containers[dndOnDrag.parentDropId],
                                                elements: newElementArr
                                            },
                                            [source.droppableId]: {
                                                ...emailState.entities.containers[source.droppableId],
                                                elements: prevElementArr
                                            }
                                        }
                                    }
                                }
                                return setEmailState(containerSwapOtherColumn);
                            }
                        }
                        const containerSwapOtherColumn = {
                            ...emailState,
                            entities: {
                                ...emailState.entities,
                                containers: {
                                    ...emailState.entities.containers,
                                    [dndOnDrag.parentDropId]: {
                                        ...emailState.entities.containers[dndOnDrag.parentDropId],
                                        elements: newElementArr
                                    },
                                    [source.droppableId]: {
                                        ...emailState.entities.containers[source.droppableId],
                                        elements: prevElementArr
                                    }
                                }
                            }
                        }
                        return setEmailState(containerSwapOtherColumn);
                    }
                }
                break;
            case "STRUCTURE":
                const newStructuresArr = [...emailState.entities.section[dndOnDrag.parentDropId].structures];
                if (source.droppableId === "LAYOUTS") {
                    const newStructureId = uuid();
                    const firstColumnId = uuid();
                    const firstContainerId = uuid();
                    const secoundColumnId = uuid();
                    const secoundContainerId = uuid();
                    const thirdColumnId = uuid();
                    const thirdContainerId = uuid();
                    const fourthColumnId = uuid();
                    const fourthContainerId = uuid();
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, newStructureId);
                    switch (draggableId) {
                        case "esc_100":
                            const esc100 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 560,
                                            containers: [firstContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc100);
                        case "esc_50_50":
                            const esc5050 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 280,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 280,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc5050);
                        case "esc_70_30":
                            const esc7030 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 392,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 168,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc7030);
                        case "esc_30_70":
                            const esc3070 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 168,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 392,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc3070);
                        case "esc_33_33_33":
                            const esc333333 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId, secoundColumnId, thirdColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 187,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 187,
                                            containers: [secoundContainerId]
                                        },
                                        [thirdColumnId]: {
                                            id: thirdColumnId,
                                            width: 187,
                                            containers: [thirdContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: []
                                        },
                                        [thirdContainerId]: {
                                            id: thirdContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc333333);
                        case "esc_25_25_25_25":
                            const esc25252525 = {
                                ...emailState,
                                entities: {
                                    ...emailState.entities,
                                    section: {
                                        ...emailState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...emailState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...emailState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            columns: [firstColumnId, secoundColumnId, thirdColumnId, fourthColumnId]
                                        }
                                    },
                                    columns: {
                                        ...emailState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 140,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 140,
                                            containers: [secoundContainerId]
                                        },
                                        [thirdColumnId]: {
                                            id: thirdColumnId,
                                            width: 140,
                                            containers: [thirdContainerId]
                                        },
                                        [fourthColumnId]: {
                                            id: fourthColumnId,
                                            width: 140,
                                            containers: [fourthContainerId]
                                        }
                                    },
                                    containers: {
                                        ...emailState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: []
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: []
                                        },
                                        [thirdContainerId]: {
                                            id: thirdContainerId,
                                            elements: []
                                        },
                                        [fourthContainerId]: {
                                            id: fourthContainerId,
                                            elements: []
                                        }
                                    }
                                }
                            }
                            return setEmailState(esc25252525);
                        default:
                            break;
                    }
                    
                } else if (dndOnDrag.parentDropId === source.droppableId) {
                    newStructuresArr.splice(source.index, 1);
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const structureInSameArray = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            section: {
                                ...emailState.entities.section,
                                [dndOnDrag.parentDropId]: {
                                    ...emailState.entities.section[dndOnDrag.parentDropId],
                                    structures: newStructuresArr
                                }
                            },
                        }
                    }
                    return setEmailState(structureInSameArray);
                } else {
                    const prevStructuresArr = [...emailState.entities.section[source.droppableId].structures];
                    prevStructuresArr.splice(source.index, 1);
                    const sectionArr = [...emailState.result.sections];
                    if (prevStructuresArr.length === 0) {
                        const positionOfPrevSection = sectionArr.indexOf(source.droppableId);
                        sectionArr.splice(positionOfPrevSection, 1);
                    }
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const structureInSameArray = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            section: {
                                ...emailState.entities.section,
                                [dndOnDrag.parentDropId]: {
                                    ...emailState.entities.section[dndOnDrag.parentDropId],
                                    structures: newStructuresArr
                                },
                                [source.droppableId]: {
                                    ...emailState.entities.section[source.droppableId],
                                    structures: prevStructuresArr
                                }
                            },
                        },
                        result: {
                            sections: sectionArr
                        }
                    }
                    return setEmailState(structureInSameArray);
                }
            default:
                break;
        }
    }

    const popupOnDragStart = (result) => {
        const { source, draggableId, type } = result;
        setIsDndRuning(true);
        setDndDragType(type);
        setDndOnDrag({
            ...dndOnDrag,
            type: type
        })
        if (source.droppableId === "ITEMS") {
            return;
        }
    }

    const handleDelete = () => {
        setDeleteAction({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentIDs: null
        });
        setEditable({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentId: null
        });
        setSelectedElement(null);
        switch (deleteAction.type) {
            case "STRUCTURE":
                const newStructureArr = [...emailState.entities.section[deleteAction.parentIDs].structures]
                newStructureArr.splice(deleteAction.index, 1);
                const deleteStructure = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        section: {
                            ...emailState.entities.section,
                            [deleteAction.parentIDs]: {
                                ...emailState.entities.section[deleteAction.parentIDs],
                                structures: newStructureArr
                            }
                        }
                    }
                }
                return setEmailState(deleteStructure);
            case "CONTAINER":
                const newContainerArr = [...emailState.entities.columns[deleteAction.parentIDs].containers]
                newContainerArr.splice(deleteAction.index, 1);
                if (newContainerArr.length === 0) {
                    const newContainerId = uuid();
                    const deleteContainerIfNoContainers = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            columns: {
                                ...emailState.entities.columns,
                                [deleteAction.parentIDs]: {
                                    ...emailState.entities.columns[deleteAction.parentIDs],
                                    containers: [newContainerId]
                                }
                            },
                            containers: {
                                ...emailState.entities.containers,
                                [newContainerId]: {
                                    id: newContainerId,
                                    elements: []
                                }
                            }
                        }
                    }
                    return setEmailState(deleteContainerIfNoContainers);   
                } else {
                    const deleteContainer = {
                        ...emailState,
                        entities: {
                            ...emailState.entities,
                            columns: {
                                ...emailState.entities.columns,
                                [deleteAction.parentIDs]: {
                                    ...emailState.entities.columns[deleteAction.parentIDs],
                                    containers: newContainerArr
                                }
                            }
                        }
                    }
                    return setEmailState(deleteContainer);
                }
            case "ELEMENT":
                const newElementArr = [...emailState.entities.containers[deleteAction.parentIDs].elements];
                newElementArr.splice(deleteAction.index, 1);
                const deletedElement = {
                    ...emailState,
                    entities: {
                        ...emailState.entities,
                        containers: {
                            ...emailState.entities.containers,
                            [deleteAction.parentIDs]: {
                                ...emailState.entities.containers[deleteAction.parentIDs],
                                elements: newElementArr
                            }
                        }
                    }
                }
                return setEmailState(deletedElement);
            case "SECTION": 
                const newSectionArr = [...emailState.result.sections];
                newSectionArr.splice(deleteAction.index, 1);
                const deletedSection = {
                    ...emailState,
                    result: {
                        ...emailState.result,
                        sections: newSectionArr
                    }
                }
                return setEmailState(deletedSection);
            default:
                break;
        }
    }

    return (
        <React.Fragment>
            <DragDropContext
                onDragEnd={popupOnDragEnd}
                onDragStart={popupOnDragStart}
            >
                <Popup 
                    show={deleteAction.enable}
                    size='md' onHide={() => setDeleteAction({
                        enable: false,
                        type: null,
                        id: null,
                        index: null,
                        parentIDs: null
                    })}
                    onHandle={handleDelete} onHandleText='Delete' heading='Delete Warning'>
                    <p>Are you sure you want to delete this? This action cannot be undone.</p>
                </Popup>
                <div className="email-canvas">
                    {selectedElement !== null && <PropertyBar />}
                    <EmailEditorCanvas />
                </div>
                <div className="email-right-menu">
                    <EmailEditorMenuBar />
                </div>
                {editable.enable && <div className="email-right-properties-bar">
                    <AllProperties />
                </div>}
            </DragDropContext>
        </React.Fragment>
    );
}

export default EmailEditorBuilder;
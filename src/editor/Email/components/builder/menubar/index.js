import React, { useContext } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './menubar.scss';
import EmailWidgets from './widgets';
import EmailColumns from './columns';
import EmailSettings from './settings';
import { EmailStore } from '../../../store';

const EmailEditorMenuBar = () => {
    return (
        <Tabs className="email-menubar-tabs">
            <TabPanel className="email-menubar-content">
                <EmailWidgets />
            </TabPanel>
            <TabPanel className="email-menubar-content">
                <EmailColumns />
            </TabPanel>
            {/* <TabPanel className="email-menubar-content">
                <EmailSettings />
            </TabPanel> */}
            <TabList className="email-menubar-list">
                <Tab className="email-menubar-item">
                    <i className="material-icons">widgets</i>
                    <span>Widgets</span>
                </Tab>
                <Tab className="email-menubar-item">
                    <i className="material-icons" style={{ transform: "rotate(90deg)" }}>view_agenda</i>
                    <span>Column</span>
                </Tab>
                {/* <Tab className="email-menubar-item">
                    <i className="material-icons">settings</i>
                    <span>Settings</span>
                </Tab> */}
            </TabList>
        </Tabs>
    );
}

export default EmailEditorMenuBar;
import React from 'react';
import classnames from 'classnames';
import { Droppable, Draggable } from 'react-beautiful-dnd';

const EmailWidgets = () => {
    const Widgets = [
        {
            name: "Text",
            icon: "text_fields"
        },
        {
            name: "Image",
            icon: "image"
        },
        {
            name: "Button",
            icon: "remove_from_queue"
        },
        {
            name: "Video",
            icon: "ondemand_video"
        },
        {
            name: "Space",
            icon: "space_bar"
        },
        {
            name: "Line",
            icon: "line_style"
        }
    ]
    return (
        <Droppable droppableId="ITEMS" isDropDisabled type="ELEMENT">
            {(provided, snapshot) => (
                <ul
                    className="widget-list"
                    ref={provided.innerRef}
                >
                    {Widgets.map((widget, index) => (
                        <Draggable key={widget.name} draggableId={widget.name} index={index} disableInteractiveElementBlocking>
                            {(dragProvided, snapshot) => (
                                <React.Fragment>
                                    <li
                                        className={classnames("widget", snapshot.isDragging && "is-dragging")}
                                        key={widget.name}
                                        ref={dragProvided.innerRef}
                                        {...dragProvided.draggableProps}
                                    >
                                        <div className="widget-button" {...dragProvided.dragHandleProps}>
                                            <i className="material-icons">{widget.icon}</i>
                                            <span>{widget.name}</span>
                                        </div>
                                    </li>
                                    {snapshot.isDragging && <li className="widget">
                                        <div className="widget-button">
                                            <i className="material-icons">{widget.icon}</i>
                                            <span>{widget.name}</span>
                                        </div>
                                    </li>}
                                </React.Fragment>
                            )}
                        </Draggable>
                    ))}
                </ul>
            )}
        </Droppable>
    );
}

export default EmailWidgets;
import React, { useContext, useState, useEffect } from 'react';
import { Redirect } from "react-router-dom";
import { normalize } from "normalizr";
import cookie from "react-cookies";

import EmailEditorBuilder from '../components/builder';
import { PostData } from '../../../services/PostData';
import EmailEditorHeader from '../components/header';
import emailSchema from "../store/schema";
import { EmailStore } from '../store';
import { toast } from 'react-toastify';

const EmailBuider = (props) => {

    const { emailState, setEmailState } = useContext(EmailStore);
    //console.log('sadsddad',props.location.state)
    const sampleData = {
        sections: [
            {
                id: "sectionOne",
                style: {
                    backgroundColor: "#ffffff"
                },
                structures: [
                    {
                        id: "structureImage",
                        style: {
                            backgroundColor: "#28a5d1"
                        },
                        columns: [
                            {
                                id: "columnImage",
                                width: 560,
                                containers: [
                                    {
                                        id: "containerImage",
                                        elements: [
                                            {
                                                id: "elementImage",
                                                type: "IMAGE",
                                                url: "https://micro4.cisconet.world/public/uploads/imagelibraries/user_180/1343620606.png",
                                                alt: "Image",
                                                style: {
                                                    width: 100,
                                                    textAlign: "center",
                                                    paddingTop: 50,
                                                    paddingRight: 100,
                                                    paddingBottom: 50,
                                                    paddingLeft: 100
                                                }
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        id: "structureOne",
                        style: {
                            backgroundColor: "#28a5d1",
                            backgroundImage: "https://lh3.google.com/u/0/d/1iloWPPNGhkRhRVCsiLWn4QK2F3ecWtYO=w1600-h827-iv1",
                            backgroundRepeat: "no-repeat",
                            backgroundPosition: "bottom right",
                            backgroundSize: "contain"
                        },
                        columns: [
                            {
                                id: "columnTwo",
                                width: 560,
                                containers: [
                                    {
                                        id: "containerThree",
                                        elements: [
                                            {
                                                id: "elementFour",
                                                type: "TEXT",
                                                content: "Get them before thy're gone",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "elementSix",
                                                type: "TEXT",
                                                content: "Pick 4 free minis + enjoy free shipping with any $40 purchase.",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "buttonElement",
                                                type: "BUTTON",
                                                content: "CHOOSE NOW",
                                                buttonStyle: {
                                                    backgroundColor: "#ffa7bd",
                                                    fontSize: 12,
                                                    color: "#ffffff",
                                                    paddingTop: 10,
                                                    paddingRight: 15,
                                                    paddingBottom: 10,
                                                    paddingLeft: 15,
                                                    display: "inline-block",
                                                    textAlign: "center"
                                                },
                                                style: {
                                                    paddingTop: 15,
                                                    paddingRight: 15,
                                                    paddingBottom: 15,
                                                    paddingLeft: 15
                                                }
                                            },
                                            {
                                                id: "elementFive",
                                                type: "TEXT",
                                                content: "Don't wait. Ends in.",
                                                style: {
                                                    color: "#ffffff",
                                                    textAlign: "center"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }

    useEffect(() => {
        document.title = 'Email editor';
        setEmailState(null);
        const postInput = {
            "automation_email_id": cookie.load("_automationid"),
            "type":props.location.state.type
        }
        PostData('ms3', 'automationtemplateretrivejson', postInput).then((retriveData) => {
            if(retriveData !== 'Invalid') {
                // toast.info(retriveData.message);
                if (retriveData.json !== null) {
                    console.log(retriveData.json);
                    console.log(normalize(JSON.parse(retriveData.json), emailSchema));
                    setEmailState(normalize(JSON.parse(retriveData.json), emailSchema));
                }
            }
        });
        // Local Sample JSON
        // setEmailState(normalize(sampleData, emailSchema));
    }, []);

    if (cookie.load("_automationid") === undefined || cookie.load("_automationid") === null) {
        return (
            <Redirect to="/dashboard" />
        );
    }

    return (
        <div className="app-editor">
            {emailState !== null ? <React.Fragment>
                <EmailEditorHeader />
                <main className="app-content">
                    <EmailEditorBuilder />
                </main>
            </React.Fragment> : <span 
                style={{
                    padding:"5px",
                    fontSize: "16px",
                    fontWeight: "bold"
                }}>Loading...</span>}
        </div>
    );
}

export default EmailBuider;
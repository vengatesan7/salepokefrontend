import uuid from "uuidv4";

export const newSingleColumnStructure = (result, emailState) => {
    console.log(result)
    const newStructuresArr = [...emailState.entities.section[destination.droppableId].structures];
    const { draggableId, destination, source, type } = result;
    const newStructureId = uuid();
    const newColumnId = uuid();
    const newContainers = uuid();
    newStructuresArr.splice(destination.index, 0, newStructureId);
    const newStructure = {
        ...emailState,
        entities: {
            ...emailState.entities,
            section: {
                ...emailState.entities.section,
                [destination.droppableId]: {
                    ...emailState.entities.section[destination.droppableId],
                    structures: newStructuresArr
                }
            },
            structures: {
                ...emailState.entities.structures,
                [newStructureId]: {
                    id: newStructureId,
                    columns: [newColumnId]
                }
            },
            columns: {
                ...emailState.entities.columns,
                [newColumnId]: {
                    id: newColumnId,
                    width: 560,
                    containers: [newContainers]
                }
            },
            containers: {
                ...emailState.entities.containers,
                [newContainers]: {
                    id: newContainers,
                    elements: []
                }
            }
        }
    }
    return newStructure;
}
import React, { useContext, useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';

import { FormStore } from './store';
import General from './properties/general';
import Sizing from './properties/sizing';
import Option from './properties/option';
import ButtonProerties from './properties/button';
import LabelProperties from './properties/label';
import PlaceholderProperties from './properties/placeholder';
import TextareaProperties from './properties/textarea';
import InputProperties from './properties/input';

const PropertyBar = () => {
    const { template, editable, setEditable } = useContext(FormStore);
    const [fieldType, setFieldType] = useState(null);

    useEffect(() => {
        let selectedEleUpdate = template.entities.elements[editable.ele];
        setFieldType(selectedEleUpdate.fieldType);
    }, [editable]);

    const PropertyList = () => {
        if (fieldType === "text" || fieldType === "email" || fieldType === "tel" || fieldType === "number" || fieldType === "date") {
            return <React.Fragment>
                <General />
                <InputProperties />
                <hr />
                <Sizing />
            </React.Fragment>
        } else if (fieldType === "submit") {
            return <React.Fragment>
                <ButtonProerties />
            </React.Fragment>
        } else if (fieldType === "textarea") {
            return <React.Fragment>
                <LabelProperties />
                <hr />
                <PlaceholderProperties />
                <hr />
                <InputProperties />
                <hr />
                <TextareaProperties />
                <hr />
                <Sizing />
            </React.Fragment>
        
        } else if (fieldType === "select" || fieldType === "radio" || fieldType === "checkbox") {
            return <React.Fragment>
                <LabelProperties />
                <InputProperties />
                <hr />
                <Option />
                <hr />
                <Sizing />
            </React.Fragment>
        } else {
            return <React.Fragment>
                <General />
                <hr />
                <Sizing />
            </React.Fragment>;
        }
    }

    const handleClosePropertyBar = () => {
        setEditable({
            row: null,
            col: null,
            ele: null
        })
    }

    return (<React.Fragment>
        <div
            className="bg-light border-bottom pl-3 py-1 pr-1"
            style={{
                position: 'sticky',
                top: '0',
                zIndex: '3'
            }}
        >
            Properties
            <span
                className="float-right property-close-btn px-2 py-1"
                onMouseDown={() => handleClosePropertyBar()}
                style={{
                    lineHeight: "0"
                }}
            >
                <i className="fa fa-times" />
            </span>
        </div>
        <Row className="m-0" style={{ width: "100%" }}>
            <Col className="pb-3">
                {PropertyList()}
            </Col>
        </Row>
    </React.Fragment>);
}
 
export default PropertyBar;
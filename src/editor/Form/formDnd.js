import uuid from "uuid";

export function formDnd (result, template) {
    let newTemplate = template;
    const { destination, draggableId, source, type, reason } = result;
    switch (type) {
        case "FORMROW":
            if (destination.droppableId === source.droppableId) {
                let newRowUpdate = [...template.result.rows];
                newRowUpdate.splice(source.index, 1);
                newRowUpdate.splice(destination.index, 0, draggableId);
                newTemplate = {
                    ...template,
                    result: {
                        ...template.result,
                        rows: newRowUpdate
                    }
                }
            } else if (source.droppableId === 'GROUPTOOLBAR') {
                newTemplate = addNewGroupElement(result, template);
            }
            break;
        case "FORMELEMENT":
            if (destination.droppableId === source.droppableId) {
                let newEleUpdate = [...template.entities.cols[destination.droppableId].elements];
                newEleUpdate.splice(source.index, 1);
                newEleUpdate.splice(destination.index, 0, draggableId);
                newTemplate = {
                    ...template,
                    entities: {
                        ...template.entities,
                        cols: {
                            ...template.entities.cols,
                            [destination.droppableId]: {
                                ...template.entities.cols[destination.droppableId],
                                elements: newEleUpdate
                            }
                        }
                    }
                }
            } else if (source.droppableId === "TOOLBAR") {
                newTemplate = addNewElement(result, template);
            } else {
                let newEleSourceUpdate = [...template.entities.cols[source.droppableId].elements];
                let newEleDestinationUpdate = [...template.entities.cols[destination.droppableId].elements];
                newEleSourceUpdate.splice(source.index, 1);
                newEleDestinationUpdate.splice(destination.index, 0, draggableId);
                newTemplate = {
                    ...template,
                    entities: {
                        ...template.entities,
                        cols: {
                            ...template.entities.cols,
                            [destination.droppableId]: {
                                ...template.entities.cols[destination.droppableId],
                                elements: newEleDestinationUpdate
                            },
                            [source.droppableId]: {
                                ...template.entities.cols[source.droppableId],
                                elements: newEleSourceUpdate
                            }
                        }
                    }
                }
            }
        default:
            break;
    }
    return newTemplate;
}

function addNewElement (result, template) {
    const { destination, draggableId, source, type, reason } = result;
    const newEle = uuid();
    let newElementArr = [...template.entities.cols[destination.droppableId].elements];
    newElementArr.splice(destination.index, 0, newEle);
    console.log(template);
    let newElementObj = {}
    switch (draggableId) {
        case "SINGLELINE":
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "text",
                    label: "Label",
                    isLabel: true,
                    isRequired: false,
                    placeholder: "text...",
                    fieldSizes: 12,
                    labelSizes: 12,
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
        case "BUTTON":
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "submit",
                    buttonText: "Submit",
                    fieldSizes: 12,
                    color: "primary",
                    buttonStyle: {
                        display: "inline-block",
                        paddingLeft: 15,
                        paddingRight: 15,
                        paddingTop: 5,
                        paddingBottom: 5
                    },
                    buttonCol: {
                        textAlign: "center"
                    }
                }
            }
            break;
        case "SELECTBOX":
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "select",
                    label: "Select Label",
                    isLabel: true,
                    isRequired: false,
                    placeholder: "Select",
                    fieldSizes: 12,
                    labelSizes: 12,
                    options:["Option 1", "Option 2"],
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
        case "MULTILINE": {
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "textarea",
                    label: "Textarea Label",
                    isLabel: true,
                    isRequired: false,
                    placeholder: "Long text...",
                    fieldSizes: 12,
                    labelSizes: 12,
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
        }
        case "RADIO":
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "radio",
                    label: "Radio Label",
                    isLabel: true,
                    isRequired: false,
                    fieldSizes: 12,
                    labelSizes: 12,
                    options:["Option 1", "Option 2"],
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
        case "CHECKBOX":
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "checkbox",
                    label: "Checkbox Label",
                    isLabel: true,
                    isRequired: false,
                    fieldSizes: 12,
                    labelSizes: 12,
                    options:["Option 1", "Option 2"],
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
        default:
            newElementObj = {
                ...template.entities.elements,
                [newEle]: {
                    id: newEle,
                    fieldType: "text",
                    label: "Label",
                    isLabel: true,
                    isRequired: false,
                    placeholder: "text...",
                    fieldSizes: 12,
                    labelSizes: 12,
                    labelCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0},
                    controlCol: {paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0}
                }
            }
            break;
    }
    return {
        ...template,
        entities: {
            ...template.entities,
            cols: {
                ...template.entities.cols,
                [destination.droppableId]: {
                    ...template.entities.cols[destination.droppableId],
                    elements: newElementArr
                }
            },
            elements: newElementObj
        }
    }
}

function addNewGroupElement (result, template) {
    const { destination, draggableId, source, type, reason } = result;
    const newRow = uuid();
    const newRowTwo = uuid();
    const newRowThree = uuid();
    const newColOne = uuid();
    const newColTwo = uuid();
    const newColThree = uuid();
    const newColFour = uuid();
    const newColFive = uuid();
    const newEleOne = uuid();
    const newEleTwo = uuid();
    const newEleThree = uuid();
    const newEleFour = uuid();
    const newEleFive = uuid();
    let newRowUpdate = [...template.result.rows];

    newRowUpdate.splice(destination.index, 0, newRow);
    switch (draggableId) {
        case "SINGLELINE":
            const newSingleLine = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 12
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            fieldType: "text",
                            label: "Label",
                            isRequired: false,
                            placeholder: "text...",
                            fieldSizes: 12,
                            labelSizes: 12
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newRowUpdate
                }
            }
            return newSingleLine;
        case "NAME":
            const newName = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne, newColTwo]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            elements: [newEleTwo],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            isLabel: true,
                            fieldType: "text",
                            label: "First Name",
                            isRequired: true,
                            placeholder: "Enter First Name...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newEleTwo]: {
                            id: newEleTwo,
                            isLabel: true,
                            fieldType: "text",
                            label: "Last Name",
                            isRequired: true,
                            placeholder: "Enter Last Name...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newRowUpdate
                }
            }
            return newName;
        case "EMAIL":
            const newEmail = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 12,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            label: "Email",
                            isLabel: true,
                            fieldType: "email",
                            prependIcon: "fa fa-envelope-o",
                            prepend: true,
                            isRequired: true,
                            placeholder: "example@domainname.com",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newRowUpdate
                }
            }
            return newEmail;
        case "PHONE":
            const newPhone = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 12,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            fieldType: "tel",
                            label: "Phone",
                            isLabel: true,
                            prependIcon: "fa fa-phone",
                            prepend: true,
                            isRequired: true,
                            placeholder: "Ex: 12345678",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newRowUpdate
                }
            }
            return newPhone;
        case "DATE":
            const newDateTime = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 12,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            fieldType: "date",
                            label: "Date",
                            isLabel: true,
                            isRequired: true,
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newRowUpdate
                }
            }
            return newDateTime;
        case "ADDRESS":
            let newMultiRowUpdate = [...template.result.rows];
            const newSetArr = [newRow, newRowTwo, newRowThree]
            newMultiRowUpdate.splice(destination.index, 0, ...newSetArr);
            const newAddress = {
                ...template,
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow] : {
                            id: newRow,
                            cols: [newColOne]
                        },
                        [newRowTwo] : {
                            id: newRowTwo,
                            cols: [newColTwo, newColThree]
                        },
                        [newRowThree] : {
                            id: newRowThree,
                            cols: [newColFour, newColFive]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            elements: [newEleOne],
                            size: 12,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            elements: [newEleTwo],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newColThree]: {
                            id: newColThree,
                            elements: [newEleThree],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newColFour]: {
                            id: newColFour,
                            elements: [newEleFour],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newColFive]: {
                            id: newColFive,
                            elements: [newEleFive],
                            size: 6,
                            style: {
                                paddingLeft: 0,
                                paddingRight: 0,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    },
                    elements: {
                        ...template.entities.elements,
                        [newEleOne]: {
                            id: newEleOne,
                            fieldType: "textarea",
                            label: "Address Line 1",
                            isLabel: true,
                            isRequired: true,
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newEleTwo]: {
                            id: newEleTwo,
                            fieldType: "text",
                            label: "State",
                            isLabel: true,
                            isRequired: true,
                            placeholder: "Enter Your State...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newEleThree]: {
                            id: newEleThree,
                            fieldType: "text",
                            label: "City",
                            isLabel: true,
                            isRequired: true,
                            placeholder: "Enter Your City...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newEleFour]: {
                            id: newEleFour,
                            fieldType: "text",
                            label: "Country",
                            isLabel: true,
                            isRequired: true,
                            placeholder: "Search Contry...",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        },
                        [newEleFive]: {
                            id: newEleFive,
                            fieldType: "number",
                            minLenght: 6, 
                            label: "Zip Code",
                            isLabel: true,
                            prependText: "ZIP",
                            prepend: true,
                            isRequired: true,
                            placeholder: "Ex: 345 678",
                            fieldSizes: 12,
                            labelSizes: 12,
                            controlCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            },
                            labelCol: {
                                paddingLeft: 15,
                                paddingRight: 15,
                                paddingTop: 0,
                                paddingBottom: 0
                            }
                        }
                    }
                },
                result: {
                    ...template.result,
                    rows: newMultiRowUpdate
                }
            }
            return newAddress;
        default:
            return template;
    }
}
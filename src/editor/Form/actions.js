import uuid from "uuid";

export function addColumn (result, template) {
    const { rowId, colId, colIndex, position } = result;
    let updatedTemplate = template;
    const newCol = uuid();
    let colArr = [...template.entities.rows[rowId].cols]
    colArr.splice(position === "LEFT" ? colIndex : colIndex + 1 , 0, newCol);
    let newSize = template.entities.cols[colId].size / 2;
    updatedTemplate = {
        ...template,
        entities: {
            ...template.entities,
            rows: {
                ...template.entities.rows,
                [rowId]: {
                    ...template.entities.rows[rowId],
                    cols: colArr
                }
            },
            cols: {
                ...template.entities.cols,
                [newCol]: {
                    id: newCol,
                    size: newSize,
                    elements: []
                },
                [colId]: {
                    ...template.entities.cols[colId],
                    size: newSize
                }
            }
        }
    }
    console.log(updatedTemplate);
    return updatedTemplate;
}

export function addRow (type, template) {
    let updatedTemplate = template;
    const newRow = uuid();
    const newColOne = uuid();
    const newColTwo = uuid();
    const newColThree = uuid();
    const newColFour = uuid();

    console.log(template);

    let newUpdatedRow = [...template.result.rows]
    newUpdatedRow.unshift(newRow);

    switch (type) {
        case 'fc_12':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 12,
                            elements: []
                        }
                    }
                }
            }
            break;
        case 'fc_6_6':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne, newColTwo]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 6,
                            elements: []
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            size: 6,
                            elements: []
                        }
                    }
                }
            }
            break;
        case 'fc_4_8':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne, newColTwo]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 4,
                            elements: []
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            size: 8,
                            elements: []
                        }
                    }
                }
            }
            break;
        case 'fc_8_4':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne, newColTwo]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 8,
                            elements: []
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            size: 4,
                            elements: []
                        }
                    }
                }
            }
            break;
        case 'fc_4_4_4':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne, newColTwo, newColThree]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 4,
                            elements: []
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            size: 4,
                            elements: []
                        },
                        [newColThree]: {
                            id: newColThree,
                            size: 4,
                            elements: []
                        }
                    }
                }
            }
            break;
        case 'fc_3_3_3_3':
            updatedTemplate = {
                ...template,
                result: {
                    ...template.result,
                    rows: newUpdatedRow
                },
                entities: {
                    ...template.entities,
                    rows: {
                        ...template.entities.rows,
                        [newRow]: {
                            id: newRow,
                            style: {},
                            cols: [newColOne, newColTwo, newColThree, newColFour]
                        }
                    },
                    cols: {
                        ...template.entities.cols,
                        [newColOne]: {
                            id: newColOne,
                            size: 3,
                            elements: []
                        },
                        [newColTwo]: {
                            id: newColTwo,
                            size: 3,
                            elements: []
                        },
                        [newColThree]: {
                            id: newColThree,
                            size: 3,
                            elements: []
                        },
                        [newColFour]: {
                            id: newColFour,
                            size: 3,
                            elements: []
                        }
                    }
                }
            }
            break;
        default:
            break;
    }
    return updatedTemplate;
}
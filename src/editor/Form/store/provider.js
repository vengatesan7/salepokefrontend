import React, { useState } from "react";

import { FormStore } from "./index";

const FormProvider = props => {
    const [template, setTemplate] = useState(null);
    const [editable, setEditable] = useState({
        row: null,
        col: null,
        ele: null
    })

    const store = {
        template,
        setTemplate,
        editable,
        setEditable
    };
    return <FormStore.Provider value={store}>{props.children}</FormStore.Provider>;
};

export default FormProvider;

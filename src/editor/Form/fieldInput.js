import React from 'react';
import _get from "lodash.get";
import { InputGroup } from 'react-bootstrap';

const findInput = (field) => {
    console.log(field);
    const controlStyle = _get(field, 'controlStyle', {})
    if (field.fieldType === "text" || field.fieldType === "tel" || field.fieldType === "email" || field.fieldType === "number") {
        return (
            _get(field, 'prepend', false) ? <InputGroup>
                {/* <div className="input-group-prepend">
                    <span className="input-group-text"
                     style={{
                        ...controlStyle
                    }}
                    >
                        {_get(field, 'prependIcon', null) !== null && <i className={field.prependIcon} />}
                        {_get(field, 'prependText', '')}
                    </span>
                </div> */}
                <span className="form-control"
                    style={{
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        alignItems: 'center',
                        display: 'flex',
                        ...controlStyle
                    }}
                >
                    {field.placeholder}
                </span>
            </InputGroup> : <span className="form-control"
                style={{
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    alignItems: 'center',
                    display: 'flex',
                    height: field.fieldType === "textarea" ? "80px" : "auto",
                    ...controlStyle
                }}
            >
                {field.placeholder}
            </span>
        )
    } else if (field.fieldType === "date") {
        return <input className="form-control" style={{...controlStyle}} type="date" />
    } else if (field.fieldType === "textarea") {
        return <textarea className="form-control" style={{...controlStyle}} placeholder={field.placeholder} rows={_get(field, 'rows', 4)}/>
    } else if (field.fieldType === "password") {
        return (
            <InputGroup>
                <span className="form-control"
                    style={{
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        alignItems: 'center',
                        display: 'flex',
                        ...controlStyle
                    }}
                >
                    ******
                </span>
                <div className="input-group-prepend">
                    <span className="input-group-text">
                        <i className="fa fa-eye" />
                    </span>
                </div>
            </InputGroup>
        )
    } else if (field.fieldType === "select") {
        return <InputGroup>
            <span className="form-control"
                style={{
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    alignItems: 'center',
                    display: 'flex',
                    ...controlStyle
                }}
            >
                {field.placeholder}                
            </span>
            <div className="input-group-prepend">
                <span className="input-group-text">
                    <i className="fa fa-angle-down" />
                </span>
            </div>
        </InputGroup>
    } else if (field.fieldType === "radio") {
        return field.options.map((option, index) => {
            return <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name={field.label} id={`${field.label}-${index}`} value={option} />
                <label class="form-check-label" for={`${field.label}-${index}`}>
                    {option}
                </label>
            </div>
        })
    } 
    else if (field.fieldType === "checkbox") {
        return field.options.map((option, index) => {
            return <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name={field.label} id={`${field.label}-${index}`} value={option} />
                <label class="form-check-label" for={`${field.label}-${index}`}>
                    {option}
                </label>
            </div>
        })
    }
}

export default findInput;
import React, { Component } from 'react';
import { Container, Row, Col, Card, FormGroup, InputGroup, FormLabel, Button, ThemeProvider } from 'react-bootstrap';
import { schema, normalize } from "normalizr";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import classnames from "classnames";
import _get from "lodash.get";

import { FormStore } from './store';
import { formDnd } from './formDnd';
import { addColumn, addRow } from './actions';

import './form.scss';
import PropertyBar from './propertyBar';
import findInput from './fieldInput';

const widgets = [
    { name: 'Single Line', icon: 'fa fa-i-cursor' },
    { name: 'Select Box', icon: 'fa fa-angle-down' },
    { name: 'Multi Line', icon: '' },
    { name: 'Radio', icon: 'fa fa-dot-circle-o' },
    { name: 'Checkbox', icon: 'fa fa-check-square-o'},
    { name: 'Button', icon: '' }
]

const groupWidgets = [
    { name: 'Name', icon: 'fa fa-user-o' },
    { name: 'Email', icon: 'fa fa-envelope-o' },
    { name: 'Address', icon: 'fa fa-address-card-o' },
    { name: 'Phone', icon: 'fa fa-mobile' },
    { name: 'Date', icon: 'fa fa-calendar-check-o' },
    // { name: 'Card', icon: 'fa fa-credit-card' }
]

const AddRowList = ({ handleAddRow }) => {
    const rowList = [
        { btnName: 'fc_12', col: [12] },
        { btnName: 'fc_6_6', col: [6, 6] },
        { btnName: 'fc_4_8', col: [4, 8] },
        { btnName: 'fc_8_4', col: [8, 4] },
        { btnName: 'fc_4_4_4', col: [4, 4, 4] },
        { btnName: 'fc_3_3_3_3', col: [3, 3, 3, 3] },
    ]
    return (
        <ul className="add-row-list m-0">
            {rowList.map(row => {
                return <li className="add-row-type">
                    <span className="btn-block btn btn-secondary border mb-3 p-1" style={{ height: "45px", outline: "0px" }} onClick={() => handleAddRow(row.btnName)}>
                        {row.col.map(size => {
                            return <div className={classnames("p-1 d-inline-block", `col-${size}`)}>
                                <div
                                    className="d-block border "
                                    style={{ height: '32px' }}
                                />
                            </div>
                        })}
                    </span>
                </li>
            })}
        </ul>
    )
}

class Builder extends Component {
    static contextType = FormStore;
    constructor(props) {
        super(props);
        this.state = {
            addRowPopup: false,
        }
    }
    
    onDragEnd = (result) => {
        const { template } = this.context;
        if (result.destination === null) {
            return;
        }
        return this.context.setTemplate(formDnd(result, template));
    }

    onDragStart = (result) => {
        console.log(result);
    }

    handleAddRow = (type) => {
        const { template, setTemplate } = this.context;
        this.setState({
            addRowPopup: false,
        });
        return setTemplate(addRow(type, template));
    }

    handleDelete = (type, id, index) => {
        const { template } = this.context;
        let updatedTemplate = template;
        switch (type) {
            case "FORMROW":
                let newUpdatedRow = [...template.result.rows];
                newUpdatedRow.splice(index, 1);
                updatedTemplate = {
                    ...template,
                    result: {
                        ...template.result,
                        rows: newUpdatedRow
                    }
                }
                break;
            case "FORMELE":
                let newUpdatedEle = [...template.entities.cols[id].elements];
                newUpdatedEle.splice(index, 1);
                updatedTemplate = {
                    ...template,
                    entities: {
                        ...template.entities,
                        cols: {
                            ...template.entities.cols,
                            [id]: {
                                ...template.entities.cols[id],
                                elements: newUpdatedEle
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
        return this.context.setTemplate(updatedTemplate);
    }

    handleEditToggle = (eleId, colId, rowId) => {
        this.context.setEditable({
            row: rowId,
            col: colId,
            ele: eleId
        });
    }

    render() {
        const { template, editable } = this.context;
        const { addRowPopup } = this.state;
        let rowsArr = [];
        if (template !== null) {
            rowsArr = _get(template, 'result.rows', [])
        }
        return (
            template !== null && <Container  style={{ maxWidth: '100%'}}>
                <Row>
                    {/* <Col xs='12' className='ml-auto mr-auto py-2'> */}
                    <Col xs='12' className=''>

                        <Card className="border-0 bg-transparent">
                            <Card.Body className='p-0' style={{ width: '100%'}}>
                                <DragDropContext
                                    onDragEnd={this.onDragEnd}
                                    onDragStart={this.onDragStart}
                                >
                                    <Row className="m-0 bg-white">
                                        <Col xs='2' className='border overflow-auto bg-white form-element-bar'>
                                            <Row
                                                className="border-bottom bg-white"
                                                style={{
                                                    position: 'sticky',
                                                    top: '0',
                                                    zIndex: 2
                                                }}
                                            >
                                                <Col className="py-2 px-2">
                                                    <button
                                                        className="btn btn-outline-info btn-block px-3 py-2 border text-left"
                                                        style={{ fontSize: '10px', borderRadius: '3px' }}
                                                        onClick={() => this.setState({addRowPopup: !addRowPopup})}
                                                    >
                                                        <span className="d-inline-block text-center" style={{ width: '25px' }}>
                                                            <i className={classnames("fa", addRowPopup ? "fa-times" : "fa-plus")} style={{ fontSize: '14px', marginRight: '5px' }} />
                                                        </span>
                                                        Add Row
                                                    </button>
                                                </Col>
                                            </Row>
                                            <div>
                                                <Row
                                                    className="border-bottom mb-3 pb-1"
                                                    style={{
                                                        position: 'sticky',
                                                        top: '39px',
                                                        zIndex: 1
                                                    }}
                                                >
                                                    <Col className="text-center">
                                                        <small>Group Fields</small>
                                                    </Col>
                                                </Row>
                                                <Droppable
                                                    droppableId="GROUPTOOLBAR"
                                                    isDropDisabled
                                                    type="FORMROW"
                                                >
                                                    {FormToolDropProvided => (
                                                        <div 
                                                            className='pb-1 row'
                                                            {...FormToolDropProvided.droppableProps}
                                                            ref={FormToolDropProvided.innerRef}
                                                        >
                                                            {groupWidgets.map((widget, index) => {
                                                                const widgetId = widget.name.toUpperCase().replace(/\s/g, "");
                                                                
                                                                return <Col xs='12' className='px-2'>
                                                                    <Draggable
                                                                        key={widgetId}
                                                                        draggableId={widgetId}
                                                                        index={index}
                                                                    >
                                                                        {(FormToolDragProvided, FormToolDragSnapshot) => (
                                                                            <React.Fragment>
                                                                                <div
                                                                                    key={widgetId}
                                                                                    ref={FormToolDragProvided.innerRef}
                                                                                    {...FormToolDragProvided.draggableProps}
                                                                                    {...FormToolDragProvided.dragHandleProps}
                                                                                >
                                                                                    <p className="px-3 py-2 border" style={{ fontSize: '10px', borderRadius: '3px', backgroundColor: '#ffffff' }} >
                                                                                        <span className="d-inline-block" style={{ width: '25px' }}>
                                                                                            {_get(widget, 'icon', '') !== '' && <i
                                                                                                className={widget.icon}
                                                                                                style={{ fontSize: '14px', marginRight: '5px', verticalAlign: 'bottom', marginBottom: '2px' }}
                                                                                            />}
                                                                                        </span>
                                                                                        {widget.name}
                                                                                    </p>
                                                                                </div>
                                                                                {FormToolDragSnapshot.isDragging && <div>
                                                                                    <p className="px-3 py-2 border" style={{ fontSize: '10px', borderRadius: '3px' }} >
                                                                                        <span className="d-inline-block" style={{ width: '25px' }}>
                                                                                            {_get(widget, 'icon', '') !== '' && <i
                                                                                                className={widget.icon}
                                                                                                style={{ fontSize: '14px', marginRight: '5px', verticalAlign: 'bottom', marginBottom: '2px' }}
                                                                                            />}
                                                                                        </span>
                                                                                        {widget.name}
                                                                                    </p>
                                                                                </div>}
                                                                            </React.Fragment>
                                                                        )}
                                                                    </Draggable>
                                                                </Col>
                                                            })}
                                                        </div>
                                                    )}
                                                </Droppable>
                                            </div>
                                            <div>
                                                <Row
                                                    className="border-bottom border-top mb-3 pb-1"
                                                    style={{
                                                        position: 'sticky',
                                                        top: '48px',
                                                        zIndex: 1
                                                    }}
                                                >
                                                    <Col className="text-center">
                                                        <small>Single Fields</small>
                                                    </Col>
                                                </Row>
                                                <Droppable
                                                    droppableId="TOOLBAR"
                                                    isDropDisabled
                                                    type="FORMELEMENT"
                                                >
                                                    {FormToolDropProvided => (
                                                        <div 
                                                            className='row'
                                                            {...FormToolDropProvided.droppableProps}
                                                            ref={FormToolDropProvided.innerRef}
                                                        >
                                                            {widgets.map((widget, index) => {
                                                                const widgetId = widget.name.toUpperCase().replace(/\s/g, "");
                                                                return <Col xs='12' className='px-2'>
                                                                    <Draggable
                                                                        key={widgetId}
                                                                        draggableId={widgetId}
                                                                        index={index}
                                                                    >
                                                                        {(FormToolDragProvided, FormToolDragSnapshot) => (
                                                                            <React.Fragment>
                                                                                <div
                                                                                    key={widgetId}
                                                                                    ref={FormToolDragProvided.innerRef}
                                                                                    {...FormToolDragProvided.draggableProps}
                                                                                    {...FormToolDragProvided.dragHandleProps}
                                                                                >
                                                                                    <p className="px-3 py-2 border" style={{ fontSize: '10px', borderRadius: '3px', backgroundColor: '#ffffff' }} >
                                                                                        <span className="d-inline-block" style={{ width: '25px' }}>
                                                                                            {_get(widget, 'icon', '') !== '' && <i
                                                                                                className={widget.icon}
                                                                                                style={{ fontSize: '14px', marginRight: '5px', verticalAlign: 'bottom', marginBottom: '2px' }}
                                                                                            />}
                                                                                        </span>
                                                                                        {widget.name}
                                                                                    </p>
                                                                                </div>
                                                                                {FormToolDragSnapshot.isDragging && <div>
                                                                                    <p className="px-3 py-2 border" style={{ fontSize: '10px', borderRadius: '3px' }} >
                                                                                        <span className="d-inline-block" style={{ width: '25px' }}>
                                                                                            {_get(widget, 'icon', '') !== '' && <i
                                                                                                className={widget.icon}
                                                                                                style={{ fontSize: '14px', marginRight: '5px', verticalAlign: 'bottom', marginBottom: '2px' }}
                                                                                            />}
                                                                                        </span>
                                                                                        {widget.name}
                                                                                    </p>
                                                                                </div>}
                                                                            </React.Fragment>
                                                                        )}
                                                                    </Draggable>
                                                                </Col>
                                                            })}
                                                        </div>
                                                    )}
                                                </Droppable>
                                            </div>
                                        </Col>
                                        <Col xs='10' className='p-0 border-top border-bottom form-builder-canvas'>
                                            {addRowPopup && <div
                                                className="p-2 bg-white overflow-auto"
                                                style={{
                                                    position: 'absolute',
                                                    zIndex: '3',
                                                    width: '100%',
                                                    height: '100%'
                                                }}
                                            >
                                                <div className="add-row-popup">
                                                    <AddRowList handleAddRow={type => this.handleAddRow(type)}/>
                                                </div>
                                            </div>}
                                            {template !== null && <Droppable
                                                droppableId="CANVAS"
                                                direction="vertical"
                                                type="FORMROW"
                                            >
                                                {formCanvasProvided => (
                                                    <Container
                                                        {...formCanvasProvided.droppableProps}
                                                        ref={formCanvasProvided.innerRef}
                                                        className='canvas-container overflow-auto'
                                                        // onMouseLeave={e => isDragging && this.handleMouseLeaveCanvas(e)}
                                                        // onMouseUp={e => this.handleMouseDrop(e)}
                                                        style={{ maxHeight: 'fit-content',background: "#f6f6f6" 
 }}
                                                    >
                                                        {rowsArr.length === 0 ? <div className="empty-drop text-center p-2">
                                                            <div className="empty-drop-area p-2">
                                                                <small><i className="fa fa-plus" /></small>
                                                            </div>
                                                        </div> : rowsArr.map((rowId, rowIndex) => {
                                                            const rowData = template.entities.rows[rowId]
                                                            return <React.Fragment>
                                                                <Draggable
                                                                    key={rowId}
                                                                    draggableId={rowId}
                                                                    index={rowIndex}
                                                                >{(formRowDragProvided, formRowDragSnapshot) => (
                                                                    <div
                                                                        key={rowId}
                                                                        ref={formRowDragProvided.innerRef}
                                                                        {...formRowDragProvided.draggableProps}
                                                                    >
                                                                        <Row className="canvas-row m-0 pr-2 pl-4">
                                                                            <React.Fragment>
                                                                                {/* <div className="row-options">
                                                                                    <span className="row-drag-handle p-2" style={{ lineHeight: 0 }} {...formRowDragProvided.dragHandleProps}><span class="material-icons-outlined">drag_indicator</span></span>
                                                                                    {!rowData.isSubmit && <span className="handle-row-delete p-2" style={{ lineHeight: 0 }} onClick={() => this.handleDelete('FORMROW', rowId, rowIndex)}><span class="material-icons-outlined">delete</span></span>}
                                                                                </div> */}
                                                                                {template.entities.rows[rowId].cols.map((colId, colIndex) => {
                                                                                    const colData = template.entities.cols[colId];
                                                                                    const isElement = template.entities.cols[colId].elements.length
                                                                                    return <Col
                                                                                        className={classnames("canvas-col")}
                                                                                        style={_get(colData, 'style', {})}
                                                                                        xs={_get(colData, 'size', null)}
                                                                                    >
                                                                                        <Droppable
                                                                                            droppableId={colId}
                                                                                            direction="vertical"
                                                                                            type="FORMELEMENT"
                                                                                        >{(formColDropProvided, formColDropSnapshot) => (
                                                                                            <div
                                                                                                className={classnames("droppable-col", formColDropSnapshot.isDraggingOver && "isDragOver bg-info")}
                                                                                                {...formColDropProvided.droppableProps}
                                                                                                ref={formColDropProvided.innerRef}
                                                                                            >
                                                                                                {isElement === 0 ? <div className="empty-drop text-center p-2">
                                                                                                    <div className="empty-drop-area p-2">
                                                                                                        <small><i className="fa fa-plus" /></small>
                                                                                                    </div>
                                                                                                </div> : template.entities.cols[colId].elements.map((eleId, eleIndex) => {
                                                                                                    const field = template.entities.elements[eleId];
                                                                                                    return <Draggable
                                                                                                        key={eleId}
                                                                                                        draggableId={eleId}
                                                                                                        index={eleIndex}
                                                                                                    >{(formEleDragProvided, formEleDragSnapshot) => {
                                                                                                        return <div className={classnames("canvas-col-drag", formEleDragSnapshot.isDragging && 'isDragging', editable.ele === eleId && 'isEditable')}>
                                                                                                            <div
                                                                                                                key={eleId}
                                                                                                                ref={formEleDragProvided.innerRef}
                                                                                                                {...formEleDragProvided.draggableProps}
                                                                                                            >
                                                                                                                <div className="col-options">
                                                                                                                    <span className="col-drag-handle" {...formEleDragProvided.dragHandleProps}><span class="material-icons-outlined">drag_indicator</span></span>
                                                                                                                    <span className="col-delete-handle" style={{ lineHeight: 0 }} onClick={() => this.handleDelete("FORMELE", colId, eleIndex)}><span class="material-icons">delete</span></span>
                                                                                                                    <span className="col-edit-handle" onClick={() => this.handleEditToggle(eleId, colId, rowId)} ><span class="material-icons">edit</span></span>
                                                                                                                </div>
                                                                                                                {field.fieldType === "submit" ? <div style={field.buttonCol}>
                                                                                                                    <span className={classnames("btn", `btn-${field.color}`)} style={field.buttonStyle}>
                                                                                                                        {field.buttonText}
                                                                                                                    </span>
                                                                                                                </div> : <FormGroup className='row m-0'>
                                                                                                                    {_get(field, 'isLabel', false) && <Col
                                                                                                                        style={_get(field, 'labelCol', {})}
                                                                                                                        xs={_get(field, 'labelSizes', null)}
                                                                                                                    >
                                                                                                                        <FormLabel htmlFor={field.id}>
                                                                                                                            {field.label}<em style={{color:'red'}}>{field.isRequired ? '*' : '' }</em>
                                                                                                                        </FormLabel>
                                                                                                                    </Col>}
                                                                                                                    <Col
                                                                                                                        xs={_get(field, 'fieldSizes', null)}
                                                                                                                        style={_get(field, 'controlCol', {})}
                                                                                                                    >
                                                                                                                        {findInput(field)}
                                                                                                                        {/* {_get(field, 'prepend', false) ? <InputGroup>
                                                                                                                            <div className="input-group-prepend">
                                                                                                                                <span className="input-group-text">
                                                                                                                                    {_get(field, 'prependIcon', null) !== null && <i className={field.prependIcon} />}
                                                                                                                                    {_get(field, 'prependText', '')}
                                                                                                                                </span>
                                                                                                                            </div>
                                                                                                                            <span className="form-control"
                                                                                                                                style={{
                                                                                                                                    textOverflow: 'ellipsis',
                                                                                                                                    whiteSpace: 'nowrap',
                                                                                                                                    overflow: 'hidden'
                                                                                                                                }}
                                                                                                                            >
                                                                                                                                {field.placeholder}
                                                                                                                            </span>
                                                                                                                        </InputGroup> : <span className="form-control"
                                                                                                                            style={{
                                                                                                                                textOverflow: 'ellipsis',
                                                                                                                                whiteSpace: 'nowrap',
                                                                                                                                overflow: 'hidden'
                                                                                                                            }}
                                                                                                                        >
                                                                                                                            {field.placeholder}
                                                                                                                        </span>} */}
                                                                                                                    </Col>
                                                                                                                </FormGroup>}
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    }}</Draggable>
                                                                                                })}
                                                                                            </div>
                                                                                        )}</Droppable>
                                                                                    </Col>
                                                                                })}
                                                                                <div className="row-options row-options-del">
                                                                                    <span className="row-drag-handle p-2" style={{ lineHeight: 0 }} {...formRowDragProvided.dragHandleProps}><span class="material-icons-outlined">
drag_indicator
</span></span>
                                                                                    {!rowData.isSubmit && <span className="handle-row-delete p-2" style={{ lineHeight: 0 }} onClick={() => this.handleDelete('FORMROW', rowId, rowIndex)}><span class="material-icons-outlined">
delete
</span>

</span>}
                                                                                </div>
                                                                            </React.Fragment>
                                                                        </Row>
                                                                    </div>
                                                                )}</Draggable>
                                                            </React.Fragment>
                                                        })}
                                                        {formCanvasProvided.placeholder}
                                                    </Container>
                                                )}
                                            </Droppable>}
                                        </Col>
                                        {/* <Col xs='3' className='border-left p-0'> */}
                                            {editable.ele !== null && <div
                                                className='bg-white border-right form-outr-property-bar border-top border-bottom overflow-auto'
                                                // style={{ height: '380px' }}
                                            >
                                                <PropertyBar />
                                            </div>}
                                        {/* </Col> */}
                                    </Row>
                                </DragDropContext>
                            </Card.Body>
                            <Card.Footer className="bg-white" style={{ width: "100%" }}>
                                <span
                                    style={{
                                        fontSize: '14px',
                                        padding: '5px 15px'
                                    }}
                                    className="float-right btn btn-primary"
                                    onClick={this.props.handleSave}
                                >
                                    Save
                                </span>
                                <span
                                    style={{
                                        fontSize: '14px',
                                        padding: '5px 15px'
                                    }}
                                    className="mr-3 float-right btn btn-secondary"
                                    onClick={this.props.handleClose}
                                >
                                    Close
                                </span>
                            </Card.Footer>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}
export default Builder;
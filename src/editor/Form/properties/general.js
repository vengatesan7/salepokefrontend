import React, { useContext, useEffect, useState } from 'react';
import { FormGroup, Row, Col, Button } from 'react-bootstrap';
import classnames from "classnames";
import _get from "lodash.get";

import { FormStore } from '../store';

const General = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        setSelectedEle(selectedEle);
    }, [editable]);

    const handleOnChange = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            [name]: value
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        [name]: value
                    }
                }
            }
        })
    }
    
    return ( 
        selectedEle !== null && <React.Fragment>
            <Row className="mt-2">
                <Col>
                    <FormGroup>
                        <Row>
                            <Col xs='7'>
                                <label><small>Label</small></label>
                            </Col>
                            <Col xs='5'>
                                <div className="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        className="custom-control-input"
                                        id="is-label-switch"
                                        checked={selectedEle.isLabel}
                                        onChange={e => handleOnChange("isLabel", e.target.checked)}
                                    />
                                    <label className="custom-control-label" for="is-label-switch"><small>{_get(selectedEle, 'isLabel', false) ? 'OFF' : 'ON'}</small></label>
                                </div>
                            </Col>
                        </Row>
                        <input
                            type="text"
                            className="form-control"
                            value={selectedEle.label}
                            disabled={!_get(selectedEle, 'isLabel', false)}
                            onChange={e => handleOnChange("label", e.target.value)}
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col>
                    <FormGroup>
                        <label><small>Placeholder</small></label>
                        <input
                            type="text"
                            className="form-control"
                            value={selectedEle.placeholder}
                            onChange={e => handleOnChange("placeholder", e.target.value)}
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div className="custom-control custom-switch">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="is-required-switch"
                            checked={selectedEle.isRequired}
                            onChange={e => handleOnChange("isRequired", e.target.checked)}
                        />
                        <label className="custom-control-label" for="is-required-switch">Is Required</label>
                    </div>
                </Col>
            </Row>
            <hr />
            <Row className="mt-2">
                <Col>
                    <FormGroup>
                        {/* <Row>
                            <Col xs='7'>
                                <label><small>Prepend</small></label>
                            </Col>
                            <Col xs='5'>
                                <div className="custom-control custom-switch">
                                    <input
                                        type="checkbox"
                                        className="custom-control-input"
                                        id="is-prepend-switch"
                                        checked={_get(selectedEle, 'prepend', false)}
                                        onChange={e => handleOnChange("prepend", e.target.checked)}
                                    />
                                    <label className="custom-control-label" for="is-prepend-switch"><small>{_get(selectedEle, 'prepend', false) ? 'OFF' : 'ON'}</small></label>
                                </div>
                            </Col>
                        </Row> */}
                        <input
                            type="text"
                            className="form-control"
                            disabled={!_get(selectedEle, 'prepend', false)}
                            value={_get(selectedEle, 'prependText', '')}
                            onChange={e => handleOnChange("prependText", e.target.value)}
                        />
                    </FormGroup>
                </Col>
            </Row>
        </React.Fragment>
    );
}
 
export default General;
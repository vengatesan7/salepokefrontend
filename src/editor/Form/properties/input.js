import React, { useState, useEffect, useContext } from 'react';
import { ChromePicker } from 'react-color';

import _get from "lodash.get";

import { FormStore } from '../store';
import SpinInputButton from '../../components/SpinButtonInput';
import { Row, Col } from 'react-bootstrap';
import { transpile } from 'typescript';
import { BorderStyle } from './utils';

const InputProperties = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);
    const [backgroundPopup, setBackgroundPopup] = useState(false);

    const [padding, setPadding] = useState({
        x: 15,
        y: 15
    });

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        setSelectedEle(selectedEle);
        setPadding({
            x: _get(selectedEle, 'buttonStyle.paddingLeft', 15),
            y: _get(selectedEle, 'buttonStyle.paddingTop', 15)
        });
    }, [editable]);

    const handleControlStyle = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            controlStyle: {
                ...selectedEle.controlStyle,
                [name]: value
            }
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        controlStyle: {
                            ...selectedEle.controlStyle,
                            [name]: value
                        }
                    }
                }
            }
        });
    }

    return (
        selectedEle !== null && <React.Fragment>
            <Row className="mt-3">
                <Col xs='12'><label>Input Field</label></Col>
                <Col xs="6" className="pr-0">
                    <label><small>Font Size</small></label>
                </Col>
                <Col xs="6" className="p-0">
                    <SpinInputButton 
                        step={1}
                        value={_get(selectedEle, 'controlStyle.fontSize', 14)}
                        max="25"
                        min="1"
                        callBack={v => handleControlStyle('fontSize', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>

            <Row className="mt-3">
                {/* <Col xs='12'><label>Input Field</label></Col> */}
                <Col xs="6" className="pr-0">
                    <label><small>Height</small></label>
                </Col>
                <Col xs="6" className="p-0">
                    <SpinInputButton 
                        step={1}
                        value={_get(selectedEle, 'controlStyle.height', 40)}
                        max="100"
                        min="1"
                        callBack={v => handleControlStyle('height', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>


            <Row className="mt-3">
                {/* <Col xs='12'><label>Input Field</label></Col> */}
                <Col xs="6" className="pr-0">
                    <label><small>Border Radius</small></label>
                </Col>
                <Col xs="6" className="p-0">
                    <SpinInputButton 
                        step={1}
                        value={_get(selectedEle, 'controlStyle.borderRadius', 0)}
                        max="100"
                        min="0"
                        callBack={v => handleControlStyle('borderRadius', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>

            <Row>
                <Col>
                <div className="properties-field two-col">
                    <div className="properties-title">Background Color</div>
                    <div className="properties-value" style={{height: "22px"}}>
                        <div className="color-picker-none" onClick={() => handleControlStyle("backgroundColor", "transparent")}>                      
                            <i className="material-icons">not_interested</i>
                            </div>
                        <div className="color-picker-swatch" >
                            <div 
                                className="color-picker-color"
                                onClick={() => setBackgroundPopup(true)}
                                style={{
                                    
                                    backgroundColor: _get(selectedEle, 'controlStyle.backgroundColor', "transperent")
                                }}></div>
                        </div>
                        {backgroundPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setBackgroundPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={ _get(selectedEle, 'controlStyle.backgroundColor', "inherit")} onChange={(e) => handleControlStyle("backgroundColor", e.hex)} disableAlpha />
                                <button className='color-picker-button' onClick={() => setBackgroundPopup(false)}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </div>
                </Col>
            </Row>
            <Row>
                <Col>
                <div className="properties-title">Border</div>

                <BorderStyle
                            borderStyle={ _get(selectedEle, 'controlStyle.borderStyle', "solid")}
                            borderWidth={_get(selectedEle, 'controlStyle.borderWidth', 1)}
                            borderColor={_get(selectedEle, 'controlStyle.borderColor', "inherit")}
                            handleBorderStyle={(e) => handleControlStyle('borderStyle', e)}
                            handleBorderWidth={(e) => handleControlStyle('borderWidth', e)}
                            handleBorderColor={(e) => handleControlStyle('borderColor', e)}
                        />
                </Col>
            </Row>
            
        </React.Fragment>
    );
}

export default InputProperties;
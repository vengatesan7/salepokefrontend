import React, { useContext, useEffect, useState } from 'react';
import { FormGroup, Row, Col, Button } from 'react-bootstrap';
import classnames from "classnames";
import _get from "lodash.get";

import { FormStore } from '../store';

const PlaceholderProperties = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        setSelectedEle(selectedEle);
    }, [editable]);

    const handleOnChange = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            [name]: value
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        [name]: value
                    }
                }
            }
        })
    }
    
    return ( 
        selectedEle !== null && <React.Fragment>
            <Row>
                <Col>
                    <FormGroup>
                        <label><small>Placeholder</small></label>
                        <input
                            type="text"
                            className="form-control"
                            value={selectedEle.placeholder}
                            onChange={e => handleOnChange("placeholder", e.target.value)}
                        />
                    </FormGroup>
                </Col>
            </Row>
        </React.Fragment>
    );
}
 
export default PlaceholderProperties;
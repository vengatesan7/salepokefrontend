import React, { useContext, useEffect, useState } from 'react';
import { FormGroup, Row, Col, Button } from 'react-bootstrap';
import classnames from "classnames";
import _get from "lodash.get";

import { FormStore } from '../store';
import SpinInputButton from '../../components/SpinButtonInput';

const Positioning = ({selected, handlePositioning}) => {
    const {
        template,
        setTemplate,
        editable
    } = useContext(FormStore);
    const types = [
        {
            fieldSizes: 12,
            labelSizes: 12
        },
        {
            fieldSizes: 10,
            labelSizes: 2
        },
        {
            fieldSizes: 9,
            labelSizes: 3
        },
        {
            fieldSizes: 8,
            labelSizes: 4
        },
        {
            fieldSizes: 7,
            labelSizes: 5
        },
        {
            fieldSizes: 6,
            labelSizes: 6
        }
    ]

    return <Row>
        <Col>
            {types.map(type => {
                    return <button className={classnames("mb-1 border btn-block p-1 btn", JSON.stringify(selected) === JSON.stringify(type) ? 'bg-info' : 'bg-light')} onClick={() => handlePositioning(type)}>
                        <Col xs={type.labelSizes} className="float-left p-1">
                            <div className="p-2 bg-white" />
                        </Col>
                        <Col xs={type.fieldSizes} className="float-left p-1">
                            <div className="p-2 bg-white" />
                        </Col>
                    </button>
                }
            )}
        </Col>
    </Row>
}

const Sizing = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);
    const [padding, setPadding] = useState({
        x: 15,
        y: 15
    });
    const [spacing, setSpacing] = useState({
        x: 15,
        y: 15
    });

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        let selectedCol = template.entities.cols[editable.col];
        setSelectedEle(selectedEle);
        setPadding({
            x: _get(selectedEle, 'controlCol.paddingLeft', 0),
            y: _get(selectedEle, 'controlCol.paddingTop', 0)
        });
        setSpacing({
            x: _get(selectedCol, 'style.paddingLeft', 0),
            y: _get(selectedCol, 'style.paddingTop', 0)
        });
    }, [editable]);

    const handlePositioning = (type) => {
        setSelectedEle({
            ...selectedEle,
            ...type
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        ...type   
                    }
                }
            }
        });
    }

    const handleOnChangeSpacing = (axis, value) => {
        setSpacing({
            ...spacing,
            [axis]: value
        });
        let payload  = {};
        if (axis === 'x') {
            payload = {
                paddingLeft: value,
                paddingRight: value,
            }
        } else {
            payload = {
                paddingTop: value,
                paddingBottom: value,
            }
        }
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                cols: {
                    ...template.entities.cols,
                    [editable.col]: {
                        ...template.entities.cols[editable.col],
                        style: {
                            ...template.entities.cols[editable.col].style,
                            ...payload,
                        }
                    }
                }
            }
        });
    }

    const handleOnChangePadding = (axis, value) => {
        setPadding({
            ...padding,
            [axis]: value
        });
        let payload  = {};
        if (axis === 'x') {
            payload = {
                paddingLeft: value,
                paddingRight: value,
            }
        } else {
            payload = {
                paddingTop: value,
                paddingBottom: value,
            }
        }
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        controlCol: {
                            ...template.entities.elements[editable.ele].controlCol,
                            ...payload
                        },
                        labelCol: {
                            ...template.entities.elements[editable.ele].labelCol,
                            ...payload
                        }
                    }
                }
            }
        });
    }
    
    return ( 
        selectedEle !== null && <React.Fragment>
            <Row>
                <Col>
                    <label><small>Padding</small></label>
                    <Row>
                        <Col xs="6">
                            <small>X:</small>
                            <SpinInputButton
                                step={1}
                                value={padding.x}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangePadding('x', v)}
                                inputBoxDisabled
                            />
                        </Col>
                        <Col xs="6">
                            <small>y:</small>
                            <SpinInputButton
                                step={1}
                                value={padding.y}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangePadding('y', v)}
                                inputBoxDisabled
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col>
                    <label><small>Spacing</small></label>
                    <Row>
                        <Col xs="6">
                            <small>X:</small>
                            <SpinInputButton
                                step={1}
                                value={spacing.x}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangeSpacing('x', v)}
                                inputBoxDisabled
                            />
                        </Col>
                        <Col xs="6">
                            <small>y:</small>
                            <SpinInputButton
                                step={1}
                                value={spacing.y}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangeSpacing('y', v)}
                                inputBoxDisabled
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <hr />
            <Row>
                <Col>
                    <label><small>Positioning</small></label>
                    <Positioning
                        selected={{fieldSizes: selectedEle.fieldSizes, labelSizes: selectedEle.labelSizes}}
                        handlePositioning={type => handlePositioning(type)}
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
}
 
export default Sizing;
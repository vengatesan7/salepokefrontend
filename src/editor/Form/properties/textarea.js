import React, { useContext, useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import _get from "lodash.get";

import SpinInputButton from '../../components/SpinButtonInput';
import { FormStore } from '../store';

const TextareaProperties = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        setSelectedEle(selectedEle);
    }, [editable]);

    const handleOnChange = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            [name]: value
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        [name]: value
                    }
                }
            }
        })
    }
    
    return ( 
        selectedEle !== null && <React.Fragment>
            <Row>
                <Col xs='6'>
                    <label><small>Lines</small></label>
                </Col>
                <Col xs='6' className="p-0">
                    <SpinInputButton
                        step={1}
                        value={_get(selectedEle, 'rows', 4)}
                        max="10"
                        min="0"
                        callBack={v => handleOnChange('rows', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
}
 
export default TextareaProperties;
import React, { useContext, useEffect, useState } from 'react';
import { FormGroup, Row, Col, Button } from 'react-bootstrap';
import classnames from "classnames";
import _get from "lodash.get";
import { ChromePicker } from 'react-color';

import { FormStore } from '../store';
import SpinInputButton from '../../components/SpinButtonInput';
import { BorderStyle } from './utils';

const ButtonProerties = ({fieldData}) => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);
    const [selectedButtonColStyle, setSelectedButtonColStyle] = useState(null);
    const [backgroundPopup, setBackgroundPopup] = useState(false);
    const [colorPopup, setColorPopup] = useState(false);


    const [padding, setPadding] = useState({
        x: 15,
        y: 15
    });
    const [spacing, setSpacing] = useState({
        x: 15,
        y: 15
    });

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        let selectedCol = template.entities.cols[editable.col];
        setSelectedEle(selectedEle);
        setSelectedButtonColStyle(_get(selectedEle, 'buttonCol', {}));
        setPadding({
            x: _get(selectedEle, 'buttonStyle.paddingLeft', 15),
            y: _get(selectedEle, 'buttonStyle.paddingTop', 15)
        });
        setSpacing({
            x: _get(selectedCol, 'style.paddingLeft', 15),
            y: _get(selectedCol, 'style.paddingTop', 15)
        });
    }, [editable]);

    const handleOnChange = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            [name]: value
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        [name]: value
                    }
                }
            }
        });
    }

    const handleButtonColStyle = (name, value) => {
        setSelectedButtonColStyle({
            ...selectedButtonColStyle,
            [name]: value
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        buttonCol: {
                            ...template.entities.elements[editable.ele].buttonCol,
                            [name]: value
                        }
                    }
                }
            }
        });
    }

    const handleButtonStyle = (name, value) => {
        setSelectedEle({
            ...selectedEle,
            buttonStyle: {
                ...selectedEle.buttonStyle,
                [name]: value
            }
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        buttonStyle: {
                            ...template.entities.elements[editable.ele].buttonStyle,
                            [name]: value
                        }
                    }
                }
            }
        });
    }

    const handleOnChangeSpacing = (axis, value) => {
        setSpacing({
            ...spacing,
            [axis]: value
        });
        let payload  = {};
        if (axis === 'x') {
            payload = {
                paddingLeft: value,
                paddingRight: value,
            }
        } else {
            payload = {
                paddingTop: value,
                paddingBottom: value,
            }
        }
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                cols: {
                    ...template.entities.cols,
                    [editable.col]: {
                        ...template.entities.cols[editable.col],
                        style: {
                            ...template.entities.cols[editable.col].style,
                            ...payload,
                        }
                    }
                }
            }
        });
    }

    const handleOnChangePadding = (axis, value) => {
        let payload  = {};
        if (axis === 'x') {
            payload = {
                paddingLeft: value,
                paddingRight: value,
            }
        } else {
            payload = {
                paddingTop: value,
                paddingBottom: value,
            }
        }
        setPadding({
            ...padding,
            [axis]: value
        });
        let elementPayload = {
            ...template.entities.elements[editable.ele],
            buttonStyle: {
                ...template.entities.elements[editable.ele].buttonStyle,
                ...payload
            }
        }
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: elementPayload
                }
            }
        });
    }
    
    return ( 
        selectedEle !== null && <React.Fragment>
            <Row className="mt-2">
                <Col>
                    <FormGroup className="mb-0">
                        <label><small>Button Text</small></label>
                        <input
                            type="text"
                            className="form-control"
                            value={selectedEle.buttonText}
                            onChange={e => handleOnChange("buttonText", e.target.value)}
                        />
                    </FormGroup>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col>
                    <FormGroup>
                        <label><small>Button Align</small></label>
                        <select
                            className="float-right form-control"
                            value={_get(selectedButtonColStyle, 'textAlign', "center")}
                            onChange={e => handleButtonColStyle("textAlign", e.target.value)}
                        >
                            <option value="left">Left</option>
                            <option value="center">Center</option>
                            <option value="right">Right</option>
                        </select>
                    </FormGroup>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col>
                    <FormGroup>
                        <label><small>Button Style</small></label>
                        <select
                            className="float-right form-control"
                            value={_get(selectedEle, 'color', "primary")}
                            onChange={e => handleOnChange("color", e.target.value)} 
                        >
                            <option value="primary">Primary</option>
                            <option value="secondary">Secondary</option>
                            <option value="success">Success</option>
                            <option value="danger">Danger</option>
                            <option value="warning">Warning</option>
                            <option value="info">Info</option>
                            <option value="light">Light</option>
                            <option value="dark">Dark</option>
                        </select>
                    </FormGroup>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col>
                    <FormGroup>
                        <label><small>Button Size</small></label>
                        <select
                            className="float-right form-control"
                            value={_get(selectedEle, 'buttonStyle.width', "auto")}
                            onChange={e => handleButtonStyle("width", e.target.value)}
                        >
                            <option value="100%">Full Width</option>
                            <option value="auto">Fit To Text</option>
                        </select>
                    </FormGroup>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col xs="6" className="pr-0">
                    <label><small>Button Radius</small></label>
                </Col>
                <Col xs="6" className="pl-0">
                    <SpinInputButton
                        step={1}
                        value={_get(selectedEle, 'buttonStyle.borderRadius', 2)}
                        max="25"
                        min="0"
                        callBack={v => handleButtonStyle('borderRadius', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>
            <Row>
                <Col>
                    <label><small>Text Color</small></label>
                    </Col>
                    <Col>
                    <div className="properties-value" style={{height: "22px"}}>
                        {/* <div className="color-picker-none" onClick={() => handleButtonStyle("backgroundColor", "transparent")}>                      
                            <i className="material-icons">not_interested</i>
                            </div> */}
                        <div className="color-picker-swatch" >
                            <div 
                                className="color-picker-color"
                                onClick={() => setColorPopup(true)}
                                style={{
                                    
                                    backgroundColor: _get(selectedEle, 'buttonStyle.color', "inherit")
                                }}></div>
                        </div>
                        {colorPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setColorPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={ _get(selectedEle, 'buttonStyle.color', "inherit")} onChange={(e) => handleButtonStyle("color", e.hex)} disableAlpha />
                                <button className='color-picker-button' onClick={() => setColorPopup(false)}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </Col>
            </Row>
            
            <Row className="mt-3">
                <Col xs="6" className="pr-0">
                    <label><small>Font Size</small></label>
                </Col>
                <Col xs="6" className="pl-0">
                    <SpinInputButton
                        step={1}
                        value={_get(selectedEle, 'buttonStyle.fontSize', 14)}
                        max="25"
                        min="0"
                        callBack={v => handleButtonStyle('fontSize', v)}
                        inputBoxDisabled
                    />
                </Col>
            </Row>
            <Row>
                <Col>
                    <label><small>Background Color</small></label>
                    </Col>
                    <Col>
                    <div className="properties-value" style={{height: "22px"}}>
                        <div className="color-picker-none" onClick={() => handleButtonStyle("backgroundColor", "transparent")}>                      
                            <i className="material-icons">not_interested</i>
                            </div>
                        <div className="color-picker-swatch" >
                            <div 
                                className="color-picker-color"
                                onClick={() => setBackgroundPopup(true)}
                                style={{
                                    
                                    backgroundColor: _get(selectedEle, 'buttonStyle.backgroundColor', "transperent")
                                }}></div>
                        </div>
                        {backgroundPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setBackgroundPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={ _get(selectedEle, 'buttonStyle.backgroundColor', "inherit")} onChange={(e) => handleButtonStyle("backgroundColor", e.hex)} disableAlpha />
                                <button className='color-picker-button' onClick={() => setBackgroundPopup(false)}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs="4">
                <label><small>Border</small></label>
                </Col>
                <Col xs="8">
                <BorderStyle
                            borderStyle={ _get(selectedEle, 'buttonStyle.borderStyle', "solid")}
                            borderWidth={_get(selectedEle, 'buttonStyle.borderWidth', 1)}
                            borderColor={_get(selectedEle, 'buttonStyle.borderColor', "inherit")}
                            handleBorderStyle={(e) => handleButtonStyle('borderStyle', e)}
                            handleBorderWidth={(e) => handleButtonStyle('borderWidth', e)}
                            handleBorderColor={(e) => handleButtonStyle('borderColor', e)}
                        />
                </Col>
            </Row>
            <hr />
            <Row>
                <Col>
                    <label><small>Padding</small></label>
                    <Row>
                        <Col xs="6">
                            <small>X:</small>
                            <SpinInputButton
                                step={1}
                                value={padding.x}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangePadding('x', v)}
                                inputBoxDisabled
                            />
                        </Col>
                        <Col xs="6">
                            <small>y:</small>
                            <SpinInputButton
                                step={1}
                                value={padding.y}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangePadding('y', v)}
                                inputBoxDisabled
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col>
                    <label><small>Spacing</small></label>
                    <Row>
                        <Col xs="6">
                            <small>X:</small>
                            <SpinInputButton
                                step={1}
                                value={spacing.x}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangeSpacing('x', v)}
                                inputBoxDisabled
                            />
                        </Col>
                        <Col xs="6">
                            <small>y:</small>
                            <SpinInputButton
                                step={1}
                                value={spacing.y}
                                max="25"
                                min="0"
                                callBack={v => handleOnChangeSpacing('y', v)}
                                inputBoxDisabled
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </React.Fragment>
    );
}
 
export default ButtonProerties;
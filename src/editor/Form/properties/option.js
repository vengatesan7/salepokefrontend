import React, { useContext, useEffect, useState } from 'react';
import { FormGroup, Row, Col, Button, InputGroup } from 'react-bootstrap';
import classnames from "classnames";
import _get from "lodash.get";

import { FormStore } from '../store';

const Option = () => {
    const {
        template,
        setTemplate,
        editable,
        setEditable
    } = useContext(FormStore);
    const [selectedEle, setSelectedEle] = useState(null);

    useEffect(() => {
        let selectedEle = template.entities.elements[editable.ele];
        setSelectedEle(selectedEle);
    }, [editable]);

    const handleOption = (type, index, value) => {
        const optionUpdate = [...selectedEle.options];
        switch (type) {
            case 'ADD':
                optionUpdate.push('New Option');
                break;
            case 'DELETE':
                optionUpdate.splice(index, 1);
                break;
            case 'EDIT':
                optionUpdate.splice(index, 1, value);
                break;
            default:
                break;
        }
        setSelectedEle({
            ...selectedEle,
            options: optionUpdate
        });
        return setTemplate({
            ...template,
            entities: {
                ...template.entities,
                elements: {
                    ...template.entities.elements,
                    [editable.ele]: {
                        ...template.entities.elements[editable.ele],
                        options: optionUpdate
                    }
                }
            }
        })
    }

    return (
        selectedEle !== null && <React.Fragment>
            <Row className="mt-2">
                <Col>
                    <Row>
                        <Col xs='7'>
                            <label><small>Options</small></label>
                        </Col>
                        <Col xs='5' className="text-right">
                            <button
                                className="btn btn-outline-primary d-inline-block btn-sm"
                                style={{ fontSize: "10px" }}
                                onClick={() => handleOption('ADD')}
                            >
                                <i
                                    className="fa fa-plus"
                                    style={{
                                        fontSize: "10px",
                                        lineHeight: '15px'
                                    }}
                                />
                            </button>
                        </Col>
                    </Row>
                    {selectedEle.options.map((option, index) => {
                            return <Row className="mb-2">
                                <Col>
                                    <InputGroup>
                                        <input
                                            type="text"
                                            className="form-control"
                                            key={index}
                                            value={option}
                                            onChange={e => handleOption('EDIT', index, e.target.value)}
                                        />
                                        <button 
                                            className="btn btn-outline-secondary"
                                            onClick={e => handleOption('DELETE', index)}
                                        >
                                            <i className="fa fa-trash-o" />
                                        </button>
                                    </InputGroup>
                                </Col>
                            </Row>
                        }
                    )}
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default Option;
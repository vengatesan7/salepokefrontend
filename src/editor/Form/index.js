import React, { Component, useContext, useEffect } from 'react';
import { schema, normalize, denormalize } from "normalizr";

import FormProvider from './store/provider';
// import template from './template.json';
import Builder from './builder'
import { FormStore } from './store';
import { Store } from '../LandingPage/store';
import actions from '../LandingPage/store/action-types';
import { PopupStore } from '../PopupNew/store';

const element = new schema.Entity("elements");
const col = new schema.Entity("cols", { elements: [element] });
const row = new schema.Entity('rows', { cols: [col] });
const formSchema = {
    rows: [row],
    cols: [col],
    elements: [element]
}

const PageBuilder = ({newTemplate}) => {
    const {
        setTemplate,
        template
    } = useContext(FormStore);

    const {
        state,
        dispatch,
        state: { layers },
        history: { state: stateHistory }
    } = useContext(Store);

    useEffect(() => {
        const data = normalize(newTemplate, formSchema);
        setTemplate(data);
    }, [newTemplate]);
    
    const handleClose = () => {
        return dispatch({
            type: actions.ADVANCE_FORM_POPUP,
            payload: null,
            enable: false
        })
    }

    const updateElement = (data) => {
        return dispatch({
            type: actions.UPDATE_LAYER,
            payload: data
        });
    }

    const handleSave = () => {
        const denormalizeTemplate = denormalize(template.result, formSchema, template.entities)
        const data = {
            ...layers,
            entities: {
                ...layers.entities,
                element: {
                    ...layers.entities.element,
                    [template.result.id]: denormalizeTemplate
                }
            }
        }
        updateElement(data);
        return handleClose();
    }

    return template !== null && <Builder
        template={template}
        handleSave={() => handleSave()}
        handleClose={() => handleClose()}
    />
}

const PopupBuilder = ({newTemplate, save, close}) => {
    const {
        setTemplate,
        template
    } = useContext(FormStore);

    const {
        formBuilder, setFormBuilder,
        popupState, setPopupState
    } = useContext(PopupStore);

    useEffect(() => {
        const data = normalize(newTemplate, formSchema);
        setTemplate(data);
    }, [newTemplate]);
    

    const handleSave = () => {
        const denormalizeTemplate = denormalize(template.result, formSchema, template.entities)
        // const data = {
        //     ...popupState,
        //     entities: {
        //         ...popupState.entities,
        //         element: {
        //             ...popupState.entities.element,
        //             [denormalizeTemplate.id]: denormalizeTemplate
        //         }
        //     }
        // }

        save(denormalizeTemplate);
        return close();
    }

    return template !== null && <Builder
        template={template}
        handleSave={() => handleSave()}
        handleClose={() => close()}
    />
}

class FormBuilder extends Component {
    render() { 
        return ( 
            <FormProvider>
                <div
                    // className="p-5"
                    style={{
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        width: '100vw',
                        height: '100vh',
                        background: '#f6f6f6',
                        zIndex: 999
                    }}
                >
                    {this.props.isPopup ? 
                        <PopupBuilder newTemplate={this.props.template} close={this.props.close} save={this.props.save} /> : 
                        <PageBuilder newTemplate={this.props.template} />
                    }
                </div>
            </FormProvider>
        );
    }
}
 
export default FormBuilder;
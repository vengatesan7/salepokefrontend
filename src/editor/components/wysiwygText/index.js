import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from "draftjs-to-html";
import htmlToDraft from 'html-to-draftjs';
import PropTypes from 'prop-types';
import { ChromePicker } from 'react-color';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './wysiwyg.scss';
import ColorBoxSelection from '../colorBox/colorBoxSelection';

class ColorPic extends Component {
  static propTypes = {
    expanded: PropTypes.bool,
    onExpandEvent: PropTypes.func,
    onChange: PropTypes.func,
    currentState: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      color: this.props.currentState.color
    };
  }

  stopPropagation = (event) => {
    event.stopPropagation();
  };

  onChange = (color) => {
    const { onChange } = this.props;
    onChange('color', color);
  }

  colorOnChangeLocalUpdate = (color) => {
    this.setState({
      color: color.hex
    });
    // const { onChange } = this.props;
    // onChange('color', color.hex);
  }

  // renderModal = () => {
  //   const { color } = this.props.currentState;
  //   return (
  //     // <div
  //     //   className="text-editor-color-picker"
  //     //   onClick={this.stopPropagation}
  //     // >
  //     //   <ChromePicker color={this.state.color} onChangeComplete={this.colorOnChangeLocalUpdate} />
  //     // </div>
  //     <div className="color-picker-popover">
  //       <div className='color-picker-cover' onClick={this.stopPropagation}/>
  //       <div className='color-picker-wrapper'>
  //         <ChromePicker color={this.state.color} onChange={this.colorOnChangeLocalUpdate} disableAlpha />
  //         <button className='color-picker-button' onClick={this.stopPropagation}>Ok</button>
  //       </div>
  //     </div>
  //   );
  // };
  
  render() {
    const { expanded, onExpandEvent } = this.props;
    return (
      <div
        aria-haspopup="true"
        aria-expanded={expanded}
        aria-label="rdw-color-picker"
      >
        <div
          className="text-editor-color-picker-icon"
          onClick={onExpandEvent}
        >
          <i className="material-icons">
            color_lens
          </i>
        </div>
        {/* {expanded && <React.Fragment>
          <div
            className="text-editor-color-picker-overlay"
            onClick={() => this.onChange(this.state.color)}
            style={{
              position: "fixed",
              width: "100%",
              top: "0",
              left: "0",
              height: "100%",
              zIndex: "1"
            }}
          />
          {this.renderModal()}
        </React.Fragment>} */}
        {expanded && <div className="color-picker-popover" onClick={this.stopPropagation}>
          <div className='color-picker-cover' onClick={() => this.onChange(this.state.color)}/>
          <div className='color-picker-wrapper'>
            <ChromePicker color={this.state.color} onChange={this.colorOnChangeLocalUpdate} disableAlpha />
            <button className='color-picker-button' onClick={() => this.onChange(this.state.color)}>Ok</button>
          </div>
        </div>}
      </div>
    );
  }
}

class ColorBoxes extends Component {
  static propTypes = {
    expanded: PropTypes.bool,
    onExpandEvent: PropTypes.func,
    onChange: PropTypes.func,
    currentState: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      color: null
    };
  }
  componentDidMount() {
    this.setState({
      color: this.rgb2hex(this.props.currentState.color)
    })
  }
  stopPropagation = (event) => {
    event.stopPropagation();
  };
  onChange = (color) => {
    const { onChange } = this.props;
    onChange('color', color);
  }

  rgb2hex = (rgb) => {
    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    return (rgb && rgb.length === 4) ? "#" +
     ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
     ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
     ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
  }

  onInputChange = (color) => {
    this.setState({
      color: color
    })
  }

  renderModal = () => {
    return (
      <div
        className="text-editor-color-picker coustom-picher"
        onClick={this.stopPropagation}
      >
        <ColorBoxSelection selectedColor={evt => this.onChange(evt)}/>
        <input
          value={this.state.color}
          style={{
            textAlign: "center",
            padding: "5px 0"
          }}
          onChange={evt => this.onInputChange(evt.target.value)}
          onBlur={() => this.onChange(this.state.color)}
        />
      </div>
    );
  };
  render() {
    const { expanded, onExpandEvent } = this.props;
    return (
      <div
        aria-haspopup="true"
        aria-expanded={expanded}
        aria-label="rdw-color-picker"
      >
        <div
          className="text-editor-color-picker-icon"
          onClick={onExpandEvent}
        >
          <i className="material-icons">
            color_lens
          </i>
        </div>
        {expanded && this.renderModal()}
      </div>
    );
  }
}

class CustomOption extends Component {

  saveChanges = () => {
    this.props.onSubmitCallBack(draftToHtml(convertToRaw(this.props.editorState.getCurrentContent())));
  }

  render() {
    return (
      <button className="text-editor-save" onClick={() => this.saveChanges()}>Done</button>
    );
  }
}

class EditorConvertToHTML extends Component {
  constructor(props) {
    super(props);
    //const html = '<p>Test <strong>editor</strong> with simple text 😀</p>';
    const html = this.props.content;
    const contentBlock = htmlToDraft(html);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      //const editorState = EditorState.createEmpty()
      this.state = {
        editorState,
      };
    }
  }

  onEditorStateChange = (editorState) => {
    var htmlText = draftToHtml(convertToRaw(editorState.getCurrentContent())).replace(/  /g,"&nbsp;&nbsp;");
    htmlText  = htmlText.replace(/<p><\/p>/g, "<br />");
    this.setState({
      editorState,
    });
    this.props.onSubmitCallBack(htmlText);
  };

  render() {
    const { editorState } = this.state;
    return (
      <React.Fragment>
        <Editor
          editorState={editorState}
          toolbarClassName="text-editor-toolbar-absolute"
          wrapperClassName="text-editor-wrapper"
          editorClassName="text-editor-editor"
          onEditorStateChange={(e) => this.onEditorStateChange(e)}          
          // toolbarOnFocus
          // toolbarCustomButtons={[<CustomOption editorState={editorState} onSubmitCallBack={this.props.onSubmitCallBack}/>]}
          toolbar={{
            options: this.props.options !== undefined ? this.props.options : [ 'inline', 'blockType', 'fontSize', 'fontFamily', 'colorPicker', 'list', 'textAlign', 'link' ], //'history',
            inline: {
              options: ['bold', 'italic', 'underline', 'strikethrough'], // 'monospace'
              bold: { className: 'bordered-option-classname' },
              italic: { className: 'bordered-option-classname' },
              underline: { className: 'bordered-option-classname' },
              strikethrough: { className: 'bordered-option-classname' },
              code: { className: 'bordered-option-classname' },
            },
            list: {
              options: [ 'unordered', 'ordered' ]
            },
            blockType: {
              className: 'bordered-option-classname',
            },
            fontSize: {
              className: 'bordered-option-classname',
            },
            fontFamily: {
              className: 'bordered-option-classname',
              options: ["Roboto", "Open Sans", "Ubuntu", "Nunito Sans",'Noto Serif',"Noto Sans JP","Noto Sans", "Lato","Roboto Slab","Montserrat", "Anton","Source Sans Pro","Roboto Condensed","Oswald", "Josefin Sans", "Tajawal", "Raleway", "Oxygen", "Maven Pro", "Catamaran", "Orbitron", "Darker Grotesque", "Montserrat Alternates", "Enemy"]
            },
            colorPicker: { component: ColorPic },
            link: {
              inDropdown: false,
              className: undefined,
              component: undefined,
              popupClassName: "text-editor-link-popup",
              dropdownClassName: undefined,
              showOpenOptionOnHover: true,
              defaultTargetOption: '_blank',
              options: ['link', 'unlink'],
              linkCallback: undefined
            }
          }}
        />
      </React.Fragment>
    );
  }
}

export default EditorConvertToHTML;
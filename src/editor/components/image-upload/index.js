import React, { useState, useEffect } from "react";
import { ClipLoader } from "react-spinners";
import { toast } from "react-toastify";
import classnames from "classnames";

import { imageUploadPath } from "../../../views/services/Appurl";
import Popup from "../../../components/popup";
import { PostData } from "../../../views/services/PostData";

const ImageUpload = ({ imageLink, popupClose }) => {
  const [image, setImage] = useState(null);
  const [gallery, setGallery] = useState(null);
  const [popupTab, setPopupTab] = useState("gallery");
  const [uploadLoader, setUploadLoader] = useState(false);
  
  const getGalleryList = () => {
    PostData("ms1", "listimage").then(data => {
      if (data !== "Invalid") {
        if (data.status === "success") {
          setGallery(data.imagedata);
        }
      }
    });
  };
  useEffect(() => {
    getGalleryList();
  }, []);
  useEffect(() => {
    const newFileData = {
      image: image,
      image_text: "Sample Text"
    };
    {
      image &&
        PostData("ms1", "addimage", newFileData).then(response => {
          if (response !== "Invalid") {
            if (response.status === "success") {
              const uploadedPath = `${imageUploadPath}${response.imagedata.image_url}`;
              return imageLink(uploadedPath);
            }
          }
        });
    }
  }, [image]);
  const importImageFromGallery = path => {
    const uploadedPath = `${imageUploadPath}${path}`;
    return imageLink(uploadedPath);
  };
  const handleImageUpload = e => {
    setUploadLoader(true);
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length) return;
    createImage(files[0]);
  };
  const createImage = e => {
    let reader = new FileReader();
    reader.onload = e => {
      setImage(e.target.result);
    };
    reader.readAsDataURL(e);
  };
  const deleteImageFromGallery = id => {
    const deleteData = { image_id: id };
    PostData("ms1", "deleteimage", deleteData).then(response => {
      if (response !== "Invalid") {
        if (response.status === "success") {
          toast.info(response.message);
          getGalleryList();
        }
      }
    });
  };
  return (
    <Popup
      show
      size="lg"
      onHide={popupClose}
    // heading='Select Your Image'
    >
      <div className="popup-tab">
        <div className="popup-tab-head">
          <ul className="popup-tab-buttons">
            <li
              className={classnames(
                "popup-tab-button",
                popupTab === "gallery" && "active"
              )}
              onMouseDown={() => setPopupTab("gallery")}
            >
              Gallery
            </li>
            <li
              className={classnames(
                "popup-tab-button",
                popupTab === "upload" && "active"
              )}
              onMouseDown={() => setPopupTab("upload")}
            >
              Upload
            </li>
          </ul>
        </div>
        <div className="popup-tab-content">
          {popupTab === "gallery" && (
            <div className="popup-tab-type">
              {gallery ? (
                <React.Fragment>
                  <ol className="image-gallery">
                    {gallery.map(data => (
                      <li className="image">
                        <span
                          className="deleteIcon"
                          onClick={() => deleteImageFromGallery(data.image_id)}
                        >
                          <i className="material-icons">delete</i>
                        </span>
                        <span
                          className="imageView"
                          style={{
                            backgroundImage: `url(${imageUploadPath}${data.image_url})`
                          }}
                          onClick={() => importImageFromGallery(data.image_url)}
                        />
                      </li>
                    ))}
                  </ol>
                </React.Fragment>
              ) : (
                  <React.Fragment>
                    <div className="popup-loader">
                      <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={"#123abc"}
                        loading
                      />
                    </div>
                  </React.Fragment>
                )}
            </div>
          )}
          {popupTab === "upload" && (
            <div className="popup-tab-type">
              {uploadLoader ? (
                <div className="popup-loader">
                  <ClipLoader
                    sizeUnit={"px"}
                    size={50}
                    color={"#123abc"}
                    loading
                  />
                </div>
              ) : (
                  <div className="upload-wrapper">
                    <input
                      type="file"
                      onChange={e => handleImageUpload(e)}
                      className="cta upload"
                    />
                  </div>
                )}
            </div>
          )}
        </div>
      </div>
    </Popup>
  );
};

export default ImageUpload;

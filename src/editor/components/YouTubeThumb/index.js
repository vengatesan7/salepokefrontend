export const YoutubeThumb = (url, size) => {
    // // 'use strict';

    // var video, results;

    // var getThumb = function (url, size) {
        if (url === null) {
            return '';
        }
        const thumbSize    = (size === null) ? 'big' : size;
        const results = url.match('[\\?&]v=([^&#]*)');
        // const oldResults = url.match(/youtube\.com.*(\?v=|\/embed\/)(.{11})/);
        // console.log("results", results);
        // // console.log("oldResults", oldResults[1]);
        // console.log(url.search("youtube.com"), url.search("youtu.be"));
        const urlArr = url.split("/");
        const video   = (results === null) ? urlArr[urlArr.length - 1] : results[1];
        
        if (thumbSize === 'small') {
            return 'http://img.youtube.com/vi/' + video + '/2.jpg';
        }
        return 'http://img.youtube.com/vi/' + video + '/maxresdefault.jpg';
    // };

    // return {
    //     thumb: getThumb
    // };
}
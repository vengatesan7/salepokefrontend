import React, { useState, useEffect } from "react";

import './spinButtonInput.scss';

const SpinInputButton = ({
  step,
  value,
  max,
  min,
  callBack,
  inputBoxDisabled
}) => {
  const [thisValue, setValue] = useState(value);
  const [isMin, setIsMin] = useState(min || 0);
  const [isMax, setIsMax] = useState(max || null);
  const handleValue = type => {
    const stepValue = step || 1;
    if (type === "increment") {
      setValue(thisValue + stepValue);
    } else if (type === "decrement") {
      setValue(thisValue - stepValue);
    }
  };
  useEffect(() => {
    if (min || min === 0) {
      thisValue <= min ? setIsMin(true) : setIsMin(false);
      thisValue < min && setValue(min);
    }
    if (max) {
      thisValue >= max ? setIsMax(true) : setIsMax(false);
      thisValue > max && setValue(max);
    }
    callBack(thisValue);
  }, [thisValue]);
  const handleChangesInput = event => {
    let newValue = event.target.valueAsNumber;
    if (newValue <= max && newValue >= min) {
      setValue(newValue);
    }
  };
  return (
    <div className="spin-button">
      <button
        className='spin-di'
        onClick={() => handleValue("decrement")}
        disabled={isMin}
      >
        -
      </button>
      <input
        type="number"
        step={step}
        value={value}
        min={min}
        max={max}
        disabled={inputBoxDisabled}
        onChange={(e) => handleChangesInput(e)}
      />
      <button
        className='spin-in'
        onClick={() => handleValue("increment")}
        disabled={isMax}
      >
        +
      </button>
    </div>
  );
};

export default SpinInputButton;
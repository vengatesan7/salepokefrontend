import React, { useContext, useState, useEffect } from 'react';
import { Redirect } from "react-router-dom";
import { normalize } from "normalizr";
import cookie from "react-cookies";

// import { Context } from '../../../Context'; 
import PopupEditorBuilder from '../components/builder';
import PopupEditorHeader from '../components/header';
import popupSchema from "../store/schema";
import { PopupStore } from '../store';
import { toast } from 'react-toastify';
import { PostData } from '../../../views/services/PostData';

const PopupBuider = (props) => {

    // const { setFullWidthLoader } = useContext(Context);

    const { popupState, setPopupState, setPopupName } = useContext(PopupStore);
    const fullPagePopup = {
        popup: {
            closeButton:true,
            overlayColor:"#000000",
            startTime:2,
            popupStyle: {
                width:900,
                backgroundColor:"#7fa1d0",
                backgroundImage:"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/79964945.png",
                backgroundPosition:"bottom right",
                backgroundRepeat:"no-repeat",
                backgroundSize:"50%"
            },
            isFullPage:true,
            
        },
        sections: [
            {
                id:"sectionOne",
                style: {
                    backgroundColor:"#ffffff"
                },
                structures: [
                    {
                        id:"structureImage",
                        style: {
                            backgroundColor:"",
                            backgroundImage:""
                        },
                        columns: [
                            {
                                id:"columnSubForm",
                                width:560,
                                style: {
                                    width:50
                                },
                                containers: [
                                    {
                                        id:"containerSubForm",
                                        elements: [
                                            {
                                                id:"somthingNewText",
                                                type:"TEXT",
                                                content:"<p style='text-align:center;'>Submit the form below to subscribe your action</p>",
                                                style: {
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15
                                                }
                                            },
                                            {
                                                id:"linebelowthetext",
                                                type:"LINE",
                                                style: {
                                                    borderStyle:"solid",
                                                    borderRadius:0,
                                                    borderWidth:1,
                                                    borderColor:"black"
                                                }
                                            },
                                            {
                                                id:"9eaef0b0-6a7d-4066-95b0-1be0a18f5a19",
                                                type:"SPACE",
                                                style: {
                                                    height:100,
                                                    borderRadius:0
                                                }
                                            },
                                            {
                                                id:"696e397f-64f7-47e8-ad6a-bcaff21a8256",
                                                type:"SUBSCRIPTIONFROM",
                                                inputPlaceholder:"Enter your email here...",
                                                buttonText:"Send My Coupon",
                                                buttonSize:"fullwidth",
                                                formStyle: {
                                                    borderRadius:50,
                                                    paddingTop:15,
                                                    paddingRight:0,
                                                    paddingBottom:15,
                                                    paddingLeft:0
                                                },
                                                buttonStyle: {
                                                    textAlign:"center",
                                                    fontSize:14,
                                                    color:"#ffffff",
                                                    backgroundColor:"#db2141",
                                                    borderWidth:1,
                                                    borderStyle:"solid",
                                                    borderColor:"#5d4eab",
                                                    paddingTop:10,
                                                    paddingRight:10,
                                                    paddingBottom:10,
                                                    paddingLeft:10
                                                },
                                                inputStyle: {
                                                    textAlign:"left",
                                                    fontSize:14,
                                                    color:"#000000",
                                                    backgroundColor:"#ffffff",
                                                    borderWidth:1,
                                                    borderStyle:"solid",
                                                    borderColor:"#cccccc",
                                                    paddingTop:10,
                                                    paddingRight:10,
                                                    paddingBottom:10,
                                                    paddingLeft:10,
                                                    marginBottom:5
                                                }
                                            },
                                            {
                                                id:"c67b8b2a-6179-41ad-b987-e8fc7059999d",
                                                type:"SPACE",
                                                style: {
                                                    height:100,
                                                    borderRadius:0
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        id:"containerThree",
                                        elements: [
                                            {
                                                id:"elementFour",
                                                type:"TEXT",
                                                content:"<p style='text-align:center;'><span style='color: #ffffff;'>Get them before thy're gone</span></p>",
                                                style: {
                                                    paddingTop:0,
                                                    paddingRight:0,
                                                    paddingBottom:0,
                                                    paddingLeft:0
                                                }
                                            },
                                            {
                                                id:"elementSix",
                                                type:"TEXT",
                                                content:"<p style='text-align:center;'><span style='color: #ffffff;'>Pick 4 free minis + enjoy free shipping with any $40 purchase.</span></p>",
                                                style: {}
                                            },
                                            {
                                                id:"buttonElement",
                                                type:"BUTTON",
                                                content:"Button Text",
                                                buttonStyle: {
                                                    backgroundColor:"#3aaee0",
                                                    fontSize:12,
                                                    color:"#ffffff",
                                                    paddingTop:10,
                                                    paddingRight:15,
                                                    paddingBottom:10,
                                                    paddingLeft:15,
                                                    display:"inline-block",
                                                    textAlign:"center"
                                                },
                                                style: {
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15
                                                }
                                            }
                                        ],
                                        style: {
                                            borderRadius:50,
                                            backgroundImage:"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/1294043396.png",
                                            backgroundPosition:"bottom center",
                                            backgroundSize:"20%",
                                            backgroundRepeat:"no-repeat",
                                            borderTopWidth:8,
                                            borderRightWidth:8,
                                            borderBottomWidth:8,
                                            borderLeftWidth:8,
                                            borderTopStyle:"solid",
                                            borderRightStyle:"solid",
                                            borderBottomStyle:"solid",
                                            borderLeftStyle:"solid",
                                            borderTopColor:"#2b00ff",
                                            borderRightColor:"#000000",
                                            borderBottomColor:"#2b00ff",
                                            borderLeftColor:"#000000"
                                        }
                                    }
                                ]
                            },
                            {
                                id:"columnImage",
                                width:560,
                                style: {
                                    width:50
                                },
                                containers: [
                                    {
                                        id:"containerImage",
                                        elements: [
                                            {
                                                id:"ce8d2a43-2ed6-4cde-bee7-838034280a15",
                                                type:"SPACE",
                                                style: {
                                                    height:100,
                                                    borderRadius:0
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
    const sampleTwo = {
        popup: {
            closeButton:true,
            overlayColor:"#000000",
            startTime:2,
            popupStyle: {
                width:900,
                backgroundColor:"#7fa1d0",
                backgroundImage:"https://micro4.roundclicks.com/public/uploads/imagelibraries/user_40/79964945.png",
                backgroundPosition:"bottom right",
                backgroundRepeat:"no-repeat",
                backgroundSize:"50%"
            },
            isFullPage:true
        },
        sections: [
            {
                id:"sectionOne",
                style: {
                    backgroundColor:"#ffffff"
                },
                structures: [
                    {
                        id:"structureImage",
                        style: {
                            backgroundColor:"",
                            backgroundImage:""
                        },
                        columns: [
                            {
                                id:"columnSubForm",
                                width:560,
                                style: {
                                    width:50
                                },
                                containers: [
                                    {
                                        id:"containerSubForm",
                                        elements: [
                                            {
                                                id:"somthingNewText",
                                                type:"TEXT",
                                                content:"<p style=\"text-align:center;\"><span style=\"color: #683cff;font-size: 36px;\"><strong>Sample Popup Title</strong></span></p>\n",
                                                style: {
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15,
                                                    backgroundColor:"#ffffff",
                                                    borderRadius:15
                                                }
                                            },
                                            {
                                                id:"linebelowthetext",
                                                type:"LINE",
                                                style: {
                                                    borderStyle:"solid",
                                                    borderRadius:0,
                                                    borderWidth:3,
                                                    borderColor:"#ffffff",
                                                    paddingTop:0,
                                                    paddingRight:15,
                                                    paddingBottom:0,
                                                    paddingLeft:15
                                                }
                                            },
                                            {
                                                id:"e6ea7000-db83-45af-9851-9db90f898e96",
                                                type:"IMAGE",
                                                url:"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/180920317.png",
                                                alt:"Nature",
                                                style: {
                                                    width:"50",
                                                    textAlign:"center",
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15,
                                                    borderLeftWidth:4,
                                                    borderTopWidth:4,
                                                    borderRightWidth:4,
                                                    borderBottomWidth:4,
                                                    borderTopStyle:"solid",
                                                    borderRightStyle:"solid",
                                                    borderBottomStyle:"solid",
                                                    borderLeftStyle:"solid",
                                                    borderTopColor:"#65ff5e",
                                                    borderRightColor:"#65ff5e",
                                                    borderBottomColor:"#65ff5e",
                                                    borderLeftColor:"#65ff5e",
                                                    borderRadius:15
                                                },
                                                actionUrl:"http://roundclicks.com/"
                                            },
                                            {
                                                id:"696e397f-64f7-47e8-ad6a-bcaff21a8256",
                                                type:"SUBSCRIPTIONFROM",
                                                inputPlaceholder:"Enter your email here...",
                                                buttonText:"Send My Coupon",
                                                buttonSize:"fullwidth",
                                                formStyle: {
                                                    borderRadius:50,
                                                    paddingTop:15,
                                                    paddingRight:0,
                                                    paddingBottom:15,
                                                    paddingLeft:0
                                                },
                                                buttonStyle: {
                                                    textAlign:"center",
                                                    fontSize:14,
                                                    color:"#ffffff",
                                                    backgroundColor:"#db2141",
                                                    borderWidth:1,
                                                    borderStyle:"solid",
                                                    borderColor:"#5d4eab",
                                                    paddingTop:10,
                                                    paddingRight:10,
                                                    paddingBottom:10,
                                                    paddingLeft:10
                                                },
                                                inputStyle: {
                                                    textAlign:"left",
                                                    fontSize:14,
                                                    color:"#000000",
                                                    backgroundColor:"#ffffff",
                                                    borderWidth:1,
                                                    borderStyle:"solid",
                                                    borderColor:"#cccccc",
                                                    paddingTop:10,
                                                    paddingRight:10,
                                                    paddingBottom:10,
                                                    paddingLeft:10,
                                                    marginBottom:5
                                                }
                                            }, 
                                            {
                                                id:"5fae259c-94d0-4b9e-a369-d1c69383d717",
                                                type:"BUTTON",
                                                content:"Sample Button",
                                                buttonStyle: {
                                                    backgroundColor:"#6d6a6a",
                                                    color:"#ffffff",
                                                    fontSize:17,
                                                    paddingTop:5,
                                                    paddingRight:15,
                                                    paddingBottom:5,
                                                    paddingLeft:15,
                                                    display:"inline-block",
                                                    textAlign:"center",
                                                    backgroundImage:"linear-gradient(to bottom, #6d6a6a 0%, #a49f9f 51%, #6d6a6a 100%)",
                                                    borderRadius:15,
                                                    borderTopStyle:"solid",
                                                    borderRightStyle:"solid",
                                                    borderBottomStyle:"solid",
                                                    borderLeftStyle:"solid",
                                                    borderTopWidth:2,
                                                    borderRightWidth:2,
                                                    borderBottomWidth:2,
                                                    borderLeftWidth:2,
                                                    borderTopColor:"#ffffff",
                                                    borderRightColor:"#ffffff",
                                                    borderBottomColor:"#ffffff",
                                                    borderLeftColor:"#ffffff"
                                                },
                                                style: {
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15
                                                },
                                                actionUrl:"http://roundclicks.com/"
                                            }, 
                                            {
                                                id:"e90a3f7a-74fa-48f1-9b27-9069718f7e14",
                                                type:"VIDEO",
                                                embed:"",
                                                url:"https://youtu.be/VOx_anhlGMk",
                                                style: {
                                                    paddingTop:15,
                                                    paddingRight:15,
                                                    paddingBottom:15,
                                                    paddingLeft:15,
                                                    borderLeftWidth:7,
                                                    borderTopWidth:7,
                                                    borderRightWidth:7,
                                                    borderBottomWidth:7,
                                                    borderTopStyle:"solid",
                                                    borderRightStyle:"solid",
                                                    borderBottomStyle:"solid",
                                                    borderLeftStyle:"solid",
                                                    borderTopColor:"#2c01ff",
                                                    borderRightColor:"#2c01ff",
                                                    borderBottomColor:"#2c01ff",
                                                    borderLeftColor:"#2c01ff",
                                                    borderRadius:5
                                                }
                                            }
                                        ],
                                        style:{}
                                    }, 
                                    {
                                        id:"containerThree",
                                        elements: [
                                            {
                                                id:"elementFour",
                                                type:"TEXT",
                                                content:"<p style=\"text-align:center;\"><span style=\"color: #000000;\"><strong>Get them before thy're gone</strong></span></p>\n",
                                                style: {
                                                    paddingTop:25,
                                                    paddingRight:0,
                                                    paddingBottom:0,
                                                    paddingLeft:0
                                                }
                                            }, 
                                            {
                                                id:"elementSix",
                                                type:"TEXT",
                                                content:"<p style=\"text-align:center;\"><span style=\"color: rgb(0,0,0);\"><strong>Pick 4 free minis + enjoy free shipping with any $40 purchase.</strong></span></p>\n",
                                                style: {
                                                    paddingTop:0,
                                                    paddingRight:15,
                                                    paddingBottom:0,
                                                    paddingLeft:15
                                                }
                                            }, 
                                            {
                                                id:"buttonElement",
                                                type:"BUTTON",
                                                content:"Button Text",
                                                buttonStyle: {
                                                    backgroundColor:"#3aaee0",
                                                    fontSize:12,
                                                    color:"#ffffff",
                                                    paddingTop:10,
                                                    paddingRight:15,
                                                    paddingBottom:10,
                                                    paddingLeft:15,
                                                    display:"inline-block",
                                                    textAlign:"center"
                                                },
                                                style: {
                                                    paddingTop:10,
                                                    paddingRight:50,
                                                    paddingBottom:30,
                                                    paddingLeft:50
                                                }
                                            }
                                        ],
                                        style: {
                                            borderRadius:50,
                                            backgroundImage:"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/1294043396.png",
                                            backgroundPosition:"bottom center",
                                            backgroundSize:"20%",
                                            backgroundRepeat:"no-repeat",
                                            borderTopWidth:8,
                                            borderRightWidth:8,
                                            borderBottomWidth:8,
                                            borderLeftWidth:8,
                                            borderTopStyle:"solid",
                                            borderRightStyle:"solid",
                                            borderBottomStyle:"solid",
                                            borderLeftStyle:"solid",
                                            borderTopColor:"#2b00ff",
                                            borderRightColor:"#000000",
                                            borderBottomColor:"#2b00ff",
                                            borderLeftColor:"#000000",
                                            backgroundColor:"#ffffff"
                                        }
                                    }
                                ]
                            }, 
                            {
                                id:"columnImage",
                                width:560,
                                style: {
                                    width:50
                                },
                                containers: [
                                    {
                                        id:"containerImage",
                                        elements: [
                                            {
                                                id:"ce8d2a43-2ed6-4cde-bee7-838034280a15",
                                                type:"SPACE",
                                                style: {
                                                    height:150,
                                                    borderRadius:15,
                                                    backgroundColor:"",
                                                    backgroundImage:"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/180920317.png",
                                                    backgroundPosition:"center",
                                                    backgroundSize:"cover",
                                                    backgroundRepeat:"repeat",
                                                    borderTopStyle:"solid",
                                                    borderRightStyle:"solid",
                                                    borderBottomStyle:"solid",
                                                    borderLeftStyle:"solid",
                                                    borderTopWidth:4,
                                                    borderRightWidth:4,
                                                    borderBottomWidth:4,
                                                    borderLeftWidth:4,
                                                    borderTopColor:"#ffffff",
                                                    borderRightColor:"#ffffff",
                                                    borderBottomColor:"#ffffff",
                                                    borderLeftColor:"#ffffff"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }
    const sampleData = {
        popup: {
            closeButton: true,
            overlayColor: "#000000",
            startTime: 2,
            popupStyle: {
               width: 700,
            //    height: 400,
               backgroundColor: "#ffffff"
            }
         },
        sections: [
            {
                id: "sectionOne",
                style: {
                    backgroundColor: "#ffffff"
                },
                structures: [
                    {
                        id: "structureImage",
                        style: {
                            backgroundColor: "#ffffff"
                        },
                        columns: [
                            {
                                id: "columnSubForm",
                                width: 560,
                                style: {
                                    width: 50
                                },
                                containers: [
                                    {
                                        id: "containerSubForm",
                                        elements: [
                                            {
                                                id: "somthingNewText",
                                                type: "TEXT",
                                                content: "<p style='text-align:center;'>Submit the form below to subscribe your action</p>",
                                                style: {
                                                    paddingTop: 15,
                                                    paddingRight: 15,
                                                    paddingBottom: 15,
                                                    paddingLeft: 15
                                                }
                                            },
                                            {
                                                id: "linebelowthetext",
                                                type: "LINE",
                                                style: {
                                                    borderStyle: "solid",
                                                    borderRadius: 0,
                                                    borderWidth: 1,
                                                    borderColor: "black",
                                                }
                                            },
                                            {
                                                id: "696e397f-64f7-47e8-ad6a-bcaff21a8256",
                                                type: "SUBSCRIPTIONFROM",
                                                inputPlaceholder: "Enter your email here...",
                                                buttonText: "Send My Coupon",
                                                buttonSize: "fullwidth",
                                                formStyle: {
                                                   borderRadius: 50,
                                                   paddingTop: 15,
                                                   paddingRight: 0,
                                                   paddingBottom: 15,
                                                   paddingLeft: 0
                                                },
                                                buttonStyle: {
                                                   textAlign: "center",
                                                   fontSize: 14,
                                                   color: "#ffffff",
                                                   backgroundColor: "#db2141",
                                                   borderWidth: 1,
                                                   borderStyle: "solid",
                                                   borderColor: "#5d4eab",
                                                   paddingTop: 10,
                                                   paddingRight: 10,
                                                   paddingBottom: 10,
                                                   paddingLeft: 10,
                                                },
                                                inputStyle: {
                                                   textAlign: "left",
                                                   fontSize: 14,
                                                   color: "#000000",
                                                   backgroundColor: "#ffffff",
                                                   borderWidth: 1,
                                                   borderStyle: "solid",
                                                   borderColor: "#cccccc",
                                                    paddingTop: 10,
                                                    paddingRight: 10,
                                                    paddingBottom: 10,
                                                    paddingLeft: 10,
                                                    marginBottom: 5
                                                }
                                            }
                                        ]
                                    },
                                ]
                            },
                            {
                                id: "columnImage",
                                width: 560,
                                style: {
                                    width: 50
                                },
                                containers: [
                                    {
                                        id: "containerImage",
                                        elements: [
                                            {
                                                id: "elementImage",
                                                type: "IMAGE",
                                                url: "https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/2077072984.png",
                                                alt: "Image",
                                                style: {
                                                    width: 100,
                                                    textAlign: "center",
                                                    paddingTop: 0,
                                                    paddingRight: 0,
                                                    paddingBottom: 0,
                                                    paddingLeft: 0
                                                }
                                            }
                                        ]
                                    },
                                ]
                            },
                        ]
                    },
                    {
                        id: "structureOne",
                        style: {
                            backgroundColor: "#7aa0ce",
                            backgroundImage: "https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/1294043396.png",
                            backgroundRepeat: "no-repeat",
                            backgroundPosition: "bottom right",
                            backgroundSize: "10%"
                        },
                        columns: [
                            {
                                id: "columnTwo",
                                width: 560,
                                style: {
                                    width: 100
                                },
                                containers: [
                                    {
                                        id: "containerThree",
                                        elements: [
                                            {
                                                id: "elementFour",
                                                type: "TEXT",
                                                content: "<p style='text-align:center;'><span style='color: #ffffff;'>Get them before thy're gone</span></p>",
                                                style: {
                                                    // color: "#ffffff",
                                                    // textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "elementSix",
                                                type: "TEXT",
                                                content: "<p style='text-align:center;'><span style='color: #ffffff;'>Pick 4 free minis + enjoy free shipping with any $40 purchase.</span></p>",
                                                style: {
                                                    // color: "#ffffff",
                                                    // textAlign: "center"
                                                }
                                            },
                                            {
                                                id: "buttonElement",
                                                type: "BUTTON",
                                                content: "Button Text",
                                                buttonStyle: {
                                                    backgroundColor: "#3aaee0",
                                                    fontSize: 12,
                                                    color: "#ffffff",
                                                    paddingTop: 10,
                                                    paddingRight: 15,
                                                    paddingBottom: 10,
                                                    paddingLeft: 15,
                                                    display: "inline-block",
                                                    textAlign: "center"
                                                },
                                                style: {
                                                    paddingTop: 15,
                                                    paddingRight: 15,
                                                    paddingBottom: 15,
                                                    paddingLeft: 15
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    }

    const BuildFromScratch     =  {"popup":{"closeButton":true,"overlayColor":"#000000","startTime":2,"popupStyle":{"width":600,"backgroundColor":"#1c1c56","backgroundImage":"","backgroundPosition":"center","backgroundRepeat":"repeat","backgroundSize":"auto","borderRadius":0},"isFullPage":false},"sections":[{"id":"sectionOne","structures":[{"id":"67f38111-a09c-4697-bf41-1e1f7b2402d5","style":{"backgroundColor":"transparent","borderRadius":15,"paddingBottom":15,"paddingLeft":20,"paddingRight":10,"paddingTop":16},"columns":[{"id":"8fda8d82-c024-4ce8-af8e-4c9e689e9e37","width":50,"containers":[{"id":"b2cbea07-dca0-4f1b-b5f5-66586f93961a","elements":[{"id":"67689c00-ba4a-4310-a419-df468c70bb0a","type":"TEXT","content":"<p><span style=\"color: rgb(255,255,255);font-size: 24px;font-family: Roboto;\"><strong>Headline text</strong></span></p>\n","style":{"paddingTop":15,"paddingRight":0,"paddingBottom":15,"paddingLeft":0}},{"id":"ef4f43aa-6855-4385-81c2-ae35276b284c","type":"TEXT","content":"<p><span style=\"color: rgb(255,255,255);font-size: 18px;font-family: Roboto;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </span></p>\n","style":{"paddingTop":0,"paddingRight":0,"paddingBottom":15,"paddingLeft":0}}],"style":{"borderBottomColor":"#000000","borderBottomStyle":"solid","borderBottomWidth":0,"borderLeftColor":"#000000","borderLeftStyle":"solid","borderLeftWidth":0,"borderRadius":0,"borderRightColor":"#000000","borderRightStyle":"solid","borderRightWidth":0,"borderTopColor":"#000000","borderTopStyle":"solid","borderTopWidth":0,"paddingBottom":5,"paddingLeft":5,"paddingRight":5,"paddingTop":5}}]},{"id":"3196503b-b8f9-4b9b-860e-184af01e343e","width":50,"containers":[{"id":"f4cc1437-e114-48ff-a61a-4ee0c72e7c57","elements":[{"id":"b3e37b16-6136-4c9f-9242-b2e5a9a37c84","type":"FORM","formName":"","actionUrl":"","fontFamily":"Roboto","fontSize":"","fontColor":"#ffffff","lineHeight":19,"style":{"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0,"marginTop":0,"marginRight":0,"marginBottom":0,"marginLeft":0,"border":""},"rows":[{"id":"7f565cb6-5e6c-403e-8358-2c50bcd45a2f","style":{},"cols":[{"id":"de0f3395-8005-44ed-8762-613832e10216","size":12,"style":{"paddingLeft":0,"paddingRight":0,"paddingTop":7,"paddingBottom":7},"elements":[{"id":"f816370c-df60-401d-bfe1-2759588eb908","isLabel":true,"fieldType":"text","label":"First Name","isRequired":true,"placeholder":"Enter First Name...","fieldSizes":12,"labelSizes":12,"controlCol":{"paddingLeft":15,"paddingRight":15,"paddingTop":0,"paddingBottom":0},"labelCol":{"paddingLeft":15,"paddingRight":15,"paddingTop":0,"paddingBottom":0}},{"id":"2c56d775-7b25-4adb-b1ea-d848e5025b81","label":"Email","isLabel":true,"fieldType":"email","prependText":"Email","prepend":true,"isRequired":true,"placeholder":"example@domainname.com","fieldSizes":12,"labelSizes":12,"controlCol":{"paddingLeft":15,"paddingRight":15,"paddingTop":0,"paddingBottom":0},"labelCol":{"paddingLeft":15,"paddingRight":15,"paddingTop":0,"paddingBottom":0}}]}]},{"id":"7e7077a4-553a-4451-9b2a-e7fd4d89b355","style":{},"isSubmit":true,"cols":[{"id":"27f2b07e-6e17-4fc0-ae55-5b64489e1584","size":12,"style":{"paddingLeft":15,"paddingRight":15,"paddingTop":10,"paddingBottom":10},"elements":[{"id":"6e7eb634-5116-48bb-b793-c729f063c75a","fieldType":"submit","buttonText":"Submit","fieldSizes":12,"color":"success","buttonStyle":{"display":"inline-block","paddingLeft":15,"paddingRight":15,"paddingTop":5,"paddingBottom":5,"width":"100%","borderRadius":0},"buttonCol":{"textAlign":"center"}}]}]}]}],"style":{"borderBottomColor":"#000000","borderBottomStyle":"solid","borderBottomWidth":0,"borderLeftColor":"#000000","borderLeftStyle":"solid","borderLeftWidth":0,"borderRadius":0,"borderRightColor":"#000000","borderRightStyle":"solid","borderRightWidth":0,"borderTopColor":"#000000","borderTopStyle":"solid","borderTopWidth":0,"paddingBottom":5,"paddingLeft":5,"paddingRight":5,"paddingTop":5}}]}]}]}]}
    const BuildFromScratchtwo  = "{\"popup\":{\"closeButton\":true,\"overlayColor\":\"#000000\",\"startTime\":2,\"popupStyle\":{\"width\":800,\"backgroundColor\":\"#dbd3d3\",\"backgroundImage\":\"\",\"backgroundPosition\":\"bottom right\",\"backgroundRepeat\":\"repeat\",\"backgroundSize\":\"cover\"},\"isFullPage\":true},\"sections\":[{\"id\":\"sectionOne\",\"structures\":[{\"id\":\"3db02517-43ce-41a2-b337-cc4addee0e06\",\"columns\":[{\"id\":\"2b2c93ea-2bab-4645-8a88-df52160b4fdb\",\"width\":50,\"containers\":[{\"id\":\"44e6b10e-f1b1-4a5c-97f7-506dee4743a8\",\"elements\":[{\"id\":\"6ed33809-2a01-4139-b3a4-1567986ccd5e\",\"type\":\"TEXT\",\"content\":\"<p><span style=\\\"color: #3c5b97;font-size: 36px;font-family: Lato;\\\"><strong>Over 300 FREE Ideas For Your Blog</strong></span></p>\\n\",\"style\":{\"paddingTop\":35,\"paddingRight\":0,\"paddingBottom\":15,\"paddingLeft\":0}},{\"id\":\"b3129dea-b2b5-421b-8b0d-a5c410343049\",\"type\":\"TEXT\",\"content\":\"<p><span style=\\\"color: rgb(102,104,120);font-size: 18px;font-family: Lato;\\\">This <strong>FREE</strong> information will help you to structure your blog so that visitors return <strong>every-single-day</strong> and share your articles!</span></p>\\n\",\"style\":{\"paddingTop\":0,\"paddingRight\":0,\"paddingBottom\":15,\"paddingLeft\":0}},{\"id\":\"0302cc75-8994-480c-9195-8360757de63d\",\"type\":\"SUBSCRIPTIONFROM\",\"inputPlaceholder\":\"Enter your email here...\",\"buttonText\":\"Send My Coupon\",\"buttonSize\":\"fullwidth\",\"formStyle\":{\"borderRadius\":50,\"paddingTop\":15,\"paddingRight\":0,\"paddingBottom\":15,\"paddingLeft\":0},\"buttonStyle\":{\"textAlign\":\"center\",\"fontSize\":14,\"color\":\"#ffffff\",\"backgroundColor\":\"#5d4eab\",\"borderWidth\":1,\"borderStyle\":\"solid\",\"borderColor\":\"#5d4eab\",\"paddingTop\":10,\"paddingRight\":10,\"paddingBottom\":10,\"paddingLeft\":10},\"inputStyle\":{\"textAlign\":\"left\",\"fontSize\":14,\"color\":\"#000000\",\"backgroundColor\":\"#ffffff\",\"borderWidth\":1,\"borderStyle\":\"solid\",\"borderColor\":\"#cccccc\",\"paddingTop\":10,\"paddingRight\":10,\"paddingBottom\":10,\"paddingLeft\":10,\"marginBottom\":5}},{\"id\":\"b3fb13e6-40ba-479a-8cce-82f527ed8e43\",\"type\":\"SPACE\",\"style\":{\"height\":40,\"borderRadius\":0}}],\"style\":{\"borderBottomColor\":\"#000000\",\"borderBottomStyle\":\"solid\",\"borderBottomWidth\":0,\"borderLeftColor\":\"#000000\",\"borderLeftStyle\":\"solid\",\"borderLeftWidth\":0,\"borderRadius\":0,\"borderRightColor\":\"#000000\",\"borderRightStyle\":\"solid\",\"borderRightWidth\":0,\"borderTopColor\":\"#000000\",\"borderTopStyle\":\"solid\",\"borderTopWidth\":0,\"backgroundColor\":\"#ffffff\",\"paddingTop\":15,\"paddingRight\":50,\"paddingBottom\":15,\"paddingLeft\":50}}]},{\"id\":\"ab93a577-6e44-4eca-8fbd-9a00e894656e\",\"width\":50,\"containers\":[{\"id\":\"ccd366b2-ce69-449e-8e88-f3041aa5f966\",\"elements\":[{\"id\":\"dc50ffad-00c1-47a5-9d87-0c01b54c3b25\",\"type\":\"IMAGE\",\"url\":\"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/1758604360.png\",\"alt\":\"Image\",\"style\":{\"width\":\"50\",\"textAlign\":\"center\",\"paddingTop\":50,\"paddingRight\":0,\"paddingBottom\":100,\"paddingLeft\":0}}],\"style\":{\"borderBottomColor\":\"#000000\",\"borderBottomStyle\":\"solid\",\"borderBottomWidth\":0,\"borderLeftColor\":\"#000000\",\"borderLeftStyle\":\"solid\",\"borderLeftWidth\":0,\"borderRadius\":0,\"borderRightColor\":\"#000000\",\"borderRightStyle\":\"solid\",\"borderRightWidth\":0,\"borderTopColor\":\"#000000\",\"borderTopStyle\":\"solid\",\"borderTopWidth\":0,\"backgroundColor\":\"\",\"backgroundPosition\":\"bottom right\",\"backgroundImage\":\"\",\"backgroundSize\":\"contain\",\"backgroundRepeat\":\"no-repeat\"}}]}],\"style\":{\"backgroundImage\":\"https://micro4.rclicksdev.com/public/uploads/imagelibraries/user_40/645811551.png\",\"backgroundRepeat\":\"no-repeat\",\"backgroundPosition\":\"bottom right\",\"backgroundSize\":\"50%\",\"backgroundColor\":\"#fcd648\",\"paddingTop\":0,\"paddingRight\":0,\"paddingBottom\":0,\"paddingLeft\":0,\"borderRadius\":0,\"borderTopWidth\":0,\"borderRightWidth\":0,\"borderBottomWidth\":0,\"borderLeftWidth\":0,\"borderTopStyle\":\"solid\",\"borderRightStyle\":\"solid\",\"borderBottomStyle\":\"solid\",\"borderLeftStyle\":\"solid\"}}]}]}"
    
    useEffect(() => {
        setPopupState(null);
        // setFullWidthLoader(true);
        const postInput = {
            pop_up_id: cookie.load("popUpId"),
        }
        PostData("ms1", "getpopuptemplatebyid", postInput).then(response => {
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    // console.log(response);
                    console.log(response);
                    setPopupState(normalize(JSON.parse(response.getpopuptemplate.pop_up_json_code), popupSchema));
                    // setPopupState(normalize(JSON.parse(BuildFromScratchtwo), popupSchema));
                    setPopupName(response.getpopuptemplate.pop_up_name);
                            console.log(response.getpopuptemplate.pop_up_id);

                    // setFullWidthLoader(false);getpopuptemplate
                }
            }
        });

        // // Local Sample JSON
        // console.log(JSON.stringify(sampleTwo), normalize(BuildFromScratch, popupSchema));
        // setPopupName('Sample Popup');
        // setPopupState(normalize(BuildFromScratch, popupSchema));
    }, []);

    if (!cookie.load("popUpId")) {
        return (
            <Redirect to="/dashboard" />
        );
    }
    
    return (
        <div className="app-editor">
            {popupState !== null ? <React.Fragment>
                <PopupEditorHeader />
                {/* {console.log(popupState)} */}
                <main className="app-content">
                    <PopupEditorBuilder />
                </main>
            </React.Fragment> : <span 
                style={{
                    padding:"5px",
                    fontSize: "16px",
                    fontWeight: "bold"
                }}>Loading...</span>}
        </div>
    );
}

export default PopupBuider;
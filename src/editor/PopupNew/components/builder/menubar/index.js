import React, { useContext } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './menubar.scss';
import PopupWidgets from './widgets';
import PopupColumns from './columns';
import PopupSettings from './settings';
import { PopupStore } from '../../../store';

const EmailEditorMenuBar = () => {
    return (
        <Tabs className="popup-editor-menubar-tabs">
             <TabList className="popup-editor-menubar-list">
                <Tab className="popup-editor-menubar-item">
                    {/* <i className="material-icons">widgets</i> */}
                    <span>Widgets</span>
                </Tab>
                <Tab className="popup-editor-menubar-item">
                    {/* <i className="material-icons" style={{ transform: "rotate(90deg)" }}>view_agenda</i> */}
                    <span>Column</span>
                </Tab>
                <Tab className="popup-editor-menubar-item">
                    {/* <i className="material-icons">settings</i> */}
                    <span>Settings</span>
                </Tab>
            </TabList>
            <TabPanel className="popup-editor-menubar-content">
                <PopupWidgets />
            </TabPanel>
            <TabPanel className="popup-editor-menubar-content">
                <PopupColumns />
            </TabPanel>
            <TabPanel className="popup-editor-menubar-content">
                <PopupSettings />
            </TabPanel>
           
        </Tabs>
    );
}

export default EmailEditorMenuBar;
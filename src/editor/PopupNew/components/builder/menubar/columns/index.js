import React from 'react';
import classnames from 'classnames';
import { Droppable, Draggable } from 'react-beautiful-dnd';

const PopupColumns = () => {

    const layouts = {
        layoutNames: [ "esc_100", "esc_50_50", "esc_70_30", "esc_30_70", "esc_33_33_33", "esc_25_25_25_25" ],
        layout: {
            "esc_100" : {columns: [ 100 ]},
            "esc_50_50": {columns: [ 50, 50 ]},
            "esc_33_33_33": {columns: [ 33.33, 33.33, 33.33 ]},
            "esc_25_25_25_25": {columns: [ 25, 25, 25, 25 ]},
            "esc_70_30": {columns: [ 70, 30 ]},
            "esc_30_70": {columns: [ 30, 70 ]}
        }
    }

    return (
        <Droppable droppableId="LAYOUTS" isDropDisabled type="STRUCTURE">
            {(provided, snapshot) => (
                <ul className="row-list" ref={provided.innerRef}>
                    {layouts.layoutNames.map((layoutId, index) => (
                        <Draggable key={layoutId} draggableId={layoutId} index={index} disableInteractiveElementBlocking>
                            {(dragProvided, dragSnapshot) => (
                                <React.Fragment>
                                    <li
                                        className={classnames("row-type", dragSnapshot.isDragging && 'is-dragging')}
                                        key={layoutId}
                                        ref={dragProvided.innerRef}
                                        {...dragProvided.draggableProps}
                                    >
                                        <div
                                            className="row-button"
                                            {...dragProvided.dragHandleProps}
                                        >
                                            {layouts.layout[layoutId].columns.map((columnValue, index) => (
                                                <div style={{width: `${columnValue}%`}}><span></span></div>
                                            ))}
                                        </div>
                                    </li>
                                    {dragSnapshot.isDragging && <li className="row-type">
                                        <div className="row-button">
                                            {layouts.layout[layoutId].columns.map((columnValue, index) => (
                                                <div style={{width: `${columnValue}%`}}><span></span></div>
                                            ))}
                                        </div>
                                    </li>}
                                </React.Fragment>
                            )}
                        </Draggable>
                    ))}
                </ul>
            )}
        </Droppable>
    );
}

export default PopupColumns;
import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Switch from "react-switch";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';
import ImageUpload from '../../../../../components/image-upload';
import { Button } from 'react-bootstrap';
import SettingsBackground from './background';
import Border from './border';

const PopupSettings = () => {
    const { popupState, setPopupState } = useContext(PopupStore);
    const [values, setValues] = useState({});
    const [colorPickerPopup, setColorPickerPopup] = useState(false);
    const [importPopup, setImportPopup] = useState(false);

    useEffect(() => {
        setValues({
            width: _get(popupState.result.popup, 'popupStyle.width', 0),
            height: _get(popupState.result.popup, 'popupStyle.height', 0),
            backgroundColor: _get(popupState.result.popup, 'popupStyle.backgroundColor', "#ffffff"),
            backgroundImage: _get(popupState.result.popup, 'popupStyle.backgroundImage', null)
        })
    }, []);

    const handleBackgroundColor = (value) => {
        setValues({
            ...values,
            backgroundColor: value
        });
    }

    const handleChange = (value, name) => {
        setImportPopup(false);
        setColorPickerPopup(false);
        setValues({
            ...values,
            [name]: value
        });

        const newUpdate = {
            ...popupState,
            result: {
                ...popupState.result,
                popup: {
                    ...popupState.result.popup,
                    popupStyle: {
                        ...popupState.result.popup.popupStyle,
                        [name]: value
                    }
                }
            }
        }

        return setPopupState(newUpdate);
    }

    const handlePopupSetting = (value, name) => {
        const newUpdate = {
            ...popupState,
            result: {
                ...popupState.result,
                popup: {
                    ...popupState.result.popup,
                    [name]: value
                }
            }
        }

        return setPopupState(newUpdate);
    }

    return (<React.Fragment>
        <div className="settings-header">
            SETTINGS
        </div>
        <div className="settings-container">
            <div className="setting-category">
                <div className="setting-group-name">Container Size</div>
                <div className="setting-group-fields">
                    <div className="setting-property">
                        <label>Width</label>
                        <input type="number" value={values.width} onChange={e => handleChange(Number(e.target.value), 'width')} />
                    </div>
                    {/* <div className="setting-property">
                        <label>Height</label>
                        <input type="number" value={values.height} onChange={e => handleChange(Number(e.target.value), 'height')} />
                    </div> */}
                </div>
            </div>
            {/* <div className="setting-category">
                <div className="setting-group-name">Full Page Popup</div>
                <div className="setting-group-fields">
                    <div className="setting-property two-col">
                        <label>Active</label>
                        <div>
                            <Switch checked={_get(popupState.result.popup, 'isFullPage', false)} onChange={(e) => handlePopupSetting(e, 'isFullPage')}
                                onColor="#43da71" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30}
                            />
                        </div>
                    </div>
                </div>
            </div> */}
            <div className="setting-category background-properties">
                <SettingsBackground />
            </div>
            {!_get(popupState.result.popup, 'isFullPage', false) && <div className="setting-category background-properties">
                <Border />
            </div>}
        </div>
    </React.Fragment>);
}

export default PopupSettings;


{/* <div className="setting-category background-properties">
    <label>Background</label>
    <div className="two-column">
        <div className="setting-property">
            <label>Color</label>
            <div>
                <div className="color-picker-swatch" onClick={() => setColorPickerPopup(true)}>
                    <div className="color-picker-color" style={{ backgroundColor: values.backgroundColor }}></div>
                </div>
                {colorPickerPopup && <div className="color-picker-popover">
                    <div className='color-picker-cover' onClick={() => setColorPickerPopup(false)} />
                    <div className='color-picker-wrapper'>
                        <ChromePicker color={values.backgroundColor} onChange={(e) => handleBackgroundColor(e.hex)} disableAlpha />
                        <button className='color-picker-button' onClick={() => handleChange(values.backgroundColor, 'backgroundColor')}>Ok</button>
                    </div>
                </div>}
                <div className="color-picker-none" onClick={() => handleChange("", 'backgroundColor')} >
                    <i className="material-icons">not_interested</i>
                </div>
            </div>
        </div>
        <div className="setting-property">
            <label
                style={{ width: "100%" }}
            >Upload Image</label>
            <span className="upload-option" onClick={() => setImportPopup(true)}>
                <i className="material-icons">cloud_upload</i>
            </span>
            {values.backgroundImage !== null && <span className="upload-option" onClick={() => handleChange(null, 'backgroundImage')}>
                <i className="material-icons">delete</i>
            </span>}
            {importPopup && <ImageUpload imageLink={(e) => handleChange(e, 'backgroundImage')} popupClose={() => setImportPopup(false)} />}
        </div>
    </div>
    {values.backgroundImage !== null && <div className="image-viewer">
        <img src={values.backgroundImage} alt="URL is not valid" className="sample-view" onClick={() => setImportPopup(true)} />
    </div>}
</div> */}
import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from 'classnames';
import Switch from "react-switch";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';
import ImageUpload from '../../../../../components/image-upload';
import SpinButtonInput from "../../../../../components/SpinButtonInput";

const SettingsBackground = ({ blockType }) => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);
    const [blockStyle, setBlockStyle] = useState(null);

    //Popups
    const [backgroundPopup, setBackgroundPopup] = useState(false);
    const [backgroundImagePopup, setBackgroundImagePopup] = useState(false);
    const [backgroundImageMoreOptions, setBackgroundImageMoreOptions] = useState(false);

    useEffect(() => {
        setBlockStyle(_get(popupState.result.popup, "popupStyle", {backgroundColor: "#ffffff"}));
    }, [editable]);

    const handleStyle = (name, value) => {
        setBackgroundImagePopup(false);
        setBlockStyle({
            ...blockStyle,
            [name]: value
        });
        const styleUpdate = {
            ...popupState,
            result: {
                ...popupState.result,
                popup: {
                    ...popupState.result.popup,
                    popupStyle: {
                        ...popupState.result.popup.popupStyle,
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(styleUpdate);
    }

    return (
        blockStyle !== null && <div className="properties-group">
            <div className="setting-group-name">
                BACKGROUND
            </div>
            <div className="setting-group-fields">
                <div className="setting-property two-col">
                    <label>Background Color</label>
                    <div style={{height: "22px"}}>
                        <div className="color-picker-none" onClick={() => handleStyle("backgroundColor", "")}>
                            <i className="material-icons">not_interested</i>
                            </div>
                        <div className="color-picker-swatch" onClick={() => setBackgroundPopup(true)}>
                            <div 
                                className="color-picker-color"
                                style={{
                                    backgroundColor: blockStyle.backgroundColor
                                }}></div>
                        </div>
                        {backgroundPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setBackgroundPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker color={blockStyle.backgroundColor} onChange={(e) => handleStyle("backgroundColor", e.hex)} disableAlpha />
                                <button className='color-picker-button' onClick={() => setBackgroundPopup(false)}>Ok</button>
                            </div>
                        </div>}
                    </div>
                </div>
                <div className="setting-property">
                    <label>Background Image</label>
                    <div>
                        <div className="image-upload-button" onClick={() => setBackgroundImagePopup(true)}>Upload Image</div>
                        {backgroundImagePopup && <ImageUpload imageLink={(e) => handleStyle('backgroundImage', e)} popupClose={() => setBackgroundImagePopup(false)} />}
                    </div>
                    <label>Image URL</label>
                    <div>
                        <input type="text" value={blockStyle.backgroundImage} onChange={(e) => handleStyle('backgroundImage', e.target.value)}/>
                    </div>
                </div>
                {blockStyle.backgroundImage !== undefined && <div className="setting-property two-col">
                    <label>More Option</label>
                    <div>
                        <Switch checked={backgroundImageMoreOptions} onChange={(e) => setBackgroundImageMoreOptions(e)}
                            onColor="#43da71" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>}
                {backgroundImageMoreOptions && <React.Fragment>
                    <div className="setting-property two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Background Size</div>
                        <div>
                            <select 
                                value={blockStyle.backgroundSize} 
                                onChange={(e) => handleStyle('backgroundSize', e.target.value)}
                                style={{ height: '33px' }}
                            >
                                {blockStyle.backgroundPosition === undefined && <option>Select Position</option>}
                                <option value="auto">Auto</option>
                                <option value="contain">Contain</option>
                                <option value="cover">Cover</option>
                                <option value="10%">10%</option>
                                <option value="20%">20%</option>
                                <option value="30%">30%</option>
                                <option value="40%">40%</option>
                                <option value="50%">50%</option>
                                <option value="60%">60%</option>
                                <option value="70%">70%</option>
                                <option value="80%">80%</option>
                                <option value="90%">90%</option>
                                <option value="100%">100%</option>
                            </select>
                        </div>
                    </div>
                    <div className="setting-property two-col">
                        <div className="properties-title" style={{lineHeight: "40px"}}>Background Position</div>
                        <div>
                            <div className="background-positioning">
                                <div className="background-positioning-wrapper">
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "top left" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'top left')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "top center" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'top center')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "top right" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'top right')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "center left" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'center left')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "center" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'center')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "center right" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'center right')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "bottom left" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'bottom left')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "bottom center" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'bottom center')}
                                        />
                                    </div>
                                    <div className={classnames("background-positioning-field", blockStyle.backgroundPosition === "bottom right" && "active")}>
                                        <button
                                            className="background-positioning-button"
                                            onClick={() => handleStyle('backgroundPosition', 'bottom right')}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <input 
                                type="checkbox"
                                checked={blockStyle.backgroundRepeat === undefined || blockStyle.backgroundRepeat === "repeat"}
                                id="row-background-image-repeate"
                                onClick={(e) => handleStyle("backgroundRepeat", e.target.checked ? "repeat" : "no-repeat")}/>
                            <label
                                for="row-background-image-repeate"
                                style={{
                                    fontWeight: "300",
                                    fontStyle: "italic",
                                    verticalAlign: "top",
                                    marginLeft: "5px"
                                }}
                            >Repeat background image</label>
                        </div>
                    </div>
                </React.Fragment>}
            </div>
        </div>
    );
}

export default SettingsBackground;
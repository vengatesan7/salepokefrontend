import React, { useState, useContext, useEffect } from 'react';
import Switch from "react-switch";
import _get from "lodash.get";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';
import { BorderStyle } from '../../rightPropertyBar/properties/utils';

const Border = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);
    const [blockStyle, setblockStyle] = useState({
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderRadius: 0
    });

    // All Side Border
    const [allSideBorder, setAllSideBorder] = useState(false);

    useEffect(() => {
        const getStyle = _get(popupState.result.popup, "popupStyle", {
            borderTopWidth: 0,
            borderRightWidth: 0,
            borderBottomWidth: 0,
            borderLeftWidth: 0,
            borderRadius: 0
        })
        setblockStyle({
            ...blockStyle,
            ...getStyle
        });
    }, [editable]);

    useEffect(() => {
        const newBlockStyle = {...blockStyle};
        if (newBlockStyle.style !== undefined) {
            if (newBlockStyle.style.borderTopWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderRightWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderBottomWidth !== undefined) {
                console.log(newBlockStyle);
            }
            if (newBlockStyle.style.borderLeftWidth !== undefined) {
                console.log(newBlockStyle);
            }
        }
    }, [blockStyle])

    const blockStyleUpdate = (name, value) => {
        switch (name) {
            case "borderAllSideStyle":
                const newAllSideStyleContainer = {
                    ...popupState.result.popup.popupStyle,
                    borderTopStyle: value,
                    borderRightStyle: value,
                    borderBottomStyle: value,
                    borderLeftStyle: value
                }
                return newAllSideStyleContainer;
            case "borderAllSideWidth":
                const newAllSideWidthContainer = {
                    ...popupState.result.popup.popupStyle,
                    borderTopWidth: value,
                    borderRightWidth: value,
                    borderBottomWidth: value,
                    borderLeftWidth: value
                }
                return newAllSideWidthContainer;
            case "borderAllSideColor":
                const newAllSideColorContainer = {
                    ...popupState.result.popup.popupStyle,
                    borderTopColor: value,
                    borderRightColor: value,
                    borderBottomColor: value,
                    borderLeftColor: value
                }
                return newAllSideColorContainer;
            default:
                const newContainer = {
                    ...popupState.result.popup.popupStyle,
                    [name]: value
                }
                return newContainer;
        }
    }

    const handleStyle = (name, value) => {
        if (name === "borderAllSideStyle") {
            setblockStyle({
                ...blockStyle,
                borderTopStyle: value,
                borderRightStyle: value,
                borderBottomStyle: value,
                borderLeftStyle: value
            });
        } else if (name === "borderAllSideWidth") {
            setblockStyle({
                ...blockStyle,
                borderTopWidth: value,
                borderRightWidth: value,
                borderBottomWidth: value,
                borderLeftWidth: value
            });
        } else if (name === "borderAllSideColor") {
            setblockStyle({
                ...blockStyle,
                borderTopColor: value,
                borderRightColor: value,
                borderBottomColor: value,
                borderLeftColor: value
            });
        } else {
            setblockStyle({
                ...blockStyle,
                [name]: value
            });
        }
        const popupStyleUpdate = {
            ...popupState,
            result: {
                ...popupState.result,
                popup: {
                    ...popupState.result.popup,
                    popupStyle: blockStyleUpdate(name, value)
                }
            }
        }
        return setPopupState(popupStyleUpdate);
    }

    return (
        blockStyle !== null && <div className="properties-group">
            <div className="setting-group-name">
                BORDER PROPERTIES
            </div>
            <div className="setting-group-fields">
                <div className="setting-property two-col">
                    <label>Border</label>
                    <div className="properties-value">
                        <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                        <Switch checked={allSideBorder} onChange={(e) => setAllSideBorder(e)}
                            onColor="#43da71" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>
                {!allSideBorder ? <div className="setting-property two-col properties-field-popup-editor-editor-border">
                    <label style={{lineHeight: "34px"}}>All Sides</label>
                    <div className="properties-value">
                        <BorderStyle
                            borderStyle={blockStyle.borderTopStyle}
                            borderWidth={blockStyle.borderTopWidth}
                            borderColor={blockStyle.borderTopColor}
                            handleBorderStyle={(e) => handleStyle('borderAllSideStyle', e)}
                            handleBorderWidth={(e) => handleStyle('borderAllSideWidth', e)}
                            handleBorderColor={(e) => handleStyle('borderAllSideColor', e)}
                        />
                    </div>
                </div> :
                <div className="setting-property two-col properties-field-popup-editor-editor-border">
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Top</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderTopStyle}
                                    borderWidth={blockStyle.borderTopWidth}
                                    borderColor={blockStyle.borderTopColor}
                                    handleBorderStyle={(e) => handleStyle('borderTopStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderTopWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderTopColor', e)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Bottom</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderBottomStyle}
                                    borderWidth={blockStyle.borderBottomWidth}
                                    borderColor={blockStyle.borderBottomColor}
                                    handleBorderStyle={(e) => handleStyle('borderBottomStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderBottomWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderBottomColor', e)}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Right</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderRightStyle}
                                    borderWidth={blockStyle.borderRightWidth}
                                    borderColor={blockStyle.borderRightColor}
                                    handleBorderStyle={(e) => handleStyle('borderRightStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderRightWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderRightColor', e)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Left</div>
                            <div className="properties-value">
                                <BorderStyle
                                    borderStyle={blockStyle.borderLeftStyle}
                                    borderWidth={blockStyle.borderLeftWidth}
                                    borderColor={blockStyle.borderLeftColor}
                                    handleBorderStyle={(e) => handleStyle('borderLeftStyle', e)}
                                    handleBorderWidth={(e) => handleStyle('borderLeftWidth', e)}
                                    handleBorderColor={(e) => handleStyle('borderLeftColor', e)}
                                />
                            </div>
                        </div>
                    </div>
                </div>}
                <div className="setting-property two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Radius</div>
                    <div className="properties-value">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={blockStyle.borderRadius}
                            callBack={(v) => handleStyle("borderRadius", v)}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Border;
import React, { useContext, useEffect, useState } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";

import CanvasElement from "./element";
import { PopupStore } from "../../../store";

const CanvasColumn = ({ sectionId }) => {
  const { 
    popupState,
    setPopupState,
    mouseInPosition,
    setMouseInPosition,
    isDndRuning,
    dndDragType,
    setDropPosition,
    
    dndOnDrag,
    setDndOnDrag,
    editable,
    setEditable
  } = useContext(PopupStore);

  const columnStyle = (style) => {
    const updatedStyle = {
      width: `${_get(style, 'width', '100')}%`,
      display: 'inline-block'
    }
    return updatedStyle;
  }

  return (<div style={{ width: '100%' }}>
    {popupState.entities.section[sectionId].structures.map((structureId, structureIndex) => {
      const structureStyleData = _get(popupState.entities.structures[structureId], "style", {});

      const structureStyle = {
        ...structureStyleData,
        backgroundImage: structureStyleData.backgroundImage !== undefined ? `url(${structureStyleData.backgroundImage})`: ""
      }
      return (<Draggable draggableId={structureId} index={structureIndex}>
        {(structureDragProvided, structureDragSnapshot) => (
          <div
            className={
              classnames(
                "popup-editor-structure",
                structureDragSnapshot.isDragging && "isDragging",
                !editable.enable && !isDndRuning && mouseInPosition === "structure" && "isHovered",
                editable.id === structureId && "stracture-selected"
              )}
            onClick={() => mouseInPosition === "structure" && setEditable({
              enable: true,
              type: "STRUCTURE",
              id: structureId,
              index: structureIndex,
              parentId: sectionId
            })}
            {...structureDragProvided.draggableProps}
            ref={structureDragProvided.innerRef}
          >
            <span className="popup-editor-structure-label">STRUCTURE</span>
            <div
              className="popup-editor-structure-inside"
              style={structureStyle}
              onMouseEnter={() => setMouseInPosition("structure")}
              onMouseLeave={() => setMouseInPosition("section")}
            >
              {dndDragType === "STRUCTURE" && <div 
                className={classnames("drop-it-here", "top")}
                onMouseUp={() => setDndOnDrag({
                  ...dndOnDrag,
                  dropPosition: "top",
                  droppedId: structureId,
                  droppedIndex: structureIndex,
                  parentDropId: sectionId
                })}
              >
                <div className="drop-it-here-border">
                    <span>Drop it Here</span>
                </div>
              </div>}
              <span
                className="structure-option move"
                {...structureDragProvided.dragHandleProps}
              >
                <i className="material-icons">control_camera</i>
              </span>
              <div className="popup-editor-structure-wrapper">
                {popupState.entities.structures[structureId].columns.map((columnId, columnIndex) => {
                  const columnsCount = popupState.entities.structures[structureId].columns.length;
                    return (
                      <Droppable droppableId={columnId} type="CONTAINER">
                        {(columnProvider, columnSnapshot) => (
                          <div
                            {...columnProvider.droppableProps}
                            ref={columnProvider.innerRef}
                            isDraggingOver={columnSnapshot.isDraggingOver}
                            className={classnames(popupState.entities.columns[columnId].containers.length === 0 && "empty-container")}
                            style={columnStyle(popupState.entities.columns[columnId])}
                          >
                            {popupState.entities.columns[columnId].containers.map((containerId, containerIndex) => {
                              // console.log(popupState.entities.containers[containerId].elements);
                              const elementCount = popupState.entities.containers[containerId].elements.length;
                              const containerStyleData = _get(popupState.entities.containers[containerId], "style", {});
                              const containerStyle = {
                                ...containerStyleData,
                                backgroundImage: containerStyleData.backgroundImage !== undefined ? `url(${containerStyleData.backgroundImage})`: ""
                              }
                                return (
                                  <Draggable draggableId={containerId} index={containerIndex}>
                                    {(containerDragProvided, containerDragSnapshot) => (
                                      <React.Fragment>
                                        <div
                                          className={
                                            classnames("popup-editor-container",
                                            containerDragSnapshot.isDragging && "isDragging",
                                            !editable.enable && !isDndRuning && mouseInPosition === "column" && "isHovered",
                                            editable.id === containerId && "container-selected"
                                          )}
                                          {...containerDragProvided.draggableProps}
                                          ref={containerDragProvided.innerRef}
                                          onClick={() => mouseInPosition === "column" && setEditable({
                                            enable: true,
                                            type: "CONTAINER",
                                            id: containerId,
                                            index: containerIndex,
                                            parentId: columnId
                                          })}
                                        >
                                          <span className="popup-editor-container-label">CONTAINER</span>
                                          <div
                                            className={classnames("popup-editor-container-wrapper", containerDragSnapshot.isDragging && "isDragging")}
                                            style={containerStyle}
                                            onMouseEnter={() => setMouseInPosition("column")}
                                            onMouseLeave={() => setMouseInPosition("structure")}
                                          >
                                            {dndDragType === "CONTAINER" && <div 
                                              className={classnames("drop-it-here", "top")}
                                              onMouseUp={() => setDndOnDrag({
                                                ...dndOnDrag,
                                                dropPosition: "top",
                                                droppedId: containerId,
                                                droppedIndex: containerIndex,
                                                parentDropId: columnId
                                              })}
                                            >
                                              <div className="drop-it-here-border">
                                                  <span>Drop it Here</span>
                                              </div>
                                            </div>}
                                            <span
                                              className={classnames("column-option move", elementCount === 0 && "hidden")}
                                              {...containerDragProvided.dragHandleProps}
                                            >
                                                <i className="material-icons">control_camera</i>
                                            </span>
                                            <CanvasElement containerId={containerId} parentColumnId={columnId}/>
                                            {dndDragType === "CONTAINER" && <div
                                              className={classnames("drop-it-here", "bottom")}
                                              onMouseUp={() => setDndOnDrag({
                                                ...dndOnDrag,
                                                dropPosition: "bottom",
                                                droppedId: containerId,
                                                droppedIndex: containerIndex,
                                                parentDropId: columnId
                                              })}
                                            >
                                              <div className="drop-it-here-border">
                                                  <span>Drop it Here</span>
                                              </div>
                                            </div>}
                                          </div>
                                        </div>
                                      </React.Fragment>
                                    )}
                                  </Draggable>
                                );
                            })}
                          </div>
                        )}
                      </Droppable>
                    )
                  })
                }
              </div>
              {dndDragType === "STRUCTURE" && <div 
                className={classnames("drop-it-here", "bottom")}
                onMouseUp={() => setDndOnDrag({
                  ...dndOnDrag,
                  dropPosition: "bottom",
                  droppedId: structureId,
                  droppedIndex: structureIndex,
                  parentDropId: sectionId
                })}
              >
                <div className="drop-it-here-border">
                    <span>Drop it Here</span>
                </div>
              </div>}
            </div>
          </div>
        )}
      </Draggable>)
    })}
  </div>)
}

export default CanvasColumn;

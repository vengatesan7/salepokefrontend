import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const Timer = ({ elementId }) => {
    const { popupState } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);

    return (
        elementData !== null && <div className="timer" style={elementData.style}>
            <div className="time-part">
                <div className="time-value" style={_get(elementData, 'numberStyle', {})}>
                    00
                    {/* <span className="timer-dot">:</span> */}
                </div>
                <label
                    style={_get(elementData, 'textStyle', {})}
                    // style={{
                    //     fontSize: `${elementData.fontSize / 3}px`,
                    //     lineHeight: `${elementData.lineHeight / 3}px`
                    // }}
                >Days</label>
            </div>
            <div className="time-part">
                <div className="time-value" style={_get(elementData, 'numberStyle', {})}>
                    00
                    {/* <span className="timer-dot">:</span> */}
                </div>
                <label
                    style={_get(elementData, 'textStyle', {})}
                    // style={{
                    //     fontSize: `${elementData.fontSize / 3}px`,
                    //     lineHeight: `${elementData.lineHeight / 3}px`
                    // }}
                >Hours</label>
            </div>
            <div className="time-part">
                <div className="time-value" style={_get(elementData, 'numberStyle', {})}>
                    00
                    {/* <span className="timer-dot">:</span> */}
                </div>
                <label
                    style={_get(elementData, 'textStyle', {})}
                    // style={{
                    //     fontSize: `${elementData.fontSize / 3}px`,
                    //     lineHeight: `${elementData.lineHeight / 3}px`
                    // }}
                >Minutes</label>
            </div>
            <div className="time-part">
                <div className="time-value" style={_get(elementData, 'numberStyle', {})}>00</div>
                <label
                    style={_get(elementData, 'textStyle', {})}
                    // style={{
                    //     fontSize: `${elementData.fontSize / 3}px`,
                    //     lineHeight: `${elementData.lineHeight / 3}px`
                    // }}
                >Seconds</label>
            </div>
        </div>
    );
}

export default Timer;
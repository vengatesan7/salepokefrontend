import React, { useContext, useState, useEffect } from 'react';
import ContentEditable from "react-contenteditable";
import _get from "lodash.get";
import { PopupStore } from '../../../../store';

const SubscriptionForm = ({ elementId, tagName = "p" }) => {
    const {
        popupState,
        setPopupState
    } = useContext(PopupStore);

    const [elementData, setElementData] = useState(null);

    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);

    const handleChange = (name, value) => {
        setElementData({
            ...elementData,
            [name]: value
        });
        setPopupState({
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [elementId]: {
                        ...popupState.entities.element[elementId],
                        [name]: value
                    }
                }
            }
        });
    }

    return (
        <div className="popup-form" style={elementData && elementData.style}>
            <ContentEditable
                html={elementData && elementData.inputPlaceholder}
                disabled={false}
                tagName={tagName}
                onChange={e => handleChange('inputPlaceholder', e.target.value)}
                className="popup-form-input"
                style={elementData && { ...elementData.inputStyle, borderRadius: elementData.formStyle.borderRadius }}
            />
            <ContentEditable
                html={elementData && elementData.buttonText}
                disabled={false}
                tagName={"button"}
                onChange={e => handleChange('buttonText', e.target.value)}
                className="popup-form-submit"
                style={elementData && {
                    ...elementData.buttonStyle,
                    borderRadius: elementData.formStyle.borderRadius,
                    width: elementData.buttonSize === 'fittotext' ? 'auto' : '100%'
                }}
            />
        </div>
    );
}

export default SubscriptionForm;
import React, { useState, useEffect, useContext } from 'react';
import ContentEditable from "react-contenteditable";
import sanitizeHtml from "sanitize-html";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';
import EditorConvertToHTML from '../../../../../components/wysiwygText';

const ButtonElement = ({ elementId, tagName = "span" }) => {
    const {
        popupState,
        setPopupState,
        fontStyle,
        setFontStyle,
        selectedElement
    } = useContext(PopupStore);
    const [elementData, setElementData] = useState(popupState.entities.element[elementId]);

    // const sanitizeConf = {
    //     allowedTags: ["b", "i", "em", "strong", "a", "p", "h1","u", "strike", "sup", "sub", "div", "ul", "ol", "li", "font", "br", "span"],
    //     allowedAttributes: { a: ["href"], font: ["color", "size", "face"], span: ["style"] }
    // };

    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);

    const handleUpdateState = (value) => {
        setElementData({
            ...elementData,
            content: value
        });
        setPopupState({
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [elementId]: {
                        ...popupState.entities.element[elementId],
                        content: value
                    }
                }
            }
        });
    }

    // const handleChange = (value) => {
    //     setElementData({
    //         ...elementData,
    //         content: value
    //     });
    // }

    // const findStyle = () => {
    //     setFontStyle({
    //         ...fontStyle,
    //         bold: document.queryCommandState ("bold"),
    //         italic: document.queryCommandState ("italic"),
    //         underline: document.queryCommandState ("underline")
    //     })
    // }

    return (
        <span dangerouslySetInnerHTML={{__html: elementData.content}} style={elementData && {...elementData.buttonStyle, cursor: 'default' }} />
        // {/* <span onBlur={() => handleUpdateState()}>
        //     <ContentEditable
        //         html={elementData && elementData.content}
        //         style={elementData && elementData.buttonStyle}
        //         disabled={selectedElement === null || selectedElement !== elementId}
        //         tagName={tagName}
        //         onChange={e => setElementData({
        //             ...elementData,
        //             content: e.target.value
        //         })}
        //         onClick={e => findStyle()}
        //         onKeyDown={e => findStyle()}
        //     />
        // </span> */}
    )
}

export default ButtonElement;
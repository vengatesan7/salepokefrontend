import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const Icons = ({ elementId }) => {
    const { popupState } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);

    const style = () => {
        console.log(`${_get(elementData.style, 'lineHeight', elementData.style.fontSize)}px`)
        return {
            ...elementData.style,
            lineHeight: `${_get(elementData.style, 'lineHeight', elementData.style.fontSize)}px`
        }
    }

    return (
        elementData && <div className="rc-icon-widget" style={style()}>
            <i className={`fa ${elementData.contents}`}></i>
        </div>
    );
}

export default Icons;
import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const LineElement = ({ state, elementId }) => {
    const { popupState } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);
    return (
        elementData !== null && <hr style={ elementData.style } />
    )
}

export default LineElement;
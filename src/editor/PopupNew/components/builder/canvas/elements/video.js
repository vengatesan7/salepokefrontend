import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';
import { YoutubeThumb } from '../../../../../components/YouTubeThumb';

const VideoElement = ({ elementId }) => {
    const { popupState } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);

    const width = _get(elementData, 'width', null) === null ? "auto" : `${elementData.width}%`;
    const height = _get(elementData, 'height', null);
    return (
        elementData !== null && (elementData.url !== "" ? <React.Fragment>
            {/* <div style={elementData.style}
            className="video"
            dangerouslySetInnerHTML={{ __html: elementData.embed }} /> */}
            {/* <a href={elementData.url} target="_blank"> */}
            <span>
                <img src={YoutubeThumb(elementData.url, 'big')} />
            </span>
            {/* </a> */}
        </React.Fragment> : <div style={{ ...elementData.style, textAlign: "center", backgroundColor: "#ffffff" }} >
                <img
                    src="https://icon-library.net/images/video-png-icon/video-png-icon-16.jpg"
                    style={{ width: "100%", height: "100%", maxWidth: "100px", }}
                />
            </div>)
    );
}

export default VideoElement;
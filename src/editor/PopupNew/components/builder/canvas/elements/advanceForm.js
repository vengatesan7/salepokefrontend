import React from "react";
import { FormGroup, Form, Row, FormLabel, InputGroup, Col } from "react-bootstrap";
import classnames from "classnames";
import _get from "lodash.get";
import findInput from "../../../../../Form/fieldInput";

const AdvanceForm = ({ elementData }) => {    
    return <Form>
        {elementData.rows.map((rowData, rowIndex) => {
            return <Row id={rowData.id} className="m-0">
                {rowData.cols.map((colData, colId) => {
                    return <Col
                        id={colData.id}
                        style={_get(colData, 'style', {})}
                        xs={_get(colData, 'size', null)}
                    >
                        {colData.elements.map((eleData, eleId) => {
                            return <React.Fragment>
                                {eleData.fieldType === "submit" ? <div style={eleData.buttonCol}>
                                    <span className={classnames("btn", `btn-${eleData.color}`)} style={eleData.buttonStyle}>
                                        {eleData.buttonText}
                                    </span>
                                </div> : <FormGroup className="row m-0">
                                    {_get(eleData, 'isLabel', false) && <Col
                                        style={_get(eleData, 'labelCol', {})}
                                        xs={_get(eleData, 'labelSizes', null)}
                                    >
                                        <FormLabel htmlFor={eleData.id}>
                                            {eleData.label}<em style={{color:'red'}}>{eleData.isRequired ? '*' : '' }</em>
                                        </FormLabel>
                                    </Col>}
                                    <Col
                                        xs={_get(eleData, 'fieldSizes', null)}
                                        style={_get(eleData, 'controlCol', {})}
                                    >
                                        {findInput(eleData)}
                                        {/* {_get(eleData, 'prepend', false) ? <InputGroup>
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">
                                                    {_get(eleData, 'prependIcon', null) !== null && <i className={eleData.prependIcon} />}
                                                    {_get(eleData, 'prependText', '')}
                                                </span>
                                            </div>
                                            <span className="form-control"
                                                style={{
                                                    textOverflow: 'ellipsis',
                                                    whiteSpace: 'nowrap',
                                                    overflow: 'hidden'
                                                }}
                                            >
                                                {eleData.placeholder}
                                            </span>
                                        </InputGroup> : <span className="form-control"
                                            style={{
                                                textOverflow: 'ellipsis',
                                                whiteSpace: 'nowrap',
                                                overflow: 'hidden'
                                            }}
                                        >
                                            {eleData.placeholder}
                                        </span>} */}
                                    </Col>
                                </FormGroup>}
                            </React.Fragment>
                        })}
                    </Col>
                })}
            </Row>
        })}
    </Form>
}

export default AdvanceForm
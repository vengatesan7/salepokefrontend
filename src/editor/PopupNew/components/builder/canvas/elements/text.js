import React, { useState, useEffect, useContext } from 'react';
import ContentEditable from "react-contenteditable";
import sanitizeHtml from "sanitize-html";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';
import EditorConvertToHTML from '../../../../../components/wysiwygText';

const TextElement = ({ elementId, tagName = "p" }) => {
    const {
        popupState,
        setPopupState,
        fontStyle,
        setFontStyle,
        selectedElement,
        setSelectedElement,
        setEditable,
        setPropertiesDropDowns,
        setPropertyBarActive,
        propertyBarActive,
        editable
    } = useContext(PopupStore);
    const [elementData, setElementData] = useState(popupState.entities.element[elementId]);
    const [thisSelectedElement, setThisSelectedElement] = useState(selectedElement);
    
    useEffect(() => {
        setThisSelectedElement(selectedElement);
    }, [selectedElement]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);
    useEffect(() => {
        if (thisSelectedElement === null) {
            setPropertyBarActive(false);
            setSelectedElement(null);
            setEditable({
                enable: false,
                type: null,
                id: null,
                index: null,
                parentId: null
            });
        }
    }, [thisSelectedElement])

    // const sanitizeConf = {
    //     allowedTags: ["b", "i", "em", "strong", "a", "p", "h1","u", "strike", "sup", "sub", "div", "ul", "ol", "li", "font", "br", "span"],
    //     allowedAttributes: { a: ["href"], font: ["color", "size", "face"], span: ["style"] }
    // };

    // useEffect(() => {
    //     setElementData(popupState.entities.element[elementId]);
    // }, [popupState]);

    // const handleUpdateState = () => {
    //     const newContent = sanitizeHtml(elementData.content, sanitizeConf);
    //     setPropertyBarActive(false);
    //     setSelectedElement(null);
    //     setEditable({
    //         enable: false,
    //         type: null,
    //         id: null,
    //         index: null,
    //         parentId: null
    //     });
    //     const contentUpdate = {
    //         ...popupState,
    //         entities: {
    //             ...popupState.entities,
    //             element: {
    //                 ...popupState.entities.element,
    //                 [elementId]: {
    //                     ...popupState.entities.element[elementId],
    //                     content: newContent
    //                 }
    //             }
    //         }
    //     }

    //     return setPopupState(contentUpdate);
    // }

    // const handleChange = (value) => {
    //     setElementData({
    //         ...elementData,
    //         content: value
    //     });
    //     const newContent = sanitizeHtml(value, sanitizeConf);
    //     const contentUpdate = {
    //         ...popupState, 
    //         entities: {
    //             ...popupState.entities,
    //             element: {
    //                 ...popupState.entities.element,
    //                 [elementId]: {
    //                     ...popupState.entities.element[elementId],
    //                     content: newContent
    //                 }
    //             }
    //         }
    //     }

    //     return setPopupState(contentUpdate);
    // }

    // const findStyle = () => {
    //     setFontStyle({
    //         ...fontStyle,
    //         bold: document.queryCommandState ("bold"),
    //         italic: document.queryCommandState ("italic"),
    //         underline: document.queryCommandState ("underline")
    //     })
    // }

    // const handleRepeatOldData = () => {
    //     setElementData(popupState.entities.element[elementId]);
    //     setPropertyBarActive(false);
    //     setSelectedElement(null);
    //     setEditable({
    //         enable: false,
    //         type: null,
    //         id: null,
    //         index: null,
    //         parentId: null
    //     });
    // }

    const handleUpdateState = (value) => {
        setElementData({
            ...elementData,
            content: value
        });
        setThisSelectedElement(null);
        const contentUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [elementId]: {
                        ...popupState.entities.element[elementId],
                        content: value
                    }
                }
            }
        }

        return setPopupState(contentUpdate);
    }

    return (
        <React.Fragment>
            {/* {selectedElement === elementId && <div className="text-element action-buttons">
                <span className="action-buttons-clear" onMouseDown={() => handleRepeatOldData()}>
                    <i className="material-icons">
                        clear
                    </i>
                </span>
                <span className="action-buttons-save" onMouseDown={() => handleUpdateState()}>
                    <i className="material-icons">
                        done
                    </i>
                </span>
            </div>} */}
            <div className="text-element">
                {thisSelectedElement === elementId ? <React.Fragment>
                    <div className="property-bar-container" style={{ left: "0px" }} onMouseDown={() => handleUpdateState(elementData.content)}/>
                    <EditorConvertToHTML content={elementData.content} onSubmitCallBack={(e) => setElementData({...elementData, content: e})}/>
                </React.Fragment> :
                <p dangerouslySetInnerHTML={{__html: elementData.content}} style={{ cursor: 'default' }} />}
                {/* <p dangerouslySetInnerHTML={{__html: elementData.content}} /> */}
                {/* <ContentEditable
                    html={elementData && elementData.content}
                    disabled={selectedElement === null || selectedElement !== elementId}
                    tagName={tagName}
                    onChange={e => setElementData({
                        ...elementData,
                        content: e.target.value
                    })}
                    // onChange={e => handleChange(e.target.value)}
                    onClick={e => findStyle()}
                    onKeyDown={e => findStyle()}
                /> */}
            </div>
        </React.Fragment>
    )
}

export default TextElement;
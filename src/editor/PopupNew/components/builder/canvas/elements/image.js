import React, { useState, useEffect, useContext } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const ImageElement = ({ state, elementId }) => {
    const { popupState } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [elementId]);
    useEffect(() => {
        setElementData(popupState.entities.element[elementId]);
    }, [popupState]);
    return (
        elementData !== null && <React.Fragment>
            {elementData.url === null ?
                <div className="initial-sample-image">
                    <img src="https://i-love-png.com/images/img_234957_3642.png" width="50" />
                </div>
                :
                <img
                    src={popupState.entities.element[elementId].url}
                    style={{ width: `${elementData.style.width}%` }}
                    alt={popupState.entities.element[elementId].alt}
                />}
        </React.Fragment>
    )
}

export default ImageElement;
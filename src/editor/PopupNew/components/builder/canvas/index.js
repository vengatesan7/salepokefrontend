import React, { useContext, useEffect, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from "uuid";

import './canvas.scss';
import CanvasColumn from './column';
import PropertyBar from '../propertybar';

import { PopupStore } from '../../../store';
import EmailPreview from '../../preview';

const EmailEditorCanvas = () => {

    const {
        popupState,
        setPopupState,
        mouseInPosition,
        setMouseInPosition,
        isDndRuning,
        setDeleteAction,
        editable,
        setEditable
    } = useContext(PopupStore);
    
    const [newSectionPopup, setNewSectionPopup] = useState({
        enable: false,
        id: null
    });
    const [popupStyle, setPopupStyle] = useState(null);
    const [sectionOptionEnable, setSectionOptionEnable] = useState(false);
    useEffect(() => {
        if (popupState !== null) {
            const isFullPage = _get(popupState.result.popup, 'isFullPage', false);
            const getPopupStyle = _get(popupState.result.popup, 'popupStyle', {})
            const backgroundImageUrl = _get(popupState, 'result.popup.popupStyle.backgroundImage', '')
            const overAllStyle = isFullPage ? {
                width: _get(popupState, 'result.popup.popupStyle.width', ''),
                // height: _get(popupState, 'result.popup.popupStyle.height', ''),
            } : {
                ...getPopupStyle,
                backgroundImage: backgroundImageUrl !== '' && `url(${backgroundImageUrl})`,
            }
            setPopupStyle(overAllStyle)
            console.log("Popup state Json",popupState, JSON.stringify(popupState.result))
        }
    }, [popupState]);

    const addNewSection = (columnType) => {
        const newSectionArr = [...popupState.result.sections];
        const prevSecPosition = newSectionArr.indexOf(newSectionPopup.id);
        const newSectionId = uuid();
        newSectionArr.splice(prevSecPosition + 1, 0, newSectionId)
        const newStructureId = uuid();
        const firstColumnId = uuid();
        const firstContainerId = uuid();
        const secoundColumnId = uuid();
        const secoundContainerId = uuid();
        const thirdColumnId = uuid();
        const thirdContainerId = uuid();
        const fourthColumnId = uuid();
        const fourthContainerId = uuid();
        setNewSectionPopup({
            enable: false,
            id: null
        })
        switch (columnType) {
            case "esc_100":
                const esc100 = {
                    ...popupState,
                    result: {
                        ...popupState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...popupState.entities,
                        section: {
                            ...popupState.entities.section,
                            [newSectionId]: {
                                ...popupState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...popupState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId]
                            }
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 560,
                                containers: [firstContainerId]
                            }
                        },
                        containers: {
                            ...popupState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setPopupState(esc100);
            case "esc_50_50":
                const esc5050 = {
                    ...popupState,
                    result: {
                        ...popupState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...popupState.entities,
                        section: {
                            ...popupState.entities.section,
                            [newSectionId]: {
                                ...popupState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...popupState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId, secoundColumnId]
                            }
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 270,
                                containers: [firstContainerId]
                            },
                            [secoundColumnId]: {
                                id: secoundColumnId,
                                width: 270,
                                containers: [secoundContainerId]
                            }
                        },
                        containers: {
                            ...popupState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            },
                            [secoundContainerId]: {
                                id: secoundContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setPopupState(esc5050);
            case "esc_33_33_33":
                const esc333333 = {
                    ...popupState,
                    result: {
                        ...popupState.result,
                        sections: newSectionArr
                    },
                    entities: {
                        ...popupState.entities,
                        section: {
                            ...popupState.entities.section,
                            [newSectionId]: {
                                ...popupState.entities.section[newSectionId],
                                id: newSectionId,
                                structures: [newStructureId]
                            }
                        },
                        structures: {
                            ...popupState.entities.structures,
                            [newStructureId]: {
                                id: newStructureId,
                                columns: [firstColumnId, secoundColumnId, thirdColumnId]
                            }
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [firstColumnId]: {
                                id: firstColumnId,
                                width: 167,
                                containers: [firstContainerId]
                            },
                            [secoundColumnId]: {
                                id: secoundColumnId,
                                width: 167,
                                containers: [secoundContainerId]
                            },
                            [thirdColumnId]: {
                                id: thirdColumnId,
                                width: 167,
                                containers: [thirdContainerId]
                            }
                        },
                        containers: {
                            ...popupState.entities.containers,
                            [firstContainerId]: {
                                id: firstContainerId,
                                elements: []
                            },
                            [secoundContainerId]: {
                                id: secoundContainerId,
                                elements: []
                            },
                            [thirdContainerId]: {
                                id: thirdContainerId,
                                elements: []
                            }
                        }
                    }
                }
                return setPopupState(esc333333);
            default:
                break;
        }
    }

    return (
        <div className="main-content">
            <div className="popup-editor-container" style={popupStyle}>
                <Droppable droppableId="sectionDrop" type="SECTION">
                    {(sectionProvider, sectionSnapshot) => (
                        <div
                            className="popup-editor-content"
                            width="100%"
                            {...sectionProvider.droppableProps}
                            ref={sectionProvider.innerRef}
                            isDraggingOver={sectionSnapshot.isDraggingOver}
                        >
                            {popupState.result.sections.map((sectionId, sectionIndex) => {
                                const sectionStyleData = _get(popupState.entities.section[sectionId], "style", {});

                                const sectionStyle = {
                                    ...sectionStyleData,
                                    backgroundImage: sectionStyleData.backgroundImage !== undefined ? `url(${sectionStyleData.backgroundImage})`: ""
                                }
                                return (
                                    <Draggable draggableId={sectionId} index={sectionIndex}>
                                        {(sectionDragProvided, sectionSnapshot) => (
                                            <div
                                                className={
                                                    classnames(
                                                        "popup-editor-section",
                                                        sectionSnapshot.isDragging && "isDragging",
                                                        // !isDndRuning && mouseInPosition ==="section" && "isHovered",
                                                        editable.id === sectionId && "section-selected"
                                                    )}
                                                {...sectionDragProvided.draggableProps}
                                                ref={sectionDragProvided.innerRef}
                                                // onClick={() => mouseInPosition === "section" && setEditable({
                                                //     enable: true,
                                                //     type: "SECTION",
                                                //     id: sectionId,
                                                //     index: sectionIndex,
                                                //     parentId: null
                                                // })}
                                            >
                                                <div
                                                    className="popup-editor-section-wrapper"
                                                    // style={sectionStyle}
                                                    onMouseEnter={() => setMouseInPosition("section")}
                                                >
                                                    {/* {!sectionSnapshot.isDragging && <React.Fragment> */}
                                                        <Droppable droppableId={sectionId} type="STRUCTURE">
                                                            {(structureProvider, structureSnapshot) => (
                                                                <div
                                                                    className={classnames("popup-editor-structure-wrapper", sectionSnapshot.isDragging && "isDragging")}
                                                                    {...structureProvider.droppableProps}
                                                                    ref={structureProvider.innerRef}
                                                                    isDraggingOver={structureSnapshot.isDraggingOver}
                                                                    style={{ margin: "0 auto", width: '100%' }}
                                                                >
                                                                    <CanvasColumn sectionId={sectionId} />
                                                                </div>
                                                            )}
                                                        </Droppable>
                                                    {/* </React.Fragment>} */}
                                                </div>
                                            </div>
                                        )}
                                    </Draggable>
                                );
                            })}
                        </div>
                    )}
                </Droppable>
            </div>
            {/* <EmailPreview /> */}
            {/* {newSectionPopup.enable && <div 
                    style={{
                        position: "fixed",
                        top: "0",
                        width: "100vw",
                        height: "100vh",
                        background: "transparent"
                    }}
                    onMouseDown={() => setNewSectionPopup({
                        enable: false,
                        id: null
                    })}
                >
            </div>} */}
        </div>
    );
}

export default EmailEditorCanvas;
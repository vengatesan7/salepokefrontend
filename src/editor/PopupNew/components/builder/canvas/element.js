import React, { useContext, useEffect, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from "uuid";

import TextElement from './elements/text';
import ImageElement from './elements/image';
import { PopupStore } from '../../../store';
import ButtonElement from './elements/button';
import VideoElement from './elements/video';
import SpaceElement from './elements/space';
import LineElement from './elements/line';
import SubscriptionForm from './elements/subscriptionform';
import Timer from './elements/timer';
import Icons from './elements/icon';
import AdvanceForm from './elements/advanceForm';

const Element = ({ elementId, elementData, elementType, style, isSelected, isDragging }) => {
    const { popupState } = useContext(PopupStore);
    const elementDomValue = (type) => {
        switch (type) {
            case "TEXT":
                return <TextElement elementId={elementId} />;
            case "IMAGE":
                return <ImageElement elementId={elementId} />;
            case "BUTTON":
                return <ButtonElement elementId={elementId} />;
            case "VIDEO":
                return <VideoElement elementId={elementId} />;
            case "SPACE":
                return <SpaceElement elementId={elementId} />;
            case "LINE":
                return <LineElement elementId={elementId} />;
            case "SUBSCRIPTIONFROM":
                return <SubscriptionForm elementId={elementId} />;
            case "TIMER":
                return <Timer elementId={elementId} />;
            case "ICON":
                return <Icons elementId={elementId} />;
            case "FORM":
                return elementData !== undefined && <AdvanceForm elementData={elementData} />
            default:
                break;
        }
    }

    const thisStyle = () => {
        switch (elementType) {
            case "TEXT":
                return addBackgroundImage(style);
            case "IMAGE":
                const imageStyle = {...addBackgroundImage(style), width: "auto"}
                return imageStyle;
            case "BUTTON":
                const buttonStyle = _get(popupState.entities.element[elementId], "buttonStyle", {textAlign: "center"});
                return {...addBackgroundImage(style), textAlign: buttonStyle.textAlign};
            case "LINE":
                return;
            case "SPACE":
                return;
            case "ICON":
                return;
            case "TIMER":
                return;
            case "FORM":
                const fontFamily = _get(popupState.entities.element[elementId], "fontFamily", {fontFamily: "inherit"});
                const fontSize   = _get(popupState.entities.element[elementId], "fontSize", {fontSize: "inherit"});
                const lineHeight = _get(popupState.entities.element[elementId], "lineHeight", {lineHeight: "inherit"});
                const fontColor = _get(popupState.entities.element[elementId], "fontColor", {fontColor: "inherit"});
                return {...addBackgroundImage(style), fontFamily: fontFamily, fontSize: fontSize, lineHeight: lineHeight+"px", color: fontColor };
            default:
                return addBackgroundImage(style);
        }
    }
    const addBackgroundImage = (style) => {
        let updatedStyle = {}
        if (style !== undefined && style !== null) {
            updatedStyle = {
                ...style,
                backgroundImage: style.backgroundImage !== undefined && `url(${style.backgroundImage})`
            }
        }

        return updatedStyle;
    }

    return (
        <div style={isDragging ? {...thisStyle(), display: "none"} : thisStyle()} className={classnames(isSelected && "is-selected")}>
            {elementDomValue(elementType)}
        </div>
    );
}

const CanvasElement = ({ containerId, parentColumnId }) => {
    const {
        popupState,
        setPopupState,
        setSelectedElement,
        selectedElement,
        mouseInPosition,
        setMouseInPosition,
        parentIDs,
        setParentIDs,
        isDndRuning,
        dndDragType,
        setDropPosition,
        setDeleteAction,
        dndOnDrag,
        setDndOnDrag,
        setEditable,
        editable
    } = useContext(PopupStore);

    useEffect(() => {
        setSelectedElement(editable.id);
    }, [editable])

    const elementsArray = popupState.entities.containers[containerId].elements;

    const handleDuplicate = (id, index) => {
        const newContainerArr = [...popupState.entities.containers[containerId].elements];
        const newElementId = uuid();
        newContainerArr.splice(index + 1, 0, newElementId);
        const duplicatedData = {
            ...popupState,
            entities: {
                ...popupState.entities,
                containers: {
                    ...popupState.entities.containers,
                    [containerId]: {
                        ...popupState.entities.containers[containerId],
                        elements: newContainerArr
                    }
                },
                element: {
                    ...popupState.entities.element,
                    [newElementId]: {
                        ...popupState.entities.element[id],
                        id: newElementId
                    }
                }
            }
        }
        return setPopupState(duplicatedData);
    }

    const handleSelectElement = (id, index, parentId) => {
        setSelectedElement(id);
        setEditable({
            enable: true,
            type: "ELEMENT",
            id: id,
            index: index,
            parentId: parentId
        })
    }
    
    return (
        <div width="100%">
            <Droppable droppableId={containerId} type="ELEMENT">
                {(containerProvider, containerSnapshot) => (
                    <div
                        className={classnames("popup-editor-element-wapper", containerSnapshot.isDraggingOver && "on-dragging")}
                        {...containerProvider.droppableProps}
                        ref={containerProvider.innerRef}
                        isDraggingOver={containerSnapshot.isDraggingOver}
                    >
                        {elementsArray.length !== 0 ? elementsArray.map((elementId, elementIndex) => {
                            const elementStyle = _get(popupState.entities.element[elementId], "style", null);
                            popupState.entities.element[elementId].type === "FORM" && console.log(popupState.entities.element[elementId]);
                            return (
                                <Draggable draggableId={elementId} index={elementIndex}>
                                        {(elementDragProvided, elementDragSnapshot) => (
                                        <React.Fragment>
                                            <div
                                                className={
                                                    classnames(
                                                        "popup-editor-element",
                                                        elementDragSnapshot.isDragging && "isDragging",
                                                        !editable.enable && !isDndRuning && mouseInPosition === "element" && "isHovered"
                                                    )}
                                                {...elementDragProvided.draggableProps}
                                                ref={elementDragProvided.innerRef}
                                            >
                                                <span className="popup-editor-element-label">ELEMENT</span>
                                                <div
                                                    className={classnames("popup-editor-element-wrapper", elementDragSnapshot.isDragging && "isDragging")}
                                                    onClick={() => handleSelectElement(elementId, elementIndex, containerId)}
                                                    onMouseEnter={() => setMouseInPosition("element")}
                                                    onMouseLeave={() => setMouseInPosition("column")}
                                                >
                                                    {dndDragType === "ELEMENT" && <div
                                                        className={classnames("drop-it-here", "top")}
                                                        onMouseUp={() => setDndOnDrag({
                                                            ...dndOnDrag,
                                                            dropPosition: "top",
                                                            droppedId: elementId,
                                                            droppedIndex: elementIndex,
                                                            parentDropId: containerId
                                                        })}
                                                    >
                                                        <div className="drop-it-here-border">
                                                            <span>Drop it Here</span>
                                                        </div>
                                                    </div>}
                                                    <div className={classnames("element-option", selectedElement === elementId && "hidden")}>
                                                        <span
                                                            className="move"
                                                            {...elementDragProvided.dragHandleProps}
                                                            onMouseDownCapture ={() => setParentIDs({...parentIDs, column: parentColumnId})}
                                                        >
                                                            <i className="material-icons">control_camera</i>
                                                        </span>
                                                        {/* <span
                                                            className="edit"
                                                        >
                                                            <i className="material-icons">edit</i>
                                                        </span> */}
                                                    </div>
                                                    
                                                    <Element
                                                        elementId={elementId}
                                                        elementData={popupState.entities.element[elementId]}
                                                        elementType={popupState.entities.element[elementId].type}
                                                        style={elementStyle}
                                                        isSelected={selectedElement === elementId}
                                                        isDragging={elementDragSnapshot.isDragging}
                                                    />
                                                    {selectedElement === elementId && <div className="element-edit-option">
                                                        <div 
                                                            className="button"
                                                            onClick={() => setDeleteAction({
                                                                enable: true,
                                                                type: "ELEMENT",
                                                                id: elementId,
                                                                index: elementIndex,
                                                                parentIDs: containerId
                                                            })}
                                                        >
                                                            <i className="material-icons">delete</i>
                                                        </div>
                                                        <div
                                                            className="button"
                                                            onClick={() => handleDuplicate(elementId, elementIndex)}
                                                        >
                                                            <i className="material-icons">content_copy</i>
                                                        </div>
                                                    </div>}
                                                    {dndDragType === "ELEMENT" && <div
                                                        className={classnames("drop-it-here", "bottom")}
                                                        onMouseUp={() => setDndOnDrag({
                                                            ...dndOnDrag,
                                                            dropPosition: "bottom",
                                                            droppedId: elementId,
                                                            droppedIndex: elementIndex,
                                                            parentDropId: containerId
                                                        })}
                                                    >
                                                        <div className="drop-it-here-border">
                                                            <span>Drop it Here</span>
                                                        </div>
                                                    </div>}
                                                </div>
                                            </div>
                                            {elementDragSnapshot.isDragging && <div className={classnames("popup-editor-element")}>
                                                <div
                                                    style={{
                                                        backgroundColor: "white",
                                                    }}
                                                    className="popup-editor-element-wrapper"
                                                >
                                                    {/* <div className="element-option">
                                                        <span
                                                            className="move"
                                                            {...elementDragProvided.dragHandleProps}
                                                        >
                                                            <i className="material-icons">control_camera</i>
                                                        </span>
                                                    </div> */}
                                                    <Element
                                                        elementId={elementId}
                                                        elementType={popupState.entities.element[elementId].type}
                                                        style={elementStyle}
                                                        isSelected={selectedElement === elementId}
                                                    />
                                                </div>
                                            </div>}
                                        </React.Fragment>
                                        )}
                                </Draggable>
                            )
                        }) : <div style={{position: "relative"}}>
                                {dndDragType === "ELEMENT" && <div
                                    className={classnames("drop-it-here", "emptyElementArr")}
                                    onMouseUp={() => setDndOnDrag({
                                        ...dndOnDrag,
                                        dropPosition: "top",
                                        droppedId: "thisIsEmptyElementArray",
                                        droppedIndex: 0,
                                        parentDropId: containerId
                                    })}
                                >
                                    <div className="drop-it-here-border">
                                        <span>Drop it Here</span>
                                    </div>
                                </div>}
                                <div style={{ 
                                    textAlign: "center", 
                                    padding: "15px",
                                    border: "1px dashed gray",
                                    borderRadius: "4px",
                                    background: "white",
                                    fontSize: "12px"
                                }}>
                                    {/* Drop new widget here... */}
                                    <i className="fa fa-plus" />
                                </div>
                            </div>}
                    </div>
                )}
            </Droppable>
        </div>
    );
}

export default CanvasElement;
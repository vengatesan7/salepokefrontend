import React, { useState, useEffect, useContext } from 'react';
import { PopupStore } from '../../../../store';

const Video = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);

    const [blockData, setBlockData] = useState(null);

    useEffect(() => {
        setBlockData(popupState.entities.element[editable.id]);
    }, [editable]);

    const handleDataUpdate = (name, value) => {
        setBlockData({
            ...blockData,
            [name]: value
        })
        const elementStyleUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(elementStyleUpdate);
    }

    return (
        blockData !== null && <div className="properties-group">
            <div className="properties-group-name">
                VIDEO PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field">
                    <div className="properties-title">Video URL</div>
                    <div className="properties-value">
                        <input type="text" value={blockData.url} onChange={(e) => handleDataUpdate('url', e.target.value)}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Video;
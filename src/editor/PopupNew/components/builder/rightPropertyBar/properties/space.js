import React, { useState, useEffect, useContext } from 'react';

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';

const Space = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);

    const [blockData, setBlockData] = useState(null);

    useEffect(() => {
        setBlockData(popupState.entities.element[editable.id]);
    }, [editable]);

    const handleStyleUpdate = (name, value) => {
        setBlockData({
            ...blockData,
            style: {
                ...blockData.style,
                [name]: value
            }
        })
        const elementStyleUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        style: {
                            ...popupState.entities.element[editable.id].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(elementStyleUpdate);
    }

    return (
        blockData !== null && <div className="properties-group">
            <div className="properties-group-name">
                SPACE PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Height</div>
                    <div className="properties-value padding">
                        <SpinButtonInput
                            max={1000}
                            min={0}
                            step={5}
                            value={blockData.style.height}
                            callBack={(v) => handleStyleUpdate("height", v)}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Space;
import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from "classnames";
import _get from "lodash.get";
import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';
import AdvanceFormBuilder from "../../../../../Form";
import FormBuilder from '../../../../../Form';
const Form = () => {
    const {
        popupState,
        setPopupState,
        dispatch,
        editable,
        setFormBuilder
    } = useContext(PopupStore);
    const [actionUrl, setActionUrl]     = useState(null);
    const [formName, setFormName]       = useState(null);
    const [fontFamily, setFontFamily]   = useState(null);
    const [fontSize, setFontSize]       = useState(16);
    const [lineHeight, setLineHeight]   = useState(19);
    const [fontColor, setFontColor]     = useState('#000');
    useEffect(() => {
        setActionUrl(_get(popupState.entities.element[editable.id], "actionUrl", ""));
        setFormName(_get(popupState.entities.element[editable.id], "formName", ""));
        setFontFamily(_get(popupState.entities.element[editable.id], "fontFamily", ""));
        setFontSize(_get(popupState.entities.element[editable.id], "fontSize", ""));
        setLineHeight(_get(popupState.entities.element[editable.id], "lineHeight", ""));
        setFontColor(_get(popupState.entities.element[editable.id], "fontColor", ""));
    }, [editable]);    
    const [fontPopup, setFontPopup] = useState(false);
    const [elementData, setElementData] = useState(null);
    const [formPopup, setFormPopup] = useState(false);
    useEffect(() => {
        setElementData(popupState.entities.element[editable.id]);
    }, [editable]);
    const handleStyle = (name, value) => {
        setFormPopup(false);
        switch (name) {
            case "actionUrl":
                setActionUrl(value)
                break;
            case "formName":
                setFormName(value)
                break;
            case "fontFamily":
                setFontFamily(value);
                break;
            case "fontSize":
                setFontSize(value);
                break;
            case "lineHeight":
                setLineHeight(value);
                break;
            case "fontColor":
                setFontColor(value);
                break;
        }
        setElementData({
            ...elementData,
            [name]: value
        });
        const updatingForm = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(updatingForm);
    }
    
    const handleFormBuilderUpdate = (data) => {
        setElementData(data);
        const updatingForm = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        ...data
                    }
                }
            }
        }
        console.log(data, updatingForm);
        return setPopupState(updatingForm);
    }
    
    return (
        <React.Fragment>
            <div className="properties-group">
                <div class="property-form pb-15 pr-15 pl-15 pt-15">
                    <button class="edit-form-button" onClick={() => setFormPopup(true)} >Cutomize Your Form</button>
                </div>
               
            </div>
            <div className="properties-group">
                <div className="properties-group-name">
                    Form Setting
                </div>
                <div className="properties-group-fields">
                    <div className="row mb-20">
                        <div className="col-6">
                            <label for="formName">Form Name</label>
                        </div>
                        <div className="col-6">
                            <input
                                type="text"
                                name="formName"
                                id="formName"
                                className="form-control"
                                value={_get(elementData, 'formName', '')}
                                onChange={(e) => handleStyle('formName', e.target.value)}
                            />
                        </div>
                    </div>
                    <h6 className="mb-20">Form Action</h6>
                    <div className="row">
                        <div className="col-6">
                            <label for="actionUrl">Action URL</label>
                        </div>
                        <div className="col-6">
                            <input
                                type="text"
                                className="form-control"
                                name="actionUrl"
                                id="actionUrl"
                                value={_get(elementData, 'actionUrl', '')}
                                onChange={(e) => handleStyle('actionUrl', e.target.value)}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="properties-group">
                <div className="properties-group-name">
                    Font Settings
                </div>
                <div className="properties-group-fields">
                    <div className="row align-items-center mb-20">
                        <div className="col-6">
                            <label for="fontFamily">Font Family</label>
                        </div>
                        <div className="col-6">
                            <select
                                className="form-control"
                                value={_get(elementData, 'fontFamily', '')}
                                onChange={(e) => handleStyle('fontFamily', e.target.value)}
                            >
                                <option value="">Select Font Family</option>
                                <option value="Roboto">Roboto</option>
                                <option value="Open Sans">Open Sans</option>
                                <option value="Ubuntu">Ubuntu</option>
                                <option value="Josefin Sans">Josefin Sans</option>
                                <option value="Tajawal">Tajawal</option>
                                <option value="Raleway">Raleway</option>
                                <option value="Oxygen">Oxygen</option>
                                <option value="Maven Pro">Maven Pro</option>
                                <option value="Catamaran">Catamaran</option>
                                <option value="Orbitron">Orbitron</option>
                                <option value="Darker Grotesque">Darker Grotesque</option>
                                <option value="Montserrat Alternates">Montserrat Alternates</option> 
                                <option value="Nunito Sans"> Nunito Sans</option>
                                <option value="Noto Serif">Noto Serif</option>
                                <option value="Lato">Lato</option>
                                <option value="Roboto Slab"> Roboto Slab</option>
                                <option value="Montserrat"> Montserrat</option>
                                <option value="Anton">Anton</option>
                                <option value="Source Sans Pro">Source Sans Pro</option>
                                <option value="Roboto Condensed ">Roboto Condensed </option>
                                <option value="Oswald">Oswald</option>  
                                <option value="Nunito Sans">Nunito Sans</option>
                                <option value="Noto Sans JP"> Noto Sans JP </option>
                                <option value="Noto Sans"> Noto Sans </option>
                            </select>
                        </div>
                    </div>                    
                    <div className="row align-items-center justify-content-between mb-20">
                        <div className="col">
                            <label for="fontSize">Font Size</label>
                        </div>
                        <div className="col-auto">
                            <SpinButtonInput
                                max={40}
                                min={0}
                                step={1}
                                value={_get(elementData, 'fontSize', 16)}
                                callBack={(v) => handleStyle("fontSize", v)}
                            />
                        </div>
                    </div>
                    <div className="row align-items-center justify-content-between mb-20">
                        <div className="col">
                            <label for="fontSize">Line Height</label>
                        </div>
                        <div className="col-auto">
                            <SpinButtonInput
                                max={40}
                                min={0}
                                step={1}
                                value={_get(elementData, 'lineHeight', 19)}
                                callBack={(v) => handleStyle("lineHeight", v)}
                            />
                        </div>
                    </div>
                    <div className="row align-items-center justify-content-between">
                        <div className="col">
                            <div className="properties-field two-col">
                                <div className="properties-title"><label for="fontColor">Color</label></div>
                                <div className="properties-value" style={{height: "22px"}}>                                    
                                    <div className="color-picker-swatch" onClick={() => setFontPopup(true)}>
                                        <div 
                                            className="color-picker-color"
                                            style={{
                                                backgroundColor: fontColor
                                            }}>                                                    
                                        </div>
                                    </div>
                                    {fontPopup &&
                                        <div className="color-picker-popover">
                                            <div className='color-picker-cover' onClick={() => setFontPopup(false)} />
                                            <div className='color-picker-wrapper'>
                                                <ChromePicker color={fontColor} onChange={(e) => handleStyle("fontColor", e.hex)} disableAlpha />
                                                <button className='color-picker-button' onClick={() => setFontPopup(false)}>Ok</button>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>            
                        </div>
                    </div>
                </div>
            </div>
            {formPopup && <FormBuilder
                    isPopup
                    template={elementData}
                    close={() => setFormPopup(false)}
                    save={data => handleFormBuilderUpdate(data)}
                />}
        </React.Fragment>
    );
}

export default Form;
import React, { useState, useContext, useEffect } from 'react';
import classnames from 'classnames';

import ImageUpload from '../../../../../components/image-upload';
import { PopupStore } from '../../../../store';

const Image = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore)

    const [selectedData, setSelectedData] = useState(null);

    useEffect(() => {
        setSelectedData(popupState.entities.element[editable.id]);
    }, [editable.id]);

    const [uploadPopup, setUploadPopup] = useState(false);
    const handleElementData = (name, value) => {
        setUploadPopup(false);
        setSelectedData({
            ...selectedData,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    const handleStyle = (name, value) => {
        setSelectedData({
            ...selectedData,
            style: {
                ...selectedData.style,
                [name]: value
            }
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        style: {
                            ...popupState.entities.element[editable.id].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        selectedData !== null && <div className="properties-group">
            <div className="properties-group-name">
                IMAGE
            </div>
            <div className="properties-group-fields">
                <div className="properties-field">
                    <div className="properties-title">Image</div>
                    <div className="properties-value">
                        <div className="image-upload-button" onClick={() => setUploadPopup(true)}>Upload Image</div>
                        {uploadPopup && <ImageUpload imageLink={(e) => handleElementData('url', e)} popupClose={() => setUploadPopup(false)} />}
                    </div>
                    <div className="properties-title">Image URL</div>
                    <div className="properties-value">
                        <input type="text" value={selectedData.url} onChange={(e) => handleElementData('url', e.target.value)}/>
                    </div>
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Width</div>
                    <div className="properties-value flex-row alignments">
                        <select value={selectedData.style.width} onChange={(e) => handleStyle("width", e.target.value)}>
                            <option value="5">5%</option>
                            <option value="10">10%</option>
                            <option value="20">20%</option>
                            <option value="30">30%</option>
                            <option value="40">40%</option>
                            <option value="50">50%</option>
                            <option value="60">60%</option>
                            <option value="70">70%</option>
                            <option value="80">80%</option>
                            <option value="90">90%</option>
                            <option value="100">100%</option>
                        </select>
                    </div>
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Align</div>
                    <div className="properties-value flex-row alignments">
                        <button className={classnames(selectedData.style.textAlign === "left" && "active")} onClick={() => handleStyle("textAlign", "left")}>
                            <i className="material-icons">format_align_left</i>
                        </button>
                        <button className={classnames(selectedData.style.textAlign === "center" && "active")} onClick={() => handleStyle("textAlign", "center")}>
                            <i className="material-icons">format_align_center</i>
                        </button>
                        <button className={classnames(selectedData.style.textAlign === "right" && "active")} onClick={() => handleStyle("textAlign", "right")}>
                            <i className="material-icons">format_align_right</i>
                        </button>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="properties-title">Alternate Text</div>
                    <div className="properties-value">
                        <input type="text" value={selectedData.alt} onChange={(e) => handleElementData('alt', e.target.value)}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Image;
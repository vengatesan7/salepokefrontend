import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from 'classnames';
import Switch from "react-switch";
import _get from "lodash.get";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';
import { ColorLuminance } from '../../../../../components/ColorLuminance';
import { BorderStyle } from './utils';
import Popup from '../../../../../../components/popup'; 
import { iconList } from '../../../../utils/iconList';

const ColorPopup = ({color, handleCallBack}) => {
    const [popup, setPopup] = useState(false);
    return (
        <React.Fragment>
            <div className="color-picker-swatch" onClick={() => setPopup(true)}>
                <div 
                    className="color-picker-color"
                    style={{
                        backgroundColor: color
                    }}></div>
            </div>
            {popup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => setPopup(false)} />
                <div className='color-picker-wrapper'>
                    <ChromePicker color={color} onChange={(e) => handleCallBack(e.hex)} disableAlpha />
                    <button className='color-picker-button' onClick={() => setPopup(false)}>Ok</button>
                </div>
            </div>}
        </React.Fragment>
    );
}

const Icon = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);

    const [iconStyle, setIconStyle] = useState(null);
    const [iconPopup, setIconPopup] = useState(false);
    useEffect(() => {
        setIconStyle(_get(popupState.entities.element[editable.id], "style", {
            backgroundColor: "#ffffff"
        }));
    }, [editable.id]);

    const handleStyle = (name, value) => {
        setIconStyle({
            ...iconStyle,
            [name]: value
        });
        const newStyleUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        style: {
                            ...popupState.entities.element[editable.id].style,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newStyleUpdate);
    }

    const handleIconChange = (iconNmae) => {
        setIconPopup(false);
        const newIconUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        contents: iconNmae
                    }
                }
            }
        }
        return setPopupState(newIconUpdate);
    }

    return(
        iconStyle !== null && <div className="properties-group">
            <div className="properties-group-name">
                ICON PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field ">
                    {/* <div className="properties-title" style={{lineHeight: "34px"}}></div>
                    <div className="properties-value padding"></div> */}
                    <button className="btn btn-primary" onClick={() => setIconPopup(!iconPopup)}>Change Icon</button>
                    {iconPopup && <Popup
                        show
                        size="lg"
                        onHide={() => setIconPopup(false)}
                        closeButton
                        // heading='Select Your Image'
                        >
                        <div className="icon-popup-wrap">
                            {iconList.map((value, index) => {
                            return (
                                <div className='icon-popup-inner'>
                                <i
                                className={`fa ${value}`}
                                aria-hidden="true"
                                onClick={e => handleIconChange(value)}
                                ></i>
                                </div>
                            );
                            })}
                        </div>
                    </Popup>}
                </div>
                <div className="properties-field">
                    <div className="two-col" style={{marginBottom: "15px"}}>
                        <div className="properties-title">Icon Color</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <ColorPopup color={iconStyle.color} handleCallBack={(e) => handleStyle("color", e)} />
                        </div>
                    </div>
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Icon Size</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={500}
                                min={0}
                                step={1}
                                value={iconStyle.fontSize}
                                callBack={(v) => handleStyle("fontSize", v)}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Element Height</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={500}
                                min={0}
                                step={1}
                                value={_get(iconStyle, 'lineHeight', iconStyle.fontSize)}
                                callBack={(v) => handleStyle("lineHeight", v)}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Align</div>
                    <div className="properties-value flex-row alignments">
                        <button className={classnames(iconStyle.textAlign === "left" && "active")} onClick={() => handleStyle("textAlign", "left")}>
                            <i className="material-icons">format_align_left</i>
                        </button>
                        <button className={classnames(iconStyle.textAlign === "center" && "active")} onClick={() => handleStyle("textAlign", "center")}>
                            <i className="material-icons">format_align_center</i>
                        </button>
                        <button className={classnames(iconStyle.textAlign === "right" && "active")} onClick={() => handleStyle("textAlign", "right")}>
                            <i className="material-icons">format_align_right</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Icon;
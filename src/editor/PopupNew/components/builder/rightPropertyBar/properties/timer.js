import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from 'classnames';
import Switch from "react-switch";
import _get from "lodash.get";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';
import { ColorLuminance } from '../../../../../components/ColorLuminance';
import { BorderStyle } from './utils';
import { iconList } from '../../../../utils/iconList';

const ColorPopup = ({color, handleCallBack}) => {
    const [popup, setPopup] = useState(false);
    return (
        <React.Fragment>
            <div className="color-picker-swatch" onClick={() => setPopup(true)}>
                <div 
                    className="color-picker-color"
                    style={{
                        backgroundColor: color
                    }}></div>
            </div>
            {popup && <div className="color-picker-popover">
                <div className='color-picker-cover' onClick={() => setPopup(false)} />
                <div className='color-picker-wrapper'>
                    <ChromePicker color={color} onChange={(e) => handleCallBack(e.hex)} disableAlpha />
                    <button className='color-picker-button' onClick={() => setPopup(false)}>Ok</button>
                </div>
            </div>}
        </React.Fragment>
    );
}

const Timer = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);

    const [timerState, setTimerState] = useState(null);
    const [iconPopup, setIconPopup] = useState(false);
    useEffect(() => {
        setTimerState(popupState.entities.element[editable.id]);
    }, [editable.id]);

    const handleUpdate = (parent, name, value) => {
        const newUpate = {
            ...timerState,
            [parent]: {
                ...timerState[parent],
                [name]: value
            }
        }
        setTimerState(newUpate);
        const newDataUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...newUpate
                    }
                }
            }
        }
        return setPopupState(newDataUpdate);
    }

    return(
        timerState !== null && <div className="properties-group">
            {console.log(timerState)}
            <div className="properties-group-name">
                TIMER PROPERTIES
            </div>
            <div className="properties-group-fields">
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title">Status</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <Switch 
                                checked={_get(timerState.info, 'enable', false)}
                                onChange={(e) => handleUpdate('info', 'enable', e)}
                                onColor="#43da71" onHandleColor="#f8f8f8"
                                offColor="#c5c5c5" offHandleColor="#f8f8f8"
                                handleDiameter={12} height={16} width={30}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col" style={{marginBottom: "15px"}}>
                        <div className="properties-title">End Date</div>
                        <div className="properties-value">
                            <input
                                type="date"
                                onChange={(e) => handleUpdate('info', 'endDate', e.target.value)}
                                value={_get(timerState.info, 'endDate', '')}
                                disabled={!_get(timerState.info, 'enable', false)}
                                style={{
                                    height: '30px',
                                    padding: '5px 0px 5px 10px',
                                    width: '150px'
                                }}
                            />
                        </div>
                    </div>
                    <div className="two-col">
                        <div className="properties-title">End Time</div>
                        <div className="properties-value">
                            <input
                                type="time"
                                onChange={(e) => handleUpdate('info', 'endTime', e.target.value)}
                                value={_get(timerState.info, 'endTime', '')}
                                disabled={!_get(timerState.info, 'enable', false)}
                                style={{
                                    height: '30px',
                                    padding: '5px 0px 5px 10px',
                                    width: '150px'
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col" style={{marginBottom: "15px"}}>
                        <div className="properties-title">Numbers Color</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <ColorPopup color={_get(timerState.numberStyle, 'color', '#000000')} handleCallBack={(e) => handleUpdate('numberStyle', 'color', e)} />
                        </div>
                    </div>
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Numbers Size</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={500}
                                min={0}
                                step={1}
                                value={_get(timerState.numberStyle, 'fontSize', 16)}
                                callBack={(v) => handleUpdate('numberStyle', 'fontSize', v)}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col" style={{marginBottom: "15px"}}>
                        <div className="properties-title">Text Color</div>
                        <div className="properties-value" style={{height: "22px"}}>
                            <ColorPopup color={_get(timerState.textStyle, 'color', '#000000')} handleCallBack={(e) => handleUpdate('textStyle', 'color', e)} />
                        </div>
                    </div>
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Text Size</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={500}
                                min={0}
                                step={1}
                                value={_get(timerState.textStyle, 'fontSize', 16)}
                                callBack={(v) => handleUpdate('textStyle', 'fontSize', v)}
                            />
                        </div>
                    </div>
                </div>
                <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Direction</div>
                        <div className="properties-value flex-row alignments">
                            <button className={classnames(timerState.style.display === "flex" && "active")} onClick={() => handleUpdate("style", "display", "flex")}>
                                <i className="material-icons">more_horiz</i>
                            </button>
                            <button className={classnames(timerState.style.display === "block" && "active")} onClick={() => handleUpdate("style", "display", "block")}>
                                <i className="material-icons">more_vert</i>
                            </button>
                        </div>
                    </div>
                </div>
                {/* <div className="properties-field">
                    <div className="two-col">
                        <div className="properties-title" style={{lineHeight: "34px"}}>Element Height</div>
                        <div className="properties-value padding">
                            <SpinButtonInput
                                max={500}
                                min={0}
                                step={1}
                                value={_get(iconStyle, 'lineHeight', iconStyle.fontSize)}
                                callBack={(v) => handleStyle("lineHeight", v)}
                            />
                        </div>
                    </div>
                </div> */}
                {/* <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>Align</div>
                    <div className="properties-value flex-row alignments">
                        <button className={classnames(iconStyle.textAlign === "left" && "active")} onClick={() => handleStyle("textAlign", "left")}>
                            <i className="material-icons">format_align_left</i>
                        </button>
                        <button className={classnames(iconStyle.textAlign === "center" && "active")} onClick={() => handleStyle("textAlign", "center")}>
                            <i className="material-icons">format_align_center</i>
                        </button>
                        <button className={classnames(iconStyle.textAlign === "right" && "active")} onClick={() => handleStyle("textAlign", "right")}>
                            <i className="material-icons">format_align_right</i>
                        </button>
                    </div>
                </div> */}
            </div>
        </div>
    )
}

export default Timer;
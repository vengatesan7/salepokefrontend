import React, { useContext, useEffect, useState } from 'react';
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const ActionURL = () => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore);
    const [actionUrl, setActionUrl] = useState(null);

    useEffect(() => {
        setActionUrl(_get(popupState.entities.element[editable.id], "actionUrl", ""));
    }, [editable]);

    const handleStyle = (name, value) => {
        setActionUrl(value);
        const updatingActionUrl = {
            ...popupState,
            entities: {
                ...popupState.entities,
                element: {
                    ...popupState.entities.element,
                    [editable.id]: {
                        ...popupState.entities.element[editable.id],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(updatingActionUrl);
    }

    return (
        actionUrl !== null && <div className="properties-group">
            <div className="properties-group-name">
                ACTION URL
            </div>
            <div className="properties-group-fields">
                <div className="properties-field">
                    <div className="properties-title">URL</div>
                    <div className="properties-value">
                        <input
                            type="text"
                            value={actionUrl}
                            onChange={(e) => handleStyle('actionUrl', e.target.value)}
                            placeholder="https://example.com"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ActionURL;
import React, { useState, useContext, useEffect } from 'react';
import classnames from 'classnames';
import Switch from "react-switch";

import SpinButtonInput from "../../../../../components/SpinButtonInput";
import { PopupStore } from '../../../../store';

const Margin = ({ blockType }) => {
    const {
        popupState,
        setPopupState,
        editable
    } = useContext(PopupStore)

    const [selectedData, setSelectedData] = useState(null);
    const [allSideMargin, setAllSideMargin] = useState(false);


    useEffect(() => {
        const findThisData = (params) => {
            switch (blockType) {
                case "ELEMENT":
                    return popupState.entities.element[editable.id];
                case "SECTION":
                    return popupState.entities.section[editable.id];
                case "STRUCTURE":
                    return popupState.entities.structures[editable.id];
                case "CONTAINER":
                    return popupState.entities.containers[editable.id];
                default:
                    break;
            }
        }
        // const thisData = popupState.entities.element[editable.id];
        const thisData = findThisData();
        setSelectedData(thisData);
        if (thisData.style !== undefined) {
            if (thisData.style.marginTop !== undefined) {
                if (
                    thisData.style.marginTop !== thisData.style.marginRight || 
                    thisData.style.marginTop !== thisData.style.marginBottom ||
                    thisData.style.marginTop !== thisData.style.marginLeft
                ) {
                    setAllSideMargin(true);
                }
            } else {
                setSelectedData({
                    ...thisData,
                    style: {
                        ...thisData.style,
                        marginTop: 0,
                        marginRight: 0,
                        marginBottom: 0,
                        marginLeft: 0,
                    }
                });
            }
        } else {
            setSelectedData({
                ...thisData,
                style: {
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0,
                    marginLeft: 0,
                }
            });
        }
    }, [editable.id]);

    const handleStyle = (name, value) => {        
        setSelectedData({
            ...selectedData,
            style: {
                ...selectedData.style,
                [name]: value
            }
        });
        if (name === "MarginAllSide") {
            setSelectedData({
                ...selectedData,
                style: {
                    ...selectedData.style,
                    marginTop: value,
                    marginRight: value,
                    marginBottom: value,
                    marginLeft: value
                }
            });  
            console.log(selectedData);          
            switch (blockType) {                
                case "ELEMENT":
                    const newStyleUpdate = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            element: {
                                ...popupState.entities.element,
                                [editable.id]: {
                                    ...popupState.entities.element[editable.id],
                                    style: {
                                        ...popupState.entities.element[editable.id].style,
                                        marginTop: value,
                                        marginRight: value,
                                        marginBottom: value,
                                        marginLeft: value
                                    }
                                }
                            }
                        }
                    }                    
                    return setPopupState(newStyleUpdate);
                case "SECTION":
                    const newSectionStyleUpdate = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            section: {
                                ...popupState.entities.section,
                                [editable.id]: {
                                    ...popupState.entities.section[editable.id],
                                    style: {
                                        ...popupState.entities.section[editable.id].style,
                                        marginTop: value,
                                        marginRight: value,
                                        marginBottom: value,
                                        marginLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setPopupState(newSectionStyleUpdate);
                case "STRUCTURE":
                    const newStrutureStyleUpdate = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            structures: {
                                ...popupState.entities.structures,
                                [editable.id]: {
                                    ...popupState.entities.structures[editable.id],
                                    style: {
                                        ...popupState.entities.structures[editable.id].style,
                                        marginTop: value,
                                        marginRight: value,
                                        marginBottom: value,
                                        marginLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setPopupState(newStrutureStyleUpdate);
                case "CONTAINER":
                    const newContainerStyleUpdate = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            containers: {
                                ...popupState.entities.containers,
                                [editable.id]: {
                                    ...popupState.entities.containers[editable.id],
                                    style: {
                                        ...popupState.entities.containers[editable.id].style,
                                        marginTop: value,
                                        marginRight: value,
                                        marginBottom: value,
                                        marginLeft: value
                                    }
                                }
                            }
                        }
                    }
                    return setPopupState(newContainerStyleUpdate);
                default:
                    break;
            }
        }
        switch (blockType) {
            case "ELEMENT":
                const newStyleUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        element: {
                            ...popupState.entities.element,
                            [editable.id]: {
                                ...popupState.entities.element[editable.id],
                                style: {
                                    ...popupState.entities.element[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setPopupState(newStyleUpdate);
            case "SECTION":
                const newSectionStyleUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        section: {
                            ...popupState.entities.section,
                            [editable.id]: {
                                ...popupState.entities.section[editable.id],
                                style: {
                                    ...popupState.entities.section[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setPopupState(newSectionStyleUpdate);
            case "STRUCTURE":
                const newStructuresStyleUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        structures: {
                            ...popupState.entities.structures,
                            [editable.id]: {
                                ...popupState.entities.structures[editable.id],
                                style: {
                                    ...popupState.entities.structures[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setPopupState(newStructuresStyleUpdate);
            case "CONTAINER":
                const newContainersStyleUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        containers: {
                            ...popupState.entities.containers,
                            [editable.id]: {
                                ...popupState.entities.containers[editable.id],
                                style: {
                                    ...popupState.entities.containers[editable.id].style,
                                    [name]: value
                                }
                            }
                        }
                    }
                }
                return setPopupState(newContainersStyleUpdate);
            default:
                break;
        }
    }

    return(
        selectedData !== null && <div className="properties-group">
            <div className="properties-group-name">
                Margin
            </div>
            <div className="properties-group-fields">
                <div className="properties-field two-col">
                    <div className="properties-title">Margin</div>
                    <div className="properties-value">
                        <span style={{fontSize: "12px", marginRight: "10px", verticalAlign: "top"}}>More Option</span>
                        <Switch checked={allSideMargin} onChange={(e) => setAllSideMargin(e)}
                            onColor="#43da71" onHandleColor="#f8f8f8"
                            offColor="#c5c5c5" offHandleColor="#f8f8f8"
                            handleDiameter={12} height={16} width={30}
                        />
                    </div>
                </div>
                {!allSideMargin ? <div className="properties-field two-col">
                    <div className="properties-title" style={{lineHeight: "34px"}}>All Sides</div>
                    <div className="properties-value Margin">
                        <SpinButtonInput
                            max={100}
                            min={0}
                            step={1}
                            value={selectedData.style.marginTop}
                            callBack={(v) => handleStyle("MarginAllSide", v)}
                        />
                    </div>
                </div> :
                <div className="properties-field two-col">
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Margin Top</div>
                            <div className="properties-value Margin">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.marginTop}
                                    callBack={(v) => handleStyle("marginTop", v)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Margin Bottom</div>
                            <div className="properties-value Margin">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.marginBottom}
                                    callBack={(v) => handleStyle("marginBottom", v)}
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Margin Right</div>
                            <div className="properties-value Margin">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.marginRight}
                                    callBack={(v) => handleStyle("marginRight", v)}
                                />
                            </div>
                        </div>
                        <div>
                            <div className="properties-title" style={{lineHeight: "34px", textAlign: "center"}}>Margin Left</div>
                            <div className="properties-value Margin">
                                <SpinButtonInput
                                    max={100}
                                    min={0}
                                    step={1}
                                    value={selectedData.style.marginLeft}
                                    callBack={(v) => handleStyle("marginLeft", v)}
                                />
                            </div>
                        </div>
                    </div>
                </div>}
            </div>
        </div>
    )
}

export default Margin;
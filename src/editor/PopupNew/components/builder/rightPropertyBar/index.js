import React, { useContext } from 'react';
import classnames from 'classnames';

import './propertybar.scss';
import { PopupStore } from '../../../store';
import Border from './properties/border';
import Image from './properties/image';
import Padding from './properties/padding';
import Margin from './properties/margin';
import Background from './properties/background';
import Button from './properties/button';
import ActionURL from './properties/action';
import Video from './properties/video';
import Space from './properties/space';
import Line from './properties/line';
import Icon from './properties/icon';
import Timer from './properties/timer';
import Form from './properties/form';

const AllProperties = () => {
    const {
        popupState,
        setPopupState,
        setDeleteAction,
        editable,
        setEditable,
        setPropertyBarActive,
        setSelectedElement
    } = useContext(PopupStore);

    const elementProperties = () => {
        const elementType = popupState.entities.element[editable.id].type
        switch (elementType) {
            case "IMAGE":
                return <React.Fragment>
                    <Image />
                    <ActionURL />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "TEXT":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "FORM":
                return <React.Fragment>
                    <Form />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                    <Margin  blockType={editable.type} />
                </React.Fragment>;
            case "SUBSCRIPTIONFROM":
                return (
                    <React.Fragment>
                        {/* <SubscriptionFormProperties /> */}
                        <Border blockType={editable.type} />
                        <Padding  blockType={editable.type} />
                    </React.Fragment>
                );
            case "BUTTON":
                return <React.Fragment>
                    <Button />
                    <ActionURL />
                    <Background blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "VIDEO":
                return <React.Fragment>
                    <Video />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "SPACE":
                return <React.Fragment>
                    <Space />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                </React.Fragment>;
            case "LINE":
                return <React.Fragment>
                    <Line />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "TIMER":
                return <React.Fragment>
                    <Timer blockType={editable.type} />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            case "ICON":
                return <React.Fragment>
                    <Icon blockType={editable.type} />
                    <ActionURL />
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>;
            default:
                break;
        }
    }

    const propertiesByType = () => {
        switch (editable.type) {
            case "SECTION":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>
            case "STRUCTURE":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>
            case "ELEMENT":
                return elementProperties();
            case "CONTAINER":
                return <React.Fragment>
                    <Background blockType={editable.type} />
                    <Border blockType={editable.type} />
                    <Padding  blockType={editable.type} />
                </React.Fragment>
            default:
                break;
        }
    }

    const handleClose = () => {
        setPropertyBarActive(false);
        setSelectedElement(null);
        setEditable({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentId: null
        });
    }

    const handleCopyAction = () => {
        console.log('element-edit-option');
    }

    const breadcrumb = (type) => {
        const selectStyle = {
            color: '#007bff',
            cursor: 'pointer',
            fontWeight: 400,
            fontSize: '12px'
        }
        switch (type) {
            case "CONTAINER":
                let getContainerParentStructure = '';
                let getContainerParentSection = '';
                const numberOfStructure = Object.values(popupState.entities.structures);
                numberOfStructure.map(dataStructure => {
                    if (dataStructure.columns.includes(editable.parentId)) {
                        getContainerParentStructure = dataStructure.id;
                        const numberOfSection = Object.values(popupState.entities.section);
                        numberOfSection.map(dataSection => {
                            if (dataSection.structures.includes(dataStructure.id)) {
                                getContainerParentSection = dataSection.id;
                            }
                        })
                    }
                });
                return <React.Fragment>
                    <span style={selectStyle}
                        onClick={() => {
                            return setEditable({
                                enable: true,
                                type: "STRUCTURE",
                                id: getContainerParentStructure,
                                parentId: getContainerParentSection
                            });
                        }}
                    >STRUCTURE</span>
                    <i className="fa fa-angle-right" style={{padding: '0 5px'}} />
                    CONTAINER
                </React.Fragment>;
            case "ELEMENT":
                let getParentColumn = '';
                let getParentStructure = '';
                let getParentSection = '';
                const numberOfColumns = Object.values(popupState.entities.columns);
                numberOfColumns.map(columnData => {
                    if (columnData.containers.includes(editable.parentId)) {
                        getParentColumn = columnData.id;
                        const numberOfStructure = Object.values(popupState.entities.structures);
                        numberOfStructure.map(dataStructure => {
                            if (dataStructure.columns.includes(columnData.id)) {
                                getParentStructure = dataStructure.id;
                                const numberOfSection = Object.values(popupState.entities.section);
                                numberOfSection.map(dataSection => {
                                    if (dataSection.structures.includes(dataStructure.id)) {
                                        getParentSection = dataSection.id;
                                    }
                                })
                            }
                        });
                    }
                });
                return <React.Fragment>
                    <span style={selectStyle}
                        onClick={() => {
                            return setEditable({
                                enable: true,
                                type: "STRUCTURE",
                                id: getParentStructure,
                                parentId: getParentSection
                            });
                        }}
                    >STRUCTURE</span>
                    <i className="fa fa-angle-right" style={{padding: '0 5px'}} />
                    <span style={selectStyle}
                        onClick={() => {
                            return setEditable({
                                enable: true,
                                type: "CONTAINER",
                                id: editable.parentId,
                                parentId: getParentColumn
                            });
                        }}
                    >CONTAINER</span>
                    <i className="fa fa-angle-right" style={{padding: '0 5px'}} />
                    ELEMENT
                </React.Fragment>;
            default:
                return type;
        }
    }

    return (
        <React.Fragment>
            <div className={classnames("properties-options-header", editable.type === "SECTION" && "section-header")}>
                <div className="property-seleted">
                    {breadcrumb(editable.type)}
                </div>
                <div className="block-settings">
                    {editable.type === "SECTION" && <div className="button">
                        <i className="material-icons">save</i>
                    </div>}
                    <div
                        className="button"
                        onClick={() => setDeleteAction({
                            enable: true,
                            type: editable.type,
                            id: editable.id,
                            index: editable.index,
                            parentIDs: editable.parentId
                        })}
                    >
                        <i className="material-icons">delete</i>
                    </div>
                    <div className="button"
                        onClick={() => handleCopyAction()}
                    >
                        <i className="material-icons">content_copy</i>
                    </div>
                    <div className="button" onClick={() => handleClose()}
                    >
                        <i className="material-icons">close</i>
                    </div>
                </div>
            </div>
            <div className="properties-options-content">
                {propertiesByType()}
            </div>
        </React.Fragment>
    );
}

export default AllProperties;
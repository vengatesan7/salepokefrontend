import React, { useContext } from 'react';
import { DragDropContext } from "react-beautiful-dnd";
import _get from "lodash.get";
import uuid from "uuid";

import './builder.scss';
import EmailEditorCanvas from './canvas';
import EmailEditorMenuBar from './menubar';
import { PopupStore } from '../../store';
import Popup from "../../../../components/popup";
import {
    newSingleColumnStructure
} from "../../utils/popupDnd";
import PropertyBar from './propertybar';
import AllProperties from './rightPropertyBar';
import FormBuilder from '../../../Form';

const PopupEditorBuilder = () => {
    const {
        selectedElement,
        setSelectedElement,
        setIsNewElementDrag,
        popupState,
        setPopupState,
        parentIDs,
        setParentIDs,
        isDndRuning,
        setIsDndRuning,
        dndDragType,
        setDndDragType,
        dropPosition,
        editable,
        dndOnDrag,
        setDndOnDrag,
        deleteAction,
        setDeleteAction,
        setEditable,
        formBuilder
    } = useContext(PopupStore);
    const newElementJSON = (type, elementId) => {
        switch (type) {
            case "Text":
                const newText = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "TEXT",
                        content: "Text",
                        style: {
                            paddingTop: 15,
                            paddingRight: 15,
                            paddingBottom: 15,
                            paddingLeft: 15
                        }
                    }
                }
                return newText;
            case "Image":
                const newImage = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "IMAGE",
                        url: "http://www.wathannfilmfestival.com/modules/flexi/images/no-image.jpg",
                        alt: "Image",
                        style: {
                            width: 100,
                            textAlign: "center",
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0, 
                            paddingLeft: 0
                        }
                    }
                }
                return newImage;
            case "Form":
                const newForm  = {
                    ...popupState.entities.element,
                    [elementId]:{
                        id: elementId,
                        type: "FORM",
                        formName: "",
                        actionUrl: "",
                        fontFamily: "",
                        fontSize: "",
                        fontColor:"",
                        lineHeight:"",
                        style:{
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0, 
                            paddingLeft: 0,
                            marginTop: 0,
                            marginRight: 0,
                            marginBottom: 0, 
                            marginLeft: 0,
                            border:""
                        },
                        rows: [
                            {
                                id: uuid(),
                                style: {
                                    
                                },
                                cols: [
                                    {
                                        id: uuid(),
                                        size: 12,
                                        style: {
                                            paddingLeft: 0,
                                            paddingRight: 0,
                                            paddingTop: 0,
                                            paddingBottom: 0
                                        },
                                        elements: [
                                            {
                                                id: uuid(),
                                                label: "Email",
                                                isLabel: true,
                                                fieldType: "email",
                                                prependText: "Email",
                                                prepend: true,
                                                isRequired: true,
                                                placeholder: "example@domainname.com",
                                                fieldSizes: 14,
                                                labelSizes: 14,
                                                controlCol: {
                                                    paddingLeft: 0,
                                                    paddingRight: 0,
                                                    paddingTop: 0,
                                                    paddingBottom: 0
                                                },
                                                labelCol: {
                                                    paddingLeft: 0,
                                                    paddingRight: 0,
                                                    paddingTop: 0,
                                                    paddingBottom: 0
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                id: uuid(),
                                style: {},
                                isSubmit: true,
                                cols: [
                                    {
                                        id: uuid(),
                                        size: 12,
                                        style: {
                                            paddingLeft: 0,
                                            paddingRight: 0,
                                            paddingTop: 10,
                                            paddingBottom: 10
                                        },
                                        elements: [
                                            {
                                                id: uuid(),
                                                fieldType: "submit",
                                                buttonText: "Submit",
                                                fieldSizes: 12,
                                                color: "primary",
                                                buttonStyle: {
                                                    display: "inline-block",
                                                    paddingLeft: 15,
                                                    paddingRight: 15,
                                                    paddingTop: 5,
                                                    paddingBottom: 5
                                                },
                                                buttonCol: {
                                                    textAlign: "center"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }                          
                        ]
                    }
                };                
                return newForm;
            /*case "Form":
                console.log("text")
                const newSubscriptionForm = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "SUBSCRIPTIONFROM",
                        inputPlaceholder: "Enter your email here...",
                        buttonText: "Send My Coupon",
                        buttonSize: "fullwidth",
                        formStyle: {
                            borderRadius: 50,
                            paddingTop: 15,
                            paddingRight: 0,
                            paddingBottom: 15,
                            paddingLeft: 0
                        },
                        buttonStyle: {
                            textAlign: "center",
                            fontSize: 14,
                            color: "#ffffff",
                            backgroundColor: "#5d4eab",
                            borderWidth: 1,
                            borderStyle: "solid",
                            borderColor: "#5d4eab",
                            paddingTop: 10,
                            paddingRight: 10,
                            paddingBottom: 10,
                            paddingLeft: 10,
                        },
                        inputStyle: {
                            textAlign: "left",
                            fontSize: 14,
                            color: "#000000",
                            backgroundColor: "#ffffff",
                            borderWidth: 1,
                            borderStyle: "solid",
                            borderColor: "#cccccc",
                            paddingTop: 10,
                            paddingRight: 10,
                            paddingBottom: 10,
                            paddingLeft: 10,
                            marginBottom: 5
                        }
                    }
                }
                return newSubscriptionForm;*/
            case "Button":
                const newButton = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "BUTTON",
                        content: "Start Now",
                        buttonStyle: {
                            backgroundColor: "#ffffff",
                            borderBottomColor: "#000000",
                            borderBottomStyle: "solid",
                            borderBottomWidth: 1,
                            borderLeftColor: "#000000",
                            borderLeftStyle: "solid",
                            borderLeftWidth: 1,
                            borderRadius: 5,
                            borderRightColor: "#000000",
                            borderRightStyle: "solid",
                            borderRightWidth: 1,
                            borderTopColor: "#000000",
                            borderTopStyle: "solid",
                            borderTopWidth: 1,
                            color: "#000000",
                            display: "inline-block",
                            fontSize: 16,
                            paddingBottom: 10,
                            paddingLeft: 25,
                            paddingRight: 25,
                            paddingTop: 10,
                            textAlign: "center"
                        },
                        style: {
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0,
                            paddingLeft: 0
                        }
                    }
                }
                return newButton;
            case "Video": 
                const newVideo = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "VIDEO",
                        embed: "",
                        url: "https://youtu.be/Bg_tJvCA8zw",
                        // url: "https://www.youtube.com/watch?v=Bg_tJvCA8zw&feature=",
                        style: {
                            paddingTop: 0,
                            paddingRight: 0,
                            paddingBottom: 0,
                            paddingLeft: 0
                        }
                    }
                }
                return newVideo;
            case "Space": 
                const newSpace = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "SPACE",
                        style: {
                            height: 100,
                            borderRadius: 0
                        }
                    }
                }
                return newSpace;
            case "Line": 
                const newLine = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "LINE",
                        style: {
                            borderStyle: "solid",
                            borderRadius: 0,
                            borderWidth: 1,
                            borderColor: "black",
                        }
                    }
                }
                return newLine;
            case 'Timer':
                const date = new Date();
                const month = date.getMonth() + 1;
                const today = `${date.getFullYear().toString()}-${month.toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`
                const currentTime = `${date.getHours()}:${date.getMinutes().toString().padStart(2, '0')}`
            
                const newTimer = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "TIMER",
                        info: {
                            enable: true,
                            endDate: today,
                            endTime: currentTime
                        },
                        numberStyle: {
                            fontSize: 44,
                            color: '#000000'
                        },
                        textStyle: {
                            fontSize: 10,
                            color: "#eeeeee"
                        },
                        style: {
                            backgroundColor: "#ffffff",
                            borderBottomColor: "#000000",
                            borderBottomStyle: "solid",
                            borderBottomWidth: 1,
                            borderLeftColor: "#000000",
                            borderLeftStyle: "solid",
                            borderLeftWidth: 1,
                            borderRadius: 0,
                            borderRightColor: "#000000",
                            borderRightStyle: "solid",
                            borderRightWidth: 1,
                            borderTopColor: "#000000",
                            borderTopStyle: "solid",
                            borderTopWidth: 1,
                            color: "#000000",
                            display: "flex",
                            justifyContent: "space-around",
                            paddingBottom: 10,
                            paddingLeft: 25,
                            paddingRight: 25,
                            paddingTop: 10,
                            textAlign: "center"
                        }
                    }
                }
                return newTimer;
            case 'Icon':
                const newIcon = {
                    ...popupState.entities.element,
                    [elementId]: {
                        id: elementId,
                        type: "ICON",
                        contents:"fa-smile-o",
                        style: {
                            backgroundColor: "#ffffff",
                            borderBottomColor: "#000000",
                            borderBottomStyle: "solid",
                            borderBottomWidth: 1,
                            borderLeftColor: "#000000",
                            borderLeftStyle: "solid",
                            borderLeftWidth: 1,
                            borderRadius: 0,
                            borderRightColor: "#000000",
                            borderRightStyle: "solid",
                            borderRightWidth: 1,
                            borderTopColor: "#000000",
                            borderTopStyle: "solid",
                            borderTopWidth: 1,
                            color: "#000000",
                            fontSize: 36,
                            paddingBottom: 5,
                            paddingLeft: 15,
                            paddingRight: 15,
                            paddingTop: 5,
                            textAlign: "center",
                            width: '100%'
                        }
                    }
                }
                return newIcon;
            default:
                break;
        }
    }
    const containersBasicStyle = {
        borderBottomColor: "#000000",
        borderBottomStyle: "solid",
        borderBottomWidth: 0,
        borderLeftColor: "#000000",
        borderLeftStyle: "solid",
        borderLeftWidth: 0,
        borderRadius: 0,
        borderRightColor: "#000000",
        borderRightStyle: "solid",
        borderRightWidth: 0,
        borderTopColor: "#000000",
        borderTopStyle: "solid",
        borderTopWidth: 0
    }
    const containersBasicPadding = {
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 5
    }
    const popupOnDragEnd = (result) => {
        const { draggableId, destination, source, type } = result;
        console.log(draggableId, destination, source, type, dndOnDrag);
        setIsDndRuning(false);
        setDndDragType(null);
        setDndOnDrag({
            type: null,
            dropPosition: null,
            droppedId: null
        });
        // if (destination === null) {
        //     return;
        // }
        if (dndOnDrag.droppedId === null) {
            return;
        }
        switch (type) {
            case "SECTION":
                const newSectionArr = [...popupState.result.sections];
                newSectionArr.splice(source.index, 1);
                newSectionArr.splice(destination.index, 0, draggableId);
                const structureInSameArray = {
                    ...popupState,
                    result: {
                        ...popupState.result,
                        sections: newSectionArr
                    }
                }
                return setPopupState(structureInSameArray);
            case "CONTAINER":
                const newContainerArr = [...popupState.entities.columns[dndOnDrag.parentDropId].containers];
                if (dndOnDrag.parentDropId === source.droppableId) {
                    newContainerArr.splice(source.index, 1);
                    const newDroppedId = newContainerArr.indexOf(dndOnDrag.droppedId);
                    newContainerArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const containerSwapSameColumn = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            columns: {
                                ...popupState.entities.columns,
                                [dndOnDrag.parentDropId]: {
                                    ...popupState.entities.columns[dndOnDrag.parentDropId],
                                    containers: newContainerArr
                                }
                            }
                        }
                    }
                    return setPopupState(containerSwapSameColumn);
                } else {
                    const prevContainerArr = [...popupState.entities.columns[source.droppableId].containers];
                    prevContainerArr.splice(source.index, 1);
                    if (newContainerArr.length === 1) {
                        if (popupState.entities.containers[newContainerArr[0]].elements.length === 0) {
                            newContainerArr.splice(0, 1, draggableId);
                        } else {
                            newContainerArr.splice(dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1, 0, draggableId);
                        }
                    } else {
                        newContainerArr.splice(dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1, 0, draggableId);
                    }
                    if (prevContainerArr.length === 0) {
                        const newContainerId = uuid();
                        const containerSwapOtherColumn = {
                            ...popupState,
                            entities: {
                                ...popupState.entities,
                                columns: {
                                    ...popupState.entities.columns,
                                    [dndOnDrag.parentDropId]: {
                                        ...popupState.entities.columns[dndOnDrag.parentDropId],
                                        containers: newContainerArr
                                    },
                                    [source.droppableId]: {
                                        ...popupState.entities.columns[source.droppableId],
                                        containers: [newContainerId]
                                    }
                                },
                                containers: {
                                    ...popupState.entities.containers,
                                    [newContainerId]: {
                                        ...popupState.entities.containers[newContainerId],
                                        id: newContainerId,
                                        elements: [],
                                        style: {...containersBasicStyle,...containersBasicPadding}
                                    }
                                }
                            }
                        }
                        return setPopupState(containerSwapOtherColumn);
                    } else {
                        const containerSwapOtherColumn = {
                            ...popupState,
                            entities: {
                                ...popupState.entities,
                                columns: {
                                    ...popupState.entities.columns,
                                    [dndOnDrag.parentDropId]: {
                                        ...popupState.entities.columns[dndOnDrag.parentDropId],
                                        containers: newContainerArr
                                    },
                                    [source.droppableId]: {
                                        ...popupState.entities.columns[source.droppableId],
                                        containers: prevContainerArr
                                    }
                                }
                            }
                        }
                        return setPopupState(containerSwapOtherColumn);
                    }
                }
            case "ELEMENT":
                const newElementArr = [...popupState.entities.containers[dndOnDrag.parentDropId].elements];
                if (source.droppableId === "ITEMS") {
                    const newElementId = uuid();
                    const newDestination = dndOnDrag.dropPosition === "top" ? dndOnDrag.droppedIndex : dndOnDrag.droppedIndex + 1;
                    newElementArr.splice(newDestination, 0, newElementId);
                    const newElement = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            containers: {
                                ...popupState.entities.containers,
                                [dndOnDrag.parentDropId]: {
                                    ...popupState.entities.containers[dndOnDrag.parentDropId],
                                    elements: newElementArr
                                }
                            },
                            element: newElementJSON(draggableId, newElementId)
                        }
                    }
                    return setPopupState(newElement);
                } else {
                    if (source.droppableId === dndOnDrag.parentDropId) {
                        newElementArr.splice(source.index, 1);
                        const newDroppedId = newElementArr.indexOf(dndOnDrag.droppedId);
                        newElementArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                        const elementSwapSameContainer = {
                            ...popupState,
                            entities: {
                                ...popupState.entities,
                                containers: {
                                    ...popupState.entities.containers,
                                    [dndOnDrag.parentDropId]: {
                                        ...popupState.entities.containers[dndOnDrag.parentDropId],
                                        elements: newElementArr
                                    }
                                }
                            }
                        }
                        return setPopupState(elementSwapSameContainer);
                    } else {
                        const prevElementArr = [...popupState.entities.containers[source.droppableId].elements];
                        prevElementArr.splice(source.index, 1);
                        const newDroppedId = newElementArr.indexOf(dndOnDrag.droppedId);
                        newElementArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                        if(prevElementArr.length === 0) {
                            const parentContainerArr = [...popupState.entities.columns[parentIDs.column].containers];
                            if(parentContainerArr.length > 1) {
                                const containerPosition = parentContainerArr.indexOf(source.droppableId);
                                parentContainerArr.splice(containerPosition, 1);
                                const containerSwapOtherColumn = {
                                    ...popupState,
                                    entities: {
                                        ...popupState.entities,
                                        columns: {
                                            ...popupState.entities.columns,
                                            [parentIDs.column]: {
                                                ...popupState.entities.columns[parentIDs.column],
                                                containers: [parentContainerArr]
                                            }
                                        },
                                        containers: {
                                            ...popupState.entities.containers,
                                            [dndOnDrag.parentDropId]: {
                                                ...popupState.entities.containers[dndOnDrag.parentDropId],
                                                elements: newElementArr
                                            },
                                            [source.droppableId]: {
                                                ...popupState.entities.containers[source.droppableId],
                                                elements: prevElementArr
                                            }
                                        }
                                    }
                                }
                                return setPopupState(containerSwapOtherColumn);
                            }
                        }
                        const containerSwapOtherColumn = {
                            ...popupState,
                            entities: {
                                ...popupState.entities,
                                containers: {
                                    ...popupState.entities.containers,
                                    [dndOnDrag.parentDropId]: {
                                        ...popupState.entities.containers[dndOnDrag.parentDropId],
                                        elements: newElementArr
                                    },
                                    [source.droppableId]: {
                                        ...popupState.entities.containers[source.droppableId],
                                        elements: prevElementArr
                                    }
                                }
                            }
                        }
                        return setPopupState(containerSwapOtherColumn);
                    }
                }
                break;
            case "STRUCTURE":
                const newStructuresArr = [...popupState.entities.section[dndOnDrag.parentDropId].structures];
                if (source.droppableId === "LAYOUTS") {
                    const newStructureId = uuid();
                    const firstColumnId = uuid();
                    const firstContainerId = uuid();
                    const secoundColumnId = uuid();
                    const secoundContainerId = uuid();
                    const thirdColumnId = uuid();
                    const thirdContainerId = uuid();
                    const fourthColumnId = uuid();
                    const fourthContainerId = uuid();
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, newStructureId);
                    switch (draggableId) {
                        case "esc_100":
                            const esc100 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 100,
                                            containers: [firstContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc100);
                        case "esc_50_50":
                            const esc5050 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 50,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 50,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc5050);
                        case "esc_70_30":
                            const esc7030 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 70,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 30,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc7030);
                        case "esc_30_70":
                            const esc3070 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId, secoundColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 30,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 70,
                                            containers: [secoundContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc3070);
                        case "esc_33_33_33":
                            const esc333333 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId, secoundColumnId, thirdColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 33.33,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 33.33,
                                            containers: [secoundContainerId]
                                        },
                                        [thirdColumnId]: {
                                            id: thirdColumnId,
                                            width: 33.33,
                                            containers: [thirdContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [thirdContainerId]: {
                                            id: thirdContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc333333);
                        case "esc_25_25_25_25":
                            const esc25252525 = {
                                ...popupState,
                                entities: {
                                    ...popupState.entities,
                                    section: {
                                        ...popupState.entities.section,
                                        [dndOnDrag.parentDropId]: {
                                            ...popupState.entities.section[dndOnDrag.parentDropId],
                                            structures: newStructuresArr
                                        }
                                    },
                                    structures: {
                                        ...popupState.entities.structures,
                                        [newStructureId]: {
                                            id: newStructureId,
                                            style: {
                                                backgroundColor: "transparent",
                                                borderRadius: 15,
                                                paddingBottom: 5,
                                                paddingLeft: 5,
                                                paddingRight: 5,
                                                paddingTop: 5
                                            },
                                            columns: [firstColumnId, secoundColumnId, thirdColumnId, fourthColumnId]
                                        }
                                    },
                                    columns: {
                                        ...popupState.entities.columns,
                                        [firstColumnId]: {
                                            id: firstColumnId,
                                            width: 25,
                                            containers: [firstContainerId]
                                        },
                                        [secoundColumnId]: {
                                            id: secoundColumnId,
                                            width: 25,
                                            containers: [secoundContainerId]
                                        },
                                        [thirdColumnId]: {
                                            id: thirdColumnId,
                                            width: 25,
                                            containers: [thirdContainerId]
                                        },
                                        [fourthColumnId]: {
                                            id: fourthColumnId,
                                            width: 25,
                                            containers: [fourthContainerId]
                                        }
                                    },
                                    containers: {
                                        ...popupState.entities.containers,
                                        [firstContainerId]: {
                                            id: firstContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [secoundContainerId]: {
                                            id: secoundContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [thirdContainerId]: {
                                            id: thirdContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        },
                                        [fourthContainerId]: {
                                            id: fourthContainerId,
                                            elements: [],
                                            style: {...containersBasicStyle,...containersBasicPadding}
                                        }
                                    }
                                }
                            }
                            return setPopupState(esc25252525);
                        default:
                            break;
                    }
                    
                } else if (dndOnDrag.parentDropId === source.droppableId) {
                    newStructuresArr.splice(source.index, 1);
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const structureInSameArray = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            section: {
                                ...popupState.entities.section,
                                [dndOnDrag.parentDropId]: {
                                    ...popupState.entities.section[dndOnDrag.parentDropId],
                                    structures: newStructuresArr
                                }
                            },
                        }
                    }
                    return setPopupState(structureInSameArray);
                } else {
                    const prevStructuresArr = [...popupState.entities.section[source.droppableId].structures];
                    prevStructuresArr.splice(source.index, 1);
                    const sectionArr = [...popupState.result.sections];
                    if (prevStructuresArr.length === 0) {
                        const positionOfPrevSection = sectionArr.indexOf(source.droppableId);
                        sectionArr.splice(positionOfPrevSection, 1);
                    }
                    const newDroppedId = newStructuresArr.indexOf(dndOnDrag.droppedId);
                    newStructuresArr.splice(dndOnDrag.dropPosition === "top" ? newDroppedId : newDroppedId + 1, 0, draggableId);
                    const structureInSameArray = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            section: {
                                ...popupState.entities.section,
                                [dndOnDrag.parentDropId]: {
                                    ...popupState.entities.section[dndOnDrag.parentDropId],
                                    structures: newStructuresArr
                                },
                                [source.droppableId]: {
                                    ...popupState.entities.section[source.droppableId],
                                    structures: prevStructuresArr
                                }
                            },
                        },
                        result: {
                            sections: sectionArr
                        }
                    }
                    return setPopupState(structureInSameArray);
                }
            default:
                break;
        }
    }

    const popupOnDragStart = (result) => {
        const { source, draggableId, type } = result;
        setIsDndRuning(true);
        setDndDragType(type);
        setDndOnDrag({
            ...dndOnDrag,
            type: type
        })
        if (source.droppableId === "ITEMS") {
            return;
        }
    }

    const handleDelete = () => {
        setDeleteAction({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentIDs: null
        });
        setEditable({
            enable: false,
            type: null,
            id: null,
            index: null,
            parentId: null
        });
        setSelectedElement(null);
        switch (deleteAction.type) {
            case "STRUCTURE":
                const newStructureArr = [...popupState.entities.section[deleteAction.parentIDs].structures]
                newStructureArr.splice(deleteAction.index, 1);
                const deleteStructure = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        section: {
                            ...popupState.entities.section,
                            [deleteAction.parentIDs]: {
                                ...popupState.entities.section[deleteAction.parentIDs],
                                structures: newStructureArr
                            }
                        }
                    }
                }
                return setPopupState(deleteStructure);
            case "CONTAINER":
                const newContainerArr = [...popupState.entities.columns[deleteAction.parentIDs].containers]
                newContainerArr.splice(deleteAction.index, 1);
                if (newContainerArr.length === 0) {
                    const newContainerId = uuid();
                    const deleteContainerIfNoContainers = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            columns: {
                                ...popupState.entities.columns,
                                [deleteAction.parentIDs]: {
                                    ...popupState.entities.columns[deleteAction.parentIDs],
                                    containers: [newContainerId]
                                }
                            },
                            containers: {
                                ...popupState.entities.containers,
                                [newContainerId]: {
                                    id: newContainerId,
                                    elements: [],
                                    style: {...containersBasicStyle,...containersBasicPadding}
                                }
                            }
                        }
                    }
                    return setPopupState(deleteContainerIfNoContainers);   
                } else {
                    const deleteContainer = {
                        ...popupState,
                        entities: {
                            ...popupState.entities,
                            columns: {
                                ...popupState.entities.columns,
                                [deleteAction.parentIDs]: {
                                    ...popupState.entities.columns[deleteAction.parentIDs],
                                    containers: newContainerArr
                                }
                            }
                        }
                    }
                    return setPopupState(deleteContainer);
                }
            case "ELEMENT":
                const newElementArr = [...popupState.entities.containers[deleteAction.parentIDs].elements];
                newElementArr.splice(deleteAction.index, 1);
                const deletedElement = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        containers: {
                            ...popupState.entities.containers,
                            [deleteAction.parentIDs]: {
                                ...popupState.entities.containers[deleteAction.parentIDs],
                                elements: newElementArr
                            }
                        }
                    }
                }
                return setPopupState(deletedElement);
            case "SECTION": 
                const newSectionArr = [...popupState.result.sections];
                newSectionArr.splice(deleteAction.index, 1);
                const deletedSection = {
                    ...popupState,
                    result: {
                        ...popupState.result,
                        sections: newSectionArr
                    }
                }
                return setPopupState(deletedSection);
            default:
                break;
        }
    }

    const isFullPagePopup = () => {
        const isFullPage = _get(popupState, 'result.popup.isFullPage', false);
        const style = isFullPage ? {
            ...popupState.result.popup.popupStyle,
            backgroundImage: 'url(' + popupState.result.popup.popupStyle.backgroundImage + ')',
            width: 'auto'
        } : {}
        
        return style;
    }

    return (
        <React.Fragment>
            <DragDropContext
                onDragEnd={popupOnDragEnd}
                onDragStart={popupOnDragStart}
            >
                <Popup 
                    show={deleteAction.enable}
                    size='md' onHide={() => setDeleteAction({
                        enable: false,
                        type: null,
                        id: null,
                        index: null,
                        parentIDs: null
                    })}
                    onHandle={handleDelete} onHandleText='Delete' heading='Delete Warning'>
                    <p>Are you sure you want to delete this? This action cannot be undone.</p>
                </Popup>
                <div className="popup-editor-canvas" style={isFullPagePopup()}>
                    {selectedElement !== null && <PropertyBar />}
                    <EmailEditorCanvas />
                </div>
                <div className="popup-editor-right-menu">
                    <EmailEditorMenuBar />
                </div>
                {editable.enable && <div className="popup-editor-right-properties-bar">
                    <AllProperties />
                </div>}
            </DragDropContext>
        </React.Fragment>
    );
}

export default PopupEditorBuilder;
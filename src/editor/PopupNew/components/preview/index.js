import React, {useState, useEffect } from 'react';
import { normalize } from "normalizr";
import _get from "lodash.get";

import popupSchema from '../../store/schema';
import { YoutubeThumb } from '../../../components/YouTubeThumb';
import { ClipLoader } from 'react-spinners';
import Timer from './elements/timer';

const PopupPreview = ({ closePopup, dataSchema, loader }) => {

    const [popupState, setPopupState] = useState(null);

    useEffect(() => {
        dataSchema !== null && setPopupState(normalize(dataSchema, popupSchema));
    }, [dataSchema]);

    const elementStyle = (elementId, type, style) => {
        switch (type) {
            case "TEXT":
                return addBackgroundImage(style);
            case "IMAGE":
                const imageStyle = {...addBackgroundImage(style), width: "auto"}
                return imageStyle;
            case "BUTTON":
                const buttonStyle = _get(popupState.entities.element[elementId], "buttonStyle", {textAlign: "center"});
                return {...addBackgroundImage(style), textAlign: buttonStyle.textAlign};
            case "ICON":
                const iconStyle = {
                    ...style,
                    lineHeight: `${_get(style, 'lineHeight', style.fontSize)}px`
                }
                return addBackgroundImage(iconStyle);
            case "LINE":
                const lineStyle = {
                    ...style,
                    border: 0
                }
                return addBackgroundImage(lineStyle);
            // case "SUBSCRIPTIONFROM":
            //         const formStyle = {
            //             ...style,
            //             border: 0
            //         }
            //         return addBackgroundImage(formStyle);
            default:
                return addBackgroundImage(style);
        }
    }

    const addBackgroundImage = (style, more) => {
        let updatedStyle = {}
        if (style !== undefined) {
            updatedStyle = {
                ...style,
                backgroundImage: style.backgroundImage !== undefined && `url(${style.backgroundImage})`
            }
        }
        if (more !== undefined) {
            updatedStyle = {...updatedStyle, ...more}
        }

        return updatedStyle;
    }
    
    const elementDomValue = (elementData) => {
        switch (elementData.type) {
            case "TEXT":
                return  <p 
                    dangerouslySetInnerHTML={{__html: elementData.content}}
                />;
            case "IMAGE":
                return <img
                    src={elementData.url}
                    style={{ width: `${elementData.style.width}%` }}
                    alt={elementData.alt !== undefined ? elementData.alt : "image"}
                />;
            case "BUTTON":
                const buttonStyle = {
                    ...elementData.buttonStyle,
                    backgroundColor: elementData.buttonStyle.backgroundColor === '' ? 'transparent' : elementData.buttonStyle.backgroundColor,
                }
                return <button
                    style={addBackgroundImage(buttonStyle)}
                    dangerouslySetInnerHTML={{__html: elementData.content}}
                />;
            case "VIDEO":
                return <span>
                    <img src={YoutubeThumb(elementData.url, 'big')} />
                </span>;
            case "TIMER":
                return <Timer elementId={elementData.id} popupState={popupState} />;
            case "ICON":
                return <i className={`fa ${elementData.contents}`}></i>
            case "LINE":
                return <hr style={ elementData.style } />
            case "SUBSCRIPTIONFROM":
                    return <div className="popup-form" style={{padding: "0px 0px",borderRadius: "0px"}}>
                        <p className="popup-form-input" contenteditable="true" style={{textAlign: "left", fontSize: "14px", color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 255)", borderWidth: "1px", borderStyle: "solid", borderColor: "rgb(204, 204, 204)", padding: "10px", marginBottom: "5px", borderRadius: "50px"}}>Enter your email here...</p>
                        <button className="popup-form-submit" contenteditable="true" style={{textAlign: "center", fontSize: "14px", color:"rgb(255, 255, 255)", backgroundColor: "rgb(93, 78, 171)", borderWidth: "1px", borderStyle: "solid", borderColor: "rgb(93, 78, 171)", padding: "10px", borderRadius: "50px", width: "100%"}}>Send My Coupon</button></div>
            default:
                break;
        }
    }

    const closePopupWithLocal = () => {
        setPopupState(null);
        return closePopup()
    }

    const isFullPage = _get(popupState, 'result.popup.isFullPage', false);

    const coverStyle = () => {
        const { popupStyle } = popupState.result.popup;
        if (isFullPage) {
            return {
                ...popupStyle,
                backgroundImage: popupStyle.backgroundImage !== undefined && `url(${popupStyle.backgroundImage})`,
                width: '100%'
            }
        } else {
            return {
                width: '100%'
            }
        }
    }

    const containerStyle = () => {
        const { popupStyle } = popupState.result.popup;
        if (isFullPage) {
            return {
                width: popupStyle.width
            }
        } else {
            return {
                ...popupStyle,
                backgroundImage: popupStyle.backgroundImage !== undefined && `url(${popupStyle.backgroundImage})`
            }
        }
    }

    return (
        <div
            className="rc-popup-overlay preview"
            onClick={e => closePopupWithLocal()}
        >
            <div className="rc-popup">
                <div className="rc-popup-preview">
                {popupState !== null && popupState.result.popup.isFullPage && <button
                    className="popupclose"
                    onClick={e => closePopupWithLocal()}
                >
                    <i className="material-icons">clear</i>
                </button>}
                {loader ? <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    height: '100vh',
                    width: '100vw',
                    paddingTop: 'calc(50vh - 25px)',
                    textAlign: 'center'
                }}>
                    <ClipLoader
                        sizeUnit={"px"}
                        size={50}
                        color={"#123abc"}
                        loading
                    />
                </div> : popupState !== null && <div className="popup-editor-canvas" 
                    style={coverStyle()}
                >
                    <div className="main-content">
                        <div
                            className="popup-editor-container"
                            style={containerStyle()}
                        >
                            {!isFullPage && <button
                                className="popupclose"
                                onClick={closePopup}
                            >
                                <i className="material-icons">clear</i>
                            </button>}
                            <div className="popup-editor-content">
                                {popupState.result.sections.map(sectionId => {
                                    return (
                                        <div className="popup-editor-section">
                                            <div className="popup-editor-section-wrapper">
                                                <div
                                                    className="popup-editor-structure-wrapper"
                                                    style={{margin: '0px auto', width: '100%'}}
                                                >
                                                    <div className="popup-editor-structure" style={{width: '100%'}}>
                                                        {popupState.entities.section[sectionId].structures.map(structuresId => {
                                                            return (
                                                                <div
                                                                    className="popup-editor-structure-inside"
                                                                    style={addBackgroundImage(popupState.entities.structures[structuresId].style)}
                                                                >   <div className="popup-editor-structure-wrapper">
                                                                        {popupState.entities.structures[structuresId].columns.map(columnsId => {
                                                                            return (
                                                                                <div
                                                                                    style={{width: `${popupState.entities.columns[columnsId].width}%`, display: 'inline-block'}}
                                                                                >
                                                                                    <div className="popup-editor-container">
                                                                                        {popupState.entities.columns[columnsId].containers.map(containersId => {
                                                                                            return (
                                                                                                <div
                                                                                                    className="popup-editor-container-wrapper"
                                                                                                    style={addBackgroundImage(popupState.entities.containers[containersId].style)}
                                                                                                >
                                                                                                    <div className="popup-editor-element">
                                                                                                        {popupState.entities.containers[containersId].elements.map(elementsId => {
                                                                                                            return (
                                                                                                                <div
                                                                                                                    className="popup-editor-element-wrapper"
                                                                                                                    style={elementStyle(elementsId, popupState.entities.element[elementsId].type, popupState.entities.element[elementsId].style)}
                                                                                                                >
                                                                                                                    {elementDomValue(popupState.entities.element[elementsId])}
                                                                                                                </div>
                                                                                                            )
                                                                                                        })}
                                                                                                    </div>
                                                                                                </div>
                                                                                            )
                                                                                        })}
                                                                                    </div>
                                                                                </div>
                                                                            )
                                                                        })}
                                                                    </div>
                                                                </div>
                                                            )
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>}
            </div>
        </div>
    </div>
    );
}

export default PopupPreview;
import React, { useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link,Redirect } from "react-router-dom";
import { denormalize } from "normalizr";
import { toast } from 'react-toastify';
import cookie from 'react-cookies';

// import logo from "../../../../images/rc-logo-mini.png";
import "./header.scss";
import Schema from '../../store/schema';
import { PostData } from '../../../../views/services/PostData';
import { PopupStore } from '../../store';

const PopupEditorHeader = () => {
    const { popupState, popupName } = useContext(PopupStore);

    const [saveLoader, setSaveLoader] = useState(false);
    const [saveLoaderExit, setSaveLoaderExit] = useState(false);
    const [getRedir, setRedir] = useState(false);

    const savePopup = (getstatus) => {
        if(getRedir === true){
            setSaveLoaderExit(true);
        }else{
            setSaveLoader(true);
        }
        const denormalizeData = denormalize(popupState.result, Schema, popupState.entities);
        console.log(denormalizeData)
        const saveData = {
            "pop_up_id": cookie.load("popUpId"),
            "pop_up_json_code": JSON.stringify(denormalizeData)
        }
        PostData('ms1', 'popupsavejson', saveData).then(response => {
            console.log(response);

            if(getRedir === true){
                setSaveLoaderExit(false);
            }else{
                setSaveLoader(false);
            }
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    toast.info(response.message);
                    if(getstatus === true){
                        setRedir(true)
                    }
                }
            } else {
                toast.warn("Update is not saved");
            }
        });
    }
    if(getRedir === true){       
       return <Redirect  to={{ pathname:'/plain/displayrules', state:{from : "fromcreation"}}}/>
       
    }
   
    return (
       
        <header
            className="popup-editor-header"
        >
              <div className="editor-header-close">
                <h3>
                    <Link to={{ pathname: '/admin/campaigns' }}>
                        {/* <i className="material-icons">keyboard_backspace</i> */}
                        <span class="material-icons">
                        keyboard_backspace

</span>
                    </Link>
                    </h3>
            </div>
            <div className="popup-editor-header-left">
                <div className="logo">
                    <Link to="/dashboard">
                        {/* <img src={logo} alt="logo" /> */}
                <small>  <img
                        alt="Logo"
                        className=""
                        width=''
                        src={require("../../../../assets/img/brand/aclogo.png")}
                      /> </small> 

                    </Link>
                </div>
                <span className="popup-editor-header-name">{popupName} Popup Name</span>

            </div>
            <div className="popup-editor-header-center">
                {/* <span className="popup-editor-header-name">{popupName} Popup Name</span> */}
            </div>
            <div className="popup-editor-header-right">
            <Button
                    onClick={() => savePopup(true)}
                    variant="primary"
                    style={{ float: "right",marginRight: "15px" }}
                    disabled={saveLoaderExit}
                    className="exit"

>{saveLoaderExit ? "Loading..." : " Save & Set Display "}</Button>
            <Button
                    onClick={() => savePopup(false)}
                    variant="primary"
                    style={{ float: "right",marginRight: "15px" }}
                    disabled={saveLoader}
                >{saveLoader ? "Loading..." : " Save "}</Button>
                
            </div>
          
        </header>
    );
}

export default PopupEditorHeader;
import uuid from "uuid";

export const newSingleColumnStructure = (result, popupState) => {
    console.log(result)
    const newStructuresArr = [...popupState.entities.section[destination.droppableId].structures];
    const { draggableId, destination, source, type } = result;
    const newStructureId = uuid();
    const newColumnId = uuid();
    const newContainers = uuid();
    newStructuresArr.splice(destination.index, 0, newStructureId);
    const newStructure = {
        ...popupState,
        entities: {
            ...popupState.entities,
            section: {
                ...popupState.entities.section,
                [destination.droppableId]: {
                    ...popupState.entities.section[destination.droppableId],
                    structures: newStructuresArr
                }
            },
            structures: {
                ...popupState.entities.structures,
                [newStructureId]: {
                    id: newStructureId,
                    columns: [newColumnId]
                }
            },
            columns: {
                ...popupState.entities.columns,
                [newColumnId]: {
                    id: newColumnId,
                    width: '100%',
                    containers: [newContainers]
                }
            },
            containers: {
                ...popupState.entities.containers,
                [newContainers]: {
                    id: newContainers,
                    elements: []
                }
            }
        }
    }
    return newStructure;
}
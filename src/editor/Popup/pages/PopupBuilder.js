import React, { useContext, useEffect, useState } from 'react';
import { Redirect } from "react-router-dom";
import { normalize } from "normalizr";
import cookie from 'react-cookies';

import { Context } from '../../../Context';
import PopupEditorHeader from '../components/header';
import PopupEditorBuilder from '../components/builder';
import { PopupStore } from '../store';
import Schema from '../store/schema';
import { PostData } from '../../../services/PostData';

const getNormalizedData = (data, schema) => normalize(data, schema);

const PopupBuilder = () => {
    const { setFullWidthLoader } = useContext(Context);
    const { setPopupName, setPopupState } = useContext(PopupStore);
    
    const [getJSONData, setGetJSONData] = useState(null);

    console.log("Incoming...", cookie.load("popupId"));
    
    useEffect(() => {
        setFullWidthLoader(true);
        const getDataId = {
            "pop_up_id": cookie.load("popupId")
        }
        PostData("ms4", "popupgetjson", getDataId).then(response => {
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    console.log(response.popupmaster.pop_up_json_code);
                    setGetJSONData(JSON.parse(response.popupmaster.pop_up_json_code));
                    setPopupName(response.popupmaster.pop_up_name);
                    setFullWidthLoader(false);
                }
            }
        });
    }, []);

    useEffect(() => {
        getJSONData !== null && setPopupState(getNormalizedData(getJSONData, Schema));
    }, [getJSONData]);

    if (cookie.load("popupId") === undefined) {
        return <Redirect to="/popup" />
    }
    return (
        <div className="app-editor">
            {getJSONData !== null ? <React.Fragment>
                <PopupEditorHeader />
                <main className="app-content">
                    <PopupEditorBuilder />
                </main>
            </React.Fragment> : <span 
                style={{
                    padding:"5px",
                    fontSize: "16px",
                    fontWeight: "bold"
                }}>Loading...</span>}
        </div>
    );
}

export default PopupBuilder;
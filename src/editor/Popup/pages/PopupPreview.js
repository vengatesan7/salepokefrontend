import React, { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import cookie from "react-cookies";
import { Context } from "../../../Context";
import PopupPriviewCanvas from "../components/preview";

function PopupPrview(props) {
  const { setFullWidthLoader } = useContext(Context);
  if (cookie.load("popUpId") === undefined && props.template === undefined) {
    return <Redirect to="/popup" />;
  }
  return (
    <main className="app-content">
      <PopupPriviewCanvas preview={props.template} />
    </main>
  );
}

export default PopupPrview;

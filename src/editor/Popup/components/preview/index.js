import React, { useContext, useEffect, useState } from "react";
import { normalize } from "normalizr";
import cookie from "react-cookies";
import _get from "lodash.get";

import { Context } from "../../../../Context";
import { PostData } from "../../../../services/PostData";
import Schema from "../../store/schema";

const getNormalizedData = (data, schema) => normalize(data, schema);

const PopupPriviewCanvas = props => {
  const { setFullWidthLoader } = useContext(Context);
  const [getJSONData, setGetJSONData] = useState(null);
  const [popupStyle, setPopupStyle] = useState(null);
  useEffect(() => {
    setFullWidthLoader(true);
    if (props.preview) {
      const getDataId = {
        pop_up_template_id: props.preview
      };
      PostData("ms4", "popuptemplatebyid", getDataId).then(response => {
        if (response !== "Invalid") {
          if (response.status === "success") {
            setGetJSONData(
              getNormalizedData(JSON.parse(response.popuptemplate), Schema)
            );
            setFullWidthLoader(false);
          }
        }
      });
    } else {
      const getDataId = {
        pop_up_id: cookie.load("popupId")
      };
      PostData("ms4", "popupgetjson", getDataId).then(response => {
        if (response !== "Invalid") {
          if (response.status === "success") {
            setGetJSONData(
              getNormalizedData(
                JSON.parse(response.popupmaster.pop_up_json_code),
                Schema
              )
            );
            setFullWidthLoader(false);
          }
        }
      });
    }
  }, []);
  useEffect(() => {
    getJSONData !== null && setPopupStyle(getJSONData.result.popup.popupStyle);
  }, [getJSONData]);
  return ( 
    getJSONData !== null && ( 
      <div style={{...popupStyle, backgroundImage : `url(${popupStyle !== null && popupStyle.backgroundImage })`}} >
        {getJSONData.result.content.map(contentId => {
          const contentStyle =
            getJSONData.entities.content[contentId].contentStyle;
           const columnWidth = 100 / getJSONData.entities.content[contentId].columns.length
          return (
            <div style={{ display: "flex" }} className={contentId}>
              {/* {contentId} */}
              {getJSONData.entities.content[contentId].columns.map(
                (columnId, columnIndex) => {
                  const columnsStyle =
                    getJSONData.entities.columns[columnId].columnStyle;
                  return (
                    <div
                      style={{
                        ...columnsStyle,
                        width: `${columnWidth}%`,
                        display: "inline-block"
                      }}
                      className={columnId}
                    >
                      {/* {columnId} */}
                      {getJSONData.entities.columns[columnId].elements.map(
                        (elementId, columnIndex) => {
                          const elementStyle =
                            getJSONData.entities.elements[elementId]
                              .elementStyle;
                          const elementType =
                            getJSONData.entities.elements[elementId].type;
                            const buttonStyle =
                            getJSONData.entities.elements[elementId]
                              .buttonStyle;
                              const formStyle =
                            getJSONData.entities.elements[elementId]
                              .formStyle;
                              const inputStyle =
                            getJSONData.entities.elements[elementId]
                              .inputStyle;
                              const buttonSize =
                              getJSONData.entities.elements[elementId]
                                .buttonSize;
                          return (
                            <React.Fragment>
                              {elementType === "text" && (
                                <div style={elementStyle}>
                                  {
                                    getJSONData.entities.elements[elementId]
                                      .content
                                  }
                                </div>
                              )}
                              {/* //Image */}
                              {elementType === "image" && (
                                <div style={{ ...elementStyle, width: "100%" }}>
                                  <img
                                    src={
                                      getJSONData.entities.elements[elementId]
                                        .url
                                    }
                                    style={{ width: `${elementStyle.width}%` }}
                                  />
                                </div>
                              )}
                              {/* Link */}
                              {elementType === "link" && (
                                <div style={elementStyle}>
                                  {
                                    getJSONData.entities.elements[elementId]
                                      .content
                                  }
                                </div>
                              )}
                              {elementType === "button" && (
                                <div style={{marginBottom: elementStyle.marginBottom, marginTop: elementStyle.marginTop}}>
                                  <button style={{
                                    ...elementStyle,
                                    width : buttonSize === 'fullwidth' ? "100%" : 'auto',
                                    margin: "0 auto",
                                    display:"block",
                                    float: elementStyle.textAlign === "center" ? "none" : elementStyle.textAlign
                                  }}>
                                    {
                                      getJSONData.entities.elements[elementId]
                                        .content
                                    }
                                  </button>
                                </div>
                              )}
                               {elementType === "subscriptionform" && (

                                <form style={formStyle}>
                                  <input 
                                    style={{
                                      ...inputStyle,
                                      borderRadius: formStyle.borderRadius,
                                      width: "100%",
                                      padding:" 10px",
                                      marginBottom:" 5px"
                                    }}
                                    type='text' 
                                    placeholder={getJSONData.entities.elements[elementId].inputPlaceholder}
                                  />
                                  <button
                                    style={{ 
                                      ...buttonStyle,
                                      borderRadius: formStyle.borderRadius,
                                      width : buttonSize === 'fullwidth' ? "100%" : 'auto',
                                      padding: "10px",
                                      margin: "0 auto",
                                      display:"block"
                                    }}
                                    type='submit' >
                                      {getJSONData.entities.elements[elementId].buttonText}
                                    </button>
                                </form>
                              )}
                            </React.Fragment>
                          );
                        }
                      )}
                    </div>
                  );
                }
              )}
            </div>
          );
        })}
      </div>
    )
  );
};
export default PopupPriviewCanvas;

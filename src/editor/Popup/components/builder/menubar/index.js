import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './menubar.scss';
import PopupWidgets from './widgets';
import PopupColumns from './columns';
import PopupSettings from './settings';

const PopupEditorMenuBar = () => {
    return (
        <Tabs className="pope-menubar-tabs">
            <TabPanel className="pope-menubar-content">
                <PopupWidgets />
            </TabPanel>
            <TabPanel className="pope-menubar-content">
                <PopupColumns />
            </TabPanel>
            <TabPanel className="pope-menubar-content">
                <PopupSettings />
            </TabPanel>
            <TabList className="pope-menubar-list">
                <Tab className="pope-menubar-item">
                    <i className="material-icons">widgets</i>
                    <span>Widgets</span>
                </Tab>
                <Tab className="pope-menubar-item">
                    <i className="material-icons" style={{ transform: "rotate(90deg)" }}>view_agenda</i>
                    <span>Column</span>
                </Tab>
                <Tab className="pope-menubar-item">
                    <i className="material-icons">settings</i>
                    <span>Settings</span>
                </Tab>
            </TabList>
        </Tabs>
    );
}

export default PopupEditorMenuBar;
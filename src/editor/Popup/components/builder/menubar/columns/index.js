import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import classnames from 'classnames';
import _get from "lodash.get";
import uuid from "uuidv4";

import { PopupStore } from '../../../../store';

const PopupColumns = () => {
    const { popupState, setPopupState } = useContext(PopupStore);
    const contentId = popupState.result.content[0];
    const [columnType, setColumnType] = useState(null);

    const [columnOneData, setColumnOneData] = useState(null);
    const [columnTwoData, setColumnTwoData] = useState(null);

    useEffect(() => {
        setColumnType(popupState.entities.content[contentId].columns.length);
    }, []);

    const getData = () => {
        const columnOneId = popupState.entities.content[contentId].columns[0];
        setColumnOneData(popupState.entities.columns[columnOneId]);
        if (popupState.entities.content[contentId].columns.length === 2) {
            const columnTwoId = popupState.entities.content[contentId].columns[1];
            setColumnTwoData(popupState.entities.columns[columnTwoId]);
        }
    }

    useEffect(() => {
        getData();
    }, [contentId])

    useEffect(() => {
        getData();
    }, [popupState])

    const handleColumn = (value) => {
        setColumnType(value);
        switch (value) {
            case 1:
                const columnOneId = popupState.entities.content[contentId].columns[0];
                const columnTwoId = popupState.entities.content[contentId].columns[1];
                const columnOneElements = popupState.entities.columns[columnOneId].elements;
                const columnTwoElements = popupState.entities.columns[columnTwoId].elements;
                const mergedColumnElements = columnOneElements.concat(columnTwoElements);

                const newOneColumnUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        content: {
                            ...popupState.entities.content,
                            [contentId]: {
                                ...popupState.entities.content[contentId],
                                columns: [columnOneId]
                            }
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [columnOneId]: {
                                ...popupState.entities.columns[columnOneId],
                                elements: mergedColumnElements
                            }
                        }
                    }
                }
                return setPopupState(newOneColumnUpdate);
            case 2:
                const oldColumnId = popupState.entities.content[contentId].columns[0];
                const newColumnId = "scoundColumn";

                const newColumnArray = [oldColumnId, newColumnId];

                const newTwoColumnUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        content: {
                            ...popupState.entities.content,
                            [contentId]: {
                                ...popupState.entities.content[contentId],
                                columns: newColumnArray
                            }
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [newColumnId]: {
                                ...popupState.entities.columns[newColumnId],
                                id: newColumnId,
                                elements: [],
                                columnStyle: {
                                    paddingRight: 15,
                                    paddingLeft: 15
                                }
                            }
                        }
                    }
                }
                return setPopupState(newTwoColumnUpdate);
            default:
                break;
        }
    }

    const handleColumnOneStyle = (name, value) => {
        const columnOneId = popupState.entities.content[contentId].columns[0];

        setColumnOneData({
            ...columnOneData,
            columnStyle: {
                ...columnOneData.columnStyle,
                [name]: value
            }
        });

        const columnOneStyleUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                columns: {
                    ...popupState.entities.columns,
                    [columnOneId]: {
                        ...popupState.entities.columns[columnOneId],
                        columnStyle: {
                            ...popupState.entities.columns[columnOneId].columnStyle,
                            [name]: value
                        }
                    }
                }
            }
        }

        return setPopupState(columnOneStyleUpdate);
    }

    const handleColumnTwoStyle = (name, value) => {
        const columnTwoId = popupState.entities.content[contentId].columns[1];
        setColumnTwoData({
            ...columnTwoData,
            columnStyle: {
                ...columnTwoData.columnStyle,
                [name]: value
            }
        });

        const columnTwoStyleUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                columns: {
                    ...popupState.entities.columns,
                    [columnTwoId]: {
                        ...popupState.entities.columns[columnTwoId],
                        columnStyle: {
                            ...popupState.entities.columns[columnTwoId].columnStyle,
                            [name]: value
                        }
                    }
                }
            }
        }

        return setPopupState(columnTwoStyleUpdate);
    }

    return (
        <React.Fragment>
            <ul className="row-list">
                <li className="row-type">
                    {columnType === 1 && <div className="row-type-selected"><i className="material-icons">done</i></div>}
                    <div className={classnames("row-button", columnType === 1 && 'active')} onClick={e => columnType !== 1 && handleColumn(1)}>
                        <span></span>
                    </div>
                </li>
                <li className="row-type">
                    {columnType === 2 && <div className="row-type-selected"><i className="material-icons">done</i></div>}
                    <div className={classnames("row-button", columnType === 2 && 'active')} onClick={e => columnType !== 2 && handleColumn(2)}>
                        <span></span>
                        <span></span>
                    </div>
                </li>
            </ul>
            {columnOneData !== null && <div className="pope-menubar-wrapper">
                <div className="pope-menubar-properties">
                    <label>Column One Properties</label>
                    <div className="pope-menubar-property">
                        <label>Background Color</label>
                        <ChromePicker
                            color={_get(columnOneData, 'columnStyle.backgroundColor', '#ffffff')}
                            disableAlpha
                            onChange={(e) => handleColumnOneStyle('backgroundColor', e.hex)}
                        />
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Top</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnOneData, 'columnStyle.paddingTop', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnOneStyle('paddingTop', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Right</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnOneData, 'columnStyle.paddingRight', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnOneStyle('paddingRight', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Bottom</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnOneData, 'columnStyle.paddingBottom', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnOneStyle('paddingBottom', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Left</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnOneData, 'columnStyle.paddingLeft', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnOneStyle('paddingLeft', e)}
                            />
                        </div>
                    </div>
                </div>
                {columnType === 2 && columnTwoData !== null && <div className="pope-menubar-properties">
                    <label>Column Two Properties</label>
                    <div className="pope-menubar-property">
                        <label>Background Color</label>
                        <ChromePicker
                            color={_get(columnTwoData, 'columnStyle.backgroundColor', '#ffffff')}
                            disableAlpha
                            onChange={(e) => handleColumnTwoStyle('backgroundColor', e.hex)}
                        />
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Top</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnTwoData, 'columnStyle.paddingTop', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnTwoStyle('paddingTop', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Right</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnTwoData, 'columnStyle.paddingRight', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnTwoStyle('paddingRight', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Bottom</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnTwoData, 'columnStyle.paddingBottom', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnTwoStyle('paddingBottom', e)}
                            />
                        </div>
                    </div>
                    <div className="pope-menubar-property two-column">
                        <label>Padding Left</label>
                        <div className="pope-menubar-rangeslider">
                            <Slider min={0} max={25} step={1}
                                value={_get(columnTwoData, 'columnStyle.paddingLeft', 0)}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleColumnTwoStyle('paddingLeft', e)}
                            />
                        </div>
                    </div>
                </div>}
            </div>}
        </React.Fragment>
    );
}

export default PopupColumns;
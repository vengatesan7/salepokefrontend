import React, { useContext } from 'react';
import { DragDropContext } from "react-beautiful-dnd";
import uuid from "uuidv4";

import './builder.scss';
import PopupEditorCanvas from './canvas';
import PopupEditorMenuBar from './menubar';
import { PopupStore } from '../../store';

const PopupEditorBuilder = () => {
    const {
        popupState,
        setPopupState,
        setPropertyBarActive,
        setSelectedElement,
        setIsNewElementDrag
    } = useContext(PopupStore);

    const popupOnDragEnd = (result) => {
        setIsNewElementDrag(false);
        const { draggableId, destination, source, type } = result;
        if (destination === null) {
            return;
        }

        if (type === "element") {
            if (source.droppableId === "ITEMS") {
                const newElementID = uuid();
                const newElementArray = popupState.entities.columns[destination.droppableId].elements
                newElementArray.splice(destination.index, 0, newElementID);
                const newElementJson = () => {
                    switch (draggableId) {
                        case "Text":
                            const newText = {
                                id: newElementID,
                                type: "text",
                                content: "Edit your text",
                                elementStyle: {
                                    textAlign: "center",
                                    fontSize: 14,
                                }
                            }
                            return newText;
                        case "Image":
                            const newImage = {
                                id: newElementID,
                                type: "image",
                                url: null,
                                elementStyle: {
                                    textAlign: "center",
                                    width: 100
                                }
                            }
                            return newImage;
                        case "Form":
                            const newSubscriptionForm = {
                                id: newElementID,
                                type: "subscriptionform",
                                inputPlaceholder: "Enter your email here...",
                                buttonText: "Send My Coupon",
                                buttonSize: "fullwidth",
                                formStyle: {
                                    borderRadius: 50,
                                    paddingTop: 15,
                                    paddingRight: 0,
                                    paddingBottom: 15,
                                    paddingLeft: 0
                                },
                                buttonStyle: {
                                    textAlign: "center",
                                    fontSize: 14,
                                    color: "#ffffff",
                                    backgroundColor: "#5d4eab",
                                    borderWidth: 1,
                                    borderStyle: "solid",
                                    borderColor: "#5d4eab"
                                },
                                inputStyle: {
                                    textAlign: "left",
                                    fontSize: 14,
                                    color: "#000000",
                                    backgroundColor: "#ffffff",
                                    borderWidth: 1,
                                    borderStyle: "solid",
                                    borderColor: "#cccccc"
                                }
                            }
                            return newSubscriptionForm;
                        case "Button":
                            const newButton = {
                                id: newElementID,
                                type: "button",
                                content: "Button Text",
                                buttonSize: "fittotext",
                                elementStyle: {
                                    textAlign: "center",
                                    fontSize: 14,
                                    backgroundColor: "#007bff",
                                    color: "#ffffff",
                                    paddingTop: 5,
                                    paddingRight: 15,
                                    paddingBottom: 5,
                                    paddingLeft: 15,
                                    borderRadius: 2,
                                    borderWidth: 0,
                                    borderStyle: 'solid'
                                }
                            }
                            return newButton;
                        default:
                            break;
                    }
                }
                const newElementUpdate = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        elements: {
                            ...popupState.entities.elements,
                            [newElementID]: newElementJson()
                        },
                        columns: {
                            ...popupState.entities.columns,
                            [destination.droppableId]: {
                                ...popupState.entities.columns[destination.droppableId],
                                elements: newElementArray
                            }
                        }
                    }
                }
                return setPopupState(newElementUpdate);
            } else if (source.droppableId === destination.droppableId) {
                const newElementArray = popupState.entities.columns[destination.droppableId].elements;
                const findOldPosition = newElementArray.indexOf(draggableId);
                newElementArray.splice(findOldPosition, 1);
                newElementArray.splice(destination.index, 0, draggableId);

                const newElementPositionChange = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        columns: {
                            ...popupState.entities.columns,
                            [destination.droppableId]: {
                                ...popupState.entities.columns[destination.droppableId],
                                elements: newElementArray
                            }
                        }
                    }
                }
                return setPopupState(newElementPositionChange);
            } else {
                const sourceElementArray = popupState.entities.columns[source.droppableId].elements;
                const destinationElementArray = popupState.entities.columns[destination.droppableId].elements;
                const findOldPosition = sourceElementArray.indexOf(draggableId);
                sourceElementArray.splice(findOldPosition, 1);
                destinationElementArray.splice(destination.index, 0, draggableId);

                const newUpdateOfPosition = {
                    ...popupState,
                    entities: {
                        ...popupState.entities,
                        columns: {
                            ...popupState.entities.columns,
                            [destination.droppableId]: {
                                ...popupState.entities.columns[destination.droppableId],
                                elements: destinationElementArray
                            },
                            [source.droppableId]: {
                                ...popupState.entities.columns[source.droppableId],
                                elements: sourceElementArray
                            }
                        }
                    }
                }

                return setPopupState(newUpdateOfPosition);
            }
        }
    }

    const popupOnDragStart = (result) => {
        const { source, draggableId, type } = result;
        setPropertyBarActive(false);
        setSelectedElement(null);
        if (source.droppableId === "ITEMS") {
            setIsNewElementDrag(true);
        }
    }

    return (
        <React.Fragment>
            <DragDropContext
                onDragEnd={popupOnDragEnd}
                onDragStart={popupOnDragStart}
            >
                <div className="pope-canvas">
                    <PopupEditorCanvas />
                </div>
                <div className="pope-right-menu">
                    <PopupEditorMenuBar />
                </div>
            </DragDropContext>
        </React.Fragment>
    );
}

export default PopupEditorBuilder;
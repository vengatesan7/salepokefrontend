import React, { useContext, useState, useEffect } from 'react';
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const Alignment = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [align, setAlign] = useState(null);

    useEffect(() => {
        setAlign({
            textAlign: _get(popupState.entities.elements[selectedElement], 'elementStyle.textAlign', "left"),
        })
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handleAlignment = (name, value) => {
        setAlign({
            ...align,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <div className="property-select">
            {align !== null && <div className="property property-selected" onClick={() => handleThisPopup('alignOption', propertiesDropDowns.alignOption ? false : true)}>
                <i className="material-icons">
                    {align.textAlign === "left" && "format_align_left"}
                    {align.textAlign === "center" && "format_align_center"}
                    {align.textAlign === "right" && "format_align_right"}
                </i>
            </div>}
            {propertiesDropDowns.alignOption && <div className="property-option">
                <div
                    className="option"
                    onClick={() => handleAlignment("textAlign", "left")}
                >
                    <i className="material-icons">format_align_left</i>
                </div>
                <div
                    className="option"
                    onClick={() => handleAlignment("textAlign", "center")}
                >
                    <i className="material-icons">format_align_center</i>
                </div>
                <div
                    className="option"
                    onClick={() => handleAlignment("textAlign", "right")}
                >
                    <i className="material-icons">format_align_right</i>
                </div>
            </div>}
        </div>
    );
}

export default Alignment;
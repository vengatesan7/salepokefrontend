import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const Border = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [elementStyle, setElementStyle] = useState(null);
    const [borderColorPopup, setBorderColorPopup] = useState(false);
    useEffect(() => {
        setElementStyle(popupState.entities.elements[selectedElement].elementStyle);
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handleBorder = (name, value) => {
        setElementStyle({
            ...elementStyle,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <React.Fragment>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('radius', propertiesDropDowns.radius ? false : true)}>
                    <i className="material-icons" >crop_free</i>
                </div>
                {propertiesDropDowns.radius && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={50} step={1}
                            value={elementStyle.borderRadius || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handleBorder('borderRadius', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {elementStyle.borderRadius || 0}px
                    </div>
                </div>}
            </div>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('elementBorder', propertiesDropDowns.elementBorder ? false : true)}>
                    <i className="material-icons" >crop_square</i>
                </div>
                {propertiesDropDowns.elementBorder && <div className="property-border-popup">
                    <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={0} max={10} step={1}
                                value={elementStyle.borderWidth || 0}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleBorder('borderWidth', e)}
                            />
                        </div>
                        <div className="rangeslider-value">
                            {elementStyle.borderWidth || 0}px
                        </div>
                    </div>
                    <div className="property-border-style">
                        <label>Style</label>
                        <select value={elementStyle.borderStyle} onChange={(e) => handleBorder('borderStyle', e.target.value)}>
                            <option value="solid">Solid</option>
                            <option value="dotted">Dotted</option>
                            <option value="dashed">Dashed</option>
                            <option value="double">Double</option>
                        </select>
                    </div>
                    <div className="property-border-style">
                        <label>Color</label>
                        <span
                            style={{ backgroundColor: elementStyle.borderColor }}
                            onClick={() => setBorderColorPopup(borderColorPopup ? false : true)}
                        ></span>
                        {borderColorPopup && <div className='color-picker-wrapper' style={{ top: "25px" }} onBlur={() => setBorderColorPopup(false)}>
                            <ChromePicker color={elementStyle.borderColor} onChange={(e) => handleBorder("borderColor", e.hex)} disableAlpha />
                        </div>}
                    </div>
                </div>}
            </div>
        </React.Fragment>
    );
}

export default Border;
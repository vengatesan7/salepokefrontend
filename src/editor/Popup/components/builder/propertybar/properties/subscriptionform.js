import React, { useContext, useState, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from "classnames";
import _get from "lodash.get";

import { handlePopup, closeAllDropDown } from '../handleDropDowns';
import { PopupStore } from '../../../../store';
import ButtonSize from './buttonsize';


const SubscriptionFormProperties = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [buttonStyle, setButtonStyle] = useState(null);
    const [inputStyle, setInputStyle] = useState(null);

    useEffect(() => {
        setInputStyle(_get(popupState.entities.elements[selectedElement], 'inputStyle', null));
        setButtonStyle(_get(popupState.entities.elements[selectedElement], 'buttonStyle', null));
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handleInputStyles = (name, value, closeDropDown) => {
        if (closeDropDown) {
            setPropertiesDropDowns(closeAllDropDown());
        }
        setInputStyle({
            ...inputStyle,
            [name]: value
        });
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        inputStyle: {
                            ...popupState.entities.elements[selectedElement].inputStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    const handleButtonStyles = (name, value, closeDropDown) => {
        if (closeDropDown) {
            setPropertiesDropDowns(closeAllDropDown());
        }
        setButtonStyle({
            ...buttonStyle,
            [name]: value
        });
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        buttonStyle: {
                            ...popupState.entities.elements[selectedElement].buttonStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <React.Fragment>
            <div className="property-row">
                <label>Input Style</label>
                <div className="property-group color-picker">
                    <div className="property" onClick={() => handleThisPopup('inputColor', propertiesDropDowns.inputColor ? false : true)}>
                        <i className="material-icons">format_color_text</i>
                    </div>
                    {propertiesDropDowns.inputColor &&
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={inputStyle.color} onChange={(e) => handleInputStyles("color", e.hex)} disableAlpha />
                        </div>
                    }
                </div>
                <div className="property-group color-picker">
                    <div className="property" onClick={() => handleThisPopup('inputBackgroundColor', propertiesDropDowns.inputBackgroundColor ? false : true)}>
                        <i className="material-icons">format_color_fill</i>
                    </div>
                    {propertiesDropDowns.inputBackgroundColor &&
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={inputStyle.backgroundColor} onChange={(e) => handleInputStyles("backgroundColor", e.hex)} disableAlpha />
                        </div>
                    }
                </div>
                <div className="property-group">
                    <div
                        className={classnames("property", _get(inputStyle, 'fontWeight', null) === "bold" && "active")}
                        onClick={() => handleInputStyles("fontWeight", _get(inputStyle, 'fontWeight', null) === null ? "bold" : null, true)}>
                        <i className="material-icons">format_bold</i>
                    </div>
                    <div
                        className={classnames("property", _get(inputStyle, 'fontStyle', null) === "italic" && "active")}
                        onClick={() => handleInputStyles("fontStyle", _get(inputStyle, 'fontStyle', null) === null ? "italic" : null, true)}>
                        <i className="material-icons">format_italic</i>
                    </div>
                    <div
                        className={classnames("property", _get(inputStyle, 'textDecoration', null) === "underline" && "active")}
                        onClick={() => handleInputStyles("textDecoration", _get(inputStyle, 'textDecoration', null) === null ? "underline" : null, true)}>
                        <i className="material-icons">format_underline</i>
                    </div>
                </div>
            </div>
            <div className="property-row">
                <label>Button Style</label>
                <div className="property-group color-picker">
                    <div className="property" onClick={() => handleThisPopup('buttonColor', propertiesDropDowns.buttonColor ? false : true)}>
                        <i className="material-icons">format_color_text</i>
                    </div>
                    {propertiesDropDowns.buttonColor &&
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={buttonStyle.color} onChange={(e) => handleButtonStyles("color", e.hex)} disableAlpha />
                        </div>
                    }
                </div>
                <div className="property-group color-picker">
                    <div className="property" onClick={() => handleThisPopup('buttonBackgroundColor', propertiesDropDowns.buttonBackgroundColor ? false : true)}>
                        <i className="material-icons">format_color_fill</i>
                    </div>
                    {propertiesDropDowns.buttonBackgroundColor &&
                        <div className='color-picker-wrapper'>
                            <ChromePicker color={buttonStyle.backgroundColor} onChange={(e) => handleButtonStyles("backgroundColor", e.hex)} disableAlpha />
                        </div>
                    }
                </div>
                <div className="property-group">
                    <div
                        className={classnames("property", _get(buttonStyle, 'fontWeight', null) === "bold" && "active")}
                        onClick={() => handleButtonStyles("fontWeight", _get(buttonStyle, 'fontWeight', null) === null ? "bold" : null, true)}>
                        <i className="material-icons">format_bold</i>
                    </div>
                    <div
                        className={classnames("property", _get(buttonStyle, 'fontStyle', null) === "italic" && "active")}
                        onClick={() => handleButtonStyles("fontStyle", _get(buttonStyle, 'fontStyle', null) === null ? "italic" : null, true)}>
                        <i className="material-icons">format_italic</i>
                    </div>
                    <div
                        className={classnames("property", _get(buttonStyle, 'textDecoration', null) === "underline" && "active")}
                        onClick={() => handleButtonStyles("textDecoration", _get(buttonStyle, 'textDecoration', null) === null ? "underline" : null, true)}>
                        <i className="material-icons">format_underline</i>
                    </div>
                </div>
                <ButtonSize />
            </div>
        </React.Fragment >
    );
}

export default SubscriptionFormProperties;
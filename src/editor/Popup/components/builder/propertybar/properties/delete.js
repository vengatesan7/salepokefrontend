import React, { useContext, useState } from 'react';
import { PopupStore } from '../../../../store';

const DeleteElement = () => {
    const {
        popupState,
        setPopupState,
        selectedElement
    } = useContext(PopupStore);

    const [deletePopup, setDeletePopup] = useState(false);

    const handleDelete = () => {

    }

    return (
        <div className="property-group property-delete">
            <div
                className="property"
                onClick={() => setDeletePopup(deletePopup ? false : true)}>
                <i className="material-icons">delete</i>
            </div>
            {deletePopup && <div className="delete-popup">
                <label>No Undo</label>
                <button
                    className="property-button"
                    onClick={() => handleDelete()}
                >Cancel</button>
                <button
                    className="property-button"
                    onClick={() => setDeletePopup(false)}
                >Delete</button>
            </div>}
        </div>
    );
}

export default DeleteElement;
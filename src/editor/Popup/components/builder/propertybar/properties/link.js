import React, { useContext, useState, useEffect } from 'react';
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const LinkElement = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [elementLink, setElementLink] = useState(null);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    useEffect(() => {
        setElementLink(_get(popupState.entities.elements[selectedElement], 'link', null));
    }, []);

    const handleLink = (name, value) => {
        setElementLink(value);
        const newLinkUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        [name]: value 
                    }
                }
            }
        }
        
        return setPopupState(newLinkUpdate);
    }

    return(
        <div className="property-group">
            <div className="property" onClick={() => handleThisPopup('elementLink', propertiesDropDowns.elementLink ? false : true)}>
                <i className="material-icons" >link</i>
            </div>
            {propertiesDropDowns.elementLink && <div className="property-drop-down">
                <input
                    value={elementLink}
                    type="text"
                    placeholder="https://www.example.com/"
                    onChange={(e) => handleLink('link', e.target.value)}
                    onKeyPress={(e) => e.key === 'Enter' && handleThisPopup('elementLink', false)}
                />
            </div>}
        </div>
    );
}

export default LinkElement;
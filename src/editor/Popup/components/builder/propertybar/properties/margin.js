import React, { useContext, useState, useEffect } from 'react';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const Margin = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [elementStyle, setElementStyle] = useState(null);

    useEffect(() => {
        setElementStyle(_get(popupState.entities.elements[selectedElement], 'elementStyle', null));
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handleMargin =(name, value) => {
        setElementStyle({
            ...elementStyle,
            [name]: value
        });

        const marginUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        } 

        return setPopupState(marginUpdate);
    }

    return(
        elementStyle !== null && <React.Fragment>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('marginTop', propertiesDropDowns.marginTop ? false : true)}>
                    <i className="material-icons">vertical_align_top</i>
                </div>
                {propertiesDropDowns.marginTop && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={5}
                            value={elementStyle.marginTop || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handleMargin('marginTop', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {elementStyle.marginTop || 0}px
                </div>
                </div>}
            </div>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('marginBottom', propertiesDropDowns.marginBottom ? false : true)}>
                    <i className="material-icons">vertical_align_bottom</i>
                </div>
                {propertiesDropDowns.marginBottom && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={5}
                            value={elementStyle.marginBottom || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handleMargin('marginBottom', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {elementStyle.marginBottom || 0}px
                </div>
                </div>}
            </div>
        </React.Fragment>
    );
}

export default Margin;
import React, { useState, useContext, useEffect } from 'react';
import { ChromePicker } from 'react-color';
import classnames from "classnames";
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const FontStyles = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [fontStyle, setFontStyle] = useState({});

    useEffect(() => {
        setFontStyle({
            color: _get(popupState.entities.elements[selectedElement], 'elementStyle.color', "#000000"),
            backgroundColor: _get(popupState.entities.elements[selectedElement], 'elementStyle.backgroundColor', "#ffffff"),
            fontSize: _get(popupState.entities.elements[selectedElement], 'elementStyle.fontSize', null)
        });
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handleFontStyle = (name, value) => {
        setFontStyle({
            ...fontStyle,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    const fontSize = () => {
        let size = [];
        for (let i = 1; i <= 40; i++) {
            size.push(<div className={classnames("option", fontStyle.fontSize === i && "active")} onClick={() => handleFontStyle("fontSize", i)}>{i}</div>)
        }
        return size;
    }

    return (
        <React.Fragment>
            <div className="property-group color-picker">
                <div className="property" onClick={() => handleThisPopup('colorPopup', propertiesDropDowns.colorPopup ? false : true)}>
                    <i className="material-icons">format_color_text</i>
                </div>
                {propertiesDropDowns.colorPopup &&
                    <div className='color-picker-wrapper'>
                        <ChromePicker color={fontStyle.color} onChange={(e) => handleFontStyle("color", e.hex)} disableAlpha />
                        {/* <button className='color-picker-button' onClick={() => setColorPopup(false)}>Ok</button> */}
                    </div>
                }
            </div>
            <div className="property-group color-picker">
                <div className="property" onClick={() => handleThisPopup('backgroundColorPopup', propertiesDropDowns.backgroundColorPopup ? false : true)}>
                    <i className="material-icons">format_color_fill</i>
                </div>
                {propertiesDropDowns.backgroundColorPopup &&
                    <div className='color-picker-wrapper'>
                        <ChromePicker color={fontStyle.backgroundColor} onChange={(e) => handleFontStyle("backgroundColor", e.hex)} disableAlpha />
                        {/* <button className='color-picker-button' onClick={() => setBackgroundColorPopup(false)}>Ok</button> */}
                    </div>
                }
            </div>
            <div className="property-select">
                <div className="property property-selected" onClick={() => handleThisPopup('fontSizeOption', propertiesDropDowns.fontSizeOption ? false : true)}>
                    <i className="material-icons">format_size</i>
                </div>
                {propertiesDropDowns.fontSizeOption && <div className="property-option">
                    {fontSize()}
                </div>}
            </div>
        </React.Fragment >
    );
}

export default FontStyles;
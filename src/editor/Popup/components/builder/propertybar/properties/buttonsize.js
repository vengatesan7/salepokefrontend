import React, { useContext, useState, useEffect } from 'react';
import classnames from "classnames";
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const ButtonSize = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [buttonSize, setButtonSize] = useState(null);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    useEffect(() => {
        setButtonSize(_get(popupState.entities.elements[selectedElement], 'buttonSize', null));
    }, []);

    const handleButtonSize = (name, value) => {
        setButtonSize(value);
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <div className="property-select">
            <div className="property property-selected" onClick={() => handleThisPopup('buttonSize', propertiesDropDowns.buttonSize ? false : true)}>
                <i className="material-icons">photo_size_select_small</i>
            </div>
            {propertiesDropDowns.buttonSize && <div className="property-option btn-size">
                <div
                    className={classnames("option", buttonSize === "fullwidth" && "active")}
                    onClick={() => handleButtonSize("buttonSize", "fullwidth")}
                >
                    Full Width
                </div>
                <div
                    className={classnames("option", buttonSize === "fittotext" && "active")}
                    onClick={() => handleButtonSize("buttonSize", "fittotext")}
                >
                    Fit To Text
                </div>
            </div>}
        </div>
    );
}

export default ButtonSize;
import React, { useContext, useState, useEffect } from 'react';
import classnames from "classnames";
import _get from "lodash.get";

import { closeAllDropDown } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const TextStyles = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [textStyle, setTextStyle] = useState({});

    useEffect(() => {
        setTextStyle({
            fontWeight: _get(popupState.entities.elements[selectedElement], 'elementStyle.fontWeight', null),
            fontStyle: _get(popupState.entities.elements[selectedElement], 'elementStyle.fontStyle', null),
            textDecoration: _get(popupState.entities.elements[selectedElement], 'elementStyle.textDecoration', null),
        })
    }, [])

    const handleTextStyle = (name, value) => {
        setPropertiesDropDowns(closeAllDropDown());
        setTextStyle({
            ...textStyle,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }
    return (
        <div className="property-group">
            <div
                className={classnames("property", textStyle.fontWeight === "bold" && "active")}
                onClick={() => handleTextStyle("fontWeight", textStyle.fontWeight === null ? "bold" : null)}>
                <i className="material-icons">format_bold</i>
            </div>
            <div
                className={classnames("property", textStyle.fontStyle === "italic" && "active")}
                onClick={() => handleTextStyle("fontStyle", textStyle.fontStyle === null ? "italic" : null)}>
                <i className="material-icons">format_italic</i>
            </div>
            <div
                className={classnames("property", textStyle.textDecoration === "underline" && "active")}
                onClick={() => handleTextStyle("textDecoration", textStyle.textDecoration === null ? "underline" : null)}>
                <i className="material-icons">format_underline</i>
            </div>
        </div>
    );
}

export default TextStyles;
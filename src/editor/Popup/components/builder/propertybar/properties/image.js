import React, { useState, useContext, useEffect } from 'react';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import ImageUpload from '../../../../../components/image-upload';
import { PopupStore } from '../../../../store';

const ImageElement = () => {
    const {
        popupState,
        setPopupState,
        selectedElement
    } = useContext(PopupStore);

    const [uploadPopup, setUploadPopup] = useState(false);
    const [widthPopup, setWidthPopup] = useState(false);
    const [style, setStyle] = useState(null);

    useEffect(() => {
        setStyle({
            width: _get(popupState.entities.elements[selectedElement], 'elementStyle.width', 100),
        })
    }, [])

    const handleImageUpload = (name, value) => {
        setUploadPopup(false);
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        [name]: value
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    const handleFontStyle = (name, value) => {
        setStyle({
            ...style,
            [name]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [name]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <React.Fragment>
            <div className="property-group">
                <div className="property" onClick={() => setUploadPopup(uploadPopup ? false : true)}>
                    <i className="material-icons">perm_media</i>
                </div>
                {uploadPopup && <ImageUpload imageLink={(e) => handleImageUpload('url', e)} popupClose={() => setUploadPopup(false)} />}
            </div>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => setWidthPopup(widthPopup ? false : true)}>
                    <i className="material-icons">settings_overscan</i>
                </div>
                {widthPopup && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={5}
                            value={style.width || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handleFontStyle('width', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {style.width || 0}%
                </div>
                </div>}
            </div>
        </React.Fragment>
    );
}

export default ImageElement;
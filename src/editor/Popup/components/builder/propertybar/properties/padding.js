import React, { useContext, useState, useEffect } from 'react';
import Slider from 'react-rangeslider';
import _get from "lodash.get";

import { handlePopup } from '../handleDropDowns';
import { PopupStore } from '../../../../store';

const Padding = () => {
    const {
        popupState,
        setPopupState,
        selectedElement,
        propertiesDropDowns,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const [elementPaddingStyle, setElementPaddingStyle] = useState(null);

    useEffect(() => {
        setElementPaddingStyle({
            paddingTop: _get(popupState.entities.elements[selectedElement], 'elementStyle.paddingTop', 0),
            paddingRight: _get(popupState.entities.elements[selectedElement], 'elementStyle.paddingRight', 0),
            paddingBottom: _get(popupState.entities.elements[selectedElement], 'elementStyle.paddingBottom', 0),
            paddingLeft: _get(popupState.entities.elements[selectedElement], 'elementStyle.paddingLeft', 0),
        })
    }, []);

    const handleThisPopup = (name, value) => {
        return setPropertiesDropDowns(handlePopup(name, value));
    }

    const handlePadding = (nameOne, nameTwo, value) => {
        setElementPaddingStyle({
            ...elementPaddingStyle,
            [nameOne]: value,
            [nameTwo]: value
        })
        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [selectedElement]: {
                        ...popupState.entities.elements[selectedElement],
                        elementStyle: {
                            ...popupState.entities.elements[selectedElement].elementStyle,
                            [nameOne]: value,
                            [nameTwo]: value
                        }
                    }
                }
            }
        }
        return setPopupState(newUpdate);
    }

    return (
        <React.Fragment>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('verticalPadding', propertiesDropDowns.verticalPadding ? false : true)}>
                    <i className="material-icons" style={{ transform: "rotate(90deg)" }}>settings_ethernet</i>
                </div>
                {propertiesDropDowns.verticalPadding && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={5}
                            value={elementPaddingStyle.paddingTop || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handlePadding('paddingTop', 'paddingBottom', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {elementPaddingStyle.paddingTop || 0}px
                </div>
                </div>}
            </div>
            <div className="property-group rangeslider">
                <div className="property" onClick={() => handleThisPopup('horizontalPadding', propertiesDropDowns.horizontalPadding ? false : true)}>
                    <i className="material-icons">settings_ethernet</i>
                </div>
                {propertiesDropDowns.horizontalPadding && <div className="rangeslider-wrapper">
                    <div className="rangeslider-slider">
                        <Slider min={0} max={100} step={5}
                            value={elementPaddingStyle.paddingRight || 0}
                            orientation="horizontal"
                            tooltip={false}
                            onChange={(e) => handlePadding('paddingRight', 'paddingLeft', e)}
                        />
                    </div>
                    <div className="rangeslider-value">
                        {elementPaddingStyle.paddingRight || 0}px
                </div>
                </div>}
            </div>
        </React.Fragment>
    );
}

export default Padding;
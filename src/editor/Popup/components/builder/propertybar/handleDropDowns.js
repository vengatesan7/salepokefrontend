export const handlePopup = (name, value) => {
    const closeAllPopup = {
        buttonSize: false,
        inputColor: false,
        inputBackgroundColor: false,
        buttonColor: false,
        buttonBackgroundColor: false,
        colorPopup: false,
        fontSizeOption: false,
        backgroundColorPopup: false,
        alignOption: false,
        verticalPadding: false,
        horizontalPadding: false,
        radius: false,
        elementBorder: false,
        elementLink: false,
        marginTop: false,
        marginBottom: false
    }
    const newPopupStatus = {
        ...closeAllPopup,
        [name]: value
    }
    return newPopupStatus;
}

export const closeAllDropDown = () => {
    const closeAllPopup = {
        buttonSize: false,
        inputColor: false,
        inputBackgroundColor: false,
        buttonColor: false,
        buttonBackgroundColor: false,
        colorPopup: false,
        fontSizeOption: false,
        backgroundColorPopup: false,
        alignOption: false,
        verticalPadding: false,
        horizontalPadding: false,
        radius: false,
        elementBorder: false,
        elementLink: false,
        marginTop: false,
        marginBottom: false
    }
    return closeAllPopup;
}
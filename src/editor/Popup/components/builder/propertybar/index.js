import React, { useContext, useEffect } from 'react';

import './propertybar.scss';
import { closeAllDropDown } from './handleDropDowns';
import { PopupStore } from '../../../store';
import Alignment from './properties/alignment';
import FontStyles from './properties/fontstyle';
import TextStyles from './properties/textStyle';
import DeleteElement from './properties/delete';
import ImageElement from './properties/image';
import Padding from './properties/padding';
import SubscriptionFormProperties from './properties/subscriptionform';
import ButtonSize from './properties/buttonsize';
import Border from './properties/border';
import LinkElement from './properties/link';
import Margin from './properties/margin';

const PropertyBar = () => {
    const {
        popupState,
        setPropertyBarActive,
        selectedElement,
        setSelectedElement,
        setPropertiesDropDowns
    } = useContext(PopupStore);

    const closePropertyBar = () => {
        setPropertiesDropDowns(closeAllDropDown());
        setPropertyBarActive(false);
        setSelectedElement(null);
    }

    const elementTypeStyles = (type) => {
        switch (type) {
            case "text":
                return (
                    <React.Fragment>
                        <TextStyles />
                        <Alignment />
                        <FontStyles />
                        <Border />
                        <Padding />
                        <Margin />
                        {/* <DeleteElement /> */}
                    </React.Fragment>
                );
            case "image":
                return (
                    <React.Fragment>
                        <Alignment />
                        <ImageElement />
                        <Padding />
                        <Margin />
                        {/* <DeleteElement /> */}
                    </React.Fragment>
                );
            case "subscriptionform":
                return (
                    <React.Fragment>
                        <SubscriptionFormProperties />
                    </React.Fragment>
                );
            case "button":
                return (
                    <React.Fragment>
                        <TextStyles />
                        <Alignment />
                        <FontStyles />
                        <LinkElement />
                        <ButtonSize />
                        <Border />
                        <Padding />
                        <Margin />
                    </React.Fragment>
                );
            default:
                break;
        }
    }

    return (
        <React.Fragment>
            <div className="property-bar-container" onMouseDown={() => closePropertyBar()} />
            <div className="property-bar">
                {elementTypeStyles(popupState.entities.elements[selectedElement].type)}
            </div>
        </React.Fragment>
    );
}

export default PropertyBar;
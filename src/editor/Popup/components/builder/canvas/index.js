import React, { useContext, useEffect, useState } from 'react';
import { normalize } from "normalizr";
import _get from "lodash.get";

import './canvas.scss';
import { PopupStore } from '../../../store';
import CanvasColumn from './column';
import PropertyBar from '../propertybar';

const getNormalizedData = (data, schema) => normalize(data, schema);

const PopupEditorCanvas = () => {
    const {
        popupState,
        propertyBarActive
    } = useContext(PopupStore);

    const [popupStyle, setPopupStyle] = useState(null);

    useEffect(() => {
        if (popupState !== null) {
            const backgroundImageUrl = _get(popupState, 'result.popup.popupStyle.backgroundImage', '')
            const overAllStyle = {
                width: _get(popupState, 'result.popup.popupStyle.width', ''),
                height: _get(popupState, 'result.popup.popupStyle.height', ''),
                backgroundColor: _get(popupState, 'result.popup.popupStyle.backgroundColor', ''),
                backgroundImage: backgroundImageUrl !== '' && `url(${backgroundImageUrl})`,
                // paddingBottom: _get(popupState, 'result.popup.popupStyle.paddingBottom', ''),
                // paddingLeft: _get(popupState, 'result.popup.popupStyle.paddingLeft', ''),
                // paddingRight: _get(popupState, 'result.popup.popupStyle.paddingRight', ''),
                // paddingTop: _get(popupState, 'result.popup.popupStyle.paddingTop', ''),
            }
            setPopupStyle(overAllStyle)
        }
    }, [popupState]);

    return (
        <div className="main-content">
            {popupState !== null &&
                <div className="popup-container" style={popupStyle}>
                    <div className="popup-content">
                        <CanvasColumn state={popupState} />
                    </div>
                </div>
            }
            {propertyBarActive && <PropertyBar />}
        </div>
    );
}

export default PopupEditorCanvas;
import React, { useState, useEffect, useContext } from 'react';
import ContentEditable from "react-contenteditable";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const TextElement = ({ state, elementId, tagName = "p" }) => {
    const {
        popupState,
        setPopupState
    } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);

    useEffect(() => {
        setElementData(state.entities.elements[elementId]);
    }, [elementId]);

    const handleChange = (value) => {
        setElementData({
            ...elementData,
            content: value
        });
        setPopupState({
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [elementId]: {
                        ...popupState.entities.elements[elementId],
                        content: value
                    }
                }
            }
        });
    }
    // const handleUpdateState = (value) => {
    //     console.log(value);
    // }

    return (
        <ContentEditable
            html={elementData && elementData.content}
            disabled={false}
            tagName={tagName}
            onChange={e => handleChange(e.target.value)}
        // onBlur={e => handleUpdateState(e.innerHMTL)}
        />
    )
}

export default TextElement;
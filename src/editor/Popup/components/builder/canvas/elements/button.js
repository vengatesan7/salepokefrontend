import React, { useState, useEffect, useContext } from 'react';
import ContentEditable from "react-contenteditable";
import _get from "lodash.get";

import { PopupStore } from '../../../../store';

const ButtonElement = ({ state, elementId, tagName = "span" }) => {
    const {
        popupState,
        setPopupState
    } = useContext(PopupStore);
    const [elementData, setElementData] = useState(null);
    const [eTextAlign, setETextAlign] = useState(null);

    const getData = () => {
        const textAlign = _get(popupState.entities.elements[elementId].elementStyle, 'textAlign', 'center')
        const elementDataFind = JSON.parse(JSON.stringify(popupState.entities.elements[elementId]));
        delete elementDataFind.elementStyle.textAlign;
        setETextAlign(textAlign);
        setElementData({
            ...elementDataFind,
            elementStyle: {
                ...elementDataFind.elementStyle,
                float: textAlign === 'center' ? 'none' : textAlign,
                width: elementDataFind.buttonSize === 'fullwidth' ? "100%" : 'auto',
            }
        });
    }

    // const getData = () => {
    //     const textAlign = _get(popupState.entities.elements[elementId].elementStyle, 'textAlign', 'center')
    //     const elementDataFind = new Object(popupState.entities.elements[elementId]);

    //     setElementData({
    //         content: elementDataFind.content,
    //         elementStyle: {
    //             float: textAlign === 'center' ? 'none' : textAlign,
    //             width: elementDataFind.buttonSize === 'fullwidth' ? "100%" : 'auto',
    //             backgroundColor: _get(popupState.entities.elements[elementId].elementStyle, 'backgroundColor', '#007bff'),
    //             borderRadius: _get(popupState.entities.elements[elementId].elementStyle, 'borderRadius', 2),
    //             borderWidth: _get(popupState.entities.elements[elementId].elementStyle, 'borderWidth', 0),
    //             color: _get(popupState.entities.elements[elementId].elementStyle, 'color', "#ffffff"),
    //             paddingBottom: _get(popupState.entities.elements[elementId].elementStyle, 'paddingBottom', 5),
    //             paddingLeft: _get(popupState.entities.elements[elementId].elementStyle, 'paddingLeft', 15),
    //             paddingRight: _get(popupState.entities.elements[elementId].elementStyle, 'paddingRight', 15),
    //             paddingTop: _get(popupState.entities.elements[elementId].elementStyle, 'paddingTop', 5),
    //         }
    //     });
    // }

    useEffect(() => {
        getData();
    }, [elementId]);
    useEffect(() => {
        getData();
    }, [popupState]);

    const handleChange = (value) => {
        setElementData({
            ...elementData,
            content: value
        });
        setPopupState({
            ...popupState,
            entities: {
                ...popupState.entities,
                elements: {
                    ...popupState.entities.elements,
                    [elementId]: {
                        ...popupState.entities.elements[elementId],
                        content: value
                    }
                }
            }
        });
    }

    return (
        <div style={{ textAlign: eTextAlign }}>
            <ContentEditable
                html={elementData && elementData.content}
                disabled={false}
                tagName={tagName}
                onChange={e => handleChange(e.target.value)}
                className="popup-button-element"
                style={elementData && elementData.elementStyle}
            />
        </div>
    )
}

export default ButtonElement;
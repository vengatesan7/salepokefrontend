import React, { useContext, useEffect, useState } from 'react';
import { Droppable, Draggable } from "react-beautiful-dnd";
import classnames from 'classnames';
import _get from "lodash.get";

import TextElement from './elements/text';
import ImageElement from './elements/image';
import { PopupStore } from '../../../store';
import SubscriptionForm from './elements/subscriptionform';
import ButtonElement from './elements/button';

const Element = ({ state, elementId, elementType, style }) => {
    const elementDomValue = (type) => {
        switch (type) {
            case "text":
                return <TextElement state={state} elementId={elementId} />;
            case "image":
                return <ImageElement state={state} elementId={elementId} />;
            case "subscriptionform":
                return <SubscriptionForm state={state} elementId={elementId} />;
            case "button":
                return <ButtonElement state={state} elementId={elementId} />;
            default:
                break;
        }
    }
    return (
        <div style={style}>
            {elementDomValue(elementType)}
        </div>
    );
}

const CanvasElement = ({ state, columnId }) => {
    const {
        popupState,
        setPopupState,
        setPropertyBarActive,
        selectedElement,
        setSelectedElement,
        isNewElementDrag
    } = useContext(PopupStore);

    const handleDeleteElement = (columnId, elementId) => {
        const newElementsArray = popupState.entities.columns[columnId].elements;
        const oldElementPosition = newElementsArray.indexOf(elementId);
        newElementsArray.splice(oldElementPosition, 1);
        // const newElementList = popupState.entities.elements;
        // delete newElementList[elementId];

        const newUpdate = {
            ...popupState,
            entities: {
                ...popupState.entities,
                columns: {
                    ...popupState.entities.columns,
                    [columnId]: {
                        ...popupState.entities.columns[columnId],
                        elements: newElementsArray
                    }
                },
                // elements: newElementList
            }
        }
        return setPopupState(newUpdate);
    }

    const handleEditElement = (elementId) => {
        setPropertyBarActive(true);
        setSelectedElement(elementId);
    }

    return (
        <Droppable droppableId={columnId} type="element">
            {(elementProvided, elementSnapshot) => (
                <div className={classnames("popup-element-wapper", elementSnapshot.isDraggingOver && "on-dragging")}
                    {...elementProvided.droppableProps}
                    ref={elementProvided.innerRef}
                    isDraggingOver={elementSnapshot.isDraggingOver}
                >
                    {state.entities.columns[columnId].elements.map((elementId, elementIndex) => {
                        const elementType = state.entities.elements[elementId].type;
                        const elementStyle = _get(state.entities.elements[elementId], "elementStyle", null);
                        return (
                            <Draggable draggableId={elementId} index={elementIndex}>
                                {elementDragProvided => (
                                    <div
                                        className={classnames("popup-element", selectedElement === elementId && "element-editable", isNewElementDrag && "new-element-dragging")}
                                        {...elementDragProvided.draggableProps}
                                        ref={elementDragProvided.innerRef}
                                    // onClick={e => handleEditElement(elementId)}
                                    >
                                        <span
                                            className="element-option move"
                                            {...elementDragProvided.dragHandleProps}
                                        >
                                            <i className="material-icons">control_camera</i>
                                        </span>
                                        <span
                                            className="element-option delete"
                                            onClick={e => handleDeleteElement(columnId, elementId)}
                                        >
                                            <i className="material-icons">delete</i>
                                        </span>
                                        <span
                                            className="element-option edit"
                                            onClick={e => handleEditElement(elementId)}
                                        >
                                            <i className="material-icons">edit</i>
                                        </span>
                                        {elementType === "button" ? <Element
                                            state={state}
                                            elementId={elementId}
                                            elementType={state.entities.elements[elementId].type}
                                        /> : <Element
                                                state={state}
                                                elementId={elementId}
                                                elementType={state.entities.elements[elementId].type}
                                                style={elementType === "image" ? { ...elementStyle, width: "100%" } : elementStyle}
                                            />
                                        }
                                    </div>
                                )}
                            </Draggable>
                        );
                    })}
                </div>
            )}
        </Droppable>
    );
}

export default CanvasElement;
import React, { useContext, useEffect, useState } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import _get from "lodash.get";

import CanvasElement from "./element";
import { PopupStore } from "../../../store";

const CanvasColumn = ({ state }) => {
  const { popupState, setPopupState } = useContext(PopupStore);
  const [columnCount, setColumnCount] = useState(
    state.entities.content[state.result.content[0]].columns.length
  );
  const deleteColumn = (contentId, columnID) => {
    const newColumnArray = popupState.entities.content[contentId].columns;
    const columnPosition = newColumnArray.indexOf(columnID);
    newColumnArray.splice(columnPosition, 1);

    const newColumns = {
      ...popupState.entities.columns
    };
    delete newColumns[columnID];

    const newState = {
      ...popupState,
      entities: {
        ...popupState.entities,
        content: {
          ...popupState.entities.content,
          [contentId]: {
            ...popupState.entities.content[contentId],
            columns: newColumnArray
          }
        }
      }
    };
    setPopupState(newState);
  };
  return state.result.content.map((contentId, index) => {
    return (
      <Droppable droppableId={contentId} type="column" direction="horizontal">
        {(columnProvided, snapshot) => (
          <div
            className="popup-column-wapper"
            {...columnProvided.droppableProps}
            ref={columnProvided.innerRef}
            isDraggingOver={snapshot.isDraggingOver}
          >
            {state.entities.content[contentId].columns.map(
              (columnId, columnIndex) => {
                const columnStyle = _get(
                  state.entities.columns[columnId],
                  "columnStyle",
                  null
                );
                return (
                  <Draggable draggableId={columnId} index={columnIndex}>
                    {columnDragProvided => (
                      <div
                        className="popup-column"
                        {...columnDragProvided.draggableProps}
                        ref={columnDragProvided.innerRef}
                        style={{
                          ...columnStyle,
                          width: `${100 / columnCount}%`
                        }}
                      >
                        <span
                          className="column-option move"
                          {...columnDragProvided.dragHandleProps}
                        >
                          <i className="material-icons">control_camera</i>
                        </span>
                        <span
                          className="column-option delete"
                          onClick={() => deleteColumn(contentId, columnId)}
                        >
                          <i className="material-icons">delete</i>
                        </span>
                        <CanvasElement state={state} columnId={columnId} />
                      </div>
                    )}
                  </Draggable>
                );
              }
            )}
          </div>
        )}
      </Droppable>
    );
  });
};

export default CanvasColumn;

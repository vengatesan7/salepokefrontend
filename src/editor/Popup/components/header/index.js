import React, { useContext, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { denormalize } from "normalizr";
import { toast } from 'react-toastify';
import cookie from 'react-cookies';

import logo from "../../../../images/rc-logo-mini.png";
import "./header.scss";
import Schema from '../../store/schema';
import { PostData } from '../../../../services/PostData';
import { PopupStore } from '../../store';



const PopupEditorHeader = () => {
    const { popupState, popupName } = useContext(PopupStore);

    const [saveLoader, setSaveLoader] = useState(false);

    const sampleJSON = {
        "popup": {
            "closeButton": true,
            "overlayColor": "#000000",
            "startTime": 2,
            "popupStyle": {
                "width": 600,
                "height": 320,
                "backgroundColor": "#2d2d2d",
                "backgroundImage": null
            }
        },
        "header": [
            {
                "id": "1f787bf7-9911-4500-930a-24d2b1a722e8",
                "enable": false,
                "headerStyle": {
                    "backgroundColor": "#cccccc",
                    "backgroundImage": "https://wallpaperaccess.com/full/125024.jpg"
                },
                "columns": [
                    {
                        "id": "23afa346-025b-4b4b-912b-aac8a8be6ce1",
                        "elements": [
                            {
                                "id": "ca1ee7af-eb80-46ea-bd6f-cd00b42f8590",
                                "type": "text",
                                "content": "Header Text"
                            }
                        ]
                    }
                ]
            }
        ],
        "content": [
            {
                "id": "99af3fea-92ef-40d9-bc41-f7eecbab656b",
                "contentStyle": {
                    "backgroundColor": "#cccccc",
                    "backgroundImage": null
                },
                "columns": [
                    {
                        "id": "4e6c2160-95b8-4c1d-8b12-9063e5ca586c",
                        "elements": [
                            {
                                "id": "2872a192-9b8e-4ffc-be85-4f40f03e460b",
                                "type": "image",
                                "url": "https://micro4.cisconet.world/public/uploads/imagelibraries/user_83/1580049269.png",
                                "elementStyle": {
                                    "textAlign": "center",
                                    "width": 50,
                                    "paddingTop": 80,
                                    "paddingRight": 0,
                                    "paddingBottom": 80,
                                    "paddingLeft": 0
                                }
                            }
                        ]
                    },
                    {
                        "id": "45e641ce-c8d2-48a9-a24f-6d8521197a58",
                        "columnStyle": {
                            "backgroundColor": "#ffffff",
                            "paddingLeft": 15,
                            "paddingRight": 15,
                            "paddingTop": 25,
                            "paddingBottom": 25
                        },
                        "elements": [
                            {
                                "id": "829ed1ee-4dd3-422c-8064-b2e6c9fea116",
                                "type": "text",
                                "content": "Welcome!",
                                "elementStyle": {
                                    "textAlign": "center",
                                    "fontSize": 34,
                                    "paddingTop": 0,
                                    "paddingRight": 0,
                                    "paddingBottom": 0,
                                    "paddingLeft": 0
                                }
                            },
                            {
                                "id": "3b273fa6-5ce5-4f7b-9795-28b1afea6af8",
                                "type": "text",
                                "content": "As a first-time visitor we would like to<div>offer you a complimentary</div>",
                                "elementStyle": {
                                    "textAlign": "center",
                                    "fontSize": 14,
                                    "paddingTop": 0,
                                    "paddingRight": 0,
                                    "paddingBottom": 15,
                                    "paddingLeft": 0
                                }
                            },
                            {
                                "id": "ef61801d-62f0-40f9-8ddd-07a8f9535879",
                                "type": "text",
                                "content": "30% Discount Coupon",
                                "elementStyle": {
                                    "textAlign": "center",
                                    "fontSize": 20,
                                    "color": "#ed6a62",
                                    "backgroundColor": "#feeeee",
                                    "borderColor": "#ed6a62",
                                    "borderWidth": 1,
                                    "borderStyle": "solid",
                                    "borderRadius": 50,
                                    "paddingTop": 5,
                                    "paddingRight": 0,
                                    "paddingBottom": 5,
                                    "paddingLeft": 0,
                                    "fontStyle": null,
                                    "fontWeight": null,
                                    "textDecoration": null
                                }
                            }
                        ]
                    }
                ]
            }
        ],
        "footer": [
            {
                "id": "3c59a90e-fb74-41a5-90b9-a9d984859d6b",
                "enable": false,
                "headerStyle": {
                    "backgroundColor": "#cccccc",
                    "backgroundImage": "https://wallpaperaccess.com/full/125024.jpg"
                },
                "columns": [
                    {
                        "id": "63eeed31-22a8-4561-ac8d-0bf60d5dcf3f",
                        "elements": [
                            {
                                "id": "fe81159b-af07-44de-9dfd-98051564c742",
                                "type": "link",
                                "content": "No thanks, I'm already all knowing"
                            }
                        ]
                    }
                ]
            }
        ]
    }

    const savePopup = () => {
        setSaveLoader(true);
        const denormalizeData = denormalize(popupState.result, Schema, popupState.entities);
        const saveData = {
            "pop_up_id": cookie.load("popupId"),
            "pop_up_json_code": JSON.stringify(denormalizeData)
        }
        PostData('ms4', 'popupsavejson', saveData).then(response => {
            setSaveLoader(false);
            if (response !== 'Invalid') {
                if (response.status === "success") {
                    toast.info(response.message);
                }
            } else {
                toast.warn("Update is not saved");
            }
        });
    }
    return (
        <header
            className="pope-header"
        >
            <div className="pope-header-left">
                <div className="logo">
                    <Link to="/dashboard">
                        <img src={logo} alt="logo" />
                    </Link>
                </div>
                <h3 className="title">
                    <Link to="/popup">
                        <i className="material-icons">keyboard_backspace</i>
                        Back to popup list
                    </Link>
                </h3>
            </div>
            <div className="pope-header-center">
                <span className="pope-header-name">{popupName}</span>
            </div>
            <div className="pope-header-right">
                <Button
                    onClick={() => savePopup()}
                    variant="primary"
                    style={{ float: "right" }}
                >{saveLoader ? "Loading..." : " Save "}</Button>
            </div>
        </header>
    );
}

export default PopupEditorHeader;
import { schema } from "normalizr";

const element = new schema.Entity("elements");

const columns = new schema.Entity("columns", {
    elements: [element]
});
const rows = new schema.Entity("rows", {
    columns: [columns]
});
const footer = new schema.Entity("footer", {
    columns: [columns]
});
const content = new schema.Entity("content", {
    columns: [columns]
});
const header = new schema.Entity("header", {
    columns: [columns]
});
const popup = new schema.Entity("popup");

const Schema = {
    popup: { popup },
    header: [header],
    content: [content],
    footer: [footer],
    rows: [rows],
    columns: [columns],
    elements: [element]
};

export default Schema;
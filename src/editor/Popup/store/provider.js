import React, { useState } from "react";

import { PopupStore } from "./index";

const PopupDataProvider = props => {
    const [popupName, setPopupName] = useState(null);
    const [popupState, setPopupState] = useState(null);
    const [selectedElement, setSelectedElement] = useState(null);
    const [propertyBarActive, setPropertyBarActive] = useState(false);
    const [isNewElementDrag, setIsNewElementDrag] = useState(false);
    const [propertiesDropDowns, setPropertiesDropDowns] = useState({
        buttonSize: false,
        inputColor: false,
        inputBackgroundColor: false,
        buttonColor: false,
        buttonBackgroundColor: false,
        colorPopup: false,
        fontSizeOption: false,
        backgroundColorPopup: false,
        alignOption: false,
        verticalPadding: false,
        horizontalPadding: false,
        radius: false,
        elementBorder: false,
        elementLink: false,
        marginTop: false,
        marginBottom: false
    });

    const store = {
        popupName,
        setPopupName,
        popupState,
        setPopupState,
        propertyBarActive,
        setPropertyBarActive,
        selectedElement,
        setSelectedElement,
        isNewElementDrag,
        setIsNewElementDrag,
        propertiesDropDowns,
        setPropertiesDropDowns,
    };
    return <PopupStore.Provider value={store}>{props.children}</PopupStore.Provider>;
};

export default PopupDataProvider;

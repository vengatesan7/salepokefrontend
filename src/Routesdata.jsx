import Index from "./views/Index";
import Profile from "./views/examples/Profile";
// import Maps from "views/examples/Maps.jsx";
import Register from "./views/examples/Register";
import Login from "./views/examples/Login.jsx";
import Inviteteam from "./views/examples/Inviteteam.jsx";
import Createclient from "./views/examples/Createclient.jsx";
import Tables from "./views/examples/Tables.jsx";
import Icons from "./views/examples/Icons.jsx";
import Clients from "./views/examples/Clients.jsx";
import Teammembers from "./views/examples/Teammembers.jsx";
import Forgetpassword from "./views/examples/Forgetpassword.jsx";
import Updatepassword from "./views/examples/Changepassword.jsx";
import Campaignlist from "./views/examples/Campaignlist.jsx";
import Addnewcampaign from "./views/examples/Addnewcampaign.jsx";
import Selectintegration from "./views/examples/Selectintegration.jsx";
import Quickemail from "./views/examples/Quickemail.jsx";
import Quicksms from "./views/examples/Quicksms.jsx";
import Leads from "./views/examples/Leads.jsx";
import Addnewintegration from "./views/examples/Addnewintegrarion.jsx";
import Updatequickemail from "./views/examples/Updatequickemail.jsx";
import Campagindetail from "./views/examples/Campagindetail.jsx";
import Profiledetail from "./views/examples/Profiledetail.jsx";
import Activitylog from "./views/examples/Activitylog.jsx";
import Switchaccounts from "./views/examples/Switchaccounts.jsx";
import Autoresponders from "./views/examples/Autoresponders.jsx"; 
import Choosetemplate from "./views/examples/Pickyourtemplate.jsx"; 
import Displayrules from "./views/examples/Displayrules.jsx"; 

import PopupBuilder from "./editor/PopupNew/pages/PopupBuilder"
import Addnewdomain from "./views/examples/Addnewdomain";
import Addnewchannel from "./views/examples/Addnewchannel.jsx";
import Install from "./views/examples/Install.jsx";
import Socialproof from "./views/examples/Socialprooflist.jsx";
import SocialprooffOverview from "./views/examples/SocialprooffOverview.jsx";

import Pulseeditor from "./views/examples/Pulseeditor.jsx"; 



export const allRoute = [
    {
      path: "/index",
      name: "Dashboard",
      icon: "ni ni-world text-primary",
      component: Index,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/campaigns",
      name: "Campaign",
      icon: "ni ni-send text-blue",
      component: Campaignlist,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/leads",
      name: "Leads",
      icon: "ni ni-collection text-blue",
      component: Leads,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/clients-list",
      name: "Clients",
      icon: "ni ni-hat-3 text-blue",
      component: Clients,
      layout: "/admin"
    },

    {
      path: "/invite-team",
      name: "Invite team",
      icon: "ni ni-active-40 text-blue",
      component: Inviteteam,
      layout: "/plain"
    },

    {
      path: "/create-client",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Createclient,
      layout: "/plain"
    },
    {
      path: "/select-integration",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Selectintegration,
      layout: "/plain"
    },
    {
      path: "/quickemail",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Quickemail,
      layout: "/plain"
    },
    {
      path: "/quicksms",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Quicksms,
      layout: "/plain"
    },
    {
      path: "/update-quick-email",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Updatequickemail,
      layout: "/plain"
    },
    {
      path: "/add-new-integration",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Addnewintegration,
      layout: "/plain"
    },
    {
      path: "/add-new-campaign",
      // name: "Invite team",
      // icon: "ni ni-active-40 text-blue",
      component: Addnewcampaign,
      layout: "/plain"
    },

    {
      path: "/team-member-list",
      name: "Team Members",
      icon: "ni ni-circle-08 text-blue",
      component: Teammembers,
      layout: "/admin"
    },
    {
      path: "/integrations",
      name: "Integrations",
      icon: "ni ni-spaceship text-blue",
      component: Autoresponders,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/user-profile",
      name: "Settings",
      icon: "ni ni-settings-gear-65 text-blue",
      component: Profile,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/campagin-detail",
      // name: "Team Members",
      // icon: "ni ni-circle-08 text-blue",
      component: Campagindetail,
      layout: "/admin"
    },
    {
      path: "/profile-detail",
      // name: "Leads Detail",
      // icon: "ni ni-circle-08 text-blue",
      component: Profiledetail,
      layout: "/admin",
      menu: "no"
    },

    // {
    //   path: "/tables",
    //   name: "Tables",
    //   icon: "ni ni-bullet-list-67 text-red",
    //   component: Tables,
    //   layout: "/admin"
    // },
    {
      path: "/login",
      // name: "Login",
      // icon: "ni ni-key-25 text-info",
      component: Login,
      layout: "/auth"
    },
    {
      path: "/forgetpassword",
      // name: "Login",
      // icon: "ni ni-key-25 text-info",
      component: Forgetpassword,
      layout: "/auth"
    },
    {
      path: "/activity-log",
      // name: "Activitylog",
      // icon: "ni ni-hat-3 text-blue",
      component: Activitylog,
      layout: "/admin"
    },
    {
      path: "/register",
      // name: "Register",
      // icon: "ni ni-circle-08 text-pink",
      component: Register,
      layout: "/auth"
    },
    {
      path: "/change-password",
      // name: "Login",
      // icon: "ni ni-key-25 text-info",
      component: Updatepassword,
      layout: "/auth"
    }
    // {
    //   path: "/icons",
    //   name: "Icons",
    //   icon: "ni ni-planet text-blue",
    //   component: Icons,
    //   layout: "/admin",
    //   type:'mainmenu'
    // },
  ];

  export const agencyRoute = [
    {
      path: "/builder",
      name: "Builder",
      icon: "ni ni-collection text-blue",
      component: PopupBuilder,
      layout: "/editor",
      // type: "mainmenu"
    },
    {
      path: "/pulse-editor",
      name: "Builder",
      icon: "ni ni-collection text-blue",
      component: Pulseeditor,
      layout: "/editor",
      // type: "mainmenu"
    },
   {
      path: "/index",
      name: "Dashboard",
      icon: "poll",
      component: Index,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/index",
      name: "Sites",
      icon: "web",
      component: Index,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/choose-template",
      component: Choosetemplate,
      layout: "/plain"
    },
    {
      path: "/displayrules",
      component: Displayrules,
      layout: "/plain"
    },
    {
      path: "/addnewchannel",
      component: Addnewchannel,
      layout: "/plain"
    },
    {
      path: "/addnewdomain",
      component: Addnewdomain,
      layout: "/plain"
    },
    {
      path: "/switch-account",
      component: Switchaccounts,
      layout: "/plain",
    },
    {
      path: "/campaigns",
      name: "Campaign",
      icon: "open_in_new",
      component: Campaignlist,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/socialproof",
      name: "Socialproof",
      icon: "flash_on",
      component: Socialproof,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/leads",
      name: "Leads",
      icon: "account_circle",
      component: Leads,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/clients-list",
      name: "Clients",
      icon: "launch",
      component: Clients,
      layout: "/admin"
    },

    {
      path: "/invite-team",
      name: "Invite team",
      icon: "ni ni-active-40 text-blue",
      component: Inviteteam,
      layout: "/plain"
    },

    {
      path: "/create-client",
      component: Createclient,
      layout: "/plain"
    },
    {
      path: "/select-integration",
      component: Selectintegration,
      layout: "/plain"
    },
    {
      path: "/quickemail",
      component: Quickemail,
      layout: "/plain"
    },
    {
      path: "/quicksms",
      component: Quicksms,
      layout: "/plain"
    },
    {
      path: "/update-quick-email",
      component: Updatequickemail,
      layout: "/plain"
    },
    {
      path: "/add-new-integration",
      component: Addnewintegration,
      layout: "/plain"
    },
    {
      path: "/add-new-campaign",
      component: Addnewcampaign,
      layout: "/plain"
    },

    {
      path: "/team-member-list",
      name: "Team Members",
      icon: "ni ni-circle-08 text-blue",
      component: Teammembers,
      layout: "/admin"
    },
    {
      path: "/install",
      name: "Install",
      icon: "code",
      component: Install,
      layout: "/admin",
      type: "mainmenu"
    }, 
    // {
    //   path: "/integrations",
    //   name: "Integrations",
    //   icon: "ni ni-spaceship text-blue",
    //   component: Autoresponders,
    //   layout: "/admin",
    //   type: "mainmenu"
    // },
    {
      path: "/user-profile",
      name: "Settings",
      icon: "settings",
      component: Profile,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/campagin-detail",
      component: Campagindetail,
      layout: "/admin"
    },
    {
      path: "/profile-detail",
      component: Profiledetail,
      layout: "/admin",
      menu: "no"
    },
    {
      path: "/activity-log",
      component: Activitylog,
      layout: "/admin"
    },
    
  ];

  export const premiumRoute = [
    {
      path: "/builder",
      name: "Builder",
      icon: "ni ni-collection text-blue",
      component: PopupBuilder,
      layout: "/editor",
      // type: "mainmenu"
    },
   
    {
      path: "/pulse-editor",
      name: "Builder",
      icon: "ni ni-collection text-blue",
      component: Pulseeditor,
      layout: "/editor",
      // type: "mainmenu"
    },
    {
      path: "/addnewchannel",
      component: Addnewchannel,
      layout: "/plain"
    },
   {
      path: "/index",
      name: "Dashboard",
      icon: "assessment",
      component: Index,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/socialproof",
      name: "Socialproof",
      icon: "flash_on",
      component: Socialproof,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/socialproof/socialprooff-overview",
      name: "Socialproof Overview",
      component: SocialprooffOverview,
      layout: "/admin",
    },
    {
      path: "/choose-template",
      component: Choosetemplate,
      layout: "/plain"
    },
    {
      path: "/displayrules",
      component: Displayrules,
      layout: "/plain"
    },
    {
      path: "/addnewdomain",
      component: Addnewdomain,
      layout: "/plain"
    },
    {
      path: "/switch-account",
      component: Switchaccounts,
      layout: "/plain",
    },
    // {
    //   path: "/campaigns",
    //   name: "Campaign",
    //   icon: "launch",
    //   component: Campaignlist,
    //   layout: "/admin",
    //   type: "mainmenu"
    // },
    // {
    //   path: "/leads",
    //   name: "Leads",
    //   icon: "sentiment_very_satisfied",
    //   component: Leads,
    //   layout: "/admin",
    //   type: "mainmenu"
    // },
    {
      path: "/clients-list",
      name: "Clients",
      icon: "launch",
      component: Clients,
      layout: "/admin"
    },

    {
      path: "/invite-team",
      name: "Invite team",
      icon: "ni ni-active-40 text-blue",
      component: Inviteteam,
      layout: "/plain"
    },

    {
      path: "/create-client",
      component: Createclient,
      layout: "/plain"
    },
    {
      path: "/select-integration",
      component: Selectintegration,
      layout: "/plain"
    },
    {
      path: "/quickemail",
      component: Quickemail,
      layout: "/plain"
    },
    {
      path: "/quicksms",
      component: Quicksms,
      layout: "/plain"
    },
    {
      path: "/update-quick-email",
      component: Updatequickemail,
      layout: "/plain"
    },
    {
      path: "/add-new-integration",
      component: Addnewintegration,
      layout: "/plain"
    },
    {
      path: "/add-new-campaign",
      component: Addnewcampaign,
      layout: "/plain"
    },

    {
      path: "/team-member-list",
      name: "Team Members",
      icon: "ni ni-circle-08 text-blue",
      component: Teammembers,
      layout: "/admin"
    },
    {
      path: "/install",
      name: "Install",
      icon: "code",
      component: Install,
      layout: "/admin",
      type: "mainmenu"
    }, 
    // {
    //   path: "/integrations",
    //   name: "Integrations",
    //   icon: "ni ni-spaceship text-blue",
    //   component: Autoresponders,
    //   layout: "/admin",
    //   type: "mainmenu"
    // },
    {
      path: "/user-profile",
      name: "Settings",
      icon: "settings",
      component: Profile,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/campagin-detail",
      component: Campagindetail,
      layout: "/admin"
    },
    {
      path: "/profile-detail",
      component: Profiledetail,
      layout: "/admin",
      menu: "no"
    },
    {
      path: "/activity-log",
      component: Activitylog,
      layout: "/admin"
    },
    
  ];


//Manager Route

export const managerRoute = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-world text-primary",
    component: Index,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campaigns",
    name: "Campaign",
    icon: "ni ni-send text-blue",
    component: Campaignlist,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/leads",
    name: "Leads",
    icon: "ni ni-collection text-blue",
    component: Leads,
    layout: "/admin",
    type: "mainmenu"
  },

  {
    path: "/invite-team",
    name: "Invite team",
    icon: "ni ni-active-40 text-blue",
    component: Inviteteam,
    layout: "/plain"
  },
  {
    path: "/select-integration",
    component: Selectintegration,
    layout: "/plain"
  },
  {
    path: "/quickemail",
    component: Quickemail,
    layout: "/plain"
  },
  {
    path: "/quicksms",
    component: Quicksms,
    layout: "/plain"
  },
  {
    path: "/update-quick-email",
    component: Updatequickemail,
    layout: "/plain"
  },
  {
    path: "/add-new-integration",
    component: Addnewintegration,
    layout: "/plain"
  },
  {
    path: "/add-new-campaign",
    component: Addnewcampaign,
    layout: "/plain"
  },

  // {
  //   path: "/team-member-list",
  //   name: "Team Members",
  //   icon: "ni ni-circle-08 text-blue",
  //   component: Teammembers,
  //   layout: "/admin"
  // },
  // {
  //   path: "/autoresponders",
  //   name: "Autoresponders",
  //   icon: "ni ni-spaceship text-blue",
  //   // component: Autoresponders,
  //   layout: "/admin",
  //   type: "mainmenu"
  // },
  {
    path: "/user-profile",
    name: "Settings",
    icon: "ni ni-settings-gear-65 text-blue",
    component: Profile,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campagin-detail",
    component: Campagindetail,
    layout: "/admin"
  },
  {
    path: "/profile-detail",
    component: Profiledetail,
    layout: "/admin",
    menu: "no"
  },
  {
    path: "/activity-log",
    component: Activitylog,
    layout: "/admin"
  }
];


//sale Route

export const saleRoute = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-world text-primary",
    component: Index,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campaigns",
    name: "Campaign",
    icon: "ni ni-send text-blue",
    component: Campaignlist,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/leads",
    name: "Leads",
    icon: "ni ni-collection text-blue",
    component: Leads,
    layout: "/admin",
    type: "mainmenu"
  },

  // {
  //   path: "/invite-team",
  //   name: "Invite team",
  //   icon: "ni ni-active-40 text-blue",
  //   component: Inviteteam,
  //   layout: "/plain"
  // },
  // {
  //   path: "/select-integration",
  //   component: Selectintegration,
  //   layout: "/plain"
  // },
  // {
  //   path: "/quickemail",
  //   component: Quickemail,
  //   layout: "/plain"
  // },
  // {
  //   path: "/quicksms",
  //   component: Quicksms,
  //   layout: "/plain"
  // },
  // {
  //   path: "/update-quick-email",
  //   component: Updatequickemail,
  //   layout: "/plain"
  // },
  // {
  //   path: "/add-new-integration",
  //   component: Addnewintegration,
  //   layout: "/plain"
  // },
  // {
  //   path: "/add-new-campaign",
  //   component: Addnewcampaign,
  //   layout: "/plain"
  // },

  // {
  //   path: "/team-member-list",
  //   name: "Team Members",
  //   icon: "ni ni-circle-08 text-blue",
  //   component: Teammembers,
  //   layout: "/admin"
  // },
  // {
  //   path: "/autoresponders",
  //   name: "Autoresponders",
  //   icon: "ni ni-spaceship text-blue",
  //   // component: Autoresponders,
  //   layout: "/admin",
  //   type: "mainmenu"
  // },
  {
    path: "/user-profile",
    name: "Settings",
    icon: "ni ni-settings-gear-65 text-blue",
    component: Profile,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campagin-detail",
    component: Campagindetail,
    layout: "/admin"
  },
  {
    path: "/profile-detail",
    component: Profiledetail,
    layout: "/admin",
    menu: "no"
  },
  {
    path: "/activity-log",
    component: Activitylog,
    layout: "/admin"
  }
];


//clientRoute Route

export const clientRoute = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-world text-primary",
    component: Index,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campaigns",
    name: "Campaign",
    icon: "ni ni-send text-blue",
    component: Campaignlist,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/leads",
    name: "Leads",
    icon: "ni ni-collection text-blue",
    component: Leads,
    layout: "/admin",
    type: "mainmenu"
  },

  // {
  //   path: "/invite-team",
  //   name: "Invite team",
  //   icon: "ni ni-active-40 text-blue",
  //   component: Inviteteam,
  //   layout: "/plain"
  // },
  // {
  //   path: "/select-integration",
  //   component: Selectintegration,
  //   layout: "/plain"
  // },
  // {
  //   path: "/quickemail",
  //   component: Quickemail,
  //   layout: "/plain"
  // },
  // {
  //   path: "/quicksms",
  //   component: Quicksms,
  //   layout: "/plain"
  // },
  // {
  //   path: "/update-quick-email",
  //   component: Updatequickemail,
  //   layout: "/plain"
  // },
  // {
  //   path: "/add-new-integration",
  //   component: Addnewintegration,
  //   layout: "/plain"
  // },
  // {
  //   path: "/add-new-campaign",
  //   component: Addnewcampaign,
  //   layout: "/plain"
  // },

  // {
  //   path: "/team-member-list",
  //   name: "Team Members",
  //   icon: "ni ni-circle-08 text-blue",
  //   component: Teammembers,
  //   layout: "/admin"
  // },
  // {
  //   path: "/autoresponders",
  //   name: "Autoresponders",
  //   icon: "ni ni-spaceship text-blue",
  //   // component: Autoresponders,
  //   layout: "/admin",
  //   type: "mainmenu"
  // },
  {
    path: "/user-profile",
    name: "Settings",
    icon: "ni ni-settings-gear-65 text-blue",
    component: Profile,
    layout: "/admin",
    type: "mainmenu"
  },
  {
    path: "/campagin-detail",
    component: Campagindetail,
    layout: "/admin"
  },
  {
    path: "/profile-detail",
    component: Profiledetail,
    layout: "/admin",
    menu: "no"
  },
  {
    path: "/activity-log",
    component: Activitylog,
    layout: "/admin"
  }
];

//user route

export const userRoute = [
    {
      path: "/index",
      name: "Dashboard",
      icon: "ni ni-world text-primary",
      component: Index,
      layout: "/admin",
      type: "mainmenu"
    },
       {
      path: "/icons",
      name: "Icons",
      icon: "ni ni-planet text-blue",
      component: Icons,
      layout: "/admin",
      type:'mainmenu'
    },
    {
      path: "/user-profile",
      name: "Settings",
      icon: "ni ni-settings-gear-65 text-blue",
      component: Profile,
      layout: "/admin",
      type: "mainmenu"
    },
    {
      path: "/login",
      component: Login,
      layout: "/auth"
    },
    {
      path: "/forgetpassword",
      component: Forgetpassword,
      layout: "/auth"
    },
    {
      path: "/register",
      component: Register,
      layout: "/auth"
    },
    {
      path: "/change-password",
      component: Updatepassword,
      layout: "/auth"
    }
  ];

  
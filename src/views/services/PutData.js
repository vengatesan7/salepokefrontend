import Path from './Path';
import StatusValidation from './Validation';

export function PutBeforeLogin(type, userData) {
    return new Promise((resolve, reject) => {
        fetch(Path() + type, {
            method:'PUT',
            body:JSON.stringify(userData),
            headers: {'Content-Type':'application/json'},
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export function PutData(pathValue, type, userData) {
    return new Promise((resolve, reject) => {
        fetch(Path(pathValue) + type, {
            method:'PUT',
            body:JSON.stringify(userData),
            headers: {
                'Content-Type':'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('cachedValue')
            },
        })
        .then((response) => StatusValidation(response))
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error)
        });
    });
}
import Path from './Path';
import StatusValidation from './Validation';
import Cookies from 'js-cookie'

export function PostBeforeLogin(type, userData) {
    return new Promise((resolve, reject) => {
        fetch(Path() + type, {
            method:'POST',
            body:JSON.stringify(userData),
            headers: {'Content-Type':'application/json'},
        })
        .then((response) => StatusValidation(response))
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}

export function PostData(pathValue, type, userData, formData = false) {
    return new Promise((resolve, reject) => {
        fetch(Path(pathValue) + type, {
            method:'POST',
            body: formData ? userData :JSON.stringify(userData),
            headers: {
                'Content-Type': formData ? 'multipart/form-data' : 'application/json',
                'Authorization': 'Bearer ' + Cookies.get("_token")
            },
        })
        .then((response) => StatusValidation(response) )
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}
import { appURL } from "./Appurl";
const mainPath = appURL;
const Path = pathValue => {
  switch (pathValue) {
    case "ms1":
      return `http://${mainPath}`;
    case "ms1_user":
      return `https://micro1.${mainPath}user/`;
    case "ms2":
      return `https://micro2.${mainPath}`;
    case "ms3":
      return `https://micro3.${mainPath}`;
    case "ms4":
      return `https://micro4.${mainPath}`;
    default:
      return `http://${mainPath}`;
  }
};

export default Path;

import Path from './Path';
import StatusValidation from './Validation';

export function DeleteData(pathValue, type, data) {
    return new Promise((resolve, reject) => {
        fetch(Path(pathValue) + type , {
            method : 'DELETE',
            body: JSON.stringify(data),
            headers : {
                'Content-Type':'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('cachedValue')
            }
        })
        .then((response) => StatusValidation(response))
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error)
        });
    });
}
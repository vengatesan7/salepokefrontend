const statusValidation = response => {
  switch (response.status) {
    case 200:
      return response.json();
    case 401:
      // alert('Login session expired!');
      localStorage.clear();
      // window.location = "/login";
      break;
    case 422:
      return "Invalid";
    default:
      return "Invalid";
  }
};

export default statusValidation;

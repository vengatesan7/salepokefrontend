const microURL = {
  ms1: "micro1.",
  ms2: "micro2.",
  ms3: "micro3.",
  ms4: "micro4.",
  ms5: "micro5."
};
const serverURL = { http: "http://", https: "https://" };
export const mainUrl2 = (props) => {
  const appURL2 = props
  console.log(appURL2)
  }
const mainURL = { localURL: "localhost:8000/", liveURL: "optinowl.com/"  };
export const appURL = mainURL.localURL;

// export const previewURL = appURL === "roundclicks.com/" ? `https://app.${appURL}` : `https://alpha.${appURL}`;
export const scriptURL = `https://cdn.${appURL}install/myowlpixel.js?id=`;

export const ms1URL = serverURL.https + microURL.ms1 + appURL;
export const ms2URL = serverURL.https + microURL.ms2 + appURL;
export const ms3URL = serverURL.https + microURL.ms3 + appURL;
export const ms4URL = serverURL.https + microURL.ms4 + appURL;
export const ms5URL = serverURL.https + microURL.ms5 + appURL;

export const ms1ImageURL = ms1URL + "public/";
export const ms2ImageURL = ms2URL + "public/";
export const ms3ImageURL = ms3URL + "public/";
export const ms4ImageURL = ms4URL + "public/";
export const ms5ImageURL = ms5URL + "public/";
export const imageUploadPath = ms1URL + "public";

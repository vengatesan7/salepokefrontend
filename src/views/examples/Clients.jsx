import React from "react";
import FacebookLogin from 'react-facebook-login';

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import TableSkelton from "../../components/Skeleton/index";
import Cookies from 'js-cookie'

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: null,
      firstName: undefined,
      lastName: undefined,
      email: undefined,
      address: undefined,
      city: undefined,
      countryCode: undefined,
      mobile: undefined,
      oldPassword: undefined,
      newPassword: undefined,
      confirmNewPassword: undefined,
      state: undefined,
      addNew : false,
      loading: true,
    };
  }

  componentDidMount() {
    this.getClientData();
  }
  responseFacebook(response) {
    console.log(response);
  }
 
  getClientData = () => {
    GetData("ms5", "user/getclientaccounts").then(data => {
      this.setState({
        clientData: data.client_account,
        loading:false
      });
    });
  };
  addNewClient = () =>{
    this.setState({
      addNew : true
    })
  }
  delete = (id) =>{
    console.log(id)
    this.setState({
      deletedId : id,
      popupEnable:true
    })
     }
     close = () => {
      this.setState({
        popupEnable: false,
        // manageQuickReplyenable: false
      });
    };
    confirmDelete = () =>{
      console.log(this.state.deletedId)
      const delId = {
        client_id: this.state.deletedId,
      }
      
      PostData("ms1", "user/deleteclientaccount", delId).then(data => {
        this.setState({
          popupEnable: false,
        });
        this.getClientData();
    
      });
    }
  render() {
    const { clientData,loading, addNew , popupEnable} = this.state;
    if(addNew){
      return <Redirect to='/plain/create-client'/>
    }
    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              {/* <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Add New Client{" "}
                      </Button>
                    </Col>
                  </Row>
                </CardHeader> */}
                {/* <CardBody> */}
                  
                    <div className="border-0">
                    { loading ? <TableSkelton /> : 
                     clientData !== null && ( <React.Fragment>  
                        <div className="border-0 mb-5">
                        <Row className="align-items-center">
                          <div className="col">
                            <h3 className="mb-0">Clients</h3>
                          </div>
                          <div className="col text-right">
                          { Cookies.get("_ACrole") === "2" && <Button
                              color="primary"
                              onClick={this.addNewClient}
                              size="sm"
                            >
                              Add New Client
                            </Button>
                          
                          }
                          </div>
                        </Row>
                      </div>
                      <Table
                        className="align-items-center table-flush"
                        responsive
                      >
                        <thead className="thead-light">
                          <tr>
                            <th scope="col">Client name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Mobile Number</th>
                            <th scope="col">Business Name</th>
                            <th scope="col"></th>

                          </tr>
                        </thead>
                        <tbody>
                          {clientData.map((data) =>{
   return <tr>
   <th scope="row">{data.first_name}</th>
   <td>{data.last_name}</td>
   <td>{data.email}</td>
   <td>
     {/* <i className="fas fa-arrow-up text-success mr-3" />{" "} */}
     {data.mobile_number}
   </td>
   <td>
     {data.business_name}
   </td>
   <td scope="col"> <span className="table-buttons float-right">
   
                                <button
                                   onClick={() =>
                                    this.delete(data.user_id)
                                  }
                                  name={data.user_id}
                                >
                                  <i class="material-icons">delete</i>
                                </button>
                                {/* <button
                                  onClick={() =>
                                    this.manageQuickReply(data.lead_contact_id)
                                  }
                                  name={data.lead_contact_id}
                                >
                                  <i class="material-icons">reply</i>
                                </button>
                                <button
                                  onClick={() =>
                                    this.leadDetail(data.lead_contact_id)
                                  }
                                  name={data.lead_contact_id}
                                >
                                  <i class="material-icons">visibility</i>
                                </button> */}
                              </span></td>
 </tr>
                          })}
                        </tbody>
                      </Table>
                     </React.Fragment>)}
                    </div>
                  
                {/* </CardBody> */}
              {/* </Card> */}


              
            </Col>
            <Col>
            {/* <FacebookLogin
          appId="1453217321423657"
          autoLoad={true}
          fields="name,email,picture"
          scope="public_profile,manage_pages,leads_retrieval,user_actions.books"
          callback={()=>this.responseFacebook}
          redirectUri="https://micro2.wwmarketing.co/_oauth/facebook"
        /> */}
  
      </Col>
          </Row>
          {popupEnable && (
                    <div className="popup-overlay">
                      <div className="popup">
                        <div className="select-domain-popup">
                          <button
                            onClick={this.close}
                            className="close new-project-modal__close"
                          >
                            x
                          </button>
                          {/* <div className="pop-up-head no-border">
                            <h3>Delete team member</h3>
                            
                          </div> */}
                          
                          <div className="pop-up-footer">
                          <h3>Delete team member</h3>

                          <p>
                            Are you sure want to delete this team member?
                            </p>
                          <Button  onClick={this.confirmDelete} color="primary">Yes</Button>
                          <Button onClick={this.close}>No</Button>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  )}
        </Container>
      </>
    );
  }
}

export default Profile;

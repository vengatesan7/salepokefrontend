import React from "react";

// reactstrap components
import CopyToClipboard from 'react-copy-to-clipboard'
import {
  FormGroup,
  InputGroup,
  CardHeader,
  InputGroupAddon,
  InputGroupText,
  DropdownItem,
  UncontrolledDropdown,
  Input,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  CardBody,
  Card,
  Container,
  Row,
  UncontrolledTooltip,
  Button,
  Col
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { ms1ImageURL, scriptURL } from "../services/Appurl";

// import { PostData } from "../services/PostData";
// import { Formik } from "formik";
// import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import { PostData } from "../services/PostData";
import TableSkelton from "../../components/Skeleton/index";
import Cookies from "js-cookie";
import cookie from 'react-cookies'


class Install extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      value: `<script  id='owl-pixel-script'  type="text/javascript" src="${scriptURL}${Cookies.get('_ACid')}"></script>`, 
      copied: false

    };
  }

  componentDidMount() {
this.setState({
loading:false})

  }
 
  onCopy = () => {
    this.setState({copied: true});
  };
  render() {
    const {
      loading,
      clientData,
    } = this.state;
    

    return (
      <>
        <UserHeader />
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
            <Card className="border-0">
                <CardHeader className="bg-transparent pl-0 pr-0">
                  <Row className="align-items-center">
                    <div className="col">
                     
                      <h2 className="mb-0">Install my Owl</h2>
                      <h6 className="text-uppercase text-muted ls-1 mb-1">
                       Follow the instruction below or ask a collegue to help. 
                      </h6>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody  className="bg-transparent pl-0 pr-0">
                <CopyToClipboard onCopy={this.onCopy} text={this.state.value}>
            <span className="copytoclip"> {this.state.value} 
            
            <span className='copybutton'>
            <span class="material-icons"
 style={{}}>
file_copy
</span>   Click to Copy  </span>
 </span>

            
          </CopyToClipboard>

          {this.state.copied ? <span className="copytoclipInner">Snippet Copied.</span> : null}
                </CardBody>
              </Card>
              
             
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Install;

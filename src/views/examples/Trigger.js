import React, { useState } from "react";
import Switch from "react-switch";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import cookie from 'react-cookies';
import { PostData } from "../services/PostData";

class Alltriggers extends React.Component {
  constructor(props){
    super(props);
    this.state={
      loading : true,
    }
  }
  componentDidMount(){
    if(cookie.load('comingFrom') === "EDITTRIGGERS"){
      this.getCampaginTriggers();
    }
    this.setState({
      loading:false,
    })
  }
  getCampaginTriggers = () =>{
    
    const Popiupid = { pop_up_id : cookie.load("popupId")}
    PostData("ms1", "getpopuptrigger", Popiupid).then(data=>{
      console.log(data)
      // if(data.message === "")
    })
  }
  render() {
    const intialTrigger = {
      popup: {
        onPageLoad: {
          enable: true,
          startDelay: 0,
        },
        onPageExit: {
          enable: false,
        },
        onPageScroll: {
          enable: false,
          pagePosition: 0,
        },
        onPageCount: {
          enable: false,
          visits: 5,
        },
        onPageClick: {
          enable: false,
          count: 5,
        },
      },
    }
    return <Triggerlist triggerData={intialTrigger} />;
  }
}

export default Alltriggers;

const Triggerlist = (props) => {

  const { popup } = props.triggerData;
  let {
    onPageClick,
    onPageLoad,
    onPageCount,
    onPageExit,
    onPageScroll,
  } = popup;
  const [checkd, setCheckd] = useState(false);
  const [onPageLoadCheckd, setOnPageLoadCheckd] = useState(onPageLoad.enable);
  const [timeDelay, setTimeDelay] = useState(onPageLoad.startDelay/1000);
  const [onPageExitCheckd, setOnPageExitCheckd] = useState(onPageExit.enable);
  const [onPageScrollCheckd, setOnPageScrollCheckd] = useState(onPageScroll.enable);
  const [scrollValue, setScrollValue] = useState(onPageScroll.pagePosition);
  const [onPageCountCheckd, setOnPageCountCheckd] = useState(onPageCount.enable);
  const [pageCount, setPageCount] = useState(onPageCount.visits);
  const [redirect, setRedirect] = useState(false);


  const handleChange = (checked, value) => {
    console.log(checked, value);
    setCheckd(checked);
  };
  const onPageloadEnableChange = (checked) => {
    setOnPageLoadCheckd(checked);
    return onPageLoad.enable = checked;
  };
  const onPageExitEnableChange = (checked) => {
    setOnPageExitCheckd(checked);
    return onPageExit.enable = checked;
  };
  const onPageScrollEnableChange = (checked) => {
    setOnPageScrollCheckd(checked);
    return onPageScroll.enable = checked;
  };
  const onPageCountEnableChange = (checked) => {
    setOnPageCountCheckd(checked);
    return onPageCount.enable = checked;
  };
  
  const onPageloadDelayChange = (event) => {
    // console.log(event.target.value);
    setTimeDelay(event.target.value)
    return onPageLoad.startDelay = event.target.value*1000;
  };
  const onScrollDelayChange = (event) => {
    // console.log(event.target.value);
    setScrollValue(event.target.value)
    return onPageScroll.pagePosition = event.target.value;
  };
  const onPagecountChange = (event) => {
    // console.log(event.target.value);
    setPageCount(event.target.value)
 const red = onPageCount.visits = event.target.value
    return ;
  };
  const onDone = () =>{
    const savedTrigger = {
      popup: {
        onPageLoad: {
          enable: onPageLoadCheckd,
          startDelay: timeDelay,
        },
        onPageExit: {
          enable: onPageExitCheckd,
        },
        onPageScroll: {
          enable: onPageScrollCheckd,
          pagePosition: scrollValue,
        },
        onPageCount: {
          enable: onPageCountCheckd,
          visits: pageCount,
        },
        onPageClick: {
          enable: false,
          count: 0,
        },
      },



    }
    const popupTrigger = {pop_up_id:cookie.load("popUpId"),
    trigger:savedTrigger}
    console.log(popupTrigger)
PostData("ms1","savepopuptrigger", popupTrigger).then(response => console.log(response))
setRedirect(true);

  }
  const redirectFun = () =>{
    if(redirect){
      return <Redirect  to={{pathname:'/admin/campaigns'}}/>
    }
  }
  const savedTrigger = {
    popup: {
      onPageLoad: {
        enable: onPageLoadCheckd,
        startDelay: timeDelay,
      },
      onPageExit: {
        enable: onPageExitCheckd,
      },
      onPageScroll: {
        enable: onPageScrollCheckd,
        pagePosition: scrollValue,
      },
      onPageCount: {
        enable: onPageCountCheckd,
        visits: pageCount,
      },
      onPageClick: {
        enable: false,
        count: 0,
      },
    },



  }
  console.log(savedTrigger)
  return (
    <React.Fragment>
      { redirectFun() }
      <Col md="12" className="display-center mt-3 pb-3 cancel">
            <Link to="/editor/builder" className="secondary-cta">
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  {/* <i class="material-icons">close</i> */}
                </span>
                Back
              </Link>
              <Button 
              className="cta"
              onClick={onDone}
              >
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  {/* <i class="material-icons">close</i> */}
                </span>
                Done
              </Button>
            </Col>
      {/* <div className="col-md-12 mb-3">
      </div> */}
      <div className="col-md-6 competor-them-list-wrap">
      <h2 className="mb-3">Triggers</h2>

        {popup && 
                    <React.Fragment>
                        {onPageLoad && (
          <React.Fragment>
            <div className={true ? " comp-list" : "active comp-list"}>
              <div>
                <h4 className="mb-0">Time delay</h4>
                <p>
                  Display after
                  <input type="number" defaultValue={timeDelay} onChange={onPageloadDelayChange}/>
                  seconds on the page
                </p>
              </div>
              <div>
                <Switch
                  onChange={onPageloadEnableChange}
                  checked={onPageLoadCheckd}
                  onColor="#3a5bd6"
                  // onHandleColor="#2693e6"
                  handleDiameter={24}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                  activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                  height={20}
                  width={48}
                  className="react-switch"
                  id="material-switch"
                />
              </div>
            </div>
          </React.Fragment>
        )}
        {onPageExit && (
            <React.Fragment>
              <div className={true ? " comp-list" : "active comp-list"}>
                <div>
                  <h4 className="mb-0"> Exit intent</h4>
                  <p>
                  Display when the visitor is about to leave the page


                  </p>
                </div>
                <div>
                  <Switch
                    onChange={onPageExitEnableChange}
                    checked={onPageExitCheckd}
                    onColor="#3a5bd6"

                     // onHandleColor="#2693e6"
                  handleDiameter={24}
                  uncheckedIcon={false}
                  checkedIcon={false}
                  boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                  activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                  height={20}
                  width={48}
                  className="react-switch"
                  id="material-switch"
                  />
                </div>
              </div>
            </React.Fragment>
          )}

{onPageScroll && (
            <React.Fragment>
              <div className={true ? " comp-list" : "active comp-list"}>
                <div>
                  <h4 className="mb-0">  Page Scroll</h4>
                  <p>
                  Display after 




                  <input type="number" defaultValue={scrollValue} onChange={onScrollDelayChange}/>  % on the page

                  </p>
                </div>
                <div>
                  <Switch
                    onChange={onPageScrollEnableChange}
                    checked={onPageScrollCheckd}
                    onColor="#3a5bd6"

 // onHandleColor="#2693e6"
 handleDiameter={24}
 uncheckedIcon={false}
 checkedIcon={false}
 boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
 activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
 height={20}
 width={48}
 className="react-switch"
 id="material-switch"                  />
                </div>
              </div>
            </React.Fragment>
          )}

{onPageCount && (
            <React.Fragment>
              <div className={true ? " comp-list" : "active comp-list"}>
                <div>
                  <h4 className="mb-0"> Page Count
</h4>
                  <p>
                  Display after 




                  visiting <input type="number" value={pageCount} onChange={onPagecountChange}/>   pages

                  </p>
                </div>
                <div>
                  <Switch
                    onChange={onPageCountEnableChange}
                    checked={onPageCountCheckd}
                    onColor="#3a5bd6"

 // onHandleColor="#2693e6"
 handleDiameter={24}
 uncheckedIcon={false}
 checkedIcon={false}
 boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
 activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
 height={20}
 width={48}
 className="react-switch"
 id="material-switch"                  />
                </div>
              </div>
            </React.Fragment>
          )}


          
          </React.Fragment>

        }
        
      </div>

      <div className="col-md-6">
      <div className="col-md-12 mb-5">
        <h2 className="mb-2">Summary - when my owl will show up your popup</h2>
        {onPageExitCheckd && <p><b>  Exit intent: </b>On</p>}
        {onPageScrollCheckd && <p>
<b>Page Scroll: </b>Display after {scrollValue} % on the page</p>}
        {onPageLoadCheckd && <p><b>Time delay:</b> Display after {timeDelay} seconds on the page</p>}
        {onPageCountCheckd && <p><b>Page Count:</b> Display after visiting {pageCount} pages</p>}

      </div>

      <div className="col-md-12 mb-3">
      {/* <h2>Preview</h2> */}

        <div className="preview-wrap">
          <div className="prev-head">
            <div className="template__circlebox">
              <div className="template__circle"></div>
              <div className="template__circle"></div>
              <div className="template__circle"></div>
            </div>
          </div>
          <div className="prev-img">
            
             <img
                           className="img-fluid"
                           title="Preview image"

                                  alt="Preview image"
                                  src={require("../../assets/img/theme/popup-prview.jpg")}
                                />
          </div>
          
       
          </div>
        </div>
        
      </div>
    </React.Fragment>
  );
};




/////Targetting

const Targetting = () =>{
  const targetList = {
    popup: {
      device: {
        mobile: true,
        desktop:true,
      },
      displayTo: {
        enable: true,
        visitors:"everyone"
      },
      country: {
        enable: false,
        country: "",
      },
      viewCount: {
        enable: false,
        visits: 0,
      },
      ipBlocking: {
        enable: false,
        ip: [],
      },
    },
  };
  const { popup } = targetList;
  return console.log(popup)
}
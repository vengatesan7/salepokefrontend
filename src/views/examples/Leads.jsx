import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  // CardBody,
  // FormGroup,
  // Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
// import { PostData } from "../services/PostData";
// import { Formik } from "formik";
// import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import { PostData } from "../services/PostData";
import Popup from "../../components/popup.jsx";
import Customreply from "../examples/Customreply";
import TableSkelton from "../../components/Skeleton/index";
import DynamicDataTable from "@langleyfoxall/react-dynamic-data-table";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientData: [],
      loading:true
    };
  }

  componentDidMount() {
    this.getClientData();
  }
  getClientData = () => {
    PostData("ms1", "getusercontactlist").then(data => {
      console.log(data);
      this.dataConverter(data.contacts); 

      this.setState({
        loading:false

      });
    });
  };

  dataConverter = convertData => {
    let contactData = Object.values(convertData);
    let contactArray = {};
    for (let i = 0; i < contactData.length; i++) {
      contactArray[i] = {
        contact_id: contactData[i].contact_id,
        contact_type: contactData[i].contact_type,
        created_at: contactData[i].created_at,
        email: contactData[i].email,
        ip_address: contactData[i].ip_address,
        name: contactData[i].name,
      };
    }
    let contactData2 = Object.values(contactArray);
    this.setState({
      userCummulativeDetail: convertData,
      clientData: contactData2
    });    
  };
  render() {
    const {
      clientData,
      manageQuickReplyenable,
      leadId,

      loading
    } = this.state;
    console.log(clientData);
    // if (addNew) {
    //   return <Redirect to="/plain/add-new-campaign" />;
    // }
    // if (leadDetail && leadId !== null) {
    //   return (
    //     <Redirect
    //       to={{
    //         pathname: "/admin/profile-detail",
    //         state: {
    //           leadId: this.state.leadId
    //         }
    //       }}
    //     />
    //   );
    // }
    // if (leadActivity && leadId !== null) {
    //   return (
    //     <Redirect
    //       to={{
    //         pathname: "/admin/activity-log",
    //         state: {
    //           leadId: this.state.leadId
    //         }
    //       }}
    //     />
    //   );
    // }
    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              {/* <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Add New Client{" "}
                      </Button>
                    </Col>
                  </Row>
                </CardHeader> */}
              {/* <CardBody> */}
           
                <Card className="shadow">
                { loading ? <TableSkelton/> :
                   clientData !== null && (  <React.Fragment>
                  <CardHeader className="border-0 leads-filter">
                    <Row className="align-items-center ">
                      <div className="col">
                        <h3 className="mb-0">Leads</h3>
                      </div>
                      

                       
                      <div className="col-4 text-left">
                      <small className=''> Filter by campaign </small>

                        <Input
                          type="select"
                          name="select"
                          id="exampleSelect"
                          onChange={this.campaignLeads}
                        >
                          <option value="all">All</option>

                          {/* {campaignsList !== null &&
                            campaignsList.map(data => {
                              return (
                                <React.Fragment>
                                  <option value={data.campaign_id}>
                                    {data.campaign_name}
                                  </option>
                                </React.Fragment>
                              );
                            })} */}
                        </Input>

                      </div>
                      {/* <Input type="select" name="select" id="exampleSelect" /> */}
                    </Row>
                  </CardHeader>
                  {/* <DynamicDataTable rows={clientData} /> */}
                  <DynamicDataTable
                    rows={clientData}
                    fieldsToExclude={[/_?id/, "updated_at", "contact_type"]}
                    renderCheckboxes
                    fieldOrder={[/_Name/, "Email", "Mobile", "Phone"]}
                    // dataItemManipulator={(field, value) => {
                    //   switch (field) {
                    //     case "created_at":
                    //       return this.dateFormat(value);
                    //     case "subscription_status":
                    //       return this.isSbuscribe(value);
                    //   }
                    //   return value;
                    // }}
                    orderByField={this.state.orderByField}
                    orderByDirection={this.state.orderByDirection}
                    disallowOrderingBy={["subscription_status"]}
                    /*changeOrder={(field, direction) =>
                      this.changeOrder(field, direction)
                    }*/
                    buttons={row => <td></td>}
                    // onClick={(event, row) => this.rowFetch(row)}
                  />
         
                  
                  </React.Fragment>)
                  }
                </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Profile;

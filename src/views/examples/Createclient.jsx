import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { Context } from "../../context/index";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Cookies from 'js-cookie'

export default class Inviteteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isCreated: false
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
  }

  createClient = values => {
    // event.preventDefault(); 
    const clientDetail = {
      first_name : values.firstName,
      last_name: values.lasttName,
      email: values.email,
      mobile_number: values.phone,
      business_name: values.business
    }
    console.log(clientDetail);
    PostData("ms5", "user/createclientaccount", clientDetail ).then(data => {
      this.setState({
        isCreated:true,
       loading: false
      });
    });
  };

  render() {
    const{ roleType, loading} = this.state;
    if(this.state.isCreated){
      return <Redirect to='/admin/clients-list'/>
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
              <Col md="12" className="text-right mt-3 pb-3 cancel">
          <Link to="/admin/clients-list" className='new-project-modal__close'>
            {/* <span className="text-center new-project-modal__close" style={{ display: "inline-block" }}>
              <i class="material-icons">close</i>
            </span> */}
          </Link>
        </Col>
                <Col  lg="5" md="5" className='mt-6' >
        {/* <h1 className='text-white'>Welcome To Leadify</h1>
        <p className='text-white'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
     
                        <Link to='/auth/register' className='text-cta text-white'><small> Create new account</small> </Link> */}
    <img
                              alt="..."
                              src={require("../../assets/img/theme/create-client.svg")}
                              className='img-fluid'
                              style={{width:'100%'}}

                            />
      
      </Col>
            <Col lg="7" xs="12" className='pl-5 mt-7'>
              <h1 className='mb-3'>Create New Client</h1>
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  email: "",
                  firstName: "",
                  lasttName: "",
                  phone: "",
                  business: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  this.setState({
                    loading: true
                  });

                  console.log(values)
                  this.createClient(values);
                }}
                validationSchema={Yup.object().shape({
                  email: Yup.string()
                    .email("Please enter a valid email address")
                    .required("Email Required"),
                    business: Yup.string().required("Role Required"),
                    firstName: Yup.string().required("First name Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-circle-08" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="First name"
                                type="text"
                                name="firstName"
                                // onChange={this.handleChangeInput}
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.firstName &&
                                    touched.firstName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.email && touched.firstName && (
                              <div className="input-feedback">
                                {errors.firstName}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-circle-08" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Last name"
                                type="text"
                                name="lasttName"
                                // onChange={this.handleChangeInput}
                                value={values.lasttName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.lasttName &&
                                    touched.lasttName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.lasttName && touched.lasttName && (
                              <div className="input-feedback">
                                {errors.lasttName}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs-12>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-email-83" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Email"
                                type="email"
                                name="email"
                                // onChange={this.handleChangeInput}
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.email && touched.email && "error")
                                }
                              />
                            </InputGroup>
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-mobile-button" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Mobile number"
                                type="number"
                                name="phone"
                                // onChange={this.handleChangeInput}
                                value={values.phone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.phone && touched.phone && "error")
                                }
                              />
                            </InputGroup>
                            {errors.phone && touched.phone && (
                              <div className="input-feedback">
                                {errors.phone}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-badge" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Business"
                                type="text"
                                name="business"
                                // onChange={this.handleChangeInput}
                                value={values.business}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.business && touched.business && "error")
                                }
                              /> 
                            </InputGroup>
                            {errors.role && touched.role && (
                              <div className="input-feedback">
                                {errors.role}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row>
                      {/* <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password"   name="password"
                      //onChange={this.handleChangeInput}
                      value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.password && touched.password && "error")
                        }
                        autofill={false}
                      />  
                      </InputGroup>
                      {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                      {this.state.isWrongCreditional && (
                        <div className="input-feedback">
                          Invalid Email or Password
                        </div>
                      )}
                </FormGroup> */}
                      {/* <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div> */}
                      <div className="text-center">
                        <Button
                          className="my-2 cta cta-fullwidth"
                          color="primary"
                          type="submit"
                        >
                         Create New Client

                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}
// const Validatedfrom = () =>{
//   return(

//   )
// }

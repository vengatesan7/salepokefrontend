
import React from "react";
import { PostBeforeLogin } from '../services/PostData'
import { Link, Redirect } from "react-router-dom";
// reactstrap components
import {
  Button,
  Card,
  // CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  // Row,
  Col
} from "reactstrap";
import Cookies from 'js-cookie'

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      firstName:null,
      lastName:null,
      phone:null,
      loading:true,
      isRegister : false,
      isWrongCreditional:false
    };
  }
  componentDidMount(){
    this.setState({
       loading:false,
    });
  }
  handleChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  createNewAccount = event => {
    event.preventDefault();
    this.setState({
      loading : true,
    });
    const userData = {
      email : this.state.email,
    }
    PostBeforeLogin('create-user', userData).then(result => {
      console.log(result)
      if(result !== 'Invalid'){
        // var inHalfADay = 1;
        Cookies.set('_token', result.token )
        Cookies.set('_ACid',result.user.user_id)
        // Cookies.set('_ACrole',result.user.role_id)
        Cookies.set('_ACname',`${result.user.name}`)
        this.setState({
          isRegister : true,  
          loading : false,
        }); 
      }else{
        this.setState({
          isRegister : false,  
          loading : false,
          isWrongCreditional:true,
        });
      }
    });
  };
  render() {
    if(this.state.isRegister && Cookies.get('_token')  ){
      return <Redirect to='/admin/index'/>
    }
    return (
      <>
     <Col  lg="3" md="3" >
       
      </Col>
        <Col lg="6" md="6">
          <Card className="border-0">
            {/* <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-4">
                <small>Sign up with</small>
              </div>
              <div className="text-center">
                <Button
                  className="btn-neutral btn-icon mr-4"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div>
            </CardHeader> */}
            <CardBody className="">
            <div className="text-left text-muted mb-3">
                <small>  <img
                        alt="Logo"
                        className="mb-4 mt-3"
                        width='50'
                        src={require("../../assets/img/brand/aclogo.png")}
                      /> </small> 

<h1 className="mb-0"> Start your 14-day free trial today</h1>
<p> Try Owl for free, and explore all the tools and services you need to start, run, and grow your business.</p>

              </div>
              <Form role="form" onSubmit={this.createNewAccount}>
                {/* <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-circle-08" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="First Name" type="text"  name="firstName"
                      onChange={this.handleChangeInput} />
                  </InputGroup>
                </FormGroup> */}
                {/* <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-circle-08" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Last Name" type="text"  name="lastName"
                      onChange={this.handleChangeInput} />
                  </InputGroup>
                </FormGroup> */}
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email" type="email"  name="email"
                      onChange={this.handleChangeInput} />
                  </InputGroup>
                </FormGroup>
                {/* <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-mobile-button" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Phone Number" type="text"  name="phone"
                      onChange={this.handleChangeInput} />
                  </InputGroup>
                </FormGroup> */}
                {/* <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password"  name="password"
                      onChange={this.handleChangeInput} />
                  </InputGroup>
                  
                </FormGroup> */}
                {this.state.isWrongCreditional && (
                        <div className="input-feedback" style={{    marginBottom: "0px",                        }}>
                          The user hasbeen already registered or something went wrong please contact support team.
                        </div>
                      )}
                {/* <div className="text-muted font-italic">
                  <small>
                    password strength:{" "}
                    <span className="text-success font-weight-700">strong</span>
                  </small>
                </div> */}
                {/* <Row className="my-4">
                  <Col xs="12">
                    <div className="custom-control custom-control-alternative custom-checkbox">
                      <input
                        className="custom-control-input"
                        id="customCheckRegister"
                        type="checkbox"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customCheckRegister"
                      >
                        <span className="text-muted">
                          I agree with the{" "}
                          <a href="#pablo" onClick={e => e.preventDefault()}>
                            Privacy Policy
                          </a>
                        </span>
                      </label>
                    </div>
                  </Col>
                </Row> */}
                <div className="text-left">
                  <Button className="cta" color="primary" type="submit" style={{paddingRight: "26px",}}>
                    Create your account 
                    <span className="cta-icon right">
                                       &#x02192;

                                       </span>
   
                  </Button>
                </div>
              </Form>

              <p className="mt-5 mb-2"> Already have an account? <Link to='/auth/login' className='text-cta p-l-0'>Login Here</Link></p>
            </CardBody>
          </Card>
        </Col>
        <Col  lg="3" md="3" >
        {/* <h1 className=''>Join millions worldwide who increase their leads using OptinMaster.</h1>
        <p className=''>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
      */}
             
            
      
      </Col>
      </>
    );
  }
}

export default Register;

import React from "react";
import { PostBeforeLogin } from '../services/PostData'
import { Link, Redirect } from "react-router-dom";
import Cookies from 'js-cookie'
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
// reactstrap components
import {
  Button,
  Card,
  // CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import { Context } from "../../context/index";

class Changepassword extends React.Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      isLogin : false,
      isWrongCreditional: false,
      islogedin: false,
      loading: true 

    };
  }
  componentDidMount(){
    this.setState({
      loading:false,
    });
  }
  handleChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  createNewAccount = values => {
    // event.preventDefault();
    const userData = {
      email : values.email,
      password: values.password,
    }
    PostBeforeLogin('user/login', userData).then(result => {
      console.log(result)
      if(result !== 'Invalid'){
        // var inHalfADay = 1;
        // Cookies.set('_token', result.token ,{expires: inHalfADay})

        Cookies.set('_token', result.token)
        Cookies.set('_ACid',result.user_data.user_id,)
        this.setState({
          isLogin : true
        });
      }else{
        this.setState({
          isLogin : true
        });
      }
    });
  };

  render() {
    if(this.state.isLogin && Cookies.get('_token')    ){
      return <Redirect to='/admin/index'/>
    }
    return (
      <>
      <Col  lg="7" md="5" >
        <h1 className=''>Welcome To OptinMaster</h1>
        <p className=''>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
     
                        <Link to='/auth/register' className='text-cta'><small> Create new account</small> </Link>

      
      </Col>
      
        <Col lg="5" md="7">{this.state.loading && (
                  <div className="signupLoadingDiv">
                    <SkeletonTheme color="#f7fafc" highlightColor="#5e72e4">
                      <Skeleton height={7} />
                    </SkeletonTheme>
                  </div>
                )}
          <Card className="bg-secondary shadow border-0">
            {/* <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div>
            </CardHeader> */}
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-left text-muted mb-4">
                <small>  <img
                        alt="..."
                        width='190'
                        src={require("../../assets/img/brand/aclogo.png")}
                      /> <strong> Change password</strong></small>
              </div>
              <Formik
        enableReinitialize={true}
        initialValues={{
          password: "",
          passwordConfirmation: "",
        }}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          const Password = {
            password: values.passwordConfirmation
          };
          // this.setState({ loading: true });
          console.log(values);
          console.log(Password);
          resetForm();
        }}
        validationSchema={Yup.object({
          password: Yup.string()
            .required("Password Required")
            .min(8, "Password is Too short min 8 char"),
            // .matches(/(?=.*[0-9])/, "Must Contain Number")
            // .matches(/(?=.*[a-z])/, "Must Contain One Lower Case Letter")
            // .matches(/(?=.*[@$!%*?&])/, "Must Contain one special character")
          // .matches(/(?=.*[A-Z])/, "Must Contain  One Uppercase Letter")
          passwordConfirmation: Yup.string()
            .required("Password is required")
            .oneOf([Yup.ref("password"), null], "Passwords must match")
        })}
      >
        {props => {
          const {
            values,
            touched,
            errors,
            // isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          return (
            <Form role="form" onSubmit={handleSubmit} novalidate>
              <div className="">
                <Row>
                  {/* <Col lg="4">
                    <FormGroup>
                      <label
                        className="form-control-label"
                        htmlFor="input-first-name"
                      >
                        Old password
                      </label>
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="Old password"
                          type="password"
                          name="oldpassword"
                          value={values.oldpassword}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className={
                            "col" ||
                            (errors.oldpassword &&
                              touched.oldpassword &&
                              "error")
                          }
                          autofill={false}
                        />
                      </InputGroup>
                      {errors.oldpassword && touched.oldpassword && (
                        <div className="input-feedback">
                          {errors.oldpassword}
                        </div>
                      )}
                    </FormGroup>
                  </Col> */}
                  <Col lg="12">
                    <FormGroup>

                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="Password"
                          type="password"
                          name="password"
                          //onChange={this.handleChangeInput}
                          value={values.password}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className={
                            "col" ||
                            (errors.password && touched.password && "error")
                          }
                          autofill={false}
                        />
                      </InputGroup>
                      {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                    </FormGroup>
                  </Col>
                  <Col lg="12">
                    <FormGroup>
                      <InputGroup className="input-group-alternative">
                        <Input
                          placeholder="Confirm password"
                          type="password"
                          name="passwordConfirmation"
                          //onChange={this.handleChangeInput}
                          value={values.passwordConfirmation}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          className={
                            "col" ||
                            (errors.passwordConfirmation &&
                              touched.passwordConfirmation &&
                              "error")
                          }
                          autofill={false}
                        />
                      </InputGroup>

                      {errors.passwordConfirmation &&
                        touched.passwordConfirmation && (
                          <div className="input-feedback">
                            {errors.passwordConfirmation}
                          </div>
                        )}
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <FormGroup>
                      <Input
                        className="btn btn-primary"
                        value="UPDATE PASSWORD"
                        type="submit"
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </div>
            </Form>
          );
        }}
      </Formik>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Changepassword;

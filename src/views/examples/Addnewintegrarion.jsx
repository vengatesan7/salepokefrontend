import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";

import Cookies from "js-cookie";
import { textChangeRangeIsUnchanged } from "typescript";

export default class Addnewintegration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      roleType: null,
      isLogin: false,
      hook : null,
      string:null,
      integrationList:null,
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
    this.getCampaginData();
  }
  getCampaginData = () =>{
    var x = document.getElementsByTagName("INPUT");
    console.log(x)
    const campaginId = {
      campaign_id : this.props.location.state.campaignId
    }
  //  const x = document.getElementsByName("integration")
  // //  var x = document.getElementsByName("animal");
  //  var i;
  //  for (i = 0; i < x.length; i++) {
  //    if (x[i].id == "unbounce") {
  //      x[i].checked = true;
  //    }
  //  }
  //  console.log(x)
    PostData("ms2", "getcampaignsbyid", campaginId ).then((data) =>{
      const hello = data.getcampaign.map(data => data.campaigns_integration);
      console.log(hello)
      // let hellow = hello.map(data => data.filter(int => int.integration_name))
      // let hellow = hello.map(dta => dta.integration_id);
      let result = hello[0].map(({integration_name}) => ({integration_name}));
let name = result.map(names => names.integration_name)
// return data.integration_name;
console.log(hello)
// console.log(hellow)
console.log(result)
console.log(name)
// let compare = name.map(ints => ints === "unbounce" ? "ok" : "no" );
// console.log(compare)
const unbounceCheck = (integ) => integ === "unbounce"
const clickFunnelsCheck = (integ) => integ === "clickfunnels"
const instapageCheck = (integ) => integ === "instapage"

const unbounceInteg = name.some(unbounceCheck)
const clickFunnelsInteg = name.some(clickFunnelsCheck)
const instapageInteg = name.some(instapageCheck)




      this.setState({
        unbounceInteg: unbounceInteg,
        clickFunnelsInteg : clickFunnelsInteg,
        instapageInteg : instapageInteg
      })
    })
  }

  updateIntegration = values => {
    // event.preventDefault();
    const updateIntegration = {
      campaign_id: this.props.location.state.campaignId,
      integration_name: values.integration,
      url_string: this.state.string
    };
          console.log(updateIntegration);

    PostData("ms2", "campaignintegrationupdate", updateIntegration).then(result => {
      console.log(result);
      if (result !== "Invalid") {
        this.setState({
          isLogin: true,
          loading: false
        });
      } else {
        this.setState({
          isLogin: true,
          loading: false
        });
      }
    }); 
   };

  integrationChange = integration => {
    console.log(integration);
    const baseUrl = "https://micro2.wwmarketing.co/webhookapi/";
    const stringGen = Math.random()
      .toString(36)
      .slice(2);
    switch (integration) {
      case "unbounce":
        return this.setState({
          hook: `${baseUrl}unbounce/${stringGen}`,
          string :stringGen
        });
      case "clickfunnels":
        return this.setState({
          hook: `${baseUrl}clickfunnels/${stringGen}`,
          string :stringGen
        });
      case "instapage":
        return this.setState({
          hook: `${baseUrl}instapage/${stringGen}`,
          string :stringGen
        });
    }
  };
  copyCodeToClipboard = () => {
    const el = this.textArea;
    // el.select();
    // document.execCommand("copy");
    console.log(el)
  };
  render() {
    console.log(this.state.integrationList);
    const { roleType, loading, unbounceInteg, clickFunnelsInteg, instapageInteg } = this.state;
    if (this.state.isLogin) {
      return (
        <Redirect
          to={{
            pathname: "/admin/campaigns",
            // state: {
            //   string: this.state.string,
            //   integration : this.state.integration
            // }
          }}
        />
      );
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            <Col md="12" className="text-right mt-3 pb-3 cancel">
              {/* <Link to="/admin/campaigns" > */}
              <Link to={{
  pathname:this.props.location.state.from === "campainDetail" ?  "/admin/campagin-detail": "/admin/campaigns",
  state: {
    campaignId: this.props.location.state.campaignId,
  }
}}
className="new-project-modal__close"
>
                {/* <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  <i class="material-icons">close</i>
                </span> */}
              </Link>
            </Col>

            <Col lg="12" xs="12" className="pl-5 mt-5 add-new-campaign">
              {/* <h3 className="mb-3">Campaign Name</h3> */}
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  integration: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  this.setState({
                    // loading: true
                  });
                  this.updateIntegration(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  integration: Yup.string().required("integration is Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      {/* <Row>
                        <Col xs-12>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-send" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Campaign name"
                                type="text"
                                name="campignName"
                                // onChange={this.handleChangeInput}
                                value={values.campignName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.campignName &&
                                    touched.campignName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.campignName && touched.campignName && (
                              <div className="input-feedback">
                                {errors.campignName}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row> */}
                      <Row>
                        <Col xs-12>
                          <h2 className="mb-3 text-center mt-3 mb-4">
                            Select your integration
                          </h2>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="unbounce"
                                type="radio"
                                name="integration"
                                value="unbounce"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("unbounce")
                                }
                                onBlur={handleBlur}
                                className={
                                  "custom-control-input" ||
                                  (errors.unbounce &&
                                    touched.unbounce &&
                                    "error")
                                }
                                
                                disabled={
                                  
                                  unbounceInteg !== null && unbounceInteg }
                                // active
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="unbounce"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("../../assets/img/theme/unbounce-icon-dark.svg")}
                                />{" "}
                              </label>
                   
                            </div>
                          </FormGroup>
                        </Col>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="clickfunnels"
                                type="radio"
                                name="integration"
                                value="clickfunnels"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("clickfunnels")
                                }
                                onBlur={handleBlur}
                                className={
                                  "custom-control-input" ||
                                  (errors.clickfunnels &&
                                    touched.clickfunnels &&
                                    "error")
                                }
                                disabled={
                                  
                                  clickFunnelsInteg !== null && clickFunnelsInteg }
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="clickfunnels"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("../../assets/img/theme/clickfunnels.png")}
                                />
                              </label>
                            </div>
                          </FormGroup>
                        </Col>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="instapage"
                                type="radio"
                                name="integration"
                                value="instapage"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("instapage")
                                }
                                onBlur={handleBlur}
                                className={"custom-control-input"}
                                disabled={
                                  
                                  instapageInteg !== null && instapageInteg }
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="instapage"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("../../assets/img/theme/insta-page.png")}
                                />
                              </label>
                            </div>
                          </FormGroup>
                        </Col>
                      </Row>
                      {errors.integration && touched.integration && (
                                <div className="input-feedback">
                                  {errors.integration}
                                </div>
                              )}
                      <Row className="hook mt-4">
                        <Col>
                          <input
                            ref={textarea => (this.textArea = textarea)}
                            value={this.state.hook}
                            className="col-12"
                            style={{height:"50px"}}
                          />
                          <button onClick={() => this.copyCodeToClipboard()} 
                          type="button"
                          style={{zIndex:"999",position:"relative",marginTop:'-36px',height:"50",lineHeight:"26px", cursor:'pointer', fontSize:"16px", background:'none', border:"none"}}
                          className="float-right">
<i class="material-icons text-blue"
 style={{ fontSize:"20px"}}>
file_copy
</i>                          </button>
                        </Col>
                      </Row>
                      <div className="text-center mt-5" style={{maxWidth:"150px", margin: "0 auto"}}>
                        <Button
                          className="my-2 cta cta-fullwidth"
                          color="primary"
                          type="submit"
                        >
                          Continue
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

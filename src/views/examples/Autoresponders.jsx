import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Cookies from "js-cookie";
import { textChangeRangeIsUnchanged } from "typescript";

export default class Addnewintegration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isLogin: false,
      integrationList: null,
      mailchimpData: null,
      mailchimpKey:null,
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
    this.getCampaginData();
  }
  getCampaginData = () => {
    PostData("ms5", "getmailchimp_key").then(data => {
      console.log(data);
      const APIvalue = data.mailchimp_key.map(key => console.log(key.key))
      this.setState({
        mailchimpData: data.mailchimp_key,
        mailchimpKey : APIvalue,
        updateAPI : false
      });
    });
  };

  setAutoresponder = values => {
    // event.preventDefault();
    const addResponders = {
      key: values.campignName
    };
    console.log(addResponders);

    PostData("ms5", "mailchimp_keyadd", addResponders).then(result => {
      console.log(result);
      if (result !== "Invalid") {
        // this.setState({
        //   isLogin: true,
        //   loading: false
        // });
        // reserForm();
        this.getCampaginData();

      } else {
        // this.setState({
        //   isLogin: true,
        //   loading: false
        // });
      }
    });
  };
delete = (event)=>{
  console.log(event.target.name)
  const addRespondersdelete = {
    mailchimp_key_id: event.target.name
  };
  PostData("ms5", "mailchimpkey_delete", addRespondersdelete).then(result => {
    console.log(result)
    this.setState({
      mailchimpData: null,
      mailchimpKey:null,
    })
    this.getCampaginData();
  })
}
update = (event)=>{
  console.log(event.target.name)
  const addRespondersUpdate = event.target.name;
this.setState({
  updateAPI: true,
  mailchimpData: null,
})
  // PostData("ms2", "mailchimpkey_delete", addRespondersUpdate).then(result => {
  //   console.log(result)
  //   this.getCampaginData();
  // })
}

  render() {
    console.log(this.state.integrationList);
    const { mailchimpData, roleType, loading, mailchimpKey , updateAPI} = this.state;
    if (this.state.isLogin) {
      return (
        <Redirect
          to={{
            pathname: "/admin/campaigns"
            // state: {
            //   string: this.state.string,
            //   integration : this.state.integration
            // }
          }}
        />
      );
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            <Col md="12" className="text-right mt-3 pb-3 cancel">
              
            </Col>

            <Col lg="12" xs="12" className="pl-5 mt-5 add-new-campaign">
              {/* <h3 className="mb-3">Campaign Name</h3> */}
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  campignName: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  this.setState({
                    // loading: true
                  });
                  this.setAutoresponder(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  campignName: Yup.string().required("API key  is Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs="4" className="mt-5 shadow pt-4">
                          <img
                            alt="..."
                            width=""
                            className="img-fluid mb-3"
                            src={require("../../assets/img/theme/mailchimp-logo.png")}
                          />
                          {updateAPI && <React.Fragment>
                              <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                  <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                      <i className="ni ni-send" />
                                    </InputGroupText>
                                  </InputGroupAddon>
                                  <Input
                                    placeholder="Enter your API code"
                                    type="text"
                                    name="campignName"
                                    defaultValue={mailchimpKey !== null && mailchimpKey}
                                    // onChange={this.handleChangeInput}
                                    value={values.campignName}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                      "col" ||
                                      (errors.campignName &&
                                        touched.campignName &&
                                        "error")
                                    }
                                  />
                                </InputGroup>
                                {errors.campignName && touched.campignName && (
                                  <div className="input-feedback">
                                    {errors.campignName}
                                  </div>
                                )}
                              </FormGroup>
                              {errors.integration && touched.integration && (
                                <div className="input-feedback">
                                  {errors.integration}
                                </div>
                              )}

                              <div
                                className="text-left mt-3"
                                style={{ maxWidth: "150px" }}
                              >
                                <Button
                                  className="my-2 cta cta-fullwidth"
                                  color="primary"
                                  type="submit"
                                >
                                  Submit
                                </Button>
                              </div>
                            </React.Fragment>}
                          {mailchimpData !== null &&
                          mailchimpData.length === 0 ? (
                            <React.Fragment>
                              <FormGroup className="mb-3">
                                <InputGroup className="input-group-alternative">
                                  <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                      <i className="ni ni-send" />
                                    </InputGroupText>
                                  </InputGroupAddon>
                                  <Input
                                    placeholder="Enter your API code"
                                    type="text"
                                    name="campignName"
                                    // onChange={this.handleChangeInput}
                                    value={values.campignName}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    className={
                                      "col" ||
                                      (errors.campignName &&
                                        touched.campignName &&
                                        "error")
                                    }
                                  />
                                </InputGroup>
                                {errors.campignName && touched.campignName && (
                                  <div className="input-feedback">
                                    {errors.campignName}
                                  </div>
                                )}
                              </FormGroup>
                              {errors.integration && touched.integration && (
                                <div className="input-feedback">
                                  {errors.integration}
                                </div>
                              )}

                              <div
                                className="text-left mt-3"
                                style={{ maxWidth: "150px" }}
                              >
                                <Button
                                  className="my-2 cta cta-fullwidth"
                                  color="primary"
                                  type="submit"
                                >
                                  Submit
                                </Button>
                              </div>
                            </React.Fragment>
                          ) : (
                            <React.Fragment>
                              {mailchimpData &&
                                mailchimpData.map(data => {
                                  return (
                                    <React.Fragment>
                                      <Button
                                        className="my-2 "
                                        color="primary"
                                        type="button"
                                        name={data.mailchimp_id}
                                        onClick={this.update}
                                      >
                                        Update
                                      </Button>
                                      <Button
                                        className="my-2 "
                                        color="secondary"
                                        type="button"
                                        name={data.mailchimp_id}
                                        onClick={this.delete}
                                      >
                                        Delete
                                      </Button>
                                    </React.Fragment>
                                  );
                                })}
                            </React.Fragment>
                          )}
                        </Col>
                      </Row>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

import React from 'react'

// reactstrap components
import {
    Button,
    Card,
    // CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Row,
    Col,
    Alert
  } from "reactstrap";
  import * as Yup from "yup";
  import { Formik } from "formik";
  import { PostData } from "../services/PostData";

class Customreply extends React.Component{
    constructor(props){
        super(props);
        this.state={
            success : false
        }
    }
    sendCustomMail = (values) =>{
        const customMailData = 
            {
                lead_contact_id:this.props.data,
                subject:values.email,
                content:values.password,
                from_name: values.fromname
            }
            
            PostData("ms2", "leademailformsubmit", customMailData).then(data => {
                console.log(data)
                this.setState({
                  sucess: true
                });
                // resetForm();
              });

    }
    close = () =>{
        this.props.close();

    }
    render(){
        console.log(this.props.data)
        return(
            <Formik 
            initialValues={{ email: "", password: "", fromname:'' }}
            onSubmit={(values, { setSubmitting }) => {
console.log(values)
this.sendCustomMail(values)
        }} 
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .required("Subject Required"),
          password: Yup.string().required("Content Required"),
          fromname: Yup.string().required("From name Required")

        })}
        >
          {props => {
          const {
            values,
            touched,
            errors,
            // isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          return (
            <Form role="form" onSubmit={handleSubmit} novalidate>
                  <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-circle-08" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="From name" type="text"   name="fromname"
                    // onChange={this.handleChangeInput} 
                    value={values.fromname}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      "col" || (errors.fromname && touched.fromname && "error")
                    }
                    />
                </InputGroup>
                     {errors.fromname && touched.fromname && (
                      <div className="input-feedback">{errors.fromname}</div>
                    )}
              </FormGroup>
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-tag" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Subject" type="text"   name="email"
                    // onChange={this.handleChangeInput} 
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      "col" || (errors.email && touched.email && "error")
                    }
                    />
                </InputGroup>
                     {errors.email && touched.email && (
                      <div className="input-feedback">{errors.email}</div>
                    )}
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                {/* <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-send" />
                    </InputGroupText>
                  </InputGroupAddon> */}
                  <Input placeholder="Content" type="textarea"   name="password"
                    //onChange={this.handleChangeInput}
                    value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        "col" ||
                        (errors.password && touched.password && "error")
                      }
                      autofill={false}
                    />  
                    </InputGroup>
                    {errors.password && touched.password && (
                      <div className="input-feedback">{errors.password}</div>
                    )}
              </FormGroup>
              <div className="text-center">
                <Button className="my-4 cta cta-fullwidth" color="primary" type="submit">
Send                </Button>
              </div>
              {this.state.sucess &&  <Alert color="success" toggle={this.close}>
        Mail send sucessfully
      </Alert>}
            </Form>)}}
            
            </Formik>
        )
    }
}
export default Customreply
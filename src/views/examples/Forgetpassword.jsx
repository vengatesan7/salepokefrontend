import React from "react";
import { PostBeforeLogin } from '../services/PostData'
import { Link, Redirect } from "react-router-dom";
import Cookies from 'js-cookie'
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

// reactstrap components
import {
  Button,
  Card,
  // CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import { Context } from "../../context/index";

class Login extends React.Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      message: null,
      isLogin : false,
      loading: true 

    };
  }
  componentDidMount(){
    this.setState({
      loading:false,
    });
  }
 
  requestPasswordLink = values => {
    // event.preventDefault();
    const userData = {
      email : values.email,
    }
    PostBeforeLogin('user/forgotpassword', userData).then(result => {
      if(result !== 'Invalid'){
        console.log(result)
        // var inHalfADay = 0.5;
        // Cookies.set('_token', result.token ,{expires: inHalfADay})
        // Cookies.set('_Lid',result.user_data.user_id,)
        this.setState({
          isLogin : true,
          email: values.email,
          message: result.message,
        });
      }else{
        this.setState({
          isLogin : true,
          message: result.message,
        });
      }
    });
  };

  render() {
    // if(this.state.isLogin && Cookies.get('_token')    ){
    //   return <Redirect to='/admin/index'/>
    // }
    return (
      <>
      <Col  lg="3" md="3" >
        

      
      </Col>
      
        <Col lg="6" md="6">{this.state.loading && (
                  <div className="signupLoadingDiv">
                    <SkeletonTheme color="#f7fafc" highlightColor="#5e72e4">
                      <Skeleton height={7} />
                    </SkeletonTheme>
                  </div>
                )}
          <Card className="border-0">
            {/* <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div>
            </CardHeader> */}
            <CardBody className="">
              <div className="text-left text-muted mb-4">
              <small>  <img
                        alt="Logo"
                        className="pb-4"
                        width='50'
                        src={require("../../assets/img/brand/aclogo.png")}
                      /> </small> 
              <h1 className='mb-0'>Forgot your Password? Well, let's fix that!
</h1>
        <p className=''>Enter your email address to recover your password.  
 </p>
     
              </div>
              <Formik 
              initialValues={{ email: "",}}
              onSubmit={(values, { setSubmitting }) => {
this.requestPasswordLink(values)
          }} 
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Please enter a valid email address")
              .required("Email Required"),
          })}
          >
            {props => {
            const {
              values,
              touched,
              errors,
              // isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit
            } = props;
            return (
              <Form role="form" onSubmit={handleSubmit} novalidate>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email adress" type="email"   name="email"
                      // onChange={this.handleChangeInput} 
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        "col" || (errors.email && touched.email && "error")
                      }
                      />
                  </InputGroup>
                       {errors.email && touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                       {this.state.isLogin && <p className="sucess-message"> {`${this.state.message} to ${this.state.email}`} </p>}

                </FormGroup>
                <div className="text-left">
                  <Button className="cta" color="primary" type="submit">
                  Request password link
                  </Button>
                </div>
              </Form>)}}
              </Formik>
 <p className="mt-5 mb-2"> Want to login into your account? <Link to='/auth/login' className='text-cta p-l-0'>Login Here</Link></p>

            </CardBody>
            
          </Card>
          
        </Col>
        <Col  lg="3" md="3" >
        

      
        </Col>
      </>
    );
  }
}

export default Login;

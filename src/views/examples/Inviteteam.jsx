import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Cookies from 'js-cookie'

export default class Inviteteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      roleType : null,
      isLogin: false
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
  this.getRole();
  }
  getRole = () => {
    GetData("ms5", "user/roletypemaster").then(data => {
      this.setState({
        roleType: data.role_master
      });
    });
  };
  inviteTeam = values => {
    // event.preventDefault(); 
    const Teammember = {
      first_name : values.firstName,
      last_name: values.lasttName,
      email: values.email,
      mobile_number: values.phone,
      role_id: values.role
    }
    console.log(Teammember);
    PostData("ms1", "user/inviteteammembers", Teammember ).then(data => {
      if( data !== 'Invalid' ){
        this.setState({
         isLogin:true,
         loading: false
        });
      }else{
        this.setState({
         isLogin:false,
         loading: false
        });
      }
   
    });
  };


  render() {
    const{ roleType, loading} = this.state;
    if(this.state.isLogin && Cookies.get('_token')){
      return <Redirect to='/admin/team-member-list'/>
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            <Col lg="12" >
            <Link to="/admin/team-member-list" className='new-project-modal__close'>
            {/* <span className="text-center new-project-modal__close" style={{ display: "inline-block" }}>
              <i class="material-icons">close</i>
            </span> */}
          </Link></Col>
                <Col  lg="5" md="5" className='mt-6'>
        {/* <h1 className='text-white'>Welcome To Leadify</h1>
        <p className='text-white'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
     
                        <Link to='/auth/register' className='text-cta text-white'><small> Create new account</small> </Link> */}
    <img
                              alt="..."
                              src={require("../../assets/img/theme/invite-team.svg")}
                              className='img-fluid'
                            />
      
      </Col>
            <Col lg="7" xs="12" className='pl-5 mt-7'>
              <h1 className='mb-3'>Invite Your New Team Member </h1>
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  email: "",
                  firstName: "",
                  lasttName: "",
                  phone: "",
                  role: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  this.setState({
                    loading: true
                  });
                  this.inviteTeam(values);
                }}
                validationSchema={Yup.object().shape({
                  email: Yup.string()
                    .email("Please enter a valid email address")
                    .required("Email Required"),
                    role: Yup.string().required("Role Required"),
                    firstName: Yup.string().required("First name Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs-12>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-circle-08" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Name"
                                type="text"
                                name="firstName"
                                // onChange={this.handleChangeInput}
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.firstName &&
                                    touched.firstName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.email && touched.firstName && (
                              <div className="input-feedback">
                                {errors.firstName}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                        {/* <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-circle-08" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Last name"
                                type="text"
                                name="lasttName"
                                // onChange={this.handleChangeInput}
                                value={values.lasttName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.lasttName &&
                                    touched.lasttName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.lasttName && touched.lasttName && (
                              <div className="input-feedback">
                                {errors.lasttName}
                              </div>
                            )}
                          </FormGroup>
                        </Col> */}
                      </Row>
                      <Row>
                        <Col xs-12>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-email-83" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Email"
                                type="email"
                                name="email"
                                // onChange={this.handleChangeInput}
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.email && touched.email && "error")
                                }
                              />
                            </InputGroup>
                            {errors.email && touched.email && (
                              <div className="input-feedback">
                                {errors.email}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row>
                      {/* <Row>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-mobile-button" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Mobile number"
                                type="number"
                                name="phone"
                                // onChange={this.handleChangeInput}
                                value={values.phone}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.phone && touched.phone && "error")
                                }
                              />
                            </InputGroup>
                            {errors.phone && touched.phone && (
                              <div className="input-feedback">
                                {errors.phone}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                        <Col xs-6>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-badge" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Role"
                                type="select"
                                name="role"
                                // onChange={this.handleChangeInput}
                                value={values.role}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.role && touched.role && "error")
                                }
                              >
                                <option> Select role</option>
                               { roleType && roleType.map((data) => <option value={data.role_id}>{data.name}</option>) }
                                 <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option> 
                              </Input>
                            </InputGroup>
                            {errors.role && touched.role && (
                              <div className="input-feedback">
                                {errors.role}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row> */}
                      {/* <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password"   name="password"
                      //onChange={this.handleChangeInput}
                      value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.password && touched.password && "error")
                        }
                        autofill={false}
                      />  
                      </InputGroup>
                      {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                      {this.state.isWrongCreditional && (
                        <div className="input-feedback">
                          Invalid Email or Password
                        </div>
                      )}
                </FormGroup> */}
                      {/* <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div> */}
                      <div className="text-center">
                        <Button
                          className="my-2 cta cta-fullwidth"
                          color="primary"
                          type="submit"
                        >
                          Invite Team Mate 
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}
// const Validatedfrom = () =>{
//   return(

//   )
// }

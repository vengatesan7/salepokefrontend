import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  // InputGroupAddon,
  // InputGroupText,
  Button
} from "reactstrap";
import Skeleton from "react-loading-skeleton";

class themes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previewPopup: false
    };
  }
  render() {
    console.log(this.props)
    return this.props.loading ? (
      <div>
        <Col xs={4} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
        <Col xs={12} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
        <Col xs={12} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
        <Col xs={12} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
        <Col xs={12} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
        <Col xs={12} md={4} className="mb-20">
          <div>
            <div className="text-center mb-20">
              <Skeleton height={200} />
            </div>
            <Skeleton />
          </div>
        </Col>
      </div>
    ) : (
      <Col xs={12} md={4} className="">
        <div>
          <div className="themes-image">
            <img
              src={this.props.src || <Skeleton />}
              alt={this.props.alt}
              className="img-fluid"
            />
            <div className="overlay">
              <Button
                onClick={this.props.themesClick}
                value={this.props.value}
                name={this.props.name}
              >
                {/* {this.props.label} */}
                <i class="material-icons">check</i>
                {/* <i class="material-icons">
                  touch_app
                </i> */}
              </Button>
              {this.props.value !== 27 && <Button onClick={() => this.setState({previewPopup: false})}>
                <i class="material-icons">visibility</i>
              </Button>}
            </div>
          </div>
          <p className=" mt-20 mb-20 themes-title">
            {this.props.title || <Skeleton />}
          </p>
        </div>
        {this.state.previewPopup && <div className="popup-preview">
          <div className="popup-preview-overlay" onClick={() => this.setState({previewPopup: false})} />
          <div className="popup-preview-container">
            <span className="popup-preview-close" onClick={() => this.setState({previewPopup: false})}>
              <i class="material-icons">close</i>
            </span>
            <div className="preview">
              <img src={this.props.previewImage}/>
            </div>
          </div>
        </div>}
      </Col>
    );
  }
}

export default themes;

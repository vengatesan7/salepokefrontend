import React from "react";

// reactstrap components

import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  DropdownMenu,
  DropdownItem,
  FormGroup,
  Form,
  CardBody,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Row,
  Modal,
  Button,
  Col,
  Label
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { ms1ImageURL } from "../services/Appurl";
import * as Yup from "yup";
import { Formik } from "formik";
// import { PostData } from "../services/PostData";
// import { Formik } from "formik";
// import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import { PostData } from "../services/PostData";
import TableSkelton from "../../components/Skeleton/index";
import Cookies from "js-cookie";
import cookie from 'react-cookies'
import Alltriggers from "./Pulsetrigger";
import FacebookLogin from 'react-facebook-login';
import { toast } from "react-toastify";


class Socialproof extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
  campaignModal:false,
      loading: true,
      campaignId:null,
      addNew: false,
      currentStep:0,
      callMethod:false,
      passData:null,
      fields: [""],
      fieldsc: [""],

    };
    this.onClickButtonAdder = this.onClickButtonAdder.bind(this);
    // this.onClickButtonSubmit = this.onClickButtonSubmit.bind(this);
    this.onClickButtonAdderc = this.onClickButtonAdderc.bind(this);

  }

  componentDidMount() {
    this.getCampaignData();
    this.setState({
      loading: false
    })
  }
  toggleModal = state => {
    this.setState({
      [state]: !this.state[state]
    });
  };
  getCampaignData = () => {
    PostData("ms1", "get-all-campaigns").then(data => {
      console.log(data);
      toast.info(data.message);

      this.setState({
        campaignData: data.campaigns,
        loading: false
      });
    });
  };

  createCampaign = (values) =>{
    const filteredValues = this.state.fields.filter(value => value);
    const filteredValuesc = this.state.fieldsc.filter(value => value);

    console.log(filteredValues, filteredValuesc);
    const campaignData = {
     campaign_name:values.campignName,
    website_url:filteredValues,
    conversion_url:filteredValuesc,
    time_frame : values.time_frame,
    utm_source: "salepoke",
    utm_medium: "notification",
    initial_delay: 5,
    display_time: 5,
    inbetween_delay: 5,
    loop_notification: false,
    campaign_status: true, 
    mob_notification:true,
    desk_notification:true,
    mob_position:"bottom",
    desk_position:"bottom-left",
    url_type: values.url_type,
    campaign_type: values.notfi_type,

  }
    PostData("ms1", "create-campaign", campaignData).then(data => {
      console.log(data);
      this.setState({
        campaignId: data.campaign.campaign_id,
        campaignType:data.campaign.campaign_type,
        // addNew: true,
      });
      this.nextStep();
    });
      }
      nextStep = () => {
        const { currentStep } = this.state;
        let s = currentStep + 1;
        if (s === 3) {
          s = 0;
        }
        this.setState({
          currentStep: s,
        });
      };
      PreviousStep = () => {
        const { currentStep } = this.state;
        let s = currentStep - 1;
        if (s === -1) {
          s = 2;
        }
        this.setState({
          currentStep: s,
        });
      };
      
  responseFacebook = (response) => {
    console.log(response);
    this.setState({
      facebookLoginLoading: true,
      facebookIntegrationPopup:false,
    });
    var data = { access_token: response.accessToken, fb_user_id: response.id };
    PostData('ms1', 'getfacebookpagesaccess', data).then(response => {
      console.log(response);
      // if (response !== 'invalid') {
      //   if (response.status === 'success') {
      //     if (response.message !== 'No Facebook Page Found On Your Account' && response.message !== 'No Facebook AdAccount Found On Your Account') {
      //       // this.getFacebookPageData();
      //     } else {
      //       this.setState({
      //         loginButton: true,
      //         facebookDataLoading: false,
      //         facebookLoginLoading: false,
      //         facebookDisconnectLoading: false,
      //       })
      //     }
      //     toast.info(response.message);
      //     this.setState({
      //       facebookAPIError: '',
      //       pageErrors: response.errors ? response.errors : [],
      //     })
      //   } else if (response.status === 'api_error') {
      //     var message = response.message;
      //     this.setState({
      //       facebookAPIError: message.message,
      //       facebookLoginLoading: false,
      //     })
      //   }
      // }
    });
  }
  cancelFacebook = (response) => {
    this.setState({
      facebookLoginLoading: false,
      facebookIntegrationPopup: false,
    });
    toast.info("Facebook Connect Canceled");
  }
  onClickButtonAdder(event) {
    event.preventDefault();
    this.setState({
      fields: [...this.state.fields, ""]
    });
  }

  onClickFormGroupButton(index) {
    let fields = [...this.state.fields];
    fields.splice(index, 1);
    this.setState({ fields });
  }

  onChangeFormGroupInput(index, event) {
    let fields = [...this.state.fields];
    fields[index] = event.target.value;
    this.setState({ fields });
  }

  onClickButtonAdderc(event) {
    event.preventDefault();
    this.setState({
      fieldsc: [...this.state.fieldsc, ""]
    });
  }

  onClickFormGroupButtonc(index) {
    let fieldsc = [...this.state.fieldsc];
    fieldsc.splice(index, 1);
    this.setState({ fieldsc });
  }

  onChangeFormGroupInputc(index, event) {
    let fieldsc = [...this.state.fieldsc];
    fieldsc[index] = event.target.value;
    this.setState({ fieldsc });
  }
  // onClickButtonSubmit(event) {
  //   event.preventDefault();
  //   const filteredValues = this.state.fields.filter(value => value);
  //   console.log(filteredValues);
  // }
  render() {
    const {
      loading,
      campaignData,
      campaignId,
      addNew,
      currentStep
    } = this.state;
    const isFormGroupDeletionAllowed =
      this.state.fields.length > 1 ? true : false;
    // cookie.save('comingFrom', "EDITTRIGGERS", { path: '/' })

    // if (addNew && campaignId) {
    //   return <Redirect  to={{
    //     pathname: "/admin/socialproof/socialprooff-overview",
    //     state: {
    //       campaignId: campaignId
    //     }
    //   }}
    //   />;
    // }

    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              <div className="">
                {loading ? <TableSkelton />:  
                  <React.Fragment>
<Row className="align-items-center">
  {/* <Col>
  <FacebookLogin
                  appId="2567301360244976"
                  autoLoad={false}
                  fields="name,email,picture"
                  textButton="Connect"
                  callback={this.responseFacebook}
                  scope="pages_show_list,pages_read_user_content "
                  cssClass="connect-facebook bg-success"
                  onFailure={this.cancelFacebook}
                />

  </Col> */}
<Col xl="12">
              <Card className="border-0 bg-transparent">
                <CardHeader className="bg-transparent pl-0 pr-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-muted ls-1 mb-1">
                      Campaigns
                      </h6>
                      <h2 className="mb-0">All Campaigns</h2>
                    </div>
                    <div className="col text-right">
                    <Button
                            color="primary"
                            className='inner-cta'
                            size="md"
                            onClick={() => this.toggleModal("campaignModal")}

                          >
                            Add new campaign
                          </Button>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody className=" pl-0 pr-0">
                  {/* Chart */}
                  { campaignData && campaignData.length !== 0 ? campaignData.map( data => <>
                    <Card className="mb-4">
                    <CardBody>
                      <Link 
                      to={{
                                pathname: "/admin/socialproof/socialprooff-overview",
                                state: {
                                  campaignId: data.campaign_id
                                }
                              }}
                      >
                        <Row className='align-items-center'>
                          <Col md='1' className='text-center'>
                          <img src={require('../../assets/img/theme/live.svg')} alt="upload-csv" title="upload-csv" className="img-fluid" width='50px' />
                          </Col>
                          <Col>
                          <h3> {data.campaign_name}</h3>
<Badge color="success">Active</Badge>

<p className='mb-0'><small>Create at {data.created_at}</small></p>

                          </Col>
                        </Row>
                        
                        </Link>
                    </CardBody>
                  </Card>
                  
                 </>) : <p>No Data</p>}
                 

                </CardBody>
              </Card>
            </Col>
            </Row>    
                  </React.Fragment>
                
  }
              </div>
            </Col>
          </Row>
        </Container>
        <Modal
          className="modal-dialog-centered full-popup"
          isOpen={this.state.campaignModal}
          toggle={() => this.toggleModal("campaignModal")}
        >
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
Create Campaign            </h5>
            <button
              aria-label="Close"
              className="close"
              data-dismiss="modal"
              type="button"
              onClick={() => this.toggleModal("campaignModal")}
            >
              <span aria-hidden={true}>×</span>
            </button>
          </div>
          <div className="modal-body mt-0">
          <>
          <Formik
                initialValues={{
                  campignName: "example name",
                  // website: "example.com",
                  notfi_type: 'notification',
                  url_type: 'all',
                  conversion_url:'',
                  time_frame: "alltime"

                }}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  // 
                  

                  this.createCampaign(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  campignName: Yup.string().required("Campign name Required"),
                  // website: Yup.string().required("Website url Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
          {currentStep === 0 &&
          <>
                       <Row>
                        <Col xs-12 md-12>
                        <h3 className="mb-2 mt-4">
                        Campaign Name                          </h3>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-send" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Site name"
                                type="text"
                                name="campignName"
                                // onChange={this.handleChangeInput}
                                value={values.campignName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.campignName &&
                                    touched.campignName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.campignName && touched.campignName && (
                              <div className="input-feedback">
                                {errors.campignName}
                              </div>
                            )}
                          </FormGroup>
                         
                        </Col>
                        </Row>
                      <Row>
                        <Col>
                        <h3 className="mb-2 mt-4">
                        Notification Type                          </h3>
                        </Col>
                        </Row>
                   
                       
                       
                       
                        <Row>
                      <Col md="4" className="accountForm">
                            <Label className="accountTypeOption">
                              <Input
                                type="radio"
                                name="notfi_type"
                                id="accountSelection"
                                value="notification"
                                checked={values.notfi_type === 'notification'}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={"col" || (errors.notfi_type && touched.notfi_type && "error")}
                              />
                              <div className="account-slection-wrap">
                                <img src={require('../../assets/img/theme/notes.png')} alt="upload-csv" title="upload-csv" className="img-fluid" />
                                <h2 className="">Sticky Notification</h2>
                                <p className="">Emphasis important points to your visitors,make them feel comfortable</p>
                              </div>
                            </Label>                        
                          </Col>
                          <Col md="4" className="accountForm">
                            <Label className="accountTypeOption">
                              <Input
                                type="radio"
                                name="notfi_type"
                                id="accountSelection"
                                value="livevisitor"
                                checked={values.notfi_type === 'livevisitor'}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={"col" || (errors.notfi_type && touched.notfi_type && "error")}
                              />
                              <div className="account-slection-wrap">
                                <img src={require('../../assets/img/theme/woman.png')} alt="upload-csv" title="upload-csv" className="img-fluid" />
                                <h2 className="">Live Visitors</h2>
                                <p className="">shows the number of people currently viewing a page or your whole site.</p>
                              </div>
                            </Label>                        
                          </Col>
                          <Col md="4" className="accountForm">
                            <Label className="accountTypeOption">
                              <Input
                                type="radio"
                                name="notfi_type"
                                id="accountSelection"
                                value="roundup"
                                checked={values.notfi_type === 'roundup'}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={"col" || (errors.notfi_type && touched.notfi_type && "error")}
                              />
                              <div className="account-slection-wrap">
                                <img src={require('../../assets/img/theme/fire.png')} alt="upload-csv" title="upload-csv" className="img-fluid" />
                                <h2 className="">Heat</h2>
                                <p className="">show the total page visits, conversions, or real time visitor on your website</p>
                              </div>
                            </Label>                        
                          </Col>
                      </Row>
                     
                     
                      </>
                }           
                   {currentStep === 1 &&  
                   <>  <Row>
                        <Col>
                        <h3 className="mb-2 mt-4">
                       URL Match type                     </h3>
                        </Col>
                      </Row>
                     <Row>
                       <Col>
                       <div className="custom-control custom-radio mb-3">
          <input
            type="radio"
            name="url_type"
            id="customRadio5"
            value="all"
            checked={values.url_type === 'all'}
            onChange={handleChange}
            onBlur={handleBlur}
            className={"custom-control-input" || (errors.url_type && touched.url_type && "error")}

          />
          <label className="custom-control-label" htmlFor="customRadio5">
            All Pages
          </label>
        </div>
                       <div className="custom-control custom-radio mb-3">
          <input
            type="radio"
            name="url_type"
            id="customRadio7"
            value="simple"
            checked={values.url_type === 'simple'}
            onChange={handleChange}
            onBlur={handleBlur}
            className={"custom-control-input" || (errors.url_type && touched.url_type && "error")}
          />
          <label className="custom-control-label" htmlFor="customRadio7">
            Simple
          </label>
        </div>
        <div className="custom-control custom-radio mb-3">
          <input
           type="radio"
           name="url_type"
           id="customRadio6"
           value="contains"
           checked={values.url_type === 'contains'}
           onChange={handleChange}
           onBlur={handleBlur}
           className={"custom-control-input" || (errors.url_type && touched.url_type && "error")}
          />
          <label className="custom-control-label" htmlFor="customRadio6">
            Contains
          </label>
        </div>

                       </Col>
                     </Row>

                      <Row>
                      <Col xs-12 md-12>
                        <h3 className="mb-2 mt-4">
                        Where to display                       </h3>


                        <div className="dynamicForm">
     

        {this.state.fields.map((value, index) => (
          <FormGroups
            inputChange={this.onChangeFormGroupInput.bind(this, index)}
            buttonClick={this.onClickFormGroupButton.bind(this, index)}
            buttonDisabled={index === 0 ? !isFormGroupDeletionAllowed : undefined}
            value={value}
            key={index}
          />
        ))}

<div className="dynamicForm__buttonWrapper">
          <FormButtons
            click={this.onClickButtonAdder}
            type="ghost"
            innerHtml="Add URL"
          />
          {/* <FormButtons click={this.onClickButtonSubmit} innerHtml="Submit" /> */}
        </div>
      </div>


                        
                          {/* <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-world" />
                                </InputGroupText>
                              </InputGroupAddon>

                              <Input
                                placeholder="Type your site url"
                                type="text"
                                name="website"
                                // onChange={this.handleChangeInput}
                                value={values.website}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.website &&
                                    touched.website &&
                                    "error")
                                }
                              />


                            </InputGroup>
                            {errors.website && touched.website && (
                              <div className="input-feedback">
                                {errors.website}
                              </div>
                            )}
                          </FormGroup> */}
                                                 {/* <Selectbox /> */}

                        </Col>
                     
                      </Row>

                      {values.notfi_type === 'roundup' && <><Row>
                      <Col xs-12 md-12>
                        <h3 className="mb-2 mt-4">
                        Where to Capture Conversions                       </h3>



                        <div className="dynamicForm">
     

     {this.state.fieldsc.map((value, index) => (
       <FormGroups
         inputChange={this.onChangeFormGroupInputc.bind(this, index)}
         buttonClick={this.onClickFormGroupButtonc.bind(this, index)}
         buttonDisabled={index === 0 ? !isFormGroupDeletionAllowed : undefined}
         value={value}
         key={index}
       />
     ))}

<div className="dynamicForm__buttonWrapper">
       <FormButtons
         click={this.onClickButtonAdderc}
         type="ghost"
         innerHtml="Add URL"
       />
       {/* <FormButtons click={this.onClickButtonSubmit} innerHtml="Submit" /> */}
     </div>
   </div>


   
                        
                          {/* <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-world" />
                                </InputGroupText>
                              </InputGroupAddon>

                              <Input
                                placeholder="Type your conversion url"
                                type="text"
                                name="conversion_url"
                                // onChange={this.handleChangeInput}
                                value={values.conversion_url}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.conversion_url &&
                                    touched.conversion_url &&
                                    "error")
                                }
                              />


                            </InputGroup>
                            {errors.conversion_url && touched.conversion_url && (
                              <div className="input-feedback">
                                {errors.conversion_url}
                              </div>
                            )}
                          </FormGroup> */}
                                                 {/* <Selectbox /> */}

                        </Col>
                     
                      </Row>

                      <Row>
                        <Col>
                        <h3 className="mb-2 mt-4">
                        Display conversions from the last
                  </h3>
                        </Col>
                      </Row>
                     <Row>
                       <Col>
                       <div className="custom-control custom-radio mb-3">
          <input
            type="radio"
            name="time_frame"
            id="customRadio10"
            value="24 hours"
            checked={values.time_frame === '24 hours'}
            onChange={handleChange}
            onBlur={handleBlur}
            className={"custom-control-input" || (errors.time_frame && touched.time_frame && "error")}

          />
          <label className="custom-control-label" htmlFor="customRadio10">
           24 hours
          </label>
        </div>
                       <div className="custom-control custom-radio mb-3">
          <input
            type="radio"
            name="time_frame"
            id="customRadio11"
            value="7 days"
            checked={values.time_frame === '7 days'}
            onChange={handleChange}
            onBlur={handleBlur}
            className={"custom-control-input" || (errors.time_frame && touched.time_frame && "error")}
          />
          <label className="custom-control-label" htmlFor="customRadio11">
            7 days
          </label>
        </div>
        <div className="custom-control custom-radio mb-3">
          <input
           type="radio"
           name="time_frame"
           id="customRadio12"
           value="30 days"
           checked={values.time_frame === '30 days'}
           onChange={handleChange}
           onBlur={handleBlur}
           className={"custom-control-input" || (errors.time_frame && touched.time_frame && "error")}
          />
          <label className="custom-control-label" htmlFor="customRadio12">
            30 Days
          </label>
        </div>
        <div className="custom-control custom-radio mb-3">
          <input
           type="radio"
           name="time_frame"
           id="customRadio13"
           value="alltime"
           checked={values.time_frame === 'alltime'}
           onChange={handleChange}
           onBlur={handleBlur}
           className={"custom-control-input" || (errors.time_frame && touched.time_frame && "error")}
          />
          <label className="custom-control-label" htmlFor="customRadio13">
            Alltime
          </label>
        </div>

                       </Col>
                     </Row>
                      </>
                            }

                      
                    </>
                            }
                       
<Row>
                       <div className="text-left mt-4 col-6 ">{currentStep === 0 && 
                      
                         <Button type="button" className="cancel-btn" onClick={() => this.toggleModal("campaignModal")}>Cancel</Button> }
                         {currentStep === 1 && 
                      <Button type="button" className="cancel-btn" onClick={this.PreviousStep} > Previous </Button>}</div>
                            
                            
                  {currentStep === 0 &&<div className="text-right mt-4 col-6 ">
                  <button className="continue-btn cta btn btn-primary" type="button" onClick={this.nextStep} disabled={!values.campignName} >Next</button>   
                  </div> }


                  {currentStep === 1 &&  <div className="col-6 text-right mt-4">
                        <Button
                          className="my-2 cta "
                          color="primary"
                          type="submit"
                          // disabled={!this.state.values} 
                        >
                          Continue
                        </Button>
                      </div>}
                      </Row>
                    </Form>
                  );
                }}
              </Formik>
           

              {currentStep === 2 && <Row>
                        <Col>
                      <Alltriggers type={this.state.campaignType} campaignId={this.state.campaignId}/>
                      </Col>
                      </Row>}


           
      </>

          </div>
          
        </Modal>
      </>
    );
  }
}

export default Socialproof;



function FormButtons(props) {
  let buttonType = props.type;
  let buttonModifierClassName = "";

  if (buttonType) {
    buttonType = `${buttonType[0].toUpperCase()}${buttonType.slice(1)}`; // capitalize
    buttonModifierClassName = `btn btn-secondary dynamicForm__button--is${buttonType}`;
  }

  return (
    <button
      className={`dynamicForm__button ${buttonModifierClassName}`}
      onClick={props.click}
    >
      {props.innerHtml}
    </button>
  );
}

function FormGroups(props) {
  return (
    <div className="dynamicForm__item">
      <input
        className="dynamicForm__itemInput"
        type="text"
        value={props.value}
        onChange={props.inputChange}
      />
      <button
        className="dynamicForm__itemButton"
        type="button"
        onClick={props.buttonClick}
        disabled={props.buttonDisabled}
        tabIndex="-1"
      />
    </div>
  );
}
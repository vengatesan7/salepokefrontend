import React from "react";
import { GetData } from "../services/GetData";
import { Link, Redirect } from "react-router-dom";
import Cookies from "js-cookie";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

// reactstrap components
import {
  Button,
  Card,
  // CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

import { Context } from "../../context/index";
import { PostData } from "../services/PostData";

class Login extends React.Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
    
      mailData: {
        subject: "",
        content:""
      },
      isLogin: false,
      isWrongCreditional: false,
      islogedin: false,
      loading: true,
      emailReply: true,
      smsReply: false,
      activeId : Cookies.get('_LassignUserId') ? Cookies.get('_LassignUserId')  : "self",
      activeUserName :     Cookies.get('_LassignUserName') ?     Cookies.get('_LassignUserName')  : "self",


    };
  }
  componentDidMount() {
    this.getClientData();
      this.setState({
        loading: false,
      });
  
  }
  
  selectQuickmail = id => {
    // console.log(id)
    this.setState({
      activeId:id,
    });
    Cookies.set('_LassignUserId' , id )
    const selectedQucikMail = this.state.clientData.filter(data => {
      return data.user_id == id;
    });
    const contentIn = selectedQucikMail.map(data => {
      Cookies.set('_LassignUserName' , `${data.first_name} ${data.last_name}` )

      this.setState({
        activeUserName : `${data.first_name} ${data.last_name}`,
      });
      return data;
    });
  };
  leadfrom = event => {
    console.log(event);
    this.setState({
      // loading: true,
      // contactType: event.target.name,
      active: event.target.name
    });
  };
 
  getClientData = () => {
    PostData("ms1", "channellist").then(data => {
      this.setState({
        clientData: data.channellist,
        loading:false
      });
    });
  };
 
  render() {
    const { clientData ,activeId , campaginData, activeUserName} = this.state;
    console.log(activeId);
    if (this.state.isLogin) {
      return <Redirect 
      to={{
        pathname: "/plain/quicksms",
        state: campaginData
      }}
     />;
    }
    return (
      <>
      <Row>
        <Col md="12" className="text-right mt-3 pb-3 cancel">
          <Link to="/admin/campaigns" className='new-project-modal__close'>
            {/* <span className="text-center new-project-modal__close" style={{ display: "inline-block" }}>
              <i class="material-icons">close</i>
            </span> */}
          </Link>
        </Col>
        <Col md='12' className='mb-4 mt-5'>
        <h1 className="">Switch Account </h1>
          <p className="">
            Quick replies allow you to instantly send a preset email or SMS
            (through the mobile app only) message to your leads.</p>
        </Col>
        <Col lg="12" md="12">
         
          <React.Fragment>
          
            <Row>
            <Col xs='12'>
            <div
                    className={"self"=== activeId ? "active qucikreplay" :"qucikreplay"}
                    onClick={() => this.selectQuickmail("self")}
                  >
                    <i class="material-icons">
check_circle
</i>
                    {/* <h4>Channel one</h4> */}
                  </div>

</Col>
           
            {clientData &&
            clientData.map(data => {
              return (
                <React.Fragment>
                   <Col xs='4'>
                  <div
                    className={data.user_id == activeId ? "active qucikreplay" :"qucikreplay"}
                    onClick={() => this.selectQuickmail(data.user_id)}
                  >
                    <i class="material-icons">
check_circle
</i>
                    <h4>{ `${data.first_name} ${data.last_name}`}</h4>
                    {/* <p>{data.content}</p> */}
                  </div>
                  </Col>
                </React.Fragment>
              );
            })}

            </Row>
          </React.Fragment>
        
        </Col>
        </Row>
      </>
    );
  }
}

export default Login;

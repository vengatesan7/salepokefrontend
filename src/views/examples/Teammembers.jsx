import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import TableSkelton from "../../components/Skeleton/index";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      teamMemberList: null,
      loading: true,
      addNew: false,
    };
  }

  componentDidMount() {
this.setState({
  loading: false,
  teamMemberList :  [
    {
        "user_id": 9,
        "user_name": "dsfa",
        "email": "abcd2121@yopmail.com",
        "mobile_number": null,
        "created_at": "2020-08-12 14:20:47",
        "role_id": 2
    }
]

})  }
delete = () =>{
  console.log("delete")
}

addNewmember = () =>{
this.setState({
  addNew:true,
})}
  render() {
    const {
      error,
      loading,
      addNew,
      teamMemberList
    } = this.state;

    if (addNew) {
      return <Redirect to="/plain/invite-team" />;
    }
    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              {/* <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Add New Client{" "}
                      </Button>
                    </Col>
                  </Row>
                </CardHeader> */}
              {/* <CardBody> */}

              <div className="border-0">
                {loading ? (
                  <TableSkelton />
                ) : (
                  teamMemberList !== null && (
                    <React.Fragment>
                      <div className="border-0 mb-5"> 
                        <Row className="align-items-center">
                          <div className="col">
                            <h3 className="mb-0">Team Members</h3>
                          </div>
                          <div className="col text-right">
                            <Button
                              color="primary"
                              onClick={this.addNewmember}
                              size="sm"
                            >
                              Invite Team Member
                            </Button>
                          </div>
                        </Row>
                      </div>
                      <Table
                        className="align-items-center table-flush"
                        responsive
                      >
                        <thead className="thead-light">
                          <tr>
                            <th scope="col">Name</th>

                            <th scope="col">Email</th>
                            <th scope="col">Created at</th>

                            <th scope="col"> </th>
                          </tr>
                        </thead>
                        <tbody>
                          {teamMemberList.map(data => {
                            return (
                              <tr>
                                <th scope="row">{data.user_name}</th>
                                <td>
                                  {/* <i className="fas fa-arrow-up text-success mr-3" />{" "} */}
                                  {data.email}
                                </td>
                                <td>{data.created_at}</td>
                                <th scope="col">
                                  {" "}
                                  <span className="table-buttons float-right">
                                    <button
                                      onClick={() => this.delete(data.user_id)}
                                      name={data.user_id}
                                    >
                                      <i class="material-icons">delete</i>
                                    </button>
                                  </span>
                                </th>
                              </tr>
                            );
                          })}
                        </tbody>
                      </Table>
                    </React.Fragment>
                  )
                )}
              </div>

              {/* </CardBody> */}
              {/* </Card> */}
            </Col>
          </Row>
          
        </Container>
      </>
    );
  }
}

export default Profile;

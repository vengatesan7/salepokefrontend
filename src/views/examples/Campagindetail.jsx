import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import Detailskeleton from "../../components/Skeleton/Detailskeleton.jsx";
import { ms5ImageURL } from "../services/Appurl";

class Campagindetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      campaignData: null,
      campaignUrlData:null,
      firstName: undefined,
      lastName: undefined,
      email: undefined,
      address: undefined,
      city: undefined,
      countryCode: undefined,
      mobile: undefined,
      oldPassword: undefined,
      newPassword: undefined,
      confirmNewPassword: undefined,
      state: undefined,
      addNew: false,
      loading: true
    };
  }

  componentDidMount() {
    this.getClientData();
    this.getUrlData();
  }
  getClientData = () => {
    const id = {
      campaign_id: this.props.location.state.campaignId
    };
    console.log(id);
    PostData("ms2", "getcampaignbyid", id).then(data => {
      console.log(data.getcampaign);
      this.setState({
        campaignData: data.getcampaign,
        loading: false
      });
    });
  };
  getUrlData = () => {
    const id = {
      campaign_id: this.props.location.state.campaignId
    };
    console.log(id);
    PostData("ms2", "getsiteurlsbycampaignid", id).then(data => {
      console.log(data.getcampaign);
      this.setState({
        campaignUrlData: data.getcampaignsiteurls,
        loading: false
      });
    });
  };

  addIntegration = id => {
    // console.log(event.target.name)
    this.setState({
      addNewIntegration: true,
      campaignId: id
    });
  };
  manageQuickReply = id => {
    this.setState({
      manageQuickReply: true,
      campaignId: id
    });
  };
  back = () => {
    this.setState({
      back: true
    });
  };
  render() {
    const {
      campaignData,
      back,
      addNewIntegration,
      manageQuickReply,
      campaignId,
      loading,
      campaignUrlData,
    } = this.state;
console.log(this.state.campaignUrlData)
    if (addNewIntegration && campaignId !== null) {
      return (
        <Redirect
          to={{
            pathname: "/plain/add-new-integration",
            state: {
              campaignId: this.state.campaignId,
              from: "campainDetail"
            }
          }}
        />
      );
    }
    if (back) {
      return (
        <Redirect
          to={{
            pathname: "/admin/campaigns"
          }}
        />
      );
    }
    if (manageQuickReply && campaignId !== null) {
      return (
        <Redirect
          to={{
            pathname: "/plain/update-quick-email",
            state: {
              campaignId: this.state.campaignId,
              from: "campainDetail"
            }
          }}
        />
      );
    }

    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              {/* <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Add New Client{" "}
                      </Button>
                    </Col>
                  </Row>
                </CardHeader> */}
              {/* <CardBody> */}
              <Card className="border-0">
                {loading ? (
                  <Detailskeleton />
                ) : (
                  campaignData !== null && (
                    <React.Fragment>
                      <CardHeader className="border-0">
                        <Row className="align-items-center">
                          <div className="col">
                            <h3 className="mb-0"> Campaign information</h3>
                          </div>
                          <div className="col text-right">
                            {campaignData.map(data => {
                              return (
                                <React.Fragment>
                                  <span className="table-buttons">
                                    <button
                                      onClick={() =>
                                        this.addIntegration(data.campaign_id)
                                      }
                                      name={data.campaign_id}
                                    >
                                      {" "}
                                      <i class="material-icons">link</i>
                                    </button>
                                    <button
                                      onClick={() =>
                                        this.manageQuickReply(data.campaign_id)
                                      }
                                      name={data.campaign_id}
                                    >
                                      {" "}
                                      <i class="material-icons">reply</i>
                                    </button>
                                    <small>
                                      {" "}
                                      <button
                                        onClick={this.back}
                                        name={"data.campaign_id"}
                                      >
                                        <i
                                          class="material-icons"
                                          style={{
                                            position: "relative",
                                            top: "6px"
                                          }}
                                        >
                                          arrow_back_ios
                                        </i>
                                        Back
                                      </button>
                                    </small>
                                    {/* <button
                            onClick={() =>
                              this.campaginDetail(data.campaign_id)
                            }
                            name={data.campaign_id}
                          >
                            {" "}
                            <i class="material-icons">assignment</i>
                          </button> */}
                                  </span>
                                </React.Fragment>
                              );
                            })}
                          </div>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        {campaignData.map(data => {
                          return (
                            <React.Fragment>
                              <Row className="align-items-">
                                <div className="col-7">
                                  <h6 className="heading-small text-muted mb-2">
                                    Campaign Name
                                  </h6>

                                  <p className="mb-5"> {data.campaign_name}</p>
                                  <h6 className="heading-small text-muted mb-2">
                                    Visitors
                                  </h6>

                                  <p> {data.visitors}</p>

                                  <h6 className="heading-small text-muted mb-2">
                                  Views
                                  </h6>

                                  <p> {data.views}</p>

                                  <h6 className="heading-small text-muted mb-2">
                                  Conversions
                                  </h6>

                                  <p> {data.conversions}</p>
                                  <h6 className="heading-small text-muted mb-2">
                                  Conversion Rate
                                  </h6>

                                  <p> {data.conversion_rate}</p>
                                  <h6 className="heading-small text-muted mb-2">
                                  Sites
                                  </h6>
                                  {campaignUrlData !== null && (
                                    campaignUrlData.map(data => {
                                   return <p>{data.view_website_url}</p>
                                    })
                                  )}
                                </div>
                                <div className="col-5 shadow">
                                  <img
                                    src={`${ms5ImageURL}${data.pop_up_tmpl_img_prvw_url}`}
                                    alt={data.campaign_name}
                                    title={data.campaign_name}
                                  />
                                </div>
                              </Row>
                            </React.Fragment>
                          );
                        })}
                      </CardBody>
                    </React.Fragment>
                  )
                )}
              </Card>

              {/* </CardBody> */}
              {/* </Card> */}
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Campagindetail;

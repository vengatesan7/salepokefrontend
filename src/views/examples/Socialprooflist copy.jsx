class DynamicForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { fields: ["", "", ""] };
    this.onClickButtonAdder = this.onClickButtonAdder.bind(this);
    this.onClickButtonSubmit = this.onClickButtonSubmit.bind(this);
  }

  onClickButtonAdder(event) {
    event.preventDefault();
    this.setState({
      fields: ["", ...this.state.fields]
    });
  }

  onClickFormGroupButton(index) {
    let fields = [...this.state.fields];
    fields.splice(index, 1);
    this.setState({ fields });
  }

  onChangeFormGroupInput(index, event) {
    let fields = [...this.state.fields];
    fields[index] = event.target.value;
    this.setState({ fields });
  }

  onClickButtonSubmit(event) {
    event.preventDefault();
    const filteredValues = this.state.fields.filter(value => value);
    console(filteredValues);
  }

  render() {
    const isFormGroupDeletionAllowed =
      this.state.fields.length > 1 ? true : false;

    return (
      <div className="dynamicForm">
        <div className="dynamicForm__buttonWrapper">
          <FormButton
            click={this.onClickButtonAdder}
            type="ghost"
            innerHtml="Add Field"
          />
          <FormButton click={this.onClickButtonSubmit} innerHtml="Submit" />
        </div>

        {this.state.fields.map((value, index) => (
          <FormGroup
            inputChange={this.onChangeFormGroupInput.bind(this, index)}
            buttonClick={this.onClickFormGroupButton.bind(this, index)}
            buttonDisabled={index === 0 ? !isFormGroupDeletionAllowed : undefined}
            value={value}
            key={index}
          />
        ))}
      </div>
    );
  }
}

function FormButton(props) {
  let buttonType = props.type;
  let buttonModifierClassName = "";

  if (buttonType) {
    buttonType = `${buttonType[0].toUpperCase()}${buttonType.slice(1)}`; // capitalize
    buttonModifierClassName = `dynamicForm__button--is${buttonType}`;
  }

  return (
    <button
      className={`dynamicForm__button ${buttonModifierClassName}`}
      onClick={props.click}
    >
      {props.innerHtml}
    </button>
  );
}

function FormGroup(props) {
  return (
    <div className="dynamicForm__item">
      <input
        className="dynamicForm__itemInput"
        type="text"
        value={props.value}
        onChange={props.inputChange}
      />
      <button
        className="dynamicForm__itemButton"
        type="button"
        onClick={props.buttonClick}
        disabled={props.buttonDisabled}
        tabIndex="-1"
      />
    </div>
  );
}

ReactDOM.render(<DynamicForm />, document.getElementById("root"));

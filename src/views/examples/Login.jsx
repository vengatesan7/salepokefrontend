
import React from "react";
import { PostBeforeLogin } from '../services/PostData'
import { Link, Redirect } from "react-router-dom";
import Cookies from 'js-cookie'
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
  CardFooter
} from "reactstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import { Context } from "../../context/index";

class Login extends React.Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      isLogin : false,
      isWrongCreditional: false,
      islogedin: false,
      loading: true 

    };
  }
  componentDidMount(){
    this.setState({
      loading:false,
    });
  }
  handleChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value,isWrongCreditional: false,});
  };
  createNewAccount = values => {
    // event.preventDefault();
    const userData = {
      email : values.email,
      password: values.password,
    }
    PostBeforeLogin('login-user', userData).then(result => {
      console.log(result)
      if(result !== 'Invalid'){
        if(result.status === "success"){
          // var inHalfADay = 1;
          Cookies.set('_token', result.token );
          Cookies.set('_ACid',result.user.user_id);
          // Cookies.set('_ACrole',result.user_data.role_id);
          Cookies.set('_ACname',`${result.user.name}`)
          this.setState({
            isLogin : true
          });
        }else{
          this.setState({
            isWrongCreditional : true
          });
        }
       
      }else{
        this.setState({
          isWrongCreditional : true
        });
      }
    });
  };

  render() {
    
    if(this.state.isLogin && Cookies.get('_token')    ){
      return <Redirect to='/admin/index'/>
    }
    return (
      <>
      
      <Col  lg="3" md="3" >
        </Col>
        <Col lg="6" md="6">
          {this.state.loading && (
                  <div className="signupLoadingDiv">
                    <SkeletonTheme color="#f7fafc" highlightColor="#5e72e4">
                      <Skeleton height={7} />
                    </SkeletonTheme>
                  </div>
                )}
          <Card className="border-0">
            {/* <CardHeader className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div>
            </CardHeader> */}
            {/* <CardBody className="px-lg-5 py-lg-5"> */}
            <CardBody className="">

              <div className="text-left text-muted mb-3">
                <small>  <img
                        alt="Logo"
                        className="mb-4 mt-3"
                        width='50'
                        src={require("../../assets/img/brand/aclogo.png")}
                      /> </small> 

<h1 className="mb-0">SalePoke Sign in</h1>
<p> to continue your SalePoke account </p>

              </div>
              <Formik 
              initialValues={{ email: "", password: "" }}
              onSubmit={(values, { setSubmitting }) => {
console.log(values)
this.createNewAccount(values)
          }} 
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Please enter a valid email address")
              .required("Email Required"),
            password: Yup.string().required("Password Required")
          })}
          >
            {props => {
            const {
              values,
              touched,
              errors,
              // isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit
            } = props;
            return (
              <Form role="form" onSubmit={handleSubmit} novalidate>
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Email" type="email"   name="email"
                      // onChange={this.handleChangeInput} 
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      className={
                        "col" || (errors.email && touched.email && "error")
                      }
                      />
                  </InputGroup>
                       {errors.email && touched.email && (
                        <div className="input-feedback">{errors.email}</div>
                      )}
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input placeholder="Password" type="password"   name="password"
                      //onChange={this.handleChangeInput}
                      value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.password && touched.password && "error")
                        }
                        autofill={false}
                      />  
                      </InputGroup>
                      {errors.password && touched.password && (
                        <div className="input-feedback">{errors.password}</div>
                      )}
                      {this.state.isWrongCreditional && (
                        <div className="input-feedback">
                          Invalid Email or Password
                        </div>
                      )}
                </FormGroup>
                <div className=" custom-control-alternative custom-checkbox">
                  {/* <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label> */}
                  <Button className=" cta " color="primary" type="submit">
                    Enjoy!
                    <span className="cta-icon right">
                                       &#x02192;

                                       </span>
                  </Button>
                  {/* <Link
                className="text-blue float-right"
                // onClick={e => e.preventDefault()}
                to='/auth/forgetpassword'
              >
                Forgot password?
              </Link> */}
                </div>
                {/* <div className="text-left">
                  <Button className="my-4 cta " color="primary" type="submit">
                    Next
                  </Button>
                </div> */}
              </Form>)}}
              </Formik>

              <p className='mt-5 mb-2'> Forgot your Password? Well, let's fix that! <Link to='/auth/forgetpassword' className='text-cta'><> Reset Password</> </Link>
</p>

              <p className='mt-1 mb-0'> New to Owl? <Link to='/auth/register' className='text-cta'><> Get started</> </Link>
</p>

            </CardBody>
            {/* <CardFooter className="bg-transparent pb-5">
              <div className="text-muted text-center mt-2 mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/github.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Github</span>
                </Button>
                <Button
                  className="btn-neutral btn-icon"
                  color="default"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                >
                  <span className="btn-inner--icon">
                    <img
                      alt="..."
                      src={require("assets/img/icons/common/google.svg")}
                    />
                  </span>
                  <span className="btn-inner--text">Google</span>
                </Button>
              </div>
            
            </CardFooter> */}
          </Card>
          {/* <Row className="mt-3">
            <Col xs="6">
          
            </Col>
          
          </Row> */}
        </Col>

        <Col  lg="3" md="3" >
        </Col>
      </>
    );
  }
}

export default Login;

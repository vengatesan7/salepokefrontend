import React from "react";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { PostData } from "../services/PostData";
import { Formik } from "formik";
import * as Yup from "yup";
import Detailskeleton from "../../components/Skeleton/Detailskeleton.jsx";
import Cookies from "js-cookie";
import { toast } from "react-toastify";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: null,
      firstName: undefined,
      lastName: undefined,
      email: undefined,
      address: undefined,
      city: undefined,
      countryCode: undefined,
      mobile: undefined,
      oldPassword: undefined,
      newPassword: undefined,
      confirmNewPassword: undefined,
      state: undefined,
      loading: true
    };
  }
  handleChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
    console.log(this.state.lastName, event.target.value);
  };
  componentDidMount() {
    this.getUserData();
  }
  getUserData = () => {
const userDetail = { user_id :    Cookies.get('_ACid')}
    PostData("ms1", "getuserbyid", userDetail).then(data => {
      this.setState({
        userData: data.userdata,
        loading: false
      });
    });
  };
  updateUser = values => {
    const updatedDate = {
      first_name: values.firstName,
      last_name: values.lastName,
      mobile_number: values.mobile,
      address: values.address,
      city: values.city,
      state: values.state,
      country_code: values.countryCode,
      user_id:Cookies.get("_ACid")
    };
    this.setState({
      loading: true
    });
    console.log(updatedDate);
    PostData("ms1", "user/updateuserdetail", updatedDate).then(status =>
      this.setState({
        loading: false
      })
    );
    this.getUserData();

  };
  updatePassword = () => {
    console.log(this.state.oldPassword, "change-password");
  };
  render() {
    console.log(this.state.lastName);
    const { userData, loading } = this.state;
    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              <Card className="border-0">
                {loading ? (
                  <Detailskeleton />
                ) : (
                  userData !== null && (
                    <React.Fragment>
                      <CardHeader className="border-0">
                        <Row className="align-items-center">
                          <Col xs="8">
                            <h3 className="mb-0">My account</h3>
                          </Col>
                          <Col className="text-right" xs="4">
                            {/* <Button
                        color="primary"
                        href="#pablo"
                        size="sm"
                        type='submit'
                      >
                       Update Profile
                      </Button> */}
                          </Col>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        <React.Fragment>
                          <Formik
                            initialValues={{
                              email: userData.email,
                              firstName: userData.first_name,
                              lastName: userData.last_name,
                              address: userData.address,
                              city: userData.last_name,
                              mobile: userData.mobile_number,
                              countryCode: userData.country_code,
                              state: userData.state
                            }}
                            onSubmit={(values, { setSubmitting }) => {
                              this.updateUser(values);
                            }}
                            validationSchema={Yup.object().shape({
                              email: Yup.string()
                                .email("Please enter a valid email address")
                                .required("Email Required"),
                              firstName: Yup.string().required(
                                "First Name Required"
                              ),
                              mobile: Yup.string()
                                .required("Mobile number Required")
                                // .matches(
                                //   /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
                                //   "Please enter valide Number"
                                // )
                                .min(
                                  8,
                                  "Mobile number is too short min 10 char"
                                )
                            })}
                          >
                            {props => {
                              const {
                                values,
                                touched,
                                errors,
                                // isSubmitting,
                                handleChange,
                                handleBlur,
                                handleSubmit
                              } = props;
                              return (
                                <Form onSubmit={handleSubmit}>
                                  <h6 className="heading-small text-muted mb-4">
                                    User information
                                  </h6>
                                  <div className="">
                                    <Row>
                                      <Col lg="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-first-name"
                                          >
                                            First name
                                          </label>
                                          {/* <Input
                                        // defaultValue={userData.first_name}
                                        // id="input-first-name"
                                        placeholder="First name"
                                        type="text"
                                        // onChange={this.handleChangeInput}
                                        // name="firstName"
                                        value={userData.first_name || values.firstName}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        className={
                                          "form-control-alternative" || "col" ||
                                          (errors.passwordConfirmation &&
                                            touched.passwordConfirmation &&
                                            "error")
                                        }
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="First name"
                                              type="text"
                                              name="firstName"
                                              value={values.firstName}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.firstName &&
                                                  touched.firstName &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.firstName &&
                                            touched.firstName && (
                                              <div className="input-feedback">
                                                {errors.firstName}
                                              </div>
                                            )}
                                        </FormGroup>
                                      </Col>
                                      <Col lg="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-last-name"
                                          >
                                            Last name
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        defaultValue={userData.last_name}
                                        id="input-last-name"
                                        placeholder="Last name"
                                        type="text"
                                        name="lastName"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Last name"
                                              type="text"
                                              name="lastName"
                                              value={values.lastName}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.lastName &&
                                                  touched.lastName &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.lastName &&
                                            touched.lastName && (
                                              <div className="input-feedback">
                                                {errors.lastName}
                                              </div>
                                            )}
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                    <Row>
                                      {/* <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              Username
                            </label>
                            <Input
                              className="form-control-alternative"
                              defaultValue={userData.first_name}
                              id="input-username"
                              placeholder="Username"
                              type="text"
                            />
                          </FormGroup>
                        </Col> */}
                                      <Col lg="12">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-email"
                                          >
                                            Email address
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        id="input-email"
                                        placeholder={userData.email}
                                        type="email"
                                        name="email"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Email"
                                              type="email"
                                              name="email"
                                              value={values.email}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.email &&
                                                  touched.email &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.email && touched.email && (
                                            <div className="input-feedback">
                                              {errors.email}
                                            </div>
                                          )}
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                  </div>
                                  <hr className="my-4" />
                                  {/* Address */}
                                  <h6 className="heading-small text-muted mb-4">
                                    Contact information
                                  </h6>
                                  <div className="">
                                    <Row>
                                      <Col md="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-address"
                                          >
                                            Address
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        defaultValue={userData.address}
                                        id="input-address"
                                        placeholder="Home Address"
                                        type="text"
                                        name="address"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Home Address"
                                              type="text"
                                              name="address"
                                              value={values.address}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.address &&
                                                  touched.address &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.address &&
                                            touched.address && (
                                              <div className="input-feedback">
                                                {errors.address}
                                              </div>
                                            )}
                                        </FormGroup>
                                      </Col>
                                      <Col lg="6">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-country"
                                          >
                                            Mobile
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        id="input-postal-code"
                                        placeholder={userData.mobile_number}
                                        type="number"
                                        name="mobile"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Mobile number"
                                              type="number"
                                              name="mobile"
                                              value={values.mobile}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.mobile &&
                                                  touched.mobile &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.mobile && touched.mobile && (
                                            <div className="input-feedback">
                                              {errors.mobile}
                                            </div>
                                          )}
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col lg="4">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-country"
                                          >
                                            State
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        id="input-postal-code"
                                        placeholder={userData.mobile_number}
                                        type="number"
                                        name="mobile"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="State"
                                              type="text"
                                              name="state"
                                              value={values.state}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.state &&
                                                  touched.state &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.state && touched.state && (
                                            <div className="input-feedback">
                                              {errors.state}
                                            </div>
                                          )}
                                        </FormGroup>
                                      </Col>
                                      <Col lg="4">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-city"
                                          >
                                            City
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        defaultValue={userData.city}
                                        id="input-city"
                                        placeholder="City"
                                        type="text"
                                        name="city"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="City"
                                              type="text"
                                              name="city"
                                              value={values.city}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.city &&
                                                  touched.city &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.city && touched.city && (
                                            <div className="input-feedback">
                                              {errors.city}
                                            </div>
                                          )}
                                        </FormGroup>
                                      </Col>
                                      <Col lg="4">
                                        <FormGroup>
                                          <label
                                            className="form-control-label"
                                            htmlFor="input-country"
                                          >
                                            Country code
                                          </label>
                                          {/* <Input
                                        className="form-control-alternative"
                                        defaultValue={userData.country_code}
                                        id="input-country"
                                        placeholder="Country"
                                        type="text"
                                        name="countryCode"
                                        onChange={this.handleChangeInput}
                                      /> */}
                                          <InputGroup className="input-group-alternative">
                                            <Input
                                              placeholder="Country code"
                                              type="text"
                                              name="countryCode"
                                              value={values.countryCode}
                                              onChange={handleChange}
                                              onBlur={handleBlur}
                                              className={
                                                "col" ||
                                                (errors.city &&
                                                  touched.countryCode &&
                                                  "error")
                                              }
                                              autofill={false}
                                            />
                                          </InputGroup>
                                          {errors.countryCode &&
                                            touched.countryCode && (
                                              <div className="input-feedback">
                                                {errors.countryCode}
                                              </div>
                                            )}
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col lg="4">
                                        <FormGroup>
                                          <Input
                                            className="btn btn-primary cta"
                                            value="Update information"
                                            type="submit"
                                          />
                                        </FormGroup>
                                      </Col>
                                    </Row>
                                  </div>
                                  <hr className="my-4" />
                                  {/* Description */}
                                  {/* <h6 className="heading-small text-muted mb-4">About me</h6> */}
                                  {/* <div className="pl-lg-4">
                      <FormGroup>
                        <label>About Me</label>
                        <Input
                          className="form-control-alternative"
                          placeholder="A few words about you ..."
                          rows="4"
                          defaultValue="A beautiful Dashboard for Bootstrap 4. It is Free and
                          Open Source."
                          type="textarea"
                        />
                      </FormGroup>
                    </div> */}
                                </Form>
                              );
                            }}
                          </Formik>
                          <Updatepassword />
                        </React.Fragment>
                      </CardBody>{" "}
                    </React.Fragment>
                  )
                )}
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Profile;

export const Updatepassword = () => {
  const passwordUpdate = (values) =>{
    PostData("ms1", "user/changepassword", values).then(status =>
      // this.setState({
      //   loading: false
      // })
      toast.info(status.message)
    );
  }
  return (
    <Formik
      enableReinitialize={true}
      initialValues={{
        password: "",
        passwordConfirmation: "",
        oldpassword: ""
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        const Password = {
          oldpassword: values.oldpassword,
          password: values.passwordConfirmation
        };
        passwordUpdate(Password)
        console.log(Password)
        resetForm();
      }}
      validationSchema={Yup.object({
        oldpassword: Yup.string()
          .required("Old Password Required")
          .min(8, "Password is Too short min 8 char"),
        password: Yup.string()
          .required("Password Required")
          .min(8, "Password is Too short min 8 char")
          // .matches(/(?=.*[0-9])/, "Must Contain Number")
          // .matches(/(?=.*[a-z])/, "Must Contain One Lower Case Letter")
          // .matches(/(?=.*[@$!%*?&])/, "Must Contain one special character")
          .notOneOf(
            [Yup.ref("oldpassword"), null],
            "Passwords should not be old one"
          ),
        // .matches(/(?=.*[A-Z])/, "Must Contain  One Uppercase Letter")
        passwordConfirmation: Yup.string()
          .required("Password is required")
          .oneOf([Yup.ref("password"), null], "Passwords must match")
      })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          // isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <Form role="form" onSubmit={handleSubmit} novalidate>
            <h6 className="heading-small text-muted mb-4">Update Password</h6>
            <div className="">
              <Row>
                <Col lg="4">
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-first-name"
                    >
                      Old password
                    </label>
                    <InputGroup className="input-group-alternative">
                      <Input
                        placeholder="Old password"
                        type="password"
                        name="oldpassword"
                        value={values.oldpassword}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.oldpassword && touched.oldpassword && "error")
                        }
                        autofill={false}
                      />
                    </InputGroup>
                    {errors.oldpassword && touched.oldpassword && (
                      <div className="input-feedback">{errors.oldpassword}</div>
                    )}
                  </FormGroup>
                </Col>
                <Col lg="4">
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-first-name"
                    >
                      New password
                    </label>
                    <InputGroup className="input-group-alternative">
                      <Input
                        placeholder="Password"
                        type="password"
                        name="password"
                        //onChange={this.handleChangeInput}
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.password && touched.password && "error")
                        }
                        autofill={false}
                      />
                    </InputGroup>
                    {errors.password && touched.password && (
                      <div className="input-feedback">{errors.password}</div>
                    )}
                  </FormGroup>
                </Col>
                <Col lg="4">
                  <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-last-name"
                    >
                      Confirm new password
                    </label>
                    <InputGroup className="input-group-alternative">
                      <Input
                        placeholder="Confirm password"
                        type="password"
                        name="passwordConfirmation"
                        //onChange={this.handleChangeInput}
                        value={values.passwordConfirmation}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        className={
                          "col" ||
                          (errors.passwordConfirmation &&
                            touched.passwordConfirmation &&
                            "error")
                        }
                        autofill={false}
                      />
                    </InputGroup>

                    {errors.passwordConfirmation &&
                      touched.passwordConfirmation && (
                        <div className="input-feedback">
                          {errors.passwordConfirmation}
                        </div>
                      )}
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col lg="4">
                  <FormGroup>
                    <Input
                      className="btn btn-primary cta"
                      value="Update Password"
                      type="submit"
                    />
                  </FormGroup>
                </Col>
              </Row>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

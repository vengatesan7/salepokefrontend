import React from "react";
import { PostData } from "../services/PostData";
import { Link, Redirect } from "react-router-dom";
import Cookies from "js-cookie";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

// reactstrap components
import {
  Button,
  Card,
  // CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import * as Yup from "yup";
import { Formik } from "formik";
import { Context } from "../../context/index";

class Login extends React.Component {
  static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      userData: [
        {
          id: 1,
          subject: "Thanks for reaching out",
          content:
            "We have received your message and will be getting back to you within 24 hours.If need immediate attention, please call or text. Cool. "
        },
        {
          id: 2,
          subject: "Following Up",
          content:
            "Hey,I'm tied-up at the moment. Please give me 3 times when you are available to chat over the next 2-3 days.Thanks! P.S. For more information check out this site --->"
        },
        {
          id: 3,
          subject: "For you...",
          content:
            "Hi, Thanks for signing up! Here is your coupon: What do you do next? Good question.Thank you!"
        }
      ],

      mailData: {
        subject: "",
        content:""
      },
      isLogin: false,
      isWrongCreditional: false,
      islogedin: false,
      loading: true,
      emailReply: true,
      smsReply: false,
      activeId : 1,

    };
  }
  componentDidMount() {
    
      this.setState({
        loading: false,
        mailData: {
          subject: "Thanks for reaching out",
          content:
            "We have received your message and will be getting back to you within 24 hours.If need immediate attention, please call or text. Cool. "
        },
      });
  
  }
  
  componentDidUpdate() {}
  handleChangeInput = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };
  selectQuickmail = id => {
    this.setState({
      mailData: null
    });
    // console.log(id);
    const selectedQucikMail = this.state.userData.filter(data => {
      return data.id == id;
    });
    const contentIn = selectedQucikMail.map(data => {
      console.log(data.id)
      this.setState({
        activeId : data.id,
        mailData: {
          subject: data.subject,
          content: data.content,
          title: data.title
        }
      });
      return data;
    });

    console.log(contentIn);
  };
  leadfrom = event => {
    console.log(event);
    this.setState({
      // loading: true,
      // contactType: event.target.name,
      active: event.target.name
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    console.log(this.state.mailData.subject, this.state.mailData.content)
    const createCampaign = {
      campaign_name: this.props.location.state.campaign_name,
      integration_name: this.props.location.state.integration_name,
      sms_reply : { subject: this.state.mailData.subject, content: this.state.mailData.content },
      email_response: this.props.location.state.email_response,
      url_string: this.props.location.state.url_string,
      assign_clientuser: Cookies.get('_LassignUserId'),

    };
    console.log(createCampaign);
    PostData("ms2", "campaigncreate", createCampaign).then(result => {
      console.log(result);
      if (result !== "Invalid") {
        this.setState({
          isLogin: true,
          loading: false
        });
      } else {
        this.setState({
          isLogin: true,
          loading: false
        });
      }
    });
  }
  handleChangeContent = (event) => {
    event.preventDefault();
    console.log(event.target.name)
    if(event.target.name === "subject"){
      this.setState({
        mailData: {
          subject: event.target.value,
          content: this.state.mailData.content 
        }
      })
    }else{
      this.setState({
        mailData: {
          content: event.target.value,
          subject: this.state.mailData.subject 
        }
      })
    }

  }
  render() {
    const { userData,activeId } = this.state;
    console.log(this.props.location.state);
    if (this.state.isLogin) {
      return <Redirect to="/admin/campaigns" />;
    }
    return (
      <>
      <Row>
        <Col md="12" className="text-right mt-3 pb-3 cancel">
          <Link to="/admin/campaigns" className='new-project-modal__close'>
            {/* <span className="text-center new-project-modal__close" style={{ display: "inline-block" }}>
              <i class="material-icons">close</i>
            </span> */}
          </Link>
        </Col>
        <Col md='12' className='mb-4 mt-5'>
        <h1 className="">Quick Replies</h1>
          <p className="">
            Quick replies allow you to instantly send a preset email or SMS
            (through the mobile app only) message to your leads. You can press
            the edit button to the right of each quick reply to edit it. The
            defaults for each reply are setup and ready to use if you'd like to
            start using OptinMaster suggested replies instantly.
          </p>
        </Col>
        <Col lg="7" md="5">
         
          <React.Fragment>
            <div className="select-lead col-md-12 mb-10 pl-0 mt-10 pt-3">
              <ul>
                <li>
                  <button
                    onClick={this.leadfrom}
                    name="landingpage"
                    className="active"
                  >
                    SMS Quick Reply
                  </button>
                </li>
              </ul>
            </div>
          </React.Fragment>
          {userData &&
            userData.map(data => {
              return (
                <React.Fragment>
                  <div
                    className={data.id === activeId ? "active qucikreplay" :"qucikreplay"}
                    onClick={() => this.selectQuickmail(data.id)}
                  >
                    <i class="material-icons">
check_circle
</i>
                    <h4>{data.subject}</h4>
                    <p>{data.content}</p>
                  </div>
                </React.Fragment>
              );
            })}
        </Col>

        <Col lg="5" md="7">
          {this.state.loading && (
            <div className="signupLoadingDiv">
              <SkeletonTheme color="#f7fafc" highlightColor="#5e72e4">
                <Skeleton height={7} />
              </SkeletonTheme>
            </div>
          )}
           {this.state.mailData !== null && (
                <React.Fragment>
                  <div className="text-left text-muted mb-4 mt-3 pt-3">
                    <h3>SMS Quick Reply</h3>
                    {/* <small className="text-red">Edit before continue</small> */}
                  </div>
                  
                        <Form onSubmit={this.handleSubmit} novalidate>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-tag " />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Subject"
                                type="text"
                                name="subject"
                                onChange={this.handleChangeContent}
                                defaultValue={
                                  this.state.mailData.subject
                                }
                                // value=''
                                // onChange={handleChange}
                                // onBlur={handleBlur}
                                className={ "col"}
                              />
                            </InputGroup>
                         
                          </FormGroup>
                          <FormGroup>
                            <InputGroup className="input-group-alternative">
                              {/* <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="ni ni-send" />
                            </InputGroupText>
                          </InputGroupAddon> */}
                              <Input
                                placeholder="Content"
                                type="textarea"
                                name="content"
                                style={{ height: "130px" }}
                                //onChange={this.handleChangeInput}
                                defaultValue={this.state.mailData.content}
                                value={this.state.mailData.content}
                                onChange={this.handleChangeContent}
                                // onBlur={handleBlur}
                                className={
                                  "col" 
                                }
                           
                              />
                            </InputGroup>
                          
                          </FormGroup>

                          <div className="text-center">
                            <Button
                              className="my-4 cta cta-fullwidth"
                              color="primary"
                              type="submit"
                            >
                              Continue
                            </Button>
                          </div>
                        </Form>
                   
                </React.Fragment>
            )}
        </Col>
        {/* <SmsQuickReply/> */}
        </Row>
      </>
    );
  }
}

export default Login;

// class SmsQuickReply extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       smsData: [
//         {
//           id: 1,
//           subject: "Thanks for reaching out",
//           content:
//             "We have received your message and will be getting back to you within 24 hours.If need immediate attention, please call or text. Cool. "
//         },
//         {
//           id: 2,
//           subject: "Following Up",
//           content:
//             "Hey,I'm tied-up at the moment. Please give me 3 times when you are available to chat over the next 2-3 days.Thanks! P.S. For more information check out this site --->"
//         },
//         {
//           id: 3,
//           subject: "For you...",
//           content:
//             "Hi, Thanks for signing up! Here is your coupon: What do you do next? Good question.Thank you!"
//         }
//       ],
//       smsDataCollection: {
//         subject: "",
//         content:
//           ""
//       },
//     };
//   }
//   componentDidMount(){
//     if(this.props.location.state.campaignId){
//       this.getExsitingMail();
//     }else{
//       this.setState({
//         smsDataCollection: {
//           subject: "Thanks for reaching out",
//           content:
//             "We have received your message and will be getting back to you within 24 hours.If need immediate attention, please call or text. Cool. "
//         }
//       })
//     }
  
//   }
//   getExsitingMail = ()=>{
// console.log("hai")
//   }
//   render() {
//     console.log(this.props.location.state.campaignId)
//     const { smsData } = this.state;
//     return (
//       <React.Fragment>
//           <Col lg="7" md="5">
//         <div className="select-lead col-md-12 mb-10 pl-0 mt-10 pt-3">
//           <ul>
//             <li>
//               <button
//                 onClick={this.leadfrom}
//                 name="stickypage"
//                 className="active"
//               >
//                 Sms Quick Replay
//               </button>
//             </li>
//           </ul>
//         </div>
//         {smsData &&
//           smsData.map(data => {
//             return (
//               <React.Fragment>
//                 <div
//                   className="qucikreplay"
//                   onClick={() => this.selectQuickmail(data.id)}
//                 >
//                   <h4>{data.subject}</h4>
//                   <p>{data.content}</p>
//                 </div>
//               </React.Fragment>
//             );
//           })}</Col>
//             <Col lg="5" md="5">
//           <Card className="bg-secondary shadow border-0">
           
//             {this.state.smsData !== null && (
//               <CardBody className="px-lg-5 py-lg-5">
//                 <React.Fragment>
//                   <div className="text-left text-muted mb-4">
//                     <h3>SMS Quick Reply</h3>
//                   </div>
//                   <Formik
//                     initialValues={{ subject: "", content: "" }}
//                     onSubmit={(values, { setSubmitting }) => {
//                       console.log(values);
//                       this.setState({
//                         loading: true
//                       });
//                       this.createNewAccount(values);
//                     }}
//                     validationSchema={Yup.object().shape({
//                       subject: Yup.string().required("Edit subject"),
//                       content: Yup.string().required("Edit content")
//                     })}
//                   >
//                     {props => {
//                       const {
//                         values,
//                         touched,
//                         errors,
//                         // isSubmitting,
//                         handleChange,
//                         handleBlur,
//                         handleSubmit
//                       } = props;
//                       return (
//                         <Form role="form" onSubmit={handleSubmit} novalidate>
//                           <FormGroup className="mb-3">
//                             <InputGroup className="input-group-alternative">
//                               <InputGroupAddon addonType="prepend">
//                                 <InputGroupText>
//                                   <i className="ni ni-tag " />
//                                 </InputGroupText>
//                               </InputGroupAddon>
//                               <Input
//                                 placeholder="Subject"
//                                 type="text"
//                                 name="subject"
//                                 // onChange={this.handleChangeInput}
//                                 value={
//                                   values.subject || this.state.smsDataCollection.subject
//                                 }
//                                 onChange={handleChange}
//                                 onBlur={handleBlur}
//                                 className={
//                                   "col" ||
//                                   (errors.subject && touched.subject && "error")
//                                 }
//                               />
//                             </InputGroup>
//                             {errors.subject && touched.subject && (
//                               <div className="input-feedback">
//                                 {errors.subject}
//                               </div>
//                             )}
//                           </FormGroup>
//                           <FormGroup>
//                             <InputGroup className="input-group-alternative">
//                               {/* <InputGroupAddon addonType="prepend">
//                             <InputGroupText>
//                               <i className="ni ni-send" />
//                             </InputGroupText>
//                           </InputGroupAddon> */}
//                               <Input
//                                 placeholder="Content"
//                                 type="textarea"
//                                 name="content"
//                                 style={{ height: "130px" }}
//                                 //onChange={this.handleChangeInput}
//                                 defaultvalue={
//                                   values.content || this.state.smsDataCollection.content
//                                 }
//                                 onChange={handleChange}
//                                 onBlur={handleBlur}
//                                 className={
//                                   "col" ||
//                                   (errors.content && touched.content && "error")
//                                 }
//                                 autofill={false}
//                               />
//                             </InputGroup>
//                             {errors.content && touched.content && (
//                               <div className="input-feedback">
//                                 {errors.content}
//                               </div>
//                             )}
//                             {/* {this.state.isWrongCreditional && (
//                           <div className="input-feedback">
//                             Invalid Email or Password
//                           </div>
//                         )} */}
//                           </FormGroup>

//                           <div className="text-center">
//                             <Button
//                               className="my-4 cta cta-fullwidth"
//                               color="primary"
//                               type="submit"
//                             >
//                               Continue
//                             </Button>
//                           </div>
//                         </Form>
//                       );
//                     }}
//                   </Formik>
//                 </React.Fragment>
//               </CardBody>
//             )}
//           </Card>
//           </Col>
//       </React.Fragment>
//     );
//   }
// }

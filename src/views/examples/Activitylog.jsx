import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Button,
  Row,
  Col,
  Table,

  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
// core components
import UserHeader from "../../components/Headers/UserHeader.jsx";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import { Formik } from "formik";
import * as Yup from "yup";
import { Link, Redirect } from "react-router-dom";
import Switch from "react-switch";
import Detailskeleton from "../../components/Skeleton/Detailskeleton.jsx";

class Activitylog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      campaignData: null,
      firstName: undefined,
      lastName: undefined,
      email: undefined,
      address: undefined,
      city: undefined,
      countryCode: undefined,
      mobile: undefined,
      oldPassword: undefined,
      newPassword: undefined,
      confirmNewPassword: undefined,
      state: undefined,
      addNew: false,
      checked: false ,
      loading : true,
    };
    this.handleChange = this.handleChange.bind(this);

  }
  handleChange(checked, event) {
    console.log(event)
    if(checked === true){
      const status = {
        lead_contact_id: this.props.location.state.leadId,
        status_name:"close"
      };
      console.log(status);
      PostData("ms2", "leadestatusupdate", status).then(data => {
        console.log(data.leadestatus);     
      });
    }else{
      const status = {
        lead_contact_id: this.props.location.state.leadId,
        status_name:"open"
      };
      console.log(status);
      PostData("ms2", "leadestatusupdate", status).then(data => {
        console.log(data.leadestatus);     
      });
    }
    this.setState({ checked });
  }
  componentDidMount() {
    this.getClientData();
  }
  getClientData = () => {
    const id = {
      lead_contact_id: this.props.location.state.leadId
    };
    console.log(id);
    PostData("ms2", "getleadsactivitybyid", id).then(data => {
      console.log(data.leadactivity);
      // const campaignData = data.getcampaign.map(element => {
      //   return element;
      // });
      // console.log(campaignData);

      this.setState({
        campaignData: data.leadactivity,
        loading : false,
      });
    });
  };
 
  addIntegration = (id) =>{
    // console.log(event.target.name)
    this.setState({
      addNewIntegration : true,
      campaignId:id
    })
  }
  manageQuickReply = (id) =>{
    console.log(id)
    this.setState({
      manageQuickReply : true,
      campaignId:id
    })
  }
  back = () =>{
    console.log("back")
    this.setState({
      back : true,
    })
  }
  render() {
    const { campaignData,  loading , addNewIntegration,manageQuickReply, campaignId ,back} = this.state;
console.log(this.state.checked)
    if(addNewIntegration && campaignId !== null ){
      return <Redirect 
      to={{
        pathname: "/plain/add-new-integration",
        state: {
          campaignId: this.state.campaignId,
        }
      }}
      />
    }
    if(manageQuickReply && campaignId !== null ){
      return <Redirect 
      to={{
        pathname: "/plain/update-quick-email",
        state: {
          campaignId: this.state.campaignId,
        }
      }}
      />
    }
    if(back){
      return <Redirect 
      to={{
        pathname: "/admin/leads",
      }}
      />
    }


    return (
      <>
        <UserHeader />
        {/* Page content */}
        <Container className="pb-8 mt--7" fluid>
          <Row>
            <Col className="order-xl-1" xl="12">
              {/* <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                        size="sm"
                      >
                        Add New Client{" "}
                      </Button>
                    </Col>
                  </Row>
                </CardHeader> */}
              {/* <CardBody> */}
              
                <Card className="shadow">
              {loading ?    <Detailskeleton /> :
                  campaignData !== null && (  <React.Fragment>
                  <CardHeader className="border-0">
                    <Row className="align-items-center">
                      <div className="col">
                        <h3 className="mb-0"> Leads Detail</h3>
                      </div>
                      <div className="col text-right">
                      
<React.Fragment>
                      <span className="table-buttons">
                      <label htmlFor="material-switch">
                      {/* <span className="pr-3">Status<small className='pl-3'>data.status_name</small></span> */}
  {/* <Switch
    checked={this.state.checked}
    onChange={this.handleChange}
    onColor="#000000b3"
    onHandleColor="#5e72e4"
    handleDiameter={18}
    uncheckedIcon={false}
    checkedIcon={false}
    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
    height={10}
    width={30}
    className="react-switch"
    id="material-switch"
  /> */}
</label>
                          
                          {/* <button
                            onClick={() =>
                              this.manageQuickReply("data.campaign_id")
                            }
                            name={"data.campaign_id"}
                          >
                            {" "}
                            <i class="material-icons">reply</i>
                          </button> */}
                        <small> <button
                            onClick={this.back}
                            name={"data.campaign_id"}
                          >
                            <i class="material-icons" style={{    position: "relative",
    top: "6px"}}>
arrow_back_ios
</i>Back
                          </button>
 </small>  
                        </span>
                        </React.Fragment> 
                      {/* )} */}
                      </div>
                    </Row>
                  </CardHeader>
                  <CardBody>
                  <ul class="timeline">
                    {campaignData.map(data => {
                      return (
                        <React.Fragment>
             



	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">{data.status_name}</span>
				<span class="time-wrapper"><span class="time">{data.response_datetime}</span></span>
			</div>
			<div class="desc">{data.name}<small>{data.name.rolename}</small></div>
		</div>
	</li>
                          {/* <h6 className="heading-small text-muted mb-2">
                            Response date time
                          </h6>

                          <p > {data.response_datetime}</p>
                          <h6 className="heading-small text-muted mb-2">
                            Status Name
                          </h6>

                          <p> {data.status_name}</p> */}
                          {/* <h6 className="heading-small text-muted mb-2">
Email                          </h6>

                          <p> {data.email}</p>
                          <h6 className="heading-small text-muted mb-2">
                            Phone Number
                          </h6>

                          <p> {data.phone_number}</p>
                          <h6 className="heading-small text-muted mb-2">
                            Page Name
                          </h6>

                      <p> {data.page_name}<br /><small>URL : {data.page_url}</small></p>
                       
                          <h6 className="heading-small text-muted mb-2">
                            Page Submitted
                          </h6>

                          <p> {data.date_submitted}</p> */}
                          
                         </React.Fragment>
                      );
                    })}
                      </ul>

                  </CardBody>
                  </React.Fragment>)}
                </Card>
              
              {/* </CardBody> */}
              {/* </Card> */}
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default Activitylog;

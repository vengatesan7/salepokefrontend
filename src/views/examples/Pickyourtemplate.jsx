import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  // InputGroupAddon,
  // InputGroupText,
  InputGroup
} from "reactstrap";
import cookie from 'react-cookies'
import { Link, Redirect } from "react-router-dom";
import Themes from "./Themes";
import { PostData } from "../services/PostData";
// import Themslist from "../Components/Themslist";
// import { Context } from "../../../../Context";
// import { toast } from "react-toastify";
import { ms1ImageURL } from "../services/Appurl";
import Imageplaceholder from "../../assets/img/theme/image-placeholder.png";
import { toast } from "react-toastify";

class pickyourtemplate extends React.Component {
  // static contextType = Context;
  constructor(props) {
    super(props);
    this.state = {
      themsList: [],
      loading: true,
      isThemeSelection: false,
      notification: false,
      rcHighlight : true,
      savedHighlight: null,
    };
  }
  getThemeList = () => {
    this.setState({ loading: true });
    if (this.props.location.state.activeId) {
      const selectList = {
        pop_up_type: this.props.location.state.activeId
      };
      PostData("ms1", "getpopupmaster", selectList).then(list => {
        list !== "Invalid"
          ? this.setState({ themsList: list.popupmaster, loading: false,userTemplate : false ,rcHighlight : true,   savedHighlight: false,         })
          : this.setState({ themsList: [], loading: false });
      });
    } 
    else{
      this.setState({ themsList: [], loading: false });
    }
  };
  
  componentDidMount() {
    console.log("yes");    this.setState({ loading: false });

    this.getThemeList();
  }
  // filterClick = event => {
  //   this.setState({ loading: true });
  //   const funnelCategory = {
  //     page_tmpl_category_id: event.target.name
  //   };
  //   PostData("ms3", "pagetemplatelistbycategory", funnelCategory).then(list => {
  //     list != "Invalid"
  //       ? this.setState({ themsList: list.data, loading: false })
  //       : this.setState({ themsList: [], loading: false });
  //   });
  // };
  themesClick = (id) => {
    if (this.props.location.state.activeId) {
      const createpopupCampaign = {
        campaign_name: this.props.location.state.campaign_name,
        pop_up_template_id: id,
        pop_up_type:this.props.location.state.activeId,
        addsite_urls: this.props.location.state.website
      };

      PostData("ms1", "createpopupcampaign", createpopupCampaign).then(result => {
        if (result !== "Invalid") {
          this.setState({ isThemeSelection: true });
          console.log(result)
          cookie.save('popUpId', result.campaign.pop_up_id, { path: '/' })
          cookie.save('comingFrom', "CREATENEW", { path: '/' })

          toast.info(result.message);
        } else {
          this.setState({ isThemeSelection: false });
          toast.warn("Failed . Please try again");
          
        }
      });
    }  
  };

  render() {
    console.log(this.props.location,"else")
    if (this.state.isThemeSelection) {
      return <Redirect to="/editor/builder" />;
    }
    return (
      <React.Fragment>
     
          <Col md="12" className="text-right mt-3 pb-3 cancel">
              <Link to="/admin/campaigns" className="new-project-modal__close">
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  {/* <i class="material-icons">close</i> */}
                </span>
              </Link>
            </Col>
            <Col lg="12" xs="12" className=" add-new-campaign mt-5">              <h3 className="text-left mb-40">
                Pick Your Template <span className="or">or</span>
                <span
                  style={{ cursor: "pointer", color: "#", marginLeft:'5px' }}
                  onClick={() => this.themesClick(27, "Build from scratch")}
                >
                  {/* <Link to='/funnels/overview' className='template-scratch'>Build from Scratch </Link> */}
                  Build from Scratch
                </span>
              </h3>
              </Col>
            <div className="col-12 text-center mb-30">
              {/* <div className='template-choosing'>

              <button onClick={this.getThemeList} className={this.state.rcHighlight ? 'active mr-30' : 'mr-30'}>Rc Templates</button>
              <button onClick={this.getSavedThemeList} className={this.state.savedHighlight ? 'active' : ''}>Saved Templates</button> 
              </div>
                     */}
            </div>
            <Col>
            <div className="mb-40">
              <div className="themes">
                <div className="themes-inner-wrap text-center">
                  {this.state.loading ? (
                    <Themes loading />
                  ) : this.state.themsList.length > 0 ? (
                    <Row>
                      {this.state.themsList.map(result => (
                        
                        <Themes
                          title={result.pop_up_name}
                          src={result.pop_up_img_prvw_url ? `${ms1ImageURL}${result.pop_up_img_prvw_url}` : Imageplaceholder }
                          previewImage={result.page_tmpl_prvw_img_url ? `${ms1ImageURL}${result.page_tmpl_prvw_img_url}` : `${ms1ImageURL}${result.tmpl_prvw_img_url}`}
                          themesClick={() =>
                            this.themesClick(
                              result.pop_up_template_id
                            )
                          }
                          name={result.pop_up_template_id ? result.pop_up_template_id : result.pop_up_template_id}
                          label={"Select"}
                          value={result.pop_up_template_id ? result.pop_up_template_id : result.pop_up_template_id}
                        />
                      ))}
                    </Row>
                  ) : (
                    <div className="text-left">
                      <p>No themes found</p>{" "}
                    </div>
                  )}
                </div>
              </div>
            </div>
              </Col>
          
        {/* <div className="sideright">
          {this.state.loading ? (
            <Themslist loading 
            typeId={this.props.location.state.typeId}
            funnelCardId={
              this.props.location.state.funnel_card_id
            } />
          ) : (
            <Themslist
              getThemeList={this.getThemeList}
              filterClick={this.filterClick}
              typeId={this.props.location.state.typeId}
              funnelCardId={
                this.props.location.state.funnel_card_id
              }
            />
          )}
        </div> */}
      </React.Fragment>
    );
  }
}
export default pickyourtemplate;

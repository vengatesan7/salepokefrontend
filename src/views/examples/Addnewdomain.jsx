import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Selectbox from "./Seleactbox";

import Cookies from "js-cookie";
import Alltriggers from "./Trigger";

export default class Addnewdomain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      domainAdd: false,
    };
  }
  componentDidMount() {
    this.setState({
      loading: false,
    });
  }

  inviteTeam = (values) => {
    // event.preventDefault();
    const Domainname = {
      domain_name: values.website,
    };
    PostData("ms1", "adddomain", Domainname).then((data) => {
      console.log(data);
      this.setState({
        domainAdd: true,
      });
    });
  };

  render() {
    const { domainAdd, loading } = this.state;
    if (this.state.domainAdd) {
      if (Cookies.get("_ACrole") == "1") {
        return (
          <Redirect
            to={{
              pathname: "/plain/addnewchannel",
              // state: {
              //   campaign_name: this.state.campaignName,
              //   activeId:this.state.activeId,
              //   website: this.state.website
              // }
            }}
          />
        );
      } else {
        return (
          <Redirect
            to={{
              pathname: "/admin/index",
              // state: {
              //   campaign_name: this.state.campaignName,
              //   activeId:this.state.activeId,
              //   website: this.state.website
              // }
            }}
          />
        );
      }
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            <Col md="12" className="text-right mt-3 pb-3 cancel">
              <Link to="/admin/campaigns" className="new-project-modal__close">
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  {/* <i class="material-icons">close</i> */}
                </span>
              </Link>
            </Col>

            <Col lg="12" xs="12" className="pl-5 add-new-campaign mt-5">
              <Formik
                initialValues={{
                  website: "",
                }}
                onSubmit={(values, { setSubmitting }) => {
                  this.setState({
                    loading: true,
                  });
                  this.inviteTeam(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  website: Yup.string().required("Website url Required"),
                })}
              >
                {(props) => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs-12>
                          <h1>Welcome Enter Your Website</h1>
                          <h3 className="mb-4 mt-0">
                            Which website(s) do you want to load all campaigns?
                          </h3>

                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-world" />
                                </InputGroupText>
                              </InputGroupAddon>

                              <Input
                                placeholder="Type your site url"
                                type="text"
                                name="website"
                                // onChange={this.handleChangeInput}
                                value={values.website}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.website && touched.website && "error")
                                }
                              />
                            </InputGroup>
                            {errors.website && touched.website && (
                              <div className="input-feedback">
                                {errors.website}
                              </div>
                            )}
                          </FormGroup>
                        </Col>
                      </Row>
                      <div className="text-left mt-4">
                        <Button
                          className="my-2 cta "
                          color="primary"
                          type="submit"
                        >
                          Continue
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}


// import React, { useState, Fragment } from "react";
// import { Link, Redirect } from "react-router-dom";
// import {
//   Button,
//   Card,
//   CardHeader,
//   CardBody,
//   FormGroup,
//   Form,
//   Input,
//   InputGroupAddon,
//   InputGroupText,
//   InputGroup,
//   Row,
//   Col,
// } from "reactstrap";
// // import "bootstrap/dist/css/bootstrap.css";

// const Addnewdomain = () => {
//   const [inputFields, setInputFields] = useState([
//     { firstName: '' }
//   ]);

//   const handleAddFields = () => {
//     const values = [...inputFields];
//     values.push({ firstName: '' });
//     setInputFields(values);
//   };

//   const handleRemoveFields = index => {
//     const values = [...inputFields];
//     values.splice(index, 1);
//     setInputFields(values);
//   };

//   const handleInputChange = (index, event) => {
//     const values = [...inputFields];
//     if (event.target.name === "firstName") {
//       values[index].firstName = event.target.value;
//     } else {
//       values[index].lastName = event.target.value;
//     }

//     setInputFields(values);
//   };

//   const handleSubmit = e => {
//     e.preventDefault();
//     console.log("inputFields", inputFields.length !== 1);
//   };

//   return (
//     <>
//     <React.Fragment>
//              <Col md="12" className="text-right mt-3 pb-3 cancel">
//                <Link to="/admin/campaigns" className="new-project-modal__close">
//                  <span
//                    className="text-center"
//                    style={{ display: "inline-block" }}
//                  >
//                    {/* <i class="material-icons">close</i> */}
//                  </span>
//                </Link>
//              </Col>

//              <Col lg="12" xs="12" className="pl-5 add-new-campaign mt-5">
//              <h1>Welcome Enter Your Website</h1>
//                            <h3 className="mb-4 mt-0">
//                              Which website(s) do you want to load all campaigns?
//                            </h3>

//       <form onSubmit={handleSubmit}>
//         <div className="form-row">
//           {inputFields.map((inputField, index) => (
//             <Fragment key={`${inputField}~${index}`}>
//               <div className="form-group col-sm-10">
//                 <label htmlFor="firstName">Domain Name</label>
//                 <input
//                   type="text"
//                   className="form-control"
//                   id="firstName"
//                   name="firstName"
//                   value={inputField.firstName}
//                   placeholder=""
//                   onChange={event => handleInputChange(index, event)}
//                 />
//               </div>
//               <div className="form-group col-sm-2">
//                {inputFields.length !== 1 && <button
//                   className="btn btn-link"
//                   type="button"
//                   onClick={() => handleRemoveFields(index)}
//                 >
//                   -
//                 </button>}
//                 <button
//                   className="btn btn-link"
//                   type="button"
//                   onClick={() => handleAddFields()}
//                 >
//                   +
//                 </button>
//               </div>
//             </Fragment>
//           ))}
//         </div>
//         <div className="submit-button">
//           <button
//             className="btn btn-primary mr-2"
//             type="submit"
//             onSubmit={handleSubmit}
//           >
//             Save
//           </button>
//         </div>
//         <br/>
//         <pre>
//           {JSON.stringify(inputFields, null, 2)}
//         </pre>
//       </form>
// </Col>
// </React.Fragment>
     
//     </>
//   );
// };

// export default Addnewdomain
import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Selectbox from './Seleactbox'

import Cookies from "js-cookie";
import Alltriggers from "./Trigger";

export default class Inviteteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      roleType: null,
      createCamapign: false,
      hook: null,
      campaignName: null,
      string: null,
      mailList:[
      {campaign_type_name:"Regular", campaign_type_id: 1}, 
      {campaign_type_name:"Full Page", campaign_type_id: 2}],
      activeId : 1,
      domainList : [],
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
    this.getDomains();

  }

  getDomains = () =>{
    PostData('ms1','getuserdomainlist').then(result =>{
      if(result.status === "success")
      {
        this.setState({
          domainList: result.data
        })
      }
      else{
        this.setState({
          domainList: []
        })
      }
      })
  }

 
  inviteTeam = values => {
    // event.preventDefault();
    this.setState({
      campaignName: values.campignName,
      website: values.website,
      createCamapign: true
    });

    // });
  };

  // integrationChange = integration => {
  //   const baseUrl = "https://micro2.wwmarketing.co/webhookapi/";
  //   const stringGen = Math.random()
  //     .toString(36)
  //     .slice(2);
  //   switch (integration) {
  //     case "unbounce":
  //       return this.setState({
  //         hook: `${baseUrl}unbounce/${stringGen}`,
  //         string: stringGen
  //       });
  //     case "clickfunnels":
  //       return this.setState({
  //         hook: `${baseUrl}clickfunnels/${stringGen}`,
  //         string: stringGen
  //       });
  //     case "instapage":
  //       return this.setState({
  //         hook: `${baseUrl}instapage/${stringGen}`,
  //         string: stringGen
  //       });
  //   }
  // };
  // copyCodeToClipboard = () => {
  //   const el = this.textArea;
  //   el.select();
  //   document.execCommand("copy");
  // };
  selectQuickmail = id => {
    this.setState({
      mailData: null
    });
    // console.log(id);
    const selectedQucikMail = this.state.mailList.filter(data => {
      return data.campaign_type_id == id;
    });
    const contentIn = selectedQucikMail.map(data => {
      this.setState({
        activeId:data.campaign_type_id,
      });
      return data;
    });

    console.log(contentIn);
  };
  render() {
    const { roleType,activeId,mailList, loading } = this.state;
    if (this.state.createCamapign) {
      return (
        <Redirect
          to={{
            pathname: "/plain/choose-template",
            state: {
              campaign_name: this.state.campaignName,
              activeId:this.state.activeId,
              website: this.state.website
            }
          }}
        />
      );
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            <Col md="12" className="text-right mt-3 pb-3 cancel">
              <Link to="/admin/campaigns" className="new-project-modal__close">
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  {/* <i class="material-icons">close</i> */}
                </span>
              </Link>
            </Col>

            <Col lg="12" xs="12" className="pl-5 add-new-campaign mt-5">
              <h3 className="mb-3">What do you want to call your popup campaign?</h3>
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  campignName: "",
                  website: ""
                }}
                onSubmit={(values, { setSubmitting }) => {
                  console.log(values);
                  this.setState({
                    loading: true
                  });
                  this.inviteTeam(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  campignName: Yup.string().required("Campign name Required"),
                  website: Yup.string().required("Website url Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs-12>
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-send" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Campaign name"
                                type="text"
                                name="campignName"
                                // onChange={this.handleChangeInput}
                                value={values.campignName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.campignName &&
                                    touched.campignName &&
                                    "error")
                                }
                              />
                            </InputGroup>
                            {errors.campignName && touched.campignName && (
                              <div className="input-feedback">
                                {errors.campignName}
                              </div>
                            )}
                          </FormGroup>
                         
                        </Col>

                      </Row>
                      {/* <Row>
                        <Col xs-12>
                          <h3 className="mb-3 mt-4">
                          Select Your Campaign Type
                          </h3>
                        </Col>
                      </Row> */}
                      {/* <Row>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="unbounce"
                                type="radio"
                                name="integration"
                                value="unbounce"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("unbounce")
                                }
                                onBlur={handleBlur}
                                className={
                                  "custom-control-input" ||
                                  (errors.unbounce &&
                                    touched.unbounce &&
                                    "error")
                                }
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="unbounce"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("assets/img/theme/unbounce-icon-dark.svg")}
                                />{" "}
                              </label>
                              {errors.unbounce && touched.unbounce && (
                                <div className="input-feedback">
                                  {errors.unbounce}
                                </div>
                              )}
                            </div>
                          </FormGroup>
                        </Col>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="clickfunnels"
                                type="radio"
                                name="integration"
                                value="clickfunnels"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("clickfunnels")
                                }
                                onBlur={handleBlur}
                                className={
                                  "custom-control-input" ||
                                  (errors.clickfunnels &&
                                    touched.clickfunnels &&
                                    "error")
                                }
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="clickfunnels"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("assets/img/theme/clickfunnels.png")}
                                />
                              </label>
                            </div>
                          </FormGroup>
                        </Col>
                        <Col xs-2>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative ">
                              <input
                                // className="custom-control-input"
                                id="instapage"
                                type="radio"
                                name="integration"
                                value="instapage"
                                onChange={handleChange}
                                onClick={e =>
                                  this.integrationChange("instapage")
                                }
                                onBlur={handleBlur}
                                className={"custom-control-input"}
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="instapage"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("assets/img/theme/insta-page.png")}
                                />
                              </label>
                            </div>
                          </FormGroup>
                        </Col>
                      </Row>
                      {errors.integration && touched.integration && (
                        <div className="input-feedback">
                          {errors.integration}
                        </div>
                      )} */}
                      {/* {this.state.hook && (
                        <Row className="hook mt-4">
                          <Col>
                            <input
                              ref={textarea => (this.textArea = textarea)}
                              value={this.state.hook}
                              className="col-12"
                              style={{ height: "50px" }}
                            />
                            <button
                              onClick={() => this.copyCodeToClipboard()}
                              type="button"
                              style={{
                                zIndex: "999",
                                position: "relative",
                                marginTop: "-36px",
                                height: "50",
                                lineHeight: "26px",
                                cursor: "pointer",
                                fontSize: "16px",
                                background: "none",
                                border: "none"
                              }}
                              className="float-right"
                            >
                              <i
                                class="material-icons text-blue"
                                style={{ fontSize: "20px" }}
                              >
                                file_copy
                              </i>{" "}
                            </button>
                          </Col>
                        </Row>
                      )} */}
                     { mailList &&  <Row>
                        <Col className='mt-4'>
                        <React.Fragment> <h3 className="mb-4 mt-4">
                          Select Your Campaign Type
                          </h3>
                          <FormGroup className="mb-3 row">
                         
                              {mailList.map(data =>{
                                return (
                                  <React.Fragment>
                                    <Col>
                                    <div
                                      className={data.campaign_type_id === activeId ? "active qucikreplay text-center" :"qucikreplay text-center"}
                                      onClick={() => this.selectQuickmail(data.campaign_type_id)}
                                    >
                                      <i class="material-icons">
                  check_circle
                  </i>
                  <img alt="..." src={require("../../assets/img/theme/pop-up.png")}  className="mb-3 mt-2"/>

                                      <h4 className='mb-3'>{data.campaign_type_name}</h4>
                                    </div>
                                    </Col>
                                    
                                  </React.Fragment>
                                );
                                
                              })}
                          
                       
                        </FormGroup></React.Fragment>
                        </Col>
                      </Row>}

                      <Row>
                        <Col xs-12>
                        <h3 className="mb-4 mt-4">
                        Which website(s) do you want to load this campaign on?                          </h3>
                        
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-world" />
                                </InputGroupText>
                              </InputGroupAddon>

                              {/* <Input
                                placeholder="Type your site url"
                                type="select"
                                name="website"
                                // onChange={this.handleChangeInput}
                                value={values.website}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.website &&
                                    touched.website &&
                                    "error")
                                }
                              /> */}

<Input 
type="select"                                 
name="website"
placeholder="Type your site url"
id="exampleSelect"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "col" ||
                                  (errors.website &&
                                    touched.website &&
                                    "error")
                                }
><option>Select domain</option>
         {this.state.domainList && this.state.domainList.map((data)=>{
           return <option value={data.domains_id}>{data.domain_name}</option>
         })} 
        </Input>


                            </InputGroup>
                            {errors.website && touched.website && (
                              <div className="input-feedback">
                                {errors.website}
                              </div>
                            )}
                          </FormGroup>
                                                 {/* <Selectbox /> */}

                        </Col>
                      </Row>
                      <div className="text-right mt-4">
                        <Button
                          className="my-2 cta "
                          color="primary"
                          type="submit"
                        >
                          Continue
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import * as Yup from "yup";
import { Formik } from "formik";
import { GetData } from "../services/GetData";
import { PostData } from "../services/PostData";
import Cookies from "js-cookie";

export default class Inviteteam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      roleType: null,
      isLogin: false
    };
  }
  componentDidMount() {
    this.setState({
      loading: false
    });
  }

  inviteTeam = values => {
    // event.preventDefault();
    if (values.unbounce) {
      const urlString = Math.random().toString(36).slice(2);
      console.log(urlString)
      this.setState({
        integrationName: "unbounce",
        isLogin: true,
        loading: false,
        url_string : urlString

      });
    }else{
      this.setState({
        integrationName: "unbounce",
        isLogin: false,
        loading: false


      });
    }

    // PostData("ms1", "user/inviteteammembers", Teammember ).then(data => {
    // if( data !== 'Invalid' ){
    //   toast(data.message);

    // }else{
    //   toast(data.message);
    //   this.setState({
    //    isLogin:false,
    //    loading: false
    //   });
    // }

    // });
  };

  render() {
    const { roleType, loading } = this.state;
    if (this.state.isLogin && Cookies.get("_token")) {
      return (
        <Redirect
          to={{
            pathname: "/plain/quickemail",
            state: {
              campaign_name: this.props.location.state.campaign_name,
              integration_name: this.state.integrationName,
              url_string: this.state.url_string
            }
          }}
        />
      );
    }
    return (
      <React.Fragment>
        {this.state.loading ? (
          <p>Loading</p>
        ) : (
          <React.Fragment>
            {/* <Col  lg="5" md="5" className=''>
        
    <img
                              alt="..."
                              src={require("assets/img/theme/invite-team.svg")}
                              className='img-fluid'
                            />
      
      </Col> */}
        <Col  md="12" className='text-right mt-3 pb-3 cancel'>
            <Link to="/admin/campaigns">   
            <span className='text-center' style={{    display: "inline-block"}}>   <i className="ni ni-button-power" /><br />
            <small>cancel </small> </span>  
</Link>
            </Col>
            <Col lg="12" xs="12" className="select-integration">
              <h1 className="mb-3">Select Your Integration </h1>
              {/* <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5"> */}
              <Formik
                initialValues={{
                  unbounce: false
                }}
                onSubmit={(values, { setSubmitting }) => {
                  this.setState({
                    loading: true
                  });
                  this.inviteTeam(values);
                }}
                validationSchema={Yup.object().shape({
                  // email: Yup.string()
                  //   .email("Please enter a valid email address")
                  //   .required("Email Required"),
                  //   role: Yup.string().required("Role Required"),
                  // firstName: Yup.boolean().required("First name Required")
                })}
              >
                {props => {
                  const {
                    values,
                    touched,
                    errors,
                    // isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit
                  } = props;
                  return (
                    <Form role="form" onSubmit={handleSubmit} novalidate>
                      <Row>
                        <Col xs-4>
                          <FormGroup className="mb-3">
                            <div className="custom-control custom-control-alternative custom-checkbox">
                              <input
                                // className="custom-control-input"
                                id="unbounce"
                                type="checkbox"
                                value={values.firstName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                className={
                                  "custom-control-input" ||
                                  (errors.firstName &&
                                    touched.firstName &&
                                    "error")
                                }
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="unbounce"
                              >
                                <img
                                  alt="..."
                                  width="190"
                                  src={require("../../assets/img/theme/unbounce-icon-dark.svg")}
                                />{" "}
                              </label>
                              {errors.firstName && touched.firstName && (
                                <div className="input-feedback">
                                  {errors.firstName}
                                </div>
                              )}
                            </div>
                          </FormGroup>
                        </Col>
                        
                      </Row>

                      <div className="text-center">
                        <Button
                          className="my-2 cta cta-fullwidth"
                          color="primary"
                          type="submit"
                        >
                          Continue
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              {/* </CardBody>
          </Card> */}
            </Col>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

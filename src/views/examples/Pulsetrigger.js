import React, { useState ,useEffect} from "react";
import Switch from "react-switch";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import { ChromePicker } from 'react-color';

import Slider from 'react-rangeslider';

import { Link, Redirect } from "react-router-dom";
import cookie from 'react-cookies';
import { PostData } from "../services/PostData";

class Alltriggers extends React.Component {
  constructor(props){
    super(props);
    this.state={
      loading : true,
    }
  }
  componentDidMount(){
    if(cookie.load('comingFrom') === "EDITTRIGGERS"){
      this.getCampaginTriggers();
    }
    this.setState({
      loading:false,
    })
  }
  getCampaginTriggers = () =>{
    
    const Popiupid = { pop_up_id : cookie.load("popupId")}
    PostData("ms1", "getpopuptrigger", Popiupid).then(data=>{
      console.log(data)
      // if(data.message === "")
    })
  }
  render() {
    const intialTrigger = {
      popup: {
        onPageLoad: {
          enable: true,
          startDelay: 0,
        },
        onPageExit: {
          enable: false,
        },
        onPageScroll: {
          enable: false,
          pagePosition: 0,
        },
        onPageCount: {
          enable: false,
          visits: 5,
        },
        onPageClick: {
          enable: false,
          count: 5,
        },
      },
    }
    return <Triggerlist triggerData={intialTrigger} type={this.props.type} campaignId={this.props.campaignId}/>;
  }
}

export default Alltriggers;

const Triggerlist = (props) => {
const type = props.type;
const campaignId = props.campaignId;
  const { popup } = props.triggerData;
  const IconRoundup = "https://img.icons8.com/color/144/000000/fire-element--v2.png";
  const IconNotification = "https://img.icons8.com/fluent/96/000000/briefcase.png";
  let {
    onPageClick,
    onPageLoad,
    onPageCount,
    onPageExit,
    onPageScroll,
  } = popup;
  const [checkd, setCheckd] = useState(false);
  const [colorPicker, setColorPicker] = useState(false);
  const [thisColor, pickedColor] = useState({ 'background': "white" });
  const [primaryColor, setPrimaryColor] = useState({ 'color': "#06062e" });
  const [primaryClrPopup, setPrimaryClrPopup] = useState(false);
  const [secondaryColor, setSecondaryColor] = useState({ 'color': "#7d90b8" });
  const [secondaryClrPopup, setSecondaryClrPopup] = useState(false);
  const [linkColor, setLinkColor] = useState({ 'color': "#516eff" });
  const [linkClrPopup, setLinkClrPopup] = useState({ 'color': "#516eff" });

  const [message, setMessage] = useState("are viewing this page");
  const [title, setTitle] = useState("");
  const [icon, setIcon] = useState("" );

  const [borderRadius, setBorderRadius] = useState(0);

  const [onPageLoadCheckd, setOnPageLoadCheckd] = useState(onPageLoad.enable);
  const [timeDelay, setTimeDelay] = useState(onPageLoad.startDelay/1000);
  const [onPageExitCheckd, setOnPageExitCheckd] = useState(onPageExit.enable);
  const [onPageScrollCheckd, setOnPageScrollCheckd] = useState(onPageScroll.enable);
  const [scrollValue, setScrollValue] = useState(onPageScroll.pagePosition);
  const [onPageCountCheckd, setOnPageCountCheckd] = useState(onPageCount.enable);
  const [pageCount, setPageCount] = useState(onPageCount.visits);
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    // Update the document title using the browser API
   if(type === "notification"){
setIcon(IconNotification);
setTitle("No credit card required");
setMessage("Get started with our freeplan! Any reason not to give this a try")
   }else if(type === "livevisitor"){
    setTitle("Marketers");
    setMessage("are viewing this page ")
   }
   else{
    setIcon(IconRoundup);
    setTitle("Peoples");
    setMessage("are started a free trial ")
   }
  },[type]);
  const handleChange = (checked, value) => {
    setCheckd(checked);
  };
  const handleChanges = (value, name) => {
    console.log(value, name)
    if( name === 'background'){pickedColor({ 'background': value });setColorPicker(false)}
    else if(name === 'borderRadius'){setBorderRadius( value);}
    else if(name === 'primarycolor'){setPrimaryColor( { color: value });setPrimaryClrPopup(false)}
    else if(name === 'secondarycolor'){setSecondaryColor( { color: value });setSecondaryClrPopup(false)}
  }
  const handleBackgroundColor = (value) => {
    pickedColor({ background: value })
    
}
const handleTextColor = (value, name) => {
  if(name === "primarycolor"){
  setPrimaryColor({ color: value })  }
  else if(name === 'secondarycolor'){
    setSecondaryColor( { color: value })
  }

}
  const onPageloadEnableChange = (checked) => {
    setOnPageLoadCheckd(checked);
    return onPageLoad.enable = checked;
  };
  const onPageExitEnableChange = (checked) => {
    setOnPageExitCheckd(checked);
    return onPageExit.enable = checked;
  };
  const onPageScrollEnableChange = (checked) => {
    setOnPageScrollCheckd(checked);
    return onPageScroll.enable = checked;
  };
  const onPageCountEnableChange = (checked) => {
    setOnPageCountCheckd(checked);
  };
  
  const onPageloadDelayChange = (event) => {
    setTimeDelay(event.target.value)
  };

  const onMessageChange = (event) => {
    // console.log(event.target.value);
    setMessage(event.target.value)
  };
  const onIconChange = (event) => {
    // console.log(event.target.value);
    setIcon(event.target.value)
  };
  const onTitleChange = (event) => {
    // console.log(event.target.value);
    setTitle(event.target.value)
  };
  const onScrollDelayChange = (event) => {
    // console.log(event.target.value);
    setScrollValue(event.target.value)
    return onPageScroll.pagePosition = event.target.value;
  };
  const onPagecountChange = (event) => {
    // console.log(event.target.value);
    setPageCount(event.target.value)
 const red = onPageCount.visits = event.target.value
    return ;
  };
  const onDone = () =>{
     
    const savedTrigger = {  
        style: {
        thisColor,
        primaryColor,
        secondaryColor,
        borderRadius
        },
        content: {
         title : title,
         description : message  
        },
        image:{
          icon
        }
      };
   var design = JSON.stringify(savedTrigger);
   
    const proffJson = {campaign_id:campaignId,campaign_design:design}


    PostData("ms1","update-campaign-html", proffJson).then(response => console.log(response));


setRedirect(true);

  }
  const redirectFun = () =>{
    if(redirect){
      
      return <Redirect  to={{pathname: "/admin/socialproof/socialprooff-overview",
      state: {
        campaignId: campaignId
      }
    }}/>
    }
   
  }
  // const savedTrigger = {
  //   popup: {
  //     onPageLoad: {
  //       enable: onPageLoadCheckd,
  //       startDelay: timeDelay,
  //     },
  //     onPageExit: {
  //       enable: onPageExitCheckd,
  //     },
  //     onPageScroll: {
  //       enable: onPageScrollCheckd,
  //       pagePosition: scrollValue,
  //     },
  //     onPageCount: {
  //       enable: onPageCountCheckd,
  //       visits: pageCount,
  //     },
  //     onPageClick: {
  //       enable: false,
  //       count: 0,
  //     },
  //   },



  // }
  // console.log(savedTrigger)
  return (
    
    <React.Fragment>
      { redirectFun() }
      {/* <Row>
      <Col md="12" className="display-center mt-3 pb-3 cancel">
             <Link to="/editor/builder" className="secondary-cta">
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  
                </span>
                Back
              </Link> 
         
            </Col>
     
</Row>  */}
<Row>


     <div className="col-md-8">
      
      {/* <h2>Preview</h2> */}

        <div className="preview-wrap">
          <div className="prev-head">
            <div className="template__circlebox">
              <div className="template__circle"></div>
              <div className="template__circle"></div>
              <div className="template__circle"></div>
            </div>
          </div>
          <div className="prev-img" style={{width:"100%", padding:"10px", minHeight:"300px"}}>
            
           

{/* <div className="notification-container" style={{background : thisColor.background, borderRadius : borderRadius+"px", color: secondaryColor.color}}>
   <img  style={{borderRadius : borderRadius+"px"}} src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0cfd9e3f3d9596c7a6c7c0_Recent%20Activity%20Icon-min.jpg" alt="Recent Activity Icon " />
   <div class="notification-text">
      <div><strong class="dark-bold-text"style={{color : primaryColor.color}}>Mary</strong> from <strong style={{color : primaryColor.color}}
      class="dark-bold-text">Austin, TX</strong></div>
      <div>{message}</div>
      <div class="verified-container">
         <div class="text-block-109" style={{color : secondaryColor.color}}>2 min ago</div>
         <img className="verified-check _8px-left-margin" src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0be3421520077020700b59_Check%20Icon%20(1).svg" alt="Pulse Verified Check" />
         <div style={{color : linkColor.color}}>
           
        

Verified by Owl</div>
      </div>
   </div>
</div> */}


          {type && type === "roundup" &&
           <div  class="notification-container"  style={{background : thisColor.background, borderRadius : borderRadius+"px", color: secondaryColor.color}}>
             {/* <img src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0be3423f3d956e58a20e2d_Hot%20Streaks%20Icon%20(1).svg"  /> */}
           <img src={icon} width="76" alt="Hot Streaks Icon"/><div class="notification-text"><div><strong class="dark-bold-text" style={{color : primaryColor.color}}>13&nbsp;{title}</strong> {message}</div><div>in the last 24 hours</div><div class="verified-container"><img src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0be3421520077020700b59_Check%20Icon%20(1).svg" alt="Pulse Verified Check" class="verified-check" /><div>Verified by Owl
           </div></div></div></div>



}
{type && type === "notification" && <div class="notification-container"  style={{background : thisColor.background, borderRadius : borderRadius+"px", color: secondaryColor.color}}>


<img src={icon} alt="icon" title="icon" className="img-fluid" width="76" />
                                
  <div class="notification-text"><div>
    <strong class="dark-bold-text" style={{color : primaryColor.color}}>{title}</strong></div><div>{message}</div>
    <div class="verified-container">
      <img src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0be3421520077020700b59_Check%20Icon%20(1).svg" alt="Pulse Verified Check" class="verified-check" /><div>Verified by Owl
</div>
      </div>
      </div>
      </div>}
      {type && type === "livevisitor" &&
<div class="notification-container"  style={{background : thisColor.background, borderRadius : borderRadius+"px", color: secondaryColor.color}}>


 <div class="circle-ripple" style={{  backgroundColor: "#0673ff"}}><span className="count"> 31</span> </div>
                                
  <div class="notification-text">
    <div>
    <strong class="dark-bold-text" style={{color : primaryColor.color}}>83&nbsp;{title}</strong></div><div>{message}</div>
    <div class="verified-container">
      <img src="https://uploads-ssl.webflow.com/59318798d83bff2781822428/5e0be3421520077020700b59_Check%20Icon%20(1).svg" alt="Pulse Verified Check" class="verified-check" /><div>Verified by Owl
</div>
      </div>
      </div>
      </div>}
     


          </div>
          
       
          </div>
        </div>
        <div className="col-md-4 pulse-editor-wrap">
        <Button 
              className="cta"
              onClick={onDone}
              >
                <span
                  className="text-center"
                  style={{ display: "inline-block" }}
                >
                  <i class="material-icons">close</i>
                </span>
                Save
              </Button>
      <h4 className="mb-2">Design</h4>


<div className="two-column">
                        <label>Border Radius</label>
                        <div className="rangeslider-wrapper">
                        <div className="rangeslider-slider">
                            <Slider min={1} max={100} step={1}
                                value={borderRadius}
                                orientation="horizontal"
                                tooltip={false}
                                onChange={(e) => handleChanges(e, 'borderRadius')}
                            />
                            
                        </div>
                        {/* <div className="rangeslider-value">
                            {borderRadius}
                        </div> */}
                        </div>
                    </div>


                    <div className="two-column">
                        <label>Background Color</label>

                        <div className="color-picker-swatch" onClick={() => setColorPicker(true)}>
                            <div className="color-picker-color" style={thisColor}></div>
                        </div>
                        
                        {colorPicker && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setColorPicker(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker 
                                color={thisColor.background} 
                                onChange={(e) => handleBackgroundColor(e.hex)} 
                                disableAlpha 
                                />
                                <button className='color-picker-button'
                                 onClick={() => handleChanges(thisColor.background, 'background')}>Ok</button>
                            </div>
                        </div>}  
                      
                    </div>


                    <div className="two-column">
                        <label>Primary Text Color</label>

                        <div className="color-picker-swatch" onClick={() => setPrimaryClrPopup(true)}>
                            <div className="color-picker-color"  style={{background : primaryColor.color}}></div>
                        </div>
                        {primaryClrPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setPrimaryClrPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker 
                                color={primaryColor.color} 
                                onChange={(e) => handleTextColor(e.hex , 'primarycolor' )} 
                                disableAlpha 
                                />
                                <button className='color-picker-button'
                                 onClick={() => handleChanges(primaryColor.color, 'primarycolor')}>Ok</button>
                            </div>
                        </div>}
                            
                      
                    </div>


                    <div className="two-column">
                        <label>Secondary Text Color</label>

                        <div className="color-picker-swatch" onClick={() => setSecondaryClrPopup(true)}>
                            <div className="color-picker-color" style={{background : secondaryColor.color}}></div>
                        </div>
                        {secondaryClrPopup && <div className="color-picker-popover">
                            <div className='color-picker-cover' onClick={() => setSecondaryClrPopup(false)} />
                            <div className='color-picker-wrapper'>
                                <ChromePicker 
                                color={secondaryColor.color} 
                                onChange={(e) => handleTextColor(e.hex,'secondarycolor')} 
                                disableAlpha 
                                />
                                <button className='color-picker-button'
                                 onClick={() => handleChanges(secondaryColor.color, 'secondarycolor')}>Ok</button>
                            </div>
                        </div>}
                            
                      
                    </div>
                    <h4 className="mb-2">Title</h4>
      
          <React.Fragment>
            <div>
              <div>
               
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-notification-70" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Site name"
                                type="text"
                                name="message"
                                // onChange={this.handleChangeInput}
                                value={title}
                                onChange={onTitleChange}
                                
                              />
                            </InputGroup>
                           
                          </FormGroup>
               
              </div>
              <div>
              
              </div>
            </div>
          </React.Fragment>
       




      <h4 className="mb-2">Message</h4>
      
          <React.Fragment>
            <div>
              <div>
               
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-notification-70" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="Site name"
                                type="text"
                                name="message"
                                // onChange={this.handleChangeInput}
                                value={message}
                                onChange={onMessageChange}
                                
                              />
                            </InputGroup>
                           
                          </FormGroup>
               
              </div>
              <div>
              
              </div>
            </div>
          </React.Fragment>


          <h4 className="mb-2">Icon</h4>
      
          <React.Fragment>
            <div>
              <div>
               
                          <FormGroup className="mb-3">
                            <InputGroup className="input-group-alternative">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="ni ni-notification-70" />
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input
                                placeholder="image url"
                                type="text"
                                name="message"
                                // onChange={this.handleChangeInput}
                                value={icon}
                                onChange={onIconChange}
                                
                              />
                            </InputGroup>
                           
                          </FormGroup>
               
              </div>
              <div>
              
              </div>
            </div>
          </React.Fragment>
       


      {/* <h2 className="mb-3">Display Rules</h2>

        
        //popupcomeshese */}
      </div>

      
  
        </Row>
 
    </React.Fragment>
  );
};




/////Targetting

const Targetting = () =>{
  const targetList = {
    popup: {
      device: {
        mobile: true,
        desktop:true,
      },
      displayTo: {
        enable: true,
        visitors:"everyone"
      },
      country: {
        enable: false,
        country: "",
      },
      viewCount: {
        enable: false,
        visits: 0,
      },
      ipBlocking: {
        enable: false,
        ip: [],
      },
    },
  };
  const { popup } = targetList;
  return console.log(popup)
}